<!doctype html>
<html lang="en">
  <head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <link rel="stylesheet" href="<?= base_url('assets/css/bootstrap.min.css') ?>">
  <link rel="stylesheet" href="<?= base_url('assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.css') ?>">
  <link rel="stylesheet" href="<?= base_url('assets/css/select2.min.css') ?>">
  <link rel="stylesheet" href="<?= base_url('assets/css/backend_panel.css') ?>">
  <link rel="stylesheet" href="<?= base_url('assets/css/clock/clockpicker.css') ?>">
  <link rel="stylesheet" href="<?= base_url('assets/css/clock/standalone.css') ?>">
  <link rel="stylesheet" href="<?= base_url('assets/css/clock/bootstrap-clockpicker.css') ?>">
  <link rel="stylesheet" href="<?= base_url('assets/css/clock/bootstrap-clockpicker.min.css') ?>">
  <link rel="stylesheet" href="<?= base_url('assets/css/clock/jquery-clockpicker.css') ?>">
  <link rel="stylesheet" href="<?= base_url('assets/css/clock/jquery-clockpicker.min.css') ?>">
  <link rel="stylesheet" href="<?= base_url('assets/plugins/font-awesome-4.7.0/css/font-awesome.min.css') ?>">
  <link rel="stylesheet" href="<?= base_url('assets/plugins/font-awesome-4.7.0/css/font-awesome.min.css') ?>">


	<script type="text/javascript">
		var base_url = "<?= base_url() ?>";
    var service_url = "<?= $this->configs->url ?>";
	</script>
    <title>Hello, world!</title>
  </head>
  <body>

<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<link rel="stylesheet" href="<?= base_url('assets/css/bootstrap.min.css') ?>">
<link rel="stylesheet" href="<?= base_url('assets/css/select2.min.css') ?>">
<link rel="stylesheet" href="<?= base_url('assets/css/adminpanel.css') ?>">
<link rel="stylesheet" href="<?= base_url('assets/css/mainPage.css') ?>">

<link rel="shortcut icon" href="<?= base_url('assets/img/favicon.png') ?>">
<link rel="apple-touch-icon" href="<?= base_url('assets/img/favicon_60x60.png') ?>">
<link rel="apple-touch-icon" sizes="76x76" href="<?= base_url('assets/img/favicon_76x76.png') ?>">
<link rel="apple-touch-icon" sizes="120x120" href="<?= base_url('assets/img/favicon_120x120.png') ?>">
<link rel="apple-touch-icon" sizes="152x152" href="<?= base_url('assets/img/favicon_152x152.png') ?>">

<link rel="stylesheet" href="<?= base_url('assets/plugins/bootstrap/dist/css/bootstrap.min.css') ?>">
<link rel="stylesheet" href="<?= base_url('assets/plugins/slick-carousel/slick/slick.css') ?>">
<link rel="stylesheet" href="<?= base_url('assets/plugins/animate.css/animate.min.css') ?>">
<link rel="stylesheet" href="<?= base_url('assets/plugins/animsition/dist/css/animsition.min.css') ?>">

<link rel="stylesheet" href="<?= base_url('assets/css/themify-icons.css') ?>">
<link rel="stylesheet" href="<?= base_url('assets/plugins/font-awesome/css/font-awesome.min.css') ?>">
<link id="theme" rel="stylesheet" href="<?= base_url('assets/css/themes/theme-red.min.css') ?>">


    
    


    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header modal-header-lg dark bg-dark">
                <div class="bg-image"><img src="<?= base_url(); ?>/assets/img/photos/modal-add.jpg" alt=""></div>
                <h4 class="modal-title">Şifreyi Sıfırla</h4>
            </div>
            <?php echo form_open('auth/reset_password/' . $code);?>
                <div class="modal-product-details">
                    <div class="example-box-content">
                        
                        <div class="form-group">
                            <label>Yeni Şifre</label>
                            <input name="new" id="new" type="password" pattern="^.{6}.*$" class="form-control">
                        </div>
                        <div class="form-group">
                            <label>Yeni Şifre Onayı</label>
                            <input name="new_confirm" id="new_confirm" type="password" pattern="^.{6}.*$" class="form-control">
                        </div>

                        <?php echo form_input($user_id);?>
                        <?php echo form_hidden($csrf); ?>

                    </div>
                </div>
     
                <button type="submit" class="modal-btn btn btn-success btn-block btn-lg"><span>kaydet</span></button>
                
           <?php echo form_close();?>
        </div>
    </div>

    


<?php $this->load->view('frontend/template/footer'); ?>
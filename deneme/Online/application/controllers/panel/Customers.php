<?php

class Customers extends Panel{
    public function __construct()
    {
        parent::__construct();
        $this->load->model("backend/customers_model");
        $this->load->model("backend/orders_model");
        $this->load->model("backend/opinions_model");
        $this->load->model("frontend/address_model");

		$this->load->helper(array('form', 'url'));
		$this->load->library('form_validation');

    }

    public function index()
	{
        $customers = $this->customers_model->customers();
        $groups = $this->customers_model->customer_groups();
        
        $this->load->view("backend/customers",compact('customers','groups'));
    }
    
    public function delete()
    {
        $customer_id = $this->input->post('customer_id');
        $status = $this->input->post('status');
        $this->customers_model->delete($customer_id,$status);
    }

    public function orders()
    {
        $date = date('Y-m-d');
        $orders = $this->orders_model->orders($date);
        $restaurantPos = $this->Posmaks->getRestaurants([]);
        $customers = $this->customers_model->customers();
        $payment_type = $this->Posmaks->getPayment_Types([]);
        $customer_address = $this->address_model->customers_address();
        $pos_orders = $this->Posmaks->getAll_Order([]);
        $order_detail = $this->Posmaks->getOrder_Detail([]);


        $this->load->view("backend/orders",compact('orders','restaurantPos','customers','payment_type','customer_address','pos_orders','order_detail'));
    }

    public function order_search()
    {
        $start_date = $this->input->post('start_date');
        $finish_date = $this->input->post('finish_date');

        $orders = $this->orders_model->order_search($start_date,$finish_date);
        $restaurantPos = $this->Posmaks->getRestaurants([]);
        $customers = $this->customers_model->customers();
        $payment_type = $this->Posmaks->getPayment_Types([]);
        $customer_address = $this->address_model->customers_address();
        $pos_orders = $this->Posmaks->getAll_Order([]);
        $order_detail = $this->Posmaks->getOrder_Detail([]);

        $data = array(
            "orders" => $orders,
            "restaurantPos" => $restaurantPos,
            "customers" => $customers,
            "payment_type" => $payment_type,
            "customer_address" => $customer_address,
            "pos_orders" => $pos_orders,
            "order_detail" => $order_detail
        );

        echo json_encode($data);

    }

    public function order_detail()
    {
        $sale_id = $this->input->post('sale_id');
        $order_detail = $this->Posmaks->getOrder_Detail([
            'sale_id' => $sale_id
        ]);
        echo json_encode($order_detail);
    }

    public function opinions()
    {
        $opinions = $this->opinions_model->opinions();
        $this->load->view("backend/opinions",compact('opinions'));
    }
}
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends My_Controller {


	public function __construct()
	{
		parent::__construct();
		$this->load->model("frontend/address_model");
		$this->load->model("frontend/sale_model");
		$this->load->model("frontend/users_model");
		$this->load->model("backend/sehir_table_model");
		$this->load->model("backend/products_model");
		$this->load->model("backend/restaurants_model");
		$this->load->model("backend/slide_model");
		$this->load->model("backend/settings_model");
		$this->load->model("backend/discounts_model");
		$this->load->model("backend/payment_type_model");
		$this->load->model("backend/customers_model");
        $this->load->model("backend/opinions_model");
        $this->load->model("backend/min_area_price_model");

		$this->load->helper(array('form', 'url'));
		$this->load->library('ion_auth','form_validation');


		$this->min_password_length = $this->config->item('min_password_length', 'ion_auth');
		$this->max_password_length = $this->config->item('max_password_length', 'ion_auth');
	}

	public function index()
	{		
		$restaurantPos = $this->Posmaks->getRestaurants([
			]);

		$restaurants = $this->restaurants_model->restaurants();
		
		if(count($restaurants) == 1)
		{
			foreach ($restaurants as $restaurant) {
				$restaurant_id = $restaurant->id;
			}
			$this->main($restaurant_id);
		}
		else
		{
			$this->load->view("frontend/locationPage",compact('restaurants','restaurantPos'));
		} 

	}

	
	public function slider()
	{
		$slider_image = $this->slide_model->getImages();
		echo json_encode($slider_image);
	}

	public function logo()
	{
		$logo = $this->website->image;
		echo json_encode($logo);
	}

	
	public function main($restaurant_id = null)
	{
		$locations = $this->restaurants_model->restaurants();
		$restaurant_info = $this->restaurants_model->restaurants_info($restaurant_id);
		$products = $this->products_model->products($restaurant_id);
		$this->load->view("frontend/main",compact('locations','products','restaurant_id','restaurant_info'));

	}

	public function get_Products_Additional()
	{
		$product_id = $this->input->post('product_id');
		$products_additional = $this->Posmaks->getProducts_Additional([
			'product_id' => $product_id
		]);
		echo json_encode($products_additional);
	}

	public function get_FindProductsWithRelatedItems()
	{
		$location_id = $this->input->post('restaurant_id');

		$system_products = $this->Posmaks->getFindProductsWithRelatedItems([
			'location_id' => $location_id
		]);

		echo json_encode($system_products);
	}

	public function get_products_group()
	{
		$products_group = $this->Posmaks->getProductsGroup([
		]);
		echo json_encode($products_group);

	}

	public function get_products()
	{

		$restaurant_id = $this->input->post('restaurant_id');
		$products = $this->Posmaks->getProducts([
			'location_id' => $restaurant_id
		]);
		echo json_encode($products);

	}

	public function order($restaurant_id)
	{
		try {
			
		
		$user_id = $this->ion_auth->user()->row()->id;

		$restaurant_info = $this->restaurants_model->restaurants_info($restaurant_id);
		if(!$restaurant_info)
		{
			throw new Exception("Böyle bir restaurant yok");
		}

		$products = $this->products_model->products($restaurant_id);
		$discounts = $this->discounts_model->index();
		$user_orders = $this->sale_model->get_Order($user_id);
		$payment_type = $this->payment_type_model->index();
		$payment_type_pos = $this->Posmaks->getPayment_Types([]);
		$address = $this->address_model->user_address($user_id);
		$basket_products = $this->cart->contents();

		$this->load->view("frontend/orderPage",compact('products','restaurant_info','restaurant_id','address','discounts','user_orders','payment_type','payment_type_pos','basket_products'));

		} catch (Exception $e) {
			echo json_encode([
				'status' => 'error',
				'message' => $e->getMessage(),
			]);
		}
	}

	public function userEdit($id)
	{
		$address = $this->address_model->user_address($id);
		$user = $this->users_model->user($id);
		$sehirler = $this->sehir_table_model->sehir();
		$user_orders = $this->sale_model->get_Order($id);

		$this->load->view("frontend/auth/edit",compact('address','user','sehirler','user_orders'));
	}

	public function user_Orders()
	{
		$user_id = $this->ion_auth->user()->row()->id;
		$pos_customer_id = $this->users_model->customer_id($user_id);
		$user_orders = $this->sale_model->get_Order($user_id);
		$restaurantPos = $this->Posmaks->getRestaurants([]);
        $customers = $this->customers_model->customers();
        $payment_type = $this->Posmaks->getPayment_Types([]);
        $customer_address = $this->address_model->customers_address();

        foreach ($pos_customer_id as $pos) {
        	$customer_id = array(
        		"customer_id" => $pos
        	);
        }

        try {
			$pos_customer_order = $this->Posmaks->customer_Order($customer_id);

		echo json_encode(compact('user_orders','restaurantPos','customers','payment_type','customer_address','pos_customer_order'));

		} catch (Exception $e) {
			
		}
	}

	public function user_order_detail()
    {
        $sale_id = $this->input->post('sale_id');
        $order_detail = $this->Posmaks->getOrder_Detail([
            'sale_id' => $sale_id
        ]);
        echo json_encode($order_detail);
    }

	public function opinions()
	{
		try
		{

			if($this->input->post())
			{
				$this->form_validation->set_rules('first_name', 'first_name Adı', 'trim|required');
				$this->form_validation->set_rules('email', 'email', 'trim|required|valid_email');
				$this->form_validation->set_rules('message', 'message', 'trim|required');
				$this->form_validation->set_rules('lezzet', 'lezzet', 'trim|required');
				$this->form_validation->set_rules('sunum', ' sunum', 'trim|required');
				$this->form_validation->set_rules('ilgi', 'ilgi', 'trim|required');
				$this->form_validation->set_rules('servis', 'servis ', 'trim|required');
				$this->form_validation->set_rules('fiyat', 'fiyat', 'trim|required');
				$this->form_validation->set_rules('temizlik', 'temizlik', 'trim|required');

				if(!$this->form_validation->run()) {
					throw new Exception(validation_errors());
				}

				$first_name = $this->input->post('first_name');
				$email 		= $this->input->post('email');
				$message 		= $this->input->post('message');
				$lezzet 	= $this->input->post('lezzet');
				$sunum = $this->input->post('sunum');
				$ilgi 	= $this->input->post('ilgi');
				$servis = $this->input->post('servis');
				$fiyat 	= $this->input->post('fiyat');
				$temizlik = $this->input->post('temizlik');

				$data = array(
					'first_name' 	=> $first_name,
					'email' 	 	=> $email,
					'message' 	 	=> $message,
					'flavor' 	 	=> $lezzet,
					'presentations' => $sunum,
					'interest' 	 	=> $ilgi,
					'service' 	 	=> $servis,
					'price' 	 	=> $fiyat,
					'cleaning' 	 	=> $temizlik
				);

				if (!$this->users_model->opinions($data))
				{
					
				}

				echo json_encode([
					'status' => 'success',
					'message' => 'Kayıt başarılı',
				]);
			}
		}
		catch (Exception $e) {

			echo json_encode([
				'status' => 'error',
				'message' => $e->getMessage(),
			]);
		}

		
	}

	

}

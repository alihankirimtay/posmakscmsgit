<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Address extends My_Controller {


	public function __construct()
	{
		parent::__construct();
		$this->load->model("frontend/address_model");
		$this->load->model("frontend/users_model");
		$this->load->model("backend/sehir_table_model");
		$this->load->model("backend/ilce_table_model");
		$this->load->model("backend/mahalle_table_model");

		$this->load->helper(array('form', 'url'));
		$this->load->library('ion_auth','form_validation');
		

		$this->min_password_length = $this->config->item('min_password_length', 'ion_auth');
		$this->max_password_length = $this->config->item('max_password_length', 'ion_auth');
	}

	public function userEdit()
	{
		$user_id = $this->ion_auth->user()->row()->id;

		$address = $this->address_model->user_address($user_id);
		$user = $this->users_model->user($user_id);
		$sehirler = $this->sehir_table_model->sehir();

		echo json_encode(compact('address','user','sehirler'));
		//$this->load->view("frontend/auth/edit",compact('address','user','sehirler'));
	}

	public function addressEdit()
	{
		$address_id = $_POST["address_id"];
		$addressEdit = $this->address_model->addressEdit($address_id);
		echo json_encode($addressEdit);
	}

	public function get_sehir()
	{
		$sehirler = $this->sehir_table_model->sehir();
		echo json_encode(compact('sehirler'));
	}

	public function get_ilce()
	{
		$sehir_key = $this->input->post('sehir_key');
		$ilceler = $this->ilce_table_model->ilce($sehir_key);
		if(count($ilceler)>0)
		{
			$ilce_select_box = '';
			foreach($ilceler as  $ilce)
			{
				$ilce_select_box .='<option value="'.$ilce->ilce_id.'" key="'.$ilce->ilce_key.'" >'.$ilce->ilce_title.'</option>';
			}

			echo json_encode($ilce_select_box);
		}
	}

	public function get_mahalle()
	{
		$ilce_key = $this->input->post('ilce_key');
		$mahalleler = $this->mahalle_table_model->mahalle($ilce_key);
		if(count($mahalleler)>0)
		{
			$mahalle_select_box = '<optgroup data-ilce="'.$ilce_key.'" label="'.$ilce_key.'">';
			foreach($mahalleler as  $mahalle)
			{
				$mahalle_select_box .='<option value="'.$mahalle->mahalle_id.'" key="'.$mahalle->mahalle_key.'" >'.$mahalle->mahalle_title.'</option>';
			}
			$mahalle_select_box .= '</optgroup>';
			echo json_encode($mahalle_select_box);
		}
	}

	public function insert()
	{
		
		try
		{
			if($this->input->post())
			{
				$this->form_validation->set_rules('adres_name', 'Adres Adı', 'trim|required');
				$this->form_validation->set_rules('sehir', 'sehir', 'required');
				$this->form_validation->set_rules('ilce', 'ilce', 'required');
				$this->form_validation->set_rules('mahalle', 'mahalle', 'required');
				$this->form_validation->set_rules('acik_adres', 'Acik Adres', 'trim|required');

				if(!$this->form_validation->run()) {
					throw new Exception(validation_errors());
				}

				$user_id 	= $this->ion_auth->user()->row()->id;
				$adres_name = $this->input->post('adres_name');
				$sehir 		= $this->input->post('sehir');
				$ilce 		= $this->input->post('ilce');
				$mahalle 	= $this->input->post('mahalle');
				$acik_adres = $this->input->post('acik_adres');

				$sehir_info = $this->address_model->address_control($sehir);
				$ilce_control = 0;
				$mahalle_control = 0; 
			
				foreach ($sehir_info as $address_control) {
					if($ilce == $address_control->ilce_id)
					{
						$ilce_control = 1;
					}
					if($mahalle == $address_control->mahalle_id)
					{
						$mahalle_control = 1;
					}
				}

				if($ilce_control == 1 && $mahalle_control == 1)
				{

				}
				else
				{
					throw new Exception("Adreslerde Tutarsızlık Var");
				}


				$data = array(
					'user_id' 	 => $user_id,
					'adres_name' => $adres_name,
					'sehir_id' 		 => $sehir,
					'ilce_id' 		 => $ilce,
					'mahalle_id' 	 => $mahalle,
					'acik_adres' => $acik_adres
				);

				if (!$this->address_model->add($data))
				{
					throw new Exception($this->ion_auth->errors());
				}
				 
				 echo json_encode([
					'status' => 'success',
					'message' => 'Kayıt başarılı',
				]);
			}
		}
		catch (Exception $e) {

			echo json_encode([
				'status' => 'error',
				'message' => $e->getMessage(),
			]);
		}	

	}

	public function update()
	{
		try
		{
			if($this->input->post())
			{
				$this->form_validation->set_rules('adres_name', 'Adres Adı', 'trim|required');
				$this->form_validation->set_rules('sehirEdit', 'Sehir', 'trim|required');
				$this->form_validation->set_rules('ilceEdit', 'İlce', 'trim|required');
				$this->form_validation->set_rules('mahalleEdit', 'Mahalle', 'trim|required');
				$this->form_validation->set_rules('acik_adres', 'Acik Adres', 'trim|required');

				if(!$this->form_validation->run()) {
					throw new Exception(validation_errors());
				}

				$address_id = $this->input->post('address_id');
				$adres_name = $this->input->post('adres_name');
				$sehir 		= $this->input->post('sehirEdit');
				$ilce 		= $this->input->post('ilceEdit');
				$mahalle 	= $this->input->post('mahalleEdit');
				$acik_adres = $this->input->post('acik_adres');

				$sehir_info = $this->address_model->address_control($sehir);
				$ilce_control = 0;
				$mahalle_control = 0; 
			
				foreach ($sehir_info as $address_control) {
					if($ilce == $address_control->ilce_id)
					{
						$ilce_control = 1;
					}
					if($mahalle == $address_control->mahalle_id)
					{
						$mahalle_control = 1;
					}
				}

				if($ilce_control == 1 && $mahalle_control == 1)
				{

				}
				else
				{
					throw new Exception("Adreslerde Tutarsızlık Var");
				}
				

				$data = array(
					'adres_name' => $adres_name,
					'sehir_id' 		 => $sehir,
					'ilce_id' 		 => $ilce,
					'mahalle_id' 	 => $mahalle,
					'acik_adres' => $acik_adres
				);

				if (!$this->address_model->update($address_id,$data))
				{
					throw new Exception($this->ion_auth->errors());
				}
				 
				 echo json_encode([
					'status' => 'success',
					'message' => 'Kayıt başarılı',
				]);
			}

		}
		catch (Exception $e) {

			echo json_encode([
				'status' => 'error',
				'message' => $e->getMessage(),
			]);
		}	
	}

	public function delete($id)
	{
		$this->address_model->delete($id);
		redirect(base_url());

	}


}

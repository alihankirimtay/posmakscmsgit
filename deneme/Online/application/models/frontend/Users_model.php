<?php


class Users_model extends CI_Model
{
	public function user($id)
	{
		$users = $this->db->get_where("{$this->database_name}users",array('id' => $id));
		return $users->result();
	}

	public function customer_id($id)
	{
		$users = $this->db->select('pos_customer_id')->get_where("{$this->database_name}users",array('id' => $id));
		return $users->row();
	}


	public function delete($id)
	{
		$this->db->where('id', $id);
		$this->db->delete("{$this->database_name}users");

		$this->db->where('user_id', $id);
		$this->db->delete("{$this->database_name}address");
	}

	public function opinions($data)
	{
		$this->db->insert("{$this->database_name}opinions",$data);
	}
	

}
$(document).ready(function(){ 

	$("body").on("click","button[name=doSubmit]",function(e){

		e.preventDefault();

		var
		form = $(this).closest("form"),
		url = form.attr("action"),
		gourl = form.attr("gourl"),
		resultContainer = form.find(".result"),
		fileExists = form.find("input[type=file]"),
		formData = null;

		function doSuccess (result) {
			if (result.status === "success") {

				success_warning(result.message);
				
				resultContainer.html(`
					<div class="alert alert-success" role="alert">
						${result.message}
					</div>
				`);
				

				if (!!gourl) {
					location.href = gourl;
				}
	
			} else {

				var result_message = result.message;
				error_warning(result_message);
	
				resultContainer.html(`
					<div class="alert alert-danger" role="alert">
						${result.message}
					</div>
				`);
	
			}
		}

		function success_warning(result_message)
		{
			$("#Panel_Warning").css("display","block");
			$("#Panel_Warning .status").removeClass("modal-content bg-danger text-white");
			$("#Panel_Warning .status").addClass("modal-content bg-success text-white");
			$("#Panel_Warning .status .modal-title").text(result_message);
			setTimeout(warning_close, 2000);
		}

		function error_warning(result_message)
		{
			$("#Panel_Warning").css("display","block");
			$("#Panel_Warning .status").removeClass("modal-content bg-success text-white");
			$("#Panel_Warning .status").addClass("modal-content bg-danger text-white");
			$("#Panel_Warning .status .modal-title").text(result_message);
			setTimeout(warning_close, 2000);
		}

		function warning_close()
		{
			$("#Panel_Warning").css("display","none");
		}

		if (fileExists.length >= 1) {

			formData = new FormData(form[0]);

			$.ajax({
				url: url,
				dataType: "json",
				cache: false,
				contentType: false,
				processData: false,
				data: formData,                         
				type: "post",
				success: doSuccess
			});
		} else {

			formData = form.serializeArray();

			$.post(url, formData, doSuccess, "json");
		}

	});
});
	
<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_initial extends CI_Migration
{
  public function up()
  {

    $this->db->query('SET FOREIGN_KEY_CHECKS=0');

    // banks
    $this->db->query("CREATE TABLE `banks` (
      `id` int(11) NOT NULL AUTO_INCREMENT,
      `location_id` int(11) NOT NULL,
      `title` varchar(45) DEFAULT NULL,
      `amount` decimal(15,2) NOT NULL,
      `desc` text,
      `created` datetime DEFAULT NULL,
      `modified` datetime DEFAULT NULL,
      `active` tinyint(1) NOT NULL,
      PRIMARY KEY (`id`),
      KEY `fk_banks_locations1_idx` (`location_id`),
      CONSTRAINT `fk_banks_locations1` FOREIGN KEY (`location_id`) REFERENCES `locations` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8");



    // customers
    $this->db->query("CREATE TABLE `customers` (
      `id` int(11) NOT NULL AUTO_INCREMENT,
      `ys_id` varchar(32) DEFAULT NULL,
      `name` varchar(45) DEFAULT NULL,
      `phone` varchar(45) DEFAULT NULL,
      `address` text,
      `tax_office` varchar(100) DEFAULT NULL,
      `tax_no` varchar(100) DEFAULT NULL,
      `isSupplier` tinyint(1) NOT NULL,
      `amount` decimal(15,2) NOT NULL,
      `created` datetime DEFAULT NULL,
      `modified` datetime DEFAULT NULL,
      `deleted` datetime DEFAULT NULL,
      `active` tinyint(1) NOT NULL,
      PRIMARY KEY (`id`),
      KEY `idx_ys_id` (`ys_id`)
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8");



    // endofdays
    $this->db->query("CREATE TABLE `endofdays` (
      `id` int(11) NOT NULL AUTO_INCREMENT,
      `location_id` int(11) NOT NULL,
      `amount` decimal(15,2) NOT NULL,
      `cash` decimal(15,2) NOT NULL,
      `credit` decimal(15,2) DEFAULT NULL,
      `ticket` decimal(15,2) DEFAULT NULL,
      `sodexo` decimal(15,2) DEFAULT NULL,
      `multinet` decimal(15,2) DEFAULT NULL,
      `setcart` decimal(15,2) DEFAULT NULL,
      `cari` decimal(15,2) DEFAULT NULL,
      `start_date` datetime DEFAULT NULL,
      `end_date` datetime DEFAULT NULL,
      `created` datetime DEFAULT NULL,
      `modified` datetime DEFAULT NULL,
      `active` tinyint(1) NOT NULL,
      PRIMARY KEY (`id`),
      KEY `fk_endofdays_locations1_idx` (`location_id`),
      CONSTRAINT `fk_endofdays_locations1` FOREIGN KEY (`location_id`) REFERENCES `locations` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8");



    // expense_attachments
    $this->db->query("CREATE TABLE `expense_attachments` (
      `id` int(11) NOT NULL AUTO_INCREMENT,
      `expense_id` int(11) NOT NULL,
      `file` varchar(150) DEFAULT NULL,
      `name` varchar(150) DEFAULT NULL,
      PRIMARY KEY (`id`),
      KEY `fk_expense_attachments_expenses1_idx` (`expense_id`),
      CONSTRAINT `fk_expense_attachments_expenses1` FOREIGN KEY (`expense_id`) REFERENCES `expenses` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8");



    // expense_types
    $this->db->query("CREATE TABLE `expense_types` (
      `id` int(11) NOT NULL AUTO_INCREMENT,
      `title` varchar(45) NOT NULL,
      `desc` text,
      `created` datetime DEFAULT NULL,
      `modified` datetime DEFAULT NULL,
      `active` tinyint(1) NOT NULL DEFAULT '0',
      PRIMARY KEY (`id`)
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8");




    // expenses
    $this->db->query("CREATE TABLE `expenses` (
      `id` int(11) NOT NULL AUTO_INCREMENT,
      `location_id` int(11) DEFAULT NULL,
      `expense_type_id` int(11) DEFAULT NULL,
      `payment_type_id` int(11) DEFAULT NULL,
      `payment_date` datetime DEFAULT NULL,
      `user_id` int(11) DEFAULT NULL,
      `customer_id` int(11) DEFAULT NULL,
      `title` varchar(45) NOT NULL,
      `desc` text,
      `amount` decimal(19,2) NOT NULL DEFAULT '0.00',
      `tax` decimal(15,4) NOT NULL,
      `created` datetime DEFAULT NULL,
      `modified` datetime DEFAULT NULL,
      `active` tinyint(1) NOT NULL DEFAULT '0',
      `image` varchar(45) DEFAULT NULL,
      `has_stock` tinyint(1) NOT NULL DEFAULT '0',
      `receipt_date` datetime DEFAULT NULL,
      `is_paid` tinyint(1) NOT NULL,
      PRIMARY KEY (`id`),
      KEY `fk_expenses_locations1_idx` (`location_id`),
      KEY `fk_expenses_expense_types1_idx` (`expense_type_id`),
      KEY `fk_expenses_users1_idx` (`user_id`),
      KEY `fk_expenses_payment_types1_idx` (`payment_type_id`),
      KEY `fk_expenses_customers1_idx` (`customer_id`),
      CONSTRAINT `fk_expenses_customers1` FOREIGN KEY (`customer_id`) REFERENCES `customers` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
      CONSTRAINT `fk_expenses_expense_types1` FOREIGN KEY (`expense_type_id`) REFERENCES `expense_types` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
      CONSTRAINT `fk_expenses_locations1` FOREIGN KEY (`location_id`) REFERENCES `locations` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
      CONSTRAINT `fk_expenses_payment_types1` FOREIGN KEY (`payment_type_id`) REFERENCES `payment_types` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
      CONSTRAINT `fk_expenses_users1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8");



    // floors
    $this->db->query("CREATE TABLE `floors` (
      `id` int(11) NOT NULL AUTO_INCREMENT,
      `location_id` int(11) DEFAULT NULL,
      `title` varchar(45) NOT NULL,
      `desc` text,
      `created` datetime DEFAULT NULL,
      `modified` datetime DEFAULT NULL,
      `active` tinyint(1) NOT NULL,
      PRIMARY KEY (`id`),
      KEY `fk_pos_points_locations1_idx` (`location_id`),
      CONSTRAINT `fk_pos_points_locations10` FOREIGN KEY (`location_id`) REFERENCES `locations` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8");



    // inventory
    $this->db->query("CREATE TABLE `inventory` (
      `id` int(11) NOT NULL AUTO_INCREMENT,
      `location_id` int(11) NOT NULL,
      `title` varchar(255) NOT NULL,
      `desc` text,
      `barcode` varchar(255) DEFAULT NULL,
      `quantity` int(11) NOT NULL,
      `price` decimal(15,2) NOT NULL,
      `warranty_exp` datetime DEFAULT NULL,
      `created` datetime DEFAULT NULL,
      `modified` datetime DEFAULT NULL,
      `active` tinyint(1) NOT NULL DEFAULT '1',
      PRIMARY KEY (`id`),
      KEY `fk_inventory_locations1_idx` (`location_id`),
      CONSTRAINT `fk_inventory_locations1` FOREIGN KEY (`location_id`) REFERENCES `locations` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8");



    // inventory_histories
    $this->db->query("CREATE TABLE `inventory_histories` (
      `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
      `inventory_id` int(11) NOT NULL,
      `history` text,
      PRIMARY KEY (`id`),
      KEY `fk_inventory_histories_inventory1_idx` (`inventory_id`),
      CONSTRAINT `fk_inventory_histories_inventory1` FOREIGN KEY (`inventory_id`) REFERENCES `inventory` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8");



    // invoice_template
    $this->db->query("CREATE TABLE `invoice_template` (
      `location_id` int(11) NOT NULL,
      `logo` text,
      `header` text,
      `footer` text,
      KEY `fk_ticket_template_locations1_idx` (`location_id`),
      CONSTRAINT `fk_ticket_template_locations10` FOREIGN KEY (`location_id`) REFERENCES `locations` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8");



    // invoices
    $this->db->query("CREATE TABLE `invoices` (
      `id` int(11) NOT NULL AUTO_INCREMENT,
      `location_id` int(11) NOT NULL,
      `title` varchar(45) DEFAULT NULL,
      `description` text,
      `json` text,
      `default` tinyint(1) NOT NULL DEFAULT '0',
      `width` float(5,2) NOT NULL,
      `height` float(5,2) NOT NULL,
      `created` datetime DEFAULT NULL,
      `modified` datetime DEFAULT NULL,
      `deleted` datetime DEFAULT NULL,
      PRIMARY KEY (`id`),
      KEY `fk_invoices_locations1_idx` (`location_id`),
      CONSTRAINT `fk_invoices_locations1` FOREIGN KEY (`location_id`) REFERENCES `locations` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8");



    // location_has_products
    $this->db->query("CREATE TABLE `location_has_products` (
      `id` int(11) NOT NULL AUTO_INCREMENT,
      `location_id` int(11) NOT NULL,
      `product_id` int(11) NOT NULL,
      `price` decimal(15,4) DEFAULT NULL,
      `tax_inclusive` tinyint(1) NOT NULL DEFAULT '1',
      `master` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'ürün bu lokasyon için master ürün mü?\nestimated sale hesaplarken kullanılıyor.',
      `created` datetime DEFAULT NULL,
      `modified` datetime DEFAULT NULL,
      `active` tinyint(1) NOT NULL DEFAULT '1',
      PRIMARY KEY (`id`),
      KEY `fk_locations_has_products_products1_idx` (`product_id`),
      KEY `fk_locations_has_products_locations1_idx` (`location_id`),
      CONSTRAINT `fk_locations_has_products_locations1` FOREIGN KEY (`location_id`) REFERENCES `locations` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
      CONSTRAINT `fk_locations_has_products_products1` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8");



    // location_losts
    $this->db->query("CREATE TABLE `location_losts` (
      `id` int(11) NOT NULL AUTO_INCREMENT,
      `location_id` int(11) NOT NULL,
      `unsold` int(11) NOT NULL DEFAULT '0' COMMENT 'pictures printed but not sold',
      `waste` int(11) NOT NULL DEFAULT '0' COMMENT 'test pictures',
      `comps` int(11) NOT NULL DEFAULT '0' COMMENT 'pictures given for free',
      `unseen` int(11) NOT NULL DEFAULT '0',
      `date` datetime NOT NULL,
      `created` datetime DEFAULT NULL,
      `modified` datetime DEFAULT NULL,
      `active` tinyint(1) NOT NULL DEFAULT '0',
      PRIMARY KEY (`id`),
      KEY `fk_donations_locations1_idx` (`location_id`),
      CONSTRAINT `fk_donations_locations1000` FOREIGN KEY (`location_id`) REFERENCES `locations` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8");



    // locations
    $this->db->query("CREATE TABLE `locations` (
      `id` int(11) NOT NULL AUTO_INCREMENT,
      `title` varchar(45) NOT NULL,
      `desc` text,
      `currency` varchar(5) DEFAULT NULL,
      `prepaid_limit` int(11) NOT NULL DEFAULT '0' COMMENT 'kullanıcının resimleri bu limit sayısına eriştiğinde zip-boox üzerinde albüm oluşturlulur.',
      `dtz` varchar(100) NOT NULL DEFAULT 'UTC',
      `yemeksepeti` text,
      `created` datetime DEFAULT NULL,
      `modified` datetime DEFAULT NULL,
      `active` tinyint(1) NOT NULL DEFAULT '0',
      PRIMARY KEY (`id`)
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8");



    // logs
    $this->db->query("CREATE TABLE `logs` (
      `id` int(11) NOT NULL AUTO_INCREMENT,
      `user_id` int(11) DEFAULT NULL,
      `location_id` int(11) DEFAULT NULL,
      `type` int(11) DEFAULT NULL,
      `message` text,
      `json` text,
      `ip` varchar(16) DEFAULT NULL,
      `url_coming` varchar(255) DEFAULT NULL,
      `url_current` varchar(255) DEFAULT NULL,
      `created` datetime DEFAULT NULL,
      PRIMARY KEY (`id`),
      KEY `fk_logs_users1_idx` (`user_id`),
      KEY `fk_logs_locations1_idx` (`location_id`),
      CONSTRAINT `fk_logs_locations1` FOREIGN KEY (`location_id`) REFERENCES `locations` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
      CONSTRAINT `fk_logs_users1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8");



    // messages
    $this->db->query("CREATE TABLE `messages` (
      `id` int(11) NOT NULL AUTO_INCREMENT,
      `reciever` int(11) NOT NULL,
      `sender` int(11) NOT NULL,
      `message` text NOT NULL,
      `created` datetime NOT NULL,
      `viewed` varchar(45) NOT NULL DEFAULT '0',
      PRIMARY KEY (`id`),
      KEY `fk_messages_users1_idx` (`reciever`),
      KEY `fk_messages_users2_idx` (`sender`),
      CONSTRAINT `fk_messages_users1` FOREIGN KEY (`reciever`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
      CONSTRAINT `fk_messages_users2` FOREIGN KEY (`sender`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8");



    // package_has_products
    $this->db->query("CREATE TABLE `package_has_products` (
      `id` int(11) NOT NULL AUTO_INCREMENT,
      `package_id` int(11) NOT NULL,
      `product_id` int(11) DEFAULT NULL,
      `production_id` int(11) NOT NULL,
      `title` varchar(45) DEFAULT NULL,
      `price` decimal(15,2) NOT NULL,
      `content` tinyint(1) NOT NULL DEFAULT '1',
      `digital` tinyint(1) DEFAULT NULL,
      `is_video` tinyint(1) NOT NULL DEFAULT '0',
      `quantity` int(11) NOT NULL,
      `completed` int(11) NOT NULL,
      `created` datetime DEFAULT NULL,
      `modified` datetime DEFAULT NULL,
      `active` tinyint(1) DEFAULT NULL,
      `tax` decimal(15,4) NOT NULL,
      PRIMARY KEY (`id`),
      KEY `fk_package_has_products_sale_has_products1_idx` (`package_id`),
      KEY `fk_package_has_products_products1_idx` (`product_id`),
      KEY `fk_package_has_products_productions1_idx` (`production_id`),
      CONSTRAINT `fk_package_has_products_productions1` FOREIGN KEY (`production_id`) REFERENCES `productions` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
      CONSTRAINT `fk_package_has_products_products1` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
      CONSTRAINT `fk_package_has_products_sale_has_products1` FOREIGN KEY (`package_id`) REFERENCES `sale_has_products` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8");



    // payment_types
    $this->db->query("CREATE TABLE `payment_types` (
      `id` int(11) NOT NULL AUTO_INCREMENT,
      `title` varchar(45) NOT NULL,
      `alias` varchar(45) DEFAULT NULL,
      `icon` varchar(255) DEFAULT NULL,
      `color` varchar(45) DEFAULT NULL,
      `created` datetime DEFAULT NULL,
      `modified` datetime DEFAULT NULL,
      `active` tinyint(1) NOT NULL DEFAULT '0',
      PRIMARY KEY (`id`)
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8");



    // permissions
    $this->db->query("CREATE TABLE `permissions` (
      `id` int(11) NOT NULL AUTO_INCREMENT,
      `action` varchar(100) NOT NULL,
      `description` text,
      `created` datetime DEFAULT NULL,
      `modified` datetime DEFAULT NULL,
      `active` tinyint(1) NOT NULL DEFAULT '0',
      PRIMARY KEY (`id`)
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8");


    
    // points
    $this->db->query("CREATE TABLE `points` (
      `id` int(11) NOT NULL AUTO_INCREMENT,
      `location_id` int(11) DEFAULT NULL,
      `floor_id` int(11) DEFAULT NULL,
      `zone_id` int(11) DEFAULT NULL,
      `sale_id` int(11) DEFAULT NULL,
      `title` varchar(45) NOT NULL,
      `desc` text,
      `planner_settings` text COMMENT 'masa düzeni düzenleyicisindeki ayarları',
      `status` tinyint(1) NOT NULL COMMENT '1: dolu, 0: boş',
      `opened_at` datetime DEFAULT NULL,
      `service_type` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1=table, 2=package, 3=self',
      `created` datetime DEFAULT NULL,
      `modified` datetime DEFAULT NULL,
      `active` tinyint(1) NOT NULL DEFAULT '0',
      PRIMARY KEY (`id`),
      KEY `fk_cameras_locations1_idx` (`location_id`),
      KEY `fk_points_zones1_idx` (`zone_id`),
      KEY `fk_points_floors1_idx` (`floor_id`),
      KEY `fk_points_sales1_idx` (`sale_id`),
      CONSTRAINT `fk_cameras_locations1` FOREIGN KEY (`location_id`) REFERENCES `locations` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
      CONSTRAINT `fk_points_floors1` FOREIGN KEY (`floor_id`) REFERENCES `floors` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
      CONSTRAINT `fk_points_sales1` FOREIGN KEY (`sale_id`) REFERENCES `sales` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
      CONSTRAINT `fk_points_zones1` FOREIGN KEY (`zone_id`) REFERENCES `zones` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8");


    // pos
    $this->db->query("CREATE TABLE `pos` (
      `id` int(11) NOT NULL AUTO_INCREMENT,
      `location_id` int(11) DEFAULT NULL,
      `title` varchar(45) NOT NULL,
      `desc` text,
      `amount` decimal(15,2) NOT NULL,
      `created` datetime DEFAULT NULL,
      `modified` datetime DEFAULT NULL,
      `active` tinyint(1) NOT NULL,
      PRIMARY KEY (`id`),
      KEY `fk_pos_points_locations1_idx` (`location_id`),
      CONSTRAINT `fk_pos_points_locations1` FOREIGN KEY (`location_id`) REFERENCES `locations` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8");



    // product_has_stocks
    $this->db->query("CREATE TABLE `product_has_stocks` (
      `product_id` int(11) NOT NULL,
      `stock_id` int(11) NOT NULL,
      `quantity` decimal(15,2) DEFAULT NULL,
      `optional` tinyint(1) NOT NULL,
      PRIMARY KEY (`product_id`,`stock_id`),
      KEY `fk_products_stocks_stocks1_idx` (`stock_id`),
      KEY `fk_products_stocks_products1_idx` (`product_id`),
      CONSTRAINT `fk_products_stocks_products1` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
      CONSTRAINT `fk_products_stocks_stocks1` FOREIGN KEY (`stock_id`) REFERENCES `stocks` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8");



    // product_types
    $this->db->query("CREATE TABLE `product_types` (
      `id` int(11) NOT NULL AUTO_INCREMENT,
      `parent_id` int(11) DEFAULT NULL COMMENT 'parent product type id',
      `title` varchar(45) NOT NULL,
      `desc` text,
      `sort` int(11) NOT NULL,
      `created` datetime DEFAULT NULL,
      `modified` datetime DEFAULT NULL,
      `active` tinyint(1) NOT NULL,
      PRIMARY KEY (`id`),
      KEY `fk_product_types_product_types1_idx` (`parent_id`),
      CONSTRAINT `fk_product_types_product_types1` FOREIGN KEY (`parent_id`) REFERENCES `product_types` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8");



    // productions
    $this->db->query("CREATE TABLE `productions` (
      `id` int(11) NOT NULL AUTO_INCREMENT,
      `title` varchar(45) NOT NULL,
      `description` text,
      `created` datetime DEFAULT NULL,
      `modified` datetime DEFAULT NULL,
      `active` tinyint(1) NOT NULL,
      PRIMARY KEY (`id`)
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8");



    // products
    $this->db->query("CREATE TABLE `products` (
      `id` int(11) NOT NULL AUTO_INCREMENT,
      `production_id` int(11) NOT NULL,
      `portion_id` int(11) DEFAULT NULL,
      `title` varchar(45) NOT NULL,
      `desc` text,
      `type` enum('package','product','ves') NOT NULL COMMENT 'ves: özel indirimli paket ürünüdür.',
      `digital` tinyint(1) NOT NULL DEFAULT '0',
      `is_video` tinyint(1) NOT NULL DEFAULT '0',
      `content` tinyint(1) NOT NULL DEFAULT '1' COMMENT 'ürün içeriğindeki sayı',
      `color` varchar(15) DEFAULT NULL,
      `image` varchar(255) DEFAULT NULL,
      `sort` int(11) NOT NULL DEFAULT '0',
      `created` datetime DEFAULT NULL,
      `modified` datetime DEFAULT NULL,
      `active` tinyint(1) NOT NULL DEFAULT '0',
      `location_id` int(11) NOT NULL,
      `price` decimal(15,2) DEFAULT NULL,
      `tax_inclusive` tinyint(1) DEFAULT NULL,
      `master` tinyint(1) DEFAULT NULL,
      `product_type_id` int(11) DEFAULT NULL,
      `bar_code` varchar(50) DEFAULT NULL,
      `tax` decimal(15,4) NOT NULL,
      `bonus` decimal(15,2) NOT NULL,
      `bonus_type` varchar(10) NOT NULL DEFAULT 'value' COMMENT 'value or percent',
      PRIMARY KEY (`id`),
      KEY `fk_products_locations1_idx` (`location_id`),
      KEY `fk_products_product_types1_idx` (`product_type_id`),
      KEY `fk_products_productions1_idx` (`production_id`),
      KEY `fk_products_products1_idx` (`portion_id`),
      CONSTRAINT `fk_products_locations1` FOREIGN KEY (`location_id`) REFERENCES `locations` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
      CONSTRAINT `fk_products_product_types1` FOREIGN KEY (`product_type_id`) REFERENCES `product_types` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
      CONSTRAINT `fk_products_productions1` FOREIGN KEY (`production_id`) REFERENCES `productions` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
      CONSTRAINT `fk_products_products1` FOREIGN KEY (`portion_id`) REFERENCES `products` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8");



    // products_additional_products
    $this->db->query("CREATE TABLE `products_additional_products` (
      `product_id` int(11) NOT NULL,
      `additional_product_id` int(11) NOT NULL,
      PRIMARY KEY (`product_id`,`additional_product_id`),
      KEY `fk_products_products_products2_idx` (`additional_product_id`),
      KEY `fk_products_products_products1_idx` (`product_id`),
      CONSTRAINT `fk_products_products_products1` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
      CONSTRAINT `fk_products_products_products2` FOREIGN KEY (`additional_product_id`) REFERENCES `products` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8");



    // products_has_products
    $this->db->query("CREATE TABLE `products_has_products` (
      `package_id` int(11) NOT NULL,
      `product_id` int(11) NOT NULL,
      KEY `fk_products_has_products_products2_idx` (`product_id`),
      KEY `fk_products_has_products_products1_idx` (`package_id`),
      CONSTRAINT `fk_products_has_products_products1` FOREIGN KEY (`package_id`) REFERENCES `products` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
      CONSTRAINT `fk_products_has_products_products2` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8");



    // role_has_permissions
    $this->db->query("CREATE TABLE `role_has_permissions` (
      `role_id` int(11) NOT NULL,
      `permission_id` int(11) NOT NULL,
      KEY `fk_roles_has_permissions_permissions1_idx` (`permission_id`),
      KEY `fk_roles_has_permissions_roles1_idx` (`role_id`),
      CONSTRAINT `fk_roles_has_permissions_permissions1` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
      CONSTRAINT `fk_roles_has_permissions_roles1` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8");



    // roles
    $this->db->query("CREATE TABLE `roles` (
      `id` int(11) NOT NULL AUTO_INCREMENT,
      `title` varchar(45) NOT NULL,
      `alias` varchar(45) DEFAULT NULL,
      `editable` tinyint(1) NOT NULL,
      `created` datetime DEFAULT NULL,
      `modified` datetime DEFAULT NULL,
      `active` tinyint(1) NOT NULL DEFAULT '0',
      PRIMARY KEY (`id`)
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8");



    // sale_has_products
    $this->db->query("CREATE TABLE `sale_has_products` (
      `id` int(11) NOT NULL AUTO_INCREMENT,
      `user_id` int(11) NOT NULL,
      `sale_id` int(11) NOT NULL,
      `product_id` int(11) NOT NULL,
      `additional_sale_product_id` int(11) DEFAULT NULL,
      `production_id` int(11) NOT NULL,
      `title` varchar(45) DEFAULT NULL,
      `price` decimal(15,2) DEFAULT NULL COMMENT 'vergisiz fiyatı',
      `content` tinyint(1) NOT NULL DEFAULT '1' COMMENT 'ürün içeriğindeki sayı',
      `digital` tinyint(1) DEFAULT NULL COMMENT 'ürün usb mi?',
      `is_video` tinyint(1) NOT NULL DEFAULT '0',
      `type` enum('package','product','ves') DEFAULT NULL COMMENT 'ves:özel indirimli paket ürünüdür.',
      `quantity` int(11) NOT NULL DEFAULT '1',
      `return_quantity` int(11) NOT NULL COMMENT 'iade edilen adet',
      `completed` int(11) NOT NULL COMMENT 'tamamlanan sipariş(quantity) adedi',
      `delivered` int(11) NOT NULL,
      `seen` tinyint(1) NOT NULL COMMENT 'garson siparişi gördüğünde (delivered den dolayı ihtiyaç kalmadı)',
      `paid_cash` int(11) NOT NULL COMMENT 'ara ödeme adedi',
      `paid_credit` int(11) NOT NULL COMMENT 'ara ödeme adedi',
      `paid_ticket` int(11) NOT NULL COMMENT 'ara ödeme adedi',
      `paid_sodexo` int(11) NOT NULL,
      `paid_multinet` int(11) NOT NULL,
      `paid_setcart` int(11) NOT NULL,
      `paid_cari` int(11) NOT NULL,
      `created` datetime DEFAULT NULL,
      `modified` datetime DEFAULT NULL,
      `active` tinyint(1) NOT NULL DEFAULT '1',
      `tax` decimal(15,4) NOT NULL,
      `bonus` decimal(15,2) NOT NULL,
      `bonus_type` varchar(10) NOT NULL DEFAULT 'value' COMMENT 'value or percent',
      `is_gift` tinyint(1) NOT NULL COMMENT 'ikram',
      PRIMARY KEY (`id`),
      KEY `fk_sales_has_products_products1_idx` (`product_id`),
      KEY `fk_sales_has_products_sales1_idx` (`sale_id`),
      KEY `fk_sale_has_products_sale_has_products1_idx` (`additional_sale_product_id`),
      KEY `fk_sale_has_products_users1_idx` (`user_id`),
      KEY `fk_sale_has_products_productions1_idx` (`production_id`),
      CONSTRAINT `fk_sale_has_products_productions1` FOREIGN KEY (`production_id`) REFERENCES `productions` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
      CONSTRAINT `fk_sale_has_products_sale_has_products1` FOREIGN KEY (`additional_sale_product_id`) REFERENCES `sale_has_products` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
      CONSTRAINT `fk_sale_has_products_users1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
      CONSTRAINT `fk_sales_has_products_products1` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
      CONSTRAINT `fk_sales_has_products_sales1` FOREIGN KEY (`sale_id`) REFERENCES `sales` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8");



    // sale_packages_stocks
    $this->db->query("CREATE TABLE `sale_packages_stocks` (
      `sale_package_id` int(11) NOT NULL,
      `stock_id` int(11) NOT NULL,
      `quantity` decimal(15,2) NOT NULL,
      `in_use` tinyint(1) NOT NULL,
      `optional` tinyint(1) NOT NULL,
      PRIMARY KEY (`sale_package_id`,`stock_id`),
      KEY `fk_sale_packages_stocks_package_has_products1_idx` (`sale_package_id`),
      KEY `fk_sale_packages_stocks_stocks1_idx` (`stock_id`),
      CONSTRAINT `fk_sale_packages_stocks_package_has_products1` FOREIGN KEY (`sale_package_id`) REFERENCES `package_has_products` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
      CONSTRAINT `fk_sale_packages_stocks_stocks1` FOREIGN KEY (`stock_id`) REFERENCES `stocks` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8");



    // sale_product_notes
    $this->db->query("CREATE TABLE `sale_product_notes` (
      `id` int(11) NOT NULL AUTO_INCREMENT,
      `sale_product_id` int(11) DEFAULT NULL,
      `note` text,
      `sound` varchar(255) DEFAULT NULL,
      `type` tinyint(1) NOT NULL COMMENT '1= text, 2= sound',
      `viewed` tinyint(1) NOT NULL,
      `created` datetime DEFAULT NULL,
      `modified` datetime DEFAULT NULL,
      `deleted` datetime DEFAULT NULL,
      PRIMARY KEY (`id`),
      KEY `fk_sale_product_notes_sale_has_products1_idx` (`sale_product_id`),
      CONSTRAINT `fk_sale_product_notes_sale_has_products1` FOREIGN KEY (`sale_product_id`) REFERENCES `sale_has_products` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8");



    // sale_products_stocks
    $this->db->query("CREATE TABLE `sale_products_stocks` (
      `sale_product_id` int(11) NOT NULL,
      `stock_id` int(11) NOT NULL,
      `quantity` decimal(15,2) NOT NULL,
      `in_use` tinyint(1) NOT NULL,
      `optional` tinyint(1) NOT NULL,
      PRIMARY KEY (`sale_product_id`,`stock_id`),
      KEY `fk_sale_products_stocks_sale_has_products1_idx` (`sale_product_id`),
      KEY `fk_sale_products_stocks_stocks1_idx` (`stock_id`),
      CONSTRAINT `fk_sale_products_stocks_sale_has_products1` FOREIGN KEY (`sale_product_id`) REFERENCES `sale_has_products` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
      CONSTRAINT `fk_sale_products_stocks_stocks1` FOREIGN KEY (`stock_id`) REFERENCES `stocks` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8");



    // sale_types ?
    $this->db->query("CREATE TABLE `sale_types` (
      `id` int(11) NOT NULL AUTO_INCREMENT,
      `title` varchar(45) NOT NULL,
      `desc` text,
      `created` datetime DEFAULT NULL,
      `modified` datetime DEFAULT NULL,
      `active` tinyint(1) NOT NULL DEFAULT '0',
      PRIMARY KEY (`id`)
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8");



    // sales
    $this->db->query("CREATE TABLE `sales` (
      `id` int(11) NOT NULL AUTO_INCREMENT,
      `location_id` int(11) NOT NULL,
      `pos_id` int(11) DEFAULT NULL,
      `floor_id` int(11) DEFAULT NULL,
      `zone_id` int(11) DEFAULT NULL,
      `point_id` int(11) DEFAULT NULL COMMENT 'masa',
      `sale_type_id` int(11) NOT NULL,
      `payment_type_id` int(11) DEFAULT NULL,
      `payment_date` datetime DEFAULT NULL,
      `user_id` int(11) NOT NULL,
      `completer_user_id` int(11) DEFAULT NULL COMMENT 'hesabı alan kullanıcı',
      `customer_id` int(11) DEFAULT NULL,
      `courier_id` int(11) DEFAULT NULL,
      `invoice_id` int(11) DEFAULT NULL,
      `invoice_date` datetime DEFAULT NULL,
      `ticket_number` varchar(45) DEFAULT NULL,
      `amount` decimal(15,2) DEFAULT NULL COMMENT 'toplam tutar',
      `subtotal` decimal(15,2) DEFAULT NULL COMMENT 'aratoplam= vergisiz ve indirimsiz hali',
      `taxtotal` decimal(15,2) NOT NULL,
      `change` decimal(15,2) DEFAULT NULL COMMENT 'ödenen para üstü',
      `gift_amount` decimal(15,2) NOT NULL,
      `interim_payment_amount` decimal(15,2) NOT NULL,
      `cash` decimal(15,2) DEFAULT NULL COMMENT 'ödenen nakit miktarı',
      `credit` decimal(15,2) DEFAULT NULL COMMENT 'ödenen k kart''ı miktarı',
      `ticket` decimal(15,2) DEFAULT NULL COMMENT 'bina kasasına ödenen miktar',
      `sodexo` decimal(15,2) DEFAULT NULL COMMENT 'ödenen kupon ',
      `multinet` decimal(15,2) DEFAULT NULL,
      `setcart` decimal(15,2) DEFAULT NULL,
      `cari` decimal(15,2) DEFAULT NULL,
      `discount_amount` decimal(15,2) DEFAULT NULL,
      `discount_type` enum('amount','percent') DEFAULT NULL COMMENT 'yüzdelik veya parasal değer indirimi.',
      `pos_amount` decimal(15,2) NOT NULL,
      `title` varchar(45) NOT NULL,
      `desc` text,
      `return` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'asıl satışta iade var mı?',
      `return_id` int(11) DEFAULT NULL COMMENT 'iadenin ait olduğu asıl satışın id si.',
      `status` enum('pending','cancelled','completed','shipped') DEFAULT NULL,
      `service_type` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1:table, 2:package, 3:self (check points table)',
      `call_account` tinyint(1) NOT NULL,
      `created` datetime DEFAULT NULL,
      `modified` datetime DEFAULT NULL,
      `active` tinyint(1) NOT NULL DEFAULT '0',
      PRIMARY KEY (`id`),
      KEY `fk_sales_locations1_idx` (`location_id`),
      KEY `fk_sales_payment_types1_idx` (`payment_type_id`),
      KEY `fk_sales_users1_idx` (`user_id`),
      KEY `fk_sales_sale_types1_idx` (`sale_type_id`),
      KEY `fk_sales_sales1_idx` (`return_id`),
      KEY `index_payment_date` (`payment_date`),
      KEY `fk_sales_pos1_idx` (`pos_id`),
      KEY `fk_sales_points1_idx` (`point_id`),
      KEY `fk_sales_floors1_idx` (`floor_id`),
      KEY `fk_sales_zones1_idx` (`zone_id`),
      KEY `fk_sales_users2_idx` (`completer_user_id`),
      KEY `fk_sales_customers1_idx` (`customer_id`),
      KEY `fk_sales_users3_idx` (`courier_id`),
      KEY `fk_sales_invoices1_idx` (`invoice_id`),
      CONSTRAINT `fk_sales_customers1` FOREIGN KEY (`customer_id`) REFERENCES `customers` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
      CONSTRAINT `fk_sales_floors1` FOREIGN KEY (`floor_id`) REFERENCES `floors` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
      CONSTRAINT `fk_sales_invoices1` FOREIGN KEY (`invoice_id`) REFERENCES `invoices` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
      CONSTRAINT `fk_sales_locations1` FOREIGN KEY (`location_id`) REFERENCES `locations` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
      CONSTRAINT `fk_sales_payment_types1` FOREIGN KEY (`payment_type_id`) REFERENCES `payment_types` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
      CONSTRAINT `fk_sales_points1` FOREIGN KEY (`point_id`) REFERENCES `points` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
      CONSTRAINT `fk_sales_pos1` FOREIGN KEY (`pos_id`) REFERENCES `pos` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
      CONSTRAINT `fk_sales_sale_types1` FOREIGN KEY (`sale_type_id`) REFERENCES `sale_types` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
      CONSTRAINT `fk_sales_sales1` FOREIGN KEY (`return_id`) REFERENCES `sales` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
      CONSTRAINT `fk_sales_users1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
      CONSTRAINT `fk_sales_users2` FOREIGN KEY (`completer_user_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
      CONSTRAINT `fk_sales_users3` FOREIGN KEY (`courier_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
      CONSTRAINT `fk_sales_zones1` FOREIGN KEY (`zone_id`) REFERENCES `zones` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8");

  
  
    // sales_cari
    $this->db->query("CREATE TABLE `sales_cari` (
      `id` int(11) NOT NULL AUTO_INCREMENT,
      `sale_id` int(11) NOT NULL,
      `customer_id` int(11) NOT NULL,
      `amount` decimal(15,2) NOT NULL,
      `customer_amount` decimal(15,2) NOT NULL,
      `cancelled` tinyint(1) NOT NULL,
      PRIMARY KEY (`id`),
      KEY `fk_sales_cari_sales1_idx` (`sale_id`),
      KEY `fk_sales_cari_customers1_idx` (`customer_id`),
      CONSTRAINT `fk_sales_cari_customers1` FOREIGN KEY (`customer_id`) REFERENCES `customers` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
      CONSTRAINT `fk_sales_cari_sales1` FOREIGN KEY (`sale_id`) REFERENCES `sales` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8");



    // sales_ys_orders
    $this->db->query("CREATE TABLE `sales_ys_orders` (
      `sale_id` int(11) NOT NULL,
      `ys_order_id` varchar(32) NOT NULL,
      KEY `idx_ys_order_id` (`ys_order_id`),
      KEY `fk_sales_ys_orders_sales1_idx` (`sale_id`),
      CONSTRAINT `fk_sales_ys_orders_sales1` FOREIGN KEY (`sale_id`) REFERENCES `sales` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8");



    // sessions
    $this->db->query("CREATE TABLE `sessions` (
      `id` int(11) NOT NULL AUTO_INCREMENT,
      `location_id` int(11) NOT NULL,
      `user_id` int(11) NOT NULL,
      `manager_id` int(11) DEFAULT NULL COMMENT '    kasayı teslim alan yönetici',
      `status` tinyint(1) DEFAULT NULL COMMENT '1=approved, 0=disapproved, NULL=open',
      `cash` decimal(15,2) NOT NULL,
      `credit` decimal(15,2) NOT NULL,
      `ticket` decimal(15,2) NOT NULL,
      `sodexo` decimal(15,2) NOT NULL,
      `multinet` decimal(15,2) NOT NULL,
      `setcart` decimal(15,2) NOT NULL,
      `cari` decimal(15,2) NOT NULL,
      `cash_exists` decimal(15,2) NOT NULL COMMENT 'Eldeki Gerçek değerler (tüm _exists ler)',
      `credit_exists` decimal(15,2) NOT NULL,
      `ticket_exists` decimal(15,2) NOT NULL,
      `sodexo_exists` decimal(15,2) NOT NULL,
      `multinet_exists` decimal(15,2) NOT NULL,
      `setcart_exists` decimal(15,2) NOT NULL,
      `cari_exists` decimal(15,2) NOT NULL,
      `cash_opening` decimal(15,2) NOT NULL,
      `values` text COMMENT 'money data as json',
      `description` text,
      `auto_closed` tinyint(1) NOT NULL COMMENT 'auto closed by cron job 1: auto closed, 2: edited    ',
      `closed` datetime DEFAULT NULL,
      `created` datetime DEFAULT NULL,
      `modified` datetime DEFAULT NULL,
      `deleted` datetime DEFAULT NULL,
      PRIMARY KEY (`id`),
      KEY `fk_sessions_locations1_idx` (`location_id`),
      KEY `fk_sessions_users1_idx` (`user_id`),
      KEY `fk_sessions_users2_idx` (`manager_id`),
      CONSTRAINT `fk_sessions_locations1` FOREIGN KEY (`location_id`) REFERENCES `locations` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
      CONSTRAINT `fk_sessions_users1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
      CONSTRAINT `fk_sessions_users2` FOREIGN KEY (`manager_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8");



    // settings
    $this->db->query("CREATE TABLE `settings` (
      `id` int(11) NOT NULL AUTO_INCREMENT,
      `name` varchar(45) DEFAULT NULL,
      `company` varchar(45) DEFAULT NULL,
      `url` varchar(45) DEFAULT NULL,
      `email` varchar(45) DEFAULT NULL,
      `dtz` varchar(45) DEFAULT NULL,
      `date_long` varchar(45) DEFAULT NULL,
      `date_short` varchar(45) DEFAULT NULL,
      `screen_saver` varchar(255) DEFAULT NULL,
      `smtp_host` varchar(150) DEFAULT NULL,
      `smtp_user` varchar(150) DEFAULT NULL,
      `smtp_pass` varchar(150) DEFAULT NULL,
      `smtp_port` int(3) DEFAULT NULL,
      `is_ssl` tinyint(4) DEFAULT NULL,
      `active` tinyint(1) DEFAULT NULL,
      `virtual_keyboard` tinyint(1) NOT NULL,
      `date_expire` datetime DEFAULT NULL COMMENT 'Sistem üyelik bitiş tarihi',
      `api_token` varchar(40) DEFAULT NULL,
      PRIMARY KEY (`id`)
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8");



    // shift_schedule
    $this->db->query("CREATE TABLE `shift_schedule` (
      `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
      `user_id` int(11) NOT NULL,
      `location_id` int(11) NOT NULL,
      `start_date` datetime DEFAULT NULL,
      `end_date` datetime DEFAULT NULL,
      `created` datetime DEFAULT NULL,
      `active` tinyint(1) NOT NULL,
      PRIMARY KEY (`id`),
      KEY `fk_shift_schedule_users1_idx` (`user_id`),
      KEY `fk_shift_schedule_locations1_idx` (`location_id`),
      CONSTRAINT `fk_shift_schedule_locations1` FOREIGN KEY (`location_id`) REFERENCES `locations` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
      CONSTRAINT `fk_shift_schedule_users1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8");



    // sound_notes
    $this->db->query("CREATE TABLE `sound_notes` (
      `id` int(11) NOT NULL AUTO_INCREMENT,
      `sale_id` int(11) NOT NULL,
      `file` varchar(255) NOT NULL,
      `created` datetime DEFAULT NULL,
      `modified` datetime DEFAULT NULL,
      `is_new` tinyint(1) DEFAULT NULL COMMENT '1: new, 0 or NULL: old',
      `active` tinyint(1) NOT NULL,
      PRIMARY KEY (`id`),
      KEY `fk_sound_notes_sales1_idx` (`sale_id`),
      CONSTRAINT `fk_sound_notes_sales1` FOREIGN KEY (`sale_id`) REFERENCES `sales` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8");



    // stock_contents
    $this->db->query("CREATE TABLE `stock_contents` (
      `id` int(11) NOT NULL AUTO_INCREMENT,
      `user_id` int(11) NOT NULL,
      `stock_id` int(11) NOT NULL,
      `expense_id` int(11) DEFAULT NULL,
      `quantity` decimal(15,2) NOT NULL COMMENT 'eklenen miktar',
      `price` decimal(15,2) NOT NULL,
      `tax` decimal(15,2) NOT NULL,
      `desc` text,
      `created` datetime DEFAULT NULL,
      `modified` datetime DEFAULT NULL,
      `active` tinyint(1) NOT NULL,
      PRIMARY KEY (`id`),
      KEY `fk_stock_contents_users1_idx` (`user_id`),
      KEY `fk_stock_contents_stocks1_idx` (`stock_id`),
      KEY `fk_stock_contents_expenses1_idx` (`expense_id`),
      CONSTRAINT `fk_stock_contents_expenses1` FOREIGN KEY (`expense_id`) REFERENCES `expenses` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
      CONSTRAINT `fk_stock_contents_stocks1` FOREIGN KEY (`stock_id`) REFERENCES `stocks` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
      CONSTRAINT `fk_stock_contents_users1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8");



    // stock_counts
    $this->db->query("CREATE TABLE `stock_counts` (
      `id` int(11) NOT NULL AUTO_INCREMENT,
      `location_id` int(11) NOT NULL,
      `user_id` int(11) NOT NULL,
      `start_date` datetime DEFAULT NULL,
      `end_date` datetime DEFAULT NULL,
      `created` datetime DEFAULT NULL,
      `modified` datetime DEFAULT NULL,
      `deleted` datetime DEFAULT NULL,
      PRIMARY KEY (`id`),
      KEY `fk_stock_counts_locations1_idx` (`location_id`),
      KEY `fk_stock_counts_users1_idx` (`user_id`),
      CONSTRAINT `fk_stock_counts_locations1` FOREIGN KEY (`location_id`) REFERENCES `locations` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
      CONSTRAINT `fk_stock_counts_users1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8");



    // stocks
    $this->db->query("CREATE TABLE `stocks` (
      `id` int(11) NOT NULL AUTO_INCREMENT,
      `location_id` int(11) NOT NULL,
      `title` varchar(255) NOT NULL,
      `desc` text,
      `quantity` decimal(15,2) NOT NULL COMMENT 'güncel miktar',
      `sub_limit` decimal(15,2) NOT NULL,
      `unit` varchar(45) DEFAULT NULL,
      `created` datetime DEFAULT NULL,
      `modified` datetime DEFAULT NULL,
      `active` tinyint(1) NOT NULL,
      PRIMARY KEY (`id`),
      KEY `fk_stocks_locations1_idx` (`location_id`),
      CONSTRAINT `fk_stocks_locations1` FOREIGN KEY (`location_id`) REFERENCES `locations` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8");



    // stocks_stock_counts
    $this->db->query("CREATE TABLE `stocks_stock_counts` (
      `stock_id` int(11) NOT NULL,
      `stock_count_id` int(11) NOT NULL,
      `hand_amount` decimal(15,2) NOT NULL,
      `system_amount` decimal(15,2) NOT NULL,
      PRIMARY KEY (`stock_id`,`stock_count_id`),
      KEY `fk_stocks_stock_counts_stock_counts1_idx` (`stock_count_id`),
      CONSTRAINT `fk_stocks_stock_counts_stock_counts1` FOREIGN KEY (`stock_count_id`) REFERENCES `stock_counts` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
      CONSTRAINT `fk_stocks_stock_counts_stocks1` FOREIGN KEY (`stock_id`) REFERENCES `stocks` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8");



    // ticket_template
    $this->db->query("CREATE TABLE `ticket_template` (
      `location_id` int(11) NOT NULL,
      `logo` text,
      `header` text,
      `footer` text,
      KEY `fk_ticket_template_locations1_idx` (`location_id`),
      CONSTRAINT `fk_ticket_template_locations1` FOREIGN KEY (`location_id`) REFERENCES `locations` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8");



    // transactions
    $this->db->query("CREATE TABLE `transactions` (
      `id` int(11) NOT NULL AUTO_INCREMENT,
      `pos_id` int(11) DEFAULT NULL,
      `bank_id` int(11) DEFAULT NULL,
      `customer_id` int(11) DEFAULT NULL,
      `related_transaction_id` int(11) DEFAULT NULL,
      `type_id` int(11) NOT NULL DEFAULT '0',
      `amount` decimal(15,2) NOT NULL DEFAULT '0.00',
      `pos_amount` decimal(15,2) NOT NULL DEFAULT '0.00',
      `bank_amount` decimal(15,2) NOT NULL DEFAULT '0.00',
      `customer_amount` decimal(15,2) NOT NULL DEFAULT '0.00',
      `desc` text,
      `increment` tinyint(1) NOT NULL DEFAULT '0',
      `date` datetime DEFAULT NULL,
      `created` datetime DEFAULT NULL,
      `modified` datetime DEFAULT NULL,
      PRIMARY KEY (`id`),
      KEY `fk_transaction_transaction1_idx` (`related_transaction_id`),
      KEY `fk_transaction_customers1_idx` (`customer_id`),
      KEY `fk_transaction_banks1_idx` (`bank_id`),
      KEY `fk_transaction_pos1_idx` (`pos_id`),
      CONSTRAINT `fk_transaction_banks1` FOREIGN KEY (`bank_id`) REFERENCES `banks` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
      CONSTRAINT `fk_transaction_customers1` FOREIGN KEY (`customer_id`) REFERENCES `customers` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
      CONSTRAINT `fk_transaction_pos1` FOREIGN KEY (`pos_id`) REFERENCES `pos` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
      CONSTRAINT `fk_transaction_transaction1` FOREIGN KEY (`related_transaction_id`) REFERENCES `transactions` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8");



    // user_has_activation ?
    $this->db->query("CREATE TABLE `user_has_activation` (
      `user_id` int(11) NOT NULL,
      `password` varchar(255) NOT NULL,
      `activation_code` varchar(45) NOT NULL,
      KEY `fk_user_has_activation_users1_idx` (`user_id`),
      CONSTRAINT `fk_user_has_activation_users1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8");



    // user_has_locations
    $this->db->query("CREATE TABLE `user_has_locations` (
      `user_id` int(11) NOT NULL,
      `location_id` int(11) NOT NULL,
      KEY `fk_users_has_locations_locations1_idx` (`location_id`),
      KEY `fk_users_has_locations_users1_idx` (`user_id`),
      CONSTRAINT `fk_users_has_locations_locations1` FOREIGN KEY (`location_id`) REFERENCES `locations` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
      CONSTRAINT `fk_users_has_locations_users1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8");



    // users
    $this->db->query("CREATE TABLE `users` (
      `id` int(11) NOT NULL AUTO_INCREMENT,
      `role_id` int(11) NOT NULL,
      `username` varchar(45) NOT NULL,
      `email` varchar(45) NOT NULL,
      `password` varchar(40) DEFAULT NULL,
      `password_token` varchar(50) DEFAULT NULL COMMENT 'new password key.\nForgot password template: forgot.{value}\nChange password template: change.{value}',
      `image` varchar(150) DEFAULT NULL,
      `name` varchar(255) DEFAULT NULL COMMENT 'isim/soyisim',
      `lastip` varchar(16) DEFAULT NULL,
      `lastlogin` datetime DEFAULT NULL,
      `dtz` varchar(45) DEFAULT NULL,
      `pin` int(11) DEFAULT NULL COMMENT 'POS password',
      `created` datetime DEFAULT NULL,
      `modified` datetime DEFAULT NULL,
      `active` tinyint(1) NOT NULL DEFAULT '0',
      PRIMARY KEY (`id`),
      KEY `fk_users_roles1_idx` (`role_id`),
      CONSTRAINT `fk_users_roles1` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8");



    // users_zones
    $this->db->query("CREATE TABLE `users_zones` (
      `user_id` int(11) NOT NULL,
      `zone_id` int(11) NOT NULL,
      PRIMARY KEY (`user_id`,`zone_id`),
      KEY `fk_users_zones_zones1_idx` (`zone_id`),
      KEY `fk_users_zones_users1_idx` (`user_id`),
      CONSTRAINT `fk_users_zones_users1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
      CONSTRAINT `fk_users_zones_zones1` FOREIGN KEY (`zone_id`) REFERENCES `zones` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8");



    // ys_products
    $this->db->query("CREATE TABLE `ys_products` (
      `id` varchar(40) NOT NULL,
      `location_id` int(11) NOT NULL,
      `product_id` int(11) NOT NULL,
      `stocks_id` int(11) DEFAULT NULL,
      `portion_id` int(11) DEFAULT NULL,
      `add_product_id` int(11) DEFAULT NULL,
      KEY `fk_ys_products_locations1_idx` (`location_id`),
      KEY `fk_ys_products_products1_idx` (`product_id`),
      KEY `fk_ys_products_stocks1_idx` (`stocks_id`),
      KEY `fk_ys_products_products2_idx` (`portion_id`),
      KEY `fk_ys_products_products3_idx` (`add_product_id`),
      CONSTRAINT `fk_ys_products_locations1` FOREIGN KEY (`location_id`) REFERENCES `locations` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
      CONSTRAINT `fk_ys_products_products1` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
      CONSTRAINT `fk_ys_products_products2` FOREIGN KEY (`portion_id`) REFERENCES `products` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
      CONSTRAINT `fk_ys_products_products3` FOREIGN KEY (`add_product_id`) REFERENCES `products` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
      CONSTRAINT `fk_ys_products_stocks1` FOREIGN KEY (`stocks_id`) REFERENCES `stocks` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8");



    // zones
    $this->db->query("CREATE TABLE `zones` (
      `id` int(11) NOT NULL AUTO_INCREMENT,
      `location_id` int(11) NOT NULL,
      `floor_id` int(11) DEFAULT NULL,
      `title` varchar(45) NOT NULL,
      `desc` text,
      `background` varchar(255) DEFAULT NULL,
      `created` datetime DEFAULT NULL,
      `modified` datetime DEFAULT NULL,
      `active` tinyint(1) NOT NULL,
      PRIMARY KEY (`id`),
      KEY `fk_zones_floors1_idx` (`floor_id`),
      KEY `fk_zones_locations1_idx` (`location_id`),
      CONSTRAINT `fk_zones_floors1` FOREIGN KEY (`floor_id`) REFERENCES `floors` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
      CONSTRAINT `fk_zones_locations1` FOREIGN KEY (`location_id`) REFERENCES `locations` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8");



    $this->seed();


    $this->db->query('SET FOREIGN_KEY_CHECKS=1');

  }

  /**
   * down (drop table)
   *
   * @return void
   */
  public function down()
  {

    $this->db->query('SET FOREIGN_KEY_CHECKS=0');

    // Drop tables
    $this->dbforge->drop_table('banks', TRUE);
    $this->dbforge->drop_table('customers', TRUE);
    $this->dbforge->drop_table('endofdays', TRUE);
    $this->dbforge->drop_table('expense_attachments', TRUE);
    $this->dbforge->drop_table('expense_types', TRUE);
    $this->dbforge->drop_table('expenses', TRUE);
    $this->dbforge->drop_table('floors', TRUE);
    $this->dbforge->drop_table('inventory', TRUE);
    $this->dbforge->drop_table('inventory_histories', TRUE);
    $this->dbforge->drop_table('invoice_template', TRUE);
    $this->dbforge->drop_table('invoices', TRUE);
    $this->dbforge->drop_table('location_has_products', TRUE);
    $this->dbforge->drop_table('location_losts', TRUE);
    $this->dbforge->drop_table('locations', TRUE);
    $this->dbforge->drop_table('logs', TRUE);
    $this->dbforge->drop_table('messages', TRUE);
    $this->dbforge->drop_table('package_has_products', TRUE);
    $this->dbforge->drop_table('payment_types', TRUE);
    $this->dbforge->drop_table('permissions', TRUE);
    $this->dbforge->drop_table('points', TRUE);
    $this->dbforge->drop_table('pos', TRUE);
    $this->dbforge->drop_table('product_has_stocks', TRUE);
    $this->dbforge->drop_table('product_types', TRUE);
    $this->dbforge->drop_table('productions', TRUE);
    $this->dbforge->drop_table('products', TRUE);
    $this->dbforge->drop_table('products_additional_products', TRUE);
    $this->dbforge->drop_table('products_has_products', TRUE);
    $this->dbforge->drop_table('role_has_permissions', TRUE);
    $this->dbforge->drop_table('roles', TRUE);
    $this->dbforge->drop_table('sale_has_products', TRUE);
    $this->dbforge->drop_table('sale_packages_stocks', TRUE);
    $this->dbforge->drop_table('sale_product_notes', TRUE);
    $this->dbforge->drop_table('sale_products_stocks', TRUE);
    $this->dbforge->drop_table('sale_types', TRUE);
    $this->dbforge->drop_table('sales', TRUE);
    $this->dbforge->drop_table('sales_cari', TRUE);
    $this->dbforge->drop_table('sales_ys_orders', TRUE);
    $this->dbforge->drop_table('sessions', TRUE);
    $this->dbforge->drop_table('settings', TRUE);
    $this->dbforge->drop_table('shift_schedule', TRUE);
    $this->dbforge->drop_table('sound_notes', TRUE);
    $this->dbforge->drop_table('stock_contents', TRUE);
    $this->dbforge->drop_table('stock_counts', TRUE);
    $this->dbforge->drop_table('stocks', TRUE);
    $this->dbforge->drop_table('stocks_stock_counts', TRUE);
    $this->dbforge->drop_table('ticket_template', TRUE);
    $this->dbforge->drop_table('transactions', TRUE);
    $this->dbforge->drop_table('user_has_activation', TRUE);
    $this->dbforge->drop_table('user_has_locations', TRUE);
    $this->dbforge->drop_table('users', TRUE);
    $this->dbforge->drop_table('users_zones', TRUE);
    $this->dbforge->drop_table('ys_products', TRUE);
    $this->dbforge->drop_table('zones', TRUE);

    $this->db->query('SET FOREIGN_KEY_CHECKS=1');
  }



  private function seed()
  {
    
    // locations
    $this->db->insert('locations', [
      'id' => 1,
      'title' => 'POSMAKS',
      'desc' => '',
      'currency' => '₺',
      'prepaid_limit' => '0',
      'dtz' => 'Europe/Istanbul',
      'yemeksepeti' => null,
      'created' => '2016-08-11 00:00:00',
      'modified' => null,
      'active' => 1
    ]);


    
    // roles
    $this->db->insert_batch('roles', [
      [
        'id' => 1,
        'title' => 'Admin',
        'alias' => 'admin',
        'editable' => 0,
        'created' => '2016-08-11 00:00:00',
        'modified' => null,
        'active' => 1,
      ],
      [
        'id' => 2,
        'title' => 'Kurye',
        'alias' => 'courier',
        'editable' => 0,
        'created' => '2016-08-11 00:00:00',
        'modified' => null,
        'active' => 1,
      ],
    ]);



    // users
    $this->db->insert('users', [
      'id' => 1,
      'role_id' => 1,
      'username' => 'admin',
      'email' => 'admin@posmaks.com',
      'password' => '95d1c5b3317c423586cb932a096d0e4e761cb895',
      'password_token' => null,
      'image' => '1-YU7K-UHQF-7RIK-HC0O-1525342260_8.jpg',
      'name' => 'ADMIN',
      'lastip' => null,
      'lastlogin' => null,
      'dtz' => 'Asia/Kuwait',
      'pin' => '1234',
      'created' => '2016-08-11 00:00:00',
      'modified' => null,
      'active' => 1,
    ]);



    // payment_types
    $this->db->insert_batch('payment_types', [
      [
        'id' => 1,
        'title' => 'Nakit',
        'alias' => 'cash',
        'icon' => 'fa fa-money',
        'color' => 'btn-success',
        'created' => '2016-08-11 00:00:00',
        'modified' => null,
        'active' => 1,
      ],
      [
        'id' => 2,
        'title' => 'Kredi Kartı',
        'alias' => 'credit',
        'icon' => 'fa fa-credit-card',
        'color' => 'btn-primary',
        'created' => '2016-08-11 00:00:00',
        'modified' => null,
        'active' => 1,
      ],
      [
        'id' => 3,
        'title' => 'Ticket',
        'alias' => 'ticket',
        'icon' => 'fa fa-ticket',
        'color' => 'btn-danger',
        'created' => '2016-08-11 00:00:00',
        'modified' => null,
        'active' => 1,
      ],
      [
        'id' => 4,
        'title' => 'Sodexo',
        'alias' => 'sodexo',
        'icon' => 'fa fa-scribd',
        'color' => 'btn-light-blue',
        'created' => '2016-08-11 00:00:00',
        'modified' => null,
        'active' => 1,
      ],
      [
        'id' => 5,
        'title' => 'Multinet',
        'alias' => 'multinet',
        'icon' => 'fa fa-credit-card',
        'color' => 'btn-light-green',
        'created' => '2016-08-11 00:00:00',
        'modified' => null,
        'active' => 1,
      ],
      [
        'id' => 6,
        'title' => 'Setcart',
        'alias' => 'setcart',
        'icon' => 'fa fa-credit-card-alt',
        'color' => 'btn-primary',
        'created' => '2016-08-11 00:00:00',
        'modified' => null,
        'active' => 1,
      ],
      [
        'id' => 7,
        'title' => 'Cari',
        'alias' => 'cari',
        'icon' => 'fa fa-credit-card-alt',
        'color' => 'btn-primary',
        'created' => '2016-08-11 00:00:00',
        'modified' => null,
        'active' => 1,
      ],
    ]);



    // permissions
    $this->db->insert_batch('permissions', [
      [
        'id' => 1,
        'action' => 'locations.index',
        'description' => 'Restoranlar',
        'created' => '2016-08-11 00:00:00',
        'modified' => null,
        'active' => 1,
      ],
      [
        'id' => 2,
        'action' => 'locations.add',
        'description' => 'Restoran oluştur',
        'created' => '2016-08-11 00:00:00',
        'modified' => null,
        'active' => 1,
      ],
      [
        'id' => 3,
        'action' => 'locations.edit',
        'description' => 'Restoran düzenle',
        'created' => '2016-08-11 00:00:00',
        'modified' => null,
        'active' => 1,
      ],
      [
        'id' => 4,
        'action' => 'locations.delete',
        'description' => 'Restoran sil',
        'created' => '2016-08-11 00:00:00',
        'modified' => null,
        'active' => 1,
      ],
      [
        'id' => 5,
        'action' => 'points.index',
        'description' => 'Masalar',
        'created' => '2016-08-11 00:00:00',
        'modified' => null,
        'active' => 1,
      ],
      [
        'id' => 6,
        'action' => 'points.add',
        'description' => 'Masa oluştur',
        'created' => '2016-08-11 00:00:00',
        'modified' => null,
        'active' => 1,
      ],
      [
        'id' => 7,
        'action' => 'points.edit',
        'description' => 'Masa düzenle',
        'created' => '2016-08-11 00:00:00',
        'modified' => null,
        'active' => 1,
      ],
      [
        'id' => 8,
        'action' => 'points.delete',
        'description' => 'Masa sil',
        'created' => '2016-08-11 00:00:00',
        'modified' => null,
        'active' => 1,
      ],
      [
        'id' => 9,
        'action' => 'employes.index',
        'description' => 'Personeller',
        'created' => '2016-08-11 00:00:00',
        'modified' => null,
        'active' => 1,
      ],
      [
        'id' => 10,
        'action' => 'employes.add',
        'description' => 'Personel oluştur',
        'created' => '2016-08-11 00:00:00',
        'modified' => null,
        'active' => 1,
      ],
      [
        'id' => 11,
        'action' => 'employes.edit',
        'description' => 'Personel düzenle',
        'created' => '2016-08-11 00:00:00',
        'modified' => null,
        'active' => 1,
      ],
      [
        'id' => 12,
        'action' => 'employes.delete',
        'description' => 'Personel sil',
        'created' => '2016-08-11 00:00:00',
        'modified' => null,
        'active' => 1,
      ],
      [
        'id' => 13,
        'action' => 'customers.index',
        'description' => 'Müşteriler',
        'created' => '2016-08-11 00:00:00',
        'modified' => null,
        'active' => 1,
      ],
      [
        'id' => 14,
        'action' => 'customers.add',
        'description' => 'Müşteri oluştur',
        'created' => '2016-08-11 00:00:00',
        'modified' => null,
        'active' => 1,
      ],
      [
        'id' => 15,
        'action' => 'customers.edit',
        'description' => 'Müşteri düzenle',
        'created' => '2016-08-11 00:00:00',
        'modified' => null,
        'active' => 1,
      ],
      [
        'id' => 16,
        'action' => 'customers.delete',
        'description' => 'Müşteri sil',
        'created' => '2016-08-11 00:00:00',
        'modified' => null,
        'active' => 1,
      ],
      [
        'id' => 17,
        'action' => 'products.index',
        'description' => 'Ürünler',
        'created' => '2016-08-11 00:00:00',
        'modified' => null,
        'active' => 1,
      ],
      [
        'id' => 18,
        'action' => 'products.add',
        'description' => 'Ürün oluştur',
        'created' => '2016-08-11 00:00:00',
        'modified' => null,
        'active' => 1,
      ],
      [
        'id' => 19,
        'action' => 'products.edit',
        'description' => 'Ürün düzenle',
        'created' => '2016-08-11 00:00:00',
        'modified' => null,
        'active' => 1,
      ],
      [
        'id' => 20,
        'action' => 'products.delete',
        'description' => 'Ürün sil',
        'created' => '2016-08-11 00:00:00',
        'modified' => null,
        'active' => 1,
      ],
      [
        'id' => 21,
        'action' => 'reports.index',
        'description' => 'Raporlar Özet',
        'created' => '2016-08-11 00:00:00',
        'modified' => null,
        'active' => 1,
      ],
      [
        'id' => 22,
        'action' => 'reports.home',
        'description' => 'Raporlar Satışlar',
        'created' => '2016-08-11 00:00:00',
        'modified' => null,
        'active' => 1,
      ],
      [
        'id' => 23,
        'action' => 'reports.daily',
        'description' => 'Raporlar Kasiyerler',
        'created' => '2016-08-11 00:00:00',
        'modified' => null,
        'active' => 1,
      ],
      [
        'id' => 24,
        'action' => 'reports.monthly',
        'description' => 'Raporlar Aylık Kasa',
        'created' => '2016-08-11 00:00:00',
        'modified' => null,
        'active' => 1,
      ],
      [
        'id' => 25,
        'action' => 'reports.status',
        'description' => 'Raporlar Gelir-Gider',
        'created' => '2016-08-11 00:00:00',
        'modified' => null,
        'active' => 1,
      ],
      [
        'id' => 26,
        'action' => 'reports.stock',
        'description' => 'Raporlar Stok',
        'created' => '2016-08-11 00:00:00',
        'modified' => null,
        'active' => 1,
      ],
      [
        'id' => 27,
        'action' => 'reports.endofday',
        'description' => 'Raporlar Z-Raporu',
        'created' => '2016-08-11 00:00:00',
        'modified' => null,
        'active' => 1,
      ],
      [
        'id' => 28,
        'action' => 'reports.fullness',
        'description' => 'Raporlar Yoğunluk',
        'created' => '2016-08-11 00:00:00',
        'modified' => null,
        'active' => 1,
      ],
      [
        'id' => 29,
        'action' => 'reports.prim',
        'description' => 'Raporlar Prim Raporu',
        'created' => '2016-08-11 00:00:00',
        'modified' => null,
        'active' => 1,
      ],
      [
        'id' => 30,
        'action' => 'roles.index',
        'description' => 'Yetkiler',
        'created' => '2016-08-11 00:00:00',
        'modified' => null,
        'active' => 1,
      ],
      [
        'id' => 31,
        'action' => 'roles.add',
        'description' => 'Rol oluştur',
        'created' => '2016-08-11 00:00:00',
        'modified' => null,
        'active' => 1,
      ],
      [
        'id' => 32,
        'action' => 'roles.edit',
        'description' => 'Rol düzenle',
        'created' => '2016-08-11 00:00:00',
        'modified' => null,
        'active' => 1,
      ],
      [
        'id' => 33,
        'action' => 'roles.delete',
        'description' => 'Rol sil',
        'created' => '2016-08-11 00:00:00',
        'modified' => null,
        'active' => 1,
      ],
      [
        'id' => 34,
        'action' => 'roles.permission',
        'description' => 'Yetkileri yönet',
        'created' => '2016-08-11 00:00:00',
        'modified' => null,
        'active' => 1,
      ],
      [
        'id' => 35,
        'action' => 'configuration.index',
        'description' => 'Uygulama Ayarları',
        'created' => '2016-08-11 00:00:00',
        'modified' => null,
        'active' => 1,
      ],
      [
        'id' => 36,
        'action' => 'expenses.index',
        'description' => 'Giderler',
        'created' => '2016-08-11 00:00:00',
        'modified' => null,
        'active' => 1,
      ],
      [
        'id' => 37,
        'action' => 'expenses.add',
        'description' => 'Gider oluştur',
        'created' => '2016-08-11 00:00:00',
        'modified' => null,
        'active' => 1,
      ],
      [
        'id' => 38,
        'action' => 'expenses.edit',
        'description' => 'Gider düzenle',
        'created' => '2016-08-11 00:00:00',
        'modified' => null,
        'active' => 1,
      ],
      [
        'id' => 39,
        'action' => 'expenses.addwithstock',
        'description' => 'Gider oluştur (Faturalı)',
        'created' => '2016-08-11 00:00:00',
        'modified' => null,
        'active' => 1,
      ],
      [
        'id' => 40,
        'action' => 'expenses.editwithstock',
        'description' => 'Gider düzenle (Faturalı)',
        'created' => '2016-08-11 00:00:00',
        'modified' => null,
        'active' => 1,
      ],
      [
        'id' => 41,
        'action' => 'pointofsales.index',
        'description' => 'POS',
        'created' => '2016-08-11 00:00:00',
        'modified' => null,
        'active' => 1,
      ],
      [
        'id' => 42,
        'action' => 'pointofsales.setup',
        'description' => 'POS kasa kurulumu',
        'created' => '2016-08-11 00:00:00',
        'modified' => null,
        'active' => 1,
      ],
      [
        'id' => 43,
        'action' => 'pointofsales.return',
        'description' => 'POS ürün iade',
        'created' => '2016-08-11 00:00:00',
        'modified' => null,
        'active' => 1,
      ],
      [
        'id' => 44,
        'action' => 'pointofsales.adddiscount',
        'description' => 'POS indirim ekleme',
        'created' => '2016-08-11 00:00:00',
        'modified' => null,
        'active' => 1,
      ],
      [
        'id' => 45,
        'action' => 'pointofsales.orders',
        'description' => 'POS mutfak siparişleri',
        'created' => '2016-08-11 00:00:00',
        'modified' => null,
        'active' => 1,
      ],
      [
        'id' => 46,
        'action' => 'pointofsales.packageserviceorders',
        'description' => 'POS paket servis takibi',
        'created' => '2016-08-11 00:00:00',
        'modified' => null,
        'active' => 1,
      ],
      [
        'id' => 47,
        'action' => 'expensetypes.index',
        'description' => 'Gider tipleri',
        'created' => '2016-08-11 00:00:00',
        'modified' => null,
        'active' => 1,
      ],
      [
        'id' => 48,
        'action' => 'expensetypes.add',
        'description' => 'Gider tipi oluştur',
        'created' => '2016-08-11 00:00:00',
        'modified' => null,
        'active' => 1,
      ],
      [
        'id' => 49,
        'action' => 'expensetypes.edit',
        'description' => 'Gider tipi düzenle',
        'created' => '2016-08-11 00:00:00',
        'modified' => null,
        'active' => 1,
      ],
      [
        'id' => 50,
        'action' => 'expensetypes.delete',
        'description' => 'Gider tipi sil',
        'created' => '2016-08-11 00:00:00',
        'modified' => null,
        'active' => 1,
      ],
      [
        'id' => 51,
        'action' => 'sales.index',
        'description' => 'Satışlar',
        'created' => '2016-08-11 00:00:00',
        'modified' => null,
        'active' => 1,
      ],
      [
        'id' => 52,
        'action' => 'sales.add',
        'description' => 'Satış oluştur',
        'created' => '2016-08-11 00:00:00',
        'modified' => null,
        'active' => 1,
      ],
      [
        'id' => 53,
        'action' => 'sales.edit',
        'description' => 'Satış düzenle',
        'created' => '2016-08-11 00:00:00',
        'modified' => null,
        'active' => 1,
      ],
      [
        'id' => 54,
        'action' => 'sales.delete',
        'description' => 'Satış sil',
        'created' => '2016-08-11 00:00:00',
        'modified' => null,
        'active' => 1,
      ],
      [
        'id' => 55,
        'action' => 'sales.view',
        'description' => 'Satış faturası',
        'created' => '2016-08-11 00:00:00',
        'modified' => null,
        'active' => 1,
      ],
      [
        'id' => 56,
        'action' => 'pos.index',
        'description' => 'Kasalar',
        'created' => '2016-08-11 00:00:00',
        'modified' => null,
        'active' => 1,
      ],
      [
        'id' => 57,
        'action' => 'pos.add',
        'description' => 'Kasa oluştur',
        'created' => '2016-08-11 00:00:00',
        'modified' => null,
        'active' => 1,
      ],
      [
        'id' => 58,
        'action' => 'pos.edit',
        'description' => 'Kasa düzenle',
        'created' => '2016-08-11 00:00:00',
        'modified' => null,
        'active' => 1,
      ],
      [
        'id' => 59,
        'action' => 'pos.delete',
        'description' => 'Kasa sil',
        'created' => '2016-08-11 00:00:00',
        'modified' => null,
        'active' => 1,
      ],
      [
        'id' => 60,
        'action' => 'inventory.index',
        'description' => 'Envanterler',
        'created' => '2016-08-11 00:00:00',
        'modified' => null,
        'active' => 1,
      ],
      [
        'id' => 61,
        'action' => 'inventory.add',
        'description' => 'Envanter oluştur',
        'created' => '2016-08-11 00:00:00',
        'modified' => null,
        'active' => 1,
      ],
      [
        'id' => 62,
        'action' => 'inventory.edit',
        'description' => 'Envanter düzenle',
        'created' => '2016-08-11 00:00:00',
        'modified' => null,
        'active' => 1,
      ],
      [
        'id' => 63,
        'action' => 'inventory.delete',
        'description' => 'Envanter sil',
        'created' => '2016-08-11 00:00:00',
        'modified' => null,
        'active' => 1,
      ],
      [
        'id' => 64,
        'action' => 'inventory.history',
        'description' => 'Envanter geçmişi',
        'created' => '2016-08-11 00:00:00',
        'modified' => null,
        'active' => 1,
      ],
      [
        'id' => 65,
        'action' => 'stocks.index',
        'description' => 'Stoklar',
        'created' => '2016-08-11 00:00:00',
        'modified' => null,
        'active' => 1,
      ],
      [
        'id' => 66,
        'action' => 'stocks.add',
        'description' => 'Stok oluştur',
        'created' => '2016-08-11 00:00:00',
        'modified' => null,
        'active' => 1,
      ],
      [
        'id' => 67,
        'action' => 'stocks.edit',
        'description' => 'Stok düzenle',
        'created' => '2016-08-11 00:00:00',
        'modified' => null,
        'active' => 1,
      ],
      [
        'id' => 68,
        'action' => 'stocks.delete',
        'description' => 'Stok sil',
        'created' => '2016-08-11 00:00:00',
        'modified' => null,
        'active' => 1,
      ],
      [
        'id' => 69,
        'action' => 'stockcontents.index',
        'description' => 'Stok işlemleri',
        'created' => '2016-08-11 00:00:00',
        'modified' => null,
        'active' => 1,
      ],
      [
        'id' => 70,
        'action' => 'stockcontents.add',
        'description' => 'Stok işlemi oluştur',
        'created' => '2016-08-11 00:00:00',
        'modified' => null,
        'active' => 1,
      ],
      [
        'id' => 71,
        'action' => 'stockcontents.edit',
        'description' => 'Stok işlemi düzenle',
        'created' => '2016-08-11 00:00:00',
        'modified' => null,
        'active' => 1,
      ],
      [
        'id' => 72,
        'action' => 'stockcontents.delete',
        'description' => 'Stok işlemi sil',
        'created' => '2016-08-11 00:00:00',
        'modified' => null,
        'active' => 1,
      ],
      [
        'id' => 73,
        'action' => 'floors.index',
        'description' => 'Katlar',
        'created' => '2016-08-11 00:00:00',
        'modified' => null,
        'active' => 1,
      ],
      [
        'id' => 74,
        'action' => 'floors.add',
        'description' => 'Kat oluştur',
        'created' => '2016-08-11 00:00:00',
        'modified' => null,
        'active' => 1,
      ],
      [
        'id' => 75,
        'action' => 'floors.edit',
        'description' => 'Kat düzenle',
        'created' => '2016-08-11 00:00:00',
        'modified' => null,
        'active' => 1,
      ],
      [
        'id' => 76,
        'action' => 'floors.delete',
        'description' => 'Kat sil',
        'created' => '2016-08-11 00:00:00',
        'modified' => null,
        'active' => 1,
      ],
      [
        'id' => 77,
        'action' => 'producttypes.index',
        'description' => 'Ürün tipleri',
        'created' => '2016-08-11 00:00:00',
        'modified' => null,
        'active' => 1,
      ],
      [
        'id' => 78,
        'action' => 'producttypes.add',
        'description' => 'Ürün tipi oluştur',
        'created' => '2016-08-11 00:00:00',
        'modified' => null,
        'active' => 1,
      ],
      [
        'id' => 79,
        'action' => 'producttypes.edit',
        'description' => 'Ürün tipi düzenle',
        'created' => '2016-08-11 00:00:00',
        'modified' => null,
        'active' => 1,
      ],
      [
        'id' => 80,
        'action' => 'producttypes.delete',
        'description' => 'Ürün tipi sil',
        'created' => '2016-08-11 00:00:00',
        'modified' => null,
        'active' => 1,
      ],
      [
        'id' => 81,
        'action' => 'zones.index',
        'description' => 'Bölgeler',
        'created' => '2016-08-11 00:00:00',
        'modified' => null,
        'active' => 1,
      ],
      [
        'id' => 82,
        'action' => 'zones.add',
        'description' => 'Bölge ekle',
        'created' => '2016-08-11 00:00:00',
        'modified' => null,
        'active' => 1,
      ],
      [
        'id' => 83,
        'action' => 'zones.edit',
        'description' => 'Bölge düzenle',
        'created' => '2016-08-11 00:00:00',
        'modified' => null,
        'active' => 1,
      ],
      [
        'id' => 84,
        'action' => 'zones.delete',
        'description' => 'Bölge sil',
        'created' => '2016-08-11 00:00:00',
        'modified' => null,
        'active' => 1,
      ],
      [
        'id' => 85,
        'action' => 'shiftschedule.index',
        'description' => 'Vardiyalar',
        'created' => '2016-08-11 00:00:00',
        'modified' => null,
        'active' => 1,
      ],
      [
        'id' => 86,
        'action' => 'shiftschedule.add',
        'description' => 'Vardiya oluştur',
        'created' => '2016-08-11 00:00:00',
        'modified' => null,
        'active' => 1,
      ],
      [
        'id' => 87,
        'action' => 'shiftschedule.edit',
        'description' => 'Vardiya düzenle',
        'created' => '2016-08-11 00:00:00',
        'modified' => null,
        'active' => 1,
      ],
      [
        'id' => 88,
        'action' => 'shiftschedule.delete',
        'description' => 'Vardiya sil',
        'created' => '2016-08-11 00:00:00',
        'modified' => null,
        'active' => 1,
      ],
      [
        'id' => 89,
        'action' => 'sales.scoreboard',
        'description' => 'Günlük Performans',
        'created' => '2016-08-11 00:00:00',
        'modified' => null,
        'active' => 1,
      ],
      [
        'id' => 90,
        'action' => 'mobilpos.index',
        'description' => 'Garson Terminal',
        'created' => '2016-08-11 00:00:00',
        'modified' => null,
        'active' => 1,
      ],
      [
        'id' => 91,
        'action' => 'mobilpos.tablet',
        'description' => 'Müşteri Terminal',
        'created' => '2016-08-11 00:00:00',
        'modified' => null,
        'active' => 1,
      ],
      [
        'id' => 92,
        'action' => 'points.planner',
        'description' => 'Masa düzeni',
        'created' => '2016-08-11 00:00:00',
        'modified' => null,
        'active' => 1,
      ],
      [
        'id' => 93,
        'action' => 'pointofsales.tables',
        'description' => 'Açık Hesaplar',
        'created' => '2016-08-11 00:00:00',
        'modified' => null,
        'active' => 1,
      ],
      [
        'id' => 94,
        'action' => 'syslogs.index',
        'description' => 'Loglar',
        'created' => '2016-08-11 00:00:00',
        'modified' => null,
        'active' => 1,
      ],
      [
        'id' => 95,
        'action' => 'invoices.index',
        'description' => 'Fatura şablonları',
        'created' => '2016-08-11 00:00:00',
        'modified' => null,
        'active' => 1,
      ],
      [
        'id' => 96,
        'action' => 'invoices.add',
        'description' => 'Fatura şablonu oluştur',
        'created' => '2016-08-11 00:00:00',
        'modified' => null,
        'active' => 1,
      ],
      [
        'id' => 97,
        'action' => 'invoices.edit',
        'description' => 'Fatura şablonu düzenle',
        'created' => '2016-08-11 00:00:00',
        'modified' => null,
        'active' => 1,
      ],
      [
        'id' => 98,
        'action' => 'invoices.delete',
        'description' => 'Fatura şablonu sil',
        'created' => '2016-08-11 00:00:00',
        'modified' => null,
        'active' => 1,
      ],
      [
        'id' => 99,
        'action' => 'productions.index',
        'description' => 'Üretim yerleri',
        'created' => '2016-08-11 00:00:00',
        'modified' => null,
        'active' => 1,
      ],
      [
        'id' => 100,
        'action' => 'productions.add',
        'description' => 'Üretim yeri oluştur',
        'created' => '2016-08-11 00:00:00',
        'modified' => null,
        'active' => 1,
      ],
      [
        'id' => 101,
        'action' => 'productions.edit',
        'description' => 'Üretim yeri düzenle',
        'created' => '2016-08-11 00:00:00',
        'modified' => null,
        'active' => 1,
      ],
      [
        'id' => 102,
        'action' => 'productions.delete',
        'description' => 'Üretim yeri sil',
        'created' => '2016-08-11 00:00:00',
        'modified' => null,
        'active' => 1,
      ],
      [
        'id' => 103,
        'action' => 'pointofsales.approvesession',
        'description' => 'Kasa sayımı',
        'created' => '2016-08-11 00:00:00',
        'modified' => null,
        'active' => 1,
      ],
      [
        'id' => 104,
        'action' => 'pointofsales.packageservicebutton',
        'description' => 'POS paket servis',
        'created' => '2016-08-11 00:00:00',
        'modified' => null,
        'active' => 1,
      ],
      [
        'id' => 105,
        'action' => 'pointofsales.selfservicebutton',
        'description' => 'POS self servis',
        'created' => '2016-08-11 00:00:00',
        'modified' => null,
        'active' => 1,
      ],
      [
        'id' => 106,
        'action' => 'pointofsales.productdelete',
        'description' => 'POS ürün silme',
        'created' => '2016-08-11 00:00:00',
        'modified' => null,
        'active' => 1,
      ],
      [
        'id' => 107,
        'action' => 'pointofsales.productedit',
        'description' => 'POS ürün düzenleme',
        'created' => '2016-08-11 00:00:00',
        'modified' => null,
        'active' => 1,
      ],
      [
        'id' => 108,
        'action' => 'pointofsales.check',
        'description' => 'POS Hesap Alma',
        'created' => '2016-08-11 00:00:00',
        'modified' => null,
        'active' => 1,
      ],
      [
        'id' => 109,
        'action' => 'pointofsales.interimpayment',
        'description' => 'POS ara ödeme',
        'created' => '2016-08-11 00:00:00',
        'modified' => null,
        'active' => 1,
      ],
      [
        'id' => 110,
        'action' => 'pointofsales.tablemove',
        'description' => 'POS Masa taşıma',
        'created' => '2016-08-11 00:00:00',
        'modified' => null,
        'active' => 1,
      ],
      [
        'id' => 111,
        'action' => 'pointofsales.tablecancel',
        'description' => 'POS Masa İptal',
        'created' => '2016-08-11 00:00:00',
        'modified' => null,
        'active' => 1,
      ],
      [
        'id' => 112,
        'action' => 'pointofsales.changepos',
        'description' => 'POS Kasa değiştirme',
        'created' => '2016-08-11 00:00:00',
        'modified' => null,
        'active' => 1,
      ],
      [
        'id' => 113,
        'action' => 'pointofsales.forceproductdecrement',
        'description' => 'POS Hazırlanmış ürünü silme',
        'created' => '2016-08-11 00:00:00',
        'modified' => null,
        'active' => 1,
      ],
    ]);



    // productions
    $this->db->insert_batch('productions', [
      [
        'id' => 1,
        'title' => 'Mutfak',
        'description' => 'Mutfak',
        'created' => '2016-08-11 00:00:00',
        'modified' => null,
        'active' => 1,
      ],
      [
        'id' => 2,
        'title' => 'Bar',
        'description' => 'Bar',
        'created' => '2016-08-11 00:00:00',
        'modified' => null,
        'active' => 1,
      ],
    ]);



    // sale_types
    $this->db->insert_batch('sale_types', [
      [
        'id' => 1,
        'title' => 'Payment',
        'desc' => 'Payment',
        'created' => '2016-08-11 00:00:00',
        'modified' => null,
        'active' => 1,
      ],
      [
        'id' => 2,
        'title' => 'Sales Receipt',
        'desc' => 'Sales Receipt',
        'created' => '2016-08-11 00:00:00',
        'modified' => null,
        'active' => 1,
      ],
      [
        'id' => 3,
        'title' => 'Invoice',
        'desc' => 'Invoice',
        'created' => '2016-08-11 00:00:00',
        'modified' => null,
        'active' => 1,
      ],
      [
        'id' => 5,
        'title' => 'Estimate',
        'desc' => 'Estimate',
        'created' => '2016-08-11 00:00:00',
        'modified' => null,
        'active' => 1,
      ],
    ]);



    // settings
    $this->db->insert('settings', [
      'id' => 1,
      'name' => 'POSMAKS',
      'company' => 'POSMAKS',
      'url' =>  'http://localhost',
      'email' => 'eoruc@kodar.com.tr',
      'dtz' => 'America/New_York',
      'date_long' => 'F d, Y',
      'date_short' => 'j-m-Y',
      'screen_saver' => 'assets/uploads/screen_saver/06ebecb427cf0764067a6335b48d062b.jpg',
      'smtp_host' => null,
      'smtp_user' => null,
      'smtp_pass' => null,
      'smtp_port' => 587,
      'is_ssl' => 0,
      'active' => 1,
      'virtual_keyboard' => 1,
      'date_expire' => null,
      'api_token' => null,
    ]);

    //pos 
    $this->db->insert('pos', [
      'id' => 1,
      'location_id' => 1,
      'title' => 'Kasa 1',
      'desc' => '',
      'amount' => 0.00,
      'created' => '2016-08-11 00:00:00',
      'modified' => null,
      'active' => 1,
     
    ]);

    $this->db->insert_batch('product_types', [
      [
        'id' => 1,
        'parent_id' => null,
        'title' => 'İçecekler',
        'desc' => '',
        'sort' => 0,
        'created' => '2016-08-11 00:00:00',
        'modified' => null,
        'active' => 1,

      ],
      [
        'id' => 2,
        'parent_id' => null,
        'title' => 'Yiyecekler',
        'desc' => '',
        'sort' => 0,
        'created' => '2016-08-11 00:00:00',
        'modified' => null,
        'active' => 1,
      ],
      [
        'id' => 3,
        'parent_id' => 1,
        'title' => 'Soğuk İçecekler',
        'desc' => '',
        'sort' => 0,
        'created' => '2016-08-11 00:00:00',
        'modified' => null,
        'active' => 1,
      ],
      [
        'id' => 4,
        'parent_id' => 2,
        'title' => 'Salatalar',
        'desc' => '',
        'sort' => 0,
        'created' => '2016-08-11 00:00:00',
        'modified' => null,
        'active' => 1,
      ],
      [
        'id' => 5,
        'parent_id' => 2,
        'title' => 'Tatlılar',
        'desc' => '',
        'sort' => 0,
        'created' => '2016-08-11 00:00:00',
        'modified' => null,
        'active' => 1,
      ],
      [
        'id' => 6,
        'parent_id' => 2,
        'title' => 'Sıcak Yiyecekler',
        'desc' => '',
        'sort' => 0,
        'created' => '2016-08-11 00:00:00',
        'modified' => null,
        'active' => 1,
      ],
      [
        'id' => 7,
        'parent_id' => 2,
        'title' => 'Soğuk Yiyecekler',
        'desc' => '',
        'sort' => 0,
        'created' => '2016-08-11 00:00:00',
        'modified' => null,
        'active' => 1,
      ],
    ]);


  }

}


<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_insert_quick_note_permissions extends CI_Migration
{

    public function up()
    {
      // quick_notes permission
      $this->db->insert_batch('permissions', [
        [
          'action' => 'quicknotes.index',
          'description' => 'Hızlı Notlar',
          'created' => '2016-08-11 00:00:00',
          'active' => 1
        ],
        [
          'action' => 'quicknotes.add',
          'description' => 'Hızlı Not Oluştur',
          'created' => '2016-08-11 00:00:00',
          'active' => 1
        ],
        [
          'action' => 'quicknotes.edit',
          'description' => 'Hızlı Not Düzenle',
          'created' => '2016-08-11 00:00:00',
          'active' => 1
        ],
        [
          'action' => 'quicknotes.delete',
          'description' => 'Hızlı Not Sil',
          'created' => '2016-08-11 00:00:00',
          'active' => 1
        ]
      ]);
    }

    public function down()
    {
      $this->db->where_in('action', [
        'quicknotes.index',
        'quicknotes.add',
        'quicknotes.edit',
        'quicknotes.delete'
      ])->delete('permissions');
    }

}
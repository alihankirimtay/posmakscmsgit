<?php
/**
 * File for class YemekSepetiStructGetTopMessages
 * @package YemekSepeti
 * @subpackage Structs
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20140325-01
 * @date 2015-02-01
 */
/**
 * This class stands for YemekSepetiStructGetTopMessages originally named GetTopMessages
 * Meta informations extracted from the WSDL
 * - from schema : {@link http://messaging.yemeksepeti.com/MessagingWebService/Integration.asmx?WSDL}
 * @package YemekSepeti
 * @subpackage Structs
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20140325-01
 * @date 2015-02-01
 */
class YemekSepetiStructGetTopMessages extends YemekSepetiWsdlClass
{
    /**
     * The count
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 1
     * @var int
     */
    public $count;
    /**
     * Constructor method for GetTopMessages
     * @see parent::__construct()
     * @param int $_count
     * @return YemekSepetiStructGetTopMessages
     */
    public function __construct($_count)
    {
        parent::__construct(array('count'=>$_count),false);
    }
    /**
     * Get count value
     * @return int
     */
    public function getCount()
    {
        return $this->count;
    }
    /**
     * Set count value
     * @param int $_count the count
     * @return int
     */
    public function setCount($_count)
    {
        return ($this->count = $_count);
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see YemekSepetiWsdlClass::__set_state()
     * @uses YemekSepetiWsdlClass::__set_state()
     * @param array $_array the exported values
     * @return YemekSepetiStructGetTopMessages
     */
    public static function __set_state(array $_array,$_className = __CLASS__)
    {
        return parent::__set_state($_array,$_className);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}

<?php
/**
 * File for class YemekSepetiStructGetAllMessagesV2Response
 * @package YemekSepeti
 * @subpackage Structs
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20140325-01
 * @date 2015-02-01
 */
/**
 * This class stands for YemekSepetiStructGetAllMessagesV2Response originally named GetAllMessagesV2Response
 * Meta informations extracted from the WSDL
 * - from schema : {@link http://messaging.yemeksepeti.com/MessagingWebService/Integration.asmx?WSDL}
 * @package YemekSepeti
 * @subpackage Structs
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20140325-01
 * @date 2015-02-01
 */
class YemekSepetiStructGetAllMessagesV2Response extends YemekSepetiWsdlClass
{
    /**
     * The GetAllMessagesV2Result
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 0
     * @var string
     */
    public $GetAllMessagesV2Result;
    /**
     * Constructor method for GetAllMessagesV2Response
     * @see parent::__construct()
     * @param string $_getAllMessagesV2Result
     * @return YemekSepetiStructGetAllMessagesV2Response
     */
    public function __construct($_getAllMessagesV2Result = NULL)
    {
        parent::__construct(array('GetAllMessagesV2Result'=>$_getAllMessagesV2Result),false);
    }
    /**
     * Get GetAllMessagesV2Result value
     * @return string|null
     */
    public function getGetAllMessagesV2Result()
    {
        return $this->GetAllMessagesV2Result;
    }
    /**
     * Set GetAllMessagesV2Result value
     * @param string $_getAllMessagesV2Result the GetAllMessagesV2Result
     * @return string
     */
    public function setGetAllMessagesV2Result($_getAllMessagesV2Result)
    {
        return ($this->GetAllMessagesV2Result = $_getAllMessagesV2Result);
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see YemekSepetiWsdlClass::__set_state()
     * @uses YemekSepetiWsdlClass::__set_state()
     * @param array $_array the exported values
     * @return YemekSepetiStructGetAllMessagesV2Response
     */
    public static function __set_state(array $_array,$_className = __CLASS__)
    {
        return parent::__set_state($_array,$_className);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}

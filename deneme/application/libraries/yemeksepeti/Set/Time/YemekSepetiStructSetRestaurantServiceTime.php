<?php
/**
 * File for class YemekSepetiStructSetRestaurantServiceTime
 * @package YemekSepeti
 * @subpackage Structs
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20140325-01
 * @date 2015-02-01
 */
/**
 * This class stands for YemekSepetiStructSetRestaurantServiceTime originally named SetRestaurantServiceTime
 * Meta informations extracted from the WSDL
 * - from schema : {@link http://messaging.yemeksepeti.com/MessagingWebService/Integration.asmx?WSDL}
 * @package YemekSepeti
 * @subpackage Structs
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20140325-01
 * @date 2015-02-01
 */
class YemekSepetiStructSetRestaurantServiceTime extends YemekSepetiWsdlClass
{
    /**
     * The serviceTime
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 1
     * @var int
     */
    public $serviceTime;
    /**
     * The catalogName
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 0
     * @var string
     */
    public $catalogName;
    /**
     * The categoryName
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 0
     * @var string
     */
    public $categoryName;
    /**
     * Constructor method for SetRestaurantServiceTime
     * @see parent::__construct()
     * @param int $_serviceTime
     * @param string $_catalogName
     * @param string $_categoryName
     * @return YemekSepetiStructSetRestaurantServiceTime
     */
    public function __construct($_serviceTime,$_catalogName = NULL,$_categoryName = NULL)
    {
        parent::__construct(array('serviceTime'=>$_serviceTime,'catalogName'=>$_catalogName,'categoryName'=>$_categoryName),false);
    }
    /**
     * Get serviceTime value
     * @return int
     */
    public function getServiceTime()
    {
        return $this->serviceTime;
    }
    /**
     * Set serviceTime value
     * @param int $_serviceTime the serviceTime
     * @return int
     */
    public function setServiceTime($_serviceTime)
    {
        return ($this->serviceTime = $_serviceTime);
    }
    /**
     * Get catalogName value
     * @return string|null
     */
    public function getCatalogName()
    {
        return $this->catalogName;
    }
    /**
     * Set catalogName value
     * @param string $_catalogName the catalogName
     * @return string
     */
    public function setCatalogName($_catalogName)
    {
        return ($this->catalogName = $_catalogName);
    }
    /**
     * Get categoryName value
     * @return string|null
     */
    public function getCategoryName()
    {
        return $this->categoryName;
    }
    /**
     * Set categoryName value
     * @param string $_categoryName the categoryName
     * @return string
     */
    public function setCategoryName($_categoryName)
    {
        return ($this->categoryName = $_categoryName);
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see YemekSepetiWsdlClass::__set_state()
     * @uses YemekSepetiWsdlClass::__set_state()
     * @param array $_array the exported values
     * @return YemekSepetiStructSetRestaurantServiceTime
     */
    public static function __set_state(array $_array,$_className = __CLASS__)
    {
        return parent::__set_state($_array,$_className);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}

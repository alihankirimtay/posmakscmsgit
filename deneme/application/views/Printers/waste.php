    
<div class="content">

    <div class="page-header full-content">
        <div class="row">
            <div class="col-sm-6">
                <h1>Edit Location Waste <small></small></h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb">
                    <li><a href="<?= base_url(); ?>" ><i class="ion-home"></i></a></li>
                    <li><a href="<?= base_url('printers/index/'.$current_location) ?>">Printers</a></li>
                    <li><a href="<?= base_url('printers/index/'.$current_location) ?>"><?= @$this->user->locations_array[$current_location]['title'] ?></a></li>
                    <li><a href="#" class="active">Edit Location Waste</a></li>
                </ol>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="panel">

                <div class="panel-heading">
                    <div class="panel-title">
                        <h4>Location Waste Properties</h4>
                    </div>
                </div>

                <div class="panel-body">
                    <form action="#" class="form-horizontal">
                        <div class="form-content">

                            <div class="form-group">
                                <label class="control-label col-md-3">Date</label>
                                <div class="col-md-9">
                                    <div class="inputer">
                                        <div class="input-wrapper">
                                            <input type="text" name="date" class="form-control datetimepicker-basic" value="<?= dateConvert($waste->date, 'client', dateFormat()) ?>">
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-sm-3" title="Pictures printed but not sold.">Unsold</label>
                                <div class="col-sm-9">
                                    <div class="inputer">
                                        <div class="input-wrapper">
                                            <input type="number" placeholder="Pictures printed but not sold." name="unsold" min="0" class="form-control" value="<?= (int) @$waste->unsold ?>">
                                        </div>
                                    </div>
                                </div>
                            </div>

                             <div class="form-group">
                                <label class="control-label col-sm-3" title="Pictures printed for test.">Waste</label>
                                <div class="col-sm-9">
                                    <div class="inputer">
                                        <div class="input-wrapper">
                                            <input type="number" placeholder="Pictures printed for test." name="waste" min="0" class="form-control" value="<?= (int) @$waste->waste ?>">
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-sm-3" title="Pictures given for free.">Comps</label>
                                <div class="col-sm-9">
                                    <div class="inputer">
                                        <div class="input-wrapper">
                                            <input type="number" placeholder="Pictures given for free." name="comps" min="0" class="form-control" value="<?= (int) @$waste->comps ?>">
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-sm-3" title="Pictures given for free.">Unseen</label>
                                <div class="col-sm-9">
                                    <div class="inputer">
                                        <div class="input-wrapper">
                                            <input type="number" placeholder="Pictures unseen." name="unseen" min="0" class="form-control" value="<?= (int) @$waste->unseen ?>">
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-buttons-fixed">
                                <a type="button" href="<?= base_url('printers/index/'.$current_location); ?>" class="btn btn-grey">Cancel</a>
                                <div class="btn-group pull-right">
                                    <button type="submit" class="btn btn-success" name="dosubmit" data-url="<?= base_url('printers/waste/'.$current_location.'/'.$waste->id);?>" >Save</button>
                                    <button type="submit" class="btn btn-blue" name="dosubmit" data-url="<?= base_url('printers/waste/'.$current_location.'/'.$waste->id);?>" data-return="<?= base_url('printers/index/'.$current_location); ?>" >Save & Exit</button>
                                </div>
                            </div>

                        </div>
                    </form>
                </div>

            </div>
        </div>
    </div>

</div>
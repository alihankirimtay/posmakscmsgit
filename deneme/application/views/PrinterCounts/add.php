    
<div class="content">

    <div class="page-header full-content">
        <div class="row">
            <div class="col-sm-6">
                <h1>New Printer Count <small></small></h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb">
                    <li><a href="<?= base_url(); ?>" ><i class="ion-home"></i></a></li>
                    <li><a href="<?= base_url('printers/index/'.$current_location) ?>">Printers</a></li>
                    <li><a href="<?= base_url('printers/index/'.$current_location) ?>"><?= @$this->user->locations_array[$current_location]['title'] ?></a></li>
                    <li><a href="<?= base_url('printercounts/index/'.$current_printer) ?>"><?= $printer->title ?></a></li>
                    <li><a href="#" class="active">New Printer Count</a></li>
                </ol>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="panel">

                <div class="panel-heading">
                    <div class="panel-title">
                        <h4>Printer Count Properties</h4>
                    </div>
                </div>

                <div class="panel-body">
                    <form action="#" class="form-horizontal">
                        <div class="form-content">

                            <div class="form-group">
                                <label class="control-label col-md-3">Current Counter</label>
                                <div class="col-md-9">
                                    <div class="inputer">
                                        <div class="input-wrapper">
                                            <input type="number" name="counter" class="form-control">
                                            <span class="help-block"><small></small></span>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3">Date</label>
                                <div class="col-md-9">
                                    <div class="inputer">
                                        <div class="input-wrapper">
                                            <input type="text" name="date" class="form-control datetimepicker-basic" value="<?= dateConvert(date('Y-m-d H:i:s'), 'client', dateFormat()) ?>">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                            <input type="hidden" name="printer" value="<?= $this->uri->segment(3) ?>">

                            <div class="form-buttons-fixed">
                                <a type="button" href="<?= base_url('printercounts/index/'.$current_printer); ?>" class="btn btn-grey">Cancel</a>
                                <div class="btn-group pull-right">
                                    <button type="submit" class="btn btn-success" name="dosubmit" data-url="<?= base_url('printercounts/add/'.$current_printer);?>" >Save</button>
                                    <button type="submit" class="btn btn-blue" name="dosubmit" data-url="<?= base_url('printercounts/add/'.$current_printer);?>" data-return="<?= base_url('printercounts/index/'.$current_printer); ?>" >Save & Exit</button>
                                </div>
                            </div>

                        </div>
                    </form>
                </div>

            </div>
        </div>
    </div>

</div>

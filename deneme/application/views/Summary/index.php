<div class="content">

    <div class="page-header full-content">
        <div class="row">
            <div class="col-sm-6">
                <h1><?= __('Özet') ?></h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb">
                    <li><a href="<?php ECHO base_url(); ?>" ><i class="ion-home"></i></a></li>
                    <li><a href="#" class="active"><?= __('Özet') ?></a></li>
                </ol>
            </div>
        </div>
    </div>


    <div class="row">

        <div class="panel">
            <div class="panel-body">


                <form action="" method="get" class="form-horizontal" role="form">
                    <div class="form-content">
                        
                        <div class="col-xs-4 col-sm-4">
                            <div class="form-group">
                                <label class="control-label col-md-3 col-xs-12"><?= __('Restoran:') ?> </label>
                                <div class="col-md-9 col-xs-12">
                                    <select name="location" class="form-co1ntrol chosen-select">
                                        <option value="0"><?= __('Tümü') ?></option>
                                        <?php foreach ($this->user->locations_array as $location): ?>
                                            <option value="<?= $location['id']; ?>" <?php selected(@$location_id, $location['id']) ?>>
                                                <?= $location['title'] ;?>
                                            </option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="col-xs-4 col-sm-3">
                            <div class="form-group hidden">
                                <label class="control-label col-md-3 col-xs-12"><?= __('PAX:') ?> </label>
                                <div class="col-md-9 col-xs-12">
                                    <div class="form-control"><?= $pax_today ?></div>
                                </div>
                            </div>
                        </div>

                        <div class="col-xs-4 col-sm-4">
                            <div class="form-group">
                                <label class="control-label col-md-3 col-xs-12"><?= __('Başlangıç Tarihi:') ?> </label>
                                <div class="col-md-9 col-xs-12">
                                    <div class="inputer">
                                        <div class="input-wrapper">
                                            <input type="text" name="date" class="form-control datetimepicker-basic" value="<?= $date_client_today ?>"/>
                                            <p class="help-block small"><small><?= __('Bitiş Tarihi:') ?> <?= dateReFormat($date_client_today_to, 'Y-m-d H:i:s', dateFormat()) ?></small></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-sm-1 hidden-xs">
                            <div class="form-group">
                                <div class="col-md-9 col-xs-10">
                                    <button type="submit" class="btn btn-primary"><?= __('Uygula') ?></button>
                                </div>
                            </div>
                        </div>

                    </div>
                </form>

            </div>
        </div>

        <div class="col-md-12">
            <!-- VES -->
            <div class=" col-xs-12 col-sm-6 col-md-3">
                <div class="list-group">
                    <a href="#" class="list-group-item active"><?= __('KATEGORİLER') ?></a>
                    <?php $tmp_total = 0; ?>
                    <?php foreach ($categorized_products as $key => $product): ?>
                        <div class="list-group-item">
                            <h5 class="list-group-item-heading"><?= $product[ 'title' ] ?></h5>
                            <p class="list-group-item-text"><?= $currency . number_format($product[ 'total' ], 2) ?></p>
                        </div>
                    <?php $tmp_total += $product[ 'total' ]; ?>
                    <?php endforeach; ?>
                    <div class="list-group-item">
                        <h5 class="list-group-item-heading"><?= __('Toplam Satış') ?></h5>
                        <p class="list-group-item-text"><?= $currency . number_format($tmp_total, 2) ?></p>
                    </div>
                    <div class="list-group-item hidden">
                        <h5 class="list-group-item-heading"><?= __('PPS') ?></h5>
                        <p class="list-group-item-text"><?= $currency . number_format($tmp_total / $pax_today, 2) ?></p>
                    </div>
                </div>
            </div>
            <!-- END - VES -->

            <!-- PRODUCT SALES -->
            <div class=" col-xs-12 col-sm-6 col-md-6">
                <div class="list-group">
                    <a href="#" class="list-group-item active"><?= __('SATILAN ÜRÜNLER') ?></a>
                    <div class="list-group-item">
                        <?php $tmp_total = 0; ?>
                        <?php foreach ($categorized_product_sales as $product_sale): ?>
                            <p class="list-group-item-text"><?= $product_sale[ 'title' ] ?>: <span class="pull-right"><?= $currency . number_format($product_sale[ 'total' ], 2) ?></span></p>
                        <?php $tmp_total += $product_sale[ 'total' ]; ?>
                        <?php endforeach; ?>
                    </div>
                    <div class="list-group-item">
                        <h5 class="list-group-item-heading"><?= __('Toplam Satış') ?></h5>
                        <p class="list-group-item-text"><?= $currency . number_format($tmp_total, 2) ?></p>
                    </div>
                    <div class="list-group-item hidden">
                        <h5 class="list-group-item-heading"><?= __('PPS') ?></h5>
                        <p class="list-group-item-text"><?= $currency . number_format($tmp_total / $pax_today, 2) ?></p>
                    </div>
                </div>
            </div>
            <!-- END - PRODUCT SALES -->

            <!-- ENTRANCE -->
            <div class="col-xs-12 col-sm-3 col-md-3">
                <div class="list-group">
                    <a href="#" class="list-group-item active"><?= __('KASALAR') ?></a>
                    <?php $tmp_total = 0; ?>
                    <?php foreach ($categorized_entrances as $key => $entrance): ?>
                        <div class="list-group-item">
                            <h5 class="list-group-item-heading"><?= $entrance[ 'title' ] ?></h5>
                            <p class="list-group-item-text"><?= $currency . number_format($entrance[ 'total' ], 2) ?></p>
                        </div>
                    <?php $tmp_total += $entrance[ 'total' ]; ?>
                    <?php endforeach; ?>
                    <div class="list-group-item hidden">
                        <h5 class="list-group-item-heading"><?= __('PPS') ?></h5>
                        <p class="list-group-item-text"><?= $currency . number_format($tmp_total / $pax_today, 2) ?></p>
                    </div>
                </div>
            </div>
            <!-- END - ENTRANCE -->
        </div>


        <div class="col-md-12">
            <!-- TOTAL SALES -->
            <div class="col-xs-12 col-sm-3 col-md-3">
                <div class="list-group">
                    <a href="#" class="list-group-item active"><?= __('TOPLAM SATIŞLAR') ?></a>
                    <div class="list-group-item">
                        <!-- <h5 class="list-group-item-heading">Total Overall Sales</h5> -->
                        <table class="table table-condensed">
                            <thead>
                                <tr>
                                    <th><h6><?= $date_client_today_format ?></h6>Bugün</th>
                                    <th><h6><?= $date_client_lyear_today_format ?></h6><?= __('Geçen Yıl') ?></th>
                                    <th><?= __('Fark') ?></th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td><?= $currency . number_format($sales_today, 2) ?></td>
                                    <td><?= $currency . number_format($sales_last_year_today, 2) ?></td>
                                    <td><?= $currency . number_format($sales_today - $sales_last_year_today, 2) ?></td>
                                </tr>
                            </tbody>
                            <thead>
                                <tr>
                                    <th><?= __('Bu Ay') ?></th>
                                    <th><?= __('Geçen Yıl') ?></th>
                                    <th><?= __('Fark') ?></th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td><?= $currency . number_format($sales_this_month, 2) ?></td>
                                    <td><?= $currency . number_format($sales_last_year_month, 2) ?></td>
                                    <td><?= $currency . number_format($sales_this_month - $sales_last_year_month, 2) ?></td>
                                </tr>
                            </tbody>
                            <thead>
                                <tr>
                                    <th><?= __('Bu Yıl') ?></th>
                                    <th><?= __('Geçen Yıl') ?></th>
                                    <th><?= __('Fark') ?></th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td><?= $currency . number_format($sales_this_year, 2) ?></td>
                                    <td><?= $currency . number_format($sales_last_year, 2) ?></td>
                                    <td><?= $currency . number_format($sales_this_year - $sales_last_year, 2) ?></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <!-- END - TOTAL SALES -->

            <!-- PPS -->
            <div class="col-xs-12 col-sm-3 col-md-3 hidden">
                <div class="list-group">
                    <a href="#" class="list-group-item active"><?= __('PPS') ?></a>
                    <div class="list-group-item">
                        <!-- <h5 class="list-group-item-heading">Total Overall PPS</h5> -->
                        <table class="table table-condensed">
                            <thead>
                                <tr>
                                    <th><h6><?= $date_client_today_format ?></h6>Bugün</th>
                                    <th><h6><?= $date_client_lyear_today_format ?></h6><?= __('Geçen Yıl') ?></th>
                                    <th><?= __('Fark') ?></th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td><?= $currency . number_format($sales_today / $pax_today, 2) ?></td>
                                    <td><?= $currency . number_format($sales_last_year_today / $pax_last_year_today, 2) ?></td>
                                    <td><?= $currency . number_format(($sales_today / $pax_today) - ($sales_last_year_today / $pax_last_year_today), 2) ?></td>
                                </tr>
                            </tbody>
                            <thead>
                                <tr>
                                    <th><?= __('Bu Ay') ?></th>
                                    <th><?= __('Geçen Yıl') ?></th>
                                    <th><?= __('Fark') ?></th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td><?= $currency . number_format($sales_this_month / $pax_this_month, 2) ?></td>
                                    <td><?= $currency . number_format($sales_last_year_month / $pax_last_year_month, 2) ?></td>
                                    <td><?= $currency . number_format(($sales_this_month / $pax_this_month) - ($sales_last_year_month / $pax_last_year_month), 2) ?></td>
                                </tr>
                            </tbody>
                            <thead>
                                <tr>
                                    <th><?= __('Bu Yıl') ?></th>
                                    <th><?= __('Geçen Yıl') ?></th>
                                    <th><?= __('Fark') ?></th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td><?= $currency . number_format($sales_this_year / $pax_this_year, 2) ?></td>
                                    <td><?= $currency . number_format($sales_last_year / $pax_last_year, 2) ?></td>
                                    <td><?= $currency . number_format(($sales_this_year / $pax_this_year) - ($sales_last_year / $pax_last_year), 2) ?></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <!-- END - PPS -->
        

            <!-- CAPTURE -->
            <div class="col-xs-12 col-sm-3 col-md-3 hidden">
                <div class="list-group">
                    <a href="#" class="list-group-item active"><?= __('CAPTURE %') ?></a>
                    <div class="list-group-item">
                        <h5 class="list-group-item-heading"><?= __('PAX:') ?></h5>
                        <p class="list-group-item-text"><?= $pax_today ?></p>
                    </div>
                    <div class="list-group-item">
                        <h5 class="list-group-item-heading"><?= __('NEGS:') ?></h5>
                        <p class="list-group-item-text"><?= $negs_today ?></p>
                    </div>
                    <div class="list-group-item">
                        <h5 class="list-group-item-heading"><?= __('CAPTURE %:') ?></h5>
                        <p class="list-group-item-text"><?= number_format($negs_today / $pax_today, 2) ?></p>
                    </div>
                    <div class="list-group-item">
                        <h5 class="list-group-item-heading"><?= __('People Per Pic.:') ?></h5>
                        <p class="list-group-item-text"><?= number_format($negs_today / $gallery_printed, 2) ?></p>
                    </div>
                </div>
            </div>
            <!-- END - CAPTURE -->

            <!-- SALES -->
            <div class="col-xs-12 col-sm-3 col-md-3">
                <div class="list-group">
                    <a href="#" class="list-group-item active"><?= __('SATIŞLAR %') ?></a>
                    <div class="list-group-item hidden">
                        <h5 class="list-group-item-heading">Total Gallery Printed:</h5>
                        <p class="list-group-item-text"><?= number_format($gallery_printed, 2) ?></p>
                    </div>
                    <div class="list-group-item">
                        <h5 class="list-group-item-heading"><?= __('Satış %:') ?></h5>
                        <p class="list-group-item-text"><?= number_format($master_product_sold / $gallery_printed, 2) ?></p>
                    </div>
                    <div class="list-group-item hidden">
                        <h5 class="list-group-item-heading">Unseen %:</h5>
                        <p class="list-group-item-text"><?= number_format($unseen / $gallery_printed, 2) ?></p>
                    </div>
                    <div class="list-group-item hidden">
                        <h5 class="list-group-item-heading">Persentage of sales from seen image:</h5>
                        <p class="list-group-item-text"><?= number_format($master_product_sold / ($gallery_printed - $unseen), 2) ?></p>
                    </div>
                </div>
            </div>
            <!-- END - SALES -->


            <div class="col-sm-12 text-right small">
                <i><?= 'Compiled in ' . $this->benchmark->elapsed_time() . ' sec.' ?></i>
            </div>
        </div>

    </div>

</div>
<?php 
?>
<div class="content">

    <div class="page-header full-content">
        <div class="row">
            <div class="col-sm-6">
                <h1><?= __('Stok Sayımı') ?></h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb">
                    <li><a href="<?php ECHO base_url(); ?>" ><i class="ion-home"></i></a></li>
                    <li><a href="#" class="active"><?= __('Stok Sayımı') ?></a></li>
                </ol>
            </div>
        </div>
    </div>

     <div class="row">
        <div class="col-md-12">
            <div class="panel">
                <div class="panel-heading">
                    <div class="panel-title">
                        <h4>All Points</h4>
                        <div class="btn-group pull-right">
                            <button id="stockSync" class="btn btn-success btn-xs" data-id = "<?= $id ?>"><i class="fa fa-refresh"></i> <?= __('EŞİTLE') ?></button>
                        </div>
                    </div>
                </div>
                <div class="panel-body">
                    <div class="overflow-table">
                        <table class="display datatables-basic">
                            <thead>
                                <tr>
                                    <th><?= __('Stok Adı') ?></th>
                                    <th><?= __('Girilen Stoklar') ?></th>
                                    <th><?= __('Sistem Stokları') ?></th>
                                    <th><?= __('Aradaki Fark') ?> </th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($stock_counts as $stock): ?>      
                                <?php $diff = $stock->hand_amount - $stock->system_amount; ?>
                                <?php  ?>
                                    <tr>
                                        <td><?= $stock->stock_name ?></td>
                                        <td><?= $stock->hand_amount ?> <b><?= @$this->units[$stock->unit] ?></b></td>
                                        <td><?= $stock->system_amount ?> <b><?= @$this->units[$stock->unit] ?></b></td>
                                        <?php if ($diff >= 0): ?>
                                        <td style = "color:darkgreen;"><?= $diff ?> <b><?= @$this->units[$stock->unit] ?></b></td>
                                    	<?php elseif ($diff < 0): ?>
                                    	<td style = "color:red;"><?= $diff ?> <b><?= @$this->units[$stock->unit] ?></b></td>
                                    	<?php endif ?>
                                    </tr>
                                <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                </div>

            </div>
        </div>
    </div>

</div>
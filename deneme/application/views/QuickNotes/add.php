    
<div class="content">

    <div class="page-header full-content">
        <div class="row">
            <div class="col-sm-6">
                <h1><?= __('Hızlı Not Ekle') ?> <small></small></h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb">
                    <li><a href="<?= base_url(); ?>" ><i class="ion-home"></i></a></li>
                    <li><a href="<?= base_url('quickNotes'); ?>"><?= __('Hızlı Not') ?></a></li>
                    <li><a href="#" class="active"><?= __('Hızlı Not Oluştur') ?></a></li>
                </ol>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="panel">

                <div class="panel-heading">
                    <div class="panel-title">
                        <h4><?= __('Hızlı Not Detayları') ?></h4>
                    </div>
                </div>

                <div class="panel-body">
                    <form action="#" class="form-horizontal">
                        <div class="form-content">
                            <div class="form-group">
                                <label class="control-label col-md-3"><?= __('Not') ?></label>
                                <div class="col-md-9">
                                    <div class="inputer">
                                        <div class="input-wrapper">
                                            <input type="text" name="title" class="form-control">
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3"><?= __('Durum') ?></label>
                                <div class="col-md-9">
                                    <div class="radioer form-inline">
                                        <input type="radio" name="active" id="active1" checked="checked" value="1" >
                                        <label for="active1"><?= __('Aktif') ?></label>
                                    </div>
                                    <div class="radioer form-inline">
                                        <input type="radio" name="active" id="active0" value="0" >
                                        <label for="active0"><?= __('Pasif') ?></label>
                                    </div>
                                </div>
                            </div>

                            <div class="form-buttons-fixed">
                                <a type="button" href="<?= base_url('quicknotes'); ?>" class="btn btn-grey"><?= __('İptal') ?></a>
                                <div class="btn-group pull-right">
                                    <button type="submit" class="btn btn-success" name="dosubmit" data-url="<?= base_url('quicknotes/add');?>" ><?= __('Kaydet') ?></button>
                                    <button id="kaydetVeCik" type="submit" class="btn btn-blue" name="dosubmit" data-url="<?= base_url('quicknotes/add');?>" data-return="<?= base_url('quicknotes'); ?>" ><?= __('Kaydet & Çık') ?></button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>

            </div>
        </div>
    </div>

</div>
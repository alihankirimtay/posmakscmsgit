<?php $last_table = 0; ?>
<?php foreach ($orders as $order): ?>

    <?php if ($text_note = trim(@$sales[$order->sale_id]['desc'])): ?>
        <?php $text_note = '<a class="btn btn-sm btn-success modal-show_note_button" data-toggle="modal" href="#" data-note="'.$text_note.'"><i class="fa fa-pencil-square-o"></i></a>'; ?>
    <?php endif; ?>
    
    <?php if ($function_name == 'orders'): ?>
        <?php if (!$last_table): ?>
            <tr class="group">
                <td colspan="6"><?= @$sales[$order->sale_id]['table_name'] ?> <?= $text_note ?></td>
                <td>
                    <?php if (isset($sounds[$order->sale_id])): ?>
                        <button class="btn btn-primary btn-lg modal-sounds" data-id="<?php ECHO $order->sale_id ?>"> <i class="fa fa-volume-up"></i> </button>
                    <?php endif; ?>
                </td>
            </tr>
        <?php elseif ($last_table != $order->sale_id): ?>
            <tr class="group">
                <td colspan="6"><?= @$sales[$order->sale_id]['table_name'] ?> <?= $text_note ?></td>
                <td>
                    <?php if (isset($sounds[$order->sale_id])): ?>
                        <button class="btn btn-primary btn-lg modal-sounds" data-id="<?php ECHO $order->sale_id ?>"> <i class="fa fa-volume-up"></i> </button>
                    <?php endif; ?>
                </td>
            </tr>
        <?php endif; ?>
        <?php $last_table = $order->sale_id; ?>
    <?php endif; ?>
    
    <tr>
        <td><?= $order->id ?></td>
        <td><?= $order->title ?></td>
        <td><?= ($function_name == 'getCompletedOrders') ? $order->quantity : $order->quantity - $order->completed ?></td>
        <td><?= @$sales[$order->sale_id]['table_name'] ?></td>
        <td><?= @$sales[$order->sale_id]['employe'] ?></td>
        <td><?= dateConvert($order->created, 'client', dateFormat()) ?></td>
        <td>
            <?php if ($function_name == 'getCompletedOrders'): ?>
                <button class="btn btn-success btn-lg doseenorder" data-id="<?php ECHO $order->id ?>"> <i class="fa fa-thumbs-up"></i> </button>
            <?php else: ?>
                <button class="btn btn-success btn-lg docompleteorder" data-id="<?php ECHO $order->id ?>" data-quantity="<?php ECHO $order->quantity - $order->completed ?>"> <i class="fa fa-thumbs-up"></i> </button>
            <?php endif; ?>
        </td>
    </tr>
<?php endforeach; ?>
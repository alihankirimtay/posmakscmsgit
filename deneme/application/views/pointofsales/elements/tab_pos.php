<form id="pos">

    <input type="hidden" name="point_id" value="">
    <input type="hidden" name="sale_id" value="">
    <input type="hidden" name="customer_id" value="">
    <input type="hidden" name="discount_key" value="">
    
    <div class="left-pos-buttons">
        <ul class="list-group">
            <li class="list-group-item"><button id="pos-create-order" class="btn btn-success btn-blue btn-ripple btn-xxl"><i class="fa fa-shopping-basket fa-2x"></i><br><?= __("SİPARİŞ"); ?></button></li>                      
            <li class="list-group-item"><button id="pos-complete" class="btn btn-success btn-ripple btn-xxl"><i class="fa fa-check fa-2x"></i><br><?= __("HESAP AL"); ?></button></li>                     
            <li class="list-group-item hidden"><button id="pos-delete-order" class="btn btn-success btn-red btn-ripple btn-xxl"><i class="fa fa-ban fa-2x"></i> <?= __("İPTAL ET"); ?></button></li>                     
            <li class="list-group-item"><button id="interim-payments" class="btn btn-red btn-ripple btn-xxl openTab" data-tab="interimpayments"><i class="fa fa-hand-scissors-o fa-2x"></i><br><?= __("ARA"); ?><br> <?= __("ÖDEME"); ?></button></li>
            <li class="list-group-item" id="package_service_button_container"><button class="btn btn-primary btn-md openTab" data-tab="packageserviceorders"><i class="fa fa-truck fa-2x"></i><br><?= __("PAKET S."); ?><br><?= __("TAKİP"); ?></button></li>
            <li class="list-group-item"><button id="handle-adisyon" type="button" class="btn btn-purple btn-xxl"><i class="fa fa-file-text-o fa-2x" aria-hidden="true"></i><br><?= __("ADİSYON"); ?></button></li>

            <li class="list-group-item"><button id="move-table" class="btn btn-blue-grey btn-ripple btn-xxl openTab" data-tab="movetable"><i class="fa fa-arrows fa-2x"></i><br><?= __("MASA"); ?><br><?= __("TAŞI"); ?></button></li>
            <li class="list-group-item"><button type="button" class="btn btn-orange btn-ripple btn-xxl" data-toggle="modal" data-target="#modal-case"><i class="fa fa-television fa-2x"></i><br><?= __("KASA"); ?></button></li>
        </ul>  
    </div>
    

    <!-- SUMMARY -->
    <div class="col-sm-5">
        <div class="panel m-b-0 br-0">

            <div class="panel-body" id="pos-list" style="height: 270px;  overflow-y: auto; padding: 0;">
                <!-- TICKET -->
                <!-- <input type="text" name="ticket" class="form-control text-center" placeholder="Ticket Number" autocomplete="off" autofocus="true"> -->
                <!-- END - TICKET -->
                

                <div class="table">
                    <table class="table table-condensed">
                        <thead>
                            <tr>
                                <th class="small col-sm-1"></th>
                                <th class="small col-sm-2"><?= __("Adet"); ?></th>
                                <th class="small col-sm-4"><?= __("Ürün"); ?></th>
                                <th class="small col-sm-2 text-right"><?= __("Fiyat"); ?></th>
                                <th class="small col-sm-2 text-right"><?= __("Toplam"); ?></th>
                                <th class="small col-sm-1"></th>
                            </tr>
                        </thead>
                        <tbody id="pos-basket">

                        </tbody>
                    </table>
                </div>
            </div>

            <div class="panel-body p-a-0" id="pos-summary">
                <div class="col-sm-12">
                    <ul class="list-group m-b-0">
                        <li class="list-group-item text-right small p-y-0 hidden">
                            <span class="pull-left"><?= __("Aratoplam:"); ?></span> <strong id="subtotal1" class="small"></strong>
                        </li>
                        <li class="list-group-item text-right small p-y-0 hidden">
                            <span class="pull-left"><?= __("Toplam Vergi:"); ?></span> <strong id="tax-total1" class="small"></strong>
                        </li>
                        <li class="list-group-item text-right small p-y-0 hidden ">
                            <span class="pull-left"><?= __("İndirim:"); ?></span>
                            <a href="#" id="add-discount" class="btn btn-green btn-xs"><i class="fa fa-arrow-down"></i></a>
                            <strong id="discount-total1" class="small">0.00</strong>
                        </li>
                        <li class="list-group-item text-center p-y-0 hidden">
                            <h3 class="m-b-0"><strong id="total1"></strong></h3>
                        </li>
                        <li class="list-group-item p-a-0">
                            <div class="btn-group btn-group-justified">
                                <a id="payment-cari" class="btn btn-danger btn-ripple btn-xxl"data-toggle="modal"><i class="fa fa-money" ></i> <?= __("CARİ HESAP"); ?></a>                            
                                <a href="#modal-other_payments" class="btn btn-cyan btn-ripple btn-xxl" data-toggle="modal"><i class="fa fa-credit-card-alt"></i> <?= __("TICKET"); ?></a>
                                <a id="add-credit" class="btn btn-primary btn-ripple btn-xxl"><i class="fa fa-credit-card"></i> <?= __("K. KARTI"); ?></a>
                                <a id="add-cash" class="btn btn-light-green btn-ripple btn-xxl"><i class="fa fa-money"></i> <?= __("NAKİT"); ?></a>
                            </div>
                        </li>
                        <li class="list-group-item p-a-0 paid-group">
                            <div class="" style="display:-webkit-box; margin-top: 10px;">
                               <p class="col-sm-3 text-center">Ara Ödeme</p>
                               <p class="col-sm-3 text-center">Ödenen</p>
                               <p class="col-sm-3 text-center">Para Üstü</p>
                               <p class="col-sm-3 text-center">Kalan</p>
                            </div>
                            <div class="text-center">
                               <strong id="pos-interim_paid" class="col-sm-3"></strong>
                               <strong id="pos-paid" class="col-sm-3"></strong>
                               <strong id="pos-change" class="col-sm-3"></strong>
                               <strong id="pos-rest" class="col-sm-3" style="color: #8bda34;"></strong>
                            </div>
                        </li>

                        
                        
                        <li class="list-group-item clearfix p-a-0" style="height:80px !important; background: #C2BFBF">
                            
                            <input name="payments[cash][]" type="hidden" class="hidden" value="0">
                            

                            <div id="payments-in-credit" class="col-sm-3 col-sm-offset-6">
                            </div>


                            <div id="payments-in" class="col-sm-12" style="overflow:auto; white-space: nowrap">
                            </div>
                            

                            <!-- OTHER PAYMENTS -->
                            <div class="modal" id="modal-other_payments" data-backdrop="false">>
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                            <h4 class="modal-title"><?= __("Ödemeler"); ?></h4>
                                        </div>
                                        <div class="modal-body">
                                            <div class="btn-group btn-group-justified">
                                                <?php foreach ($payment_types as $payment_type): ?>
                                                    <?php if (!in_array($payment_type->alias, ['cash', 'credit','cari'])): ?>
                                                        <a id="add-<?= $payment_type->alias ?>" class="btn btn-light-blue btn-ripple btn-xxl"><i class="<?= $payment_type->icon ?>"></i> <?= $payment_type->title ?></a>
                                                    <?php endif ?>
                                                <?php endforeach; ?>
                                            </div>

                                            <?php foreach ($payment_types as $payment_type): ?>
                                                <?php if (!in_array($payment_type->alias, ['cash', 'credit', 'cari'])): ?>
                                                    <div id="payments-in-<?= $payment_type->alias ?>" class="col-sm-3"></div>
                                                <?php endif ?>
                                            <?php endforeach; ?>

                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-success" data-dismiss="modal"><?= __("Kapat"); ?></button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- OTHER PAYMENTS -->

                            <!-- CARİ HESAP -->
                            <div class="modal" id="modal-cari" data-backdrop="false">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                            <h4 class="modal-title"><?= __("CARİ HESABA EKLE"); ?></h4>
                                        </div>
                                        <div class="modal-body" style="height:300px">
                                            <div class="form-group">
                                                <label for="customerSelect" class="label">Müşteri Seçiniz : </label>
                                                <select id="customerSelect" name="customers" class="chosen-select" style="display: none;">
                                                </select>
                                            </div>

                                            <div class="form-group m-t-3">
                                                <input name="payment_cari" class="text-center input-lg col-md-12 m-t-3" type="number" value="0">    
                                            </div>
                                     
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" id="addCari" class="btn btn-success col-md-12 input-lg2" data-dismiss="modal"><?= __("EKLE"); ?></button>
                                        </div>
                                    </div>
                                </div>
                            </div>
        
                        </li>

                        <li class="list-group-item p-a-0 payment-group">
                            <div class="col-sm-3 type">
                                <p class="col-sm-3 text-center">Ara Toplam</p>
                                <strong id="subtotal" class="col-sm-3 text-center">0.00</strong>   
                            </div>
                            <div class="col-sm-3 type">
                                <p class="col-sm-3 text-center">Toplam Vergi</p>
                                <strong id="tax-total" class="col-sm-3 text-center">0.00</strong>                                 
                            </div>
                            <div class="col-sm-3 type">
                                <p class="col-sm-3 text-center">Genel Toplam</p>
                                <strong id="total" class="col-sm-3 text-center">0.00</strong>
                            </div>
                            <div id="add-discount" class="col-sm-3 type" style="background: #FD8024; cursor:pointer;">
                                <p class="col-sm-3 text-center" >İndirim</p>
                                <strong id="discount-total" class="col-sm-3 text-center">0.00</strong> 
                            </div>
                        </li>

                        <li class="list-group-item text-right small p-y-0 hidden">
                            <span class="pull-left"><?= __("Ara Ödeme:"); ?></span> <strong id="pos-interim_paid1" class="small"></strong>
                        </li>
                        <li class="list-group-item text-right small p-y-0 hidden">
                            <span class="pull-left"><?= __("Ödenen:"); ?></span> <strong id="pos-paid1" class="small"></strong>
                        </li>
                        <li class="list-group-item text-right small p-y-0 hidden">
                            <span class="pull-left"><?= __("Kalan:"); ?></span> <strong id="pos-rest1" class="small"></strong>
                        </li>
                        <li class="list-group-item text-right small p-y-0 hidden">
                            <span class="pull-left"><?= __("Para Üstü:"); ?></span> <strong id="pos-change1" class="small"></strong>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <!-- SUMMARY -->


    <!-- LIST -->
    <div class="col-sm-7">
        <div class="panel br-0 m-b-0">

            
            <!-- PRODUCTS -->
            <div class="panel-body p-a-0" id="pos-products">
                <div class="thumbnail">
                    <div class="caption text-center">
                        <h3><?= __("Ürünler Yükleniyor..."); ?></h3>
                    </div>
                </div>
            </div>
            <!-- PRODUCTS -->

        </div>
    </div>
    <!-- LIST -->

    
    
</form>
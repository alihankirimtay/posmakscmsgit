<form>
    <div class="panel panel-default">

        <div class="panel-heading">
            <h3 class="panel-title">
                <div class="btn-group backButton">
                    <a href="#tab-pos" data-toggle="tab" class="btn btn-danger"><i class="fa fa-arrow-left"></i> <?= __("Geri"); ?></a>
                </div>
                <?= __("Kasa Kapanışı"); ?>
            </h3>
        </div>
            
        <div class="panel-body p-a-0">
            <!-- Start -->
            <div role="tabpanel">
        
                <div class="hidden">
                    <a href="#cct-moneys" data-toggle="tab">moneys</a>
                    <a href="#cct-approval" data-toggle="tab">approval</a>
                    <a href="#cct-print" data-toggle="tab">print</a>
                </div>

                <div class="tab-content">

                    <div role="tabpanel" class="tab-pane" id="cct-moneys">
                        <h3 class="text-center" id="cc-username">{OPERATOR NAME}</h3>
                        <div class="row text-center">
                            <div class="col-sm-12 text-right moneyInputs">
                                <div id="cash_inputs" class="row" role="form">
                                    <!-- Cash Inputs -->
                                </div>
                                <div id="payment_inputs">
                                    <!-- Payment Inputs -->
                                </div>
                                <div class="col-sm-3">
                                    <h3 class="m-b-0 text-red"><?= __("Açılış Miktarı"); ?></h3>
                                    <p class="cash_opening"><input type="text" name="cash_opening" class="form-control text-center" placeholder="0.00" autocomplete="off"></p>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <button id="open-approval" class="btn btn-green btn-xxl btn-block"><?= __("Kaydet / Devam Et"); ?></button>
                            </div>
                        </div>
                    </div>


                    <div role="tabpanel" class="tab-pane" id="cct-approval">
                        <h2 class="text-center"><?= __("Yönetici Onayı"); ?></h2>
                        <div class="form-horizontal" role="form">
                            <div class="form-content">
                                <div class="inputer">
                                    <div class="input-wrapper">
                                        <input type="text" name="manager-username" class="form-control text-center" placeholder="<?= __("Yönetici adı veya e-posta"); ?>">
                                    </div>
                                </div>
                                <div class="inputer">
                                    <div class="input-wrapper">
                                        <input type="password" name="manager-password" class="form-control text-center" placeholder="<?= __("Şifre"); ?>">
                                    </div>
                                </div>
                                <button id="submit-approval" class="btn btn-green btn-xxl btn-block"><?= __("Onayla"); ?></button>
                                <a href="#cct-moneys" data-toggle="tab"><?= __("Geri"); ?></a>
                            </div>
                        </div>
                    </div>

                    <div role="tabpanel" class="tab-pane" id="cct-print">
                        <button id="print-session" class="btn btn-grey btn-xxl btn-block"><i class="fa fa-print" aria-hidden="true"></i> <?= __("Yazdır"); ?></button>
                        <button onclick="location.reload();" class="btn btn-info btn-xxl btn-block"><?= __("Kapat"); ?></button>
                        <pre id="print-content" class="p-a-3">
                            <!-- Print Content -->
                        </pre>
                    </div>
                    

                </div>
            </div>
            <!-- End -->
        </div>
    </div>
</form>
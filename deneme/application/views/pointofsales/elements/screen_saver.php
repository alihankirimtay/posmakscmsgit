<div id="screen_saver_page">

	<style type="text/css">
		#screen_saver {
			background-image: url(<?= base_url($image) ?>);
			background-size: cover;
			background-color: black;
			position: fixed;
			left: 0;
			top: 0;
			width: 100%;
			height: 100%;
			z-index: 10;
			display: flex;
			align-items: center;
		}

		#screen_saver .ss_keys button {
			border-radius: 0;
			border: 2px solid #fff;
		}

		#screen_saver input[name="password"] {
			text-align: center;
		}

		#screen_saver .panel {
			    box-shadow: 0px 0px 25px rgba(0,0,0,0.8);
		}

		#screen_saver [type=password] {
			font-size: 50px;
		}
	</style>

	<div id="screen_saver" class="content-center">
		
		<div class="container">
			<div class="row">
				<div class="col-xs-12 col-md-6 col-md-offset-3">
					<div class="panel panel-default">
						<div class="panel-body">
							<form clas="row" role="form">
								
								<div class="col-xs-12">
									<div class="form-group ">
										<input name="password" type="password" class="form-control" readonly>
									</div>
								</div>

								<div class="col-xs-12 ss_keys">
									<div class="col-xs-12">
										<button type="button" class="btn btn-primary btn-ripple btn-xxl col-xs-4" data-value="1">1</button>
										<button type="button" class="btn btn-primary btn-ripple btn-xxl col-xs-4" data-value="2">2</button>
										<button type="button" class="btn btn-primary btn-ripple btn-xxl col-xs-4" data-value="3">3</button>
									</div>
									<div class="col-xs-12">
										<button type="button" class="btn btn-primary btn-ripple btn-xxl col-xs-4" data-value="4">4</button>
										<button type="button" class="btn btn-primary btn-ripple btn-xxl col-xs-4" data-value="5">5</button>
										<button type="button" class="btn btn-primary btn-ripple btn-xxl col-xs-4" data-value="6">6</button>
									</div>
									<div class="col-xs-12">
										<button type="button" class="btn btn-primary btn-ripple btn-xxl col-xs-4" data-value="7">7</button>
										<button type="button" class="btn btn-primary btn-ripple btn-xxl col-xs-4" data-value="8">8</button>
										<button type="button" class="btn btn-primary btn-ripple btn-xxl col-xs-4" data-value="9">9</button>
									</div>
									<div class="col-xs-12">
										<button type="button" class="btn btn-blue-grey btn-ripple btn-xxl col-xs-4" data-value="erase"><i class="fa fa-long-arrow-left"></i></button>
										<button type="button" class="btn btn-primary btn-ripple btn-xxl col-xs-4" data-value="0">0</button>
										<button type="button" class="btn btn-success btn-ripple btn-xxl col-xs-4" data-value="submit"><i class="fa fa-check"></i></button>
									</div>
								</div>

							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
		
	</div>


</div>
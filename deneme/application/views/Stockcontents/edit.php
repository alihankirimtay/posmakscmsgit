    
<div class="content">

    <div class="page-header full-content">
        <div class="row">
            <div class="col-sm-6">
                <h1><?= __('Stok İşlemi Düzenle') ?> <small></small></h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb">
                    <li><a href="<?= base_url(); ?>" ><i class="ion-home"></i></a></li>
                    <li><a href="<?= base_url('stocks'); ?>"><?= __('Stoklar') ?></a></li>
                    <li><a href="#"><?= $current_stock->title ?></a></li>
                    <li><a href="#" class="active"><?= __('Stok İşlemi Düzenle') ?></a></li>
                </ol>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="panel">

                <div class="panel-heading">
                    <div class="panel-title">
                        <h4><?= __('Stok Detayları') ?></h4>
                    </div>
                </div>

                <div class="panel-body">
                    <form action="#" class="form-horizontal">
                        <div class="form-content">

                            <div class="form-group">
                                <label class="control-label col-md-3"><?= __('Fiyat') ?></label>
                                <div class="col-md-9">
                                    <div class="inputer">
                                        <div class="input-wrapper">
                                            <input type="number" name="price" class="form-control" step="0.01" value="<?php ECHO $stock_content->price; ?>" placeholder="0.00">
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3"><?= isset($this->units[$current_stock->unit]) ? $this->units[$current_stock->unit] : 'Miktar' ?></label>
                                <div class="col-md-9">
                                    <div class="inputer">
                                        <div class="input-wrapper">
                                            <input type="number" name="quantity" class="form-control" step="0.01" value="<?php ECHO $stock_content->quantity; ?>" placeholder="0.00">
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3"><?= __('Açıklama') ?></label>
                                <div class="col-md-9">
                                    <div class="inputer">
                                        <div class="input-wrapper">
                                            <textarea name="desc" class="form-control js-auto-size valid"><?php ECHO $stock_content->desc; ?></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>


                            <div class="form-buttons-fixed">
                                <a type="button" href="<?= base_url('stockcontents/index/'.$current_stock->id); ?>" class="btn btn-grey"><?= __('İptal') ?></a>
                                <div class="btn-group pull-right">
                                    <button type="submit" class="btn btn-success" name="dosubmit" data-url="<?= base_url('stockcontents/edit/'.$current_stock->id.'/'.$stock_content->id);?>" ><?= __('Kaydet') ?></button>
                                    <button type="submit" class="btn btn-blue" name="dosubmit" data-url="<?= base_url('stockcontents/edit/'.$current_stock->id.'/'.$stock_content->id);?>" data-return="<?= base_url('stockcontents/index/'.$current_stock->id); ?>" ><?= __('Kaydet & Çık') ?></button>
                                </div>
                            </div>

                        </div>
                    </form>
                </div>

            </div>
        </div>
    </div>

</div>

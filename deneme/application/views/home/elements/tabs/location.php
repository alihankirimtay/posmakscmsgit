<div class="panel">
    <div class="panel-heading bb-0">
        <div class="panel-title p-l-0">
            <h4 class="content-header"><?= __('İŞLETME BİLGİLERİNİZ') ?></h4>
            <h6 style="color: black"><?= __('Burada yeni bir işletme oluşturabilir veya mevcut işletmenizi seçerek Hızlı Kurulum Sihirbazına devam edebilirsiniz.'); ?></h6>
        </div>
    </div>

    <div class="p-l-0 p-r-0 panel-body" style="overflow-x: hidden;overflow-y: auto;height: 300px;">
        <div class="well p-a-2">
            <div class="hidden">
                <a href="#locationAdd" data-toggle="tab" >1.</a>
                <a href="#locationEdit" data-toggle="tab">2.</a>
            </div>
            <div class="tab-content p-a-0 b-0" >
                <div class="tab-pane active" id="locationAdd">
                    <label for="locationId"><?= __('İşletme Adını Giriniz') ?></label>
                    <form action="#" class="form-horizontal">
                        <div class="form-content">
                            <div class="form-inline" style="width: calc(100% - 120px);">
                                <input type="name" class="form-control input-lg bg-special-white w100 bb-0" name="title" placeholder="<?= __('İşletme Adı') ?>">
                                <input type="hidden" name="dtz" value="<?= $this->user->dtz; ?>">
                                <input type="hidden" name="currency" value="₺">
                                <input type="hidden" name="active" value="<?= 1 ?>">
                            </div>
                            <div class="form-inline pull-right">   
                                <button id="restaurantAdd" type="button" class="btn btn-success input-lg br-0"><?= __('KAYDET') ?></button>   
                            </div>
                        </div>
                    </form>
                </div>

                <div class="tab-pane b-0 p-a-0" id="locationEdit">
                    <label for="locationId"><?= __('İşletme Adını Düzenleyiniz') ?></label>
                    <form action="#" class="form-horizontal">
                        <div class="form-content">
                            <div class="form-inline" style="width: calc(100% - 200px);">
                                <input type="name" class="form-control input-lg bg-special-white w100 bb-0" name="title">
                                <input type="hidden" name="dtz" value="">
                                <input type="hidden" name="currency" value="">
                                <input type="hidden" name="active" value="">
                                <input type="hidden" name="location_id" value="">
                            </div>
                            <div class="form-inline pull-right">   
                                <button id="btnLocationEdit" type="button" class="btn btn-success input-lg br-0"><?= __('DÜZENLE') ?></button>   
                                <button id="btnLocationCancel" type="button" class="btn btn-danger input-lg br-0"><?= __('İPTAL') ?></button>   
                            </div>
                        </div>
                    </form>
                </div>
            </div>

        </div>
        <div class="location-list">
            <ul class="list-group">
                
            </ul>
        </div> 
        <button type="button" class="btn btn-success footer-button-fixed br-0 btn-xxl" disabled>İLERİ</button>
               

    </div>

</div>

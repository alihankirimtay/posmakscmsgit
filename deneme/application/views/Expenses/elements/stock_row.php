<?php if (!isset($stock)): ?>

    <tr data-number="<?= $stock_number ?>">
        <td class="col-sm-3">
            <select name="stocks[<?= $stock_number ?>][title]" class="form-control stock_title" ></select>
            <input type="hidden" name="stocks[<?= $stock_number ?>][id]" value="" class="stock_id">
            <input type="hidden" name="stocks[<?= $stock_number ?>][stock_content][id]" value="" class="stock_content_id">
        </td>
        <td>
            <div class="col-sm-4">
                <select name="stocks[<?= $stock_number ?>][unit]" class="form-control text-right stock_unit">
                    <?php foreach ($this->units as $key => $unit): ?>
                        <option value="<?= $key ?>"><?= $unit ?></option>
                    <?php endforeach; ?>
                </select>
                <input type="hidden" name="stocks[<?= $stock_number ?>][unit]" class="stock_unit_hidden">
            </div>
            <div class="col-sm-8">
                <input type="text" name="stocks[<?= $stock_number ?>][stock_content][quantity]" value="" class="form-control calculate stock_content_quantity" placeholder="<?= __("Miktar"); ?>">
            </div>
        </td>
        <td>
            <input type="text" name="stocks[<?= $stock_number ?>][stock_content][price]" value="" class="form-control f_money calculate stock_content_price" placeholder="<?= __("Birim Fiyatı"); ?>">
        </td>
        <td class="col-sm-1">
            <select name="stocks[<?= $stock_number ?>][stock_content][tax]" class="form-control calculate stock_content_tax">
                <?php foreach ($this->taxes as $key => $tax): ?>
                    <option value="<?= $key ?>"><?= $tax ?></option>
                <?php endforeach; ?>
            </select>
        </td>
        <td>
            <input type="text" name="stocks[<?= $stock_number ?>][total]" value="" class="form-control f_money calculate stock_content_total" placeholder="<?= __("Toplam"); ?>">
        </td>
        <td>
            <button type="button" class="btn btn-danger btn-ripple btn-xs delete_stock"><i class="fa fa-trash"></i></button>
        </td>
    </tr>
    
<?php else: ?>

    <tr data-number="<?= $stock_number ?>">
        <td>
            <input type="text" name="stocks[<?= $stock_number ?>][title]" value="<?= $stock->title ?>" class="form-control stock_title" readonly>
            <input type="hidden" name="stocks[<?= $stock_number ?>][id]" value="<?= $stock->id ?>" class="stock_id">
            <input type="hidden" name="stocks[<?= $stock_number ?>][stock_content][id]" value="<?= $stock_content->id ?>" class="stock_content_id">
        </td>
        <td>
            <div class="col-sm-4">
                <select name="stocks[<?= $stock_number ?>][unit]" class="form-control text-right stock_unit" disabled>
                    <?php foreach ($this->units as $unit_key => $unit): ?>
                        <option value="<?= $unit_key ?>" <?= selected($stock->unit, $unit_key) ?>><?= $unit ?></option>
                    <?php endforeach; ?>
                </select>
                <input type="hidden" name="stocks[<?= $stock_number ?>][unit]" class="stock_unit_hidden" value="<?= $stock->unit ?>">
            </div>
            <div class="col-sm-8">
                <input type="text" name="stocks[<?= $stock_number ?>][stock_content][quantity]" value="<?= $stock_content->quantity ?>" class="form-control calculate stock_content_quantity" placeholder="<?= __("Miktar"); ?>" readonly>
            </div>
        </td>
        <td>
            <input type="text" name="stocks[<?= $stock_number ?>][stock_content][price]" value="<?= number_format($stock_content->price, 2) ?>" class="form-control f_money calculate stock_content_price" placeholder="<?= __("Birim Fiyatı"); ?>" readonly>
        </td>
        <td class="col-sm-1">
            <select name="stocks[<?= $stock_number ?>][stock_content][tax]" class="form-control calculate stock_content_tax" disabled>
                <?php foreach ($this->taxes as $tax_key => $tax): ?>
                    <option value="<?= $tax_key ?>" <?= selected($stock_content->tax, $tax_key) ?>><?= $tax ?></option>
                <?php endforeach; ?>
            </select>
            <input type="hidden" name="stocks[<?= $stock_number ?>][stock_content][tax]" class="" value="<?= $stock_content->tax ?>">
        </td>
        <td>
            <input type="text" name="stocks[<?= $stock_number ?>][total]" value="" class="form-control f_money calculate stock_content_total" placeholder="<?= __("Toplam"); ?>" readonly>
        </td>
        <td>
            <button type="button" class="btn btn-danger btn-ripple btn-xs delete_stock"><i class="fa fa-trash"></i></button>
        </td>
    </tr>

<?php endif; ?>
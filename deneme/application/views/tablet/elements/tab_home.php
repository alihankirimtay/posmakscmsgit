  <div class="container-fluid tab-body body-bg">
    <div class="wrapper">

      <nav id="left-sidebar">
        <div id="sidebar-left-menu">
          <div class="sidebar-logo">
            <img src="<?= asset_url('waiter.svg'); ?>" style="max-height: 150px"></img>
            <h3 id="displayUserName"><?= __('Garson Adı') ?></h3>
          </div>
          <div class="sidebar-left-body">
            <a style="background: #e8edee;">
              <h3><?= __('Görünüm') ?></h3>
            </a>
            <a href="" id="menu-tab">
              <img src="<?= asset_url('tab-icon-02.svg'); ?>"></img>
              <h4><?= __('Tab') ?></h4>
            </a>
            <a href="" id="menu-list">
              <img src="<?= asset_url('list.svg'); ?>"></img>
              <h4><?= __('Liste') ?></h4>
            </a>

            <a style="background: #e8edee;">
              <h3><?= __('Ayarlar') ?></h3>
            </a>

            <a id="exit_operator_session">
              <img src="<?= asset_url('lock.svg'); ?>"></img>
              <h4> <?= __('Ekranı Kilitle') ?></h4>
            </a>

            <a id="change_pos" data-device ="tablet">
              <img src="<?= asset_url('change.svg'); ?>"></img>
              <h4> <?= __('Kasayı Değiştir') ?></h4>
            </a>
                
            <a href="<?= base_url('login/logout?device=tablet') ?>" id="sign-out">
              <img src="<?= asset_url('sign-out.svg'); ?>"></img>
              <h4><?= __('Çıkış Yap') ?></h4>
            </a>
          </div>
      
        </div> 
      </nav>

      <div id="content">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 nav-row">

          <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 p-10">      
            <img id="left-sidebar-button" src="<?= asset_url('profile-01.svg'); ?>" style="height: 50px;"></img>
          </div> <!-- col-lg-3 close -->

          <div class="col-lg-9 col-md-9 col-sm-9 col-xs-8 nav-span-home p-10">
            <span class="home-table-choose text-center"><?= __('MASA SEÇİNİZ') ?></span>    
          </div>

          <div class="col-lg-1 col-md-1 col-sm-1 col-xs-2 text-right p-10">
            <img id="sidebarCollapse" class="icon-svg-size" src="<?= asset_url('menu.svg'); ?>"></img>
          </div>

        </div> <!-- nav-row close -->

        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 bb-3 h-53 bg-nav">
          <ul class="nav nav-tabs mb-5" id="zonelist">
          </ul>
        </div>

        <div class = "touch-swipe">
          <div class="col-md-12 col-sm-12 col-lg-12 col-xs-12 p-8" id="tablelist">
          </div>
        </div>
      </div>

      <nav id="sidebar">
        <div class="sidebar-header">
          <h4><?= __('KAT SEÇİNİZ') ?></h4>
        </div>
        <ul class="list-group" id="floorid">
        </ul>  
      </nav>

  </div>
  </div>
<div class="content">

    <div class="page-header full-content">
        <div class="row">
            <div class="col-sm-6">
                <h1><?= __('Kasalar') ?></h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb">
                    <li><a href="<?php ECHO base_url(); ?>" ><i class="ion-home"></i></a></li>
                    <li><a href="#" class="active"><?= __('Kasalar') ?></a></li>
                </ol>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="panel">

                <div class="panel-heading">
                    <div class="panel-title">
                        <?php ECHO dropdownMenu($this->user->locations_array, $location_id, 'pos/index/', 'Restoranlar'); ?>
                    </div>

                    <div class="panel-title">
                        <h4><?= __('Tüm Kasalar') ?></h4>
                        <div class="btn-group pull-right">
                            <a id="kasaOlustur" href="<?php ECHO base_url('pos/add/'); ?>" class="btn btn-success btn-xs"><i class="fa fa-plus"></i> <?= __('Kasa Oluştur') ?></a>
                        </div>
                    </div>
                </div>

                <div class="panel-body">
                    <div class="overflow-table">
                        <table class="display datatables-basic">
                            <thead>
                                <tr>
                                    <th><?= __('İsim') ?></th>
                                    <th><?= __('Restoran') ?></th>
                                    <th><?= __('Oluşturuldu') ?></th>
                                    <th><?= __('Durum') ?></th>
                                    <th><?= __('İşlem') ?></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($poses as $pos): ?>
                                    <tr>
                                        <td><?= $pos->title ?></td>
                                          <td><?= $pos->location_title ?></td>
                                        <td><?=dateConvert($pos->created, 'client', dateFormat())?></td>
                                        <td><?= get_status($pos->active) ?></td>
                                        <td>
                                            <a class="btn btn-primary btn-xs" href="<?php ECHO base_url('pos/edit/'.$pos->id); ?>"> <i class="fa fa-pencil"></i> </a>
                                            <button class="btn btn-danger btn-xs" data-url="<?=base_url('pos/delete/'.$pos->id)?>" name="dodelete"> <i class="fa fa-trash-o"></i> </button>
                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                </div>

            </div>
        </div>
    </div>

</div>
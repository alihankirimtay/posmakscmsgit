<?php
    $dFrom = dateConvert($data['date_from'], 'client', dateFormat());
    $dTo = dateConvert($data['date_to'], 'client', dateFormat());
 ?>
<div class="col-md-12">
        <!-- FILTER START -->
        <div class="dropdown">
            <button class="btn btn-default btn-xl dropdown-toggle" type="button" id="dropdownFilter" data-toggle="dropdown">
                <?= __('Filtrele') ?> <span class="caret"></span>    
            </button>
            <ul class="dropdown-menu" aria-labelledby="dropdownFilter" style="width: 100%;">

                <form action="" method="get" class="form-horizontal" role="form">
                    <div class="form-content">

                        <div class="col-xs-12 col-sm-12 col-md-6">

                            <div class="form-group">
                                <label class="control-label col-md-3"><i class="ion-android-calendar"></i> <?= __('Başlangıç Tarihi') ?></label>
                                <div class="col-md-9">
                                    <div class="inputer">
                                        <div class="input-wrapper">
                                            <input type="text" name="d" class="form-control datetimepicker-basic" value="<?php ECHO $dFrom ?>"/>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3"><i class="ion-android-calendar"></i> <?= __('Bitiş Tarihi') ?></label>
                                <div class="col-md-9">
                                    <div class="inputer">
                                        <div class="input-wrapper">
                                            <input type="text" name="dTo" class="form-control datetimepicker-basic" value="<?php ECHO $dTo ?>"/>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="form-group">
                                <label class="control-label col-md-3"><i class="ion-location"></i> <?= __('Restoran') ?></label>
                                <div class="col-md-9">
                                    <select name="l" class="chosen-select">
                                        <?php foreach ($this->user->locations_array as $location): ?>
                                            <option value="<?= $location['id']; ?>" <?= selected($data['location_id'], $location[ 'id' ]) ?>>
                                                <?= $location['title'] ;?>
                                            </option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <div class="form-group">
                                <div class="col-xs-6 col-sm-6 col-md-6 text-left">
                                    <a href="<?php ECHO base_url(); ?>" class="btn btn-default"><?= __('Temizle') ?></a>
                                </div>
                                <div class="col-xs-6 col-sm-6 col-md-6 text-right">
                                    <button type="submit" class="btn btn-primary"><?= __('Uygula') ?></button>
                                    <input type="hidden" name="tab" value="prim">

                                </div>
                            </div>
                        </div>

                    </div>
                </form>

            </ul>
        </div>                        
    </div>


    <div class="col-md-12">
        <div class="panel blue-grey">
            <div class="panel-heading">
                <div class="panel-title"><h4><?= __('Personeller') ?></h4></div>
            </div><!--.panel-heading-->
            <div class="panel-body">
                <table class="table table-bordered table-striped table-condensed">
                    <thead>
                        <tr>
                            <th class="col-md-4 text-center"><?= __('Personel Adı') ?></th>
                            <th class="col-md-3 text-center"><?= __('Toplam Ürün') ?></th>
                            <th class="col-md-3 text-center"><?= __('Toplam Prim') ?></th>
                            <th class="col-md-3 text-center"><?= __('Toplam Satış') ?></th>
                            <th class="col-md-2 text-center"><?= __('Detay') ?></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach($users as $user): ?>
                        <?php 
                            $url = array(
                                'tab'   => 'primdetails',
                                'd'     => $dFrom,
                                'dTo'   => $dTo,
                                'l'     => $data['location_id'],
                                'garsonFilter' => $user['id']
                            );

                            $url = http_build_query($url);
                        ?>

                            <tr>
                                <td class="text-center"><?= $user['username'] ?></td>
                                <td style="text-align: center;"><?= $user['quantity'] ?></td>
                                <td style="text-align: center;"><?= $user['bonus'] ?></td>
                                <td style="text-align: center;"><?= $user['sum_sale'] ?></td>
                                <td class="text-center"><a target="_blank" href="<?= base_url("reports/?$url") ?>" class="glyphicon glyphicon-search"></a></td>
                            </tr>
                        <?php endforeach; ?>
                    </tbody>
                    <thead>
                        <tr>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
   	


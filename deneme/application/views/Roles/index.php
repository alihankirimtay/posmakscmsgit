<div class="content">

    <div class="page-header full-content">
        <div class="row">
            <div class="col-sm-6">
                <h1><?= __('Roller') ?></h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb">
                    <li><a href="<?= base_url() ?>" ><i class="ion-home"></i></a></li>
                    <li><a href="#" class="active"><?= __('Ayarlar') ?></a></li>
                    <li><a href="#" class="active"><?= __('Roller') ?></a></li>
                </ol>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="panel">

                <div class="panel-heading">
                    
                    <div class="panel-title">
                        <h4><?= __('Tüm Roller') ?></h4>
                        <div class="btn-group pull-right">
                            <a id="yetkiler" href="<?= base_url('roles/permission') ?>" class="btn btn-success btn-xs"><i class="fa fa-key"></i> <?= __('Yetkiler') ?></a>
                            <a id="rolOlustur" href="<?= base_url('roles/add/') ?>" class="btn btn-success btn-xs"><i class="fa fa-plus"></i> <?= __('Rol Oluştur') ?></a>
                        </div>
                    </div>
                </div>

                <div class="panel-body">
                    <div class="overflow-table">
                        <table class="display datatables-basic">
                            <thead>
                                <tr>
                                    <th class="col-md-9"><?= __('isim') ?></th>
                                    <th class="col-md-2"><?= __('Durum') ?></th>
                                    <th class="col-md-1"><?= __('İşlem') ?></th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php foreach($roles as $role): ?>
                                <tr>
                                    <td><?= $role->title ?></td>
                                    <td><?= get_status($role->active); ?></td>
                                    <td>
                                        <?php if ($role->editable): ?>
                                            <a class="btn btn-primary btn-xs" href="<?= base_url('roles/edit/'.$role->id) ?>"> <i class="fa fa-pencil"></i> </a>
                                            <button class="btn btn-danger btn-xs" data-url="<?= base_url('roles/delete/'.$role->id) ?>" name="dodelete"> <i class="fa fa-trash-o"></i> </button>
                                        <?php endif; ?>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                                
                            </tbody>
                        </table>
                    </div>
                </div>

            </div>
        </div>
    </div>

</div>

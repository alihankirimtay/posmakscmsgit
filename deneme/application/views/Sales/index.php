<?php
    
    $saleType = function ($is_return) {
        if ($is_return)
            return '<span class="label label-danger">İade</span>';
        return '<span class="label label-success">Satış</span>';
    };

    $total = 0;
    $sum = function ($sale) {
        return $sale->return_id ? $sale->amount*-1 : $sale->amount;
    };
    
?>
<div class="content">

    <div class="page-header full-content">
        <div class="row">
            <div class="col-sm-6">
                <h1><?= __('Satışlar') ?></h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb">
                    <li><a href="<?= base_url() ?>" ><i class="ion-home"></i></a></li>
                    <li><a href="#" class="active"><?= __('İşlemler') ?></a></li>
                    <li><a href="#" class="active"><?= __('Satışlar') ?></a></li>
                </ol>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="panel">

                <div class="panel-heading">
                    <div class="panel-title">
                        <div class="dropdown">
                            <button class="btn btn-default btn-xl dropdown-toggle" type="button" id="dropdownFilter" data-toggle="dropdown">
                                <?= __('Filtrele') ?> <span class="caret"></span>    
                            </button>
                            <ul class="dropdown-menu" aria-labelledby="dropdownFilter" style="width: 100%;">

                                <form action="<?= base_url('sales') ?>" method="get" class="form-horizontal" role="form" id="filter">
                                    <div class="form-content">
                                        <div class="row">
                                            <div class="col-xs-12 col-sm-12 col-md-6">

                                                <div class="form-group">
                                                    <label class="control-label col-md-3"><?= __('Tarih') ?></label>
                                                    <div class="col-md-9">
                                                        <select name="date_filter" class="chosen-select">
                                                            <?php foreach ($this->days as $day): ?>
                                                                <option value="<?=$day[ 'id' ]?>" <?= selected($filter['date_filter'], $day['id']) ?>>
                                                                    <?= $day[ 'title' ] ?>
                                                                </option>
                                                            <?php endforeach; ?>
                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label class="control-label col-md-3"><i class="ion-android-calendar"></i> <?= __('Başlangıç') ?></label>
                                                    <div class="col-md-9">
                                                        <div class="inputer">
                                                            <div class="input-wrapper">
                                                                <input type="text" name="date_start" class="form-control datetimepicker-basic" value="<?= $filter['date_start_client'] ?>"/>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label class="control-label col-md-3"><i class="ion-android-calendar"></i> <?= __('Bitiş') ?></label>
                                                    <div class="col-md-9">
                                                        <div class="inputer">
                                                            <div class="input-wrapper">
                                                                <input type="text" name="date_end" class="form-control datetimepicker-basic" value="<?= $filter['date_end_client'] ?>" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label class="control-label col-md-3"><?= __('Restoran') ?></label>
                                                    <div class="col-md-9">
                                                        <select name="location_id" class="chosen-select">
                                                            <?php foreach ($this->user->locations_array as $location): ?>
                                                                <option value="<?= $location['id']; ?>" <?= selected($filter['location_id'], $location['id']) ?>>
                                                                    <?= $location['title'] ;?>
                                                                </option>
                                                            <?php endforeach; ?>
                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label class="control-label col-md-3"><?= __('Kasa') ?></label>
                                                    <div class="col-md-9">
                                                        <select name="pos_id" class="chosen-select">
                                                            <option value="0"><?= __('Hepsi') ?></option>
                                                            <?php foreach ($poses as $pos): ?>
                                                                <option value="<?= $pos->id ?>" <?= selected($filter['pos_id'], $pos->id) ?>>
                                                                    <?= $pos->title ?>
                                                                </option>
                                                            <?php endforeach; ?>
                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label class="control-label col-md-3"><?= __('Personel(Siparişi Alan)') ?></label>
                                                    <div class="col-md-9">
                                                        <select name="user_id" class="chosen-select">
                                                            <option value="0"><?= __('Hepsi') ?></option>
                                                            <?php foreach ($users as $user): ?>
                                                                <option value="<?= $user->id; ?>" <?= selected($filter['user_id'], $user->id) ?>>
                                                                    <?= $user->username ;?>
                                                                </option>
                                                            <?php endforeach; ?>
                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label class="control-label col-md-3"><?= __('Personel(Hesabı Alan)') ?></label>
                                                    <div class="col-md-9">
                                                        <select name="completer_user_id" class="chosen-select">
                                                            <option value="0"><?= __('Hepsi') ?></option>
                                                            <?php foreach ($users as $user): ?>
                                                                <option value="<?= $user->id; ?>" <?= selected($filter['completer_user_id'], $user->id) ?>>
                                                                    <?= $user->username ;?>
                                                                </option>
                                                            <?php endforeach; ?>
                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label class="control-label col-md-3"><?= __('Ödeme Tipi') ?></label>
                                                    <div class="col-md-9">
                                                        <select name="payment_type_id" class="chosen-select">
                                                            <option value="0"><?= __('Hepsi') ?></option>
                                                            <?php foreach ($payment_types as $payment_type): ?>
                                                                <option value="<?= $payment_type->id ?>" <?= selected($filter['payment_type_id'], $payment_type->id) ?>>
                                                                    <?= $payment_type->title ?>
                                                                </option>
                                                            <?php endforeach; ?>
                                                        </select>
                                                    </div>
                                                </div>

                                            </div>
                                            
                                            <div class="col-xs-12 col-sm-12 col-md-12">
                                                <div class="form-group">
                                                    <div class="col-sm-12 text-right">
                                                        <button type="submit" class="btn btn-primary"><?= __('Uygula') ?></button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </form>

                            </ul>
                        </div>                        
                    </div>
                </div>

                <div class="panel-heading">
                    <div class="panel-title">
                        <h4><?= __('Tüm Satışlar') ?></h4>
                        <div class="btn-group pull-right">
                            <a id="satisOlustur" href="<?= base_url('sales/add') ?>" class="btn btn-success btn-xs"><i class="fa fa-plus"></i> <?= __('Satış Oluştur') ?></a>
                        </div>
                    </div>
                </div>

                <div class="panel-body">

                    <div class="overflow-table">
                        <table class="display datatables-basic">
                            <thead>
                                <tr>
                                    <th><?= __('Tarih') ?></th>
                                    <th><?= __('#ID') ?></th>
                                    <th><?= __('Tür') ?></th>
                                    <th><?= __('Personel(Sipariş)') ?></th>
                                    <th><?= __('Personel(Hesap)') ?></th>
                                    <th><?= __('Kat') ?></th>
                                    <th><?= __('Bölge') ?></th>
                                    <th><?= __('Masa') ?></th>
                                    <th><?= __('Toplam') ?></th>
                                    <th><?= __('Ödeme Tipi') ?></th>
                                    <th><?= __('İşlem') ?></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($sales as $sale): ?>
                                    <?php $total += $sum($sale); ?>
                                    <tr data-id="<?= $sale->id ?>">
                                        <td momentjs="utc2local" datetime="<?= $sale->payment_date ?>"></td>
                                        <td><?= $sale->id ?></td>
                                        <td><?= $saleType($sale->return_id) ?></td>
                                        <td><?= $sale->user_name ?></td>
                                        <td><?= $sale->completer_user_name ?></td>
                                        <td><?= $sale->floor_title ?></td>
                                        <td><?= $sale->zone_title ?></td>
                                        <td><?= $sale->point_title ?></td>
                                        <td class="text-right"><?= number_format($sale->amount, 2) ?> <?= $currency ?></td>
                                        <td><?= $sale->payment_type_title ?></td>
                                        <td>
                                            <div class="btn-group">
                                                <button type="button" class="btn btn-grey btn-riple dropdown-toggle" data-toggle="dropdown">
                                                    <i class="fa fa-file-text-o"></i> <span class="caret"></span>
                                                </button>
                                                <ul class="dropdown-menu dropdown-menu-right" role="menu">
                                                    <?php if ($sale->invoice_id): ?>
                                                        <li><a href="#" class="load_invoice"><?= __('Fatura Düzenle') ?></a></li>
                                                        <li><a href="<?= base_url('sales/view/'.$sale->id) ?>"><?= __('Fatura Yazdır') ?></a></li>
                                                    <?php else: ?>
                                                        <li><a href="#" class="load_invoice"><?= __('Fatura Oluştur') ?></a></li>
                                                    <?php endif; ?>
                                                    <li><a href="#" class="adisyon"><?= __('Adisyon') ?></a></li>
                                                </ul>
                                            </div>
                                            <a class="btn btn-primary btn-riple" href="<?= base_url('sales/edit/'.$sale->id) ?>"><i class="fa fa-pencil"></i></a>
                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                            </tbody>
                            <tfoot>
                                <tr>
                                    <th class="text-right" colspan="9"><?= number_format($total, 2) ?> <?= $currency ?></th>
                                    <th></th>
                                    <th></th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>



<?php $this->load->view('Sales/elements/invoice_modal'); ?>
<?php $this->load->view('Sales/elements/adisyon_modal'); ?>
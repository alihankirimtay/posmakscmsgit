    
<div class="content">

    <div class="page-header full-content">
        <div class="row">
            <div class="col-sm-6">
                <h1><?= __('Stok Düzenle') ?> <small></small></h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb">
                    <li><a href="<?= base_url(); ?>" ><i class="ion-home"></i></a></li>
                    <li><a href="<?= base_url('stocks'); ?>"><?= __('Stoklar') ?></a></li>
                    <li><a href="#" class="active"><?= __('Stok Düzenle') ?></a></li>
                </ol>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="panel">

                <div class="panel-heading">
                    <div class="panel-title">
                        <h4><?= __('Stok Detayları') ?></h4>
                    </div>
                </div>

                <div class="panel-body">
                    <form action="#" class="form-horizontal">
                        <div class="form-content">

                            <div class="form-group">
                                <label class="control-label col-md-3"><?= __('İsim') ?></label>
                                <div class="col-md-9">
                                    <div class="inputer">
                                        <div class="input-wrapper">
                                            <input type="text" name="title" class="form-control" value="<?php ECHO $stock->title; ?>">
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3"><?= __('Açıklama') ?></label>
                                <div class="col-md-9">
                                    <div class="inputer">
                                        <div class="input-wrapper">
                                            <textarea name="desc" class="form-control js-auto-size valid"><?php ECHO $stock->desc; ?></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3"><?= __('Restoran') ?></label>
                                <div class="col-md-9">
                                    <select name="location_id" data-placeholder="Restoran bulunamadı" class="chosen-select">
                                        <option value="0"><?= __('Restoran Seç') ?></option>
                                        <?php foreach ($this->user->locations_array as $location): ?>
                                            <option value="<?php ECHO $location['id']; ?>" <?php ECHO ($location['id']==$stock->location_id)?'selected="selected"':'';?>><?php ECHO $location['title'] ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3"><?= __('Ölçü Birimi') ?></label>
                                <div class="col-md-9">
                                    <select name="unit" class="chosen-select">
                                        <option value=""><?= __('Ölçü Birimi Seç') ?></option>
                                        <?php foreach ($this->units as $key => $unit): ?>
                                            <option value="<?= $key ?>" <?= selected($stock->unit, $key) ?>><?= $unit ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3"><?= __('Alt Limit') ?></label>
                                <div class="col-md-9">
                                    <div class="inputer">
                                        <div class="input-wrapper">
                                            <input type="number" name="sub_limit" class="form-control" step="0.01" value="<?= $stock->sub_limit ?>" placeholder="0.00">
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3"><?= __('Durum') ?></label>
                                <div class="col-md-9">
                                    <div class="radioer">
                                        <input type="radio" name="active" id="active1" <?php checked($stock->active, 1); ?> value="1" >
                                        <label for="active1"><?= __('Aktif') ?></label>
                                    </div>
                                    <div class="radioer">
                                        <input type="radio" name="active" id="active0" <?php checked($stock->active, 0); ?> value="0" >
                                        <label for="active0"><?= __('Pasif') ?></label>
                                    </div>
                                </div>
                            </div>

                            <div class="form-buttons-fixed">
                                <a type="button" href="<?= base_url('stocks'); ?>" class="btn btn-grey"><?= __('İptal') ?></a>
                                <div class="btn-group pull-right">
                                    <button type="submit" class="btn btn-success" name="dosubmit" data-url="<?= base_url('stocks/edit/'.$stock->id);?>" ><?= __('Kaydet') ?></button>
                                    <button type="submit" class="btn btn-blue" name="dosubmit" data-url="<?= base_url('stocks/edit/'.$stock->id);?>" data-return="<?= base_url('stocks'); ?>" ><?= __('Kaydet & Çık') ?></button>
                                </div>
                            </div>

                        </div>
                    </form>
                </div>

            </div>
        </div>
    </div>

</div>

<div class="content">

    <div class="page-header full-content">
        <div class="row">
            <div class="col-sm-6">
                <h1><?= __("Restoranlar"); ?></h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb">
                    <li><a href="<?php ECHO base_url(); ?>" ><i class="ion-home"></i></a></li>
                    <li><a href="#" class="active"><?= __("Restoranlar"); ?></a></li>
                </ol>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="panel">

                <div class="panel-heading">
                    <div class="panel-title">
                        <h4><?= __("Tüm Restoranlar"); ?></h4>
                        <div class="btn-group pull-right" id="restoranolustur">
                            <a href="<?php ECHO base_url('locations/add/'); ?>" class="btn btn-success btn-xs"><i class="fa fa-plus"></i><?= __("Restoran Oluştur"); ?></a>
                        </div>
                    </div>
                </div>

                <div class="panel-body">
                    <div class="overflow-table">
                        <table class="display datatables-basic">
                            <thead>
                                <tr>
                                    <th class="col-md-4"><?= __("İsim"); ?></th>
                                    <th class="col-md-4"><?= __("Oluşturuldu"); ?></th>
                                    <th class="col-md-2"><?= __("Durum"); ?></th>
                                    <th class="col-md-2"><?= __("İşlem"); ?></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($locations as $location): ?>
                                    <tr>
                                        <td><?php ECHO $location->title; ?></td>
                                        <td><?=dateConvert($location->created, 'client', dateFormat())?></td>
                                        <td><?php ECHO get_status($location->active); ?></td>
                                        <td> 
                                            <a class="btn btn-success btn-xs" href="<?php ECHO base_url('template/invoice/'.$location->id); ?>"> <i class="fa fa-ticket"></i> <?= __("Fiş Tasarımı"); ?></a>
                                            <a class="btn btn-primary btn-xs" href="<?php ECHO base_url('locations/edit/'.$location->id); ?>"> <i class="fa fa-pencil"></i> </a>
                                            <button class="btn btn-danger btn-xs" data-url="<?php ECHO base_url('locations/delete/'.$location->id); ?>" name="dodelete"> <i class="fa fa-trash-o"></i> </button>
                                        </td>
                                    </tr>
                                <?php endforeach ?>
                            </tbody>
                        </table>
                    </div>
                </div>

            </div>
        </div>
    </div>

</div>

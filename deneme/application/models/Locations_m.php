<?php
	if (!defined('BASEPATH')) exit('No direct script access allowed');
	
	class Locations_M extends M_Model {
	
	    public $table = 'locations';

	    function __construct()
	    {
            $this->before_create = ['_setDtz', '_setYemekSepeti'];
            $this->before_update = ['_setDtz', '_setYemekSepeti'];
            $this->before_get = ['_whereNonDeleted'];

            $this->has_many_pivot['users'] = [
                'foreign_model'     =>'Users_M',
                'pivot_table'       =>'user_has_locations',
                'local_key'         =>'id',
                'pivot_local_key'   =>'location_id',
                'pivot_foreign_key' =>'user_id',
                'foreign_key'       =>'id',
            ];

            $this->rules = [
                'insert' => [
                    'title' => [
                        'field' => 'title',
                        'label' => 'Başlık',
                        'rules' => 'required|trim|xss_clean|is_unique[locations.title]',
                    ],
                    'desc' => [
                        'field' => 'desc',
                        'label' => 'Açıklama',
                        'rules' => 'trim|xss_clean',
                    ],
                    'currency' => [
                        'field' => 'currency',
                        'label' => 'Para Birimi',
                        'rules' => 'required|trim|xss_clean',
                    ],
                    'dtz' => [
                        'field' => 'dtz',
                        'label' => 'Zaman Dilimi',
                        'rules' => 'required|trim|xss_clean',
                    ],
                    'active' => [
                        'field' => 'active',
                        'label' => 'Durum',
                        'rules' => 'required|trim|in_list[1,0]',
                    ],
                ],
                'update' => [
                    'title' => [
                        'field' => 'title',
                        'label' => 'Başlık',
                        'rules' => 'required|trim|xss_clean',
                    ],
                    'desc' => [
                        'field' => 'desc',
                        'label' => 'Açıklama',
                        'rules' => 'trim|xss_clean',
                    ],
                    'currency' => [
                        'field' => 'currency',
                        'label' => 'Para Birimi',
                        'rules' => 'required|trim|xss_clean',
                    ],
                    'dtz' => [
                        'field' => 'dtz',
                        'label' => 'Zaman Dilimi',
                        'rules' => 'required|trim|xss_clean',
                    ],
                    'active' => [
                        'field' => 'active',
                        'label' => 'Durum',
                        'rules' => 'required|trim|in_list[1,0]',
                    ],
                ]
            ];

	    	parent::__construct();
	    }

        public function _whereNonDeleted()
        {
            $this->where('active !=', 3);
        }

        public function _setDtz($data)
        {
            if (!isset($data['dtz'])) {
                return $data;
            }
            
            if (!in_array($data['dtz'], $this->findDateTimeZones())) {
                messageAJAX('error', 'Gerçerli bir Zaman dilimi seçin.');
            }

            return $data;
        }

        public function _setYemekSepeti($data)
        {
            $username = $this->input->post('yemeksepeti[username]');
            $password = $this->input->post('yemeksepeti[password]');

            $data['yemeksepeti'] = json_encode([
                'username' => $username,
                'password' => $password
            ]);

            return $data;
        }

        public function findDateTimeZones()
        {
            return DateTimeZone::listIdentifiers();
        }

        public function findLocationUsers($location_id, $select = 'U.id, U.username') {
           $users = $this->db->select($select)
            ->join('user_has_locations as UHL', 'U.id = UHL.user_id', 'inner')
            ->where([
                'location_id' => $location_id,
                'active !=' => 3
            ])
            ->get('users U')->result();
            return $users;
        }

        public function findByUser($user_id = null)
      {
            return $this->db
            ->select('locations.*')
            ->where([
                  'users.active !=' => 3,
                  'locations.active !=' => 3,
                  'users.id' => $user_id,
            ])
            ->join('user_has_locations', 'user_has_locations.location_id = locations.id')
            ->join('users', 'users.id = user_has_locations.user_id')
            ->group_by('locations.id')
            ->get('locations')
            ->result();
      }
	}    
?>

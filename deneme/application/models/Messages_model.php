<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Messages_model extends CI_Model {

    /**
     * @name string TABLE_NAME Holds the name of the table in use by this model
     */
    const TABLE_NAME = 'messages';

    /**
     * @name string PRI_INDEX Holds the name of the tables' primary index used in this model
     */
    const PRI_INDEX = 'id';

    /**
     * Inserts new data into database
     *
     * @param Array $data Associative array with field_name=>value pattern to be inserted into database
     * @return mixed Inserted row ID, or false if error occured
     */
    public function insert(Array $data) {
        if ($this->db->insert(self::TABLE_NAME, $data)) {
            return $this->db->insert_id();
        } else {
            return false;
        }
    }

    /**
     * Updates selected record in the database
     *
     * @param Array $data Associative array field_name=>value to be updated
     * @param Array $where Optional. Associative array field_name=>value, for where condition. If specified, $id is not used
     * @return int Number of affected rows by the update query
     */
    public function update(Array $data, $where = array()) {
            if (!is_array($where)) {
                $where = array(self::PRI_INDEX => $where);
            }
        $this->db->update(self::TABLE_NAME, $data, $where);
        return $this->db->affected_rows();
    }

    /**
     * Deletes specified record from the database
     *
     * @param Array $where Optional. Associative array field_name=>value, for where condition. If specified, $id is not used
     * @return int Number of rows affected by the delete query
     */
    public function delete($where = array()) {
        if (!is_array()) {
            $where = array(self::PRI_INDEX => $where);
        }
        $this->db->delete(self::TABLE_NAME, $where);
        return $this->db->affected_rows();
    }




    public function getUsers()
    {          
        $this->db->select('users.id , users.username , users.image , messages.viewed');
        $this->db->where('users.id !=' , $this->user->id);
        $this->db->where('active', 1);
        $this->db->join('messages' , 'messages.sender = users.id' , 'left');
        $this->db->group_by('users.id');
        $data = $this->db->get('users')->result();
        
        foreach($data as $val):
                
            $users[] = array(
                'id' => $val->id,
                'username' => $val->username,
                'image'    => (!file_exists(FCPATH.'assets/uploads/profile/'.$val->image) ? 'no_image.png' : ($val->image == NULL ? 'no_image.png' : $val->image) ),
                'viewed'   => $val->viewed
            );
            
        endforeach;
        return $users;
    }


    public function startChat($id)
    {

        $this->db->where('sender' , $id);
        $this->db->where('reciever' , $this->user->id);
        $this->db->update('messages' , array('viewed' => 1));

        $this->db->select('messages.id , messages.reciever , messages.sender , messages.message , messages.created');
        $this->db->group_start();
        $this->db->where('sender' , $this->user->id);
        $this->db->where('reciever' , $id);
        $this->db->group_end();
        $this->db->or_group_start();
        $this->db->where('sender' , $id);
        $this->db->where('reciever' , $this->user->id);
        $this->db->group_end();
        return $this->db->get('messages')->result();
    }

    public function msgControl($id , $msgid)
    {
        $this->db->where('reciever' , $this->user->id);
        $this->db->where('sender', $id);
        $this->db->where('id > '.$msgid);
        return $this->db->get('messages')->result();
    }


    public function NewControl()
    {   
        $this->db->select('id');
        $this->db->where('reciever' , $this->user->id);
        $this->db->where('viewed' , 0);
        $this->db->group_by('sender');
        return $this->db->get('messages')->num_rows();
    }
}

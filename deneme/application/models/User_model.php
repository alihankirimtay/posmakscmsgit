<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User_model extends CI_Model {

    public function __construct()
    {
        parent::__construct();
        //Do your magic here
    }

    public function edit($image = NULL)
    {   
        if($this->input->post('password')):
            if(sha1($this->input->post('password') . $this->config->config['salt']) != $this->user->password):

                $values['user_id']              = $this->user->id;
                $values['password']             = sha1($this->input->post('password') . $this->config->config['salt']);
                $values['activation_code']      = uniqid(sha1($this->user->username.$this->user->id));

                $control = $this->db->where('user_id' , $this->user->id)->get('user_has_activation')->num_rows();

                if($control > 0):

                    $this->db->where('user_id' , $this->user->id);
                    $this->db->update('user_has_activation' , $values);

                    if($this->db->affected_rows() > 0):
                        return TRUE;
                    else:
                        return NULL;
                    endif;

                else:

                    if(!$this->db->insert('user_has_activation' , $values)):

                        return FALSE;
                    else:
                        return TRUE;
                    endif;

                endif;
                

            else:

                return NULL;

            endif;
        endif;
        if($image != NULL):

            $this->db->where('id' , $this->user->id);
            $this->db->update('users' , $image);

            if($this->db->affected_rows() > 0):
                return TRUE;
            else:
                return FALSE;
            endif;

        endif;



    }


    public function activationData()
    {
        return $this->db->where('user_id' , $this->user->id)->get('user_has_activation')->row_array();
    }


    

    

}

/* End of file User_model.php */
/* Location: ./application/models/User_model.php */

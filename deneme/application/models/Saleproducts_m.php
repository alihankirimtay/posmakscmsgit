<?php
	if (!defined('BASEPATH')) exit('No direct script access allowed');
	
	class SaleProducts_M extends M_Model {
	
	    public $table = 'sale_has_products';

	    public function __construct()
	    {
            $this->before_create = [];
            $this->before_update = [];
            $this->before_get = ['_whereNonDeleted'];

            $this->has_many['additional_products'] = [
                'foreign_model' => 'Saleproducts_M',
                'foreign_table' => 'sale_has_products',
                'foreign_key'   => 'additional_sale_product_id',
                'local_key'     => 'id'
            ];

            $this->has_many_pivot['product_stocks'] = [
                'foreign_model'     =>'Stocks_M',
                'pivot_table'       =>'sale_products_stocks',
                'local_key'         =>'id',
                'pivot_local_key'   =>'sale_product_id',
                'pivot_foreign_key' =>'stock_id',
                'foreign_key'       =>'id'
            ];

            $this->rules = [
                // 'insert' => [
                //     'sale_id' => [
                //         'field' => 'sale_id',
                //         'label' => 'Satış',
                //         'rules' => 'required|trim|search_table[sales.id]',
                //     ],
                //     'product_id' => [
                //         'field' => 'product_id',
                //         'label' => 'Ürün',
                //         'rules' => 'required|trim|search_table[products.id]',
                //     ],
                //     'title' => [
                //         'field' => 'title',
                //         'label' => 'Başlık',
                //         'rules' => 'required|trim',
                //     ],
                //     'subtotal' => [
                //         'field' => 'subtotal',
                //         'label' => 'Ara Toplam',
                //         'rules' => 'required|trim|is_decimal',
                //     ],
                //     'change' => [
                //         'field' => 'change',
                //         'label' => 'Para Üstü',
                //         'rules' => 'required|trim|is_decimal',
                //     ],
                //     'cash' => [
                //         'field' => 'cash',
                //         'label' => 'Nakit',
                //         'rules' => 'required|trim|is_decimal',
                //     ],
                //     'credit' => [
                //         'field' => 'credit',
                //         'label' => 'Kredi Kartı',
                //         'rules' => 'required|trim|is_decimal',
                //     ],
                //     'voucher' => [
                //         'field' => 'voucher',
                //         'label' => 'İkram',
                //         'rules' => 'required|trim|is_decimal',
                //     ],
                //     'ticket' => [
                //         'field' => 'ticket',
                //         'label' => 'Ticket',
                //         'rules' => 'required|trim|is_decimal',
                //     ],
                //     'sodexo' => [
                //         'field' => 'sodexo',
                //         'label' => 'Sodexo',
                //         'rules' => 'required|trim|is_decimal',
                //     ],
                //     'discount_amount' => [
                //         'field' => 'discount_amount',
                //         'label' => 'discount_amount',
                //         'rules' => 'trim',
                //     ],
                //     'discount_type' => [
                //         'field' => 'discount_type',
                //         'label' => 'discount_type',
                //         'rules' => 'trim',
                //     ],
                //     'desc' => [
                //         'field' => 'desc',
                //         'label' => 'Not',
                //         'rules' => 'trim|xss_clean',
                //     ],
                //     'status' => [
                //         'field' => 'status',
                //         'label' => 'Sipariş Durumu',
                //         'rules' => 'required|trim|in_list[pending,completed,cancelled]',
                //     ],
                //     'active' => [
                //         'field' => 'active',
                //         'label' => 'Durum',
                //         'rules' => 'required|trim|in_list[1,0]',
                //     ],
                // ],
                // 'update' => [
                //     'sale_id' => [
                //         'field' => 'sale_id',
                //         'label' => 'Satış',
                //         'rules' => 'required|trim|search_table[sales.id]',
                //     ],
                //     'product_id' => [
                //         'field' => 'product_id',
                //         'label' => 'Ürün',
                //         'rules' => 'required|trim|search_table[products.id]',
                //     ],
                //     'title' => [
                //         'field' => 'title',
                //         'label' => 'Başlık',
                //         'rules' => 'required|trim',
                //     ],
                //     'subtotal' => [
                //         'field' => 'subtotal',
                //         'label' => 'Ara Toplam',
                //         'rules' => 'required|trim|is_decimal',
                //     ],
                //     'change' => [
                //         'field' => 'change',
                //         'label' => 'Para Üstü',
                //         'rules' => 'required|trim|is_decimal',
                //     ],
                //     'cash' => [
                //         'field' => 'cash',
                //         'label' => 'Nakit',
                //         'rules' => 'required|trim|is_decimal',
                //     ],
                //     'credit' => [
                //         'field' => 'credit',
                //         'label' => 'Kredi Kartı',
                //         'rules' => 'required|trim|is_decimal',
                //     ],
                //     'voucher' => [
                //         'field' => 'voucher',
                //         'label' => 'İkram',
                //         'rules' => 'required|trim|is_decimal',
                //     ],
                //     'ticket' => [
                //         'field' => 'ticket',
                //         'label' => 'Ticket',
                //         'rules' => 'required|trim|is_decimal',
                //     ],
                //     'sodexo' => [
                //         'field' => 'sodexo',
                //         'label' => 'Sodexo',
                //         'rules' => 'required|trim|is_decimal',
                //     ],
                //     'discount_amount' => [
                //         'field' => 'discount_amount',
                //         'label' => 'discount_amount',
                //         'rules' => 'trim',
                //     ],
                //     'discount_type' => [
                //         'field' => 'discount_type',
                //         'label' => 'discount_type',
                //         'rules' => 'trim',
                //     ],
                //     'desc' => [
                //         'field' => 'desc',
                //         'label' => 'Not',
                //         'rules' => 'trim|xss_clean',
                //     ],
                //     'status' => [
                //         'field' => 'status',
                //         'label' => 'Sipariş Durumu',
                //         'rules' => 'required|trim|in_list[pending,completed,cancelled]',
                //     ],
                //     'active' => [
                //         'field' => 'active',
                //         'label' => 'Durum',
                //         'rules' => 'required|trim|in_list[1,0]',
                //     ],
                // ]
            ];

	    	parent::__construct();
	    }

        public function _whereNonDeleted()
        {
            $this->where('active !=', 3);
        }

        public function findSaleProductWithProducts($sale_product_id)
        {
            $sale_products =
            $sale_package_products =
            $sale_products_stocks =
            $sale_packages_stocks = [];


            $sale_products = $this->db
            ->group_start()
                ->where('id', $sale_product_id)
                ->or_where('additional_sale_product_id', $sale_product_id)
            ->group_end()
            ->where('active !=', 3)
            ->get('sale_has_products')
            ->result();

            if (!$sale_products) {
                return false;
            }

            
            // Handle Products
            if ($sale_products) {

                $product_ids = [];
                $package_ids = [];
                foreach ($sale_products as $row) {
                    if ($row->type == 'package') {
                        $package_ids[] = $row->id;
                    } else if ($row->type == 'product') {
                        $product_ids[] = $row->id;
                    }
                }
                
                // Handle Packages
                if ($package_ids) {
                    
                    // Grouping Where In..
                    $ids = [];
                    $this->db->group_start();
                    foreach ($package_ids as $key => $id) {

                        $key++;
                        $ids[] = $id;
                        if ($key % 20 === 0) {
                            $this->db->or_where_in('package_id', $ids);
                            $ids = [];
                        }
                    }
                    if ($ids)
                        $this->db->or_where_in('package_id', $ids);
                    $this->db->group_end();
                    // END - Grouping Where In..

                    $sale_package_products = $this->db
                    ->where('active !=', 3)
                    ->get('package_has_products')
                    ->result();

                    // Handle Stocks in sale packages
                    if ($sale_package_products) {
                        
                        // Grouping Where In..
                        $ids = [];
                        $this->db->group_start();
                        foreach ($sale_package_products as $key => $row) {

                            $key++;
                            $ids[] = $row->id;;
                            if ($key % 20 === 0) {
                                $this->db->or_where_in('sale_package_id', $ids);
                                $ids = [];
                            }
                        }
                        if ($ids)
                            $this->db->or_where_in('sale_package_id', $ids);
                        $this->db->group_end();
                        // END - Grouping Where In..

                        $sale_packages_stocks = $this->db
                        ->select('*, (SELECT title FROM stocks WHERE id = stock_id LIMIT 1) As stock_name')
                        ->get('sale_packages_stocks')
                        ->result();

                        foreach ($sale_package_products as &$sale_package_product) {
                            
                            $sale_package_product->stocks = [];

                            // Stocks in package
                            foreach ($sale_packages_stocks as $key => $sale_package_stock) {
                                if ($sale_package_stock->sale_package_id != $sale_package_product->id){
                                    continue;
                                }

                                $sale_package_product->stocks[$sale_package_stock->stock_id] = $sale_package_stock;
                                unset($sale_packages_stocks[$key]);
                            }
                        }
                        unset($sale_package_product);

                    }
                }

                // Handle Stocks in sale products
                if ($product_ids) {
                    
                    // Grouping Where In..
                    $ids = [];
                    $this->db->group_start();
                    foreach ($product_ids as $key => $id) {

                        $key++;
                        $ids[] = $id;
                        if ($key % 20 === 0) {
                            $this->db->or_where_in('sale_product_id', $ids);
                            $ids = [];
                        }
                    }
                    if ($ids)
                        $this->db->or_where_in('sale_product_id', $ids);
                    $this->db->group_end();
                    // END - Grouping Where In..

                    $sale_products_stocks = $this->db
                    ->select('*, (SELECT title FROM stocks WHERE id = stock_id LIMIT 1) As stock_name')
                    ->get('sale_products_stocks')
                    ->result();
                }
            }


            // Hadnle stocks in sale products
            foreach ($sale_products as &$sale_product) {
                
                $sale_product->additional_products = [];

                if ($sale_product->type == 'product') {
                    
                    $sale_product->stocks = [];

                    // Stocks in product
                    foreach ($sale_products_stocks as $key => $sale_product_stock) {
                        if ($sale_product_stock->sale_product_id != $sale_product->id){
                            continue;
                        }

                        $sale_product->stocks[$sale_product_stock->stock_id] = $sale_product_stock;
                        unset($sale_products_stocks[$key]);
                    }
                }

                
                else if ($sale_product->type == 'package') {

                    $sale_product->products = [];

                    // Products in package
                    foreach ($sale_package_products as $key2 => $sale_package_product) {
                        if ($sale_package_product->package_id != $sale_product->id){
                            continue;
                        }

                        $sale_product->products[] = $sale_package_product;
                        unset($sale_package_products[$key2]);
                    }
                }
            }
            unset($sale_product);

            // Move the additional products into the parent product.
            $_sale_products = $sale_products;
            foreach ($sale_products as $key => &$sale_product) {
                if (!$sale_product->additional_sale_product_id) {
                    foreach ($_sale_products as $key1 => $_sale_product) {
                        if ($sale_product->id == $_sale_product->additional_sale_product_id) {
                            $sale_product->additional_products[$_sale_product->product_id] = $_sale_product;
                            unset($_sale_products[$key1]);
                        }
                    }
                } else {
                    unset($sale_products[$key]);
                }
            }
            unset($sale_product, $_sale_products);


            return $sale_products[0];
        }
	}    
?>

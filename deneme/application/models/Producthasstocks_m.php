<?php
	if (!defined('BASEPATH')) exit('No direct script access allowed');
	
	class ProductHasStocks_M extends M_Model {
	
	    public $table = 'product_has_stocks';
	    public $soft_deletes = false;
        public $timestamps = false;

	    public function __construct()
	    {
	    	parent::__construct();
	    }
	}    
?>

<?php
	if (!defined('BASEPATH')) exit('No direct script access allowed');
	
	class Products_M extends M_Model {
	
	    public $table = 'products';

	    public function __construct()
	    {
            $this->before_create = ['_setImage'];
            $this->before_update = ['_setImage'];
            $this->before_get = ['_whereNonDeleted'];

            $this->has_many['portions'] = [
                'foreign_model' => 'Products_m',
                'foreign_table' => 'products',
                'foreign_key'   => 'portion_id',
                'local_key'     => 'id'
            ];

            $this->has_many_pivot['stocks'] = [
                'foreign_model'     =>'Stocks_M',
                'pivot_table'       =>'product_has_stocks',
                'local_key'         =>'id',
                'pivot_local_key'   =>'product_id',
                'pivot_foreign_key' =>'stock_id',
                'foreign_key'       =>'id',
            ];

            $this->has_many_pivot['additional_products'] = [
                'foreign_model'     =>'Products_M',
                'pivot_table'       =>'products_additional_products',
                'local_key'         =>'id',
                'pivot_local_key'   =>'product_id',
                'pivot_foreign_key' =>'additional_product_id',
                'foreign_key'       =>'id',
            ];

            $this->has_many_pivot['quick_notes'] = [
                'foreign_model'     =>'Products_M',
                'pivot_table'       =>'products_quick_notes',
                'local_key'         =>'id',
                'pivot_local_key'   =>'product_id',
                'pivot_foreign_key' =>'quick_note_id',
                'foreign_key'       =>'id',
            ];

            // $this->has_many['productsAdditionalProducts'] = [
            //     'foreign_model' => 'ProductsAdditionalProducts_M',
            //     'foreign_table' => 'products_additional_products',
            //     'foreign_key'   => 'product_id',
            //     'local_key'     => 'id'
            // ];

            $taxes = implode(',', array_keys($this->taxes));

            $this->rules = [
                'insert' => [
                    'location_id' => [
                        'field' => 'location_id',
                        'label' => 'Restoran',
                        'rules' => 'required|trim|search_table[locations.id]',
                    ],
                    'product_type_id' => [
                        'field' => 'product_type_id',
                        'label' => 'Kategori',
                        'rules' => 'required|trim|search_table[product_types.id]',
                    ],
                    'production_id' => [
                        'field' => 'production_id',
                        'label' => 'Üretim Yeri',
                        'rules' => 'required|trim|search_table[productions.id]',
                    ],
                    'title' => [
                        'field' => 'title',
                        'label' => 'İsim',
                        'rules' => 'required|trim|xss_clean',
                    ],
                    'desc' => [
                        'field' => 'desc',
                        'label' => 'Açıklama',
                        'rules' => 'trim|xss_clean',
                    ],
                    'bar_code' => [
                        'field' => 'bar_code',
                        'label' => 'Barkod',
                        'rules' => 'trim|xss_clean',
                    ],
                    'type' => [
                        'field' => 'type',
                        'label' => 'Ürün Tipi',
                        'rules' => 'required|trim|in_list[product,package]',
                    ],
                    'sort' => [
                        'field' => 'sort',
                        'label' => 'Sıra',
                        'rules' => 'trim',
                    ],
                    'price' => [
                        'field' => 'price',
                        'label' => 'Fiyat',
                        'rules' => 'required|trim|is_money',
                    ],
                     'bonus' => [
                        'field' => 'bonus',
                        'label' => 'Prim',
                        'rules' => 'trim|is_money',
                    ],
                    'bonus_type' => [
                        'field' => 'bonus_type',
                        'label' => 'Prim Tipi',
                        'rules' => 'required|trim|in_list[value,percent]',
                    ],
                    'color' => [
                        'field' => 'color',
                        'label' => 'Renk',
                        'rules' => 'trim|regex_match[/^#[a-fA-F0-9]{6}$/]',
                    ],
                    'tax' => [
                        'field' => 'tax',
                        'label' => 'Vergi',
                        'rules' => 'required|trim|in_list['.$taxes.']',
                    ],
                    'active' => [
                        'field' => 'active',
                        'label' => 'Durum',
                        'rules' => 'required|trim|in_list[1,0]',
                    ],
                ],
                'update' => [
                    'location_id' => [
                        'field' => 'location_id',
                        'label' => 'Restoran',
                        'rules' => 'required|trim|search_table[locations.id]',
                    ],
                    'product_type_id' => [
                        'field' => 'product_type_id',
                        'label' => 'Kategori',
                        'rules' => 'required|trim|search_table[product_types.id]',
                    ],
                    'production_id' => [
                        'field' => 'production_id',
                        'label' => 'Üretim Yeri',
                        'rules' => 'required|trim|search_table[productions.id]',
                    ],
                    'title' => [
                        'field' => 'title',
                        'label' => 'İsim',
                        'rules' => 'required|trim|xss_clean',
                    ],
                    'desc' => [
                        'field' => 'desc',
                        'label' => 'Açıklama',
                        'rules' => 'trim|xss_clean',
                    ],
                    'bar_code' => [
                        'field' => 'bar_code',
                        'label' => 'Barkod',
                        'rules' => 'trim|xss_clean',
                    ],
                    'type' => [
                        'field' => 'type',
                        'label' => 'Ürün Tipi',
                        'rules' => 'required|trim|in_list[product,package]',
                    ],
                    'sort' => [
                        'field' => 'sort',
                        'label' => 'Sıra',
                        'rules' => 'trim',
                    ],
                    'price' => [
                        'field' => 'price',
                        'label' => 'Fiyat',
                        'rules' => 'required|trim|is_money',
                    ],
                     'bonus' => [
                        'field' => 'bonus',
                        'label' => 'Prim',
                        'rules' => 'required|trim|is_money',
                    ],
                    'bonus_type' => [
                        'field' => 'bonus_type',
                        'label' => 'Prim Tipi',
                        'rules' => 'required|trim|in_list[value,percent]',
                    ],
                    'color' => [
                        'field' => 'color',
                        'label' => 'Renk',
                        'rules' => 'trim|regex_match[/^#[a-fA-F0-9]{6}$/]',
                    ],
                    'tax' => [
                        'field' => 'tax',
                        'label' => 'Vergi',
                        'rules' => 'required|trim|in_list['.$taxes.']',
                    ],
                    'active' => [
                        'field' => 'active',
                        'label' => 'Durum',
                        'rules' => 'required|trim|in_list[1,0]',
                    ],
                ]
            ];

	    	parent::__construct();
	    }

        public function _whereNonDeleted()
        {
            $this->where('active !=', 3);
        }

        public function _setImage($data)
        {
            if ($this->input->post('remove_image')) {
                $data['image'] = null;
                return $data;
            }

            if (!isset($_FILES['image']['name'])) {
                unset($data['image']);
                return $data;
            }

            $path = 'assets/uploads/products/';
            @mkdir(FCPATH . $path);

            $config = [
                'encrypt_name'  => true,
                'upload_path'   => FCPATH . $path,
                'allowed_types' => 'gif|jpg|jpeg|jpe|png|svg',
            ];

            $this->load->library('upload', $config);
            if (!$this->upload->do_upload('image')){
                messageAJAX('error', $this->upload->display_errors());
            }

            $data['image'] = $path . $this->upload->data('file_name');

            return $data;
        }

        public function findAdditionalProducts($where = [])
        {
            $products = $this->db
            ->select('*, NULL As additional_products')
            ->where($where)
            ->where('active !=', 3)
            ->get('products')
            ->result();

            if (!$products)
                return [];


            $product_ids = array_map(function ($e) {
                return $e->id;
            }, $products);

            $related_products = $this->db
            ->where_in('product_id', $product_ids)
            ->get('products_additional_products')
            ->result();

            if (!$related_products)
                return $products;

            $related_product_ids = array_map(function ($e) {
                return $e->additional_product_id;
            }, $related_products);

            $additional_products = $this->db
            ->where_in('id', $related_product_ids)
            ->where('active !=', 3)
            ->get('products')
            ->result();

            if (!$additional_products)
                return $products;

            foreach ($products as &$product) {

                $product->additional_products = [];

                foreach ($additional_products as $additional_product) {
                    if ($product->id == $additional_product->id) {
                        $product->additional_products[] = $additional_product;
                    }
                } 
            }


            return $products;
        }

        public function handleProductRelatedItems($product)
        {
            $product->stocks = $this->ProductHasStocks_M->get_all([
                'product_id' => $product->id
            ]);

            $product->package_products = $this->ProductsHasProducts_M->get_all([
                'package_id' => $product->id
            ]);

            $product->additional_products = $this->ProductsAdditionalProducts_M->get_all([
                'product_id' => $product->id
            ]);

            $product->portion_products = $this->Products_M->get_all([
                'portion_id' => $product->id
            ]);

            $product->quick_notes = $this->ProductsQuickNotes_M->get_all([
                'product_id' => $product->id
            ]);

            return $product;
        }

        public function findProductsWithRelatedItems($where = [], $includeYemekSepeti = false)
        {
            $where = array_merge([
                'products' => [],
                'stocks' => [],
            ], $where);

            $product_stocks = [];
            $package_products = [];
            $additional_products = [];
            $additional_product_stocks = [];
            $package_product_stocks = [];
            $ys_products = [];
            $portion_products = [];


            $products = $this->where($where['products'])->get_all();

            if (!$products)
                return [];

            $product_ids = [];
            $package_ids = [];
            $tmp_products = [];
            foreach ($products as $product) {
                if ($product->type == 'product') {
                    $product_ids[] = $product->id;
                } else if ($product->type == 'package') {
                    $package_ids[] = $product->id;
                }
                $tmp_products[$product->id] = $product;
            }
            $products = $tmp_products;

            // Handle Products and Packages
            if ($product_and_package_ids = array_merge($product_ids, $package_ids)) {
                
                $additional_products = $this->db
                ->where('active !=', 3)
                ->where_in('ProductsAdditionalProducts.product_id', $product_and_package_ids)
                ->join('products_additional_products ProductsAdditionalProducts', 'ProductsAdditionalProducts.additional_product_id = products.id')
                ->get('products')
                ->result();

                if ($includeYemekSepeti) {
                    $ys_products = $this->db
                    ->where_in('product_id', $product_and_package_ids)
                    ->get('ys_products')
                    ->result();
                }

                // Stocks
                if ($additional_products) {
                    
                    $additional_product_ids = array_map(function ($row) {
                        return $row->id;
                    }, $additional_products);

                    $additional_product_stocks = $this->db
                    ->select('stocks.*, ProductHasStocks.product_id As product_id, ProductHasStocks.quantity As product_stock_quantity, ProductHasStocks.optional As product_stock_optional')
                    ->where($where['stocks'])
                    ->where_in('ProductHasStocks.product_id', $additional_product_ids)
                    ->join('product_has_stocks ProductHasStocks', 'ProductHasStocks.stock_id = stocks.id')
                    ->get('stocks')->result();
                }


                foreach ($additional_products as &$additional_product) {
                    
                    $additional_product->stocks = [];
                    foreach ($additional_product_stocks as $additional_product_stock_key => $additional_product_stock) {
                        if ($additional_product_stock->product_id == $additional_product->id) {
                            $additional_product->stocks[$additional_product_stock->id] = $additional_product_stock;
                            unset($additional_product_stocks[$additional_product_stock_key]);
                        }
                    }

                    $additional_product->ys_id = null;
                    foreach ($ys_products as $ys_product_key => $ys_product) {
                        if ($ys_product->add_product_id == $additional_product->id && $ys_product->product_id == $additional_product->product_id) {
                            $additional_product->ys_id = $ys_product->id;
                            unset($ys_products[$ys_product_key]);
                        }
                    }
                }
                unset($additional_product);
            }


            // Handle Products
            if ($product_ids) {

                // Stocks
                $product_stocks = $this->db
                ->select('stocks.*, ProductHasStocks.product_id As product_id, ProductHasStocks.quantity As product_stock_quantity, ProductHasStocks.optional As product_stock_optional')
                ->where($where['stocks'])
                ->where_in('ProductHasStocks.product_id', $product_ids)
                ->join('product_has_stocks ProductHasStocks', 'ProductHasStocks.stock_id = stocks.id')
                ->get('stocks')->result();

                foreach ($product_stocks as &$product_stock) {
                    $product_stock->ys_id = null;
                    foreach ($ys_products as $ys_product_key => $ys_product) {
                        if ($ys_product->stock_id == $product_stock->id && $ys_product->product_id == $product_stock->product_id) {
                            $product_stock->ys_id = $ys_product->id;
                            unset($ys_products[$ys_product_key]);
                        }
                    }
                }
                unset($product_stock);
            }

            
            // Handle Packages
            if ($package_ids) {
                
                $package_products = $this->db
                ->where($where['products'])
                ->where_in('ProductsHasProducts.package_id', $package_ids)
                ->join('products_has_products ProductsHasProducts', 'ProductsHasProducts.product_id = products.id')
                ->get('products')
                ->result();

                // Products
                if ($package_products) {
                    
                    $package_product_ids = array_map(function ($row) {
                        return $row->id;
                    }, $package_products);

                    $package_product_stocks = $this->db
                    ->select('stocks.*, ProductHasStocks.product_id As product_id, ProductHasStocks.quantity As product_stock_quantity, ProductHasStocks.optional As product_stock_optional')
                    ->where($where['stocks'])
                    ->where_in('ProductHasStocks.product_id', $package_product_ids)
                    ->join('product_has_stocks ProductHasStocks', 'ProductHasStocks.stock_id = stocks.id')
                    ->get('stocks')->result();
                }

                foreach ($package_products as &$package_product) {
                    
                    $package_product->stocks = [];
                    foreach ($package_product_stocks as $package_product_stock_key => $package_product_stock) {
                        if ($package_product_stock->product_id == $package_product->id) {
                            $package_product->stocks[$package_product_stock->id] = $package_product_stock;
                        }
                    }
                }
                unset($package_product);
            }



            foreach ($products as &$product) {
                
                $product->additional_products = [];
                foreach ($additional_products as $additional_product_key => $additional_product) {
                    if ($additional_product->product_id == $product->id) {
                        $product->additional_products[$additional_product->id] = $additional_product;
                        unset($additional_products[$additional_product_key]);
                    }
                }

                $product->ys_id = null;
                foreach ($ys_products as $ys_product_key => $ys_product) {
                    if ($ys_product->product_id == $product->id && $this->isMainYSProduct($ys_product)) {
                        $product->ys_id = $ys_product->id;
                        unset($ys_products[$ys_product_key]);
                    } else if ($ys_product->product_id == $product->portion_id && $ys_product->portion_id == $product->id ) {
                        $product->ys_id = $ys_product->id;
                        unset($ys_products[$ys_product_key]);
                    }


                }

                if ($product->type == 'product') {
                    
            
                    $product->stocks = [];

                    foreach ($product_stocks as $product_stock_key => $product_stock) {
                        if ($product_stock->product_id == $product->id) {
                            $product->stocks[$product_stock->id] = $product_stock;
                            unset($product_stocks[$product_stock_key]);
                        }
                    }

                } else if ($product->type == 'package') {

                    $product->products = [];
                    foreach ($package_products as $package_product_key => $package_product) {
                        if ($package_product->package_id == $product->id) {
                            $product->products[$package_product->id] = $package_product;
                            unset($package_products[$package_product_key]);
                        }
                    }
                }

            }
            unset($product);

            return $products;
        }

        public function isMainYSProduct($ys_product)
        {
            return (!$ys_product->portion_id && !$ys_product->add_product_id && !$ys_product->stock_id);
        }
	}    
?>

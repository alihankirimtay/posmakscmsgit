<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Pos extends Auth_Controller {

    public function __construct()
    {
        parent::__construct();

        $this->check_auth([
            'allowed' => ['ajax']
        ]);

        $this->load->model('Pos_M');
        $this->load->model('Transactions_M');
        $this->load->model('Sales_M');
        $this->load->model('Locations_M');
    }

    public function ajax($event = null)
    {
        if(method_exists(__CLASS__, (String) $event) && $this->input->is_ajax_request()) {
            $this->{$event}();
        }

        messageAJAX('error', __('Hatalı istek [404]'));
    }

    public function index($location_id = null)
    {
        if ($location_id) {
            $this->db->where('location_id', $location_id);
        }

        $poses = $this->Pos_M
        ->fields('*')
        ->select('(SELECT title FROM locations where id = pos.location_id) as location_title')
        ->where('location_id', $this->user->locations)
        ->get_all();
       
        $this->theme_plugin = ['start' => 'TablesDataTables.init();'];
        $this->load->template('Pos/index', compact('poses', 'location_id'));
    }

    public function add()
    {
        if ($this->input->is_ajax_request()) {
            if ($this->Pos_M->from_form()->insert()) {
                messageAJAX('success', __('Kasa başarıyla oluşturuldu.'));
            }

            messageAJAX('error', validation_errors());
        }

        $this->load->template('Pos/add');     
    }

    public function edit($id = NULL)
    {
        $pos = $this->Pos_M->where('location_id', $this->user->locations)->findOrFail($id);

        if ($this->input->is_ajax_request()) {
            if ($this->Pos_M->from_form(null, null, ['id' => $id])->update()) {
                messageAJAX('success', __('Kasa başarıyla güncellendi.'));
            }

            messageAJAX('error', validation_errors());
        }

        $this->load->template('Pos/edit', compact('pos'));    
    }

    public function delete($id = NULL)
    {
        $this->Pos_M->where('location_id', $this->user->locations)->findOrFail($id);

        $this->Pos_M->update([
            'active' => 3
        ], ['id' => $id]);

        messageAJAX('success', __('Kasa başarıyla silindi.'));
    }

    private function getPosesByLocationId()
    {
        $location_id = $this->input->post('location_id');

        $poses = $this->Pos_M->where('location_id', $this->user->locations)->get_all([
            'location_id' => $location_id
        ]);

        messageAJAX('success', 'Success', compact('poses'));
    }

    public function detail($id = null)
    {
        $pos = $this->Pos_M->where('location_id', $this->user->locations)->findOrFail($id);
        $types = $this->Transactions_M->getTypes();

        $dFrom = date('Y-m-d H:i:00', strtotime('-24 hours'));
        $dFrom = $this->input->get('d');

        $dTo = date('Y-m-d H:i:59');
        $dTo = $this->input->get('dTo');

        $date['date_from'] = isValidDate($dFrom, dateFormat()) ? dateConvert($dFrom, 'server', 'Y-m-d H:i:00') : date('Y-m-d H:i:00', strtotime('-24 hours'));
        $date['date_to'] = isValidDate($dTo, dateFormat()) ? dateConvert($dTo, 'server', 'Y-m-d H:i:00') : date('Y-m-d H:i:00');

        $transactions = $this->Transactions_M
        ->fields('*')
        ->with_related_transaction([
            'fields' => [
                '*',
                '(
                    CASE 
                        WHEN customer_id IS NOT NULL THEN (SELECT name FROM customers where customers.id = transactions.customer_id)
                        WHEN bank_id IS NOT NULL THEN (SELECT title FROM banks where banks.id = transactions.bank_id)
                        WHEN pos_id IS NOT NULL THEN (SELECT title FROM pos where pos.id = transactions.pos_id)
                    END
                ) as account_title'
            ]
        ])
        ->where('date >=', $date['date_from'])
        ->where('date <=', $date['date_to'])
        ->get_all([
            'pos_id' => $id
        ]);

        
        $sales = $this->Sales_M
        ->fields('id, return_id, payment_date,amount,pos_id, desc, sales_cari.customer_id')
        ->select('(SELECT name from customers where sales_cari.customer_id = customers.id) as account_title')
        ->where('pos_id', $id)
        ->where('payment_date >=', $date['date_from'])
        ->where('payment_date <=', $date['date_to'])
        ->join('sales_cari', 'sales.id = sales_cari.sale_id', 'left')
        ->get_all([
            'status' => 'completed',
            'active' => 1,
        ]);

        $this->theme_plugin = [
            'start' => 'TablesDataTables.init(); MyFunction.datetimepicker("'.dateFormat(TRUE).'");'
        ];

        $this->load->template('Cari/pos_detail', compact('transactions', 'types', 'date', 'sales', 'pos'));
    }
}
<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Printers extends Auth_Controller {

    function __construct()
    {
        parent::__construct();
        $this->check_auth();
        $this->load->model('Printers_model');
    }

    function index($location = NULL)
    {
        $this->theme_plugin = ['start' => 'TablesDataTables.init(); MyFunction.datetimepicker("'.dateFormat(TRUE).'");'];

        $data['current_location'] = (int) $location;

        $data['printers'] = $this->Printers_model->index($data['current_location']);

        // DATES
        if (isset($_GET[ 'dFrom' ])) {
            $data[ 'date_from' ] = isValidDate($_GET[ 'dFrom' ], dateFormat())
                ? dateConvert($_GET[ 'dFrom' ], 'server')
                : date('Y-m-d H:i:s', strtotime('-24 hours'));
        } else {
            $data[ 'date_from' ] = date('Y-m-d H:i:s', strtotime('-24 hours'));
        }

        if (isset($_GET[ 'dTo' ])) {
            $data[ 'date_to' ] = isValidDate($_GET[ 'dTo' ], dateFormat())
                ? dateConvert($_GET[ 'dTo' ], 'server')
                : date('Y-m-d H:i:s', strtotime('-24 hours'));
        } else {
            $data[ 'date_to' ] = date('Y-m-d H:i:s');
        }


        // GET WASTE
        if ($data['current_location']) {

            if(!in_array($data['current_location'], $this->user->locations)) {
                error_('access');
            }

            $this->db->where('date >=', $data[ 'date_from' ]);
            $this->db->where('date <=', $data[ 'date_to' ]);
            $this->db->where('active !=', 3);
            $this->db->where('location_id', $data['current_location']);
            $data['wastes'] = $this->db->get('location_losts')->result();
            // pr($this->db->last_query(),1);
        } else {
            $data['wastes'] = [];
        }


    	$this->load->template('Printers/index', $data);
    }

    function add($location = NULL)
    {
        $data['current_location'] = (int) $location;

        if ($this->input->is_ajax_request()) {

            $this->load->library('form_validation');
            $this->form_validation->set_rules('title', 'Title', 'trim|required|min_length[2]|max_length[45]');
            $this->form_validation->set_rules('desc', 'Description', 'trim');
            $this->form_validation->set_rules('active', 'Status', 'trim|required|integer|in_list[0,1]');
            $this->form_validation->set_rules('location', 'Location', 'trim|required|integer');
            $this->form_validation->set_rules('counter', 'Counter', 'trim|required|is_natural');
            $this->form_validation->set_rules('equal', 'A single printer count', 'trim|required|is_natural');

            if($this->form_validation->run()) {

                $this->Printers_model->add();
            }
            
            messageAJAX('error', validation_errors());

        } else {

            $this->load->template('Printers/add', $data);
        }       
    }

    function edit($location = NULL, $id = NULL)
    {
        $data['current_location'] = (int) $location;
        $id = (int) $id;

        if(!$data['printer'] = $this->exists('printers', "id = $id", '*')) {
            show_404();
        }

        if ($this->input->is_ajax_request()) {
            
            $this->load->library('form_validation');
            $this->form_validation->set_rules('title', 'Title', 'trim|required|min_length[2]|max_length[45]');
            $this->form_validation->set_rules('desc', 'Description', 'trim');
            $this->form_validation->set_rules('active', 'Status', 'trim|required|integer|in_list[0,1]');
            $this->form_validation->set_rules('location', 'Location', 'trim|required|integer');

            if($this->form_validation->run()) {

                $this->Printers_model->edit($id);
            }
            
            messageAJAX('error', validation_errors());

        } else {
            
            $this->Printers_model->edit();

            $this->load->template('Printers/edit', $data);
        }      
    }

    function waste($location = NULL, $id = NULL)
    {
        $data['current_location'] = (int) $location;
        $id = (int) $id;

        if(!$data['waste'] = $this->exists('location_losts', "id = $id AND location_id = ${data['current_location']}", '*')) {
            show_404();
        }
        if(!in_array($data['current_location'], $this->user->locations)) {
            error_('access');
        }

        if ($this->input->is_ajax_request()) {
            
            $this->load->library('form_validation');
            $this->form_validation->set_rules('date', 'Date', 'trim|required|is_date['.dateFormat().']');
            $this->form_validation->set_rules('waste', 'Waste', 'trim|required|is_natural');
            $this->form_validation->set_rules('unsold', 'Unsold', 'trim|required|is_natural');
            $this->form_validation->set_rules('comps', 'Comps', 'trim|required|is_natural');
            $this->form_validation->set_rules('unseen', 'Unseen', 'trim|required|is_natural');

            if($this->form_validation->run()) {

                $this->Printers_model->waste($data['current_location'], $id);
            }
            
            messageAJAX('error', validation_errors());

        } else {

            $this->theme_plugin = ['start' => 'MyFunction.datetimepicker("'.dateFormat(TRUE).'");'];

            $this->load->template('Printers/waste', $data);
        }      
    }

    // function delete($id = NULL)
    // {
    //     $id = (int) $id;

    //     if(!$printer = $this->exists('printers', "id = $id")) {

    //         show_404();
    //     }

    //     $this->Printers_model->delete($id, $printer);
    // }
}
?>

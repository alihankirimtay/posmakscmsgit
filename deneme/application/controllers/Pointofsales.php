<?php defined('BASEPATH') OR exit('No direct script access allowed');


class Pointofsales extends Auth_Controller {

    const cookie = 'stp_1114TeUmRrKaAhN02T0L6a';
    const sound_path = 'assets/uploads/sound_notes';

    private $pos_setup;
    private $pos_session;

    public function __construct()
    {
        parent::__construct();

        $this->check_auth([
            'allowed' => ['ajax']
        ]);

        $this->load->library('Possession', null, 'PosSession');
        $this->load->library('Possale', null, 'PosSale');
        $this->load->library('Yemeksepeti', null, 'YemekSepeti');
        $this->load->library('Yemeksepetihelpers', null, 'YemekSepetiHelpers');

        $this->load->model('Sales_M');
        $this->load->model('Pos_M');
        $this->load->model('Points_M');
        $this->load->model('Floors_M');
        $this->load->model('Zones_M');
        $this->load->model('Products_M');
        $this->load->model('Locations_M');
        $this->load->model('Saleproducts_M', 'SaleProducts_M');
        $this->load->model('Producttypes_M', 'ProductTypes_M');
        $this->load->model('Paymenttypes_M', 'PaymentTypes_M');
        $this->load->model('Saleproductnotes_M', 'SaleProductNotes_M');
        $this->load->model('Customers_M');
        $this->load->model('Productions_M');
        $this->load->model('Sessions_M');
        $this->load->model('Roles_M');
        $this->load->model('Ysproducts_M');

        $this->pos_setup = $this->PosSession->getSetup();
        $this->pos_session = $this->PosSession->getSession();
    }

    // New
    public function index()
    {
        if ($this->pos_setup) {

            if (!isset($this->user->locations_array[$this->pos_setup->location_id])) {
                $this->PosSession->removeSetup();
                $this->PosSession->removeSession();
                redirect('pointofsales');
            }

            $location = $this->user->locations_array[$this->pos_setup->location_id];
            $location_invoice_template = $this->_invoiceTemplate($location['id']);
            $payment_types = $this->PosSale->getPaymentTypes();
            $cash_types = $this->Sessions_M->findCashTypes();
            $couriers = $this->findLocationCouriers();
            $is_mobile = (int) $this->agent->is_mobile();
            $package_service = (int) $this->PosSession->checkPermissionByUserSession($this->pos_session, 'pointofsales.packageserviceorders');
            $yemeksepeti_status = (int) (bool) $this->YemekSepeti->isAvaliable($location['id']);

            if (isset($location['yemeksepeti'])) {
                unset($location['yemeksepeti']);
            }
                
            $location_json = json_encode($location);
            $payment_types_json = json_encode($payment_types);
            $cash_types_json = json_encode($cash_types);
            $pos_session_json = json_encode($this->pos_session);

            $this->theme_plugin = [
                'css' => [
                    'panel/css/view/pointofsales.css',

                    // Keyboard
                    'globals/plugins/keyboard/css/keyboard-dark.min.css'
                ],
                'js' => [
                    'panel/js/view/pointofsales.js', 
                    'globals/plugins/jquery.print/print.js',
                    'globals/plugins/recorderjs/recorder.js',
                    'globals/plugins/jquery-scannerdetection/jquery.scannerdetection.js',

                    // Keyboard
                    'globals/plugins/keyboard/js/jquery.keyboard.min.js',
                    'globals/plugins/keyboard/js/jquery.mousewheel.min.js',
                    'globals/plugins/keyboard/js/jquery.keyboard.extension-extender.min.js',
                    'globals/plugins/keyboard/js/jquery.keyboard.extension-typing.min.js',
                    'globals/plugins/keyboard/languages/tr.js',
                    'globals/plugins/keyboard/layouts/turkish.min.js'
                ],
                'start' => "
                    Pointofsales.init({
                        location: {$location_json},
                        pos_id: '{$this->pos_setup->pos_id}',
                        is_mobile: {$is_mobile},
                        payment_types: {$payment_types_json},
                        cash_types: {$cash_types_json},
                        operator_session: {$pos_session_json},
                        package_service: {$package_service},
                        yemeksepeti_status: {$yemeksepeti_status}
                    });
                "
            ];

            $this->load->template('pointofsales/index', compact('location_invoice_template', 'location', 'payment_types', 'couriers'));
            
        } else {

            $pos_points = $this->Pos_M->fields('id,title,location_id')->where('location_id', $this->user->locations)->get_all([
                'active' => 1
            ]);
            $pos_points_json = json_encode($pos_points);

            $productions = $this->Productions_M->fields('id, title')->get_all(['active' => 1]);
            
            $this->theme_plugin = [
                'css' => ['panel/css/view/pointofsales.css'],
                'js'    => ['panel/js/view/pointofsales.js'],
                'start' => "
                    Pointofsales.Setup.init({
                        pos_points: ${pos_points_json}
                    });"
            ];

            $this->load->template('pointofsales/setup', compact('productions'));
        }
    }

    // New
    public function orders()
    {
        $location_id = $this->input->get('location_id');
        $production_id = $this->input->get('production_id');

        $productions = $this->Productions_M->get_all();

        $this->theme_plugin = [
            'css' => ['panel/css/view/pointofsales.css'],
            'js'    => [
                'globals/plugins/jquery.print/print.js',
                'panel/js/view/pointofsales.js'
            ],
            'start' => "Pointofsales.Orders.init();"
        ];

        $this->load->template('pointofsales/orders', compact('productions', 'location_id', 'production_id'));
    }

    // New
    public function ajax($event = null)
    {
        if(!method_exists(__CLASS__, (String) $event) || !$this->input->is_ajax_request()) {
            messageAJAX('error', __('Hatalı istek [404]'));
        }
        $this->{$event}();
    }

    // New
    private function required(Array $requireds)
    {
        if (in_array('setup', $requireds) && !$this->pos_setup) {
            messageAJAX('error', __('Kasa kurulumu yapınız.'), ['event' => 'requiredSetup']);
        }

        if (in_array('session', $requireds) && !$this->pos_session) {
            messageAJAX('error', __('Oturum açın.'), ['event' => 'requiredSession']);
        }
    }

    // New
    private function getCategoriesWithProducts()
    {   
        $this->db->where('active',1);
        $categories = $this->ProductTypes_M->findCategoriesWithProducts([
            'location_id' => $this->pos_setup->location_id,
        ]);

        messageAJAX('success', 'Success', compact('categories'), 'json');
    }

    // New
    private function getTableWithSale()
    {
        $this->required(['setup', 'session']);

        $location_id = $this->pos_setup->location_id;
        $point_id = $this->input->post('point_id');

        $point = $this->Points_M->findOrFail([
            'id' => $point_id,
            'location_id' => $location_id,
            'active' => 1,
        ], __('Geçersiz Masa.'));

        if ($point->sale_id) {

            $sale = $this->Sales_M
            ->fields(['*', '(SELECT title FROM points where id = point_id) as point_title', '(SELECT title FROM zones where id = zone_id) as zone_title'])
            ->order_by('id', 'desc')
            ->where('status', ['pending', 'shipped'])
            ->findSaleWithProducts([
                'id' => $point->sale_id,
                'location_id' => $location_id,
                'point_id' => $point_id,
                'active' => 1,
            ]);

            if (!$sale) {
                messageAJAX('error', __('Geçersiz Satış'));
            }

            $sale->customer = [];
            if ($sale->customer_id) {
                $sale->customer = $this->Customers_M->get(['id' => $sale->customer_id]);
            }
        }

        messageAJAX('success', 'Success', compact('point', 'sale'), 'json');
    }

    // New
    private function loginForDiscountKey()
    {
        $this->required(['setup', 'session']);

        $location_id = $this->pos_setup->location_id;
        $username = $this->input->post('username');
        $password = $this->input->post('password');
        $discount_amount = $this->input->post('discount_amount');
        $discount_type = $this->input->post('discount_type');
        $login_required = false;

        if (!$this->PosSession->checkPermissionByUserSession($this->pos_session, 'pointofsales.adddiscount')) { 
            $login_required = true;

        }

        $discount_key = $this->PosSession->createDiscount([
            'location_id' => $location_id,
            'username' => $username,
            'password' => $password,
            'discount_amount' => $discount_amount,
            'discount_type' => $discount_type,
        ], $login_required);

        

        if (!$discount_key) {
            messageAJAX('error', $this->PosSession->errorMessage());
        }

        messageAJAX('success', __('İndirim oluşturuldu.'), compact('discount_key', 'discount_amount', 'discount_type'));
    }

    // New
    private function loginForReturnProduct()
    {
        $this->required(['setup', 'session']);

        $location_id = $this->pos_setup->location_id;
        $sale_id = $this->input->post('sale_id');
        $username = $this->input->post('username');
        $password = $this->input->post('password');

         $sale = $this->Sales_M
        ->where('location_id', $this->user->locations)
        ->findOrFail([
            'id' => $sale_id,
            'location_id' => $location_id,
            'status' => 'completed',
            'return_id' => null,
            'active' => 1,
        ], 'Geçersiz satış.');

        

        $session = $this->PosSession->createReturn([
            'location_id' => $location_id,
            'sale_id' => $sale_id,
            'username' => $username,
            'password' => $password,
        ]);

        if (!$session) {
            messageAJAX('error', $this->PosSession->errorMessage());
        }

        messageAJAX('success', __('Giriş Başarılı'), compact('session', 'sale_id'));
    }

    // New
    private function loginForSetup()
    {
        $location_id = $this->input->post('location_id');
        $pos_id = $this->input->post('pos_id');
        $username = $this->input->post('username');
        $password = $this->input->post('password');
        $device = $this->input->post('device');

        $setup = $this->PosSession->createSetup([
            'location_id' => $location_id,
            'pos_id' => $pos_id,
            'username' => $username,
            'password' => $password,
        ]);

        if (!$setup) {
            messageAJAX('error', $this->PosSession->errorMessage());
        }

        messageAJAX('success', __('Kasa kurlumu yapıldı.'), compact('device'));
    }

    // New
    private function loginForOperatorSession()
    {
        $this->required(['setup']);

        $location_id = $this->pos_setup->location_id;
        $password = $this->input->post('password');

        $user = $this->PosSession->createSession([
            'location_id' => $location_id,
            'password' => $password,
        ]);

        if (!$user) {
            messageAJAX('error', $this->PosSession->errorMessage());
        }
        
        $pos_session = $this->PosSession->getSession();

        $package_service = (int) $this->PosSession->checkPermissionByUserSession($pos_session, 'pointofsales.packageserviceorders');

        $this->Sessions_M->createOnAvailableLocationsForUser($user, $location_id);

        messageAJAX('success', __('Otumum açıldı.'), compact('user', 'package_service'));
    }

    // New
    private function logoutForOperatorSession()
    {
        $this->required(['setup']);

        $this->PosSession->removeSession();

        messageAJAX('success', __('Oturum kapatıldı.'));
    }

    // New
    private function checkOperatorSession()
    {
        $this->required(['setup']);

        if ($this->pos_session) {
            messageAJAX('success', 'Success');
        }

        messageAJAX('error');
    }

    // New
    private function removePosSetup()
    {

        if (!$this->PosSession->checkPermissionByUserSession($this->pos_session, 'pointofsales.changepos')) { 
            messageAJAX('error', $this->PosSession->errorMessage());
        }

        $this->PosSession->removeSetup();
        $this->PosSession->removeSession();

        messageAJAX('success', __('Kasa değiştirliyor.'));
    }

    // New
    private function getSaleForReturn()
    {
        $this->required(['setup', 'session']);

        $location_id = $this->pos_setup->location_id;
        $sale_id = $this->input->post('sale_id');
        $session_key = $this->input->post('session');

        $session = $this->PosSession->getReturn();
        if (!$session || $session != $session_key) {
            messageAJAX('error', __('Geçersiz oturum.'));
        }

        $sale = $this->Sales_M
        ->where('location_id', $this->user->locations)
        ->order_by('id', 'desc')
        ->findSaleWithProducts([
            'id' => $sale_id,
            'location_id' => $location_id,
            'status' => 'completed',
            'return_id' => null,
            'active' => 1,
        ])
        OR messageAJAX('error', __('Geçersiz satış.'));

        messageAJAX('success', 'Success', compact('sale'), 'json');
    }

    // New
    private function getFloorsWithZones()
    {
        $this->required(['setup', 'session']);

        $location_id = $this->pos_setup->location_id;
        $user_id = $this->pos_session->user_id;
        $user_role = $this->pos_session->user_role;

        $where_zones = [];
        $zone_ids = [0];
        if ($user_role != 'admin') {
            if ($user_zones = $this->Zones_M->findZonesByUser($user_id)) {
                $zone_ids = array_map(function ($row) { return $row->id; }, $user_zones);
            }

            $where_zones = [
                'where_in' => [
                    'id' => $zone_ids
                ]
            ];
        }

        $floors = $this->Floors_M->fields('id, title')
        ->with_zones([
            'where' => [
                'location_id' => $location_id,
                'active' => 1,
            ],
            'order_inside' => 'title asc'
        ] + $where_zones)
        ->where('location_id', $this->user->locations)
        ->order_by('title', 'asc')
        ->get_all([
            'location_id' => $location_id,
            'active' => 1
        ]);

        messageAJAX('success', 'Success', compact('floors'));
    }

    // New
    private function getZonePoints()
    {
        $this->required(['setup', 'session']);

        $location_id = $this->pos_setup->location_id;
        $floor_id = $this->input->get('floor_id');
        $zone_id = $this->input->get('zone_id');

        $points = $this->Points_M
        ->fields('*')
        ->select('(SELECT amount FROM sales WHERE id = sale_id LIMIT 1) As sale_amount')
        ->select('(SELECT interim_payment_amount FROM sales WHERE id = sale_id LIMIT 1) As interim_payment_amount')
        ->where('location_id', $this->user->locations)
        ->get_all([
            'zone_id' => $zone_id,
            'floor_id' => $floor_id,
            'location_id' => $location_id,
            'service_type' => 1,
            'active' => 1,
        ]);

        messageAJAX('success', 'Success', compact('points'));
    }

    // New
    private function getServicePoint()
    {
        $this->required(['setup', 'session']);

        $location_id = $this->pos_setup->location_id;
        $service_type = $this->input->get('service_type');

        if (!in_array($service_type, ['self', 'package'])) {
            messageAJAX('error', __('Hatalı servis isteği.'));
        }

        $point = $this->PosSale->getServicePoint($location_id, $service_type);

        messageAJAX('success', 'Success', compact('point'));
    }

   

    private function getProductPortions()
    {
        $this->required(['setup', 'session']);

        $location_id = $this->pos_setup->location_id;
        $product_id = $this->input->get('product_id');

        $products = $this->Products_M
        ->group_start()
            ->where('id', $product_id)
            ->or_where('portion_id', $product_id)
        ->group_end()
        ->where('location_id', $this->user->locations)
        ->order_by('portion_id', 'asc')
        ->get_all([
            'location_id' => $location_id,
            'active' => 1
        ]);

        if ($products) {
            
            $product_ids = array_map(function ($row) {
                return $row->id;
            }, $products);


            $additional_products = $this->db
            ->where('products.active', 1)
            ->where_in('products_additional_products.product_id', $product_ids)
            ->join('products_additional_products', 'products_additional_products.additional_product_id = products.id')
            ->get('products')
            ->result();

            $stocks = $this->db
            ->select('stocks.*, product_has_stocks.product_id, product_has_stocks.quantity product_stock_quantity, product_has_stocks.optional')
            ->where('stocks.active', 1)
            ->where('product_has_stocks.optional', 1)
            ->where_in('product_has_stocks.product_id', $product_ids)
            ->join('product_has_stocks', 'product_has_stocks.stock_id = stocks.id')
            ->get('stocks')
            ->result();

            $package_products = $this->db
            ->where_in('products_has_products.package_id', $product_ids)
            ->join('products_has_products', 'products_has_products.product_id = products.id')
            ->get('products')
            ->result();

            $quick_notes = $this->db
            ->select('quick_notes.*, products_quick_notes.product_id')
            ->where('quick_notes.active', 1)
            ->where('quick_notes.deleted', null)
            ->where_in('products_quick_notes.product_id', $product_ids)
            ->join('products_quick_notes', 'products_quick_notes.quick_note_id = quick_notes.id')
            ->get('quick_notes')
            ->result();

            $data = [];
            foreach ($products as $product) {

                $product->additional_products = [];
                $product->stocks = [];
                $product->quick_notes = [];

                // Additional products
                foreach ($additional_products as $additional_product) {
                    if ($product->id == $additional_product->product_id) {
                        $product->additional_products[$additional_product->id] = $additional_product;
                    }
                }

                // Stocks
                foreach ($stocks as $stock) {
                    if ($product->id == $stock->product_id) {
                        $product->stocks[$stock->id] = $stock;
                    }
                }

                // Quick Notes
                foreach ($quick_notes as $quick_note) {
                    if ($product->id == $quick_note->product_id) {
                        $product->quick_notes[$quick_note->id] = $quick_note;
                    }
                }

                if ($product->type == "package") {
                    foreach ($package_products as $package_product) {
                        if ($product->id == $package_product->package_id) {
                            $product->products[$package_product->product_id] = $package_product;
                        }   
                    }
                }

                $data[$product->id] = $product;
            }

            $products = $data;
        }

        messageAJAX('success', 'Success', compact('products'));
    }

    // New 
    private function complete()
    {
        $this->required(['setup', 'session']);

        $products = (array) $this->input->post('products[]');
        $payments = (array) $this->input->post('payments[]');
        $action = (string) $this->input->get('action');
        $sale_id = (int) $this->input->post('sale_id');
        $location_id = $this->pos_setup->location_id;
        $pos_id = $this->pos_setup->pos_id;
        $point_id = (int) $this->input->post('point_id');
        $customer_id = (int) $this->input->post('customer_id');
        $user_id = $this->pos_session->user_id;
        $completer_user_id = $this->pos_session->user_id;
        $discount_amount = 0;
        $discount_type = null;
        $discount_remove = false;
        $force_product_decrement = false;
        $datetime = date('Y-m-d H:i:s');
        $discount_key = (string) $this->input->post('discount_key');

        if ($discount_key) {
            $discount = $this->PosSession->checkDiscount($discount_key);
            if (!$discount) {
                messageAJAX('error', $this->PosSession->errorMessage());
            }

            $discount_amount = $discount->discount_amount;
            $discount_type = $discount->discount_type;
            $discount_remove = ((int) $discount_amount === 0);
        }

        if ($action == 'checkout') {
            if (!$this->PosSession->checkPermissionByUserSession($this->pos_session, 'pointofsales.check')) { 
                messageAJAX('error', $this->PosSession->errorMessage());
            }
        }

        $force_product_decrement = (bool) $this->PosSession->checkPermissionByUserSession($this->pos_session, 'pointofsales.forceproductdecrement');

        $sale = $this->PosSale
        ->setProducts($products)
        ->setPayments($payments)
        ->setAction($action)
        ->setSaleId($sale_id)
        ->setLocationId($location_id)
        ->setPosId($pos_id)
        ->setPointId($point_id)
        ->setCustomerId($customer_id)
        ->setUserId($user_id)
        ->setCompleterUserId($completer_user_id)
        ->setDiscountAmount($discount_amount)
        ->setDiscountType($discount_type)
        ->setDiscountRemove($discount_remove)
        ->setForceProductDecrement($force_product_decrement)
        ->setPaymentDate($datetime)
        ->processSale();

        if (!$sale) {
            messageAJAX('error', $this->PosSale->errorMessage());
        }

        if ($discount_key) {
            $this->PosSession->removeDiscount();
        }

        $message = $sale->message;
        $sale_id = $sale->sale_id;
        $inserted_product_ids = $sale->inserted_product_ids;
        $product_calculations = $sale->product_calculations;
        if (isset($sale->new_sale_id))
            $new_sale_id = $sale->new_sale_id;


        $sale = $this->Sales_M->findSaleWithProducts(['id' => $sale_id]);
        $sale->payment_date_client = dateConvert($datetime, 'client', dateFormat());

        $sale->customer = [];
        if ($sale->customer_id) {
            $sale->customer = $this->Customers_M->get(['id' => $sale->customer_id]);
        }

        messageAJAX('success', $message, compact('status', 'new_sale_id', 'sale_id', 'inserted_product_ids', 'product_calculations', 'sale'), 'json');
    }

    // New
    private function cancelSale() 
    {
        $this->required(['setup', 'session']);

        $sale_id = $this->input->post('sale_id');
        $point_id = $this->input->post('point_id');
        $location_id = $this->pos_setup->location_id;

       

        $result = $this->PosSale
        ->setLocationId($location_id)
        ->setSaleId($sale_id)
        ->setPointId($point_id)
        ->cancelSale();

        if (!$this->PosSession->checkPermissionByUserSession($this->pos_session, 'pointofsales.tablecancel')) { 
            messageAJAX('error', $this->PosSession->errorMessage());
        }

         if (!$result) {
            messageAJAX('error', $this->PosSale->errorMessage());
        }

        messageAJAX('success', __('Masa iptal edildi.'));

    }

    // New
    private function completeInterimPayment()
    {
        $this->required(['setup', 'session']);

        $products = (Array) $this->input->post('products[]');
        $sale_id = $this->input->post('sale_id');
        $location_id = $this->pos_setup->location_id;
        $point_id = $this->input->post('point_id');
        $payment_type = $this->input->post('payment_type');

        $interim_payment_amount = $this->PosSale
        ->setLocationId($location_id)
        ->setSaleId($sale_id)
        ->setPointId($point_id)
        ->setPaymentType($payment_type)
        ->setProducts($products)
        ->makeInterimPayment();

        if (!$this->PosSession->checkPermissionByUserSession($this->pos_session, 'pointofsales.interimpayment')) { 
            messageAJAX('error', $this->PosSession->errorMessage());
        }

        if (!$interim_payment_amount) {
            messageAJAX('error', $this->PosSale->errorMessage());
        }

        messageAJAX('success', __('Ara ödeme alındı.'), compact('interim_payment_amount'));
    }

    // New
    private function completeReturn()
    {
        $this->required(['setup', 'session']);

        $products = $this->input->post('products[]');
        $sale_id = $this->input->post('sale_id');
        $location_id = $this->pos_setup->location_id;
        $pos_id = $this->pos_setup->pos_id;
        $user_id = $this->pos_session->user_id;
        $payment_type = $this->input->post('payment_type');
        $session_key = $this->input->post('session');


        $session = $this->PosSession->getReturn();
        if (!$session || $session_key != $session) {
            messageAJAX('error', __('Geçersiz indirim.'));
        }

        $return_sale_id = $this->PosSale
        ->setProducts($products)
        ->setSaleId($sale_id)
        ->setLocationId($location_id)
        ->setPosId($pos_id)
        ->setUserId($user_id)
        ->setPaymentType($payment_type)
        ->processReturn();

        if (!$return_sale_id) {
            messageAJAX('error', $this->PosSale->errorMessage());
        }

        if (!$this->PosSession->checkPermissionByUserSession($this->pos_session, 'pointofsales.return')) { 
            messageAJAX('error', $this->PosSession->errorMessage());
        }

        $this->PosSession->removeReturn();

        $return_sale = $this->Sales_M->findSaleWithProducts([
            'id' => $return_sale_id,
        ]);
        $return_sale->payment_date_client = dateConvert($return_sale->created, 'client', dateFormat());

        messageAJAX('success', __('İade alındı.'), compact('return_sale'), 'json');


        /*$amount = 0;
        $discount_amount = 0;
        $discount_type = 0;
        $datetime = date('Y-m-d H:i:s');

        $discount_total = 0;

        if (!$payment_type_id) {
            messageAJAX('error', 'Ödeme tipini seçiniz.');
        }
        if ($this->session->userdata('return_session') != $session) {
            messageAJAX('error', 'Geçersiz oturum.');
        }

        $sale = $this->Sales_M
        ->order_by('id', 'desc')
        ->findSaleWithProducts([
            'id' => $sale_id,
            'location_id' => $location_id,
            'status' => 'completed',
            'return_id' => null,
            'active' => 1,
        ])
        OR messageAJAX('error', 'Geçersiz satış.');


        $requested_products = $this->filterRequestedReturnProducts();
        $products = $this->handleRequestedReturnProducts($requested_products, $sale->products);
        $product_calculations = $this->calculateRequestedProducts($products);

        $discount = $this->calculateSaleDiscount($sale, $product_calculations->total);
        $discount_total = $discount->discount_total;
        $discount_amount = $discount->discount_amount;
        $discount_type = $discount->discount_type;

        $amount = $product_calculations->total - $discount_total;

        $_POST = array_merge($_POST, [
            'return_id' => $sale->id,
            'point_id' => $sale->point_id,
            'pos_id' => $pos_id,
            'floor_id' => $sale->floor_id,
            'zone_id' => $sale->zone_id,
            'user_id' => $this->operator_session->user_id,
            'customer_id' => $sale->customer_id,
            'completer_user_id' => $this->operator_session->user_id,
            'sale_type_id' => 1,
            'amount' => $amount,
            'subtotal' => $product_calculations->subtotal,
            'taxtotal' => $product_calculations->taxtotal,
            'gift_amount' => 0,
            'interim_payment_amount' => 0,
            'discount_amount' => $discount_amount,
            'discount_type' => $discount_type,
            'change' => 0,
            'payment_type_id' => $payment_type_id,
            'payment_date' => $datetime,
            'status' => 'completed',
            'active' => 1,
        ]);

        foreach ($this->PosSale->getPaymentTypes() as $_payment_type) {
            $refund = ($_payment_type->alias == $payment_type) ? $amount : 0;
            $_POST[$_payment_type->alias] = $refund;
        }

        if (!$return_sale_id = $this->Sales_M->rulesPaymetTypes()->from_form()->insert()) {
            messageAJAX('error' , validation_errors());
        }

        $this->Sales_M->update(['return' => 1], ['id' => $sale->id]);

        $this->processProducts($products, $return_sale_id, $datetime);
        $this->processReturnedProducts($products);

        $return_sale = $this->Sales_M->findSaleWithProducts([
            'id' => $return_sale_id,
        ]);
        $return_sale->payment_date_client = dateConvert($datetime, 'client', dateFormat());

        $this->session->unset_userdata('return_session');

        messageAJAX('success', 'İade alındı.', compact('return_sale'), 'json');*/
    }

    // New
    private function createGift()
    {
        $this->required(['setup', 'session']);

        $products = (Array) $this->input->post('products[]');
        $location_id = $this->pos_setup->location_id;
        $user_id = $this->pos_session->user_id;
        $sale_id = $this->input->post('sale_id');
        $point_id = $this->input->post('point_id');

        $result = $this->PosSale
        ->setLocationId($location_id)
        ->setUserId($user_id)
        ->setSaleId($sale_id)
        ->setPointId($point_id)
        ->setProducts($products)
        ->makeProductsAsGift();

        if (!$result) {
            messageAJAX('error', $this->PosSale->errorMessage());
        }

        messageAJAX('success', __('Ürünler ikram edildi.'));
    }

    // New
    private function mergeTables()
    {
        $this->required(['setup', 'session']);

        $location_id = $this->pos_setup->location_id;
        $source_point_id = $this->input->post('point_id');
        $target_point_id = $this->input->get('target_point_id');

        $target_sale_id = $this->PosSale
        ->setLocationId($location_id)
        ->moveAndMergeTables($source_point_id, $target_point_id);

        if (!$this->PosSession->checkPermissionByUserSession($this->pos_session, 'pointofsales.tablemove')) { 
            messageAJAX('error', $this->PosSession->errorMessage());
        }

        if (!$target_sale_id) {
            messageAJAX('error', $this->PosSale->errorMessage());
        }

        messageAJAX('success', __('Masalar taşındı.'), compact('target_sale_id'));
    }

    // New
    private function findCustomer()
    {
        $this->required(['setup', 'session']);

        $phone = trim($this->input->post('phone'));
        $name = trim($this->input->post('name'));
        $field = $this->input->get('field');

        if (empty(${$field})) {
            messageAJAX('error', __('Geçersiz alan.'));
        }
        if (($field == 'phone' && strlen($phone) < 3) || ($field == 'name' && strlen($name) < 3)) {
            messageAJAX('error', __('Geçersiz uzunluk.'));
        }

        $customers = $this->Customers_M
        ->fields('id,name,phone,address')
        ->like($field, ${$field})
        ->get_all([
            'active' => 1,
        ]);

        messageAJAX('success', 'Success', compact('customers'), "json");
    }

    // New
    private function addCustomer()
    {
        $this->required(['setup', 'session']);
        
        $_POST['active'] = 1;
        $_POST['isSupplier'] = 0;

        if (!$id = $this->Customers_M->from_form()->insert()) {
            messageAJAX('error', validation_errors());
        }

        $customer = $this->Customers_M->get($id);

        messageAJAX('success', __('Müşteri eklendi.'), compact('customer'), "json");
    }

    // New
    public function saveSoundNote()
    {
        $this->required(['setup', 'session']);

        $_POST['type'] = 2;
        $_POST['viewed'] = 0;

        if (isset($_FILES['sound']))
            if ($_FILES['sound']['name'] == 'blob')
                $_FILES['sound']['name'] = 'blob.wav';
        
        if (!$id = $this->SaleProductNotes_M->from_form()->insert()) {
            messageAJAX('error', validation_errors());
        }

        $sound_note = $this->SaleProductNotes_M->get($id);

        messageAJAX('success', 'Success', compact('sound_note'));
    }

    // New
    private function getOrders()
    {
        $location_id = (Int) $this->input->post('location_id');
        $production_id = (int) $this->input->post('production_id');
        $status = $this->input->post('status');

        switch ($status) {
            case 'completed-today':
                $date_start = dateConvert2(date('Y-m-d 00:00:00'), 'server');
                $date_end = dateConvert2(date('Y-m-d 23:59:59'), 'server');
                $this->db->where('status', 'completed');
                break;
            case 'completed':
                $date_start = dateConvert2(date('Y-m-d 00:00:00', strtotime('-7 day')), 'server');
                $date_end = dateConvert2(date('Y-m-d 23:59:59', strtotime('-1 day')), 'server');
                $this->db->where('status', 'completed');
                break;
            case 'pending':
            default:
                $date_start = date('Y-m-d H:i:s', strtotime('-6 HOURS'));
                $date_end = date('Y-m-d H:i:s');
                $this->db->where_in('status', ['pending', 'completed']);
                break;
        }
        

        $this->db
        ->select('(select title from points where id = point_id) As point_title')
        ->select('(select title from zones where id = zone_id) As zone_title')
        ->where_in('location_id', $this->user->locations);
        $sales = $this->Sales_M->fields('*')->findSalesWithProducts([
            'location_id' => $location_id,
            'created >=' => $date_start,
            'created <=' => $date_end,
            'return_id' => NULL
        ], [
            'sale_has_products' => [
                'fields' => '*, (select username from users where id = user_id) As user_name',
                'raw_where' => "`production_id` LIKE IF(`type` = 'product', {$production_id}, '%')",
            ],
            'package_has_products' => [
                'where' => [
                    'production_id' => $production_id,
                ],
            ],
            'sale_product_notes' => [
                'fields' => 'note,sound,type,created,viewed',
                'where' => [
                    'deleted' => null
                ]
            ]
        ]);

        if ($sales) {

            $customer_ids = [];
            foreach ($sales as $key => $sale) {

                $sales[$key]->customer = [];

                if ($sale->customer_id) {
                    $customer_ids[$sale->customer_id] = $sale->customer_id;
                }
            }

            if ($customer_ids) {

                $customers = $this->Customers_M->where('id', $customer_ids)->get_all();

                foreach ($sales as $key_sale => $sale) {
                    if ($sale->customer_id) {
                        foreach ($customers as $customer) {
                            if ($sale->customer_id == $customer->id) {
                                $sale->customer = $customer;
                            }
                        }
                    }
                    $sales[$key_sale] = $sale;
                }
            }
            
        }

        messageAJAX('success', 'Success', compact('sales'));
    }

    // New

    private function getNoteViewed()
    {
        $location_id = $this->input->get('location_id');
        $sale_id = $this->input->get('sale_id');
        $product_id = $this->input->get('product_id');
        $notes_id = $this->input->get('notes_id');

        $notes_id = (array) json_decode($notes_id);

        if (!$sale_id) {
            messageAJAX('error', __('Geçersiz satış.'));
        }

        if (!$notes_id) {
            messageAJAX('error', __('Not bulunamadı'));
        }

        $this->db->where('sale_product_id', $product_id)
        ->where_in('id', $notes_id)
        ->update('sale_product_notes', [
            'viewed' => 1,
        ]);

        messageAJAX('success', 'Success');

    }

    // New
    private function orderedProductCompleted()
    {
        $location_id = $this->input->get('location_id');
        $sale_id = $this->input->get('sale_id');
        $product_id = $this->input->get('product_id');
        $production_id = $this->input->get('production_id');
        $datetime = date('Y-m-d H:i:s');

        $sale = $this->Sales_M
        ->where('location_id', $this->user->locations)
        ->with_products([
            'where' => [
                'id' => $product_id,
                'active' => 1,
            ],
            'limit' => 1
        ])
        ->findOrFail([
            'id' => $sale_id,
            'location_id' => $location_id,
            'status !=' => 'cancelled',
            'return_id' => null,
            'active' => 1,
        ], 'Geçersiz satış.');

        if (!isset($sale->products)) {
            messageAJAX('error', __('Geçersiz ürün.'));
        }

        $product = current($sale->products);
        if ($product->type == 'product') {

            $rest = $product->quantity - $product->completed;
            if ($rest < 1) {
                messageAJAX('error', __('Sipariş zaten hazır.'));
            }

            $this->SaleProducts_M->update([
                'completed' => $product->completed + $rest,
            ], ['id' => $product_id]);

        } else {

            $package_products = $this->db
            ->where([
                'package_id' => $product_id,
                'production_id' => $production_id,
                'active' => 1,
            ])
            ->get('package_has_products')->result();

            if ($package_products) {

                foreach ($package_products as &$package_product) {

                    $package_product->rest = $package_product->quantity - $package_product->completed;
                    if ($package_product->rest < 1) {
                        messageAJAX('error', __('Sipariş zaten hazır.'));
                    }
                }
                unset($package_product);

                foreach ($package_products as $package_product) {
                    
                    $this->db->update('package_has_products', [
                        'completed' => $package_product->completed + $package_product->rest,
                        'modified' => $datetime,
                    ], ['id' => $package_product->id]);
                }

                // Use biggest completed menu product number for menu completed.
                $this->SaleProducts_M->update([
                    'completed' => $package_product->completed + $package_product->rest,
                ], ['id' => $product_id]);

            }
        }

        messageAJAX('success', 'Success');
    }

    // New
    private function getSaleForOrderInvoice()
    {
        $location_id = $this->input->get('location_id');
        $sale_id = $this->input->get('sale_id');

        $this->db
        ->select('(select username from users where id = user_id) As username')
        ->select('(select title from points where id = point_id) As point_title')
        ->select('(select title from zones where id = zone_id) As zone_title')
        ->where_in('location_id', $this->user->locations);
        $sale = $this->Sales_M->fields('*')->findSaleWithProducts([
            'id' => $sale_id,
            'location_id' => $location_id,
            'status !=' => 'canceled',
        ], [
            'sale_product_notes' => [
                'fields' => 'note,sound,type,created,viewed',
                'where' => [
                    'deleted' => null
                ]
            ]

            ]);

        if (!$sale) {
            messageAJAX('error', __('Geçersiz satış.'));
        }

        $sale->customer = [];
        if ($sale->customer_id) {
            $sale->customer = $this->Customers_M->get(['id' => $sale->customer_id]);
        }

        messageAJAX('success', 'Success', compact('sale'), 'json');
    }

    // New
    private function approveSession()
    {
        $this->required(['setup', 'session']);

        $this->load->library('Poscashclosing', null, 'PosCashClosing');

        $validation = $this->form_validation
        ->set_rules('manager-username' , 'Username or Email', 'trim|required')
        ->set_rules('manager-password' , 'Password', 'trim|required');
        if (!$validation->run()) {
            messageAJAX('error' , validation_errors());
        }

        $username = $this->input->post('manager-username', TRUE);
        $password = $this->input->post('manager-password', TRUE);
        $manager = $this->PosSession->checkPermission($username, $password, 'pointofsales.approvesession');
        if (!$manager) {
            messageAJAX('error', $this->PosSession->errorMessage());
        }

        $result = $this->PosCashClosing
        ->setLocationId($this->pos_setup->location_id)
        ->setUserId($this->pos_session->user_id)
        ->setManagerId($manager->id)
        ->setPayments($this->input->post('payments[]'))
        ->setCashs($this->input->post('cashs[]'))
        ->setCashOpening($this->input->post('cash_opening'))
        ->approve();

        if (!$result) {
            messageAJAX('error', $this->PosCashClosing->errorMessage());
        }

        $message = $result->message;
        $status = $result->status;
        $session = $result->session;

        if ($status) {
            $session = $this->Sessions_M->get($session->id);
            $this->PosSession->removeSession();
            $this->session->unset_userdata('loggedin');
        }

        messageAJAX('success', $message, compact('status', 'session'));
    }

    // New
    private function getCompletedUnDeliveredProducts()
    {
        $this->required(['setup', 'session']);

        $products = $this->PosSale
        ->setLocationId($this->pos_setup->location_id)
        ->setUserId($this->pos_session->user_id)
        ->getCompletedUnDeliveredProducts();

        $call_accounts = $this->PosSale->getActiveCallAccounts();


        messageAJAX('success', 'Success', compact('products', 'call_accounts'), 'json');
    }

    // New
    private function productDelivered()
    {
        $this->required(['setup', 'session']);

        $result = $this->PosSale
        ->setLocationId($this->pos_setup->location_id)
        ->setSaleId($this->input->get('sale_id'))
        ->setProductId($this->input->get('product_id'))
        ->productDelivered();

        if (!$result) {
            messageAJAX('error', $this->PosSale->errorMessage());
        }

        messageAJAX('success', 'Success');
    }

    private function setViewedCallAccount()
    {

        $result = $this->PosSale
        ->setLocationId($this->pos_setup->location_id)
        ->setSaleId($this->input->get('sale_id'))
        ->setViewedCallAccount();

        if (!$result) {
            messageAJAX('error', $this->PosSale->errorMessage());
        }

        messageAJAX('success', 'Success');

    }

    // New
    private function getPackageServiceOrders()
    {
        $this->required(['setup', 'session']);
        
        $location_id = $this->pos_setup->location_id;
        $date_start = date('Y-m-d H:i:s', strtotime('-6 HOURS'));

        $service_type = $this->Points_M->findServiceTypeByType('package');
    
        $sales = $this->Sales_M
        ->fields('*')
        ->select('(SELECT CONCAT_WS("{SEPERATOR}", name, phone, address) FROM customers WHERE id = customer_id) AS customer')
        ->select('(SELECT name FROM users WHERE id = courier_id) AS courier_name')
        ->where('status', ['pending', 'shipped'])
        ->get_all([
            'location_id' => $location_id,
            'created >=' => $date_start,
            'service_type' => $service_type->id,
        ]);


        messageAJAX('success', 'Success', compact('sales'));
    }

    private function getYemekSepetiOrders()
    {
        $this->required(['setup', 'session']);

        if ($this->YemekSepeti->initByLocationId($this->pos_setup->location_id)) {

            $order_ids = [];
            $sales_ys = $this->YemekSepeti->GetAllMessages();

            foreach ($sales_ys['items'] as $sale_id) {

                $order_ids[] = $sale_id->{'@attributes'}->Id;
                # code...
            }

            $complete_ys_sales = $this->db->select('ys_order_id')->where_in(['ys_order_id', $order_ids])->get('sales_ys_orders')->result();

            messageAJAX('success', 'Success', compact('sales_ys', 'complete_ys_sales'));

        }

    }

    private function rejectYsOrders()
    {
        if ($this->YemekSepeti->initByLocationId($this->pos_setup->location_id)) {

            $note = $this->input->post('note');
            $order_id = $this->input->post('order_id');

            $result = $this->YemekSepeti->UpdateOrder($order_id, "Cancelled", $note);

            messageAJAX('success', 'Success', compact('result'));

        }

    }

    private function acceptYsOrders() {

        if ($this->YemekSepeti->initByLocationId($this->pos_setup->location_id)) {

            $order_id = $this->input->post('order_id');
            $sale_id = $this->input->get('sale_id');
            $result = $this->YemekSepeti->UpdateOrder($order_id, "Accepted", null);

            $ys_sale = [];

            $ys_sale = [
                'sale_id' => $sale_id,
                'ys_order_id' => $order_id
            ];

            $this->db->insert('sales_ys_orders', $ys_sale);

            messageAJAX('success', 'Success', compact('result'));

        }

    }

    private function getYsOrdersProducts()
    {       
        $ys_products = $this->input->post('products');
        $ys_products = json_decode($ys_products);

        $system_products = $this->db->get('ys_products')->result();

        $products = $this->YemekSepetiHelpers->filteredYsProducts($ys_products);

        $sync_products = $this->handleYsProductSync($products, $system_products);

        messageAJAX('success', 'Success', compact('sync_products'), 'json');
    }

    private function getSaleYsProduct()
    { 

        $location_id = $this->pos_setup->location_id;
        $service_type = "package";
        $_POST['ys_id'] = (int) $this->input->post('customer_id');
        $customer_id = 0;

        $customer = $this->Customers_M->get([
                'ys_id' => $_POST['ys_id'],
                'active' => 1
        ]);

        if (!$customer) {

            $data = [
                'name' => $this->input->post('name'),
                'phone' => $this->input->post('phone'),
                'address' => $this->input->post('address'),
                'ys_id' => $this->input->post('ys_id'),
                'active' => 1
            ];
            $customer_id = $this->Customers_M->insert($data);

        } else {
            $customer_id = $customer->id;
        }

        $point = $this->PosSale->getServicePoint($location_id, $service_type);

        $point_id = (int) $point->id;

        messageAJAX('success', 'Success', compact('point_id', 'customer_id'));

    }

    // New
    private function shippingOrder()
    {
        $this->required(['setup', 'session']);

        $location_id = $this->pos_setup->location_id;
        $point_id = $this->input->post('point_id');
        $courier_id = $this->input->post('courier_id');
        $service_type = $this->Points_M->findServiceTypeByType('package');

        $point = $this->Points_M->where('location_id', $this->user->locations)->findOrFail([
            'id' => $point_id,
            'location_id' => $location_id,
            'active' => 1,
        ], __('Geçersiz Masa.'));

        if (!$point->sale_id) {
            messageAJAX('error', __('Geçersiz Satış'));
        }

        $sale = $this->Sales_M
        ->order_by('id', 'desc')
        ->get([
            'id' => $point->sale_id,
            'location_id' => $location_id,
            'point_id' => $point_id,
            'service_type' => $service_type->id,
            'status' => 'pending',
            'active' => 1,
        ]);

        if (!$sale) {
            messageAJAX('error', __('Geçersiz Satış'));
        }

        $this->Sales_M->update([
            'status' => 'shipped',
            'courier_id' => $courier_id,
        ], $sale->id);       

        messageAJAX('success', 'Success');
    }

    // New
    protected function _invoiceTemplate($location_id)
    {
        if ($invoice_template = $this->db->where('location_id', $location_id)->get('invoice_template', 1)->row()) {
            return $invoice_template;
        }

        return (Object) [
            'logo' => null,
            'header' => null,
            'footer' => null,
        ];
    }

    // New
    protected function findLocationCouriers()
    {
        $location = $this->Locations_M->with_users([
            'active' => 1
        ])->get([
            'id' => $this->pos_setup->location_id
        ]);

        if (empty($location->users)) {
            return [];
        }

        $user_ids = [];
        foreach ($location->users as $user) {
            $user_ids[] = $user->id;
        }

        $couriers = $this->Roles_M
        ->with_users([
            'where' => [
                'active' => 1,
            ],
            'where_in' => [
                'id' => $user_ids,
            ]
        ])
        ->get(['alias' => 'courier']);

        if (empty($couriers->users)) {
            return [];
        }

        return $couriers->users;
    }

    // Eski yapıdan kalmış, düzeltilmesi gerekiyor.
    private function findProductPortions()
    {
        $product_id = (int) $this->input->get('product_id');

        $products = $this->Products_M
        ->group_start()
            ->where('id', $product_id)
            ->or_where('portion_id', $product_id)
        ->group_end()
        ->order_by('portion_id', 'asc')
        ->get_all([
            'active' => 1
        ]);

        if ($products) {
            
            $product_ids = array_map(function ($row) {
                return $row->id;
            }, $products);

            $additional_products = $this->db
            ->where('products.active', 1)
            ->where_in('products_additional_products.product_id', $product_ids)
            ->join('products_additional_products', 'products_additional_products.additional_product_id = products.id')
            ->get('products')
            ->result();

            $stocks = $this->db
            ->select('stocks.*, product_has_stocks.product_id, product_has_stocks.quantity product_stock_quantity, product_has_stocks.optional')
            ->where('stocks.active', 1)
            ->where('product_has_stocks.optional', 1)
            ->where_in('product_has_stocks.product_id', $product_ids)
            ->join('product_has_stocks', 'product_has_stocks.stock_id = stocks.id')
            ->get('stocks')
            ->result();
   
            $package_products = $this->db
            ->where_in('products_has_products.package_id', $product_ids)
            ->where('active', 1)
            ->join('products_has_products', 'products_has_products.product_id = products.id')
            ->get('products')
            ->result();

            $quick_notes = $this->db
            ->select('quick_notes.*, products_quick_notes.product_id')
            ->where('quick_notes.active', 1)
            ->where('quick_notes.deleted', null)
            ->where_in('products_quick_notes.product_id', $product_ids)
            ->join('products_quick_notes', 'products_quick_notes.quick_note_id = quick_notes.id')
            ->get('quick_notes')
            ->result();

            $data = [];
            foreach ($products as $product) {

                $product->additional_products = [];
                $product->stocks = [];
                $product->products = [];
                $product->quick_notes = [];

                // Additional products
                foreach ($additional_products as $additional_product) {
                    if ($product->id == $additional_product->product_id) {
                        $product->additional_products[$additional_product->id] = $additional_product;
                    }
                }

                // Stocks
                foreach ($stocks as $stock) {
                    if ($product->id == $stock->product_id) {
                        $product->stocks[$stock->id] = $stock;
                    }
                }

                // Quick Notes
                foreach ($quick_notes as $quick_note) {
                    if ($product->id == $quick_note->product_id) {
                        $product->quick_notes[$quick_note->id] = $quick_note;
                    }
                }

                if ($product->type == "package") {
                    foreach ($package_products as $package_product) {
                        if ($product->id == $package_product->package_id) {
                            $product->products[$package_product->product_id] = $package_product;
                        }   
                    }
                }

                $data[$product->id] = $product;
            }

            $products = $data;
        }

        messageAJAX('success', 'Success', compact('products'));
    }

    private function handleYsProductSync($ys_products, $system_products)
    {

        $additionals = [];

        foreach ($system_products as $system_product) {
            
            foreach ($ys_products as $ys_product) {

                if (!isset($system_product->id) || !isset($ys_product->id))
                    continue;

                if ($system_product->add_product_id) {
                    $additionals[$system_product->product_id][$system_product->add_product_id] = $system_product;
                }


                if ($system_product->id != $ys_product->id)
                    continue;

                $ys_product->sys_id = $system_product->product_id;

               
                if (!$ys_product->options)
                    continue;
                
                foreach ($ys_product->options as $option) {

                    $exist = false;
                    foreach ($system_products as $product_option) {

                        if ($option->Id == $product_option->id ) {

                            $exist = true;
                            if ($product_option->stock_id) {
                                $option->sys_stock_id = $product_option->stock_id;
                                $ys_product->stocks[] = $option;

                            }
                            // else if ($product_option->add_product_id) {
                            //     $option->sys_additional_id = $product_option->add_product_id;
                            // } 
                            else if ($product_option->portion_id) {
                                $option->sys_portion_id = $product_option->portion_id;
                                $ys_product->portions[] = $option;
                            }
                        } 
                    }

                    if (!$exist) {
                        $ys_product->unsynchronized[] = $option;
                    }
                }

            }

        }

        $temp_products = [];

        foreach ($ys_products as $key => $ys_product) { 
            foreach ($system_products as $system_product) {
                if ($ys_product->id == $system_product->id && !$system_product->add_product_id ) {
                    $temp_products[$key] = $ys_product;
                    $temp_products[$key]->additionals = [];

                    foreach ($ys_products as $key3 => $ys_product_option) {
                        foreach ($system_products as $system_product_option) {

                            if ($ys_product_option->ProductOptionId == $system_product_option->id && $system_product_option->add_product_id && $ys_product->sys_id == $system_product_option->product_id ) {
                                $ys_product_option->sys_additional_id = $system_product_option->add_product_id;
                                $temp_products[$key]->additionals[] = $ys_product_option;
                            }

                        }
                    }

                }
            }
            # code...
        }

        return $temp_products;
    }

    private function getCustomer() {

        $customers = $this->Customers_M->get_all();

        messageAJAX('success','Success', compact('customers'));

    }














































    // Add a new item
    public function index1($table = null)
    {
        $this->check_auth();

        $data['pos_points'] = $this->db
            ->select('id, location_id, title')
            ->where('active', 1)
        ->get('pos')->result_array();


        if (!$data[ 'cookie' ] = $this->checkPosSettings()) {
            
            $this->theme_plugin = array(
                'js'    => [ 'panel/js/view/pointofsales.js' ],
                'start' => 'Pointofsales.handlePoints('.json_encode($data['pos_points']).');'
            );

            $this->load->template('pointofsales/pos_form');

        } else {

            $data['current_table']     = (int) $table;
            $data['table']             = [];
            $data['sale']              = [];
            $data['sale_has_products'] = [];
            if ($data['current_table']) {
                if (!$data['table'] = $this->db ->where(['active' => 1, 'id' => $data['current_table']]) ->get('points', 1)->row()) {
                    redirect('pointofsales','refresh'); return;
                }

                $data['sale'] = $this->Sales_M->order_by('id', 'DESC')->findSaleWithProducts([
                    'point_id'    => $data['table']->id,
                    'location_id' => $data['cookie']['location'],
                    'pos_id'      => $data['cookie']['pos'],
                    'return_id'   => null,
                    'status'      => 'pending',
                    'active'      => 1,
                ]);

                $data['invoice'] = $this->db
                ->where('location_id', $data[ 'cookie' ]['location'])
                ->get('invoice_template', 1)->row();
                
            }

            $service_point = $this->db->select('id')->where(['location_id' => $data[ 'cookie' ]['location'], 'is_service' => 1])->get('points')->row();
            $data['customer'] = $this->db->where('id', (int) $this->input->get('customer_id'))->get('customers')->row();
            

            $data['locations'] = $this->user->locations_array;
            $this->theme_plugin = array(
                'css'   => ['globals/plugins/select2/css/select2.min.css', 'panel/css/view/pointofsales.css'],
                'js'    => array( 'panel/js/view/pointofsales.js', 
                                'globals/scripts/invoice.js',
                                'globals/plugins/jquery.print/print.js',
                                'globals/plugins/ionsound/js/ion.sound.min.js',
                                'globals/scripts/soundplayer.js',
                                'globals/plugins/recorderjs/recorder.js',
                                'globals/plugins/select2/js/select2.full.min.js',
                                'globals/plugins/select2/js/i18n/tr.js',
                                'globals/plugins/jquery-scannerdetection/jquery.scannerdetection.js'
                            ),
                'start' => 'MyFunction.eNumberSpin();
                            Pointofsales.init({
                                "website_virtual_keyboard": '.$this->website->virtual_keyboard.'
                            });
                            Pointofsales.handlePoints('.json_encode($data['pos_points']).');
                            Pointofsales.handlePointsAndLocation('.json_encode($data['cookie']).');
                            ReturnPOS.init();
                            SoundPlayer.init();
                            PackageService.init({
                                servicePointId: '.(@$service_point->id).'
                            }); '
            );


            if ($this->website->virtual_keyboard) {
                array_push($this->theme_plugin['css'], 
                    'globals/plugins/keyboard/css/keyboard-dark.min.css'
                );
                array_push($this->theme_plugin['js'],
                    'globals/plugins/keyboard/js/jquery.keyboard.min.js',
                    'globals/plugins/keyboard/js/jquery.mousewheel.min.js',
                    'globals/plugins/keyboard/js/jquery.keyboard.extension-extender.min.js',
                    'globals/plugins/keyboard/js/jquery.keyboard.extension-typing.min.js',
                    'globals/plugins/keyboard/languages/tr.js',
                    'globals/plugins/keyboard/layouts/turkish.min.js'
                );
                $this->theme_plugin['start'] .= 'MyFunction.keyboard();';
            }

            $data['location_currency'] = $data['locations'][$data['cookie']['location']]['currency'];
            // $data['location_tax'] = $data['locations'][$data['cookie']['location']]['tax'];
            $this->load->template('pointofsales/index', $data);
        } 
    }

    function changePos()
    {
        unset($_COOKIE[ self::cookie ]);
        setcookie(self::cookie, "", time()-60*60*24*365, '/');

        redirect('pointofsales','refresh');
    }

    private function checkPosSettings()
    {
        if (!isset($_COOKIE[ self::cookie])) {

            if ($this->input->is_ajax_request()) {
                
                $this->load->library('form_validation');
                $this->form_validation->set_rules('location_id', 'Restoran', 'trim|required|is_natural_no_zero');
                $this->form_validation->set_rules('pos_id', 'Kasa', 'trim|required|is_natural_no_zero');
                $this->form_validation->set_rules('username', 'Kullanıcı adı/Eposta', 'trim|required');
                $this->form_validation->set_rules('password', 'Şifre', 'trim|required');

                if (!$this->form_validation->run()) {
                    messageAJAX('error' , validation_errors());
                }

                $data['location'] = $this->input->post('location_id');
                $data['pos']      = $this->input->post('pos_id');
                $data['username'] = $this->input->post('username', TRUE);
                $data['password'] = $this->input->post('password', TRUE);

                $this->db->select('password, role_id')
                    ->where('active', 1)
                    ->group_start()
                        ->where('username', $data['username'])
                        ->or_where('email', $data['username'])
                    ->group_end();
                $user = $this->db->get('users', 1)->row();

                if (! $user) {
                    $data['password'] = '{REMOVED}';
                    messageAJAX('error', logs_($this->user->id, __('Hatalı kullanıcı adı veya şifre.'), $data, 5, $data['location']));
                }
                if ($user->password != sha1($data['password'] . $this->config->config['salt'])) {
                    $data['password'] = '{REMOVED}';
                    messageAJAX('error', logs_($this->user->id, __('Hatalı şifre.'), $data, 5, $data['location']));
                }
                if (intval($user->role_id) !== 1) {

                    $this->db->where('location_id', $data['location']);
                    $this->db->limit(1);
                    if(!$this->db->get('user_has_locations', 1)->row()) {
                        $data['password'] = '{REMOVED}';
                        messageAJAX('error',
                            logs_($this->user->id, __('Bu restorana erişim izniniz yok.'), $data, 5, $data['location']));
                    }


                    $this->db->select('METHOD.id');
                    $this->db->where('CONTROLLER.page', 'pointofsales');
                        $this->db->join('permissions METHOD', 'METHOD.parent_id = CONTROLLER.id')
                        ->where('METHOD.page', 'settingsforpos');
                    $page = $this->db->get('permissions CONTROLLER', 1)->row();

                    if(! $page) {
                        messageAJAX('error', __('Erişim yetkiniz yok. x001'));
                    }

                    // Kullanıcı Rolüne atanmış mı?
                    $this->db->where('permission_id', $page->id);
                    $this->db->where('role_id', $user->role_id);
                    if(! $this->db->get('role_has_permissions', 1)->row() ) {
                        messageAJAX('error', __('Erişim yetkiniz yok. x002'));
                    }
                }

                $data['password'] = '{REMOVED}';
                logs_($this->user->id, __('Kasa ayarları bu bilgisayara kaydedildi.'), $data, 5, $data['location']);

                setcookie(self::cookie, json_encode([ 'l' => $data[ 'location' ], 'p' => $data[ 'pos' ] ]), time()+60*60*24*365, '/');

                messageAJAX('success', logs_($this->user->id, __('Kasa ayarları kaydedildi.'), $data, 5, $data['location']));
                // return true;
            }

            return false;

        } else {

            $json = json_decode(@$_COOKIE[ self::cookie ]);
            if (!isset($json->l, $json->p)) {
                return false;
            }

            $location = (int) $json->l;
            $pos      = (int) $json->p;


            if (in_array($location, $this->user->locations)) {

                $this->db
                    ->where('id', $pos)
                    ->where('location_id', $location)
                    ->where('active', 1);
                if ($this->db->get('pos', 1)->row()) {

                    return [ 'location' => $location, 'pos' => $pos ];
                }
            }

            unset($_COOKIE[ self::cookie ]);
            setcookie(self::cookie, "", time()-60*60*24*365, '/');

            return false;
        }

        
    }

    function getTables()
    {
        if ($this->input->is_ajax_request()) {

            $location = (int) $this->input->post('location');

            if (!in_array($location, $this->user->locations)) {
                messageAJAX('error', __('Geçersiz Restoran.'));
            }

            $floors = $this->db->where([
                'location_id' => $location,
                'active' => 1,
            ])->get('floors')->result();

            $zones = $this->db->where([
                'location_id' => $location,
                'active' => 1,
            ])->get('zones')->result();

            if ($this->input->get('empty_tables')) {
                // $this->db->where('status', 0);
            }
            $tables = $this->db->where([
                'location_id' => $location,
                'active' => 1,
            ])->order_by('SUBSTRING(title, -2)')->get('points')->result();


            $html = null;
            $DIFF = $this->input->get('empty_tables');

            ob_start();
            if ($this->agent->is_mobile())
                require FCPATH . 'application/views/pointofsales/all_tables.php';
            else
                require FCPATH . 'application/views/pointofsales/all_planned_tables.php';
            $html = ob_get_clean();

            messageAJAX('success', NULL, $html);

        }
    }

    function getProducts()
    {
        if ($this->input->is_ajax_request()) {

            $location = (int) $this->input->post('location');


            $categories = $this->ProductTypes_M->findCategoriesWithProducts([
                'location_id' => $location
            ]);
// exit(json_encode($categories));
            // $this->db->select('(SELECT id FROM product_types WHERE id = P.product_type_id LIMIT 1) as category');
            // $this->db->where('P.active', 1);
            // $this->db->order_by('CASE P.type WHEN "product" THEN 1 WHEN "package" THEN 2 END', null, false);
            // $this->db->order_by('P.id', 'asc');
            // $products = $this->Products_model->index( $location );

            // $this->load->model('ProductTypes_Model');
            // $this->db->where('active', 1);
            // $this->db->where('parent_id IS NOT', null);
            // $categories = $this->ProductTypes_Model->index();


            $html = $this->load->view('pointofsales/elements/products', compact('categories'), true);
            messageAJAX('success', NULL, compact('html'));

        }
    }

    function getProductById($quantity = NULL)
    {
        if ($this->input->is_ajax_request()) {

            $location   = (int) $this->input->post('location');
            $product_id = (int) $this->input->post('product');

            $product = $this->db
                ->select('*')
                ->where('location_id', $location)
                ->where('id', $product_id)
            ->get('products', 1)->row();

            if (! $product) {

                messageAJAX('error', __('Ürün bulunamadı.'));
            }

            $tax = $product->tax;

            $price = numberFormat($product->price, 2);

            if ($quantity) {

                $quantity = (int) $quantity;

                $quantity = ($quantity < 1) ? 1 : $quantity;

                messageAJAX('success', NULL, numberFormat($price * $quantity, 2));
            }


            $html_product = !$product ? null : '
            <tr data-id="'.$product->id.'" class="pr-container">
                <td class="e-number-spin">
                    <div class="col-xs-4 col-lg-3"><a class="btn btn-floating btn-small btn-light-blue btn-ripple" data-do="minus"><i class="ion-android-remove"></i></a></div>
                    <div class="col-xs-4 col-lg-4">
                        <input type="number" name="products['.$product->id.']" class="form-control input-sm text-center" value="1">
                    </div>
                    <div class="col-xs-3 col-lg-3"><a class="btn btn-floating btn-small btn-light-blue btn-ripple" data-do="plus"><i class="ion-android-add"></i></a></div>
                </td>
                <td class="small">'.$product->title.'</td>
                <td class="small text-right">'.$price.'</td>
                <td class="small text-right">'.$price.'</td>
                <td class="text-right"><span class="fa fa-trash small btn btn-xs trash" aria-hidden="true"></span></td>
            </tr>';

            messageAJAX('success', NULL, $html_product);
        }
    }

    public function checkProductInSale()
    {
        $location_id      = (int) $this->input->post('location');
        $order_id         = (int) $this->input->post('order_id');
        $product_id       = (int) $this->input->post('product_id');
        $product_quantity = (int) $this->input->post("product_quantity");
        $cancel_request   = [];

        $product = $this->db
            ->select('*')
            ->where('location_id', $location_id)
            ->where('id', $product_id)
        ->get('products', 1)->row();
        if (!$product) {
            messageAJAX('error', __('Ürün bulunamadı.'));
        }

        if ($order_id) {

            $sale = $this->db->where([
                'id'     => $order_id,
                'active' => 1,
                'status' => 'pending'
            ])
            ->order_by('id', 'DESC')
            ->get('sales', 1)->row();
            if (!$sale) {
                messageAJAX('error', __('Sipariş geçersiz veya iptal edilmiş olabilir.'));
            }

            $sale_has_product = $this->db->where([
                'sale_id' => $sale->id,
                'product_id' => $product_id,
            ])
            ->order_by('id', 'DESC')
            ->get('sale_has_products', 1)->row();

            if ($sale_has_product) {

                if (($paid_quantity = $sale_has_product->paid_cash + $sale_has_product->paid_credit + $sale_has_product->paid_voucher) > $product_quantity) {
                    messageAJAX('error', 'En az ' . $paid_quantity . ' adet ürün için ARA ÖDEME yapılmış ve iptal edilemez.');
                }

                if ($sale_has_product->completed > $product_quantity) {

                    // tamamlanmış ürünü iptal etme yetkisi var mı?
                    if ($this->user->role_id != 1) {
                        if (!isset($this->user->permissions['pointofsales.cancelcompletedproduct'])) {
                            messageAJAX('error', 'En az ' . $sale_has_product->completed . ' adet ürün teslim edilmiş ve iptal edilemez.');
                        }
                    }
                    

                    $cancelled_qunatity = $sale_has_product->completed - $product_quantity;

                    // session oluştur.
                    $session = sha1(uniqid() . json_encode($sale_has_product));
                    $this->session->set_userdata('session_cancelcompletedproduct', $session);

                    $cancel_request = [
                        'product_id' => $sale_has_product->product_id,
                        'quantity'   => $cancelled_qunatity,
                        'session'    => $session, 
                    ];                  

                }
            }
        }


        messageAJAX('success', NULL, [
            'price' => numberFormat( $product->price * $product_quantity, 2),
            'quantity' => $product_quantity,
            'canceled' => $cancel_request
        ]);
    }

    function _calculate($location = FALSE, $products = [], $payments = [])
    {
        $data[ 'total' ]                 = 0.00;
        $data[ 'total_formated' ]        = 0.00;
        $data[ 'subtotal' ]              = 0.00;
        $data[ 'subtotal_formated' ]     = 0.00;
        $data[ 'tax' ]                   = 0.00;
        $data[ 'tax_formated' ]          = 0.00;
        $data[ 'paid' ]                  = 0.00;
        $data[ 'paid_formated' ]         = 0.00;
        $data[ 'paid_cash' ]             = 0.00;
        $data[ 'paid_cash_formated' ]    = 0.00;
        $data[ 'paid_credit' ]           = 0.00;
        $data[ 'paid_credit_formated' ]  = 0.00;
        $data[ 'paid_voucher' ]          = 0.00;
        $data[ 'paid_voucher_formated' ] = 0.00;
        $data[ 'paid_ticket' ]           = 0.00;
        $data[ 'paid_ticket_formated' ]  = 0.00;
        $data[ 'paid_sodexo' ]           = 0.00;
        $data[ 'paid_sodexo_formated' ]  = 0.00;
        $data[ 'rest' ]                  = 0.00;
        $data[ 'rest_formated' ]         = 0.00;
        $data[ 'location_currency' ]     = $this->user->locations_array[ $location ][ 'currency' ];
        // $data[ 'location_tax' ]          = $this->user->locations_array[ $location ][ 'tax' ];
        $data[ 'interim_paid_cash' ]     = 0;
        $data[ 'interim_paid_credit' ]   = 0;
        $data[ 'interim_paid_voucher' ]  = 0;
        $data[ 'interim_paid_ticket' ]   = 0;
        $data[ 'interim_paid_sodexo' ]   = 0;
        $data['tax_ramoved'] = $remove_tax = ($this->session->userdata('pos_remove_tax_session') && $this->input->post('tax_ramoved') == $this->session->userdata('pos_remove_tax_session'));

        // CALCULATE PRODUCTS
        foreach ($products as $product => $quantity) {

            $this->db
                ->where_in('P.id', (int) $product)
                ->limit(1);
            $_products = $this->Products_model->index( $location ) OR messageAJAX('error', __('Geçersiz Ürün.'));

            $_product = $_products[ 0 ];

            $calc_tax = $_product->price * $_product->tax / 100;

            $quantity = is_numeric($quantity) ? $quantity : 1;

            $data[ 'subtotal' ] += $tmp_subtotal = round($_product->price * $quantity, 2);

            $data[ 'tax' ] += $remove_tax ? 0 : $tmp_subtotal * $_product->tax / 100;
        }

        $data[ 'subtotal_formated' ] = numberFormat($data[ 'subtotal' ], 2);

        // TOTAL TAX
        $data[ 'tax_formated' ] = numberFormat($data[ 'tax' ], 2);


        // TOTAL AMOUNT
        $data[ 'total' ]          = round($data[ 'subtotal' ] + $data[ 'tax' ], 2);
        $data[ 'total_formated' ] = numberFormat($data[ 'total' ], 2 );



        // CALCULATE PAYMENTS
        if (isset($payments[ 'cash' ])) {

            foreach ($payments[ 'cash' ] as $payment) {

                $data[ 'paid_cash' ] += numberUnFormat($payment);

            }
        }
        if (isset($payments[ 'credit' ])) {

            foreach ($payments[ 'credit' ] as $payment) {

                $data[ 'paid_credit' ] += numberUnFormat($payment);

            }
        }
        if (isset($payments[ 'voucher' ])) {

            foreach ($payments[ 'voucher' ] as $payment) {

                $data[ 'paid_voucher' ] += numberUnFormat($payment);

            }
        }
        if (isset($payments[ 'ticket' ])) {

            foreach ($payments[ 'ticket' ] as $payment) {

                $data[ 'paid_ticket' ] += numberUnFormat($payment);

            }
        }
        if (isset($payments[ 'sodexo' ])) {

            foreach ($payments[ 'sodexo' ] as $payment) {

                $data[ 'paid_sodexo' ] += numberUnFormat($payment);

            }
        }

        // Ara Ödemeler
        if ($order_id = (int) $this->input->post('order_id')) {

            $interim_payments = $this->db->where([
                'sale_id' => $order_id,
                'active' => 1,
            ])
            ->order_by('id', 'desc')
            ->get('sale_has_products')->result();

            if ($interim_payments) {
                
                foreach ($interim_payments as $paid_product) {
                        
                    if ($paid_product->paid_cash) {

                        $paid_product_amount = $paid_product->price * $paid_product->paid_cash;
                        $paid_product_amount = $paid_product_amount + ($remove_tax ? 0 : $paid_product_amount * $paid_product->tax / 100);

                        $data['interim_paid_cash'] += $paid_product_amount;

                    }
                    if ($paid_product->paid_credit) {

                        $paid_product_amount = $paid_product->price * $paid_product->paid_credit;
                        $paid_product_amount = $paid_product_amount + ($remove_tax ? 0 : $paid_product_amount * $paid_product->tax / 100);

                        $data['interim_paid_credit'] += $paid_product_amount;

                    }
                    if ($paid_product->paid_voucher) {

                        $paid_product_amount = $paid_product->price * $paid_product->paid_voucher;
                        $paid_product_amount = $paid_product_amount + ($remove_tax ? 0 : $paid_product_amount * $paid_product->tax / 100);

                        $data['interim_paid_voucher'] += $paid_product_amount;
                    }
                    if ($paid_product->paid_ticket) {

                        $paid_product_amount = $paid_product->price * $paid_product->paid_ticket;
                        $paid_product_amount = $paid_product_amount + ($remove_tax ? 0 : $paid_product_amount * $paid_product->tax / 100);

                        $data['interim_paid_ticket'] += $paid_product_amount;
                    }
                    if ($paid_product->paid_sodexo) {

                        $paid_product_amount = $paid_product->price * $paid_product->paid_sodexo;
                        $paid_product_amount = $paid_product_amount + ($remove_tax ? 0 : $paid_product_amount * $paid_product->tax / 100);

                        $data['interim_paid_sodexo'] += $paid_product_amount;
                    }

                }
            }
        }

        $data[ 'interim_payment' ]          = $data['interim_paid_cash'] + $data['interim_paid_credit'] + $data['interim_paid_voucher'] + $data['interim_paid_ticket'] + $data['interim_paid_sodexo'];
        $data[ 'interim_payment_formated' ] = numberFormat($data[ 'interim_payment' ], 2);


        $data[ 'paid_cash' ]                = round($data[ 'paid_cash' ], 2);
        $data[ 'paid_credit' ]              = round($data[ 'paid_credit' ], 2);
        $data[ 'paid_voucher' ]             = round($data[ 'paid_voucher' ], 2);
        $data[ 'paid_ticket' ]              = round($data[ 'paid_ticket' ], 2);
        $data[ 'paid_sodexo' ]              = round($data[ 'paid_sodexo' ], 2);
        
        $data[ 'paid_cash_formated' ]       = numberFormat($data[ 'paid_cash' ], 2);
        $data[ 'paid_credit_formated' ]     = numberFormat($data[ 'paid_credit' ], 2);
        $data[ 'paid_voucher_formated' ]    = numberFormat($data[ 'paid_voucher' ], 2);
        $data[ 'paid_ticket_formated' ]     = numberFormat($data[ 'paid_ticket' ], 2);
        $data[ 'paid_sodexo_formated' ]     = numberFormat($data[ 'paid_sodexo' ], 2);
        
        $data[ 'paid' ]                     = $data[ 'paid_cash' ] + $data[ 'paid_credit' ] + $data[ 'paid_voucher' ] + $data[ 'paid_ticket' ] + $data[ 'paid_sodexo' ];
        $data[ 'paid_formated' ]            = numberFormat($data[ 'paid' ], 2);
        
        $data[ 'rest' ]                     = $data[ 'total' ] - $data[ 'paid' ] - $data[ 'interim_payment' ];
        $data[ 'rest_formated' ]            = numberFormat($data[ 'rest' ], 2);
        
        $data[ 'status' ]                   = ($data[ 'paid' ] >= $data[ 'total' ]) ? false : true;
        
       

        return $data;
    }

    function calculate()
    {
        if ($this->input->is_ajax_request()) {

            $products = $this->input->post('products[]');
            $location = $this->input->post('location');
            $payments = $this->input->post('payments[]');

            if (! in_array($location, $this->user->locations)) {

                messageAJAX('error', __('Geçersiz Restoran.'));
            }

            $data = $this->_calculate($location, (array) $products, (array) $payments);

            messageAJAX('success', NULL, $data);
        }
    }

    

    function complete1($is_order = false)
    {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('products[]', 'Ürünler', 'trim|required', ['required'=>'En az bir ürün seçiniz.']);
        $this->form_validation->set_rules('payments[]', 'Ödemeler', 'trim|'.($is_order ?: 'required'), ['required'=>'En az bir ödeme gerekli.']);
        $this->form_validation->set_rules('location', 'Restoran', 'trim|required');
        $this->form_validation->set_rules('pos', 'Kasa', 'trim|required');
        $this->form_validation->set_rules('pos-note', 'Not', 'trim');

        if (!$this->form_validation->run()) {
            messageAJAX('error' , validation_errors());
        }

        $order_id        = (int) $this->input->post('order_id');
        $table_id        = (int) $this->input->post('table_id') ? $this->input->post('table_id') : null;
        $customer_id     = (int) $this->input->post('customer_id') ? $this->input->post('customer_id') : null;
        $products        = $this->input->post('products[]');
        $location        = $this->input->post('location');
        $pos             = $this->input->post('pos');
        $payments        = $this->input->post('payments[]');
        $posnote         = $this->input->post('pos-note', TRUE);
        $created         = _date();
        $digital_keys    = [];
        $payment_type_id = 0;

        $operator        = $this->user;
        if ($this->user->pin) {
            if (!$this->session->userdata('operator_user')) {
                messageAJAX('error' , __('Pin anahtarınızı girerek oturum açın.'));
            }

            // Operatör hesabının verileri oturum açan kullanıcıya geçici olarak atabıyor.
            if ($this->user->id != $this->session->userdata('operator_user')) {
                $operator = $this->handleOperator();
            }
        }
        

        if (! in_array($location, $operator->locations)) {
            messageAJAX('error', __('Geçersiz Restoran.'));
        }
        $this->db->where('id', $pos);
        $this->db->where('location_id', $location);
        $this->db->where('active', 1);
        if (!$this->db->get('pos', 1)->row()) {
            messageAJAX('error', __('Geçersiz Restoran/Kasa.'));
        }
        if ($is_order && !$this->db ->where(['active' => 1, 'id' => $table_id]) ->get('points', 1)->row()) {
            messageAJAX('error', __('Geçersiz Masa.'));
        }
        if ($customer_id) {
            if (!$this->Customers->get(['id' => $customer_id])) {
                messageAJAX('error', __('Geçersiz Müşteri.'));
            }
        }

        if ($order_id) {
            
            $order = $this->db->where([
                'id'     => $order_id,
                'active' => 1,
                'status' => 'pending'
            ])->order_by('id', 'DESC')->get('sales', 1)->row() OR messageAJAX('error', __('Sipariş geçersiz veya iptal edilmiş olabilir.'));

            $order_has_products = $this->db->where([
                'sale_id' => $order_id,
            ])->get('sale_has_products')->result_array();


            $tmp = [];
            $order_has_products_prdct_array = [];
            foreach ($order_has_products as $value) {
                $tmp[$value['id']] = $value;
                $order_has_products_prdct_array[$value['product_id']] = $value['id'];
            }
            $order_has_products = $tmp;
        }


        $data = $this->_calculate($location, (array) $products, (array) $payments);


        // PRODUCTS
        $ignore_products_on_delete = null;
        foreach ($products as $product => $quantity) {

            $this->db->where_in('P.id', (int)$product);
            $this->db->limit(1);
            $_products = $this->Products_model->index( $location )
            OR messageAJAX('error', __('Geçersiz Ürün.'));

            $_product = $_products[ 0 ];
            $quantity = is_numeric($quantity) ? $quantity : 1;

            // PREPARE PACKAGE CONTENT
            if ($_product->type == 'package') {

                $package_has_products = $this->db
                    ->select('P.*')
                    ->where('PHP.package_id', $_product->id)
                    ->where('P.type', 'product')
                    ->join('products P', 'P.id = PHP.product_id')
                ->get('products_has_products PHP')->result();

                $data[ 'package_has_products' ][ $product ] = NULL;
                foreach ($package_has_products as $key => $has_product) {

                    // PACKAGE CONTENT PREPARING
                    $data[ 'package_has_products' ][ $product ][] = [
                        'package_id' => NULL,
                        'product_id' => $has_product->id,
                        'quantity'   => $quantity * 1,
                        'price'      => $has_product->price,
                        'content'    => $has_product->content,
                        'digital'    => $has_product->digital,
                        'is_video'   => $has_product->is_video,
                        'title'      => $has_product->title,
                        'created'    => $created,
                        'active'     => 1,
                        'tax'        => ($data['tax_ramoved'] ? $has_product->tax : 0),
                    ];  
                }
            } 

            if ($order_id && isset($order_has_products_prdct_array[$_product->id])) {


                // Sipariş edilen toplam sayı tamamlanan eski siparişlerden fazla değilse işlemi onaylama.
                $cancelled = 0;
                if ($order_has_products[$order_has_products_prdct_array[$_product->id]]['completed'] > $quantity) {

                    // Yetkili kişilerce iptal edilmiş mi?
                    if ($this->session->userdata('session_cancelcompletedproduct') && $this->session->userdata('session_cancelcompletedproduct') == $this->input->post('session_cancelcompletedproduct')) {
                        $cancelled = (int) $this->input->post('cancelled_completed_products['.$_product->id.']');
                        if ($order_has_products[$order_has_products_prdct_array[$_product->id]]['completed'] < $cancelled) {
                            messageAJAX('error', __('Teslim edilmiş ürün sayısını aştınız. Sayfayı yenileyip tekrar deneyin.'));
                        }
                    }
                    else {
                        messageAJAX('error', __('Teslim edilen sipariş sayısından daha az sipariş veremezsiniz.'));
                    }
                }

                $ignore_products_on_delete[] = $order_has_products_prdct_array[$_product->id];

                // PREPARE SALE_HAS_PRODUCTS UPDATE ARRAY
                $data[ 'sale_has_products' ]['update'][ $product ] = [
                    'id'         => $order_has_products_prdct_array[$_product->id],
                    // 'sale_id'    => $order_id,
                    // 'product_id' => $_product->id,
                    'quantity'   => $quantity,
                    'price'      => $_product->price,
                    'content'    => $_product->content,
                    'digital'    => $_product->digital,
                    'is_video'   => $_product->is_video,
                    'title'      => $_product->title,
                    'type'       => $_product->type,
                    'modified'   => $created,

                    'completed'  => $order_has_products[$order_has_products_prdct_array[$_product->id]]['completed'] - $cancelled,
                    // 'active'     => 0
                    'tax'        => ($data['tax_ramoved'] ? 0 : $_product->tax),
                ];

            } else {

                // PREPARE SALE_HAS_PRODUCTS INSERT ARRAY
                $data[ 'sale_has_products' ]['insert'][ $product ] = [
                    'sale_id'    => NULL,
                    'product_id' => $_product->id,
                    'quantity'   => $quantity,
                    'price'      => $_product->price,
                    'content'    => $_product->content,
                    'digital'    => $_product->digital,
                    'is_video'   => $_product->is_video,
                    'title'      => $_product->title,
                    'type'       => $_product->type,
                    'created'    => $created,
                    'active'     => 1,
                    'tax'        => ($data['tax_ramoved'] ? 0 : $_product->tax),
                ];
            }

            
        }
        // END - PRODUCTS


        if (!$is_order) {
            $payment_counter = [];
            if ($data[ 'paid_cash' ] || $data[ 'interim_paid_cash' ]) {
                $payment_type_id = 1;
                $payment_counter[$payment_type_id] = $payment_type_id;
            }
            if ($data[ 'paid_credit' ] || $data[ 'interim_paid_credit' ]) {
                $payment_type_id = 2;
                $payment_counter[$payment_type_id] = $payment_type_id;
            }
            if ($data[ 'paid_voucher' ] || $data[ 'interim_paid_voucher' ]) {
                $payment_type_id = 5;
                $payment_counter[$payment_type_id] = $payment_type_id;
            }
            if ($data[ 'paid_ticket' ] || $data[ 'interim_paid_ticket' ]) {
                $payment_type_id = 6;
                $payment_counter[$payment_type_id] = $payment_type_id;
            }
            if ($data[ 'paid_sodexo' ] || $data[ 'interim_paid_sodexo' ]) {
                $payment_type_id = 7;
                $payment_counter[$payment_type_id] = $payment_type_id;
            }
            // En az 2 ödeme varsa çoklu ödeme yapılmıştır.
            if (count($payment_counter) >= 2) {
                $payment_type_id = 4;
            } 

        
            if ($data[ 'total' ] > $data[ 'paid' ] + $data[ 'interim_payment' ]) {
                messageAJAX('error', __('Yetersiz Ödeme.'));
            }

            if ( -1 * $data[ 'rest' ] > $data[ 'paid_cash' ] + $data[ 'interim_paid_cash' ]) {
                messageAJAX('error', __('Para üstü, ödenen nakit miktardan fazla olamaz.'));
            }
        }
        


        // PREPARE SALES
        $data['sales'] = [
            'location_id'     => $location,
            'pos_id'          => $pos,
            'point_id'        => $table_id,
            'sale_type_id'    => 1,
            'customer_id'     => $customer_id,
            'amount'          => $data[ 'total' ],
            'subtotal'        => $data[ 'subtotal' ],
            'change'          => $data[ 'paid' ] - $data[ 'total' ],
            // 'tax'             => (($this->session->userdata('pos_remove_tax_session') && $this->input->post('tax_ramoved') == $this->session->userdata('pos_remove_tax_session')) ? 0 : $data[ 'location_tax' ]),
            'cash'            => $data[ 'paid_cash' ] + (!$is_order ? $data[ 'interim_paid_cash' ] : 0),
            'credit'          => $data[ 'paid_credit' ] + (!$is_order ? $data[ 'interim_paid_credit' ] : 0),
            'voucher'         => $data[ 'paid_voucher' ] + (!$is_order ? $data[ 'interim_paid_voucher' ] : 0),
            'ticket'          => $data[ 'paid_ticket' ] + (!$is_order ? $data[ 'interim_paid_ticket' ] : 0),
            'sodexo'          => $data[ 'paid_sodexo' ] + (!$is_order ? $data[ 'interim_paid_sodexo' ] : 0),
            'discount_amount' => NULL,
            'discount_type'   => NULL,
            'desc'            => $posnote,
            'created'         => $created,
            'status'          => 'pending',
            'active'          => 1,
        ];

        if (!$is_order) {

            // hesap al yetkisi var mı?
            if ($operator->role_id != 1) {
                if (!isset($operator->permissions['pointofsales.poscomplete'])) {
                    messageAJAX('error', __('Hesap alma yetkiniz yok.'));
                }
            }

            $data['sales'] = array_merge($data['sales'], [
                'payment_type_id'   => $payment_type_id,
                'payment_date'      => $created,
                'status'            => 'completed',
                'completer_user_id' => $operator->id,
            ]);

        }


        if ($order_id) {

            unset($data['sales']['created']);
            
            $this->db->where('id', $order_id);
            $this->db->update('sales', $data[ 'sales' ]);
        } else {
            $data['sales']['user_id'] = $operator->id;
            $this->db->insert('sales', $data[ 'sales' ]);
            $data['sales'] = [ 'id' => $this->db->insert_id() ] + $data[ 'sales' ];
        }
        $sale_id = $order_id ? $order_id : $data[ 'sales' ][ 'id' ];
        // END - PREPARE SALES


        // UPDATE DESK STATUS
        if ($table_id) {
            if (!$is_order) {
                $this->db->where('id', $table_id);
                $this->db->update('points', [
                    'status' => 0,
                ]);

            } elseif($is_order && !$order_id) {
                $this->db->where('id', $table_id);
                $this->db->update('points', [
                    'status' => 1,
                    'opened_at' => _date()
                ]);
            }
        }
        // END - UPDATE DESK STATUS


        // PREPARE PRODUCT IN SALE
        if ($order_id) {
            if ($ignore_products_on_delete) {
                $this->db->where_not_in('id', $ignore_products_on_delete);
                $this->db->where('sale_id', $order_id);
                $this->db->delete('sale_has_products');

                $this->db->where_in('package_id', $ignore_products_on_delete);
                $this->db->delete('package_has_products');
            }

            if (@$data[ 'sale_has_products' ][ 'update' ]) {
                foreach ($data[ 'sale_has_products' ][ 'update' ] as $product => $update_product) {
                
                    $_id = $update_product['id'];
                    unset($update_product['id']);

                    $this->db->where('id', $_id);
                    $this->db->update('sale_has_products', $update_product);

                    $package_id = $_id;

                    // Products in package
                    if ($data[ 'sale_has_products' ][ 'update' ][ $product ][ 'type' ] == 'package') {
                        foreach ($data[ 'package_has_products' ][ $product ] as $has_product) {

                            $has_product[ 'package_id' ] = $package_id;

                            $this->db->insert('package_has_products', $has_product);

                            // If it is a sale, re-calculate stocks
                            if (!$is_order) {
                                $product_has_stocks = $this->db
                                ->select('PHS.*')
                                ->where('PHS.product_id', $has_product[ 'product_id' ])
                                ->join('stocks S', 'S.id = PHS.stock_id')
                                ->where('S.active', 1)
                                ->get('product_has_stocks PHS')->result();

                                if ($product_has_stocks) {
                                    foreach ($product_has_stocks as $stock) {
                                        
                                        $this->db->where('id', $stock->stock_id);
                                        $this->db->set('quantity', '(quantity - '.($update_product['quantity'] * $stock->quantity).')', false);
                                        $this->db->update('stocks', ['modified' => _date()]);
                                    }
                                }
                            }
                        }

                    } elseif ($update_product[ 'type' ] == 'product') {
                        
                        // If it is a sale, re-calculate stocks
                        if (!$is_order) {
                            $product_has_stocks = $this->db
                            ->select('PHS.*')
                            ->where('PHS.product_id', $product)
                            ->join('stocks S', 'S.id = PHS.stock_id')
                            ->where('S.active', 1)
                            ->get('product_has_stocks PHS')->result();

                            if ($product_has_stocks) {
                                foreach ($product_has_stocks as $stock) {
                                    
                                    $this->db->where('id', $stock->stock_id);
                                    $this->db->set('quantity', '(quantity - '.($update_product['quantity'] * $stock->quantity).')', false);
                                    $this->db->update('stocks', ['modified' => _date()]);
                                }
                            }
                        }
                        
                    }

                }
            }
        }
        
        if (@$data[ 'sale_has_products' ][ 'insert' ]) {
            
            foreach ($data[ 'sale_has_products' ][ 'insert' ] as $product => $insert_product) {
                
                $insert_product[ 'sale_id' ] = $sale_id;

                $this->db->insert('sale_has_products', $insert_product);

                $package_id = $this->db->insert_id();

                // Products in package
                if ($insert_product[ 'type' ] == 'package') {
                    foreach ($data[ 'package_has_products' ][ $product ] as $has_product) {

                        $has_product[ 'package_id' ] = $package_id;

                        $this->db->insert('package_has_products', $has_product);


                        // If it is a sale, re-calculate stocks
                        if (!$is_order) {
                            $product_has_stocks = $this->db
                            ->select('PHS.*')
                            ->where('PHS.product_id', $has_product[ 'product_id' ])
                            ->join('stocks S', 'S.id = PHS.stock_id')
                            ->where('S.active', 1)
                            ->get('product_has_stocks PHS')->result();

                            if ($product_has_stocks) {
                                foreach ($product_has_stocks as $stock) {
                                    
                                    $this->db->where('id', $stock->stock_id);
                                    $this->db->set('quantity', '(quantity - '.($insert_product['quantity'] * $stock->quantity).')', false);
                                    $this->db->update('stocks', ['modified' => _date()]);
                                }
                            }
                        }
                    }
                } elseif ($insert_product[ 'type' ] == 'product') {
                        
                        // If it is a sale, re-calculate stocks
                        if (!$is_order) {
                            $product_has_stocks = $this->db
                            ->select('PHS.*')
                            ->where('PHS.product_id', $product)
                            ->join('stocks S', 'S.id = PHS.stock_id')
                            ->where('S.active', 1)
                            ->get('product_has_stocks PHS')->result();

                            if ($product_has_stocks) {
                                foreach ($product_has_stocks as $stock) {
                                    
                                    $this->db->where('id', $stock->stock_id);
                                    $this->db->set('quantity', '(quantity - '.($insert_product['quantity'] * $stock->quantity).')', false);
                                    $this->db->update('stocks', ['modified' => _date()]);
                                }
                            }
                        }
                        
                    }
            }
        }
        // END PREPARE PRODUCT IN SALE

        // PREPARE PRODUCT IN SALE $product => $quantity
        // foreach($products as $product => $quantity) {

        //     $data[ 'sale_has_products' ][ $product ][ 'sale_id' ] = $data[ 'sales' ][ 'id' ];
    
        //     $this->db->insert('sale_has_products', $data[ 'sale_has_products' ][ $product ]);

        //     $package_id = $this->db->insert_id();

        //     // Products in package
        //     if (in_array($data[ 'sale_has_products' ][ $product ][ 'type' ], ['package', 'ves'])) {
        //         foreach ($data[ 'package_has_products' ][ $product ] as $has_product) {

        //             $has_product[ 'package_id' ] = $package_id;

        //             $this->db->insert('package_has_products', $has_product);
        //         }
        //     }
            
        // }
        // END PREPARE PRODUCT IN SALE       

        messageAJAX('success', $is_order ? __('Sipariş alındı.') : __('Ödeme Yapıldı.'), [
            'id'       => $sale_id,
            'is_order' => $is_order
        ]);
    }

    protected function handleOperator()
    {
        $operator = $this->exists('users', [
            'id'     => $this->session->userdata('operator_user'),
            'active' => 1
            ], 'users.*');
        if (!$operator) {
            messageAJAX('error' , __('Hesap bulunamadı!'));
        }

        $role = $this->exists('roles', ['id' => $operator->role_id], 'id, title');
        $operator->role = $role->title;

            // Kullanıcının Lokasyonları çekiliyor.
        $this->db->select('L.id, L.title, L.currency, L.tax, L.force_sale_via_ticket');
        if((int)$operator->role_id !== 1) {

            $this->db->where('UHL.user_id' , $operator->id);
            $this->db->join('user_has_locations UHL', 'UHL.location_id = L.id');
        }
        $this->db->where('active', 1);
        $operator->locations_array = $this->db->get('locations L')->result_array();
        $operator->locations       = ($operator->locations_array) ? array_column( $operator->locations_array , 'id' ) : [0];

        $tmp_locations = [];
        if($operator->locations_array) {
            foreach($operator->locations_array as $value) {
                $tmp_locations[ $value['id'] ] = ['id' => $value['id'], 'title' => $value['title'], 'currency' => $value['currency'], 'tax' => $value['tax'], 'force_sale_via_ticket' => $value['force_sale_via_ticket']];
            }
        }
        $operator->locations_array = $tmp_locations;


        $operator->permissions = [];

        $permissions = $this->db->where([
            'active' => 1,
            'parent_id' => null
        ])->get('permissions')->result_array();

        $tmp = null;
        foreach ($permissions as $permisson) {
            $tmp[$permisson['id']] = $permisson;
        }
        $permissions = $tmp;

        if (!$permissions) {
            return;
        }

        $roles_permissions = $this->db
        ->select('Permissions.*')
        ->where('Permissions.active', 1)
        ->where('RolesPermissions.role_id', $operator->role_id)
        ->where_in('Permissions.parent_id', array_keys($permissions))
        ->join('role_has_permissions AS RolesPermissions', 'RolesPermissions.permission_id = Permissions.id')
        ->get('permissions AS Permissions')->result_array();

        foreach ($permissions as $permisson) {

            foreach ($roles_permissions as $role_permisson) {
                if ($role_permisson['parent_id'] == $permisson['id']) {

                    $key = $permisson['page'] .'.'. $role_permisson['page'];

                    $operator->permissions[ $key ] = $key;
                }
            }
        }

        return $operator;
    }

    function managerAccount()
    {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('location' , 'Restoran', 'trim|required');
        $this->form_validation->set_rules('username' , 'Kullanıcı adı/Eposta', 'trim|required');
        $this->form_validation->set_rules('password' , 'Şifre', 'trim|required');

        if (!$this->form_validation->run()) {
            messageAJAX('error' , validation_errors());
        }

        $data['location'] = $this->input->post('location', TRUE);
        $data['username'] = $this->input->post('username', TRUE);
        $data['password'] = $this->input->post('password', TRUE);

        if($data['location'] && !in_array($data['location'], $this->user->locations)) {
            messageAJAX('error', __('Bu restorana erişim izniniz yok.'));
        }

        $this->db->select('users.password, users.role_id')
            ->where('users.active', 1)
            ->group_start()
                ->where('users.username', $data['username'])
                ->or_where('users.email', $data['username'])
            ->group_end();
        $this->db->join('roles', 'roles.id = users.role_id')
            ->where('roles.active', 1);
        $user = $this->db->get('users', 1)->row();

        if (! $user) {
            $data['password'] = '{REMOVED}';
            messageAJAX('error', logs_($this->user->id, __('Hatalı kullanıcı adı veya şifre.'), $data, 3, $data['location']));
        }
        if ($user->password != sha1($data['password'] . $this->config->config['salt'])) {
            $data['password'] = '{REMOVED}';
            messageAJAX('error', logs_($this->user->id, __('Hatalı şifre.'), $data, 3, $data['location']));
        }

        // Check permisson
        if (intval($user->role_id) !== 1) {
        
            
            $this->db->select('C.id');
            $this->db->where('P.page', 'removetax');
                $this->db->join('permissions C', 'C.parent_id = P.id')
                ->where('C.page', 'access');
            $page = $this->db->get('permissions P', 1)->row();

            if(! $page) {
                ($this->input->is_ajax_request()) ? messageAJAX('error', __('Erişim yetkiniz yok. x001')) : error_('access');
            }

            // Kullanıcı Rolüne atanmış mı?
            $this->db->where('permission_id', $page->id);
            $this->db->where('role_id', $user->role_id);
            if(! $this->db->get('role_has_permissions', 1)->row() ) {
                ($this->input->is_ajax_request()) ? messageAJAX('error', __('Erişim yetkiniz yok. x002')) : error_('access');
            }
        }

        $data['session'] = sha1(uniqid() . json_encode($data));
        $this->session->set_userdata('pos_remove_tax_session', $data['session']);

        $data['password'] = '{REMOVED}';
        messageAJAX('success',
            logs_($this->user->id, __('Vergi silme isteği oluşturldu.'), $data, 6, $data['location']),
            ['session' => $data['session']]
        );        
    }

    function managerAccount2Return()
    {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('location' , 'Restoran', 'trim|required');
        $this->form_validation->set_rules('username' , 'Kullanıcı adı/Eposta', 'trim|required');
        $this->form_validation->set_rules('password' , 'Şifre', 'trim|required');

        if (!$this->form_validation->run()) {
            messageAJAX('error' , validation_errors());
        }

        $data['location'] = $this->input->post('location', TRUE);
        $data['username'] = $this->input->post('username', TRUE);
        $data['password'] = $this->input->post('password', TRUE);

        if($data['location'] && !in_array($data['location'], $this->user->locations)) {
            messageAJAX('error', __('Bu restorana erişim izniniz yok.'));
        }

        $this->db->select('users.password, users.role_id')
            ->where('users.active', 1)
            ->group_start()
                ->where('users.username', $data['username'])
                ->or_where('users.email', $data['username'])
            ->group_end();
        $this->db->join('roles', 'roles.id = users.role_id')
            ->where('roles.active', 1);
        $user = $this->db->get('users', 1)->row();

        if (! $user) {
            $data['password'] = '{REMOVED}';
            messageAJAX('error', logs_($this->user->id, __('Hatalı kullanıcı adı veya şifre.'), $data, 4, $data['location']));
        }
        if ($user->password != sha1($data['password'] . $this->config->config['salt'])) {
            $data['password'] = '{REMOVED}';
            messageAJAX('error', logs_($this->user->id, __('Hatalı şifre.'), $data, 4, $data['location']));
        }

        // Check permisson
        if (intval($user->role_id) !== 1) {
        
            
            $this->db->select('C.id');
            $this->db->where('P.page', 'returns');
                $this->db->join('permissions C', 'C.parent_id = P.id')
                ->where('C.page', 'access');
            $page = $this->db->get('permissions P', 1)->row();

            if(! $page) {
                ($this->input->is_ajax_request()) ? messageAJAX('error', __('Erişim yetkiniz yok. x001')) : error_('access');
            }

            // Kullanıcı Rolüne atanmış mı?
            $this->db->where('permission_id', $page->id);
            $this->db->where('role_id', $user->role_id);
            if(! $this->db->get('role_has_permissions', 1)->row() ) {
                ($this->input->is_ajax_request()) ? messageAJAX('error', __('Erişim yetkiniz yok. x002')) : error_('access');
            }
        }



        $data['session'] = sha1(uniqid());
        $this->db->insert('return_keys', [
            'user_id' => $this->user->id,
            'session' => $data['session']
        ]);

        $data['password'] = '{REMOVED}';
        messageAJAX('success',
            logs_($this->user->id, __('İade işlemi oluşturuldu.'), $data, 4, $data[ 'location' ]), [ 'session' => $data[ 'session' ] ]
        );
    }

    function invoice($id = FALSE, $return = false)
    {
        $this->load->model('Sales_model');
        $data = $this->Sales_model->invoice_data($id);

        if(! $data) {
            if ($return) {
                return false;
            }
            messageAJAX('error', __('Satış bulunamadı.'));
        }

        $customer_template = null;
        if ($data['sale']->customer_id) {
            $customer = $this->db->where('id', $data['sale']->customer_id)->get('customers')->row();
            $customer_template = "<div><b>".__('Paket Servis:')."</b> {$customer->address} - {$customer->phone} ({$customer->name})</div>";
        }


        $html = '
        <div class="row">
            <div class="col-md-12">
                <div class="panel">
                    <div class="panel-heading">
                        <div class="panel-title">
                            <h4>'.__('FATURA').'</h4>
                        </div>
                    </div>
                    <div class="panel-body">

                        <div class="invoice">
                            <div class="invoice-heading">
                                <div class="row date-row">
                                    <div class="col-md-6 col-xs-6">
                                        <div id="invoice_barcode"></div>
                                    </div>
                                    <div class="col-md-6 col-xs-6 invoice-id">
                                        <h4>NO: #'.id_format($data['sale']->id).'</h4>
                                        <h5>'.dateConvert($data['sale']->payment_date, 'client', dateFormat()).'</h5>
                                    </div>
                                </div>
                                <div class="row customer-row">
                                    <div class="col-md-12 col-xs-12 company">'
                                        .(@$data['template']->logo ?
                                        '<a href="#" class="company-logo"><img src="'.$data['template']->logo.'" alt=""></a>' : null).
                                        @$data['template']->header.
                                    '</div>
                                </div>
                            </div>
                            <div class="invoice-body">
                                '.$customer_template.'
                                <table class="table table-hover">
                                    <thead>
                                        <tr>
                                            <th></th>
                                            <th>'.__('Ürün').'</th>
                                            <th class="text-right">'.__('Adet').'</th>
                                            <th class="text-right">'.__('Fiyat').'</th>
                                            <th class="text-right">'.__('Toplam').'</th>
                                        </tr>
                                    </thead>
                                    <tbody>';
                                    foreach($data['products'] as $key => $product):
                                        $sub_total = @$sub_total + $product['price'];
                                        $html .= '
                                        <tr>
                                            <td>'.($key+1).'</td>
                                            <td>'.$product['title'].'</td>
                                            <td class="text-right">'.$product['quantity'].'</td>
                                            <td class="text-right">'.numberFormat($product['price'], 2).'</td>
                                            <td class="text-right">'.numberFormat($product['total'], 2).'</td>
                                        </tr>';
                                    endforeach;
                                    $html .= '
                                        <tr>
                                            <td colspan="3"></td>
                                            <td class="text-right"><strong>'.__('Ara Toplam:').'</strong></td>
                                            <td class="text-right">'.$data['location']['currency'] . numberFormat($data['sale']->sub_total, 2).'</td>
                                        </tr>
                                        <tr>
                                            <td colspan="3"></td>
                                            <td class="text-right"><strong>'.__('Vergi:').'</strong></td>
                                            <td class="text-right">
                                                '.numberFormat($data['sale']->tax, 2).'
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="3"></td>
                                            <td class="text-right"><strong>'.__('TOPLAM:').'</strong></td>
                                            <td class="text-right">
                                                '.$data['location']['currency'] . numberFormat($data['sale']->amount, 2).'
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="3"></td>
                                            <td class="text-right"><strong>'.__('Alınan:').'</strong></td>
                                            <td class="text-right">
                                                '.$data['location']['currency'] . numberFormat($data['sale']->cash + $data['sale']->credit + $data['sale']->bank + $data['sale']->voucher + $data['sale']->ticket + $data['sale']->sodexo, 2).'
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="3"></td>
                                            <td class="text-right"><strong>'.__('Para üstü:').'</strong></td>
                                            <td class="text-right">
                                                '.$data['location']['currency'] . numberFormat($data['sale']->amount - ($data['sale']->cash + $data['sale']->credit + $data['sale']->bank + $data['sale']->voucher + $data['sale']->ticket + $data['sale']->sodexo), 2).'
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div class="invoice-footer">
                                '.@$data['template']->footer.'
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>';
       
        if ($return) {
            return $html;
        }
        messageAJAX('success', NULL, $html); 
    }

    function getSaleById()
    {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('saleid' , 'Satış ID', 'trim|required|integer');
        $this->form_validation->set_rules('session' , 'Oturum', 'trim|required');

        if (!$this->form_validation->run()) {
            messageAJAX('error' , validation_errors());
        }

        $data[ 'saleid' ]  = $this->input->post('saleid');
        $data[ 'session' ] = $this->input->post('session', TRUE);

        $session = $this->db
            ->where('session', $data['session'])
            ->get('return_keys', 1)->row();
        if (!$session) {
            messageAJAX('error', __('Oturum bulunamadı.'));
        }

        $this->load->model('Sales_model');
        $this->db->where('return_id', NULL); // İade ise getirme.
        $sale = $this->Sales_model->invoice_data($data[ 'saleid' ]);
        if (!$sale) {
            messageAJAX('error', __('Satış bulunamadı.'));
        }

        $table = '';
        foreach ($sale[ 'products' ] as $product) {
            $dropdown = '';

            for ($i=0; $i <= $product[ 'quantity' ]; $i++) {
                $dropdown.= '<option value="'.$i.'">'.$i.'</option>';
            }

            $table.= '
            <tr>
                <td>'.$product[ 'title' ].'</td>
                <td>'.$product[ 'price' ].'</td>
                <td>'.$product[ 'quantity' ].'</td>
                <td>'.$product[ 'total' ].'</td>
                <td>
                    <select name="products['.$product[ 'product_id' ].']" calss="form-control">'.$dropdown.'</select>
                </td>
            </tr>';
        }

        messageAJAX('success', NULL, [ 'sale' => $sale, 'html' => $table, 'data' => $data ]);

    }


    function calculateReturn()
    {
        if ($this->input->is_ajax_request()) {

            $products = $this->input->post('products[]');
            $sale_id  = $this->input->post('saleid');
            $location = $this->input->post('location');
            $session = $this->input->post('session', TRUE);

            $session_ = $this->db
            ->where('session', $session)
            ->get('return_keys', 1)->row();
            if (!$session_) {
                messageAJAX('error', __('Oturum bulunamadı.'));
            }

            if (! in_array($location, $this->user->locations)) {

                messageAJAX('error', __('Geçersiz Restoran.'));
            }

            $this->load->model('Sales_model');
            $sale = $this->Sales_model->invoice_data($sale_id);
            if (!$sale) {
                messageAJAX('error', __('Satış bulunamadı.'));
            }

            $sale_has_products = $this->db
                ->where('sale_id', $sale_id)
                ->where('active !=', 3)
            ->get('sale_has_products')->result();

            foreach ($sale_has_products as $product) {
                $products[ $product->product_id ] = $product->quantity - $products[ $product->product_id ];
            }

            $data = $this->_calculate($location, (array) $products, NULL);
            
            $data[ 'payable' ]          = round($sale[ 'sale' ]->amount - $data[ 'total' ], 2);
            $data[ 'payable_formated' ] = numberFormat($sale[ 'sale' ]->amount - $data[ 'total' ], 2);

            messageAJAX('success', NULL, $data);
        }
    }

    function completeReturn1()
    {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('products[]' , 'Ürünler', 'trim|required', ['required'=>'En az bir ürün gerekli.']);
        $this->form_validation->set_rules('payment-type' , 'Ödeme tipi', 'trim|required|in_list[cash,credit]', ['required'=>'Ödeme tipini seçin.']);
        $this->form_validation->set_rules('location' , 'Restoran', 'trim|required');
        $this->form_validation->set_rules('saleid' , 'Satış ID', 'trim|required');
        $this->form_validation->set_rules('desc' , 'İade sebebi', 'trim|required');
        $this->form_validation->set_rules('session' , 'Oturum', 'trim|required');

        
        if (!$this->form_validation->run()) {

            messageAJAX('error' , validation_errors());
        }

        $form[ 'products' ]     = $this->input->post('products[]');
        $form[ 'location' ]     = $this->input->post('location');
        $form[ 'sale_id' ]      = $this->input->post('saleid');
        $form[ 'payment_type' ] = $this->input->post('payment-type');
        $form[ 'desc' ]         = $this->input->post('desc', TRUE);
        $form[ 'session' ]      = $this->input->post('session', TRUE);
        $form[ 'created' ]      = _date();
        $data                   = NULL;
       

        if (! in_array($form[ 'location' ], $this->user->locations)) {
            messageAJAX('error', __('Geçersiz Restoran.'));
        }

        $this->load->model('Sales_model');
        $sale = $this->Sales_model->invoice_data($form[ 'sale_id' ]);
        if (!$sale) {
            messageAJAX('error', __('Satış bulunamadı.'));
        }

        $session = $this->db
            ->where('session', $form[ 'session' ])
        ->get('return_keys', 1)->row();
        if (!$session) {
            messageAJAX('error', __('Oturum bulunamadı.'));
        }

        // PRODUCTS
        foreach ($form[ 'products' ] as $product => $quantity) {

            if ($quantity && is_numeric($quantity)) {

                $data[ 'products' ][ $product ] = $quantity;

                $this->db->where_in('P.id', (int)$product);
                $this->db->limit(1);
                $_products = $this->Products_model->index( $form[ 'location' ] )
                OR messageAJAX('error', __('Geçersiz Ürün.'));

                $_product = $_products[ 0 ];

                // PREPARE SALE_HAS_PRODUCTS ARRAY
                $data[ 'sale_has_products' ][ $product ] = [
                    'sale_id'    => 0,
                    'product_id' => $_product->id,
                    'quantity'   => $quantity,
                    'price'      => $_product->price,
                    'content'    => $_product->content,
                    'digital'    => $_product->digital,
                    'is_video'   => $_product->is_video,
                    'title'      => $_product->title,
                    'type'       => $_product->type,
                    'created'    => $form[ 'created' ],
                    'active'     => 1,
                    'tax'        => $_product->tax,
                ];

            }
        }

        if (!isset($data[ 'sale_has_products' ])) {
            messageAJAX('error', __('En az bir ürün gerekli.'));
        } elseif (!count($data[ 'sale_has_products' ])) {
            messageAJAX('error', __('En az bir ürün gerekli.'));
        }
        // END - PRODUCTS

        $data[ 'calculate' ]        = $this->_calculate($form[ 'location' ], (array) $data[ 'products' ], NULL);
        // $data[ 'payable' ]          = round($sale[ 'sale' ]->amount - $data[ 'calculate' ][ 'total' ], 2);
        // $data[ 'payable_formated' ] = numberFormat($sale[ 'sale' ]->amount - $data[ 'calculate' ][ 'total' ], 2);

        if ($form[ 'payment_type' ] == 'cash') {
            $data[ 'payment_type' ] = 1;
            $data[ 'calculate' ][ 'paid_cash' ] = $data[ 'calculate' ][ 'total' ];
        }
        elseif ($form[ 'payment_type' ] == 'credit') {
            $data[ 'payment_type' ] = 2;
            $data[ 'calculate' ][ 'paid_credit' ] = $data[ 'calculate' ][ 'total' ];
        } else {
            messageAJAX('error', __('Geçersiz ödeme tipi.'));
        }


        // PREPARE SALES
        $data['sales'] = [
            'location_id'     => $form[ 'location' ],
            'pos_id'          => $sale[ 'sale' ]->pos_id,
            'sale_type_id'    => 1,
            'payment_type_id' => $data[ 'payment_type' ],
            'payment_date'    => $form[ 'created' ],
            'user_id'         => $this->user->id,
            'amount'          => $data[ 'calculate' ][ 'total' ],
            'subtotal'        => $data[ 'calculate' ][ 'subtotal' ],
            'change'          => 0.00,
            // 'tax'             => $data[ 'calculate' ][ 'location_tax' ],
            'cash'            => $data[ 'calculate' ][ 'paid_cash' ],
            'credit'          => $data[ 'calculate' ][ 'paid_credit' ],
            'voucher'         => $data[ 'calculate' ][ 'paid_voucher' ],
            'discount_amount' => NULL,
            'discount_type'   => NULL,
            'desc'            => $form[ 'desc' ],
            'created'         => $form[ 'created' ],
            'active'          => 1,

            'return_id'       => $form[ 'sale_id' ],
        ];

        if (! $this->db->insert('sales', $data[ 'sales' ])) {
            messageAJAX('error', __('Bir hata oluştu.'));
        }

        $data['sales'] = [ 'id' => $this->db->insert_id() ] + $data[ 'sales' ];
        // END - PREPARE SALES


        // UPDATE MAIN SALE
        $this->db->where('id', $form[ 'sale_id' ]);
        $this->db->update('sales', [ 'return' => 1, 'modified' => $form[ 'created' ] ]);


        // PREPARE PRODUCT IN SALE $product => $quantity
        foreach($data[ 'sale_has_products' ] as $product => $_product) {

            $data[ 'sale_has_products' ][ $product ][ 'sale_id' ] = $data[ 'sales' ][ 'id' ];
    
            $this->db->insert('sale_has_products', $data[ 'sale_has_products' ][ $product ]);
        }
        // END PREPARE PRODUCT IN SALE

        // DELETE SESSION
        $this->db
            ->where('id', $session->id)
        ->delete('return_keys');
        

        $this->load->model('Returns_model');
        $invoice = $this->Returns_model->invoice($data[ 'sales' ][ 'id' ]);
        if ($invoice) {
            $html = '
                    <div class="row">
                        <div class="col-md-12">
                            <div class="panel">
                                <div class="panel-heading">
                                    <div class="panel-title">
                                        <h4>'.__('FATURA').'</h4>
                                    </div>
                                </div>
                                <div class="panel-body">

                                    <div class="invoice">
                                        <div class="invoice-heading">
                                            <div class="row date-row">
                                                <div class="col-md-6 col-xs-6">
                                                    <div id="invoice_barcode"></div>
                                                </div>
                                                <div class="col-md-6 col-xs-6 invoice-id">
                                                    <h4>'.__('NO:').'#'.id_format($invoice['sale']->id).'</h4>
                                                    <h5>'.dateConvert($invoice['sale']->payment_date, 'client', dateFormat()).'</h5>
                                                </div>
                                            </div>
                                            <div class="row customer-row">
                                                <div class="col-md-12 col-xs-12 company">'
                                                    .(@$data['template']->logo ?
                                                    '<a href="#" class="company-logo"><img src="'.@$data['template']->logo.'" alt=""></a>' : null).
                                                    @$data['template']->header.
                                                '</div>
                                            </div>
                                        </div>
                                        <div class="invoice-body">
                                            <table class="table table-hover">
                                                <thead>
                                                    <tr>
                                                        <th></th>
                                                        <th>'.__('Ürün').'</th>
                                                        <th class="text-right">'.__('Adet').'</th>
                                                        <th class="text-right">'.__('Fiyat').'</th>
                                                        <th class="text-right">'.__('Toplam').'</th>
                                                    </tr>
                                                </thead>
                                                <tbody>';
                                                foreach($invoice['products'] as $key => $product):
                                                    // $sub_total = @$sub_total + $product->price;
                                                    $html .= '
                                                    <tr>
                                                        <td>'.($key+1).'</td>
                                                        <td>'.$product->title.'</td>
                                                        <td class="text-right">'.$product->quantity.'</td>
                                                        <td class="text-right">'.$product->price.'</td>
                                                        <td class="text-right">'.numberFormat(round($product->quantity * $product->price, 2), 2).'</td>
                                                    </tr>';
                                                endforeach;
                                                $html .= '
                                                    <tr>
                                                        <td colspan="3"></td>
                                                        <td class="text-right"><strong>'.__('Ara Toplam:').'</strong></td>
                                                        <td class="text-right">'.@$this->user->locations_array[ $invoice['sale']->location_id ][ 'currency '] . $invoice['sale']->subtotal.'</td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="3"></td>
                                                        <td class="text-right"><strong>'.__('Vergi:').'</strong></td>
                                                        <td class="text-right">
                                                            '.numberFormat($invoice['sale']->tax, 2).'
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="3"></td>
                                                        <td class="text-right"><strong>'.__('TOPLAM:').'</strong></td>
                                                        <td class="text-right">
                                                            '.@$this->user->locations_array[ $invoice['sale']->location_id ][ 'currency '] . ' -' . $invoice['sale']->amount.'
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                        <div class="invoice-footer">
                                            '.@$data['template']->footer.'
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>';
        }

        messageAJAX('success', __('Ödeme tamamlandı.'), [
            'id' => $data[ 'sales' ][ 'id' ],
            'invoice' => @$html
        ]);
    }

    function createZipBooxAccount($ticket_number = FALSE, $query = FALSE, $sale_id = FALSE)
    {
        $ticket_number = sanitize($ticket_number);
        $sale_id       = (int) $sale_id;
        $query         = sanitize($query);
        $query         = $query ? $query : 1;
        $folder        = FCPATH . 'assets/uploads/photos/';
        $folder_pure   = 'assets/uploads/photos/';

        if (!strlen($ticket_number)) {
            return [
                'status'  => 'ERROR',
                'message' => __('Geçersiz bilet numarası.'),
                'count'   => 0,
                'items'   => []
            ];
        }
        

        // Check Ticket
        $data[ 'tickets' ] = $this->db->query("   SELECT * FROM
                                        (
                                            SELECT id, title, location_id, folder
                                            FROM photos
                                            WHERE $query
                                            AND title REGEXP '^{$ticket_number}[\._]'
                                            ORDER BY id DESC
                                        ) AS tickets
                                        ORDER BY id ASC ")->result();

        if (! $data[ 'tickets' ]) {
            return [
                'status'  => 'ERROR',
                'message' => 'Ticket not found in the system.',
                'count'   => 0,
                'items'   => []
            ];
        }

        // Check sale
        $this->db->select('payment_date, location_id');
        if ($sale_id) {
            $this->db->where('id', $sale_id);
        }
        $this->db
            ->where('ticket', $ticket_number)
            ->where('location_id', $data[ 'tickets' ][ 0 ]->location_id)
            ->where('active', 1);
        $data[ 'sale' ] = $this->db->get('sales', 1)->row();


        if (! $data[ 'sale' ]) {
            return [
                'status'  => 'ERROR',
                'message' => 'No Sale found has assigmented with this Ticket Number.',
                'count'   => 0,
                'items'   => []
            ];
        }


        $data[ 'inputs' ] = [
            'ticket'   => $ticket_number,
            'location' => $data[ 'sale' ]->location_id,
            'date'     => $data[ 'sale' ]->payment_date
        ];

        // Get Images
        $data[ 'images' ] = NULL;
        foreach ($data[ 'tickets' ] as $ticket) {
            
            $file_folder = $folder . $data[ 'sale' ]->location_id . '/' . $ticket->folder . '/';

            if (!empty($ticket->title) && file_exists($file_folder . $ticket->title)) {
                $data[ 'images' ][ $ticket->id ] = [
                    'name'  => $ticket->title,
                    'image' => $folder_pure . $data[ 'sale' ]->location_id . '/' . $ticket->folder . '/' . $ticket->title
                ];
            } else {
                return [
                    'status'  => 'ERROR',
                    'message' => 'Image not found. File: ' . $file_folder . $ticket->title,
                    'count'   => 0,
                    'items'   => []
                ];
            }
        }

        $data[ 'inputs' ][ 'images' ] = $data[ 'images' ];


        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, zipBooxURL() . 'api/createAccountByStpReport');
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data[ 'inputs' ]));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $server_output = curl_exec ($ch);
        curl_close($ch);

        return json_decode($server_output, TRUE);


    }


    function zipbooxTicket()
    {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('ticket' , 'Ticket Numbaer', 'trim|required');

        if (!$this->form_validation->run()) {
            messageAJAX('error' , validation_errors());
        }
        
        $zip_boox = $this->createZipBooxAccount($this->input->post('ticket', TRUE));

        messageAJAX(strtolower($zip_boox[ 'status' ]),  $zip_boox[ 'message' ]);
    }

    // ticket varsa ves ürün kontrolü yap.
    private function isVES()
    {
        $ticket_number = $this->input->post('ticket', TRUE);
        $location      = (int) $this->input->post('location');


        // Check VES package
        if ($ticket_number && !isset($_POST[ 'ves_form_checked_in' ])) {
            
            $ves_packages = $this->db
            ->select('sales.id sale_id, sales.ticket sale_ticket, sales.payment_date sale_date, sales.amount sale_amount,
                    SHP.id package_id, SHP.title package_title, SHP.quantity package_quantity,
                    PHP.id product_id, PHP.title product_title, PHP.quantity product_quantity, PHP.active product_checked_in')
                ->where('sales.ticket', $ticket_number)
                ->where('sales.location_id', $location)

                ->join('sale_has_products SHP', 'SHP.sale_id = sales.id')
                ->where('SHP.active', 1)
                ->where('SHP.type', 'ves')

                ->join('package_has_products PHP', 'PHP.package_id = SHP.id')

                ->get('sales')->result_array();

            if ($ves_packages) {
                ob_start();
                require FCPATH . 'application/views/pointofsales/ves_packages.php';
                $ves_html = ob_get_clean();

                messageAJAX('success', 'VES Package Found', [
                    'VES'  => TRUE,
                    'html' => $ves_html
                ]);
                
            }
        }
    }

    function checkingVes()
    {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('s', 's', 'trim|required|is_natural_no_zero');
        $this->form_validation->set_rules('pa', 'pa', 'trim|required|is_natural_no_zero');
        $this->form_validation->set_rules('pr', 'pr', 'trim|required|is_natural_no_zero');

        if (!$this->form_validation->run()) {
            messageAJAX('error' , validation_errors());
        }

        $data['sale']    = $this->input->post('s');
        $data['package'] = $this->input->post('pa');
        $data['product'] = $this->input->post('pr');

        $product = $this->db
        ->select('PHP.id product_id, PHP.title product_title, PHP.quantity product_quantity, PHP.active product_checked_in')
            ->where('sales.id', $data['sale'])

            ->join('sale_has_products SHP', 'SHP.sale_id = sales.id')
            ->where('SHP.id', $data['package'])
            ->where('SHP.sale_id', $data['sale'])
            ->where('SHP.active', 1)
            ->where('SHP.type', 'ves')

            ->join('package_has_products PHP', 'PHP.package_id = SHP.id')
            ->where('PHP.id', $data['product'])
            ->where('PHP.package_id', $data['package'])

            ->get('sales')->row();

        if (!$product) {
            logs_($this->user->id, 'VES Product not found.', $data);
            exit();
        }

        if ($product->product_checked_in) {
            messageAJAX('error' , 'VES Product already checked in.', [ 'uncheck' => 1 ]);
        }

        $this->db->where('id', $data['product']);
        if ($this->db->update('package_has_products', [ 'active' => 1, 'modified' => _date() ])) {
            messageAJAX('success' , 'Product checked in.');
        }
    }

    function tables($location = NULL)
    {
        $this->check_auth();

        $data['current_location'] = (int) $location;

        if (!in_array($data[ 'current_location' ], $this->user->locations)) {
            $data[ 'current_location' ] = (int) @current($this->user->locations_array)[ 'id' ];
        }

        $floors = $this->db->where([
            'location_id' => $data['current_location'],
            'active' => 1,
        ])->get('floors')->result();

        $zones = $this->db->where([
            'location_id' => $data['current_location'],
            'active' => 1,
        ])->get('zones')->result();

        $tables = $this->db->where([
            'location_id' => $data['current_location'],
            'active' => 1,
            'sale_id !=' => NULL,
            'service_type' => 1
        ])->order_by('title', 'ASC')->get('points')->result();


        usort($tables, function($a, $b) 
        {   
           $a = (int) preg_replace('/[^0-9]/', '', $a->title);
           $b = (int) preg_replace('/[^0-9]/', '', $b->title);

           return $a > $b;
        });


        $html = null;
        foreach ($floors as $floor) {

            $html .='
            <div class="panel-group" id="floor-tab">
      
                <div class="panel panel-info">
                    <div class="panel-heading theme-dark-blue">
                        <a data-toggle="collapse" data-parent="#floor-tab" href="#floor-'.$floor->id.'">
                            <h4 class="panel-title">'.$floor->title.'</h4>
                        </a>
                    </div>
                    <div id="floor-'.$floor->id.'" class="panel-collapse collapse in">
                        <div class="panel-body">';


                        foreach ($zones as $zone) {
                            if ($zone->floor_id != $floor->id) {
                                continue;
                            }

                            $html .='
                            <div class="panel-group" id="zone-tab">
                  
                            <div class="panel panel-info">
                                <div class="panel-heading">
                                    <a data-toggle="collapse" data-parent="#zone-tab" href="#zone-'.$zone->id.'">
                                        <h4 class="panel-title">'.$zone->title.'</h4>
                                    </a>
                                </div>
                                <div id="zone-'.$zone->id.'" class="panel-collapse collapse in">
                                    <div class="panel-body">';

                                if ($tables) {

                                    $html .= '<div class="table-responsive">
                                                <table class="table table-hover">
                                                    <thead>
                                                        <tr>
                                                            <th>'.__('Masa').'</th>
                                                            <th>'.__('Personel').'</th>
                                                            <th>'.__('Açılış').'</th>
                                                            <th>'.__('Fiyat').'</th>
                                                            <th>'.__('Durum').'</th>
                                                            <th></th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>';
                                    foreach ($tables as $key => $table) {
                                        if ($table->floor_id == $floor->id && $table->zone_id == $zone->id) {
                                        
                                            $user_name = '-';
                                            $opened_at = '-';
                                            $amount    = '-';
                                            $status    = '<span class="label label-success">BOŞ</span>';
                                            if ($table->sale_id) {
                                                $sale = $this->db->select('amount, (SELECT name FROM users WHERE id = user_id LIMIT 1) as user_name')->where([
                                                 'id' => $table->sale_id   
                                                ])->order_by('id', 'DESC')->get('sales', 1)->row();

                                                $user_name = @$sale->user_name;
                                                $status    = '<span class="label label-danger">DOLU</span>';
                                                $amount    = numberFormat(@$sale->amount, 2);
                                                $opened_at = dateConvert($table->opened_at, 'client', dateFormat());
                                            }

                                            $html.= "
                                                <tr>
                                                    <td>{$table->title}</td>
                                                    <td>{$user_name}</td>
                                                    <td>{$opened_at}</td>
                                                    <td>{$amount}</td>
                                                    <td>{$status}</td>
                                                    <td class=\"text-right\"><a href=\"".base_url('pointofsales/#point:' . $table->id)."\" class=\"btn btn-success btn-blue btn-ripple\">SEÇ</a></td>
                                                </tr>
                                            ";
                                        }
                                    }

                                    $html .= '</tbody>
                                        </table>
                                    </div>';
                                }

                            $html .='
                                        </div>
                                    </div>
                                </div>
                            </div>';
                        }


            $html .='
                        </div>
                    </div>
                </div>
            </div>';
        }


        $this->load->template('pointofsales/tables', ['html' => $html] + $data);
    }

    function orders1($location = NULL)
    {
        $data[ 'current_location' ] = (int) $location;
        $data[ 'current_production' ] = (int) ltrim($this->input->get('production'), '/');
        $data[ 'tab' ] = $this->input->get('tab');
        $data[ 'last_total_amount' ] = 0;

        if (!in_array($data[ 'current_location' ], $this->user->locations)) {
            $data[ 'current_location' ] = (int) @current($this->user->locations_array)[ 'id' ];
        }

        $this->theme_plugin = [
            'js'    => [ 'panel/js/view/pointofsales.js' ],
            'start' => 'TablesDataTables.init(); Orders.init('.$data[ 'current_location' ].')',
        ];

        if ($this->input->is_ajax_request()) {

            $function_name = 'orders';

            // if (!in_array($location, $this->user->locations)) {
            //     return;
            // }


            $this->db->select('*,
                (SELECT title FROM points WHERE id = point_id LIMIT 1) as table_name,
                (SELECT name FROM users WHERE id = user_id LIMIT 1) as employe');
            
            if ($data[ 'tab' ]) {
            	switch ($data[ 'tab' ]) {
            		case 'today-completed':
            			$this->db->where('status', 'completed');
            			$this->db->where('created >=', date('Y-m-d H:i:s', strtotime('-24 hours')));
            			break;
            		case 'all-completed':
            			$this->db->where('status', 'completed');
            			break;
            		default: //today-active
            			$this->db->where_in('status', ['pending', 'completed']);
                        $this->db->where('created >=', date('Y-m-d H:i:s', strtotime('-24 hours')));
            			break;
            	}
            } else {
            	$this->db->where_in('status', ['pending', 'completed']);
                $this->db->where('created >=', date('Y-m-d H:i:s', strtotime('-24 hours')));
            }

            $this->db->where([
                'active'     => 1,
                'location_id' => $data[ 'current_location' ]
                // 'created >=' => date('Y-m-d H:i:s', strtotime('-48 hours'))
            ])
            ->order_by('id', 'DESC');
            $sales = $this->db->get('sales')->result_array();

            if (!$sales) {
                return;
            }

            $max = null;
            $tmp = [];
            $_completed = [];
            foreach ($sales as $item) {

                if ($data[ 'tab' ] == 'today-active') {
                    
                    if ($item['status'] == 'pending') {
                        $tmp[$item['id']] = $item;
                    } else {
                        $_completed[$item['id']] = $item;
                    }

                } else {
                    $tmp[$item['id']] = $item;
                }

                // $max = ($max === null) ? strtotime($item['created']) : max($max, strtotime($item['created']));

                
                $data[ 'last_total_amount' ] += $item['amount'];
            }
            $sales = $tmp + $_completed;
            unset($tmp);

            if (!$sales) {
                return;
            }

            if ($data[ 'current_production' ]) {
                $this->db->join('products As Products', 'Products.id = SaleHasProducts.product_id');
                $this->db->where('Products.production_id', $data[ 'current_production' ]);
            }
            $orders = $this->db
            ->select('SaleHasProducts.*')
            ->where('SaleHasProducts.active', 1)
            ->where('(SaleHasProducts.quantity - SaleHasProducts.completed) >', 0)
            ->where_in('SaleHasProducts.sale_id', array_column($sales, 'id'))
            ->order_by('SaleHasProducts.sale_id', 'ASC')
            ->order_by('SaleHasProducts.id', 'ASC')
            ->get('sale_has_products As SaleHasProducts')->result();

            if (!$orders) {
                return;
            }

            $sounds = $this->db
            ->where_in('sale_id', array_column($sales, 'id'))
            ->get('sound_notes')->result();

            $tmp = null;
            if ($sounds) {
                foreach ($sounds as $key => $sound) {
                    $tmp[$sound->sale_id] = 1;
                }
            }
            $sounds = $tmp;
            unset($tmp);

            $html = $this->load->view('pointofsales/orders_table', compact('sales', 'orders', 'sounds', 'function_name'), true);

            $tab = $data[ 'tab' ];
            $last_total_amount = $data[ 'last_total_amount' ];
            messageAJAX('success', null, compact('html', 'max', 'tab', 'last_total_amount'));
        }
        
        $data['productions'] = $this->Productions->getAll();
        $data['productions'] = object_to_array($data['productions']);

        $this->load->template('pointofsales/orders', $data);
    }

    function completeOrder()
    {
        if ($this->input->is_ajax_request()) {

            $id = (int) $this->input->post('id');
            $quantity = (int) $this->input->post('quantity');

            if (!$quantity) {
                return;
            }

            if(!$sale_has_product = $this->exists('sale_has_products', "id = $id AND active = 1 AND (quantity - completed) = $quantity", '*')) {
                messageAJAX('error' , __('Geçersiz Sipariş.'));
            }

            if (!$this->exists('sales', "id = {$sale_has_product->sale_id} AND active = 1 AND status IN('pending', 'completed')", 'id')) {
                messageAJAX('error' , __('Geçersiz Satış.'));
            }

            $this->db->where('id', $id);
            $this->db->set('completed', "(completed + $quantity)", false);
            $this->db->update('sale_has_products', ['active' => 1, 'modified' => _date(), 'seen' => 0]);

            messageAJAX('success' , __('Sipariş tamamlandı.'));
        }
    }

    function cancelOrder()
    {
        if ($this->input->is_ajax_request()) {

            $id       = (int) $this->input->post('id');
            $pos      = (int) $this->input->post('pos');
            $location = (int) $this->input->post('location');

            if (!$this->exists('points', "id = $id and active = 1")) {
                messageAJAX('error', __('Geçersiz sipariş.'));
            }

            $data['sale'] = $this->db->where([
                'active'      => 1,
                'point_id'    => $id,
                'location_id' => $location,
                'pos_id'      => $pos,
                'return_id'   => null,
                'status'      => 'pending',
                // 'created >='  => date('Y-m-d H:i:s', strtotime('-48 hours'))
            ])
            ->order_by('id', 'DESC')
            ->get('sales', 1)->row();

            if ($data['sale']) {

                $sale_has_completed_product = $this->db->where([
                    'sale_id' => $data['sale']->id,
                    'completed >=' => 1,
                    'active' => 1 
                ])->get('sale_has_products', 1)->row();
                if ($sale_has_completed_product) {
                    messageAJAX('error', __('Ürün teslim edilmiş masanın hesabı kapatılamaz.'));
                }

                $this->db->where('id', $id)
                ->update('points', [
                    'opened_at' => null,
                    'status'    => 0
                ]);

                $this->db->where('id', $data['sale']->id)
                ->update('sales', [
                    'modified' => _date(),
                    'status'   => 'cancelled'
                ]);

                messageAJAX('success', __('Sipariş iptal edildi.'));
            }
        }
    }

    public function getCompletedOrders()
    {
        return;

        if ($this->input->is_ajax_request()) {
            
            $location = (int) $this->input->post('location');
            $function_name = 'getCompletedOrders';

            if (!in_array($location, $this->user->locations)) {
                messageAJAX('error', __('Geçersiz lokasyon.'));
            }


            $sales = $this->db->select('*,
                (SELECT title FROM points WHERE id = point_id LIMIT 1) as table_name,
                (SELECT name FROM users WHERE id = user_id LIMIT 1) as employe')
            ->where([
                'active'     => 1,
                "status IN('pending', 'completed')",
                'location_id'     => $location,
                // 'created >=' => date('Y-m-d H:i:s', strtotime('-48 hours')),
            ])->order_by('id', 'DESC')->get('sales')->result_array();

            if (!$sales) {
                return;
            }

            $tmp = null;
            foreach ($sales as $s) {
                $tmp[$s['id']] = $s;
            }
            $sales = $tmp;
            unset($tmp);

            $orders = $this->db
            ->where('active', 1)
            ->where('seen', 0)
            ->where('(quantity - completed) =', 0, false)
            ->where_in('sale_id', array_column($sales, 'id'))
            ->order_by('sale_id', 'ASC')
            ->order_by('id', 'ASC')
            ->get('sale_has_products')->result();

            if (!$orders) {
                return;
            }


            ob_start();
            require FCPATH . 'application/views/pointofsales/orders_table.php';
            exit(ob_get_clean());
        }
    }

    public function doSeenOrder()
    {
        if ($this->input->is_ajax_request()) {

            $id = (int) $this->input->post('id');

            if(!$sale_has_product = $this->exists('sale_has_products', "id = $id AND active = 1 AND seen = 0", '*')) {
                messageAJAX('error' , __('Geçersiz Sipariş.'));
            }

            if (!$this->exists('sales', "id = {$sale_has_product->sale_id} AND active = 1 AND status IN('pending', 'completed')", 'id')) {
                messageAJAX('error' , __('Geçersiz Satış.'));
            }

            $this->db->where('id', $id);
            $this->db->update('sale_has_products', ['active' => 1, 'modified' => _date(), 'seen' => 1]);

            messageAJAX('success' , __('Sipariş tamamlandı.'));
        }
    }

    public function saveSound()
    {
        if ($this->input->is_ajax_request()) {

            $location = (int) $this->input->post('location');
            $table_id = (int) $this->input->post('table_id');

            if (!in_array($location, $this->user->locations)) {
                messageAJAX('error', __('Geçersiz Restoran.'));
            }

            $sale = $this->db->select('id')
            ->where([
                'point_id'    => $table_id,
                'location_id' => $location,
                'active'      => 1,
                'status'      => 'pending',
                // 'created >='  => date('Y-m-d H:i:s', strtotime('-48 hours')),
            ])->order_by('id', 'DESC')->get('sales', 1)->row();

            if (!$sale) {
                messageAJAX('error', __('Masa Boş.'));
            }


            $this->load->helper('string');
            $config['upload_path']   = self::sound_path;
            $config['allowed_types'] = '*'; // çalışmadı audio/wav veya wav
            $config['file_name']     = sprintf('%d-%s-%s-%s-%s-%s',
                                            $this->user->id,
                                            strtoupper(random_string('alnum', 4)),
                                            strtoupper(random_string('alnum', 4)),
                                            strtoupper(random_string('alnum', 4)),
                                            strtoupper(random_string('alnum', 4)),
                                            str_replace('.', '_', microtime(1))).'.wav';

            @mkdir(FCPATH . self::sound_path);
            $this->load->library('upload', $config);
            if ($this->upload->do_upload('audio')) {
                $values['file'] = $config['upload_path'] . '/' . $this->upload->data()['file_name'];
                $values['sale_id'] = $sale->id;
                $values['created'] = _date();
                $values['active']  = 1;
                $values['is_new']  = 1;
            } else {
                messageAJAX('error', $this->upload->display_errors());
            }


            $this->db->insert('sound_notes', $values);

            messageAJAX('success', __('Ses kaydı oluşturuldu.'));
        }
    }

    function moveTable()
    {
        $this->check_auth();

        $_products = ['update_sale' => [], 'update_products' => [], 'delete_products' => []];
        
        if (!$this->input->is_ajax_request()) {
            return;
        }

        $location = (int) $this->input->post('location');
        $order_id = (int) $this->input->post('order_id');
        $target_table_id = (int) $this->input->post('target_table_id');

        if (!in_array($location, $this->user->locations)) {
            messageAJAX('error', __('Geçersiz Restoran.'));
        }

        $current_sale = $this->db
        ->where('id', $order_id)
        ->get('sales', 1)->row();
        if (!$current_sale) {
            messageAJAX('error', __('Geçersiz masa.'));
        }

        if ($target_table_id == $current_sale->point_id) {
            messageAJAX('error', __('Lütfen farklı bir masa seçiniz.'));
        }

        $target_sale = $this->db
        ->where([
            'point_id' => $target_table_id,
            'status'   => 'pending',
            'active'   => 1
        ])
        ->order_by('id', 'desc')
        ->get('sales', 1)->row();
        // Hedef masa doluysa ürünler merge edilecek...
        if ($target_sale) {
            

            $current_products = $this->db->where([
                'sale_id' => $current_sale->id,
                'active' => 1
            ])->get('sale_has_products')->result();;
            // Taşınan masa da ürün var mı?
            if ($current_products) {

                $target_products = $this->db->where([
                    'sale_id' => $target_sale->id,
                    'active' => 1
                ])->get('sale_has_products')->result();
                // Hedef masada ürün var mı?
                if ($target_products) {

                    
                    $_processeds = [];
                    foreach ($current_products as $key_current => $current_product) {
                        foreach ($target_products as $key_target => $target_product) {

                            // hedef masada zaten var olan ürünleri güncelle ve eskilerini sil.
                            if ($current_product->product_id == $target_product->product_id) {          

                                $_products['update_products'][$current_product->id] = [
                                    'sale_id' => $target_product->sale_id,
                                    'quantity' => $target_product->quantity + $current_product->quantity,
                                    'completed' => $target_product->completed + $current_product->completed,
                                    'seen' => (!$target_product->seen || !$current_product->seen) ? 0 : 1,

                                    // ARA ÖDEMELER
                                    'paid_cash' => $target_product->paid_cash + $current_product->paid_cash,
                                    'paid_credit' => $target_product->paid_credit + $current_product->paid_credit,
                                    'paid_voucher' => $target_product->paid_voucher + $current_product->paid_voucher,
                                ];

                                $_products['delete_products'][$target_product->id] = $target_product->id;
                                
                            } 

                            // Hedef masada olmayan ürünlerin satış id sini yeni masayla değiştir.
                            else {

                                $_products['update_sale'][$current_product->id] = [
                                    'sale_id' => $target_product->sale_id
                                ];
                            }
                        }
                    }

                }
            }


            // Taşınan masayı kapat.
            $this->db->where('id', $current_sale->id)
            ->update('sales', ['status' => 'cancelled']);   

            // Taşınan masa verilerini hedef masaya aktar
            $this->db->where('id', $target_sale->id)
            ->update('sales', [
                'amount' => $target_sale->amount + $current_sale->amount,
                'subtotal' => $target_sale->amount + $current_sale->amount
            ]);




            foreach ($_products['update_products'] as $sale_has_product_id => $data) {
                $this->db->where('id', $sale_has_product_id);
                $this->db->update('sale_has_products', $data);
            }
            foreach ($_products['update_sale'] as $sale_has_product_id => $data) {
                $this->db->where('id', $sale_has_product_id);
                $this->db->update('sale_has_products', $data);
            }
            foreach ($_products['delete_products'] as $sale_has_product_id => $data) {
                $this->db->where('id', $sale_has_product_id);
                $this->db->delete('sale_has_products');
            }

        } else {

            // satışın masasını seçilen masa yap.
            $this->db->where('id', $order_id)
            ->update('sales', ['point_id' => $target_table_id]);


            // Hedef masayı aç
            $this->db->where('id', $target_table_id)
            ->update('points', ['status' => 1, 'opened_at' => _date()]);
        }

        
        // Taşına masayı kapat
        $this->db->where('id', $current_sale->point_id)
        ->update('points', ['status' => 0, 'opened_at' => null]);



        messageAJAX('success', __('Masa taşındı.'));
    }

    public function getSounds()
    {
        if (!$this->input->is_ajax_request()) {
            return;
        }

        $id = (int) $this->input->post('id');

        $sounds = $this->db
        ->where('sale_id', $id)
        ->order_by('is_new', 'DESC')
        ->order_by('id', 'DESC')
        ->get('sound_notes')->result_array();

        if (!$sounds) {
            return;
        }

        $table = null;
        foreach ($sounds as $key => $sound) {
        $table.= '
            <tr>
                <td>
                    <a class="btn btn-floating btn-primary play-sound-note" data-sound-src="'.base_url($sound['file']).'" data-id="'.$sound['id'].'"><i class="ion-play"></i></a>
                </td>
                <td>'.($sound['is_new'] ? '<span class="label label-info">Yeni</span>' : '').'</td>
                <td>'.$sound[ 'created' ].'</td>
            </tr>';
        }
        
        messageAJAX('success', NULL, ['html' => $table]);
    }

    public function doSeenSound($id = null)
    {
        $this->db->where('id', $id);
        $this->db->update('sound_notes', ['is_new' => null, 'modified' => _date()]);
    }


    public function loginSession()
    {
        if (!$this->input->is_ajax_request()) {
            return false;
        }

        $this->load->library('form_validation');
        $this->form_validation->set_rules('user', 'Kullanıcı Adı/Eposta', 'trim|required|min_length[4]|max_length[32]');
        $this->form_validation->set_rules('password', 'Şifre', 'trim|required|min_length[6]|max_length[32]');

        if (!$this->form_validation->run()) {
            messageAJAX('error', validation_errors());
        }

        $name     = $this->input->post('user', TRUE);
        $password = sha1( $this->input->post('password', TRUE) . $this->config->config['salt'] );

        $this->db->select('id, username, password, email, active, role_id, name');
        $this->db->where('email', $name);
        $this->db->or_where('username', $name);
        if ($user = $this->db->get('users')->row()) {

            if ($user->password == $password) {

                if (!$user->active) {
                    messageAJAX('error', __('Üzgünüz, hesabınız devre dışı bırakılmış.'));
                }

                if (! $this->db->where('active=1 AND id='.$user->role_id)->get('roles',1)->row()) {
                    messageAJAX('error', __('Üzgünüz, heabınızın yetki rolü devre dışı bırakılmış.'));
                }
                

                $this->db->update( 'users', ['lastip' => $this->input->ip_address(), 'lastlogin' => _date()], "id = {$user->id}" );

                

                if (!$this->session->userdata('sub-sessions')) {
                    $this->session->set_userdata('sub-sessions', []);
                }
                    
                $this->session->set_userdata('sub-sessions', $this->session->userdata('sub-sessions') + [$user->id => [
                    'id' => $user->id,
                    'name' => $user->name
                ]]);

                
                messageAJAX('success', __('Giriş başarılı.'));
            }
        }
        messageAJAX('error', __('Hatalı kullanışı veya şifre.'));
    }

    public function logoutSession($id = null)
    {
        if (isset($_SESSION['sub-sessions'][$id])) {
            unset($_SESSION['sub-sessions'][$id]);
        }
    }

    public function selectSession($id = null)
    {
        if (isset($_SESSION['sub-sessions'][$id])) {
            
            $current_user = $this->db
            ->where('id', $this->session->userdata('loggedin'))
            ->get('users', 1)->row();

            $this->session->set_userdata('loggedin', $id);

            $this->session->set_userdata('sub-sessions', $this->session->userdata('sub-sessions') + [$current_user->id => [
                'id' => $current_user->id,
                'name' => $current_user->name
            ]]);
        }
    }

    public function updateTextNote()
    {
        if (!$this->input->is_ajax_request()) {
            return;
        }

        $this->db
        ->where('id', (int) $this->input->post('sale_id'))
        ->update('sales', [
            'desc' => $this->input->post('note', true)
        ]);
    }


    

    protected function getInterimPaymentProducts()
    {
        $location_id = (int) $this->input->post('location_id');
        $order_id = (int) $this->input->post('sale_id');

        if (!in_array($location_id, $this->user->locations)) {
            messageAJAX('error', __('Geçersiz Restoran.'));
        }

        $sale = $this->db->where([
            'id'          => $order_id,
            'location_id' => $location_id,
            'active'      => 1,
            'status'      => 'pending'
        ])
        ->order_by('id', 'DESC')
        ->get('sales', 1)->row();
        if (!$sale) {
            messageAJAX('error', __('Sipariş geçersiz veya iptal edilmiş olabilir.'));
        }

        $sale_has_products = $this->db->where([
            'sale_id' => $sale->id,
        ])
        ->order_by('id', 'DESC')
        ->get('sale_has_products')->result_array();

        if (!$sale_has_products) {
            messageAJAX('error', __('Bu masada ürün bulunamadı.'));
        }

        // $has_paid = 0;
        // foreach ($sale_has_products as &$product) {

        //     $product['paid'] = $product['paid_cash'] + $product['paid_credit'] + $product['paid_voucher'];
        //     $product['unpaid'] = $product['quantity'] - $product['paid'];

        //     if (!$has_paid && $product['paid']) {
        //         $has_paid = 1;
        //     }
        // }

        $html = $this->load->view('pointofsales/interim_payments', compact('sale_has_products', 'has_paid'), true);

        messageAJAX('success', null, compact('html'));
    }

    protected function processInterimPayment()
    {
        $sale_has_products = $this->input->post('sale_has_products');
        $sale_id           = (int) $this->input->post('sale_id');
        $payment_type      = $this->input->post('payment_type');
        $date              = date('Y-m-d H:i:s');
        $updates           = $inserts = [];
        $payment_types     = [1 => 'cash', 2 => 'credit', 5 => 'voucher', 6 => 'ticket', 7 => 'sodexo'];

        if (!count($sale_has_products)) {
            messageAJAX('error', __('En az bir ürün seçmelisiniz.'));
        } elseif (!$payment_type_id = array_search($payment_type, $payment_types)) {
            messageAJAX('error', __('Geçersiz İşlem.'));
        }

        $sale = $this->db->where('id', $sale_id)->get('sales', 1)->row();
        if (!$sale) {
            messageAJAX('error', __('Geçersiz Sipariş'));
        }

        foreach ($sale_has_products as $sale_has_products_id => $quantity) {
            
            $quantity             = abs($quantity);
            $sale_has_products_id = (int) $sale_has_products_id;

            if (!$quantity) {
                messageAJAX('error', __('Her üründen en az bir adet seçilmeli.'));
            }

            $product = $this->db->where([
                'id'      => $sale_has_products_id,
                'sale_id' => $sale_id
            ])
            ->order_by('id', 'desc')
            ->get('sale_has_products', 1)->row();
            if (!$product) {
                messageAJAX('error', __('Geçersiz Sipariş'));
            }


                    
            $diff = $product->quantity - ($product->paid_cash + $product->paid_credit + $product->paid_voucher);
            if ($diff < $quantity) {
                messageAJAX('error', $product->title . ' için en fazla ' . $diff . ' adet ödeme yapabilirsiniz.');
            }

            if ($payment_type == 'cash') {
                $product->paid_cash += $quantity;
            } elseif ($payment_type == 'credit') {
                $product->paid_credit += $quantity;
            } elseif ($payment_type == 'voucher') {
                $product->paid_voucher += $quantity;
            } elseif ($payment_type == 'ticket') {
                $product->paid_ticket += $quantity;
            } elseif ($payment_type == 'sodexo') {
                $product->paid_sodexo += $quantity;
            }

            $updates[$product->id] = [
                'paid_cash'    => $product->paid_cash,
                'paid_credit'  => $product->paid_credit,
                'paid_voucher' => $product->paid_voucher,
                'paid_ticket'  => $product->paid_ticket,
                'paid_sodexo'  => $product->paid_sodexo,
                'modified'     => $date,
            ];

        }


        if ($updates) {
            foreach ($updates as $id => $update_data) {
                $this->db->where('id', $id)->update('sale_has_products', $update_data);
            }
        }

        messageAJAX('success', __('Ara Ödeme Alındı.'));
    }

    protected function getOperatorSession1()
    {
        if ($this->user->pin) {
            if (!$this->session->userdata('operator_user')) {
                messageAJAX('error', __('Lütfen oturum açınız.'));
            }
        }
    }

    protected function setOperatorSession1()
    {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('password', 'Şifre', 'trim|required');
        if (!$this->form_validation->run()) {
            messageAJAX('error' , validation_errors());
        }

        $password = (int) $this->input->post('password');

        $user = $this->db->select('id')->where([
            'active' => 1,
            'pin' => $password,
        ])->get('users', 1)->row();

        if (!$user) {
            messageAJAX('error' , __('Hesap bulunamadı!'));
        }

        $this->session->set_userdata('operator_user', $user->id);
        messageAJAX('success' , 'success');
    }

    protected function unSetOperatorSession()
    {
        $this->session->unset_userdata('operator_user');
    }

    protected function getScreenSaver()
    {
        $image = $this->website->screen_saver;
        $template = $this->load->view('pointofsales/elements/screen_saver', compact('image'), true);

        messageAJAX('success', 'success', compact('template'));
    }

}
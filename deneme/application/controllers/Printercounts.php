<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Printercounts extends Auth_Controller {

    function __construct()
    {
        parent::__construct();

        $this->check_auth([ 'allowed' => [ 'getLastCount' ] ]);
        
        $this->load->model('Printercounts_model');
    }

    function index($printer = NULL)
    {
        
        
        $this->theme_plugin = ['start' => 'TablesDataTables.init();'];

        $data['current_printer']  = (int) $printer;

        if (!$data['printer'] = $this->exists('printers', "id = ${data['current_printer']}", 'title, id, location_id')) {
            show_404();
        }
        $data['current_location'] = $data['printer']->location_id;

        $data += $data['printercounts'] = $this->Printercounts_model->index($data['current_printer']);

    	$this->load->template('PrinterCounts/index', $data);
    }

    function add($printer = NULL)
    {
        // $this->check_auth();
        
        $data['current_printer'] = (int) $printer;

        if (!$data['printer'] = $this->exists('printers', "id = ${data['current_printer']}", 'title, id, location_id')) {
            show_404();
        }
        $data['current_location'] = $data['printer']->location_id;

        if ($this->input->is_ajax_request()) {

            $this->load->library('form_validation');
            $this->form_validation->set_rules('date', 'Date', 'trim|required|is_date['.dateFormat().']|callback__unique_PrinterDate[ADD,'.$data['current_printer'].']');
            $this->form_validation->set_rules('counter', 'Counter', 'trim|required|greater_than[0]');

            if($this->form_validation->run()) {

                $this->Printercounts_model->add($data['current_printer']);
            }
            
            messageAJAX('error', validation_errors());

        } else {

            $this->theme_plugin = array(
                'js'    => array( 'panel/js/view/printercounts.js' ),
                'start' => 'MyFunction.datetimepicker("'.dateFormat(TRUE).'");
                            PrinterCounts.init();'
            );

            $this->load->template('PrinterCounts/add', $data);
        }       
    }

    function edit($printer = NULL, $id = NULL)
    {
        // $this->check_auth();
        
        $printer = (int) $printer;
        $id      = (int) $id;

        if(!$data['printerhascount'] = $this->exists('printer_has_counts', "id = $id", '*')) {
            show_404();
        }

        if(!$data['printer'] = $this->exists('printers', "id = $printer", 'title, id, location_id')) {
            show_404();
        }
        $data['current_location'] = $data['printer']->location_id;

        if ($this->input->is_ajax_request()) {
            
            $this->load->library('form_validation');
            $this->form_validation->set_rules('date', 'Date', 'trim|required|is_date['.dateFormat().']|callback__unique_PrinterDate[EDIT,'.$printer.', '.$id.']');
            $this->form_validation->set_rules('counter', 'Counter', 'trim|required|greater_than[0]');

            if($this->form_validation->run()) {

                $this->Printercounts_model->edit($printer, $id, $data);
            }
            
            messageAJAX('error', validation_errors());

        } else {
            
            $this->Printercounts_model->edit($printer, $id);

            $this->theme_plugin = array(
                'js'    => array( 'panel/js/view/printercounts.js' ),
                'start' => 'MyFunction.datetimepicker("'.dateFormat(TRUE).'");
                            PrinterCounts.init();'
            );

            $this->load->template('PrinterCounts/edit', $data);
        }      
    }

    // function delete($printer = NULL, $id = NULL)
    // {
    //     $this->check_auth();
        
    //     $printer = (int) $printer;
    //     $id      = (int) $id;

    //     if(!$data['printer'] = $this->exists('printers', "id = $printer", '*')) {

    //         show_404();
    //     }

    //     if(!$data['printer_count'] = $this->exists('printer_has_counts', "id = $id", '*')) {

    //         show_404();
    //     }

    //     $this->Printercounts_model->delete($id, $data);
    // }

    function multiple($location = NULL)
    {
        // $this->check_auth();
        
        if ($location && !in_array($location, $this->user->locations)) {
            error_('access');
        }

        $data['location'] = $this->exists('locations', "id = $location", '*');

        $this->load->model('Printers_model');

        if ($this->input->is_ajax_request()) {

            $this->load->library('form_validation');
            $this->form_validation->set_rules('date', 'Date', 'trim|required|is_date['.dateFormat().']');
            $this->form_validation->set_rules('printers[]', 'Printers', 'trim|required', ['required' => 'At least one printer required.']);
            $this->form_validation->set_rules('values[]', 'Printer Count', 'trim|is_natural|required');
            $this->form_validation->set_rules('unsold', 'Unsold', 'trim|is_natural');
            $this->form_validation->set_rules('waste', 'Waste', 'trim|is_natural');
            $this->form_validation->set_rules('comps', 'Comps', 'trim|is_natural');
            $this->form_validation->set_rules('unseen', 'Unseen', 'trim|is_natural');

            if($this->form_validation->run()) {
                $this->Printercounts_model->multiple($location);
            }
            
            messageAJAX('error', validation_errors());

        } else {

            $this->theme_plugin['start'] = 'MyFunction.datetimepicker("'.dateFormat(TRUE).'");';

            $data['printers'] = $this->Printers_model->index($data['location']->id);

            $this->load->template('PrinterCounts/multiple', $data);
        }

    }

    function _unique_PrinterDate($date, $str)
    {
        $params  = @explode(',', $str);
        $target  = @$params[0];
        $printer = (int) @$params[1];
        $id      = (int) @$params[2];

        $client_day_number = dateReFormat($date, dateFormat(), 'Y-m-d');
        $start_of_day      = dateConvert2($client_day_number . ' 00:00:00', 'server');
        $end_of_day        = dateConvert2($client_day_number . ' 23:59:59', 'server');
        
        $this->form_validation->set_message('_unique_PrinterDate', 'This %s already has been added.');

        $this->db->where([
            'date >='    => $start_of_day,
            'date <='    => $end_of_day,
            'printer_id' => $printer
        ]);

        if($target == 'EDIT') {

            $this->db->where('id !=', $id);

        } elseif($target != 'ADD') {

            return FALSE;
        }

        return (bool) (!$this->db->get('printer_has_counts', 1)->row());
    }

    function getLastCount()
    {
        if ($this->input->is_ajax_request()) {
            
            $this->load->library('form_validation');
            $this->form_validation->set_rules('date', 'Date', 'trim|required|is_date['.dateFormat().']');
            $this->form_validation->set_rules('printer', 'Printer', 'trim|required|is_natural');

            if (! $this->form_validation->run()) {
                messageAJAX('error', validation_errors());
            }

            $client_day_number = dateReFormat($this->input->post('date', TRUE), dateFormat(), 'Y-m-d');
            $start_of_day      = dateConvert2($client_day_number . ' 00:00:00', 'server');
            $end_of_day        = dateConvert2($client_day_number . ' 23:59:59', 'server');

            $this->db->select('count, date');
            $this->db->where('printer_id', $this->input->post('printer'));
            $this->db->where('date <=',  $end_of_day);
            $this->db->where('active !=', 3);
            $this->db->order_by('date', 'desc');
            if ($printercount = $this->db->get('printer_has_counts', 1)->row()) {

                $printercount->date = dateConvert($printercount->date, 'client', 'Y-m-d');
                messageAJAX('success', NULL, $printercount);
            } else{

                messageAJAX('error', 'Not Found');
            }

        }
    }
}
?>

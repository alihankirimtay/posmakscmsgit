<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Cari extends Auth_Controller {

    public function __construct()
    {
        parent::__construct();
        //Load Dependencies

        $this->check_auth([
            'allowed' => ['ajax']
        ]);

        $this->load->library('Transaction_helper', null, 'TransactionHelper');


        $this->load->model('Customers_M');
        $this->load->model('Banks_M');
        $this->load->model('Pos_M');
        $this->load->model('Transactions_M');
        $this->load->model('Sales_M');

        
    }/////////////////////////

    public function index()
    {
        $customers = $this->Customers_M->where(['active !='  => 3])->get_all();
        
        $this->theme_plugin = [
            'css' => ['panel/css/view/cari.css'],
            'js' => ['panel/js/view/cari.js'], 
            'start' => 'TablesDataTables.init(); Cari.init(); MyFunction.datetimepicker("'.dateFormat(TRUE).'"); '
        ];
        $this->load->template('Cari/index', compact('customers'));

    }

    public function customerDetail ($id = null) {

        $customer = $this->Customers_M->findOrFail($id);
        $all_transactions = [];        

        $dFrom = date('Y-m-d H:i:00', strtotime('-24 hours'));
        $dFrom = $this->input->get('d');

        $dTo = date('Y-m-d H:i:59');
        $dTo = $this->input->get('dTo');

        $date['date_from'] = isValidDate($dFrom, dateFormat()) ? dateConvert($dFrom, 'server', 'Y-m-d H:i:00') : date('Y-m-1 H:i:00');
        $date['date_to'] = isValidDate($dTo, dateFormat()) ? dateConvert($dTo, 'server', 'Y-m-d H:i:00') : date('Y-m-t H:i:00');

        
        $sales_cari = $this->Sales_M->getTransactionsWithSales($id, $date);
        $transactions = $this->Transactions_M->fields('date as payment_date,type_id as type, amount, desc, id, increment, pos_amount, customer_amount, bank_amount')
        ->get_all([
            'customer_id' => $id,
            'date >=' => $date[ 'date_from' ],
            'date <=' => $date[ 'date_to' ]
        ]);

        $types = $this->Transactions_M->getTypes();

        foreach ($transactions as $transaction) {
            $all_transactions[] = $transaction;
        }

        foreach ($sales_cari as $sale) {
            $all_transactions[] = $sale;
        }

        $this->theme_plugin = [
            'css' => ['panel/css/view/cari.css'],
            'js' => ['panel/js/view/cari.js'], 
            'start' => 'TablesDataTables.init(); Cari.init(); MyFunction.datetimepicker("'.dateFormat(TRUE).'"); '
        ];

        $this->load->template('Cari/customer_detail', compact('customer', 'all_transactions', 'date', 'types'));
    }

    public function ajax($event = null)
    {
        if(method_exists(__CLASS__, (String) $event) && $this->input->is_ajax_request()) {
            $this->{$event}();
        }

        messageAJAX('error', __('Hatalı istek [404]'));
    }

    public function getCustomers() {
        $customers = $this->Customers_M->get_all([
            'active' => 1
        ]);

        messageAJAX('success', 'Success', compact('customers'));

    }

    public function getPosAndBanks()
    {

        $location_id = $this->input->get('location_id');

        $pos = $this->Pos_M->where('location_id', $location_id)->get_all([
            'active' => 1
        ]);
        $banks = $this->Banks_M->where('location_id', $location_id)->get_all([
            'active' => 1
        ]);

        messageAJAX('success', 'Success', compact('pos', 'banks'));

    }

    public function getBanks()
    {
        $location_id = $this->input->get('location_id');

        $banks = $this->Banks_M->where('location_id', $location_id)->get_all([
            'active' => 1
        ]);

        messageAJAX('success', 'Success', compact('banks'));

    }

    public function getPos()
    {
        $location_id = $this->input->get('location_id');

        $pos = $this->Pos_M->where('location_id', $location_id)->get_all([
            'active' => 1
        ]);

        messageAJAX('success', 'Success', compact('pos'));

    }

    public function complete() {

        $type= $this->input->post('account-type');
        $bank_id= $this->input->post('bank[id]');
        $pos_id = $this->input->post('pos[id]');
        $amount = $this->input->post('amount');
        $increment = $this->input->post('increment');
        $customer_id = $this->input->post('customers');
        $payment_date = $this->input->post('date');
        $desc = $this->input->post('desc');
        $type_id = $this->input->post('type_id');

        try {

            $this->TransactionHelper
            ->setType($type)
            ->setBankId($bank_id)
            ->setPosId($pos_id)
            ->setAmount($amount)
            ->setIncrement($increment)
            ->setCustomerId($customer_id)
            ->setPaymentDate($payment_date)
            ->setDesc($desc)
            ->setTypeId($type_id)
            ->processTransactionCustomer();

            messageAJAX('success', __('İşlem başarıyla oluşturuldu.'));

        } catch (Exception $error) {
            messageAJAX('error', $error->getMessage());
        }

    }

    public function transfer()
    {   
        $sender_type = $this->input->post('sender-account-type');
        $recipient_type = $this->input->post('recipient-account-type');
        $amount= $this->input->post('amount');
        $desc = $this->input->post('desc');
        $sender_bank_id = $this->input->post('bank_sender_id');
        $sender_pos_id = $this->input->post('pos_sender_id');
        $recipient_bank_id = $this->input->post('bank_recipient_id');
        $recipient_pos_id = $this->input->post('pos_recipient_id');
        $increment = 0;
        $customer_id = null;

        $datetime = date("Y-m-d H:i:s", strtotime("+3 hours"));
        $todatetime = dateReFormat($datetime, 'Y-m-d H:i:s', 'd-m-Y H:i');

        $_POST['date'] = $todatetime;
        $payment_date =  $todatetime;
       
        try {

            $this->TransactionHelper
            ->setSenderAccountType($sender_type)
            ->setRecipientAccountType($recipient_type)
            ->setSenderBankId($sender_bank_id)
            ->setSenderPosId($sender_pos_id)
            ->setRecipientBankId($recipient_bank_id)
            ->setRecipientPosId($recipient_pos_id)
            ->setAmount($amount)
            ->setIncrement($increment)
            ->setCustomerId($customer_id)
            ->setPaymentDate($payment_date)
            ->setDesc($desc)
            ->processTransactionAccounts();

            messageAJAX('success', __('İşlem başarıyla oluşturuldu.'));

        } catch (Exception $error) {
            messageAJAX('error', $error->getMessage());
        }


    }

}

<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Locations extends API_Controller {

    function __construct()
    {
        parent::__construct();

        $this->load->model('Api/Locations_model');
    }

    function index()
    {
        $this->Locations_model->index();
    }
}
?>
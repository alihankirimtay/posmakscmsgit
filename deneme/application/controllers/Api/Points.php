	<?php
	if (!defined('BASEPATH')) exit('No direct script access allowed');

	class Points extends API_Controller {

		function __construct()
		{
			parent::__construct();

			$this->load->model('Api/Points_model');
		}

		function index()
		{
			$location = (int) $this->getPOST('location', 'trim|integer');

			if(!$this->exists('locations', "id = $location AND active = 1", 'id')) api_messageError( logs(NULL, 'Location does not exist.', $location) );

			$this->Points_model->index($location);
		}
	}
	?>
<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class App extends API_Controller {

  function __construct()
    {
        parent::__construct();

        //$this->load->model('Api/App_model');
        
    }

  function index()
    {
        
    }
	
	function version()
    {
		$json['count'] = 1;
		$json['items'][] = array(
						'version' => 1,
						'title'   => base_url('assets/uploads/versions/APP_v_1.exe')
					);

		api_messageOk($json);
    }

    function trigger()
    {
     
      ob_end_clean(); //if our framework have turn on ob_start() we don't need bufering respond up to this script will be finish 
      header("Succes"); //send information to curl is close
      ignore_user_abort(true); //script will be exisits up to finish below query even web browser will be close before sender get respond
      
      try {

        $archives = postRequest(posmaksCmsUrl("api/?updatesGetArchives&api_token={$this->website->api_token}"), [
          'version' => $this->website->version,
          'branch' => $this->website->branch,
        ]);

        if (!$archives) {
          return;
        }

        foreach ($archives as $archive) {

          $archive_name = basename($archive);
          $target = FCAPATH.'assets/updates/'.$archive_name;

          $archive_data = file_get_contents($archive);
          if (!$archive_data) {
            throw new Exception("Arşiv indirilemedi");  
          }

          if (!file_put_contents($target, $archive_data)) {
            throw new Exception("Arşiv kaydedilemedi");
          }

          mkdir(FCAPATH . "assets/updates/temp", 0777);

          $zip = new ZipArchive();

          if ($zip->open($target) === false) {
            throw new Exception("Arşiv açılamadı");
          }

          $processes = json_decode($zip->getFromName('info.json'));

          if ($processes->deleted) {

            foreach ($processes->deleted as $deleted) {
              if ($deleted && file_exists(FCAPATH . $deleted)) {
               
                $temp = copy(FCAPATH . $deleted, FCAPATH . 'assets/updates/temp');
                if (!$temp){
                  throw new Exception("Silinen dosyalar geçici klasöre kopyalanamadı.");
                }
                  
                unlink(FCAPATH . $deleted);
              }
            }

          }

          if ($processes->modified) {

            foreach ($processes->modified as $modified) {

              $file_content = $zip->getFromName($modified);
              if ($file_content === false) {
                throw new Exception("Dosya okunamadı veya bir hata oluştu: " . $zip->getStatusString());
              }

              if ($modified && file_exists(FCAPATH . $modified)) {

                $temp = copy(FCAPATH . $modified, FCAPATH . 'assets/updates/temp');
                if(!$temp){
                  throw new Exception("Eklenilen/Değiştirilen dosyalar geçici klasöre kopyalanamadı.");
                }

                file_put_contents(FCAPATH . $modified, $file_content);
              }

            }

          }  
        
          //Migration
          /*  exec('php ' . base_url('index.php'). ' migrate latest 2>&1', $exec_output);
          $exec_output = implode(' ', (Array) $exec_output);
          if (!preg_match('/\[Success\]/', $exec_output)) {
          $this->jsonResult('error', 'Migrate Error: ' . $exec_output);
          }*/

          $zip->close();
          deleteDir(FCAPATH . 'assets/updates/temp');

        }


      } catch (Exception $e) {
        trigger_error($e);

        if (!$processes) {
          exit('Güncelleme gerçekleştirilemedi lütfen sistem yöneticinize başvurunuz. Hata: ' . $e);
        }
        //RESTORE 
        $tempOpen = opendir(FCAPATH . "assets/updates/temp");
        if (!$tempOpen){
          exit("Güncellemede bir hata oluştu ve dosyalar geri yüklenemedi sistem yöneticinize başvurunuz.");
        }

        $rdtemps = readdir($tempOpen);
        if (!$rdtemps){
          exit("Güncellemede bir hata oluştu ve dosyalar geri yüklenemedi sistem yöneticinize başvurunuz.(rd)"); 
        }

        $mergeprocceses = array_merge($processes['deleted'],$processes['added']);
        foreach ($mergeprocceses as $rProcces) {
          foreach ($rdtemps as $sendfile) {

            $baseprocces = basename($rProcces);
            $basesenfile = basename($sendfile);

            if ($baseprocces == $basesenfile) {
               $putfileback = file_put_contents(FCAPATH . $rProcces, file_get_contents($sendfile));

              if (!$putfileback) {
                exit("Güncellemede bir hata oluştu ve dosyalar geri yüklenemedi sistem yöneticinize başvurunuz.(fp)"); 
              }

            }
          }   
        }

        deleteDir(FCAPATH . 'assets/updates/temp');

      }
    
    }

  function deleteDir($dirPath) 
  {
      if (! is_dir($dirPath)) {
        throw new InvalidArgumentException($dirPath . " must be a directory");
      }
      if (substr($dirPath, strlen($dirPath) - 1, 1) != '/') {
        $dirPath .= '/';
      }
      $files = glob($dirPath . '*', GLOB_MARK);
      foreach ($files as $file) {
        if (is_dir($file)) {
          self::deleteDir($file);
        } else {
          unlink($file);
        }
      }
      rmdir($dirPath);
    }
}
<?php if (!defined('BASEPATH')) exit('No direct script access allowed');


class Sales extends Auth_Controller {

    public function __construct()
    {
        parent::__construct();

        $this->check_auth([
            'allowed' => ['ajax']
        ]);

        $this->load->model('Sales_M');
        $this->load->model('Products_M');
        $this->load->model('Invoices_M');
        $this->load->model('Users_M');
        $this->load->model('Customers_M');
        $this->load->model('Paymenttypes_M', 'PaymentTypes_M');
        $this->load->model('Salescari_M');

        $this->load->model('Sales_model');
        $this->load->model('Reports_model');
        $this->load->model('Customers_model', 'Customers');
    }

    public function ajax($event = null)
    {
        if (!method_exists(__CLASS__, (String) $event) || !$this->input->is_ajax_request()) {
            messageAJAX('error', __('Hatalı istek [404]'));
        }
        $this->{$event}();
    }

    public function add()
    {
        if ($this->input->is_ajax_request()) {

            $this->load->library('Possale', null, 'PosSale');

            $payment_date = $this->input->post('payment_date');
            if (!isValidDate($payment_date, dateFormat()))
                messageAJAX('error', __('Geçersiz işlem tarihi.'));
            $payment_date = dateConvert2($payment_date, 'server', dateFormat());

            $sale = $this->PosSale
            ->setAction('checkout')
            ->setPaymentDate($payment_date)
            ->setProducts($this->input->post('products[]'))
            ->setPayments($this->input->post('payments[]'))
            ->setLocationId($this->input->post('location_id'))
            ->setPosId($this->input->post('pos_id'))
            ->setPointId($this->input->post('point_id'))
            ->setCustomerId($this->input->post('customer_id'))
            ->setUserId($this->user->id)
            ->setCompleterUserId($this->input->post('completer_user_id'))
            ->setDiscountAmount($this->input->post('discount_amount'))
            ->setDiscountType($this->input->post('discount_type'))
            ->processSale();

            if (!$sale) {
                messageAJAX('error', $this->PosSale->errorMessage());
            }

            messageAJAX('success', __('Satış başarıyla oluşturuldu.'), compact('sale'), 'json');
        }

        $payment_types = $this->PaymentTypes_M->get_all();
        $customers = $this->Customers_M->get_all();

        $this->theme_plugin = [
            'css' => ['panel/css/view/sales.css'],
            'js' => ['panel/js/view/sales.js'],
            'start' => '
                MyFunction.datetimepicker("'.dateFormat(TRUE).'");
                Sales.setPaymentTypes('.json_encode($payment_types).');
                Sales.setCustomers('.json_encode($customers).');
                Sales.init();'
        ];

        $this->load->template('Sales/add', compact('payment_types'));
    }

    public function edit($id = null)
    {
        $sale = $this->Sales_M->findOrFail($id);

        $findSales = $this->Sales_M
        ->fields('id')
        ->with_related_sales([
            'fields' => [
                'customer_id',
                 'sale_id'
            ]
        ])
        ->get([
            'id' => $sale->id
        ]);

        $existCariPaymentCustomers = [];

        if ($findSales) {
            foreach ($findSales->related_sales as $value) {
                $existCariPaymentCustomers[] = $value->customer_id;
            }
        }

        if ($this->input->is_ajax_request()) {

            $this->load->library('Possale', null, 'PosSale');

            $payment_date = $this->input->post('payment_date');
            if (!isValidDate($payment_date, dateFormat()))
                messageAJAX('error', __('Geçersiz işlem tarihi.'));
            $payment_date = dateConvert2($payment_date, 'server', dateFormat());

            $discount_remove = false;
            $discount_amount = $this->input->post('discount_amount');
            $discount_remove = ((int) $discount_amount === 0);

            $sale = $this->PosSale
            ->setAction('checkout')
            ->setSaleId($sale->id)
            ->setPaymentDate($payment_date)
            ->setProducts($this->input->post('products[]'))
            ->setPayments($this->input->post('payments[]'))
            ->setLocationId($this->input->post('location_id'))
            ->setPosId($this->input->post('pos_id'))
            ->setPointId($this->input->post('point_id'))
            ->setCustomerId($this->input->post('customer_id'))
            ->setUserId($this->user->id)
            ->setCompleterUserId($this->input->post('completer_user_id'))
            ->setDiscountAmount($discount_amount)
            ->setDiscountType($this->input->post('discount_type'))
            ->setDiscountRemove($discount_remove)
            ->setIsPanel(true)
            ->setExistCariPaymentCustomers($existCariPaymentCustomers)
            ->processSale();

            if (!$sale) {
                messageAJAX('error', $this->PosSale->errorMessage());
            }

            messageAJAX('success', __('Satış başarıyla güncellendi.'), compact('sale'), 'json');
        }

        $sale = $this->Sales_M->findSaleWithProducts([
            'id' => $sale->id
        ]);
        $sale->payment_date_client = dateConvert2($sale->payment_date, 'client', 'Y-m-d H:i:s', dateFormat());
        $payment_types = $this->PaymentTypes_M->get_all();
        $customers = $this->Customers_M->get_all();
        
        if ($sale->cari) {
            $sale->sale_cari = $this->db
            ->where('sale_id', $sale->id)
            ->where('cancelled', 0)
            ->get('sales_cari')
            ->result();
        }

        $this->theme_plugin = [
            'css' => ['panel/css/view/sales.css'],
            'js' => ['panel/js/view/sales.js'],
            'start' => '
                MyFunction.datetimepicker("'.dateFormat(TRUE).'");
                Sales.setPaymentTypes('.json_encode($payment_types).');
                Sales.setCustomers('.json_encode($customers).');
                Sales.init('.json_encode($sale).');'
        ];

        $this->load->template('Sales/edit', compact('sale', 'payment_types'));
    }

    public function index($location = null)
    {
        $filter['date_filter'] = $this->input->get('date_filter');
        $filter['date_start'] = $this->input->get('date_start');
        $filter['date_end'] = $this->input->get('date_end');
        $filter['location_id'] = $this->input->get('location_id');
        $filter['pos_id'] = $this->input->get('pos_id');
        $filter['user_id'] = $this->input->get('user_id');
        $filter['completer_user_id'] = $this->input->get('completer_user_id');
        $filter['payment_type_id'] = $this->input->get('payment_type_id');
        $filter = $this->handleFilter($filter);

        $this->queryFilter($filter);
        $sales = $this->Sales_M
        ->fields('*')
        ->select('(SELECT title FROM floors WHERE id = floor_id LIMIT 1) AS floor_title')
        ->select('(SELECT title FROM zones WHERE id = zone_id LIMIT 1) AS zone_title')
        ->select('(SELECT title FROM points WHERE id = point_id LIMIT 1) AS point_title')
        ->select('(SELECT username FROM users WHERE id = user_id LIMIT 1) AS user_name')
        ->select('(SELECT username FROM users WHERE id = completer_user_id LIMIT 1) AS completer_user_name')
        ->select('(SELECT title FROM payment_types WHERE id = payment_type_id LIMIT 1) AS payment_type_title')
        ->where('location_id', $this->user->locations)
        ->get_all([
            'status' => 'completed',
            'active' => 1,
        ]);

        $users = $this->Users_M->get_all();
        $customers = $this->Customers_M->get_all();
        $payment_types = $this->PaymentTypes_M->get_all();
        $currency = $this->user->locations_array[$filter['location_id']]['currency'];

        $this->theme_plugin = [
            'js' => ['panel/js/view/sales.js','globals/plugins/jquery.print/print.js'],
            'start' => '
                TablesDataTables.init();
                MyFunction.datetimepicker("'.dateFormat(TRUE).'");
                Sales.index();
                Sales.Invoice.init();'
        ];

        $this->load->template('Sales/index', 
            compact(
                'sales', 'users', 'customers', 'payment_types', 'filter', 'currency'
            )
        );
    }

    private function handleFilter($filter)
    {
        if (!in_array($filter['location_id'], $this->user->locations)) {
            $filter['location_id'] = $this->user->locations[0];
        }

        switch ($filter['date_filter']) {
            case 'last365days':
                $filter['date_start'] = date('Y-m-d 00:00:00', strtotime('-365 DAY'));
                $filter['date_end'] = date('Y-m-d 23:59:59');
                break;
            case 'yesterday':
                $filter['date_start'] = date('Y-m-d 00:00:00', strtotime('-1 DAY'));
                $filter['date_end'] = date('Y-m-d 23:59:59', strtotime('-1 DAY'));
                break;
            case 'thisweek':
                $filter['date_start'] = date('Y-m-d 00:00:00', strtotime('this week monday'));
                $filter['date_end'] = date('Y-m-d 23:59:59', strtotime('this week sunday'));
                break;
            case 'thismonth':
                $filter['date_start'] = date('Y-m-01 00:00:00');
                $filter['date_end'] = date('Y-m-d 23:59:59', strtotime('last day of this month'));
                break;
            case 'thisyear':
                $filter['date_start'] = date('Y-01-01 00:00:00');
                $filter['date_end'] = date('Y-12-31 23:59:59');
                break;
            case 'lastweek':
                $filter['date_start'] = date('Y-m-d 00:00:00', strtotime('last week monday'));
                $filter['date_end'] = date('Y-m-d 23:59:59', strtotime('last week sunday'));
                break;
            case 'lastmonth':
                $filter['date_start'] = date('Y-m-d 00:00:00', strtotime('first day of last month'));
                $filter['date_end'] = date('Y-m-d 23:59:59', strtotime('last day of last month'));
                break;
            case 'lastyear':
                $filter['date_start'] = date('Y-01-01 00:00:00', strtotime('-1 YEAR'));
                $filter['date_end'] = date('Y-12-31 23:59:59', strtotime('-1 YEAR'));
                break;
            case 'custome':
                if (isValidDate($filter['date_start'], dateFormat())) {
                    $filter['date_start'] = dateReFormat($filter['date_start'], dateFormat(), 'Y-m-d H:i:s');
                } else {
                    $filter['date_start'] = date('Y-m-d 00:00:00');
                }
                if (isValidDate($filter['date_end'], dateFormat())) {
                    $filter['date_end'] = dateReFormat($filter['date_end'], dateFormat(), 'Y-m-d H:i:s');
                } else {
                    $filter['date_end'] = date('Y-m-d 23:59:59');
                }
                break;
            case 'today':
            default:
                $filter['date_start'] = date('Y-m-d 00:00:00');
                $filter['date_end'] = date('Y-m-d 23:59:59');
                break;
        }

        $filter['date_start'] = dateConvert2($filter['date_start'], 'server');
        $filter['date_end'] = dateConvert2($filter['date_end'], 'server');

        $filter['date_start_client'] = dateConvert2($filter['date_start'], 'client', 'Y-m-d H:i:s', dateFormat());
        $filter['date_end_client'] = dateConvert2($filter['date_end'], 'client', 'Y-m-d H:i:s', dateFormat());

        return $filter;
    }

    private function queryFilter($filter)
    {
        if ($filter['location_id'])
            $this->db->where('location_id', $filter['location_id']);
        if ($filter['pos_id'])
            $this->db->where('pos_id', $filter['pos_id']);
        if ($filter['user_id'])
            $this->db->where('user_id', $filter['user_id']);
        if ($filter['completer_user_id'])
            $this->db->where('completer_user_id', $filter['completer_user_id']);
        if ($filter['payment_type_id'])
            $this->db->where('payment_type_id', $filter['payment_type_id']);

        $this->db->where('payment_date >=', $filter['date_start']);
        $this->db->where('payment_date <=', $filter['date_end']);
    }

    public function view($id = null)
    {
        $sale = $this->Sales_M->findSaleWithProducts(["id" => $id]);

        if (!$sale) {
            redirect(base_url('sales'));
        }

        $Costumer = [];

        if($sale->customer_id != NULL){
            $Costumer = $this->Customers->findOrFail($sale->customer_id); 
        }

        $invoice = $this->Invoices_M->get([
            'location_id' => $sale->location_id,
            'default' => 1,
        ]);
        $invoice->json = json_decode($invoice->json);


        $invContent = [
            'pdfName' => "Sales_" . $sale->id,
            'width' => $invoice->width."cm",
            'height' => $invoice->height."cm",
            'usbs' => [],
            'contents' => []
        ];

        foreach($invoice->json as $t):

            // Customers

            if($t->id == "comp-customerName"):                
                $values = [
                    'type' => "div",
                    'x'  => $t->x."px",
                    'y'  => $t->y."px",
                    'w'  => $t->w,
                    'h'  => $t->h,
                    'content' => ($sale->customer_id != NULL ? $Costumer->name : "")
                ];
                array_push($invContent['contents'], $values);
            

            elseif($t->id == "comp-customerAddress"):

                $values = [
                    'type' => "div",
                    'x'  => $t->x."px",
                    'y'  => $t->y."px",
                    'w'  => $t->w,
                    'h'  => $t->h,
                    'content' => ($sale->customer_id != NULL ? $Costumer->address : "")
                ];
                array_push($invContent['contents'], $values);

            elseif($t->id == "comp-customerPhone"):

                $values = [
                    'type' => "div",
                    'x'  => $t->x."px",
                    'y'  => $t->y."px",
                    'w'  => $t->w,
                    'h'  => $t->h,
                    'content' => ($sale->customer_id != NULL ? $Costumer->phone : "")
                ];
                array_push($invContent['contents'], $values);

            elseif($t->id == "comp-customerTaxOfficeNo"):

                $values = [
                    'type' => "div",
                    'x'  => $t->x."px",
                    'y'  => $t->y."px",
                    'w'  => $t->w,
                    'h'  => $t->h,
                    'content' => $Costumer->tax_office . ' ' . $Costumer->tax_no
                ];
                array_push($invContent['contents'], $values);

            elseif($t->id == "comp-customerTaxOfficeName"):

                $values = [
                    'type' => "div",
                    'x'  => $t->x."px",
                    'y'  => $t->y."px",
                    'w'  => $t->w,
                    'h'  => $t->h,
                    'content' => $Costumer->tax_office
                ];
                array_push($invContent['contents'], $values);

            elseif($t->id == "comp-customerTaxNo"):

                $values = [
                    'type' => "div",
                    'x'  => $t->x."px",
                    'y'  => $t->y."px",
                    'w'  => $t->w,
                    'h'  => $t->h,
                    'content' => $Costumer->tax_no
                ];
                array_push($invContent['contents'], $values);
            // Invoice
            elseif($t->id == "comp-invoiceDateOfIssue"):

                $invoice_date = date('Y-m-d H:i:s');
                if ($sale->invoice_date) {
                    $invoice_date = $sale->invoice_date;
                }

                $values = [
                    'type' => "div",
                    'x'  => $t->x."px",
                    'y'  => $t->y."px",
                    'w'  => $t->w,
                    'h'  => $t->h,
                    'content' => dateConvert2($invoice_date, 'client', 'Y-m-d H:i:s', 'd-m-Y H:i')
                ];
                array_push($invContent['contents'], $values);

            elseif($t->id == "comp-invoiceId"):

                $values = [
                    'type' => "div",
                    'x'  => $t->x."px",
                    'y'  => $t->y."px",
                    'w'  => $t->w,
                    'h'  => $t->h,
                    'content' => $sale->id
                ];
                array_push($invContent['contents'], $values);

            elseif($t->id == "comp-invoicePaymentDate"):

                $values = [
                    'type' => "div",
                    'x'  => $t->x."px",
                    'y'  => $t->y."px",
                    'w'  => $t->w,
                    'h'  => $t->h,
                    'content' => date("d-m-Y" , strtotime($sale->payment_date))
                ];
                array_push($invContent['contents'], $values);

            elseif($t->id == "comp-amountTotal"):

                $values = [
                    'type' => "div",
                    'x'  => $t->x."px",
                    'y'  => $t->y."px",
                    'w'  => $t->w,
                    'h'  => $t->h,
                    'content' => $sale->amount
                ];
                array_push($invContent['contents'], $values);

            elseif($t->id == "comp-amountSubTotal"):

                $values = [
                    'type' => "div",
                    'x'  => $t->x."px",
                    'y'  => $t->y."px",
                    'w'  => $t->w,
                    'h'  => $t->h,
                    'content' => $sale->subtotal
                ];
                array_push($invContent['contents'], $values);

            elseif($t->id == "comp-amountDiscount"):

                $values = [
                    'type' => "div",
                    'x'  => $t->x."px",
                    'y'  => $t->y."px",
                    'w'  => $t->w,
                    'h'  => $t->h,
                    'content' => ($sale->discount_amount == NULL ? "0.00" : $sale->discount_amount)
                ];
                array_push($invContent['contents'], $values);

            elseif($t->id == "comp-amountTotalTax"):

                $values = [
                    'type' => "div",
                    'x'  => $t->x."px",
                    'y'  => $t->y."px",
                    'w'  => $t->w,
                    'h'  => $t->h,
                    'content' => $this->totalTax($sale->products)
                ];
                array_push($invContent['contents'], $values);

            // Products
            elseif($t->id == "comp-productList"):
                
                $values = [
                    'type' => "table",
                    'x'  => $t->x."px",
                    'y'  => $t->y."px",
                    'w'  => $t->w,
                    'h'  => $t->h,
                    'showFieldTitles' => $t->configs->showFieldTitles,
                    'fields' => $t->configs->fields,
                    'products' => $sale->products
                ];

                array_push($invContent['contents'], $values);

            // Other
            elseif($t->id == "comp-noteText"):
                
                $values = [
                    'type' => "div",
                    'x'  => $t->x."px",
                    'y'  => $t->y."px",
                    'w'  => $t->w,
                    'h'  => $t->h,
                    'content' => $t->configs->text
                ];
                array_push($invContent['contents'], $values);

            elseif($t->id == "comp-noteTextArea"):
                
                $values = [
                    'type' => "div",
                    'x'  => $t->x."px",
                    'y'  => $t->y."px",
                    'w'  => $t->w,
                    'h'  => $t->h,
                    'content' => $t->configs->text
                ];
                array_push($invContent['contents'], $values);

            endif;

        endforeach;


        $this->theme_plugin = array(
            'js'    => array( 'panel/js/view/sales.js', 'globals/plugins/jquery.print/print.js'),
            'start' => 'Sales.invCall();'
        );

        $this->load->template('Sales/view', $invContent);
        
    }


    private function totalTax($products = [])
    {
        if($products == NULL) return;

        $total = 0;

        foreach($products as $p):

            $total += ($p->price * $p->tax) / 100;

            foreach($p->additional_products as $a):
                $total += ($a->price * $a->tax) /100;
            endforeach;
        endforeach;
        return $total;
    }

    public function scoreBoard($location_id = null)
    {
        if (!$location_id) {
            $location_id = $this->user->locations[0];
        }

        $this->theme_plugin['js']    = array('panel/js/view/sales.js');
        $this->theme_plugin['start'] = 'TablesDataTables.init(); Sales.ScoreBoard('.$location_id.');';
        $this->load->template('Sales/score_board', compact('location_id'));
    }

    private function getScores()
    {
        $location_id = $this->input->post('location_id');
        $date_start = date('Y-m-d 00:00:00');
        $date_start = dateConvert2($date_start, 'server');
        $currency = null;

        if (isset($this->user->locations_array[$location_id])) {
            $currency = $this->user->locations_array[$location_id]['currency'];
        }

        $sales = $this->Sales_M->findSalesWithProducts([
            'payment_date >=' => $date_start,
            'location_id' => $location_id,
            'status' => 'completed',
            'active' => 1
        ]);


        $users = $this->Users_M->select('users.id, users.name, 0 As total')->findByLocation($location_id);
        $admins = $this->Users_M->fields(['id', 'name', '0 As total'])->get_all([
            'role_id' => 1,
            'active' => 1,
        ]);

        $users = arrayMerge($admins, $users);

        foreach ($sales as $sale) {
            foreach ($sale->products as $product) {
                foreach ($users as &$user) {
                    if ($user->id == $product->user_id) {

                        $total = addTax($product->price * $product->quantity, $product->tax);

                        if ($product->additional_products) {
                            foreach ($product->additional_products as $add_product) {
                                $total += addTax($add_product->price * $add_product->quantity, $add_product->tax);
                            }
                        }

                        if ($sale->return_id) {
                            $user->total -= $total;
                        } else {
                            $user->total += $total;
                        }
                    }
                }
                unset($user);
            }
        }

        messageAJAX('success', null, compact('users', 'currency'));
    }

    private function getSaleInvoice()
    {
        $id = (int) $this->input->post('id');
        $datetime = date('Y-m-d H:i:s');

        $sale = $this->Sales_M->findOrFail(['id' => $id]);

        if ($sale->invoice_date) {
            $datetime = $sale->invoice_date;
        }

        $date = dateConvert2($datetime, 'client', 'Y-m-d H:i:s', 'd-m-Y');
        $time = dateConvert2($datetime, 'client', 'Y-m-d H:i:s', 'H:i');

        $sale->invoice_date_client = [
            'date' => $date,
            'time' => $time,
        ];

        messageAJAX('success', 'success', compact('sale'));
    }

    private function getSaleAdisyonWithProducts()
    {
        $id = (int) $this->input->post('id');
        $datetime = date('Y-m-d H:i:s');

        $sale = $this->Sales_M->fields(['*', '(select name from users where id = user_id) As user_name '])->findSaleWithProducts(['id' => $id]);

        if ($sale->invoice_date) {
            $datetime = $sale->invoice_date;
        }

        $date = dateConvert2($datetime, 'client', 'Y-m-d H:i:s', 'd-m-Y');
        $time = dateConvert2($datetime, 'client', 'Y-m-d H:i:s', 'H:i');

        $sale->invoice_date_client = [
            'date' => $date,
            'time' => $time,
        ];

        if($sale->payment_date) {
            $sale->payment_date_client = dateConvert2($sale->payment_date, 'client', 'Y-m-d H:i:s', 'd-m-Y');
        } else {
            $sale->payment_date_client = dateConvert2($datetime, 'client', 'Y-m-d H:i:s', 'd-m-Y');
        }


        messageAJAX('success', 'success', compact('sale'));
    }

    private function processSaleInvoice()
    {
        $sale_id = $this->input->post('sale_id');
        $customer_id = $this->input->post('customer_id');
        $date = $this->input->post('date');
        $time = $this->input->post('time');
        $datetime = "{$date} {$time}";

        if (!isValidDate($datetime, 'd-m-Y H:i')) {
            messageAJAX('error', __('Geçersiz Tarih/Saat'));
        }
        $datetime = dateConvert2($datetime, 'server', 'd-m-Y H:i');

        $sale = $this->Sales_M->findOrFail(['id' => $sale_id], __('Geçersiz satış.'));
        $customer = $this->Customers_M->findOrFail(['id' => $customer_id], __('Müşte seçiniz.'));
        $invoice = $this->Invoices_M->fields('id')->findOrFail([
            'location_id' => $sale->location_id,
            'default' => 1,
        ], __('Fatura şablonu bulunamadı.'));

        $this->Sales_M->update([
            'customer_id' => $customer->id,
            'invoice_id' => $invoice->id,
            'invoice_date' => $datetime,
        ], ['id' => $sale->id]);

        messageAJAX('success', 'success', compact('sale_id'));
    }

    public function salesCariDelete($id = null)
    {
        $id = $this->input->get('id');
        
        $sales_cari = $this->db->where('id', $id)->get('sales_cari')->row();

        if(!$sales_cari) {
            messageAJAX('error', __('Cari bulunamadı.'));
        }

        $this->db
        ->where('id', $id)
        ->set('cancelled', 1)
        ->update('sales_cari');


        $this->db
        ->where('id', $sales_cari->customer_id)
        ->set('amount', "amount - {$sales_cari->amount}", false)
        ->update('customers', [
            'modified' => date("Y-m-d H:i:s"),
        ]);
        

        $sales_cari_insert = [
            'sale_id' => $sales_cari->sale_id,
            'customer_id' => $sales_cari->customer_id,
            'amount' => $sales_cari->amount * -1,
            'customer_amount' => $sales_cari->customer_amount - $sales_cari->amount,
            'cancelled' => 1
        ];

        $this->db->insert('sales_cari', $sales_cari_insert);

        messageAJAX('success', __('İşlem başarıyla gerçekleşti.'));
        
       
}
}
<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Stockcontents extends Auth_Controller {

    function __construct()
    {
        parent::__construct();
        $this->check_auth();
        $this->load->model('Stockcontents_model');
        $this->load->model('Stocks_model');
    }

    function index($stock = NULL)
    {
        $this->theme_plugin = ['start' => 'TablesDataTables.init();'];

        $data['current_stock'] = (int) $stock;
        if(!$data['current_stock'] = $this->exists('stocks', "id = {$data['current_stock']} AND location_id IN (".implode(', ', $this->user->locations).")", '*')) {
            show_404();
        }

        $data['stock_contents'] = $this->Stockcontents_model->index($data['current_stock']->id);

    	$this->load->template('Stockcontents/index', $data);
    }

    function add($stock = null)
    {
        exit();
        $data['current_stock'] = (int) $stock;
        if(!$data['current_stock'] = $this->exists('stocks', "id = {$data['current_stock']} AND location_id IN (".implode(', ', $this->user->locations).")", '*')) {
            show_404();
        }

        if ($this->input->is_ajax_request()) {
            
            $this->load->library('form_validation');
            $this->form_validation->set_rules('price', 'Fiyat', 'required|trim|is_decimal|greater_than_equal_to[0]');
            $this->form_validation->set_rules('quantity', 'Miktar', 'required|trim|is_decimal|greater_than_equal_to[0]');
            $this->form_validation->set_rules('desc', 'Açıklama', 'trim');

            if($this->form_validation->run()) {

                $this->Stockcontents_model->add($data['current_stock']);
            }
            
            messageAJAX('error', validation_errors());

        } else {

            $this->load->template('Stockcontents/add', $data);
        }       
    }

    function edit($stock = null, $id = NULL)
    {
        exit();

        $data['current_stock'] = (int) $stock;
        $id                    = (int) $id;

        if(!$data['current_stock'] = $this->exists('stocks', "id = {$data['current_stock']} AND location_id IN (".implode(', ', $this->user->locations).")", '*')) {
            show_404();
        }

        if(!$data['stock_content'] = $this->exists('stock_contents', "id = $id AND stock_id = {$data['current_stock']->id}", '*')) {
            show_404();
        }

        if ($this->input->is_ajax_request()) {
            
            $this->load->library('form_validation');
            $this->form_validation->set_rules('price', 'Fiyat', 'required|trim|is_decimal|greater_than_equal_to[0]');
            $this->form_validation->set_rules('quantity', 'Miktar', 'required|trim|is_decimal|greater_than_equal_to[0]');
            $this->form_validation->set_rules('desc', 'Açıklama', 'trim');

            if($this->form_validation->run()) {

                $this->Stockcontents_model->edit($id, $data['current_stock'], $data['stock_content']);
            }
            
            messageAJAX('error', validation_errors());

        } else {

            $this->load->template('Stockcontents/edit', $data);
        }      
    }

    function delete($stock = null, $id = NULL)
    {
        exit();
        $data['current_stock'] = (int) $stock;
        $id                    = (int) $id;

        if(!$data['current_stock'] = $this->exists('stocks', "id = {$data['current_stock']} AND location_id IN (".implode(', ', $this->user->locations).")", '*')) {
            show_404();
        }

        if(!$data['stock_content'] = $this->exists('stock_contents', "id = $id AND stock_id = {$data['current_stock']->id}", '*')) {
            show_404();
        }

        $this->Stockcontents_model->delete(['id' => $id]);

        messageAJAX('success', __('Stok işlemi silindi.'));
    }
}
?>
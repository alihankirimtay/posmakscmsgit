var Employes = {

	form : "#employesForm",

	configs: {
		users_zones: {}
	},

	init: function(configs) {

		if (typeof configs !== "undefined") {
            $.extend(Employes.configs, configs);
        }
		
		Employes.admin_controller();
		Employes.get_zones();
	},

	get_zones: function() {

		$('.location-zone').on('change', function() {

			$.post(base_url+'employes/ajax/getZonesByLocationId', $(Employes.form).serializeArray(), 

			function(json) {

					console.log(json);


				$('.zones-list').html("");

				$.each(json.data.zones, function(index, zones) {


					let group = $('<optgroup label="">');

					$.each(zones, function(index, zone) {
						let selected = "";

						$.each(Employes.configs.users_zones, function(index, user_zone) {
							if (user_zone.id === zone.id) {
								selected = "selected";
								return;
							}
						});

						group.attr('label',zone.location_title);
					 	group.append($(`<option ${selected} ></option>`).val(zone.id).html(zone.title));
					 	 
					});
					 
					 $('.zones-list').append(group);

				});

                $('.zones-list').trigger("chosen:updated");

			},"json");
		
		});
        
        $('.location-zone').trigger("change");

	},

	admin_controller: function () {
		$("select[name='role_id']").on('change', function () {
			let role_id=$(this).val();
			if(role_id == 1){
				$('#zones').addClass('hidden');
			}
			else $('#zones').removeClass('hidden');

		});
		$("select[name='role_id']").trigger("change");

	  }
		
}
var Stockcounts = {

	configs: {
		units: {},
	},

	init: function (configs) {

		if (typeof configs !== "undefined") {
			$.extend(Stockcounts.configs, configs);
		}

		$("body").on("change", "select[name=location_id]", Stockcounts.on_change_location);
		$('select[name=location_id]').trigger('change');
		$("body").on("click", '#stockSync' ,Stockcounts.on_click_stock_sync);

	},

	on_change_location: function() {

		$('#stocksInputs').html("");

		$.post(base_url+'Stockcounts/ajax/getStockCounts', $("form").serializeArray(), function(json) {
			
			$.each(json.data.stocks, function(index, val) {

				if(typeof val.hand_amount == "undefined") {
					val.hand_amount = "";
				}
				
				$('#stocksInputs').append(`<div class="form-group>
								<label class="control-label col-md-3"></label>
                                <div class="col-md-3">${val.title} <b>( ${Stockcounts.configs.units[val.unit]} )</b>
                                    <div class="inputer">
                                        <div class="input-wrapper">
                                            <input type="text" name="stocks[${val.id}]" class="form-control" value="${val.hand_amount}">
                                        </div>
                                    </div>
                                </div>
                            </div>`);
			});

			if(typeof json.data.date_start !== "undefined") {
				$('input[name=start_date]').val(json.data.date_start);
			}

      	}, "json");

	},

	on_click_stock_sync: function() {
		var id = $('#stockSync').attr('data-id');
		$.post(base_url+'Stockcounts/ajax/systemSync', {id: id}, function(json) {

			if(json.result) {
				Pleasure.handleToastrSettings('Başarılı', json.message, 'success');
			} else {	
          		Pleasure.handleToastrSettings('Hata!', json.message, 'error');
			}
			

      	}, "json");

	}
};
;(function($){

	setTimeout(showTutorial, 1000);
})(jQuery);

function showTutorial() {
	if ($('#wizardModal').hasClass('in')) {
		return;
	}

    if (localStorage.getItem("dontShowAgain") == "dontShowAgain") {
    	return;
    }
    else if (sessionStorage.getItem("no") == "hayir") {
    	return;	
    }
    else if(sessionStorage.getItem("yes") != "evet"){


    			    var modal = $(' \
		    <div class="modal" id="modal-membership-notify" tabindex="-1" role="dialog" aria-hidden="true"> \
		      <div class="modal-dialog"> \
		        <div class="modal-content"> \
		          <div class="modal-header"> \
		            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button> \
		            <h4 class="modal-title">POSMAKS</h4> \
		          </div> \
		          <div class="modal-body"> \
		            Posmaks Kolay İşletme Otomasyon Sistemini kullanmanıza yardımcı olmamızı ister misiniz? \
		          </div> \
		          <div class="modal-footer"> \
		            <a href="#" class="btn btn-green btn-block showTutorial">Evet</a> \
		            <a href="#" class="btn btn-danger btn-block dontShow">Hayır</a> \
		            <a href="#" class="btn btn-danger btn-block dontShowAgain">Bunu bir daha gösterme !</a> \
		          </div> \
		        </div> \
		      </div> \
		    </div>');



	    modal.appendTo("body").modal("toggle");
	    modal.on("click", ".showTutorial", function(){
	      	sessionStorage.setItem("yes", "evet");
	      	modal.modal("toggle");
	      	tutorial();


	    });

	    modal.on("click", ".dontShow", function(){
	      	sessionStorage.setItem("no", "hayir");
	     	 modal.modal("toggle");
	    });

	    modal.on("click", ".dontShowAgain", function(){
	      	localStorage.setItem("dontShowAgain", "dontShowAgain");
	      	modal.modal("toggle");
	    });	
	}
	else{
		tutorial();
	}


  

}

function tutorial(){


								var onDefaultNext = function () {
									$(".hamburger").click();
									$.tutorialize.next();
								};

								var _slides = [{
									content: 'Şu anda anasayfada bulunmaktasınız. Sol taraftan menüye ulaşabilirsiniz ya da ekranda bulunan kısa yolları kullanabilirsiniz.',
									selector: 'html',
									title: 'Hoşgeldiniz !'
								},
								
								{
									content: 'Öncelikle ilk işletnizi eklemelisiniz. Menüden "İşletme Yönetimi" sayfasına ulaşabilirsiniz.',
									position: 'right-top',
									selector: '.nav-menu',
									title: 'İşletme Ekleme',
									onNext: onDefaultNext
								},
								{
									 
										
									content: 'İşletme Yönetimine ulaşmak için menüdeki "İşletme Yönetimi"ne tıklayabilirsiniz. \n "İşletmeler" seçeneğini seçerek işletmelerinizi görebileceğiniz ve yönetebileceğiniz sayfaya ulaşırsınız.',
									position: 'right-center',
									selector: '#isletmeYonetimi',
									title: 'İşletme Ekleme',
									onNext:function(){
										window.location = base_url + 'locations';
										$.tutorialize.next();
										
									}
								 
								},
								{
									content: 'Şu anda işletme yönetimi sayfasındasınız. Kaydedeceğiniz işletmeleri burada görebilir ya da yeni işletme ekleyebilirsiniz.',
									position: 'center-center',
									selector: 'html',
									title: 'İşletme Ekleme'
								},
								{
									content: 'İlk işletmenizi kaydetmek için "Restoran Oluştur" butonuna basınız.',
									position: 'left-center',
									selector: '#restoranolustur',
									title: 'İşletme Ekleme',
									onNext:function(){
										window.location = base_url + 'locations/add';
										$.tutorialize.next();
									}
								},
								{
									content: 'İşletme bilgilerini doldurunuz.',
									position: 'center-top',
									selector: '.panel-title',
									title: 'İşletme Ekleme'
								},
								{
									
									content: 'İşletme bilgilerini girdikten sonra "Kaydet & Çık" butonuna basınız.',
									position: 'top-right',
									selector: '#kaydetButonu',
									title: 'İşletme Ekleme',
									onNext:function(){
										window.location = base_url + '?tutorialize=true';
										$.tutorialize.next();
									}
								},
								{
									content: 'İşletmenizi ekledikten sonra restoranınızda bulunan katları eklemelisiniz.',
									position: 'center-center',
									selector: 'html',
									title: 'Kat Ekleme'
								},
								{
									content: 'Kat eklemek için menüden "Katlar" sayfasına ulaşabilirsiniz.',
									position: 'right-top',
									selector: '.nav-menu',
									title: 'Kat Ekleme',
									onNext: onDefaultNext
								},
								{
									content: '"Katlar" sayfasına ulaşmak için menüdeki "İşletme Yönetimi"ne tıklayabilirsiniz. \n "Katlar" seçeneğini seçerek işletmelere ait katları görebileceğiniz ve yönetebileceğiniz sayfaya ulaşırsnız.',
									position: 'right-center',
									selector: '#isletmeYonetimi',
									title: 'Kat Ekleme',
									onNext:function(){
										window.location = base_url + 'floors';
										$.tutorialize.next();
									}
								},
								{
									content: 'Şu anda katlar sayfasında bulunmaktasınız. Bu sayfada işletmelere ait katları görebilirsiniz.',
									position: 'right-center',
									selector: 'html',
									title: 'Kat Ekleme'
								},
								{
									content: 'Bir işletmeye kat eklemek için "Kat Oluştur" butonuna basınız.',
									position: 'left-center',
									selector: '#katOlustur',
									title: 'Kat Ekleme',
									onNext:function(){
										window.location = base_url + 'floors/add';
										$.tutorialize.next();
									}
								},
								{
									content: 'İşletmeye ait kat için bilgileri doldurunuz.',
									position: 'center-center',
									selector: '.panel-title',
									title: 'Kat Ekleme'
								},
								{
									content: '"Kaydet & Çık" butonuna basarak işletmeye kat ekleyebilirsiniz.',
									position: 'top-right',
									selector: '#kaydetVeCık',
									title: 'Kat Ekleme',
									onNext:function(){
										window.location = base_url + '?tutorialize=true';
										$.tutorialize.next();
									}
								},
								{
									content: 'Kat ekledikten sonra yapmamız gereken şey ise o katta bulunan bölgeleri eklemektir.',
									position: 'center-center',
									selector: 'html',
									title: 'Bölge Ekleme'
								},
								{
									content: 'Bölge eklemek için menüden "Bölgeler" sayfasına ulaşabilirsiniz.',
									position: 'right-top',
									selector: '.nav-menu',
									title: 'Bölge Ekleme',
									onNext: onDefaultNext
								},
								{
									content: '"Bölgeler" sayfasına ulaşmak için menüdeki "İşletme Yönetimi"ne tıklayabilirsiniz. \n "Bölgeler" seçeneğini seçerek işletmelere ait bölgeleri görebileceğiniz ve yönetebileceğiniz sayfaya ulaşırsnız.',
									position: 'right-center',
									selector: '#isletmeYonetimi',
									title: 'Bölge Ekleme',
									onNext:function(){
										window.location = base_url + 'zones';
										$.tutorialize.next();
									}
								},
								{
									content: 'Şu anda "Bölgeler" sayfasında bulunmaktasınız. Bu sayfada işletmelere ait bölgeleri görebilirsiniz.',
									position: 'center-center',
									selector: 'html',
									title: 'Bölge Ekleme'
								},
								{
									content: 'Bir işletmedeki kata bölge eklemek için "Bölge Oluştur" butonuna basınız.',
									position: 'left-center',
									selector: '#bolgeOlustur',
									title: 'Bölge Ekleme',
									onNext:function(){
										window.location = base_url + 'zones/add';
										$.tutorialize.next();
									}
								},
								{
									content: 'Bölge detaylarını doldurunuz.',
									position: 'center-top',
									selector: '.panel-title',
									title: 'Bölge Ekleme'
								},
								{
									
									content: 'Bölge detaylarını girdikten sonra "Kaydet & Çık" butonuna basınız.',
									position: 'top-right',
									selector: '#kaydetVeCik',
									title: 'Bölge Ekleme',
									onNext:function(){
										window.location = base_url + '?tutorialize=true';
										$.tutorialize.next();
									}
								},
								{
									content: 'Bölge ekleme işleminden sonra yapılması gereken şey bölgelere masa eklemektir.',
									position: 'center-center',
									selector: 'html',
									title: 'Masa Ekleme'
								},
								{
									content: 'Masa eklemek için menüden "Masalar" sayfasına ulaşabilirsiniz.',
									position: 'right-top',
									selector: '.nav-menu',
									title: 'Masa Ekleme',
									onNext: onDefaultNext
								},
								{
									content: '"Masalar" sayfasına ulaşmak için menüdeki "İşletme Yönetimi"ne tıklayabilirsiniz. \n "Masalar" seçeneğini seçerek işletmelere ait bölgelere eklenen masaları görebileceğiniz ve yönetebileceğiniz sayfaya ulaşırsnız.',
									position: 'right-center',
									selector: '#isletmeYonetimi',
									title: 'Masa Ekleme',
									onNext:function(){
										window.location = base_url + 'points';
										$.tutorialize.next();
									}
								},
								{
									content: 'Şu anda "Masalar" sayfasında bulunmaktayız. Bu sayfada işletmelere ait masaları görebilirsiniz.',
									position: 'center-center',
									selector: 'html',
									title: 'Masa Ekleme'
								},
								{
									content: 'Bir işletmedeki bölgeye masa eklemek için ilk önce "Masa Oluştur" butonuna basınız.',
									position: 'left-center',
									selector: '#masaOlustur',
									title: 'Masa Ekleme',
									onNext:function(){
										window.location = base_url + 'points/add';
										$.tutorialize.next();
									}
								},
								{
									content: 'Masa detaylarını doldurunuz.',
									position: 'center-top',
									selector: '.panel-title',
									title: 'Masa Ekleme'
								},
								{
									
									content: 'Masa detaylarını girdikten sonra "Kaydet & Çık" butonuna basınız.',
									position: 'top-right',
									selector: '#kaydetVeCik',
									title: 'Masa Ekleme',
									onNext:function(){
										window.location = base_url + 'points';
										$.tutorialize.next();
									}
								},
								{
									content: 'Masa ekleme işleminden sonra eklediğiniz masaları bulundukları bölgedeki konumlarına göre yerleştirmelisiniz.',
									position: 'center-center',
									selector: 'html',
									title: 'Masa Ekleme'
								},
								{
									content: 'Masa düzenini oluşturmak için "Masa Düzeni" butonuna basarak ilgili sayfaya ulaşırsınız.',
									position: 'left-center',
									selector: '#masaDuzeni',
									title: 'Masa Ekleme',
									onNext:function(){
										window.location = base_url + 'points/planner';
										$.tutorialize.next();
									}
								},
								{
									content: 'Düzenlemek istediğimiz restoran / kat / bölge seçtikten sonra o bölgeye eklenen masalar listelenir.',
									position: 'right-center',
									selector: '.panel-body',
									title: 'Masa Düzeni'
								},
								{
									content: 'Masaları yeşil bölgeye sürükle bırak yaparak işletmenizdeki bölgeye uygun bir şekilde yerleştirebilirsiniz. </br> Masaları yerleştirdikten sonra kaydet butonuna basarak masa düzenini kaydedebilirsiniz. </br> Kaydet butonu herhangi bir değişiklik yaptıktan sonra aktif olur.',
									position: 'center-center',
									selector: 'body',
									title: 'Masa Düzeni',
									onNext:function(){
										window.location = base_url + '?tutorialize=true';
										$.tutorialize.next();
									}
								},
								{
									content: 'İşletmenizin bölgelerini ve masalarını ayarladıktan sonra şimdi de işletmenize kasa eklemelisiniz.',
									position: 'center-center',
									selector: 'html',
									title: 'Kasa Ekleme'
								},
								{
									content: 'Kasa eklemek için menüden "Kasalar" sayfasına ulaşabilirsiniz.',
									position: 'right-top',
									selector: '.nav-menu',
									title: 'Kasa Ekleme',
									onNext: onDefaultNext
								},
								{
									content: '"Kasalar" sayfasına ulaşmak için menüdeki "İşletme Yönetimi"ne tıklayabilirsiniz. \n "Kasalar" seçeneğini seçerek işletmelere ait kasaları görebileceğiniz ve yönetebileceğiniz sayfaya ulaşırsınız.',
									position: 'right-center',
									selector: '#isletmeYonetimi',
									title: 'Kasa Ekleme',
									onNext:function(){
										window.location = base_url + 'pos';
										$.tutorialize.next();
									}
								},
								{
									content: 'Şu anda Kasalar sayfasında bulunmaktasınız. Bu sayfada işletmelere ait kasaları görebilirsiniz.',
									position: 'center-center',
									selector: 'html',
									title: 'Kasa Ekleme'
								},
								{
									content: 'Bir işletmeye kasa eklemek için "Kasa Oluştur" butonuna basınız.',
									position: 'left-center',
									selector: '#kasaOlustur',
									title: 'Kasa Ekleme',
									onNext:function(){
										window.location = base_url + 'pos/add';
										$.tutorialize.next();
									}
								},
								{
									content: 'Kasanızı ekleyeceğiniz sayfada bulunmaktasınız. ',
									position: 'center-center',
									selector: '.panel-heading',
									title: 'Kasa Ekleme'
								}, 
								{
									content: 'Kasa detaylarını doldurduktan sonra "Kaydet & Çık" butonuna basarak kaydetme işlemini gerçekleştirebilirsiniz.',
									position: 'top-right',
									selector: '#kaydetVeCik',
									title: 'Kasa Düzeni',
									onNext:function(){
										window.location = base_url + '?tutorialize=true';
										$.tutorialize.next();
									}
								},
								{
									content: 'Ürünlerimizi eklemeye başlamadan önce "Ürün Kategorileri" eklemelisiniz. Bu bir ürünün takibini daha kolay kılacaktır. ',
									position: 'center-center',
									selector: 'html',
									title: 'Ürün Kategorileri'
								},
								{
									content: 'Ürünlerimiz ile ilgili işlem gerçekleştirmek için menüden ürünler ile ilgili bölüme ulaşmalısınız.',
									position: 'right-top',
									selector: '.nav-menu',
									title: 'Ürün Kategorileri',
									onNext: onDefaultNext
								},
								{
									content: 'Ürünler kısmı için menüdeki "Ürünler"e tıklayabilirsiniz. </br> Ürünler altında bulunan "Ürün Kategorileri" seçeneğini seçerek ürün kategorilerini görebileceğiniz ve yönetebileceğiniz sayfaya ulaşırsınız.',
									position: 'right-center',
									selector: '#urunler',
									title: 'Ürün Kategorileri',
									onNext:function(){
										window.location = base_url + 'producttypes';
										$.tutorialize.next();
										
									}
								},
								{
									content: 'Şu anda ürün kategorileri sayfasındasınız. Kaydedeceğiniz ürün kategorilerini burada görebilir ya da yeni kategori ekleyebilirsiniz.',
									position: 'center-center',
									selector: 'html',
									title: 'Ürün Kategorileri'
								},
								{
									content: 'İlk ürün kategorinizi kaydetmek için "Yeni Ürün Kategorisi" butonuna basınız.',
									position: 'left-center',
									selector: '#yeniUrunKategorisi',
									title: 'Ürün Kategorileri',
									onNext:function(){
										window.location = base_url + 'producttypes/add';
										$.tutorialize.next();
									}
								},
								{
									content: 'Ürün kategorisi bilgilerini doldurunuz.',
									position: 'center-top',
									selector: '.panel-title',
									title: 'Ürün Kategorileri'
								},
								{
									
									content: 'Bilgileri girdikten sonra "Kaydet & Çık" butonuna basınız.',
									position: 'top-right',
									selector: '#kaydetVeCik',
									title: 'Ürün Kategorileri',
									onNext:function(){
										window.location = base_url + '?tutorialize=true';
										$.tutorialize.next();
									}
								},
								{
									content: 'Ürün kategorilerinden sonra "Üretim Yerleri"ni ayarlamalısınız. Üretim yerleri, daha sonra ürün eklerken hangi noktada üretim yapılacağını belirlemede rol oynayacak. ',
									position: 'center-center',
									selector: 'html',
									title: 'Üretim Yeri'
								},
								{
									content: 'Üretim yerleri ile ilgili işlem gerçekleştirmek için menüden ürünler ile ilgili bölüme ulaşmalıyız.',
									position: 'right-top',
									selector: '.nav-menu',
									title: 'Üretim Yeri',
									onNext: onDefaultNext
								},
								{
									content: 'Ürünler kısmı için menüdeki "Ürünler"e tıklayabilirsiniz. </br> Ürünler altında bulunan "Üretim Yerleri" seçeneğini seçerek üretim yerlerini görebileceğiniz ve yönetebileceğiniz sayfaya ulaşırsınız.',
									position: 'right-center',
									selector: '#urunler',
									title: 'Üretim Yeri',
									onNext:function(){
										window.location = base_url + 'productions';
										$.tutorialize.next();
										
									}
								},
								{
									content: 'Üretim Yerleri sayfasında bulunmaktasınız. Oluşturacağımız üretim yerleri, daha sonra bir ürünü eklediğimiz zaman hangi üretim noktasında oluşturulacağını seçmek için kullanılır.',
									position: 'center-center',
									selector: 'html',
									title: 'Üretim Yeri'
								},
								{
									content: 'Ürünlerinizin üretim veya teslim noktalarını belirleyerek, sipariş geldiğinde bu noktalara bildirim gönderilmesini sağlayabilirsiniz.',
									position: 'center-center',
									selector: 'html',
									title: 'Üretim Yeri'
								},
								{
									content: 'Örneğin bir sipariş aldınız. Siparişin içeriği </br> -Yemek (Üretim yeri: Mutfak) </br> -İçecek (Üretim Yeri: Bar) </br>Bu sipariş için mutfak ve bara ayrı bildirimler gidecek. Mutfak ve bar, sipariş hazır olduktan sonra onaylayarak siparişi ileten garsona hazır olduğunu bildirecek.',
									position: 'center-center',
									selector: 'html',
									title: 'Üretim Yeri'
								},
								{
									content: 'Garson ise siparişi teslim ettikten sonra onaylayacak ve siz de böylelikle tüm sipariş sürecini kontrol edilebilir bir yapıya dönüştürmüş olacaksınız.',
									position: 'center-center',
									selector: 'html',
									title: 'Üretim Yeri'
								},
								{
									content: 'Üretim yeri eklemek için "Üretim Yeri Oluştur" butonuna basınız.',
									position: 'left-center',
									selector: '#uretimYeriOlustur',
									title: 'Üretim Yeri',
									onNext:function(){
										window.location = base_url + 'productions/add';
										$.tutorialize.next();
									}
								},
								{
									content: 'Üretim yeri bilgilerini doldurunuz.',
									position: 'center-top',
									selector: '.panel-title',
									title: 'Üretim Yeri'
								},
								{
									
									content: 'Bilgileri girdikten sonra "Kaydet & Çık" butonuna basınız.',
									position: 'top-right',
									selector: '#kaydetVeCik',
									title: 'Üretim Yeri',
									onNext:function(){
										window.location = base_url + '?tutorialize=true';
										$.tutorialize.next();
									}
								},
								{
									content: 'Ürün kategorilerini ve üretim yerlerini ekledikten sonra şimdi de "Ürünler"i eklemelisiniz.',
									position: 'center-center',
									selector: 'html',
									title: 'Ürünler'
								},
								{
									content: 'Ürünler ile ilgili işlem gerçekleştirmek için menüden ürünler ile ilgili bölüme ulaşmalısınız.',
									position: 'right-top',
									selector: '.nav-menu',
									title: 'Ürünler',
									onNext: onDefaultNext
								},
								{
									content: 'Ürünler kısmı için menüdeki "Tüm Ürünler"e tıklayabilirsiniz. \n Ürünler altında bulunan "Tüm Ürünler" seçeneğini seçerek ürünlerinizi görebileceğiniz ve yönetebileceğiniz sayfaya ulaşırsınız.',
									position: 'right-center',
									selector: '#urunler',
									title: 'Ürünler',
									onNext:function(){
										window.location = base_url + 'products';
										$.tutorialize.next();
										
									}
								},
								{
									content: 'Ürünler sayfasında bulunmaktayız. Bu sayfada işletmelerimizin ürünlerini görebilir, ürün ekleyebilri ya da daha önce eklenen bir ürün üzerinde değişiklik yapabilirsiniz.',
									position: 'center-center',
									selector: 'html',
									title: 'Ürünler'
								},
								{
									content: 'Ürün eklemek için "Ürün Oluştur" butonuna basınız.',
									position: 'left-center',
									selector: '#urunOlustur',
									title: 'Ürünler',
									onNext:function(){
										window.location = base_url + 'products/add';
										$.tutorialize.next();
									}
								},
								{
									content: 'Ürün bilgilerini doldurunuz.',
									position: 'center-top',
									selector: '.panel-title',
									title: 'Ürünler'
								},
								{
									
									content: 'Bilgileri girdikten sonra "Kaydet & Çık" butonuna basınız.',
									position: 'top-right',
									selector: '#kaydetVeCik',
									title: 'Ürünler',
									onNext:function(){
										window.location = base_url + '?tutorialize=true';
										$.tutorialize.next();
									}
								},
								{
									content: 'Ürünlerimizden sonra işletmeniz için önem teşkil eden stok takibi için stoklarınızı ayarlamalısınız.',
									position: 'center-center',
									selector: 'html',
									title: 'Stoklar'
								},
								{
									content: 'Stoklar ile ilgili işlem gerçekleştirmek için menüden stoklar ile ilgili bölüme ulaşmalısınız.',
									position: 'right-top',
									selector: '.nav-menu',
									title: 'Stoklar',
									onNext: onDefaultNext
								},
								{
									content: 'Stoklar kısmı için menüdeki Ürünler kategorisinin altında yer alan "Stoklar"a tıklayabilirsiniz.',
									position: 'right-center',
									selector: '#urunler',
									title: 'Stoklar',
									onNext:function(){
										window.location = base_url + 'stocks';
										$.tutorialize.next();
										
									}
								},
								{
									content: 'Stoklar sayfasında bulunmaktasınız. Bu sayfada işletmelerinizin stoklarını görebilir, stok ekleyebilir ya da daha önce eklenen bir stok üzerinde değişiklik yapabilirsiniz.',
									position: 'center-center',
									selector: 'html',
									title: 'Stoklar'
								},
								{
									content: 'Stok eklemek için "Stok Oluştur" butonuna basınız.',
									position: 'left-center',
									selector: '#stokOlustur',
									title: 'Stoklar',
									onNext:function(){
										window.location = base_url + 'stocks/add';
										$.tutorialize.next();
									}
								},
								{
									content: 'Stok bilgilerini doldurunuz.',
									position: 'center-top',
									selector: '.panel-title',
									title: 'Stoklar'
								},
								{
									
									content: 'Bilgileri girdikten sonra "Kaydet & Çık" butonuna basınız.',
									position: 'top-right',
									selector: '#kaydetVeCik',
									title: 'Stoklar',
									onNext:function(){
										window.location = base_url + '?tutorialize=true';
										$.tutorialize.next();
									}
								},
								{
									content: 'Ürünlerimiz ile alakalı işlemleri tamamladıktan sonra personel yönetimine geçebiliriz.',
									position: 'center-center',
									selector: 'html',
									title: 'Personel Yönetimi'
								},
								{
									content: 'Personellerimiz ile alakalı bölüme ulaşmak için menüden "Personel Yönetimi"ne ulaşmalısınız.',
									position: 'right-top',
									selector: '.nav-menu',
									title: 'Personel Yönetimi',
									onNext: onDefaultNext
								},
								{
									content: 'Personel kısmı için menüdeki Personel Yönetimi kategorisinin altında yer alan "Personeller"e tıklayabilirsiniz.',
									position: 'right-center',
									selector: '#personel',
									title: 'Personel Yönetimi',
									onNext:function(){
										window.location = base_url + 'employes';
										$.tutorialize.next();
										
									}
								},
								{
									content: 'Personeller sayfasında bulunmaktasınız. Bu sayfada işletmelerinizde çalışan personelleri görebilir, yeni personel ekleyebilir ya da personelleriniz üzerinde değişiklikler yapabilirsiniz.',
									position: 'center-center',
									selector: 'html',
									title: 'Personel Yönetimi'
								},
								{
									content: 'Personel eklemek için "Çalışan Oluştur" butonuna basınız.',
									position: 'left-center',
									selector: '#calisanOlustur',
									title: 'Personel Yönetimi',
									onNext:function(){
										window.location = base_url + 'employes/add';
										$.tutorialize.next();
									}
								},
								{
									content: 'Personel bilgilerini doldurunuz.',
									position: 'center-top',
									selector: '.panel-title',
									title: 'Personel Yönetimi'
								},
								{
									
									content: 'Bilgileri girdikten sonra "Kaydet & Çık" butonuna basınız.',
									position: 'top-right',
									selector: "[name='dosubmit']",
									title: 'Personel Yönetimi',
									onNext:function(){
										window.location = base_url + '?tutorialize=true';
										$.tutorialize.next();
									}
								},
								{
									content: 'Personellerimizin yönetimini daha kolay bir hale getirmek için vardiya çizelgesini ayarlamalısınız.',
									position: 'center-center',
									selector: 'html',
									title: 'Vardiya Çizelgesi'
								},
								{
									content: 'Vardiya çizelgesi ile alakalı bölüme ulaşmak için menüden "Personel Yönetimi"ne ulaşmalıyız.',
									position: 'right-top',
									selector: '.nav-menu',
									title: 'Vardiya Çizelgesi',
									onNext: onDefaultNext
								},
								{
									content: 'Vardiya Çizelgesi kısmı için menüdeki Personel Yönetimi kategorisinin altında yer alan "Vardiya Çizelgesi"ne tıklayabilirsiniz.',
									position: 'right-center',
									selector: '#personel',
									title: 'Vardiya Çizelgesi',
									onNext:function(){
										window.location = base_url + 'shiftschedule';
										$.tutorialize.next();
										
									}
								},
								{
									content: 'Vardiya çizelgesi sayfasında bulunmaktasınız. Bu sayfada işletmelerinizde çalışan personellerin hangi saatler arasında çalışacağını belirleyebilir ya da istediğimiz bir güne ait çalışma raporunu oluşturabiliriz.',
									position: 'right-center',
									selector: 'div[data-tutorialize=shiftschedule]',
									title: 'Vardiya Çizelgesi'
								},
								{
									content: 'Öncelikle bu kısımdan vardiya çizelgesini ayarlayacağınız restoranı seçmelisiniz.',
									position: 'right-center',
									selector: '.dropdown-toggle',
									title: 'Vardiya Çizelgesi'
								},
								{
									content: 'Restoranı seçtikten sonra o restorandaki personelleriniz tabloya eklenir. Tabloda personel ile çalışmasını istediğiniz saatin kesiştiği noktaya tıklamalıyız.',
									position: 'right-bottom',
									selector: 'div[data-tutorialize=shiftschedule]',
									title: 'Vardiya Çizelgesi'
								},
								{
									content: 'Ardından açılan menüde çalışmaya başlama ve bitirme saatlerini seçerek "Kaydet" butonuna basmalıyız.',
									position: 'right-bottom',
									selector: 'div[data-tutorialize=shiftschedule]',
									title: 'Vardiya Çizelgesi'
								},
								{
									content: 'Hangi tarihe ait vardiya çizelgesini düzenlemek ya da incelemek istiyorsanız buradan tarih seçebilirsiniz.',
									position: 'left-center',
									selector: '.input-group',
									title: 'Vardiya Çizelgesi'
								},
								{
									content: 'Personellerinizin ne kadar çalıştıklarına dair bir rapor oluşturmak isterseniz "Rapor" butonuna basmalısınız.',
									position: 'left-center',
									selector: '.pull-right',
									title: 'Vardiya Çizelgesi'
								},
								{
									
									content: 'Düzenleme işlemi bittikten sonra tekrardan anasayfaya dönebilirsiniz.',
									position: 'center-center',
									selector: 'html',
									title: 'Vardiya Çizelgesi',
									onNext:function(){
										window.location = base_url + '?tutorialize=true';
										$.tutorialize.next();
									}
								},
								{
									content: 'Personel grupları oluşturabilir, işletme otomasyonunda personellerin erişebileceği alanlar için ayarlamalar yapabilirsiniz.',
									position: 'center-center',
									selector: 'html',
									title: 'Yetkiler'
								},
								{
									content: 'Bunu gerçekleştirmek için menüyü açın ve "Ayarlar" sekmesine tıklayın.',
									position: 'right-top',
									selector: '.nav-menu',
									title: 'Yetkiler',
									onNext: onDefaultNext
								},
								{
									content: 'Ayarlar sekmesinde "Yetkiler"e tıklayarak bu alana gidebilir ve düzenlemeler yapabilirsiniz.',
									position: 'right-center',
									selector: '#ayarlar',
									title: 'Yetkiler',
									onNext:function(){
										window.location = base_url + 'roles';
										$.tutorialize.next();
										
									}
								},
								{
									content: 'Yetkiler sayfasında bulunmaktasınız. Burada çalışanlarınız için gruplar (rol) oluşturabilirsiniz ve oluşturduğunuz bu rollere yetki atayabilirsiniz.',
									position: 'center-center',
									selector: 'html',
									title: 'Yetkiler'
								},
								{
									content: 'Rol oluşturmak için "Rol Oluştur" butonuna basınız.',
									position: 'left-center',
									selector: '#rolOlustur',
									title: 'Yetkiler',
									onNext:function(){
										window.location = base_url + 'roles/add';
										$.tutorialize.next();
									}
								},
								{
									content: 'Rol bilgilerini doldurunuz.',
									position: 'center-top',
									selector: '.panel-title',
									title: 'Yetkiler'
								},
								{
									
									content: 'Bilgileri girdikten sonra "Kaydet" butonuna basınız.',
									position: 'top-right',
									selector: '#kaydetButonu',
									title: 'Yetkiler',
									onNext:function(){
										window.location = base_url + 'roles';
										$.tutorialize.next();
									}
								},
								{
									content: 'Oluşturulan bu rollere yetki atamak için "Yetkiler" butonuna basınız.',
									position: 'left-center',
									selector: '#yetkiler',
									title: 'Yetkiler',
									onNext:function(){
										window.location = base_url + 'roles/permission';
										$.tutorialize.next();
									}
								},
								{
									content: 'Bu sayfada otomasyon sisteminde gerçekleştirebileceğiniz işlemler ve yetki gruplarını görmektesiniz.',
									position: 'left-center',
									selector: '.panel-heading',
									title: 'Yetkiler',
								},
								{
									content: 'Yapmanız gereken, hangi yetkiyi hangi gruba vermek istiyorsanız o gruba ait alanı işaretlemektir.',
									position: 'left-center',
									selector: '.panel-heading',
									title: 'Yetkiler',
								},
								{
									content: 'İşleminiz bittikten sonra "Kaydet & Çık" butonuna basarak değişiklikleri kaydedebilirsiniz.',
									position: 'top-right',
									selector: '#kaydetVeCik',
									title: 'Yetkiler',
									onNext:function(){
										window.location = base_url + '?tutorialize=true';
										$.tutorialize.next();
									}
								},
								{
									content: 'İşletmeleriniz ile alakalı düzenlemeleri gerçekleştirdikten sonra pos ekranına geçebilirsiniz.',
									position: 'center-center',
									selector: 'html',
									title: 'Pos'
								},
								{
									content: 'Menüyü açmalısınız.',
									position: 'right-top',
									selector: '.nav-menu',
									title: 'Pos',
									onNext: onDefaultNext
								},
								{
									content: 'Pos sekmesine tıklayarak bu alana ulaşabiliriz.',
									position: 'right-center',
									selector: '#pos',
									title: 'Pos',
									onNext:function(){
										window.location = base_url + 'pointofsales';
										$.tutorialize.next();
									}
								},
								{
									content: 'Pos ekranımızda işlem gerçekleştirmek için öncelikle işlem gerçekleştirilecek masayı seçmelisiniz.',
									position: 'center-center',
									selector: 'html',
									title: 'Pos'
								},
								{
									content: '"Masalar" butonuna tıklayarak işletmenize önceden eklemiş olduğunuz kat / bölge / masa planlarına ulaşabilirsiniz.',
									position: 'bottom-center',
									selector: '.fa-square-o',
									title: 'Pos'
								},
								{
									content: 'Seçtiğiniz masaya sipariş eklemek için ürünler tablosunu kullanabilirsiniz.',
									position: 'center-center',
									selector: 'body',
									title: 'Pos',
									onNext:function(){
										$('#tab_contents_container [data-id="1"]').click();
										$.tutorialize.next();
									}
								},
								{
									content: 'Masayı başka bir konuma taşımak için "Masayı Taşı" butonunu kullanabilirsiniz.',
									position: 'right-bottom',
									selector: '#move-table',
									title: 'Pos'
								},
								/*{
									content: 'Not alma özelliğini kullanabilirsiniz.',
									position: 'bottom-center',
									selector: '.fa-pencil-square-o',
									title: 'Pos'
								},*/
								{
									content: 'Masaya ait adisyonu oluşturmak için "Adisyon" butonuna basmalısınız.',
									position: 'right-bottom',
									selector: '#handle-adisyon',
									title: 'Pos'
								},
								{
									content: 'Hesap almadan önce ödemeyi tahsil etmelisiniz. Bunun için ödeme seçeneklerini kullanabilirsiniz.',
									position: 'bottom-center',
									selector: 'html',
									title: 'Pos'
								},
								{
									content: 'Nakit işlem için bu butonu kullanmalısınız.',
									position: 'bottom-center',
									selector: '#add-cash',
									title: 'Pos'
								},
								{
									content: 'Kredi kartı için bu butonu kullanmalısınız.',
									position: 'bottom-center',
									selector: '#add-credit',
									title: 'Pos'
								},
								{
									content: 'Ticket, Sodexo Multinet ya da  Setcart seçeneği için "Ticket" butonunu kullanmalısınız.',
									position: 'bottom-center',
									selector: '.fa-credit-card-alt',
									title: 'Pos'
								},
								{
									content: 'Ödemeler tamamlandıktan sonra "Hesap Al" butonuna tıklayarak ödeme işlemini tamamlayabilirsiniz.',
									position: 'right-center',
									selector: '#pos-complete',
									title: 'Pos'
								},
								{
									content: 'Pos işlemlerinden sonra anasayfaya dönüş yapabilirsiniz.',
									position: 'center-center',
									selector: 'html',
									title: 'Pos',
									onNext:function(){
										window.location = base_url + '?tutorialize=true';
										$.tutorialize.next();
									}
								},
								{
									content: 'Siparişleri "Mutfak" sekmesi üzerinden takip edebilirsiniz.',
									position: 'center-center',
									selector: 'html',
									title: 'Mutfak'
								},
								{
									content: 'Mutfağa ulaşmak için menüyü açmalısınız.',
									position: 'right-top',
									selector: '.nav-menu',
									title: 'Mutfak',
									onNext: onDefaultNext
								},
								{
									content: '"Mutfak" sekmesine tıklayarak bu alana ulaşabilirsiniz.',
									position: 'right-center',
									selector: 'li[data-tutorialize=orders]',
									title: 'Mutfak',
									onNext:function(){
										window.location = base_url + 'pointofsales/orders';
										$.tutorialize.next();
									}
								},
								{
									content: 'Siparişlerini görüntüleyeceğiniz restoranı seçmelisiniz.',
									position: 'bottom-center',
									selector: 'select[data-tutorialize="location"]',
									title: 'Mutfak'
								},
								{
									content: 'Seçtiğiniz restoranda siparişlerini listelemek istediğiniz üretim yerini seçmelisiniz.',
									position: 'bottom-center',
									selector: 'select[data-tutorialize="production"]',
									title: 'Mutfak'
								},
								{
									content: 'Seçtiğiniz restoranda bulunan üretim yerine ait mevcut, tamamlanan veya geçmiş sipariş bilgileri tabloda listelenecektir.',
									position: 'left-center',
									selector: 'select[data-tutorialize="status"]',
									title: 'Mutfak',
									onNext:function(){
										window.location = base_url + '?tutorialize=true';
										$.tutorialize.next();
									}
								},
								/*{
									content: 'Mevcut siparişleri bu sekmeden takip edebilirsiniz.',
									position: 'top-center',
									selector: 'tbody[data-tutorialize="orders"]',
									title: 'Mutfak',
									onNext:function (){
										$('select[data-tutorialize="status"]').prop('selectedIndex', 1);
										$.tutorialize.next();
									}
								},
								{
									content: 'Gün içerisinde tamamlanan siparişleri bu sekmeden görebilirsiniz.',
									position: 'top-center',
									selector: 'tbody[data-tutorialize="orders"]',
									title: 'Mutfak',
									onNext:function (){
										$('select[data-tutorialize="status"]').prop('selectedIndex', 2);
										$.tutorialize.next();
									}
								},
								{
									content: 'Geçmişte tamamlanan siparişleri bu sekmeden görebilirsiniz.',
									position: 'top-center',
									selector: 'tbody[data-tutorialize="orders"]',
									title: 'Mutfak',
									onNext:function(){
										window.location = base_url + '';
										$.tutorialize.next();
									}
								},*/
								{
									content: 'Uygulamanıza ait temel ayarları değiştirebilirsiniz.',
									position: 'top-center',
									selector: 'html',
									title: 'Uygulama Ayarları'

								},
								{
									content: 'Menüyü açarak "Ayarlar" sekmesine ulaşmalısınız.',
									position: 'right-top',
									selector: '.nav-menu',
									title: 'Uygulama Ayarları',
									onNext: onDefaultNext
								},
								{
									content: 'Ayarlar sekmesinde "Uygulama Ayarları"na tıklayarak bu alana gidebilir ve düzenlemeler yapabilirsiniz.',
									position: 'right-center',
									selector: '#ayarlar',
									title: 'Uygulama Ayarları',
									onNext:function(){
										window.location = base_url + 'configuration';
										$.tutorialize.next();
										
									}
								},
								{
									content: 'Ayarlarda değişiklik yaptıktan sonra kaydet butonuna basarak değişikliklerin kalıcı olmasını sağlarsınız.',
									position: 'top-center',
									selector: 'button[data-tutorialize=saveConfiguration]',
									title: 'Uygulama Ayarları',
									onNext:function(){
										window.location = base_url + '?tutorialize=true';
										$.tutorialize.next();
										
									}
								},
								{
									content: 'Uygulamada kim tarafından hangi işlemlerin gerçekleştirildiğini takip etmek için log sayfasını kullanabilirsiniz.',
									position: 'center-center',
									selector: 'html',
									title: 'Log'

								},
								{
									content: 'Menüyü açarak "Ayarlar" sekmesine ulaşmalısınız.',
									position: 'right-top',
									selector: '.nav-menu',
									title: 'Log',
									onNext: onDefaultNext
								},
								{
									content: 'Ayarlar sekmesinde "Loglar"a tıklayarak bu alana gidebilir ve gerçekleştirilmiş olan işlemleri görebilirsiniz.',
									position: 'right-center',
									selector: '#ayarlar',
									title: 'Log',
									onNext:function(){
										window.location = base_url + 'syslogs';
										$.tutorialize.next();
										
									}
								},
								{
									content: 'Bu sayfada hangi işlemin kim tarafından ne zaman gerçekleştirildiğine dair bilgiler yer alır.',
									position: 'center-center',
									selector: 'body',
									title: 'Log',
									onNext:function(){
										window.location = base_url + '?tutorialize=true';
										$.tutorialize.next();
										
									}
								},
								{
									content: 'Posmaks aboneliğinize ait bilgilere yine "Ayarlar" sekmesinde yer alan "Abonelik Bilgileri" alanından ulaşabilirsiniz.',
									position: 'center-center',
									selector: 'html',
									title: 'Abonelik Bilgileri'

								},
								{
									content: 'Menüyü açarak "Ayarlar" sekmesine ulaşmalısınız.',
									position: 'right-top',
									selector: '.nav-menu',
									title: 'Abonelik Bilgileri',
									onNext: onDefaultNext
								},
								{
									content: 'Ayarlar sekmesinde "Abonelik Bilgileri"ne tıklayarak açılan sayfada aboneliğinize dair bilgilere ulaşabilirsiniz.',
									position: 'right-center',
									selector: '#ayarlar',
									title: 'Abonelik Bilgileri',
									onNext:function(){
										window.location = base_url + 'memberships';
										$.tutorialize.next();
										
									}
								},
								{
									content: 'Aboneliğinize dair bütün bilgiler bu sayfada yer almaktadır.',
									position: 'center-center',
									selector: 'body',
									title: 'Abonelik Bilgileri',
									onNext:function(){
										window.location = base_url + '?tutorialize=true';
										$.tutorialize.next();
										
									}
								},
								{
									content: 'İşletmelerinizin yönetimini daha kolay bir hale getirmek için raporları kullanabilirsiniz.',
									position: 'center-center',
									selector: 'html',
									title: 'Raporlar'

								},
								{
									content: 'Raporlara ulaşmak için menüyü açmalısınız.',
									position: 'right-top',
									selector: '.nav-menu',
									title: 'Raporlar',
									onNext: onDefaultNext
								},
								{
									content: 'Raporlar butonuna tıklayarak açılan sayfada işletmenize ait bütün raporlara ulaşabilirsiniz.',
									position: 'right-center',
									selector: 'li[data-tutorialize=reports]',
									title: 'Raporlar',
									onNext:function(){
										window.location = base_url + 'reports';
										$.tutorialize.next();
										
									}
								},
								{
									content: 'Raporlar sayfasında bulunmaktasınız. Burada işletmeleriniz için oluşturulmuş raporlar yer almaktadır.',
									position: 'center-center',
									selector: 'html',
									title: 'Raporlar'
								},
								{
									content: 'Satış raporuna bu sekmeden ulaşabilirsiniz.',
									position: 'bottom-center',
									selector: '#satis',
									title: 'Raporlar'
								},
								{
									content: 'Günlük kasa raporlarına bu sekmeden ulaşabilirsiniz.',
									position: 'bottom-center',
									selector: '#gunlukKasa',
									title: 'Raporlar'
								},
								{
									content: 'Bu sekme size güne ait özet raporu sunar.',
									position: 'bottom-center',
									selector: '#gunlukOzet',
									title: 'Raporlar'
								},
								{
									content: 'Stoklarınızın takibini bu sekmede yer alan stok raporu ile gerçekleştirebilirsiniz.',
									position: 'bottom-center',
									selector: '#stokRaporu',
									title: 'Raporlar'
								},
								{
									content: 'Gün sonu raporlarına ulaşmak ya da gün sonu oluşturmak için bu sekmeyi kullanmalısınız.',
									position: 'bottom-center',
									selector: '#gunSonu',
									title: 'Raporlar'
								},
								{
									content: 'İşletmelerinizin hangi zaman diliminde ne kadar dolu olduğunu "YOĞUNLUK" sekmesinde takip edebilirsiniz.',
									position: 'bottom-center',
									selector: '#doluluk',
									title: 'Raporlar',
									onNext:function(){
										window.location = base_url + '?tutorialize=true';
										$.tutorialize.next();
										
									}
								},

								{
									content: 'İşletmelerimize ait giderleri "İşlemler" kısmından yönetebilirsiniz. Öncelikle "Gider Kategorileri" oluşturmamız gerekmektedir.',
									position: 'center-center',
									selector: 'html',
									title: 'Gider Kategorileri'

								},
								{
									content: 'Menüyü açarak "İşlemler" sekmesine ulaşmalısınız.',
									position: 'right-top',
									selector: '.nav-menu',
									title: 'Gider Kategorileri',
									onNext: onDefaultNext
								},
								{
									content: 'İşlemler sekmesinde "Gider Kategorileri"ne tıklayarak açılan sayfada gider kategorilerine ulaşabilirsiniz veya yeni bir kategori oluşturabilirsiniz.',
									position: 'right-center',
									selector: '#islemler',
									title: 'Gider Kategorileri',
									onNext:function(){
										window.location = base_url + 'expensetypes';
										$.tutorialize.next();
										
									}
								},
								{
									content: 'Gider kategorileri sayfasında bulunmaktasınız. Gider kategorisi (türü) oluşturmak için "Gider Türü Oluştur" butonuna tıklayınız.',
									position: 'left-center',
									selector: '#giderTuru',
									title: 'Gider Kategorileri',
									onNext:function(){
										window.location = base_url + 'expensetypes/add';
										$.tutorialize.next();
										
									}
								},
								{
									content: 'Gider türü için gerekli alanları doldurunuz.',
									position: 'center-center',
									selector: 'body',
									title: 'Gider Kategorileri'

								},
								{
									content: 'Bilgileri doldurduktan sonra "Kaydet & Çık" butonuna basarak gider türünü kaydedebilirsiniz.',
									position: 'top-right',
									selector: '#kaydetVeCik',
									title: 'Giderler',
									onNext:function(){
										window.location = base_url + '?tutorialize=true';
										$.tutorialize.next();
										
									}
								},
								{
									content: 'Gider türü oluşturduktan sonra artık giderlerinizi türlerine göre ekleyebilirsiniz.',
									position: 'center-center',
									selector: 'html',
									title: 'Giderler'

								},
								{
									content: 'Menüyü açarak "İşlemler" sekmesine ulaşmalısınız.',
									position: 'right-top',
									selector: '.nav-menu',
									title: 'Giderler',
									onNext: onDefaultNext
								},
								{
									content: 'İşlemler sekmesinde "Giderler"e tıklayarak açılan sayfada yeni gider ekleyebilir ya da oluşturulan giderler üzerinde değişiklik yapabilirsiniz.',
									position: 'right-center',
									selector: '#islemler',
									title: 'Giderler',
									onNext:function(){
										window.location = base_url + 'expenses';
										$.tutorialize.next();
										
									}
								},
								{
									content: 'Giderlerimizi yöneteceğiniz sayfadasınız.',
									position: 'center-center',
									selector: 'html',
									title: 'Giderler'

								},
								{
									content: 'Faturalı gider oluşturmak için butonu tıklayarak ilgili sayfaya gidebilirsiniz.',
									position: 'left-center',
									selector: '#stokluGider',
									title: 'Giderler'

								},
								{
									content: 'Stoklu olmayan bir gider oluşturmak için butonu tıklayarak ilgili sayfaya gidebilirsiniz.',
									position: 'left-center',
									selector: '#normalGider',
									title: 'Giderler'

								},
								{
									content: 'Gidelerinizi yazdırmak isterseniz yazdır seçeneğini kullanabilirsiniz.',
									position: 'left-center',
									selector: 'a[data-tutorialize=print]',
									title: 'Giderler',
									onNext:function(){
										window.location = base_url + '?tutorialize=true';
										$.tutorialize.next();
										
									}

								},
								{
									content: 'Pos ekranında gerçekleştirdiğiniz satış işlemini aynı zamanda "İşlemler" kısmında bulunan "Satışlar" üzerinden de gerçekleştirebilirsiniz.',
									position: 'center-center',
									selector: 'html',
									title: 'Satışlar'

								},
								{
									content: 'Menüyü açarak "İşlemler" sekmesine ulaşmalısınız.',
									position: 'right-top',
									selector: '.nav-menu',
									title: 'Satışlar',
									onNext: onDefaultNext
								},
								{
									content: 'İşlemler sekmesinde "Satışlar"a tıklayarak satış işlemi gerçekleştirebileceğimiz sayfaya ulaşabilirsiniz.',
									position: 'right-center',
									selector: '#islemler',
									title: 'Satışlar',
									onNext:function(){
										window.location = base_url + 'sales';
										$.tutorialize.next();
										
									}
								},
								{
									content: 'Burada daha önce gerçekleştirdiğimiz satışları görebilirsiniz. ',
									position: 'center-center',
									selector: 'body',
									title: 'Satışlar'

								},
								{
									content: 'Pos ekranında alınan ödemeler de burada listelenir. ',
									position: 'center-center',
									selector: 'body',
									title: 'Satışlar'

								},
								{
									content: 'Eğer yeni bir satış oluşturmak isterseniz buradaki "Satış Oluştur" butonunu kullanarak ilgili sayfaya ulaşabilir ve satışı oluşturabilirsiniz. ',
									position: 'left-center',
									selector: '#satisOlustur',
									title: 'Satışlar',
									onNext:function(){
										window.location = base_url + '?tutorialize=true';
										$.tutorialize.next();
										
									}

								},
								{
									content: 'Posmaks Kolay İşletme Otomasyon Sistemi kılavuzumuzun sonuna geldimiş bulunmaktayız. ',
									position: 'center-center',
									selector: 'html',
									title: 'Satışlar'

								},
								{
									content: 'Uygulamayı kullanabilmek için temel bilgilere artık sahipsiniz. İşletme ekleyerek kullanmaya başlayabilirsiniz. ',
									position: 'center-center',
									selector: 'html',
									title: 'Satışlar',
									onNext:function(){
										window.location = base_url + '';
										$.tutorialize.next();
									}
								}];

								$.tutorialize({
									slides: _slides,
									labelEnd: 'Bitir',
									labelNext: 'İleri',
									labelPrevious: 'Geri',
									labelStart: 'Başla',
									showButtonPrevious: false,
									remember: true,
									fontSize: '20px',
									onStop: function(){
										localStorage.setItem("dontShowAgain", "dontShowAgain");
									}

								});

								$.tutorialize.start();

}
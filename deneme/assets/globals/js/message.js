var Message = {


    layout:{

        message_list: '<li class="has-action-left has-action-right">\
                                <a href="#" class="visible" data-sender="{{SENDER_ID}}">\
                                    <div class="list-action-left">\
                                        <img src="http://omnibpt.com/assets/panel/img/user-image.png" class="face-radius" alt="">\
                                    </div>\
                                    <div class="list-content">\
                                        <span class="title">{{USERNAME}}</span>\
                                        <span class="caption">{{MESSAGE}}</span>\
                                    </div>\
                                    <div class="list-action-right">\
                                        <span class="top">{{AGO}}</span>\
                                        {{ICON}}\
                                    </div>\
                                </a>\
                            </li>',


        user_list   : '<li class="has-action-left {{VIEWED}} has-action-right users">\
                                <a href="#" class="visible" data-user-id="{{USER_ID}}">\
                                    <div class="list-action-left">\
                                        <img src="{{IMAGE}}" class="face-radius" alt="">\
                                    </div>\
                                    <div class="list-content">\
                                        <span class="title">{{USERNAME}}</span>\
                                        <span class="caption">Mesaj Gönder</span>\
                                    </div>\
                                    <div class="list-action-right">\
                                    </div>\
                                </a>\
                            </li>',

        messages_me     : '<div class="message right" data-id="{{ID}}">\
                                <div class="message-text"> {{MESSAGE}} <br/><span style="font-size:10px;float-right;">{{AGO}}</span></div>\
                                <img src="http://www.stblaisetowncouncil.co.uk/images/councillors/noImage.jpg" class="user-picture" alt="">\
                            </div>',


        messages_you    : '<div class="message left" data-id="{{ID}}">\
                                    <div class="message-text"> {{MESSAGE}} <br/><span style="font-size:10px;float-right;">{{AGO}}</span></div>\
                                    <img src="http://www.stblaisetowncouncil.co.uk/images/councillors/noImage.jpg" class="user-picture" alt="">\
                                </div>'

    },

    // Kullanıcı listesi
    userList:function(){

        $.ajax({
            url: base_url+'messages',
            dataType:'json',
            success: function(json){
                
                $.each(json , function(e ,a) {
                    html = Message.layout.user_list.replace('{{USER_ID}}' , a.id);
                    html = html.replace('{{USERNAME}}' , a.username);
                    if(a.image != null){
                        html = html.replace('{{IMAGE}}' , base_url+'assets/uploads/profile/'+a.image);
                    }else{
                        html = html.replace('{{IMAGE}}' , base_url+'assets/uploads/profile/no_image');
                    }
                    if(a.viewed == 0){
                        html = html.replace('{{VIEWED}}' , 'alert-green');
                    }else{
                        html = html.replace('{{VIEWED}}' , '');
                    }

                    $('.message-list').append(html);
                });
            }
        })
    },


    // Yeni konuşma başlat
    startChat:function(){
        $('body').on('click' , '.message-list > li' , function(){
            
            userID = $(this).find('a').data('user-id');
            
            $('.message-list > li').removeClass('selected');
            $(this).addClass('selected');
            Message.getMessageById(userID);
        });
    },

    getMessageById: function (id) {
        $('.message-send-container').show();
        $('#send-message-input').attr('data-sender' , id);
        $('#messages').addClass('open');

        // Define neccessary elements
            var $messages = $('#messages').find('.messages'),
                $messageSendContainer = $('.message-send-container'),
                $mobileBack = $('.mobile-back');

        // Adding Loading Bar
        $messageSendContainer.addClass('loading').prepend('<div class="loading-bar indeterminate"></div>');
        
        $('.mobile-back-button').on('click', function () {
			$('.message-list-overlay').trigger('click');
			$(this).parent().removeClass('active');
            $('#messages').removeClass('open');
		});

        // For demo purposes, 1 second delay
        setTimeout( function () {
            var jqxhr = $.ajax({
                url: base_url+'messages/start/'+id,
                beforeSend: function() {

                    $messages.html('');
                    Layout.resetSendMessage();
                }
            }).done(function(data) {
                $messages.html('');
                data = $.parseJSON(data);


                if(data != null){
                    $.each(data , function(e,a){
                       if(a.sender == user_id){

                            html = Message.layout.messages_me.replace('{{MESSAGE}}' , a.message);
                            html = html.replace('{{ID}}' , a.id);
                            html = html.replace('{{AGO}}' , a.time);

                        }else{

                            html = Message.layout.messages_you.replace('{{MESSAGE}}' , a.message);
                            html = html.replace('{{ID}}' , a.id);
                            html = html.replace('{{AGO}}' , a.time);
                        }
                        $messages.append(html);  
                    });
                }
                
                $messages.scrollTop( $messages.prop('scrollHeight') );
                $messageSendContainer.removeClass('loading').find('.loading-bar').remove();
            }).fail(function(jqXHR, textStatus) {
                $messageSendContainer.removeClass('loading').find('.loading-bar').remove();
            });
        }, 500);
        
        
        

        // If Layout is mobile then return button should come to screen
        if( $('body').hasClass('layout-device') )
            $mobileBack.addClass('active');
    },


    // Açık konuşma penceresinde yeni ileti varmı kontrol eder
    messageControl:function()
    {
        var $messages = $('#messages').find('.messages');

        setTimeout(function(){
            if($('#messages:visible').length != 0 && $('.message-send-container:visible').length != 0){
            
            id  = $('#send-message-input').attr('data-sender');
            message_id = $('.left:last').attr('data-id');

            if(typeof message_id === 'undefined'){
                message_id = 0;
            }


            $.ajax({
                url: base_url+'messages/msgControl/'+id+'/'+message_id,
                success:function(result){
                    json = $.parseJSON(result);

                    if(json.result != 0){
                        $.each(json , function(e,a){
                            html = Message.layout.messages_you.replace('{{MESSAGE}}' , a.message);
                            html = html.replace('{{ID}}' , a.id);
                            html = html.replace('{{AGO}}' , a.time);

                        });

                        $messages.append(html);
                        $messages.animate({
                            scrollTop: $(".message:last").offset().top},
                            'slow');
                    } 
                }
            });
        }
            Message.messageControl();
        }, 3000)
    },


    // Açık kullanıcı penceresinde yeni messaj gönder
    SendMessage:function()
    {
        var button = $('#send-message-button');
        var input  = $('#send-message-input');
        

        button.on('click' , function(){
            id  = $('#send-message-input').attr('data-sender');
            val = input.val();

            $.ajax({
                url: base_url+'messages/send/'+id,
                data: {message:val},
                type: 'post',
                success: function(result){

                    json = $.parseJSON(result);

                    if(json.result === 1){
                        Message.getMessageById(id);
                    }
                }
            });
        });
    },


    // Okunmamış mesajları kontrol et

    NewControl:function(){
        setTimeout(function(){
            $.ajax({
                url: base_url+'messages/NewControl',
                dataType: 'json',
                success:function(json){
                    $.each(json , function(e , a){
                        $('.badge').html(a);
                    })
                }
            })

            Message.NewControl();
        } , 5000);
    },

    // Kullanıcı arama 

    searchUser:function(){
        $('body').on('keyup' ,'#user-search' , function(){
           var value = $(this).val();

           $('.message-list > li').each(function(){
                if($(this).find('.title').text().search(value) > -1){
                    $(this).show();
                }else{
                    $(this).hide();
                }
           });

        })

    },




    init:function()
    {   
        this.userList();
        this.startChat();
        this.NewControl();
        this.messageControl();
        this.SendMessage();
        this.searchUser();

    }
}

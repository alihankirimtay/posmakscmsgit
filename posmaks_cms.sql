-- phpMyAdmin SQL Dump
-- version 4.6.3
-- https://www.phpmyadmin.net/
--
-- Anamakine: 127.0.0.1
-- Üretim Zamanı: 02 Haz 2017, 17:49:40
-- Sunucu sürümü: 5.6.21
-- PHP Sürümü: 5.6.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Veritabanı: `posmaks_cms`
--

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `custom_fields`
--

CREATE TABLE `custom_fields` (
  `id` int(11) NOT NULL,
  `title_en` varchar(100) DEFAULT NULL,
  `title_tr` varchar(100) NOT NULL,
  `tooltip_en` varchar(100) DEFAULT NULL,
  `tooltip_tr` varchar(100) NOT NULL,
  `name` varchar(55) DEFAULT NULL,
  `type` varchar(50) DEFAULT NULL,
  `req` tinyint(1) NOT NULL DEFAULT '0',
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `sorting` int(4) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Tablo döküm verisi `custom_fields`
--

INSERT INTO `custom_fields` (`id`, `title_en`, `title_tr`, `tooltip_en`, `tooltip_tr`, `name`, `type`, `req`, `active`, `sorting`) VALUES
(1, 'Company Name', 'Company Name', 'Please enter your company name', 'Please enter your company name', 'profile00001', 'profile', 1, 1, 2),
(2, 'Country', 'Country', 'Please enter your country', 'Please enter your country', 'profile00002', 'profile', 1, 1, 1);

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `email_templates`
--

CREATE TABLE `email_templates` (
  `id` int(5) NOT NULL,
  `name_en` varchar(200) NOT NULL,
  `name_tr` varchar(200) NOT NULL,
  `subject_en` varchar(255) NOT NULL,
  `subject_tr` varchar(255) NOT NULL,
  `help_en` text,
  `help_tr` text,
  `body_en` text,
  `body_tr` text,
  `type` enum('news','mailer') DEFAULT 'mailer',
  `typeid` varchar(50) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Tablo döküm verisi `email_templates`
--

INSERT INTO `email_templates` (`id`, `name_en`, `name_tr`, `subject_en`, `subject_tr`, `help_en`, `help_tr`, `body_en`, `body_tr`, `type`, `typeid`) VALUES
(1, 'Registration Email', 'Registration Email', 'Please verify your email', 'Please verify your email', 'This template is used to send Registration Verification Email, when Configuration->Registration Verification is set to YES', 'This template is used to send Registration Verification Email, when Configuration->Registration Verification is set to YES', '&lt;div align=&quot;center&quot;&gt;\n&lt;table cellspacing=&quot;5&quot; cellpadding=&quot;5&quot; border=&quot;0&quot; width=&quot;600&quot; style=&quot;background: none repeat scroll 0% 0% rgb(244, 244, 244); border: 1px solid rgb(102, 102, 102);&quot;&gt;\n    &lt;tbody&gt;\n&lt;tr&gt;\n&lt;th style=&quot;background-color: rgb(204, 204, 204);&quot;&gt;Welcome [NAME]! Thanks for registering.&lt;/th&gt;\n&lt;/tr&gt;\n&lt;tr&gt;\n&lt;td valign=&quot;top&quot; style=&quot;text-align: left;&quot;&gt;Hello,&lt;br /&gt;\n&lt;br /&gt;\nYou&#039;re now a member of [SITE_NAME].&lt;br /&gt;\n&lt;br /&gt;\nHere are your login details. Please keep them in a safe place:&lt;br /&gt;\n&lt;br /&gt;\nUsername: &lt;strong&gt;[USERNAME]&lt;/strong&gt;&lt;br /&gt;\nPassword: &lt;strong&gt;[PASSWORD]&lt;/strong&gt; &lt;hr /&gt;\nThe administrator of this site has requested all new accounts&lt;br /&gt;\nto be activated by the users who created them thus your account&lt;br /&gt;\nis currently inactive. To activate your account,&lt;br /&gt;\nplease visit the link below and enter the following:&lt;hr /&gt;\nToken: &lt;strong&gt;[TOKEN]&lt;/strong&gt;&lt;br /&gt;\nEmail: &lt;strong&gt;[EMAIL]&lt;/strong&gt; &lt;hr /&gt;\n&lt;a href=&quot;[LINK]&quot;&gt;Click here to activate your account&lt;/a&gt;&lt;/td&gt;\n&lt;/tr&gt;\n&lt;tr&gt;\n&lt;td style=&quot;text-align: left;&quot;&gt;&lt;em&gt;Thanks,&lt;br /&gt;\n[SITE_NAME] Team&lt;br /&gt;\n&lt;a href=&quot;[URL]&quot;&gt;[URL]&lt;/a&gt;&lt;/em&gt;&lt;/td&gt;\n&lt;/tr&gt;\n    &lt;/tbody&gt;\n&lt;/table&gt;\n&lt;/div&gt;', '&lt;div align=&quot;center&quot;&gt;\n&lt;table cellspacing=&quot;5&quot; cellpadding=&quot;5&quot; border=&quot;0&quot; width=&quot;600&quot; style=&quot;background: none repeat scroll 0% 0% rgb(244, 244, 244); border: 1px solid rgb(102, 102, 102);&quot;&gt;\n    &lt;tbody&gt;\n&lt;tr&gt;\n&lt;th style=&quot;background-color: rgb(204, 204, 204);&quot;&gt;Welcome [NAME]! Thanks for registering.&lt;/th&gt;\n&lt;/tr&gt;\n&lt;tr&gt;\n&lt;td valign=&quot;top&quot; style=&quot;text-align: left;&quot;&gt;Hello,&lt;br /&gt;\n&lt;br /&gt;\nYou&#039;re now a member of [SITE_NAME].&lt;br /&gt;\n&lt;br /&gt;\nHere are your login details. Please keep them in a safe place:&lt;br /&gt;\n&lt;br /&gt;\nUsername: &lt;strong&gt;[USERNAME]&lt;/strong&gt;&lt;br /&gt;\nPassword: &lt;strong&gt;[PASSWORD]&lt;/strong&gt; &lt;hr /&gt;\nThe administrator of this site has requested all new accounts&lt;br /&gt;\nto be activated by the users who created them thus your account&lt;br /&gt;\nis currently inactive. To activate your account,&lt;br /&gt;\nplease visit the link below and enter the following:&lt;hr /&gt;\nToken: &lt;strong&gt;[TOKEN]&lt;/strong&gt;&lt;br /&gt;\nEmail: &lt;strong&gt;[EMAIL]&lt;/strong&gt; &lt;hr /&gt;\n&lt;a href=&quot;[LINK]&quot;&gt;Click here to activate your account&lt;/a&gt;&lt;/td&gt;\n&lt;/tr&gt;\n&lt;tr&gt;\n&lt;td style=&quot;text-align: left;&quot;&gt;&lt;em&gt;Thanks,&lt;br /&gt;\n[SITE_NAME] Team&lt;br /&gt;\n&lt;a href=&quot;[URL]&quot;&gt;[URL]&lt;/a&gt;&lt;/em&gt;&lt;/td&gt;\n&lt;/tr&gt;\n    &lt;/tbody&gt;\n&lt;/table&gt;\n&lt;/div&gt;', 'mailer', NULL),
(2, 'Forgot Password Email', 'Forgot Password Email', 'Password Reset', 'Password Reset', 'This template is used for retrieving lost user password', 'This template is used for retrieving lost user password', '&lt;div align=&quot;center&quot;&gt;\n&lt;table width=&quot;600&quot; cellspacing=&quot;5&quot; cellpadding=&quot;5&quot; border=&quot;0&quot; style=&quot;background: none repeat scroll 0% 0% rgb(244, 244, 244); border: 1px solid rgb(102, 102, 102);&quot;&gt;\n    &lt;tbody&gt;\n&lt;tr&gt;\n&lt;th style=&quot;background-color: rgb(204, 204, 204);&quot;&gt;New password reset from [SITE_NAME]!&lt;/th&gt;\n&lt;/tr&gt;\n&lt;tr&gt;\n&lt;td valign=&quot;top&quot; style=&quot;text-align: left;&quot;&gt;Hello, &lt;strong&gt;[USERNAME]&lt;/strong&gt;&lt;br /&gt;\n&lt;br /&gt;\nIt seems that you or someone requested a new password for you.&lt;br /&gt;\nWe have generated a new password, as requested:&lt;br /&gt;\n&lt;br /&gt;\nYour new password: &lt;strong&gt;[PASSWORD]&lt;/strong&gt;&lt;br /&gt;\n&lt;br /&gt;\nTo use the new password you need to activate it. To do this click the link provided below and login with your new password.&lt;br /&gt;\n&lt;a href=&quot;[LINK]&quot;&gt;[LINK]&lt;/a&gt;&lt;br /&gt;\n&lt;br /&gt;\nYou can change your password after you sign in.&lt;hr /&gt;\nPassword requested from IP: [IP]&lt;/td&gt;\n&lt;/tr&gt;\n&lt;tr&gt;\n&lt;td style=&quot;text-align: left;&quot;&gt;&lt;em&gt;Thanks,&lt;br /&gt;\n[SITE_NAME] Team&lt;br /&gt;\n&lt;a href=&quot;[URL]&quot;&gt;[URL]&lt;/a&gt;&lt;/em&gt;&lt;/td&gt;\n&lt;/tr&gt;\n    &lt;/tbody&gt;\n&lt;/table&gt;\n&lt;/div&gt;', '&lt;div align=&quot;center&quot;&gt;\n&lt;table width=&quot;600&quot; cellspacing=&quot;5&quot; cellpadding=&quot;5&quot; border=&quot;0&quot; style=&quot;background: none repeat scroll 0% 0% rgb(244, 244, 244); border: 1px solid rgb(102, 102, 102);&quot;&gt;\n    &lt;tbody&gt;\n&lt;tr&gt;\n&lt;th style=&quot;background-color: rgb(204, 204, 204);&quot;&gt;New password reset from [SITE_NAME]!&lt;/th&gt;\n&lt;/tr&gt;\n&lt;tr&gt;\n&lt;td valign=&quot;top&quot; style=&quot;text-align: left;&quot;&gt;Hello, &lt;strong&gt;[USERNAME]&lt;/strong&gt;&lt;br /&gt;\n&lt;br /&gt;\nIt seems that you or someone requested a new password for you.&lt;br /&gt;\nWe have generated a new password, as requested:&lt;br /&gt;\n&lt;br /&gt;\nYour new password: &lt;strong&gt;[PASSWORD]&lt;/strong&gt;&lt;br /&gt;\n&lt;br /&gt;\nTo use the new password you need to activate it. To do this click the link provided below and login with your new password.&lt;br /&gt;\n&lt;a href=&quot;[LINK]&quot;&gt;[LINK]&lt;/a&gt;&lt;br /&gt;\n&lt;br /&gt;\nYou can change your password after you sign in.&lt;hr /&gt;\nPassword requested from IP: [IP]&lt;/td&gt;\n&lt;/tr&gt;\n&lt;tr&gt;\n&lt;td style=&quot;text-align: left;&quot;&gt;&lt;em&gt;Thanks,&lt;br /&gt;\n[SITE_NAME] Team&lt;br /&gt;\n&lt;a href=&quot;[URL]&quot;&gt;[URL]&lt;/a&gt;&lt;/em&gt;&lt;/td&gt;\n&lt;/tr&gt;\n    &lt;/tbody&gt;\n&lt;/table&gt;\n&lt;/div&gt;', 'mailer', NULL),
(3, 'Welcome Mail From Admin', 'Welcome Mail From Admin', 'You have been registered', 'You have been registered', 'This template is used to send welcome email, when user is added by administrator', 'This template is used to send welcome email, when user is added by administrator', '&lt;div align=&quot;center&quot;&gt;\n&lt;table cellspacing=&quot;5&quot; cellpadding=&quot;5&quot; border=&quot;0&quot; width=&quot;600&quot; style=&quot;background: none repeat scroll 0% 0% rgb(244, 244, 244); border: 1px solid rgb(102, 102, 102);&quot;&gt;\n    &lt;tbody&gt;\n&lt;tr&gt;\n&lt;th style=&quot;background-color: rgb(204, 204, 204);&quot;&gt;Welcome [NAME]! You have been Registered.&lt;/th&gt;\n&lt;/tr&gt;\n&lt;tr&gt;\n&lt;td style=&quot;text-align: left;&quot;&gt;Hello,&lt;br /&gt;\n&lt;br /&gt;\nYou&#039;re now a member of [SITE_NAME].&lt;br /&gt;\n&lt;br /&gt;\nHere are your login details. Please keep them in a safe place:&lt;br /&gt;\n&lt;br /&gt;\nUsername: &lt;strong&gt;[USERNAME]&lt;/strong&gt;&lt;br /&gt;\nPassword: &lt;strong&gt;[PASSWORD]&lt;/strong&gt;&lt;/td&gt;\n&lt;/tr&gt;\n&lt;tr&gt;\n&lt;td style=&quot;text-align: left;&quot;&gt;&lt;em&gt;Thanks,&lt;br /&gt;\n[SITE_NAME] Team&lt;br /&gt;\n&lt;a href=&quot;[URL]&quot;&gt;[URL]&lt;/a&gt;&lt;/em&gt;&lt;/td&gt;\n&lt;/tr&gt;\n    &lt;/tbody&gt;\n&lt;/table&gt;\n&lt;/div&gt;', '&lt;div align=&quot;center&quot;&gt;\n&lt;table cellspacing=&quot;5&quot; cellpadding=&quot;5&quot; border=&quot;0&quot; width=&quot;600&quot; style=&quot;background: none repeat scroll 0% 0% rgb(244, 244, 244); border: 1px solid rgb(102, 102, 102);&quot;&gt;\n    &lt;tbody&gt;\n&lt;tr&gt;\n&lt;th style=&quot;background-color: rgb(204, 204, 204);&quot;&gt;Welcome [NAME]! You have been Registered.&lt;/th&gt;\n&lt;/tr&gt;\n&lt;tr&gt;\n&lt;td style=&quot;text-align: left;&quot;&gt;Hello,&lt;br /&gt;\n&lt;br /&gt;\nYou&#039;re now a member of [SITE_NAME].&lt;br /&gt;\n&lt;br /&gt;\nHere are your login details. Please keep them in a safe place:&lt;br /&gt;\n&lt;br /&gt;\nUsername: &lt;strong&gt;[USERNAME]&lt;/strong&gt;&lt;br /&gt;\nPassword: &lt;strong&gt;[PASSWORD]&lt;/strong&gt;&lt;/td&gt;\n&lt;/tr&gt;\n&lt;tr&gt;\n&lt;td style=&quot;text-align: left;&quot;&gt;&lt;em&gt;Thanks,&lt;br /&gt;\n[SITE_NAME] Team&lt;br /&gt;\n&lt;a href=&quot;[URL]&quot;&gt;[URL]&lt;/a&gt;&lt;/em&gt;&lt;/td&gt;\n&lt;/tr&gt;\n    &lt;/tbody&gt;\n&lt;/table&gt;\n&lt;/div&gt;', 'mailer', NULL),
(4, 'Default Newsletter', 'Default Newsletter', 'Newsletter', 'Newsletter', 'This is a default newsletter template', 'This is a default newsletter template', '&lt;div align=&quot;center&quot;&gt;\n&lt;table style=&quot;background: none repeat scroll 0% 0% rgb(244, 244, 244); border: 1px solid rgb(102, 102, 102);&quot; cellpadding=&quot;5&quot; cellspacing=&quot;5&quot; border=&quot;0&quot; width=&quot;600&quot;&gt;\n    &lt;tbody&gt;\n&lt;tr&gt;\n&lt;th style=&quot;background-color: rgb(204, 204, 204);&quot;&gt;Hello [NAME]!&lt;/th&gt;\n&lt;/tr&gt;\n&lt;tr&gt;\n&lt;td style=&quot;text-align: left;&quot; valign=&quot;top&quot;&gt;You are receiving this email as a part of your newsletter subscription. &lt;hr&gt;\nHere goes your newsletter content &lt;hr&gt;\n&lt;/td&gt;\n&lt;/tr&gt;\n&lt;tr&gt;\n&lt;td style=&quot;text-align: left;&quot;&gt;&lt;em&gt;Thanks,&lt;br&gt;\n[SITE_NAME] Team&lt;br&gt;\n&lt;a href=&quot;[URL]&quot;&gt;[URL]&lt;/a&gt;&lt;/em&gt; &lt;hr&gt;\n&lt;span style=&quot;font-size: 11px;&quot;&gt;&lt;em&gt;To stop receiving future newsletters please login into your account and uncheck newsletter subscription box.&lt;/em&gt;&lt;/span&gt;&lt;/td&gt;\n&lt;/tr&gt;\n    &lt;/tbody&gt;\n&lt;/table&gt;&lt;/div&gt;', '&lt;div align=&quot;center&quot;&gt;\n&lt;table style=&quot;background: none repeat scroll 0% 0% rgb(244, 244, 244); border: 1px solid rgb(102, 102, 102);&quot; cellpadding=&quot;5&quot; cellspacing=&quot;5&quot; border=&quot;0&quot; width=&quot;600&quot;&gt;\n    &lt;tbody&gt;\n&lt;tr&gt;\n&lt;th style=&quot;background-color: rgb(204, 204, 204);&quot;&gt;Hello [NAME]!&lt;/th&gt;\n&lt;/tr&gt;\n&lt;tr&gt;\n&lt;td style=&quot;text-align: left;&quot; valign=&quot;top&quot;&gt;You are receiving this email as a part of your newsletter subscription. &lt;hr&gt;\nHere goes your newsletter content &lt;hr&gt;\n&lt;/td&gt;\n&lt;/tr&gt;\n&lt;tr&gt;\n&lt;td style=&quot;text-align: left;&quot;&gt;&lt;em&gt;Thanks,&lt;br&gt;\n[SITE_NAME] Team&lt;br&gt;\n&lt;a href=&quot;[URL]&quot;&gt;[URL]&lt;/a&gt;&lt;/em&gt; &lt;hr&gt;\n&lt;span style=&quot;font-size: 11px;&quot;&gt;&lt;em&gt;To stop receiving future newsletters please login into your account and uncheck newsletter subscription box.&lt;/em&gt;&lt;/span&gt;&lt;/td&gt;\n&lt;/tr&gt;\n    &lt;/tbody&gt;\n&lt;/table&gt;&lt;/div&gt;', 'news', NULL),
(5, 'Transaction Completed', 'Transaction Completed', 'Payment Completed', 'Payment Completed', 'This template is used to notify administrator on successful payment transaction', 'This template is used to notify administrator on successful payment transaction', '&lt;div align=&quot;center&quot;&gt;\n&lt;table width=&quot;600&quot; cellspacing=&quot;5&quot; cellpadding=&quot;5&quot; border=&quot;0&quot; style=&quot;background: none repeat scroll 0% 0% rgb(244, 244, 244); border: 1px solid rgb(102, 102, 102);&quot;&gt;\n    &lt;tbody&gt;\n&lt;tr&gt;\n&lt;th style=&quot;background-color: rgb(204, 204, 204);&quot;&gt;Hello, Admin&lt;/th&gt;\n&lt;/tr&gt;\n&lt;tr&gt;\n&lt;td valign=&quot;top&quot; style=&quot;text-align: left;&quot;&gt;You have received new payment following:&lt;br /&gt;\n&lt;br /&gt;\nUsername: &lt;strong&gt;[USERNAME]&lt;/strong&gt;&lt;br /&gt;\nMembership: &lt;strong&gt;[ITEMNAME]&lt;/strong&gt;&lt;br /&gt;\nPrice: &lt;strong&gt;[PRICE]&lt;/strong&gt;&lt;br /&gt;\nStatus: &lt;strong&gt;[STATUS] &lt;/strong&gt;&lt;br /&gt;\r\nProcessor: &lt;strong&gt;[PP] &lt;/strong&gt;&lt;br /&gt;\nIP: &lt;strong&gt;[IP] &lt;/strong&gt;&lt;/td&gt;\n&lt;/tr&gt;\n&lt;tr&gt;\n&lt;td valign=&quot;top&quot; style=&quot;text-align: left;&quot;&gt;&lt;em&gt;You can view this transaction from your admin panel&lt;/em&gt;&lt;/td&gt;\n&lt;/tr&gt;\n    &lt;/tbody&gt;\n&lt;/table&gt;\n&lt;/div&gt;', '&lt;div align=&quot;center&quot;&gt;\n&lt;table width=&quot;600&quot; cellspacing=&quot;5&quot; cellpadding=&quot;5&quot; border=&quot;0&quot; style=&quot;background: none repeat scroll 0% 0% rgb(244, 244, 244); border: 1px solid rgb(102, 102, 102);&quot;&gt;\n    &lt;tbody&gt;\n&lt;tr&gt;\n&lt;th style=&quot;background-color: rgb(204, 204, 204);&quot;&gt;Hello, Admin&lt;/th&gt;\n&lt;/tr&gt;\n&lt;tr&gt;\n&lt;td valign=&quot;top&quot; style=&quot;text-align: left;&quot;&gt;You have received new payment following:&lt;br /&gt;\n&lt;br /&gt;\nUsername: &lt;strong&gt;[USERNAME]&lt;/strong&gt;&lt;br /&gt;\nMembership: &lt;strong&gt;[ITEMNAME]&lt;/strong&gt;&lt;br /&gt;\nPrice: &lt;strong&gt;[PRICE]&lt;/strong&gt;&lt;br /&gt;\nStatus: &lt;strong&gt;[STATUS] &lt;/strong&gt;&lt;br /&gt;\r\nProcessor: &lt;strong&gt;[PP] &lt;/strong&gt;&lt;br /&gt;\nIP: &lt;strong&gt;[IP] &lt;/strong&gt;&lt;/td&gt;\n&lt;/tr&gt;\n&lt;tr&gt;\n&lt;td valign=&quot;top&quot; style=&quot;text-align: left;&quot;&gt;&lt;em&gt;You can view this transaction from your admin panel&lt;/em&gt;&lt;/td&gt;\n&lt;/tr&gt;\n    &lt;/tbody&gt;\n&lt;/table&gt;\n&lt;/div&gt;', 'mailer', NULL),
(6, 'Transaction Suspicious', 'Transaction Suspicious', 'Suspicious Transaction', 'Suspicious Transaction', 'This template is used to notify administrator on failed/suspicious payment transaction', 'This template is used to notify administrator on failed/suspicious payment transaction', '&lt;div align=&quot;center&quot;&gt;\n&lt;table width=&quot;600&quot; cellspacing=&quot;5&quot; cellpadding=&quot;5&quot; border=&quot;0&quot; style=&quot;background: none repeat scroll 0% 0% rgb(244, 244, 244); border: 1px solid rgb(102, 102, 102);&quot;&gt;\n    &lt;tbody&gt;\n&lt;tr&gt;\n&lt;th style=&quot;background-color:#ccc&quot;&gt;Hello, Admin&lt;/th&gt;\n&lt;/tr&gt;\n&lt;tr&gt;\n&lt;td valign=&quot;top&quot; style=&quot;text-align:left&quot;&gt;The following transaction has been disabled due to suspicious activity:&lt;br /&gt;\n&lt;br /&gt;\nBuyer: &lt;strong&gt;[USERNAME]&lt;/strong&gt;&lt;br /&gt;\nItem: &lt;strong&gt;[ITEM]&lt;/strong&gt;&lt;br /&gt;\nPrice: &lt;strong&gt;[PRICE]&lt;/strong&gt;&lt;br /&gt;\nStatus: &lt;strong&gt;[STATUS]&lt;/strong&gt;&lt;/td&gt;\r\nProcessor: &lt;strong&gt;[PP] &lt;/strong&gt;&lt;br /&gt;\n&lt;/tr&gt;\n&lt;tr&gt;\n&lt;td style=&quot;text-align:left&quot;&gt;&lt;em&gt;Please verify this transaction is correct. If it is, please activate it in the transaction section of your site&#039;s &lt;br /&gt;\nadministration control panel. If not, it appears that someone tried to fraudulently obtain products from your site.&lt;/em&gt;&lt;/td&gt;\n&lt;/tr&gt;\n    &lt;/tbody&gt;\n&lt;/table&gt;\n&lt;/div&gt;', '&lt;div align=&quot;center&quot;&gt;\n&lt;table width=&quot;600&quot; cellspacing=&quot;5&quot; cellpadding=&quot;5&quot; border=&quot;0&quot; style=&quot;background: none repeat scroll 0% 0% rgb(244, 244, 244); border: 1px solid rgb(102, 102, 102);&quot;&gt;\n    &lt;tbody&gt;\n&lt;tr&gt;\n&lt;th style=&quot;background-color:#ccc&quot;&gt;Hello, Admin&lt;/th&gt;\n&lt;/tr&gt;\n&lt;tr&gt;\n&lt;td valign=&quot;top&quot; style=&quot;text-align:left&quot;&gt;The following transaction has been disabled due to suspicious activity:&lt;br /&gt;\n&lt;br /&gt;\nBuyer: &lt;strong&gt;[USERNAME]&lt;/strong&gt;&lt;br /&gt;\nItem: &lt;strong&gt;[ITEM]&lt;/strong&gt;&lt;br /&gt;\nPrice: &lt;strong&gt;[PRICE]&lt;/strong&gt;&lt;br /&gt;\nStatus: &lt;strong&gt;[STATUS]&lt;/strong&gt;&lt;/td&gt;\r\nProcessor: &lt;strong&gt;[PP] &lt;/strong&gt;&lt;br /&gt;\n&lt;/tr&gt;\n&lt;tr&gt;\n&lt;td style=&quot;text-align:left&quot;&gt;&lt;em&gt;Please verify this transaction is correct. If it is, please activate it in the transaction section of your site&#039;s &lt;br /&gt;\nadministration control panel. If not, it appears that someone tried to fraudulently obtain products from your site.&lt;/em&gt;&lt;/td&gt;\n&lt;/tr&gt;\n    &lt;/tbody&gt;\n&lt;/table&gt;\n&lt;/div&gt;', 'mailer', NULL),
(7, 'Welcome Email', 'Welcome Email', 'Welcome', 'Welcome', 'This template is used to welcome newly registered user when Configuration->Registration Verification and Configuration->Auto Registration are both set to YES', 'This template is used to welcome newly registered user when Configuration->Registration Verification and Configuration->Auto Registration are both set to YES', '&lt;div align=&quot;center&quot;&gt;\n&lt;table width=&quot;600&quot; cellspacing=&quot;5&quot; cellpadding=&quot;5&quot; border=&quot;0&quot; style=&quot;background: none repeat scroll 0% 0% rgb(244, 244, 244); border: 1px solid rgb(102, 102, 102);&quot;&gt;\n    &lt;tbody&gt;\n&lt;tr&gt;\n&lt;th style=&quot;background-color: rgb(204, 204, 204);&quot;&gt;Welcome [NAME]! Thanks for registering.&lt;/th&gt;\n&lt;/tr&gt;\n&lt;tr&gt;\n&lt;td style=&quot;text-align: left;&quot;&gt;Hello,&lt;br /&gt;\n&lt;br /&gt;\nYou&#039;re now a member of [SITE_NAME].&lt;br /&gt;\n&lt;br /&gt;\nHere are your login details. Please keep them in a safe place:&lt;br /&gt;\n&lt;br /&gt;\nUsername: &lt;strong&gt;[USERNAME]&lt;/strong&gt;&lt;br /&gt;\nPassword: &lt;strong&gt;[PASSWORD]&lt;/strong&gt;&lt;/td&gt;\n&lt;/tr&gt;\n&lt;tr&gt;\n&lt;td style=&quot;text-align: left;&quot;&gt;&lt;em&gt;Thanks,&lt;br /&gt;\n[SITE_NAME] Team&lt;br /&gt;\n&lt;a href=&quot;[URL]&quot;&gt;[URL]&lt;/a&gt;&lt;/em&gt;&lt;/td&gt;\n&lt;/tr&gt;\n    &lt;/tbody&gt;\n&lt;/table&gt;\n&lt;/div&gt;', '&lt;div align=&quot;center&quot;&gt;\n&lt;table width=&quot;600&quot; cellspacing=&quot;5&quot; cellpadding=&quot;5&quot; border=&quot;0&quot; style=&quot;background: none repeat scroll 0% 0% rgb(244, 244, 244); border: 1px solid rgb(102, 102, 102);&quot;&gt;\n    &lt;tbody&gt;\n&lt;tr&gt;\n&lt;th style=&quot;background-color: rgb(204, 204, 204);&quot;&gt;Welcome [NAME]! Thanks for registering.&lt;/th&gt;\n&lt;/tr&gt;\n&lt;tr&gt;\n&lt;td style=&quot;text-align: left;&quot;&gt;Hello,&lt;br /&gt;\n&lt;br /&gt;\nYou&#039;re now a member of [SITE_NAME].&lt;br /&gt;\n&lt;br /&gt;\nHere are your login details. Please keep them in a safe place:&lt;br /&gt;\n&lt;br /&gt;\nUsername: &lt;strong&gt;[USERNAME]&lt;/strong&gt;&lt;br /&gt;\nPassword: &lt;strong&gt;[PASSWORD]&lt;/strong&gt;&lt;/td&gt;\n&lt;/tr&gt;\n&lt;tr&gt;\n&lt;td style=&quot;text-align: left;&quot;&gt;&lt;em&gt;Thanks,&lt;br /&gt;\n[SITE_NAME] Team&lt;br /&gt;\n&lt;a href=&quot;[URL]&quot;&gt;[URL]&lt;/a&gt;&lt;/em&gt;&lt;/td&gt;\n&lt;/tr&gt;\n    &lt;/tbody&gt;\n&lt;/table&gt;\n&lt;/div&gt;', 'mailer', NULL),
(8, 'Membership Expire 7 days', 'Membership Expire 7 days', 'Your membership will expire in 7 days', 'Your membership will expire in 7 days', 'This template is used to remind user that membership will expire in 7 days', 'This template is used to remind user that membership will expire in 7 days', '&lt;div align=&quot;center&quot;&gt;\n&lt;table cellspacing=&quot;5&quot; cellpadding=&quot;5&quot; border=&quot;0&quot; width=&quot;600&quot; style=&quot;background: none repeat scroll 0% 0% rgb(244, 244, 244); border: 1px solid rgb(102, 102, 102);&quot;&gt;\n    &lt;tbody&gt;\n&lt;tr&gt;\n&lt;th style=&quot;background-color: rgb(204, 204, 204);&quot;&gt;Hello, [NAME]&lt;/th&gt;\n&lt;/tr&gt;\n&lt;tr&gt;\n&lt;td valign=&quot;top&quot; style=&quot;text-align: left;&quot;&gt;\n&lt;h2 style=&quot;color: rgb(255, 0, 0);&quot;&gt;Your current membership will expire in 7 days&lt;/h2&gt;\nPlease login to your user panel to extend or upgrade your membership.&lt;/td&gt;\n&lt;/tr&gt;\n&lt;tr&gt;\n&lt;td style=&quot;text-align: left;&quot;&gt;&lt;em&gt;Thanks,&lt;br /&gt;\n[SITE_NAME] Team&lt;br /&gt;\n&lt;a href=&quot;[URL]&quot;&gt;[URL]&lt;/a&gt;&lt;/em&gt;&lt;/td&gt;\n&lt;/tr&gt;\n    &lt;/tbody&gt;\n&lt;/table&gt;\n&lt;/div&gt;', '&lt;div align=&quot;center&quot;&gt;\n&lt;table cellspacing=&quot;5&quot; cellpadding=&quot;5&quot; border=&quot;0&quot; width=&quot;600&quot; style=&quot;background: none repeat scroll 0% 0% rgb(244, 244, 244); border: 1px solid rgb(102, 102, 102);&quot;&gt;\n    &lt;tbody&gt;\n&lt;tr&gt;\n&lt;th style=&quot;background-color: rgb(204, 204, 204);&quot;&gt;Hello, [NAME]&lt;/th&gt;\n&lt;/tr&gt;\n&lt;tr&gt;\n&lt;td valign=&quot;top&quot; style=&quot;text-align: left;&quot;&gt;\n&lt;h2 style=&quot;color: rgb(255, 0, 0);&quot;&gt;Your current membership will expire in 7 days&lt;/h2&gt;\nPlease login to your user panel to extend or upgrade your membership.&lt;/td&gt;\n&lt;/tr&gt;\n&lt;tr&gt;\n&lt;td style=&quot;text-align: left;&quot;&gt;&lt;em&gt;Thanks,&lt;br /&gt;\n[SITE_NAME] Team&lt;br /&gt;\n&lt;a href=&quot;[URL]&quot;&gt;[URL]&lt;/a&gt;&lt;/em&gt;&lt;/td&gt;\n&lt;/tr&gt;\n    &lt;/tbody&gt;\n&lt;/table&gt;\n&lt;/div&gt;', 'mailer', NULL),
(9, 'Membership expired today', 'Membership expired today', 'Your membership has expired', 'Your membership has expired', 'This template is used to remind user that membership had expired', 'This template is used to remind user that membership had expired', '&lt;div align=&quot;center&quot;&gt;\n&lt;table width=&quot;600&quot; cellspacing=&quot;5&quot; cellpadding=&quot;5&quot; border=&quot;0&quot; style=&quot;background: none repeat scroll 0% 0% rgb(244, 244, 244); border: 1px solid rgb(102, 102, 102);&quot;&gt;\n    &lt;tbody&gt;\n&lt;tr&gt;\n&lt;th style=&quot;background-color: rgb(204, 204, 204);&quot;&gt;Hello, [NAME]&lt;/th&gt;\n&lt;/tr&gt;\n&lt;tr&gt;\n&lt;td valign=&quot;top&quot; style=&quot;text-align: left;&quot;&gt;\n&lt;h2 style=&quot;color: rgb(255, 0, 0);&quot;&gt;Your current membership has expired!&lt;/h2&gt;\nPlease login to your user panel to extend or upgrade your membership.&lt;/td&gt;\n&lt;/tr&gt;\n&lt;tr&gt;\n&lt;td style=&quot;text-align: left;&quot;&gt;&lt;em&gt;Thanks,&lt;br /&gt;\n[SITE_NAME] Team&lt;br /&gt;\n&lt;a href=&quot;[URL]&quot;&gt;[URL]&lt;/a&gt;&lt;/em&gt;&lt;/td&gt;\n&lt;/tr&gt;\n    &lt;/tbody&gt;\n&lt;/table&gt;\n&lt;/div&gt;', '&lt;div align=&quot;center&quot;&gt;\n&lt;table width=&quot;600&quot; cellspacing=&quot;5&quot; cellpadding=&quot;5&quot; border=&quot;0&quot; style=&quot;background: none repeat scroll 0% 0% rgb(244, 244, 244); border: 1px solid rgb(102, 102, 102);&quot;&gt;\n    &lt;tbody&gt;\n&lt;tr&gt;\n&lt;th style=&quot;background-color: rgb(204, 204, 204);&quot;&gt;Hello, [NAME]&lt;/th&gt;\n&lt;/tr&gt;\n&lt;tr&gt;\n&lt;td valign=&quot;top&quot; style=&quot;text-align: left;&quot;&gt;\n&lt;h2 style=&quot;color: rgb(255, 0, 0);&quot;&gt;Your current membership has expired!&lt;/h2&gt;\nPlease login to your user panel to extend or upgrade your membership.&lt;/td&gt;\n&lt;/tr&gt;\n&lt;tr&gt;\n&lt;td style=&quot;text-align: left;&quot;&gt;&lt;em&gt;Thanks,&lt;br /&gt;\n[SITE_NAME] Team&lt;br /&gt;\n&lt;a href=&quot;[URL]&quot;&gt;[URL]&lt;/a&gt;&lt;/em&gt;&lt;/td&gt;\n&lt;/tr&gt;\n    &lt;/tbody&gt;\n&lt;/table&gt;\n&lt;/div&gt;', 'mailer', NULL),
(10, 'Contact Request', 'Contact Request', 'Contact Inquiry', 'Contact Inquiry', 'This template is used to send default Contact Request Form', 'This template is used to send default Contact Request Form', '\n&lt;div align=&quot;center&quot;&gt;\n	&lt;table width=&quot;600&quot; cellspacing=&quot;5&quot; cellpadding=&quot;5&quot; border=&quot;0&quot; style=&quot;background: none repeat scroll 0% 0% rgb(244, 244, 244); border: 1px solid rgb(102, 102, 102);&quot;&gt;\n		&lt;tbody&gt;\n&lt;tr&gt;\n&lt;th style=&quot;background-color: rgb(204, 204, 204);&quot;&gt;Hello Admin&lt;/th&gt;\n&lt;/tr&gt;\n&lt;tr&gt;\n&lt;td valign=&quot;top&quot; style=&quot;text-align: left;&quot;&gt;You have a new contact request: &lt;hr /&gt;\n	 [MESSAGE] &lt;hr /&gt;\n	 From: &lt;span style=&quot;font-weight: bold;&quot;&gt;[SENDER] - [NAME]&lt;/span&gt;&lt;br /&gt;\n	Telephone: &lt;span style=&quot;font-weight: bold;&quot;&gt;[PHONE]&lt;/span&gt;&lt;br /&gt;\n	Subject: &lt;span style=&quot;font-weight: bold;&quot;&gt;[MAILSUBJECT]&lt;/span&gt;&lt;br /&gt;\n	Senders IP: &lt;span style=&quot;font-weight: bold;&quot;&gt;[IP]&lt;/span&gt;&lt;/td&gt;\n&lt;/tr&gt;\n		&lt;/tbody&gt;\n	&lt;/table&gt;&lt;/div&gt;', '\n&lt;div align=&quot;center&quot;&gt;\n	&lt;table width=&quot;600&quot; cellspacing=&quot;5&quot; cellpadding=&quot;5&quot; border=&quot;0&quot; style=&quot;background: none repeat scroll 0% 0% rgb(244, 244, 244); border: 1px solid rgb(102, 102, 102);&quot;&gt;\n		&lt;tbody&gt;\n&lt;tr&gt;\n&lt;th style=&quot;background-color: rgb(204, 204, 204);&quot;&gt;Hello Admin&lt;/th&gt;\n&lt;/tr&gt;\n&lt;tr&gt;\n&lt;td valign=&quot;top&quot; style=&quot;text-align: left;&quot;&gt;You have a new contact request: &lt;hr /&gt;\n	 [MESSAGE] &lt;hr /&gt;\n	 From: &lt;span style=&quot;font-weight: bold;&quot;&gt;[SENDER] - [NAME]&lt;/span&gt;&lt;br /&gt;\n	Telephone: &lt;span style=&quot;font-weight: bold;&quot;&gt;[PHONE]&lt;/span&gt;&lt;br /&gt;\n	Subject: &lt;span style=&quot;font-weight: bold;&quot;&gt;[MAILSUBJECT]&lt;/span&gt;&lt;br /&gt;\n	Senders IP: &lt;span style=&quot;font-weight: bold;&quot;&gt;[IP]&lt;/span&gt;&lt;/td&gt;\n&lt;/tr&gt;\n		&lt;/tbody&gt;\n	&lt;/table&gt;&lt;/div&gt;', 'mailer', NULL),
(11, 'New Comment', 'New Comment', 'New Comment Added', 'New Comment Added', 'This template is used to notify admin when new comment has been added', 'This template is used to notify admin when new comment has been added', '&lt;div align=&quot;center&quot;&gt;\n&lt;table width=&quot;600&quot; cellspacing=&quot;5&quot; cellpadding=&quot;5&quot; border=&quot;0&quot; style=&quot;background: none repeat scroll 0% 0% rgb(244, 244, 244); border: 1px solid rgb(102, 102, 102);&quot;&gt;\n    &lt;tbody&gt;\n&lt;tr&gt;\n&lt;th style=&quot;background-color: rgb(204, 204, 204);&quot;&gt;Hello Admin&lt;/th&gt;\n&lt;/tr&gt;\n&lt;tr&gt;\n&lt;td valign=&quot;top&quot; style=&quot;text-align: left;&quot;&gt;You have a new comment post. You can login into your admin panel to view details: &lt;hr /&gt;\n[MESSAGE] &lt;hr /&gt;\nFrom: &lt;strong&gt;[SENDER] - [NAME]&lt;/strong&gt;&lt;br /&gt;\nwww: &lt;strong&gt;[WWW]&lt;/strong&gt;&lt;br /&gt;\nPage Url: &lt;strong&gt;&lt;a href=&quot;[PAGEURL]&quot;&gt;[PAGEURL]&lt;/a&gt;&lt;/strong&gt;&lt;br /&gt;\nSenders IP: &lt;strong&gt;[IP]&lt;/strong&gt;&lt;/td&gt;\n&lt;/tr&gt;\n    &lt;/tbody&gt;\n&lt;/table&gt;\n&lt;/div&gt;', '&lt;div align=&quot;center&quot;&gt;\n&lt;table width=&quot;600&quot; cellspacing=&quot;5&quot; cellpadding=&quot;5&quot; border=&quot;0&quot; style=&quot;background: none repeat scroll 0% 0% rgb(244, 244, 244); border: 1px solid rgb(102, 102, 102);&quot;&gt;\n    &lt;tbody&gt;\n&lt;tr&gt;\n&lt;th style=&quot;background-color: rgb(204, 204, 204);&quot;&gt;Hello Admin&lt;/th&gt;\n&lt;/tr&gt;\n&lt;tr&gt;\n&lt;td valign=&quot;top&quot; style=&quot;text-align: left;&quot;&gt;You have a new comment post. You can login into your admin panel to view details: &lt;hr /&gt;\n[MESSAGE] &lt;hr /&gt;\nFrom: &lt;strong&gt;[SENDER] - [NAME]&lt;/strong&gt;&lt;br /&gt;\nwww: &lt;strong&gt;[WWW]&lt;/strong&gt;&lt;br /&gt;\nPage Url: &lt;strong&gt;&lt;a href=&quot;[PAGEURL]&quot;&gt;[PAGEURL]&lt;/a&gt;&lt;/strong&gt;&lt;br /&gt;\nSenders IP: &lt;strong&gt;[IP]&lt;/strong&gt;&lt;/td&gt;\n&lt;/tr&gt;\n    &lt;/tbody&gt;\n&lt;/table&gt;\n&lt;/div&gt;', 'mailer', NULL),
(12, 'Single Email', 'Single Email', 'Single User Email', 'Single User Email', 'This template is used to email single user', 'This template is used to email single user', '&lt;div align=&quot;center&quot;&gt;\n  &lt;table width=&quot;600&quot; cellspacing=&quot;5&quot; cellpadding=&quot;5&quot; border=&quot;0&quot; style=&quot;background: none repeat scroll 0% 0% rgb(244, 244, 244); border: 1px solid rgb(102, 102, 102);&quot;&gt;\n    &lt;tbody&gt;\n      &lt;tr&gt;\n&lt;th style=&quot;background-color:#ccc&quot;&gt;Hello [NAME]&lt;/th&gt;\n      &lt;/tr&gt;\n      &lt;tr&gt;\n&lt;td valign=&quot;top&quot; style=&quot;text-align:left&quot;&gt;Your message goes here...&lt;/td&gt;\n      &lt;/tr&gt;\n      &lt;tr&gt;\n&lt;td style=&quot;text-align:left&quot;&gt;&lt;em&gt;Thanks,&lt;br /&gt;\n[SITE_NAME] Team&lt;br /&gt;\n&lt;a href=&quot;[URL]&quot;&gt;[URL]&lt;/a&gt;&lt;/em&gt;&lt;/td&gt;\n      &lt;/tr&gt;\n    &lt;/tbody&gt;\n  &lt;/table&gt;\n&lt;/div&gt;', '&lt;div align=&quot;center&quot;&gt;\n  &lt;table width=&quot;600&quot; cellspacing=&quot;5&quot; cellpadding=&quot;5&quot; border=&quot;0&quot; style=&quot;background: none repeat scroll 0% 0% rgb(244, 244, 244); border: 1px solid rgb(102, 102, 102);&quot;&gt;\n    &lt;tbody&gt;\n      &lt;tr&gt;\n&lt;th style=&quot;background-color:#ccc&quot;&gt;Hello [NAME]&lt;/th&gt;\n      &lt;/tr&gt;\n      &lt;tr&gt;\n&lt;td valign=&quot;top&quot; style=&quot;text-align:left&quot;&gt;Your message goes here...&lt;/td&gt;\n      &lt;/tr&gt;\n      &lt;tr&gt;\n&lt;td style=&quot;text-align:left&quot;&gt;&lt;em&gt;Thanks,&lt;br /&gt;\n[SITE_NAME] Team&lt;br /&gt;\n&lt;a href=&quot;[URL]&quot;&gt;[URL]&lt;/a&gt;&lt;/em&gt;&lt;/td&gt;\n      &lt;/tr&gt;\n    &lt;/tbody&gt;\n  &lt;/table&gt;\n&lt;/div&gt;', 'mailer', NULL),
(13, 'Notify Admin', 'Notify Admin', 'New User Registration', 'New User Registration', 'This template is used to notify admin of new registration when Configuration->Registration Notification is set to YES', 'This template is used to notify admin of new registration when Configuration->Registration Notification is set to YES', '&lt;div align=&quot;center&quot;&gt;\n&lt;table cellspacing=&quot;5&quot; cellpadding=&quot;5&quot; border=&quot;0&quot; width=&quot;600&quot; style=&quot;background: none repeat scroll 0% 0% rgb(244, 244, 244); border: 1px solid rgb(102, 102, 102);&quot;&gt;\n    &lt;tbody&gt;\n&lt;tr&gt;\n&lt;th style=&quot;background-color: rgb(204, 204, 204);&quot;&gt;Hello Admin&lt;/th&gt;\n&lt;/tr&gt;\n&lt;tr&gt;\n&lt;td valign=&quot;top&quot; style=&quot;text-align: left;&quot;&gt;You have a new user registration. You can login into your admin panel to view details:&lt;hr /&gt;\nUsername: &lt;strong&gt;[USERNAME]&lt;/strong&gt;&lt;br /&gt;\nName: &lt;strong&gt;[NAME]&lt;/strong&gt;&lt;br /&gt;\nIP: &lt;strong&gt;[IP]&lt;/strong&gt;&lt;/td&gt;\n&lt;/tr&gt;\n    &lt;/tbody&gt;\n&lt;/table&gt;\n&lt;/div&gt;', '&lt;div align=&quot;center&quot;&gt;\n&lt;table cellspacing=&quot;5&quot; cellpadding=&quot;5&quot; border=&quot;0&quot; width=&quot;600&quot; style=&quot;background: none repeat scroll 0% 0% rgb(244, 244, 244); border: 1px solid rgb(102, 102, 102);&quot;&gt;\n    &lt;tbody&gt;\n&lt;tr&gt;\n&lt;th style=&quot;background-color: rgb(204, 204, 204);&quot;&gt;Hello Admin&lt;/th&gt;\n&lt;/tr&gt;\n&lt;tr&gt;\n&lt;td valign=&quot;top&quot; style=&quot;text-align: left;&quot;&gt;You have a new user registration. You can login into your admin panel to view details:&lt;hr /&gt;\nUsername: &lt;strong&gt;[USERNAME]&lt;/strong&gt;&lt;br /&gt;\nName: &lt;strong&gt;[NAME]&lt;/strong&gt;&lt;br /&gt;\nIP: &lt;strong&gt;[IP]&lt;/strong&gt;&lt;/td&gt;\n&lt;/tr&gt;\n    &lt;/tbody&gt;\n&lt;/table&gt;\n&lt;/div&gt;', 'mailer', NULL),
(14, 'Registration Pending', 'Registration Pending', 'Registration Verification Pending', 'Registration Verification Pending', 'This template is used to send Registration Verification Email, when Configuration->Auto Registration is set to NO', 'This template is used to send Registration Verification Email, when Configuration->Auto Registration is set to NO', '&lt;div align=&quot;center&quot;&gt;\n&lt;table cellspacing=&quot;5&quot; cellpadding=&quot;5&quot; border=&quot;0&quot; width=&quot;600&quot; style=&quot;background: none repeat scroll 0% 0% rgb(244, 244, 244); border: 1px solid rgb(102, 102, 102);&quot;&gt;\n    &lt;tbody&gt;\n&lt;tr&gt;\n&lt;th style=&quot;background-color: rgb(204, 204, 204);&quot;&gt;Welcome [NAME]! Thanks for registering.&lt;/th&gt;\n&lt;/tr&gt;\n&lt;tr&gt;\n&lt;td valign=&quot;top&quot; style=&quot;text-align: left;&quot;&gt;Hello,&lt;br /&gt;\n&lt;br /&gt;\nYou&#039;re now a member of [SITE_NAME].&lt;br /&gt;\n&lt;br /&gt;\nHere are your login details. Please keep them in a safe place:&lt;br /&gt;\n&lt;br /&gt;\nUsername: &lt;strong&gt;[USERNAME]&lt;/strong&gt;&lt;br /&gt;\nPassword: &lt;strong&gt;[PASSWORD]&lt;/strong&gt; &lt;hr /&gt;\nThe administrator of this site has requested all new accounts&lt;br /&gt;\nto be activated by the users who created them thus your account&lt;br /&gt;\nis currently pending verification process.&lt;/td&gt;\n&lt;/tr&gt;\n&lt;tr&gt;\n&lt;td style=&quot;text-align: left;&quot;&gt;&lt;em&gt;Thanks,&lt;br /&gt;\n[SITE_NAME] Team&lt;br /&gt;\n&lt;a href=&quot;[URL]&quot;&gt;[URL]&lt;/a&gt;&lt;/em&gt;&lt;/td&gt;\n&lt;/tr&gt;\n    &lt;/tbody&gt;\n&lt;/table&gt;\n&lt;/div&gt;', '&lt;div align=&quot;center&quot;&gt;\n&lt;table cellspacing=&quot;5&quot; cellpadding=&quot;5&quot; border=&quot;0&quot; width=&quot;600&quot; style=&quot;background: none repeat scroll 0% 0% rgb(244, 244, 244); border: 1px solid rgb(102, 102, 102);&quot;&gt;\n    &lt;tbody&gt;\n&lt;tr&gt;\n&lt;th style=&quot;background-color: rgb(204, 204, 204);&quot;&gt;Welcome [NAME]! Thanks for registering.&lt;/th&gt;\n&lt;/tr&gt;\n&lt;tr&gt;\n&lt;td valign=&quot;top&quot; style=&quot;text-align: left;&quot;&gt;Hello,&lt;br /&gt;\n&lt;br /&gt;\nYou&#039;re now a member of [SITE_NAME].&lt;br /&gt;\n&lt;br /&gt;\nHere are your login details. Please keep them in a safe place:&lt;br /&gt;\n&lt;br /&gt;\nUsername: &lt;strong&gt;[USERNAME]&lt;/strong&gt;&lt;br /&gt;\nPassword: &lt;strong&gt;[PASSWORD]&lt;/strong&gt; &lt;hr /&gt;\nThe administrator of this site has requested all new accounts&lt;br /&gt;\nto be activated by the users who created them thus your account&lt;br /&gt;\nis currently pending verification process.&lt;/td&gt;\n&lt;/tr&gt;\n&lt;tr&gt;\n&lt;td style=&quot;text-align: left;&quot;&gt;&lt;em&gt;Thanks,&lt;br /&gt;\n[SITE_NAME] Team&lt;br /&gt;\n&lt;a href=&quot;[URL]&quot;&gt;[URL]&lt;/a&gt;&lt;/em&gt;&lt;/td&gt;\n&lt;/tr&gt;\n    &lt;/tbody&gt;\n&lt;/table&gt;\n&lt;/div&gt;', 'mailer', NULL),
(15, 'Offline Payment', 'Offline Payment', 'Offline Notification', 'Offline Notification', 'This template is used to send notification to a user when offline payment method is being used', 'This template is used to send notification to a user when offline payment method is being used', '\n&lt;div align=&quot;center&quot; style=&quot;font-family: Arial,Helvetica,sans-serif; font-size: 13px; margin: 20px;&quot;&gt;\n	&lt;table width=&quot;600&quot; cellspacing=&quot;5&quot; cellpadding=&quot;10&quot; border=&quot;0&quot; style=&quot;background: none repeat scroll 0% 0% rgb(244, 244, 244); border: 2px solid rgb(187, 187, 187);&quot;&gt;\n		&lt;tbody&gt;\n&lt;tr&gt;\n&lt;th style=&quot;background-color: rgb(204, 204, 204); font-size: 16px; padding: 5px; border-bottom: 2px solid rgb(255, 255, 255);&quot;&gt;Hello [NAME]&lt;/th&gt;\n&lt;/tr&gt;\n&lt;tr&gt;\n&lt;td valign=&quot;top&quot; style=&quot;text-align: left;&quot;&gt;You have purchased the following:&lt;/td&gt;\n&lt;/tr&gt;\n&lt;tr&gt;\n&lt;td valign=&quot;top&quot; style=&quot;text-align: left;&quot;&gt;[ITEMS]&lt;/td&gt;\n&lt;/tr&gt;\n&lt;tr&gt;\n&lt;td valign=&quot;top&quot; style=&quot;text-align: left;&quot;&gt;Please send your payment to:&lt;br /&gt;\n	&lt;/td&gt;\n&lt;/tr&gt;\n&lt;tr&gt;\n&lt;td valign=&quot;top&quot; style=&quot;text-align: left;&quot;&gt;[INFO]&lt;/td&gt;\n&lt;/tr&gt;\n&lt;tr&gt;\n&lt;td valign=&quot;top&quot; style=&quot;text-align: left; background-color: rgb(255, 255, 255); border-top: 2px solid rgb(204, 204, 204);&quot;&gt;&lt;span style=&quot;font-style: italic;&quot;&gt;Thanks,&lt;br /&gt;\n		[SITENAME] Team&lt;br /&gt;\n		&lt;a href=&quot;[URL]&quot;&gt;[URL]&lt;/a&gt;&lt;/span&gt;&lt;/td&gt;\n&lt;/tr&gt;\n		&lt;/tbody&gt;\n	&lt;/table&gt;&lt;/div&gt;', '\n&lt;div align=&quot;center&quot; style=&quot;font-family: Arial,Helvetica,sans-serif; font-size: 13px; margin: 20px;&quot;&gt;\n	&lt;table width=&quot;600&quot; cellspacing=&quot;5&quot; cellpadding=&quot;10&quot; border=&quot;0&quot; style=&quot;background: none repeat scroll 0% 0% rgb(244, 244, 244); border: 2px solid rgb(187, 187, 187);&quot;&gt;\n		&lt;tbody&gt;\n&lt;tr&gt;\n&lt;th style=&quot;background-color: rgb(204, 204, 204); font-size: 16px; padding: 5px; border-bottom: 2px solid rgb(255, 255, 255);&quot;&gt;Hello [NAME]&lt;/th&gt;\n&lt;/tr&gt;\n&lt;tr&gt;\n&lt;td valign=&quot;top&quot; style=&quot;text-align: left;&quot;&gt;You have purchased the following:&lt;/td&gt;\n&lt;/tr&gt;\n&lt;tr&gt;\n&lt;td valign=&quot;top&quot; style=&quot;text-align: left;&quot;&gt;[ITEMS]&lt;/td&gt;\n&lt;/tr&gt;\n&lt;tr&gt;\n&lt;td valign=&quot;top&quot; style=&quot;text-align: left;&quot;&gt;Please send your payment to:&lt;br /&gt;\n	&lt;/td&gt;\n&lt;/tr&gt;\n&lt;tr&gt;\n&lt;td valign=&quot;top&quot; style=&quot;text-align: left;&quot;&gt;[INFO]&lt;/td&gt;\n&lt;/tr&gt;\n&lt;tr&gt;\n&lt;td valign=&quot;top&quot; style=&quot;text-align: left; background-color: rgb(255, 255, 255); border-top: 2px solid rgb(204, 204, 204);&quot;&gt;&lt;span style=&quot;font-style: italic;&quot;&gt;Thanks,&lt;br /&gt;\n		[SITENAME] Team&lt;br /&gt;\n		&lt;a href=&quot;[URL]&quot;&gt;[URL]&lt;/a&gt;&lt;/span&gt;&lt;/td&gt;\n&lt;/tr&gt;\n		&lt;/tbody&gt;\n	&lt;/table&gt;&lt;/div&gt;', 'mailer', NULL),
(16, 'Job application', 'İş Başvurusu', 'Job application', 'İş Başvurusu', 'Job application form', 'İş Başvurusu Formu', '&lt;div align=&quot;center&quot;&gt;\n	&lt;table width=&quot;600&quot; cellspacing=&quot;5&quot; cellpadding=&quot;5&quot; border=&quot;0&quot; style=&quot;background: none repeat scroll 0% 0% rgb(244, 244, 244); border: 1px solid rgb(102, 102, 102);&quot;&gt;\n	&lt;tbody&gt;\n	&lt;tr&gt;\n		&lt;th style=&quot;background-color: rgb(204, 204, 204);&quot;&gt;\n			Hello Admin\n		&lt;/th&gt;\n	&lt;/tr&gt;\n	&lt;tr&gt;\n		&lt;td valign=&quot;top&quot; style=&quot;text-align: left;&quot;&gt;\n			You have a new job application&amp;nbsp;request:\n			&lt;hr&gt;\n			&lt;b&gt; [JOB]&lt;/b&gt;\n			&lt;hr&gt;\n	 From: \n			&lt;b&gt;[SENDER] - [NAME]&lt;/b&gt;&lt;br&gt;\n	Telephone: \n			&lt;b&gt;[PHONE]&lt;/b&gt;&lt;br&gt;\n	Gender: \n			&lt;b&gt;[GENDER]&lt;/b&gt;&lt;br&gt;\n	Senders IP: \n			&lt;b&gt;[IP]&lt;/b&gt;\n		&lt;/td&gt;\n	&lt;/tr&gt;\n	&lt;/tbody&gt;\n	&lt;/table&gt;\n&lt;/div&gt;', '&lt;div align=&quot;center&quot;&gt;\n	&lt;table width=&quot;600&quot; cellspacing=&quot;5&quot; cellpadding=&quot;5&quot; border=&quot;0&quot; style=&quot;background: none repeat scroll 0% 0% rgb(244, 244, 244); border: 1px solid rgb(102, 102, 102);&quot;&gt;\n	&lt;tbody&gt;\n	&lt;tr&gt;\n		&lt;th style=&quot;background-color: rgb(204, 204, 204);&quot;&gt;\n			Hello Admin\n		&lt;/th&gt;\n	&lt;/tr&gt;\n	&lt;tr&gt;\n		&lt;td valign=&quot;top&quot; style=&quot;text-align: left;&quot;&gt;\n			Yeni bir iş başvurusu isteğiniz var:\n			&lt;hr&gt;\n	 &lt;b&gt; [JOB]&lt;/b&gt;\n			&lt;hr&gt;\n	 Kimden: \n			&lt;b&gt;[SENDER] - [NAME]&lt;/b&gt;&lt;br&gt;\n	Telefon: \n			&lt;b&gt;[PHONE]&lt;/b&gt;&lt;br&gt;\n			Cinsiyet: \n			&lt;b&gt;[GENDER]&lt;/b&gt;&lt;br&gt;\n	Gönderici IP: \n			&lt;b&gt;[IP]&lt;/b&gt;\n		&lt;/td&gt;\n	&lt;/tr&gt;\n	&lt;/tbody&gt;\n	&lt;/table&gt;\n&lt;/div&gt;', 'news', NULL),
(17, 'Demo Request', 'Sunum Talebi', 'Demo Request', 'Sunum Talebi', '', '', '&lt;div align=&quot;center&quot;&gt;\n	&lt;table width=&quot;600&quot; cellspacing=&quot;5&quot; cellpadding=&quot;5&quot; border=&quot;0&quot; style=&quot;background: none repeat scroll 0% 0% rgb(244, 244, 244); border: 1px solid rgb(102, 102, 102);&quot;&gt;\n	&lt;tbody&gt;\n	&lt;tr&gt;\n		&lt;th style=&quot;background-color: rgb(204, 204, 204);&quot;&gt;\n			Hello Admin\n		&lt;/th&gt;\n	&lt;/tr&gt;\n	&lt;tr&gt;\n		&lt;td valign=&quot;top&quot; style=&quot;text-align: left;&quot;&gt;\n			Yeni bir sunum talebi isteğiniz var:\n			&lt;hr&gt;\n			&lt;b&gt; [LIST]&lt;/b&gt;\n			&lt;hr&gt;\n	 Kimden: \n			&lt;b&gt;[SENDER] - [NAME]&lt;/b&gt;&lt;br&gt;\n	Telefon: \n			&lt;b&gt;[PHONE]&lt;/b&gt;&lt;br&gt;\n			Cinsiyet: \n			&lt;b&gt;[COMPANY]&lt;/b&gt;&lt;br&gt;\n	Gönderici IP: \n			&lt;b&gt;[IP]&lt;/b&gt;\n		&lt;/td&gt;\n	&lt;/tr&gt;\n	&lt;/tbody&gt;\n	&lt;/table&gt;\n&lt;/div&gt;', '&lt;div align=&quot;center&quot;&gt;\n	&lt;table width=&quot;600&quot; cellspacing=&quot;5&quot; cellpadding=&quot;5&quot; border=&quot;0&quot; style=&quot;background: none repeat scroll 0% 0% rgb(244, 244, 244); border: 1px solid rgb(102, 102, 102);&quot;&gt;\n	&lt;tbody&gt;\n	&lt;tr&gt;\n		&lt;th style=&quot;background-color: rgb(204, 204, 204);&quot;&gt;\n			Merhaba Admin\n		&lt;/th&gt;\n	&lt;/tr&gt;\n	&lt;tr&gt;\n		&lt;td valign=&quot;top&quot; style=&quot;text-align: left;&quot;&gt;\n			Yeni bir sunum talebi isteğiniz var:\n			&lt;hr&gt;\n			&lt;b&gt; [LIST]&lt;/b&gt;\n			&lt;hr&gt;\n	 Kimden: \n			&lt;b&gt;[SENDER] - [NAME]&lt;/b&gt;&lt;br&gt;\n	Telefon: \n			&lt;b&gt;[PHONE]&lt;/b&gt;&lt;br&gt;\n			Cinsiyet: \n			&lt;b&gt;[COMPANY]&lt;/b&gt;&lt;br&gt;\n	Gönderici IP: \n			&lt;b&gt;[IP]&lt;/b&gt;\n		&lt;/td&gt;\n	&lt;/tr&gt;\n	&lt;/tbody&gt;\n	&lt;/table&gt;\n&lt;/div&gt;', 'news', NULL);

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `gateways`
--

CREATE TABLE `gateways` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `displayname` varchar(255) NOT NULL,
  `dir` varchar(255) NOT NULL,
  `live` tinyint(1) NOT NULL DEFAULT '1',
  `extra_txt` varchar(255) NOT NULL,
  `extra_txt2` varchar(255) NOT NULL,
  `extra_txt3` varchar(255) DEFAULT NULL,
  `extra` varchar(255) NOT NULL,
  `extra2` varchar(255) NOT NULL,
  `extra3` text,
  `is_recurring` tinyint(1) NOT NULL DEFAULT '0',
  `active` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Tablo döküm verisi `gateways`
--

INSERT INTO `gateways` (`id`, `name`, `displayname`, `dir`, `live`, `extra_txt`, `extra_txt2`, `extra_txt3`, `extra`, `extra2`, `extra3`, `is_recurring`, `active`) VALUES
(1, 'paypal', 'PayPal', 'paypal', 0, 'Email Address', 'Currency Code', 'Not in Use', 'paypal@address.com', 'CAD', '', 1, 1),
(2, 'skrill', 'Skrill', 'skrill', 1, 'Email Address', 'Currency Code', 'Secret Passphrase', 'moneybookers@address.com', 'EUR', 'mypassphrase', 1, 1),
(3, 'offline', 'Offline Payment', 'offline', 0, 'Not in Use', 'Not in Use', 'Instructions', '', '', 'Please submit all payments to:\nBank Name:\nBank Account:\netc...', 0, 1),
(4, 'stripe', 'Stripe', 'stripe', 0, 'Secret Key', 'Currency Code', 'Publishable Key', '', 'CAD', '', 0, 1),
(5, 'payfast', 'PayFast', 'payfast', 0, 'Merchant ID', 'Merchant Key', 'PassPhrase', '', '', '', 0, 1),
(6, 'iyzipay', 'iyzipay', 'iyzipay', 0, 'Api Key', 'Currency Code', 'Secret Key', 'sandbox-OlCGQAIAW0VbLX9d5gzjWnA1eqsuWhhl', 'TRY', 'sandbox-5pPCujgLUP7DJozCrOj8HJwcjOZVKTjQ', 1, 1);

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `language`
--

CREATE TABLE `language` (
  `id` int(4) NOT NULL,
  `name` varchar(100) DEFAULT NULL,
  `flag` varchar(2) DEFAULT NULL,
  `langdir` enum('ltr','rtl') DEFAULT 'ltr',
  `author` varchar(200) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Tablo döküm verisi `language`
--

INSERT INTO `language` (`id`, `name`, `flag`, `langdir`, `author`) VALUES
(1, 'English', 'en', 'ltr', 'kodar'),
(2, 'Türkçe', 'tr', 'ltr', 'kodar');

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `layout`
--

CREATE TABLE `layout` (
  `plug_id` int(11) NOT NULL DEFAULT '0',
  `page_id` int(11) NOT NULL,
  `mod_id` int(11) NOT NULL DEFAULT '0',
  `modalias` varchar(30) DEFAULT NULL,
  `page_slug` varchar(50) DEFAULT NULL,
  `is_content` tinyint(1) NOT NULL DEFAULT '0',
  `plug_name` varchar(60) DEFAULT NULL,
  `place` varchar(20) NOT NULL,
  `space` tinyint(1) NOT NULL DEFAULT '10',
  `position` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Tablo döküm verisi `layout`
--

INSERT INTO `layout` (`plug_id`, `page_id`, `mod_id`, `modalias`, `page_slug`, `is_content`, `plug_name`, `place`, `space`, `position`) VALUES
(8, 7, 0, NULL, 'three-columns', 0, NULL, 'right', 10, 2),
(3, 7, 0, NULL, 'three-columns', 0, NULL, 'left', 10, 13),
(9, 7, 0, NULL, 'three-columns', 0, NULL, 'left', 10, 11),
(10, 7, 0, NULL, 'three-columns', 0, NULL, 'bottom', 5, 8),
(5, 7, 0, NULL, 'three-columns', 0, NULL, 'bottom', 5, 7),
(54, 24, 0, NULL, 'career', 0, NULL, 'top', 10, 1),
(51, 23, 0, NULL, 'hakkimizda', 0, NULL, 'bottom', 10, 1),
(60, 5, 0, NULL, 'urunler', 0, NULL, 'bottom', 10, 1),
(10, 8, 0, NULL, 'image-slider', 0, NULL, 'right', 10, 2),
(1, 8, 0, NULL, 'image-slider', 0, NULL, 'right', 10, 3),
(57, 2, 0, NULL, 'ozellikler', 0, NULL, 'bottom', 10, 1),
(2, 7, 0, NULL, 'three-columns', 1, 'newsslider', 'top', 10, 0),
(47, 1, 0, NULL, 'home', 0, NULL, 'top', 10, 2),
(6, 8, 0, NULL, 'image-slider', 0, NULL, 'top', 10, 1),
(12, 8, 0, NULL, 'image-slider', 0, NULL, 'right', 10, 4),
(37, 0, 6, 'digishop', NULL, 0, NULL, 'top', 10, 1),
(45, 1, 0, NULL, 'home', 0, NULL, 'top', 10, 1),
(62, 29, 0, NULL, 'posmaksyerel', 0, NULL, 'top', 10, 1),
(61, 27, 0, NULL, 'posmaksbulut', 0, NULL, 'top', 10, 1);

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `log`
--

CREATE TABLE `log` (
  `id` int(6) NOT NULL,
  `user_id` varchar(40) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `ip` varchar(15) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `created` datetime NOT NULL,
  `failed` tinyint(5) NOT NULL,
  `failed_last` int(11) NOT NULL,
  `type` enum('system','admin','user') NOT NULL,
  `message` text NOT NULL,
  `info_icon` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT 'default',
  `importance` enum('yes','no') NOT NULL DEFAULT 'no'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Tablo döküm verisi `log`
--

INSERT INTO `log` (`id`, `user_id`, `ip`, `created`, `failed`, `failed_last`, `type`, `message`, `info_icon`, `importance`) VALUES
(1, 'admin', '::1', '2017-05-31 09:27:21', 0, 0, 'user', 'Kullanıcı admin başarıyla giriş yapıldı.', 'user', 'no'),
(2, 'admin', '::1', '2017-06-02 11:44:57', 0, 0, 'system', 'Üyelik başarıyla güncllenmiştir!', 'content', 'no'),
(3, 'admin', '::1', '2017-06-02 15:39:28', 0, 0, 'system', 'emrah başarıyla satın alındı. ', 'payment', 'yes'),
(4, 'admin', '::1', '2017-06-02 15:40:44', 0, 0, 'system', 'emrah başarıyla satın alındı. Weekly Access', 'payment', 'yes'),
(5, 'admin', '::1', '2017-06-02 16:04:39', 0, 0, 'system', 'emrah başarıyla satın alındı. Basic 30', 'payment', 'yes'),
(6, 'admin', '::1', '2017-06-02 16:07:28', 0, 0, 'system', 'emrah başarıyla satın alındı. Weekly Access', 'payment', 'yes'),
(7, 'admin', '::1', '2017-06-02 16:09:29', 0, 0, 'system', 'emrah başarıyla satın alındı. Weekly Access', 'payment', 'yes'),
(8, 'admin', '::1', '2017-06-02 16:17:45', 0, 0, 'system', 'emrah başarıyla satın alındı. Weekly Access', 'payment', 'yes'),
(9, 'admin', '::1', '2017-06-02 16:19:48', 0, 0, 'system', 'emrah başarıyla satın alındı. Weekly Access', 'payment', 'yes'),
(10, 'admin', '::1', '2017-06-02 16:22:10', 0, 0, 'system', 'Bağlantı hatası oldu emrah', 'payment', 'yes'),
(11, 'admin', '::1', '2017-06-02 16:26:51', 0, 0, 'system', 'emrah başarıyla satın alındı. Weekly Access', 'payment', 'yes'),
(12, 'admin', '::1', '2017-06-02 16:28:22', 0, 0, 'system', 'emrah başarıyla satın alındı. Weekly Access', 'payment', 'yes'),
(13, 'admin', '::1', '2017-06-02 16:42:41', 0, 0, 'system', 'emrah başarıyla satın alındı. Weekly Access', 'payment', 'yes'),
(14, 'admin', '::1', '2017-06-02 16:57:35', 0, 0, 'system', 'emrah başarıyla satın alındı. Weekly Access', 'payment', 'yes'),
(15, 'admin', '::1', '2017-06-02 16:58:33', 0, 0, 'system', 'emrah başarıyla satın alındı. Weekly Access', 'payment', 'yes'),
(16, 'admin', '::1', '2017-06-02 17:08:19', 0, 0, 'system', 'emrah başarıyla satın alındı. Weekly Access', 'payment', 'yes'),
(17, 'admin', '::1', '2017-06-02 17:10:11', 0, 0, 'system', 'emrah başarıyla satın alındı. Weekly Access', 'payment', 'yes'),
(18, 'admin', '::1', '2017-06-02 17:18:15', 0, 0, 'system', 'emrah başarıyla satın alındı. Basic 30', 'payment', 'yes');

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `memberships`
--

CREATE TABLE `memberships` (
  `id` int(11) NOT NULL,
  `title_en` varchar(255) NOT NULL,
  `title_tr` varchar(255) NOT NULL,
  `description_en` text NOT NULL,
  `description_tr` text,
  `price` float(10,2) NOT NULL DEFAULT '0.00',
  `days` int(5) NOT NULL DEFAULT '0',
  `period` varchar(1) NOT NULL DEFAULT 'D',
  `trial` tinyint(1) NOT NULL DEFAULT '0',
  `recurring` tinyint(1) NOT NULL DEFAULT '0',
  `private` tinyint(1) NOT NULL DEFAULT '0',
  `active` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Tablo döküm verisi `memberships`
--

INSERT INTO `memberships` (`id`, `title_en`, `title_tr`, `description_en`, `description_tr`, `price`, `days`, `period`, `trial`, `recurring`, `private`, `active`) VALUES
(1, 'Trial 7', 'Trial 7', 'This is 7 days trial membership...', 'This is 7 days trial membership...', 0.00, 7, 'D', 1, 0, 0, 1),
(2, 'Basic 30', 'Basic 30', 'This is 30 days basic membership', 'This is 30 days basic membership', 2.99, 1, 'M', 0, 0, 0, 1),
(4, 'Platinum Subscription', 'Platinum Subscription', 'Platinum Monthly Subscription.', 'Platinum Monthly Subscription.', 49.99, 1, 'Y', 0, 1, 0, 1),
(5, 'Weekly Access', 'Weekly Access', 'This is 7 days basic membership', 'This is 7 days basic membership', 2.00, 1, 'W', 0, 0, 0, 1);

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `menus`
--

CREATE TABLE `menus` (
  `id` int(11) NOT NULL,
  `parent_id` int(11) NOT NULL,
  `page_id` int(11) NOT NULL,
  `page_slug` varchar(50) DEFAULT NULL,
  `mod_id` int(6) NOT NULL,
  `name_en` varchar(100) NOT NULL,
  `name_tr` varchar(100) NOT NULL,
  `slug` varchar(100) NOT NULL,
  `caption_en` varchar(100) DEFAULT NULL,
  `caption_tr` varchar(100) NOT NULL,
  `content_type` varchar(20) NOT NULL,
  `link` varchar(255) DEFAULT NULL,
  `target` enum('_self','_blank') NOT NULL DEFAULT '_blank',
  `icon` varchar(50) DEFAULT NULL,
  `cols` tinyint(1) NOT NULL DEFAULT '1',
  `position` int(11) NOT NULL DEFAULT '0',
  `home_page` tinyint(1) NOT NULL DEFAULT '0',
  `active` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Tablo döküm verisi `menus`
--

INSERT INTO `menus` (`id`, `parent_id`, `page_id`, `page_slug`, `mod_id`, `name_en`, `name_tr`, `slug`, `caption_en`, `caption_tr`, `content_type`, `link`, `target`, `icon`, `cols`, `position`, `home_page`, `active`) VALUES
(43, 0, 23, 'hakkimizda', 0, '', 'HAKKIMIZDA', 'hakkimizda', NULL, 'HAKKIMIZDA', 'page', '', '', '', 0, 5, 0, 1),
(44, 0, 3, 'iletisim', 0, '', 'İLETİŞİM', 'iletisim', NULL, 'İLETİŞİM', 'page', '', '', '', 0, 6, 0, 1),
(45, 0, 26, 'sektorelcozumler', 0, '', 'ÇÖZÜMLER', 'cozumler', NULL, 'ÇÖZÜMLER', 'page', '', '', '', 1, 3, 0, 0),
(46, 0, 0, 'news', 8, '', 'BLOG', 'blog', NULL, '', 'module', '', '', '', 0, 4, 0, 1),
(48, 0, 1, 'home', 0, '', 'ANA SAYFA', 'ana-sayfa', NULL, 'ANA SAYFA', 'page', '#ozet', '_self', '', 1, 1, 0, 1),
(50, 0, 0, 'digishop', 6, '', 'ÜRÜNLER', 'urunler', NULL, 'ÜRÜNLER', 'module', '', '', '', 1, 2, 0, 1);

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `modules`
--

CREATE TABLE `modules` (
  `id` int(11) NOT NULL,
  `title_en` varchar(120) NOT NULL,
  `title_tr` varchar(120) NOT NULL,
  `show_title` tinyint(1) NOT NULL DEFAULT '0',
  `info_en` text,
  `info_tr` text,
  `modalias` varchar(50) NOT NULL,
  `hasconfig` tinyint(1) NOT NULL DEFAULT '0',
  `system` tinyint(1) NOT NULL DEFAULT '0',
  `created` datetime NOT NULL,
  `metakey_en` varchar(200) DEFAULT NULL,
  `metakey_tr` varchar(200) NOT NULL,
  `metadesc_en` text,
  `metadesc_tr` text,
  `theme` varchar(50) DEFAULT NULL,
  `ver` varchar(4) DEFAULT '1.00',
  `active` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Tablo döküm verisi `modules`
--

INSERT INTO `modules` (`id`, `title_en`, `title_tr`, `show_title`, `info_en`, `info_tr`, `modalias`, `hasconfig`, `system`, `created`, `metakey_en`, `metakey_tr`, `metadesc_en`, `metadesc_tr`, `theme`, `ver`, `active`) VALUES
(1, 'Comments', 'Comments', 0, 'Encourage your readers to join in the discussion and leave comments and respond promptly to the comments left by your readers to make them feel valued', 'Encourage your readers to join in the discussion and leave comments and respond promptly to the comments left by your readers to make them feel valued', 'comments', 1, 0, '2014-01-10 14:10:24', NULL, '', NULL, '', NULL, '4.0', 1),
(2, 'Gallery', 'Galeriler', 0, 'Fully featured gallery module', 'Galeri Modülü', 'gallery', 1, 0, '2014-04-28 06:19:32', NULL, '', NULL, '', NULL, '4.0', 1),
(3, 'Event Manager', 'Event Manager', 0, 'Easily publish and manage your company events.', 'Easily publish and manage your company events.', 'events', 1, 0, '2014-03-22 14:18:10', NULL, '', NULL, '', NULL, '4.0', 1),
(4, 'AdBlock', 'AdBlock', 0, 'Manage Ad Campaigns', 'Manage Ad Campaigns', 'adblock', 1, 0, '2014-04-24 22:22:22', NULL, '', NULL, '', NULL, '4.0', 1),
(5, 'Google Maps', 'Google Maps', 0, 'Create multiple google maps', 'Create multiple google maps', 'gmaps', 1, 0, '2014-05-22 14:15:16', NULL, '', NULL, '', NULL, '4.0', 1),
(6, 'Digi Shop', 'Online Market', 0, 'DigiShop enables you to sell software, documents, or any other type of file right from CMS pro!', '', 'digishop', 1, 1, '2014-02-22 14:10:20', 'digital items', '', 'Variety of assorted digital items for sale', '', NULL, '4.0', 1),
(7, 'Simple Forum', 'Simple Forum', 0, 'Set up a forum/ask a question/knowledge base for your users.', 'Set up a forum/ask a question/knowledge base for your users.', 'forum', 1, 1, '2014-05-04 13:37:48', '', '', '', '', NULL, '4.0', 1),
(8, 'Blog', 'BLOG', 1, 'Universal Article/Blog/News module', 'blog', 'news', 1, 1, '2014-03-20 19:12:22', 'articles,news,blog', 'Blog', 'Latest articles and news around the world', 'blog', NULL, '4.0', 1),
(9, 'Portfolio', 'Referanslar', 0, 'Display detailed information about your work', 'Referanslar', 'portfolio', 1, 1, '2014-02-26 08:16:22', 'portfolio, sortable', 'Referanslar', 'CMS pro sortable mulit category portfolio', 'Referanslar', NULL, '4.0', 1),
(10, 'F.A.Q. Manager', 'F.A.Q. Manager', 0, 'Complete Frequently Asked Question Management Module', 'Complete Frequently Asked Question Management Module', 'faq', 1, 0, '2014-05-31 20:15:17', NULL, '', NULL, '', NULL, '4.00', 1),
(11, 'Universal Timeline', 'Universal Timeline', 0, 'Create unlimited timline pugins.', 'Create unlimited timline pugins.', 'timeline', 1, 0, '2015-01-19 23:36:36', NULL, '', NULL, '', NULL, '4.00', 1),
(12, 'Projects', 'SEKTÖREL ÇÖZÜMLER', 0, 'Display detailed information about your work', '', 'projects', 1, 1, '2014-02-26 08:16:22', 'portfolio, sortable, works', 'pos,sanalpos,satış,posmaks,', 'CMS pro sortable mulit category portfolio', '', NULL, '4.0', 1),
(13, 'Career', 'Kariyer', 0, 'Career', 'Kariyer', 'career', 1, 0, '2016-02-05 20:15:17', NULL, '', NULL, '', NULL, '4.00', 1),
(14, 'Solutions', 'Çözümler', 0, 'Display detailed information about your solutions', '', 'solutions', 1, 1, '2014-02-26 08:16:22', 'portfolio, sortable, solutions', 'portfolio, sortable, solutions', 'Veriasist sortable mulit category portfolio', 'Veriasist sortable mulit category portfolio', NULL, '4.0', 1),
(15, 'Web sites', 'Web siteleri', 0, 'Create multiple Web sites', 'Create multiple Web sites', 'websites', 1, 0, '2016-11-11 12:00:00', NULL, '', NULL, '', NULL, '4.0', 1);

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `mod_adblock`
--

CREATE TABLE `mod_adblock` (
  `id` int(11) NOT NULL,
  `title_en` varchar(255) NOT NULL,
  `title_tr` varchar(100) NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `start_date` date NOT NULL,
  `end_date` date NOT NULL,
  `total_views_allowed` int(11) NOT NULL,
  `total_clicks_allowed` int(11) NOT NULL,
  `minimum_ctr` decimal(10,2) NOT NULL,
  `banner_image` varchar(255) DEFAULT NULL,
  `banner_image_link` varchar(255) DEFAULT NULL,
  `banner_image_alt` varchar(255) DEFAULT NULL,
  `banner_html` text,
  `block_assignment` varchar(255) NOT NULL,
  `total_views` int(11) NOT NULL,
  `total_clicks` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Tablo döküm verisi `mod_adblock`
--

INSERT INTO `mod_adblock` (`id`, `title_en`, `title_tr`, `created`, `start_date`, `end_date`, `total_views_allowed`, `total_clicks_allowed`, `minimum_ctr`, `banner_image`, `banner_image_link`, `banner_image_alt`, `banner_html`, `block_assignment`, `total_views`, `total_clicks`) VALUES
(1, 'My Campaign', 'My Campaign', '2015-12-03 16:12:52', '2014-04-24', '0000-00-00', 0, 0, '0.00', 'IMG_482202-9904CA-C51FE1-EA15CF-2501CF-481509.png', 'wojoscripts.com', 'Wojo Advert', NULL, 'adblock/wojo-advert', 97, 1);

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `mod_adblock_memberlevels`
--

CREATE TABLE `mod_adblock_memberlevels` (
  `adblock_id` int(11) NOT NULL,
  `memberlevel_id` tinyint(4) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Tablo döküm verisi `mod_adblock_memberlevels`
--

INSERT INTO `mod_adblock_memberlevels` (`adblock_id`, `memberlevel_id`) VALUES
(1, 0),
(1, 1),
(1, 8),
(1, 9);

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `mod_career`
--

CREATE TABLE `mod_career` (
  `id` int(4) NOT NULL,
  `question_en` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `question_tr` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `answer_en` text COLLATE utf8_unicode_ci,
  `answer_tr` text COLLATE utf8_unicode_ci,
  `position` smallint(3) NOT NULL DEFAULT '0',
  `thumb` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='contains f.a.q. data';

--
-- Tablo döküm verisi `mod_career`
--

INSERT INTO `mod_career` (`id`, `question_en`, `question_tr`, `answer_en`, `answer_tr`, `position`, `thumb`) VALUES
(3, NULL, 'Satış ve Pazarlama Uzmanı', NULL, '&lt;div class=&quot;tab-style5&quot;&gt;\r\n                                &lt;div class=&quot;row&quot;&gt;\r\n                                    &lt;div class=&quot;col-md-12 col-sm-12 col-xs-12 xs-margin-five xs-no-margin-lr xs-no-margin-top&quot;&gt;\r\n\r\n                                        &lt;ul class=&quot;nav nav-tabs text-uppercase&quot;&gt;\r\n                                            &lt;li class=&quot;xs-width-100 active&quot;&gt;&lt;a href=&quot;#tab_sec1&quot; data-toggle=&quot;tab&quot; class=&quot;xs-no-margin&quot; aria-expanded=&quot;true&quot;&gt;Genel Nitelikler&lt;/a&gt;&lt;/li&gt;\r\n                                            &lt;li class=&quot;xs-width-100&quot;&gt;&lt;a href=&quot;#tab_sec2&quot; data-toggle=&quot;tab&quot; class=&quot;xs-no-margin&quot; aria-expanded=&quot;false&quot;&gt;İş Tanımı&lt;/a&gt;&lt;/li&gt;\r\n                                        &lt;/ul&gt;\r\n\r\n                                    &lt;/div&gt;\r\n                                &lt;/div&gt;\r\n\r\n                                &lt;div class=&quot;tab-content&quot;&gt;\r\n\r\n                                    &lt;div class=&quot;tab-pane fade active in&quot; id=&quot;tab_sec1&quot;&gt;\r\n                                        &lt;div class=&quot;row&quot;&gt;\r\n                                           &lt;div class=&quot;col-md-12 col-sm-12&quot;&gt;\r\n\r\n		&lt;li&gt;\r\n		Üniversitelerin satış pazarlama vb. bölümlerinden mezun,\r\n		&lt;/li&gt;\r\n&lt;/br&gt;\r\n		&lt;li&gt;\r\n		Minimum bir yıl bilişim teknolojileri alanında satış deneyimi olan,\r\n		&lt;/li&gt;\r\n&lt;/br&gt;\r\n&lt;li&gt;\r\nTercihen telemarketing deneyimi olan,\r\n&lt;/li&gt;\r\n&lt;/br&gt;\r\n&lt;li&gt;\r\nSatış alanında kariyer hedefleyen,&amp;nbsp;\r\n&lt;/li&gt;\r\n&lt;/br&gt;\r\n&lt;li&gt;\r\nMüşteri ve satış odaklı çalışma yeteneğine sahip,&amp;nbsp;\r\n&lt;/li&gt;\r\n&lt;/br&gt;\r\n&lt;li&gt;\r\nTakım ve proje çalışmasına yatkın, insan ilişkilerinde başarılı,&lt;/li&gt;&lt;/br&gt;\r\n&lt;li&gt;\r\n	Esnek çalışma saatlerine ve yoğun iş temposuna uyum sağlayabilecek,\r\n\r\n&lt;/li&gt;\r\n&lt;/br&gt;\r\n&lt;li&gt;\r\n\r\nHer düzeydeki insanla sosyal iletişim kurabilecek,&lt;/li&gt;&lt;/br&gt;\r\n&lt;li&gt;\r\n	Diksiyonu düzgün, iletişim becerisi yüksek,&amp;nbsp;\r\n\r\n&lt;/li&gt;\r\n&lt;/br&gt;\r\n&lt;li&gt;\r\nErkek adaylar için askerliğini tamamlamış,&lt;/li&gt;&lt;/br&gt;\r\n&lt;li&gt;\r\n	Çalışma arkadaşları arıyoruz.\r\n&lt;/li&gt;&lt;/br&gt;\r\n\r\n&lt;/div&gt;\r\n                                        &lt;/div&gt;\r\n                                    &lt;/div&gt;\r\n\r\n                                    &lt;div class=&quot;tab-pane fade&quot; id=&quot;tab_sec2&quot;&gt;\r\n                                        &lt;div class=&quot;row&quot;&gt;\r\n                                           &lt;div class=&quot;col-md-12 col-sm-12&quot;&gt;\r\n\r\n		&lt;div&gt;\r\n	Satış pazarlama personeli, şirketimiz proje ve çözümlerini; satış ve pazarlama faaliyetleri kapsamında müşterilere sunulmasıyla görevlidir.\r\n		&lt;/div&gt;&lt;/br&gt;\r\n		&lt;div&gt;\r\n	Doğrudan satış pazarlama faaliyetlerine katılmak,\r\n		&lt;/div&gt;&lt;/br&gt;\r\n		&lt;div&gt;\r\n	Telemarketing faaliyetinde aktif görev almak,\r\n		&lt;/div&gt;&lt;/br&gt;\r\n		&lt;div&gt;\r\n	Mevcut ve potansiyel müşterilere ulaşmak ve firma portföyünü genişletmek,\r\n		&lt;/div&gt;&lt;/br&gt;\r\n		&lt;div&gt;\r\n	Mevcut ve potansiyel müşterilere ziyaretler yapmak,\r\n		&lt;/div&gt;&lt;/br&gt;\r\n		&lt;div&gt;\r\n	Müşteri memnuniyetini ve verimliliğini artırıcı çalışmalar yapmak.\r\n		&lt;/div&gt;&lt;/br&gt;\r\n	&lt;/div&gt;\r\n                                        &lt;/div&gt;\r\n                                    &lt;/div&gt;\r\n', 0, 'IMG_B0A1C3-3B4DA9-C2DB4C-1DFA7D-618CBE-9D81D2.jpg');

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `mod_comments`
--

CREATE TABLE `mod_comments` (
  `id` int(11) NOT NULL,
  `parent_id` int(11) NOT NULL DEFAULT '0',
  `page_id` int(11) NOT NULL DEFAULT '0',
  `username` varchar(24) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `user_id` int(11) NOT NULL DEFAULT '0',
  `email` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `body` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `www` varchar(220) DEFAULT NULL,
  `created` datetime NOT NULL,
  `ip` varchar(16) DEFAULT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `mod_digishop`
--

CREATE TABLE `mod_digishop` (
  `id` int(11) NOT NULL,
  `cid` int(11) NOT NULL DEFAULT '0',
  `title_en` varchar(150) NOT NULL,
  `title_tr` varchar(100) NOT NULL,
  `slug` varchar(150) NOT NULL,
  `price` decimal(7,2) NOT NULL DEFAULT '0.00',
  `membership_id` varchar(20) NOT NULL DEFAULT '0',
  `token` varchar(60) NOT NULL DEFAULT '0',
  `body_en` text,
  `body_tr` text,
  `filename` varchar(50) DEFAULT NULL,
  `thumb` varchar(50) DEFAULT NULL,
  `thumbbg` varchar(50) NOT NULL,
  `filesize` varchar(20) NOT NULL DEFAULT '0',
  `gallery` smallint(4) NOT NULL DEFAULT '0',
  `likes` int(4) NOT NULL DEFAULT '0',
  `created` datetime NOT NULL,
  `modified` datetime DEFAULT NULL,
  `metakey_en` varchar(200) DEFAULT NULL,
  `metakey_tr` varchar(200) NOT NULL,
  `metadesc_en` text,
  `metadesc_tr` text,
  `active` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Tablo döküm verisi `mod_digishop`
--

INSERT INTO `mod_digishop` (`id`, `cid`, `title_en`, `title_tr`, `slug`, `price`, `membership_id`, `token`, `body_en`, `body_tr`, `filename`, `thumb`, `thumbbg`, `filesize`, `gallery`, `likes`, `created`, `modified`, `metakey_en`, `metakey_tr`, `metadesc_en`, `metadesc_tr`, `active`) VALUES
(40, 19, 'Blego ibeacon', 'PosMaks Cloud 1 Yıllık', 'posmaks-cloud-1-yillik', '2890.00', '0', '017cce5b1cd49150cc72534627f70a1e', '&lt;p&gt;&lt;/p&gt;', '&lt;p&gt;\r\n&lt;/p&gt;\r\n&lt;p&gt;\r\n	- 1 Yıla Kadar Pil ömrü\r\n&lt;/p&gt;\r\n&lt;p&gt;\r\n	- Ücretsiz Blego Api\r\n&lt;/p&gt;\r\n&lt;p&gt;\r\n	- Ücretsiz Blego Sdk\r\n&lt;/p&gt;\r\n&lt;p&gt;\r\n	- Ücretsiz Blego Bleacon Yönetim Paneli\r\n&lt;/p&gt;\r\n&lt;p&gt;\r\n	- Uzaktan Pil Durumu ve kullanım istatistikleri takip özelliği\r\n&lt;/p&gt;\r\n&lt;p&gt;\r\n&lt;/p&gt;', NULL, 'IMG_414B85-372EDE-C10102-4E628F-6732E6-83E631.jpg', 'IMG_B52654-64ECFD-7A1046-6355B3-C8FFEB-5CB571.jpg', '0', 0, 0, '2015-03-16 17:24:32', '2015-03-16 17:24:32', '', 'Blego,ibeacon,Edition​', '', 'Blego ibeacon Edition​', 1),
(41, 19, 'Blego ibeacon plus', 'PosMaks Cloud 1 Aylık', 'posmaks-cloud-1-aylik', '299.00', '0', '970102fedaf4aa3593c50c6e8845d420', '&lt;style&gt;\r\n.item-bg {\r\n	background-color:#ECECEC !important;\r\n	background-image: none !important;\r\n	border-bottom:2px dashed white;\r\n}\r\n.item-bg  .white{\r\n	color:#A0A0A0 !important\r\n}\r\n.item-bg img{\r\n	background-color:#ECECEC !important;\r\n	border-radius: 15px;\r\n    	border: 1px solid #D4D3D3;\r\n	-webkit-box-shadow: 4px 5px 10px 1px #B3B3B3;\r\n	box-shadow: 4px 5px 10px 1px #B3B3B3;\r\n}\r\n.item-bg p {\r\n	color:#696969 !important;\r\n}\r\n&lt;/style&gt;', '&lt;p style=&quot;color: #BDC3C7;&quot;&gt;\r\n	- Şarj Edilebilir\r\n&lt;/p&gt;\r\n&lt;p style=&quot;color: #BDC3C7;&quot;&gt;\r\n	- 2 Yıla Kadar Pil ömrü\r\n&lt;/p&gt;\r\n&lt;p style=&quot;color: #BDC3C7;&quot;&gt;\r\n	- Ücretsiz Blego Api\r\n&lt;/p&gt;\r\n&lt;p style=&quot;color: #BDC3C7;&quot;&gt;\r\n	- Ücretsiz Blego Sdk\r\n&lt;/p&gt;\r\n&lt;p style=&quot;color: #BDC3C7;&quot;&gt;\r\n	- Ücretsiz Blego Bleacon Yönetim Paneli\r\n&lt;/p&gt;\r\n&lt;p style=&quot;color: #BDC3C7;&quot;&gt;\r\n	- Uzaktan Pil Durumu ve kullanım istatistikleri takip özelliği\r\n&lt;/p&gt;', NULL, 'IMG_C8568E-07562D-FF6271-CACDFD-44FDF3-05AD96.jpg', 'IMG_1F3656-61786F-C10201-C164FE-24EFC7-F2B1CA.jpg', '0', 5, 0, '2015-03-16 17:29:17', '2015-03-16 17:29:17', 'item,background,color,ececec,important,border,white,shadow', 'Blego, ibeacon, plus', '.item-bg { background-color:#ECECEC !important; background-image: none !important; border-bottom:2px&#8230;', 'Blego ibeacon plus', 1),
(42, 19, '', 'POSMAKS LIFETIME', 'posmaks-lifetime', '11200.00', '0', '269fbf1ab58cb97bc75db0b56c7ca6e4', NULL, '&lt;img src=&quot;uploads/212Qozellik.png&quot; alt=&quot;212Qozellik&quot;&gt;&lt;p&gt;&lt;/p&gt;&lt;p&gt;KDV DAHİL&lt;/p&gt;', NULL, 'IMG_323178-B7658B-104BC5-6B8D76-C123B3-DAA0C9.png', 'IMG_B07BBB-30B518-A17E88-E74223-8EC9ED-88F400.jpg', '0', 0, 0, '2017-05-04 23:25:51', NULL, NULL, 'POSMAKS LIFETIME', NULL, 'POSMAKS LIFETIME', 1);

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `mod_digishop_cart`
--

CREATE TABLE `mod_digishop_cart` (
  `user_id` varchar(50) DEFAULT NULL,
  `pid` int(11) DEFAULT NULL,
  `price` float(7,2) DEFAULT NULL,
  `uid` int(11) NOT NULL DEFAULT '0',
  `adr1` int(11) NOT NULL DEFAULT '0',
  `adr2` int(11) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Tablo döküm verisi `mod_digishop_cart`
--

INSERT INTO `mod_digishop_cart` (`user_id`, `pid`, `price`, `uid`, `adr1`, `adr2`) VALUES
('fd714d52d6ef977a9b5873422ec604e242d2fab6', 41, 30.00, 0, 0, 0),
('e07e06959ecee774a7cb1ecd885005d9d1c87a2c', 41, 30.00, 0, 0, 0),
('e07e06959ecee774a7cb1ecd885005d9d1c87a2c', 41, 30.00, 0, 0, 0),
('e07e06959ecee774a7cb1ecd885005d9d1c87a2c', 41, 30.00, 0, 0, 0),
('e07e06959ecee774a7cb1ecd885005d9d1c87a2c', 41, 30.00, 0, 0, 0),
('e07e06959ecee774a7cb1ecd885005d9d1c87a2c', 41, 30.00, 0, 0, 0),
('e07e06959ecee774a7cb1ecd885005d9d1c87a2c', 41, 30.00, 0, 0, 0),
('5723ea5d5e3cc0ea66f48ba619f244d76fe6953c', 41, 30.00, 0, 4, 0),
('edd90f4878456d4fac96fdc433b825b00ecef141', 41, 30.00, 1, 8, 0),
('9e3e069576128b78f9a49d6e219edd21512b76ca', 41, 30.00, 1, 16, 0),
('9e3e069576128b78f9a49d6e219edd21512b76ca', 41, 30.00, 1, 16, 0),
('5723ea5d5e3cc0ea66f48ba619f244d76fe6953c', 41, 30.00, 0, 4, 0),
('5723ea5d5e3cc0ea66f48ba619f244d76fe6953c', 41, 30.00, 0, 4, 0),
('5723ea5d5e3cc0ea66f48ba619f244d76fe6953c', 41, 30.00, 0, 4, 0),
('5723ea5d5e3cc0ea66f48ba619f244d76fe6953c', 41, 30.00, 0, 4, 0),
('5723ea5d5e3cc0ea66f48ba619f244d76fe6953c', 41, 30.00, 0, 4, 0),
('5723ea5d5e3cc0ea66f48ba619f244d76fe6953c', 41, 30.00, 0, 4, 0),
('5723ea5d5e3cc0ea66f48ba619f244d76fe6953c', 41, 30.00, 0, 4, 0),
('5723ea5d5e3cc0ea66f48ba619f244d76fe6953c', 41, 30.00, 0, 4, 0),
('5723ea5d5e3cc0ea66f48ba619f244d76fe6953c', 41, 30.00, 0, 4, 0),
('5723ea5d5e3cc0ea66f48ba619f244d76fe6953c', 40, 30.00, 0, 4, 0),
('5723ea5d5e3cc0ea66f48ba619f244d76fe6953c', 41, 30.00, 0, 4, 0),
('a80b9accbaad18012cd98e7c3ca4ba405781fc37', 41, 30.00, 0, 0, 0),
('6ed0bc454c3cd395c2228e905e5a24e1fc57808b', 41, 30.00, 0, 0, 0),
('a80b9accbaad18012cd98e7c3ca4ba405781fc37', 41, 30.00, 0, 0, 0),
('699c5cb3fd24c95aa836e51d6f8150e39764cd41', 41, 30.00, 0, 0, 0),
('699c5cb3fd24c95aa836e51d6f8150e39764cd41', 41, 30.00, 0, 0, 0),
('699c5cb3fd24c95aa836e51d6f8150e39764cd41', 41, 30.00, 0, 0, 0),
('699c5cb3fd24c95aa836e51d6f8150e39764cd41', 41, 30.00, 0, 0, 0),
('699c5cb3fd24c95aa836e51d6f8150e39764cd41', 41, 30.00, 0, 0, 0),
('699c5cb3fd24c95aa836e51d6f8150e39764cd41', 41, 30.00, 0, 0, 0),
('699c5cb3fd24c95aa836e51d6f8150e39764cd41', 41, 30.00, 0, 0, 0),
('699c5cb3fd24c95aa836e51d6f8150e39764cd41', 41, 30.00, 0, 0, 0),
('699c5cb3fd24c95aa836e51d6f8150e39764cd41', 41, 30.00, 0, 0, 0),
('699c5cb3fd24c95aa836e51d6f8150e39764cd41', 41, 30.00, 0, 0, 0),
('bab3be90e0a1a1e01f98080eeb263882cb9c6139', 41, 20.00, 0, 0, 0),
('74850e7c8475fc67db90965f46f0d347906dbc67', 41, 20.00, 1, 0, 0),
('b17a7a619c92907dbe5b5ec0053c6ca5b0ab7ac0', 41, 20.00, 0, 1, 0),
('b17a7a619c92907dbe5b5ec0053c6ca5b0ab7ac0', 41, 20.00, 0, 1, 0),
('b17a7a619c92907dbe5b5ec0053c6ca5b0ab7ac0', 41, 20.00, 0, 1, 0),
('b17a7a619c92907dbe5b5ec0053c6ca5b0ab7ac0', 41, 20.00, 0, 1, 0),
('b17a7a619c92907dbe5b5ec0053c6ca5b0ab7ac0', 41, 20.00, 0, 1, 0),
('b17a7a619c92907dbe5b5ec0053c6ca5b0ab7ac0', 41, 20.00, 0, 1, 0),
('b17a7a619c92907dbe5b5ec0053c6ca5b0ab7ac0', 41, 20.00, 0, 1, 0),
('b17a7a619c92907dbe5b5ec0053c6ca5b0ab7ac0', 41, 20.00, 0, 1, 0),
('b17a7a619c92907dbe5b5ec0053c6ca5b0ab7ac0', 41, 20.00, 0, 1, 0),
('b17a7a619c92907dbe5b5ec0053c6ca5b0ab7ac0', 41, 20.00, 0, 1, 0),
('b17a7a619c92907dbe5b5ec0053c6ca5b0ab7ac0', 41, 20.00, 0, 1, 0),
('6ee8be213ff21474d9798672162d15a47c48423c', 41, 20.00, 1, 0, 0),
('4644f5726f94caa8e10e690f3a2a71d1ef1bdf30', 41, 20.00, 1, 0, 0),
('74850e7c8475fc67db90965f46f0d347906dbc67', 41, 20.00, 1, 0, 0),
('74850e7c8475fc67db90965f46f0d347906dbc67', 41, 20.00, 1, 0, 0),
('74850e7c8475fc67db90965f46f0d347906dbc67', 41, 20.00, 1, 0, 0),
('74850e7c8475fc67db90965f46f0d347906dbc67', 41, 20.00, 1, 0, 0),
('74850e7c8475fc67db90965f46f0d347906dbc67', 41, 20.00, 1, 0, 0),
('74850e7c8475fc67db90965f46f0d347906dbc67', 41, 20.00, 1, 0, 0),
('74850e7c8475fc67db90965f46f0d347906dbc67', 41, 20.00, 1, 0, 0),
('74850e7c8475fc67db90965f46f0d347906dbc67', 41, 20.00, 1, 0, 0),
('74850e7c8475fc67db90965f46f0d347906dbc67', 41, 20.00, 1, 0, 0),
('74850e7c8475fc67db90965f46f0d347906dbc67', 41, 20.00, 1, 0, 0),
('4644f5726f94caa8e10e690f3a2a71d1ef1bdf30', 41, 20.00, 1, 0, 0),
('2a3b9f272cbe43e358ae6ea4df583ed2afd0c8ec', 41, 20.00, 1, 0, 0),
('be92f54249381edf142bff1a3cbec65374ec9977', 40, 30.00, 0, 0, 0),
('be92f54249381edf142bff1a3cbec65374ec9977', 40, 30.00, 0, 0, 0),
('be92f54249381edf142bff1a3cbec65374ec9977', 40, 30.00, 0, 0, 0),
('be92f54249381edf142bff1a3cbec65374ec9977', 40, 30.00, 0, 0, 0),
('be92f54249381edf142bff1a3cbec65374ec9977', 40, 30.00, 0, 0, 0),
('be92f54249381edf142bff1a3cbec65374ec9977', 40, 30.00, 0, 0, 0),
('31abb6ab9edc176df73a60600db65e837f048e9b', 41, 20.00, 1, 1, 0),
('ecd2a19aa46abb7850b0e522bb78e1a3859cf279', 41, 20.00, 0, 16, 0),
('53b75d8cafbe6c1efc9a8c3a2d346b3aaf346cf9', 41, 20.00, 1, 0, 0),
('53b75d8cafbe6c1efc9a8c3a2d346b3aaf346cf9', 41, 20.00, 1, 0, 0),
('53b75d8cafbe6c1efc9a8c3a2d346b3aaf346cf9', 41, 20.00, 1, 0, 0),
('53b75d8cafbe6c1efc9a8c3a2d346b3aaf346cf9', 41, 20.00, 1, 0, 0),
('53b75d8cafbe6c1efc9a8c3a2d346b3aaf346cf9', 41, 20.00, 1, 0, 0),
('53b75d8cafbe6c1efc9a8c3a2d346b3aaf346cf9', 41, 20.00, 1, 0, 0),
('53b75d8cafbe6c1efc9a8c3a2d346b3aaf346cf9', 41, 20.00, 1, 0, 0),
('53b75d8cafbe6c1efc9a8c3a2d346b3aaf346cf9', 41, 20.00, 1, 0, 0),
('53b75d8cafbe6c1efc9a8c3a2d346b3aaf346cf9', 41, 20.00, 1, 0, 0),
('53b75d8cafbe6c1efc9a8c3a2d346b3aaf346cf9', 41, 20.00, 1, 0, 0),
('53b75d8cafbe6c1efc9a8c3a2d346b3aaf346cf9', 41, 20.00, 1, 0, 0),
('53b75d8cafbe6c1efc9a8c3a2d346b3aaf346cf9', 41, 20.00, 1, 0, 0),
('53b75d8cafbe6c1efc9a8c3a2d346b3aaf346cf9', 40, 30.00, 1, 0, 0),
('53b75d8cafbe6c1efc9a8c3a2d346b3aaf346cf9', 40, 30.00, 1, 0, 0),
('53b75d8cafbe6c1efc9a8c3a2d346b3aaf346cf9', 40, 30.00, 1, 0, 0),
('53b75d8cafbe6c1efc9a8c3a2d346b3aaf346cf9', 40, 30.00, 1, 0, 0),
('3759e7072f03d7245e67e21813d5f9a9c277ce81', 40, 30.00, 0, 0, 0),
('c35ef25c70d7918abc24f6768e6274db150338ca', 41, 20.00, 0, 0, 0),
('32c96d4d716415ace334c555cc8312f9b8fdd917', 40, 30.00, 0, 0, 0),
('1d33b58990f1330f8d60026936fc6d555187e2b7', 41, 20.00, 0, 0, 0),
('469d9a9775e5d79faca4cced828bb944866e4d13', 40, 30.00, 0, 0, 0),
('cb7b473fc7d645e215be35a255392f128d85fa6d', 40, 30.00, 0, 0, 0),
('cb7b473fc7d645e215be35a255392f128d85fa6d', 41, 20.00, 0, 0, 0),
('390be70e15a7d8f20114607f1224ff59243afeb5', 42, 11200.00, 1, 0, 0),
('390be70e15a7d8f20114607f1224ff59243afeb5', 42, 11200.00, 1, 0, 0),
('390be70e15a7d8f20114607f1224ff59243afeb5', 42, 11200.00, 1, 0, 0),
('390be70e15a7d8f20114607f1224ff59243afeb5', 42, 11200.00, 1, 0, 0),
('e585fdc260a8595170e5da7f23ff62f9c413372a', 42, 11200.00, 0, 0, 0),
('e585fdc260a8595170e5da7f23ff62f9c413372a', 42, 11200.00, 0, 0, 0),
('f430d642d7776aaf02fe2e8412091668aeea9bde', 42, 11200.00, 0, 0, 0),
('0cafe1c0f0e419f92f48684e05a1c82519fbcd5a', 42, 11200.00, 0, 0, 0),
('1e0ba85c154a9e5752cdcfca4ee3dee5f48c13ad', 42, 11200.00, 1, 0, 0),
('aff41cd8b71a73c60e00cc18b600625fe80267ab', 42, 11200.00, 0, 0, 0),
('2c796fc81d348535b5da2de6d8653ada4764aa2e', 42, 11200.00, 1, 0, 0),
('2c796fc81d348535b5da2de6d8653ada4764aa2e', 42, 11200.00, 1, 0, 0),
('d8fb6006f12373dc6c06277b3190ffb6b23a17d2', 42, 11200.00, 1, 0, 0),
('1e9b52563b69f3aee22c158614de57278df7169f', 41, 299.00, 0, 0, 0),
('458cd1891f6ceddf2a5dba5531d14409548d4ee2', 42, 11200.00, 0, 0, 0),
('942f5d4c00ccd97ee40171bced8cf90288202208', 42, 11200.00, 0, 0, 0),
('942f5d4c00ccd97ee40171bced8cf90288202208', 42, 11200.00, 0, 0, 0),
('942f5d4c00ccd97ee40171bced8cf90288202208', 42, 11200.00, 0, 0, 0),
('1e0ba85c154a9e5752cdcfca4ee3dee5f48c13ad', 42, 11200.00, 0, 0, 0),
('334dead022034b33ead9594490a93da569662ab6', 42, 11200.00, 0, 0, 0),
('ab71eae65492d9937fcd244bfe3f3fe3c721da1e', 41, 299.00, 0, 0, 0),
('e81c8c9a10a595b2823144f24b3ef65282d3ff91', 42, 11200.00, 1, 0, 0),
('e81c8c9a10a595b2823144f24b3ef65282d3ff91', 41, 299.00, 1, 0, 0),
('fbfa288da5d8c2479395c777611d666d11db9db8', 42, 11200.00, 0, 0, 0),
('bf55134bfa8ceaffef4cae63f565e3fa085656e5', 42, 11200.00, 1, 0, 0),
('bf55134bfa8ceaffef4cae63f565e3fa085656e5', 41, 299.00, 1, 0, 0),
('bf55134bfa8ceaffef4cae63f565e3fa085656e5', 42, 11200.00, 1, 0, 0),
('bf55134bfa8ceaffef4cae63f565e3fa085656e5', 42, 11200.00, 1, 0, 0),
('bf55134bfa8ceaffef4cae63f565e3fa085656e5', 42, 11200.00, 1, 0, 0),
('bf55134bfa8ceaffef4cae63f565e3fa085656e5', 42, 11200.00, 1, 0, 0);

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `mod_digishop_categories`
--

CREATE TABLE `mod_digishop_categories` (
  `id` int(5) NOT NULL,
  `name_en` varchar(100) NOT NULL,
  `name_tr` varchar(100) NOT NULL,
  `slug` varchar(100) DEFAULT NULL,
  `metakey_en` varchar(200) DEFAULT NULL,
  `metakey_tr` varchar(200) NOT NULL,
  `metadesc_en` tinytext,
  `metadesc_tr` text,
  `sorting` tinyint(2) NOT NULL DEFAULT '0',
  `active` int(1) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Tablo döküm verisi `mod_digishop_categories`
--

INSERT INTO `mod_digishop_categories` (`id`, `name_en`, `name_tr`, `slug`, `metakey_en`, `metakey_tr`, `metadesc_en`, `metadesc_tr`, `sorting`, `active`) VALUES
(19, 'Blego ibeacon', 'POSMAKS YAZILIM ÜRÜNLERİ', 'posmaks-yazilim', 'Blego ibeacon', 'POSMAKS YAZILIM ÜRÜNLERİ', 'Blego ibeacon', 'POSMAKS YAZILIM ÜRÜNLERİ', 1, 1),
(20, '', 'POS TERMİNALLERİ', 'pos-terminalleri', NULL, 'POS TERMİNALLERİ', NULL, 'POS TERMİNALLERİ', 2, 1),
(22, '', 'KAMPANYALAR', 'kampanyalar', NULL, 'KAMPANYALAR', NULL, 'KAMPANYALAR', 4, 1),
(21, '', 'EK DONANIMLAR', 'ek-donanimlar', NULL, 'EK DONANIMLAR', NULL, 'EK DONANIMLAR', 3, 1);

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `mod_digishop_transactions`
--

CREATE TABLE `mod_digishop_transactions` (
  `id` int(11) NOT NULL,
  `transaction_id` int(11) NOT NULL,
  `txn_id` varchar(50) NOT NULL,
  `pid` int(11) DEFAULT NULL,
  `uid` int(11) NOT NULL DEFAULT '0',
  `file_date` int(11) DEFAULT NULL,
  `downloads` int(11) NOT NULL DEFAULT '0',
  `token` varchar(50) NOT NULL DEFAULT '0',
  `ip` varchar(20) DEFAULT '0',
  `created` datetime NOT NULL,
  `payer_email` varchar(75) DEFAULT NULL,
  `payer_status` varchar(50) DEFAULT NULL,
  `item_qty` int(11) NOT NULL DEFAULT '1',
  `qty_complate` int(11) NOT NULL DEFAULT '0',
  `price` decimal(9,2) NOT NULL DEFAULT '0.00',
  `unit_price` decimal(9,2) NOT NULL DEFAULT '0.00',
  `mc_fee` decimal(9,2) NOT NULL DEFAULT '0.00',
  `currency` char(3) DEFAULT NULL,
  `pp` varchar(40) DEFAULT NULL,
  `memo` text,
  `address1` int(11) NOT NULL COMMENT 'kargo adresi',
  `address2` int(11) NOT NULL COMMENT 'fatura adresi',
  `status` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0:onay bekliyor,1:onaylandı,2:işlem yapıldı',
  `active` tinyint(1) DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Tablo döküm verisi `mod_digishop_transactions`
--

INSERT INTO `mod_digishop_transactions` (`id`, `transaction_id`, `txn_id`, `pid`, `uid`, `file_date`, `downloads`, `token`, `ip`, `created`, `payer_email`, `payer_status`, `item_qty`, `qty_complate`, `price`, `unit_price`, `mc_fee`, `currency`, `pp`, `memo`, `address1`, `address2`, `status`, `active`) VALUES
(1, 1, '0G853856E41160543', 40, 1, 1439210553, 0, '4b5d621d0e5b6deb4258fff2b6358425fea50463', '173.0.82.126', '2015-08-10 15:42:33', 'orucemrahis-buyer@gmail.com', 'verified', 1, 0, '30.00', '30.00', '0.00', 'USD', 'PayPal', NULL, 1, 1, 1, 1),
(2, 2, '8EC53728LA749061S', 40, 1, 1439650561, 0, '02d3d4e7bc137c5b7543c60521d6f5155c53e04a', '173.0.82.126', '2015-08-15 17:56:01', 'orucemrahis-buyer@gmail.com', 'verified', 5, 0, '150.00', '30.00', '0.00', 'USD', 'PayPal', NULL, 8, 8, 1, 1),
(3, 3, '9PP175078F0776527', 41, 1, 1439800239, 0, 'b1bfc0b0b22bdcc3aea066f145172887ab904328', '173.0.82.126', '2015-08-17 11:30:39', 'orucemrahis-buyer@gmail.com', 'verified', 1, 0, '30.00', '30.00', '0.00', 'USD', 'PayPal', NULL, 1, 1, 1, 1),
(4, 4, 'MAN_1440427119', 41, 1, 1440427119, 0, '2951ccf1a735af94b4a619d0acf491c3f6dc3f09', '85.105.193.172', '2015-08-24 17:38:39', 'eoruc@kodar.com.tr1', 'verified', 1, 0, '30.00', '30.00', '0.00', 'USD', 'Offline', NULL, 1, 1, 0, 0),
(5, 5, 'MAN_1440488076', 41, 1, 1440488076, 0, '45821af8862487908af148d07bf2d695f093311b', '85.105.193.172', '2015-08-25 10:34:36', 'eoruc@kodar.com.tr1', 'verified', 2, 0, '60.00', '30.00', '0.00', 'USD', 'Offline', NULL, 7, 7, 1, 1),
(6, 6, 'MAN_1455103472', 41, 1, 1455103472, 0, '05de056b5883fc4e09c8ca8300719c3b1ef4a2b6', '85.105.193.172', '2016-02-10 13:24:32', 'eoruc@kodar.com.tr1', 'verified', 1, 0, '20.00', '20.00', '0.00', 'USD', 'Offline', NULL, 1, 8, 0, 0),
(7, 7, 'MAN_1455752174', 41, 22223, 1455752174, 0, 'e68da788662c32254eb4879bc68151c7abb891f1', '85.105.193.172', '2016-02-18 01:36:14', 'teraphi@gmail.com', 'verified', 1, 0, '20.00', '20.00', '0.00', 'USD', 'Offline', NULL, 18, 18, 0, 0),
(8, 8, 'MAN_1455809909', 41, 1, 1455809909, 0, '1e80bfd9cabb15077467c64357dae02da5fb0f1e', '85.105.193.172', '2016-02-18 17:38:29', 'eoruc@kodar.com.tr', 'verified', 1, 0, '20.00', '20.00', '0.00', 'USD', 'Offline', NULL, 1, 1, 0, 0),
(9, 8, 'MAN_1455809909', 40, 1, 1455809909, 0, 'c97d43514f915800d1a614a61d4337a5d1dab2cd', '85.105.193.172', '2016-02-18 17:38:29', 'eoruc@kodar.com.tr', 'verified', 1, 0, '30.00', '30.00', '0.00', 'USD', 'Offline', NULL, 1, 1, 0, 0),
(10, 9, 'MAN_1455810043', 41, 1, 1455810043, 0, '4c4ffa353f94ad69cb67178e92e9297493718866', '85.105.193.172', '2016-02-18 17:40:43', 'eoruc@kodar.com.tr', 'verified', 1, 0, '20.00', '20.00', '0.00', 'USD', 'Offline', NULL, 16, 16, 0, 0),
(11, 10, 'MAN_1455810493', 41, 1, 1455810493, 0, '2be693b65316ea4068a62d24479c62ae31edff6b', '85.105.193.172', '2016-02-18 17:48:13', 'eoruc@kodar.com.tr', 'verified', 3, 0, '60.00', '20.00', '0.00', 'USD', 'Offline', NULL, 4, 4, 0, 0),
(12, 10, 'MAN_1455810493', 40, 1, 1455810493, 0, 'aec0a0ee03655eb012513ed9ffa49ae0e2871972', '85.105.193.172', '2016-02-18 17:48:13', 'eoruc@kodar.com.tr', 'verified', 3, 0, '90.00', '30.00', '0.00', 'USD', 'Offline', NULL, 4, 4, 0, 0),
(13, 11, 'MAN_1455810623', 41, 1, 1455810623, 0, 'c57d111e21ff6354c0d028e2672ed7b834cc3ac8', '85.105.193.172', '2016-02-18 17:50:23', 'eoruc@kodar.com.tr', 'verified', 1, 0, '20.00', '20.00', '0.00', 'USD', 'Offline', NULL, 4, 4, 0, 0),
(14, 12, 'BLG_12_1455812248', 41, 1, 1455812248, 0, '92879e8fd47d46c8b9fae1dc54a64c323b4a9791', '85.105.193.172', '2016-02-18 18:17:28', 'eoruc@kodar.com.tr', 'verified', 1, 0, '20.00', '20.00', '0.00', 'USD', 'Offline', NULL, 1, 1, 0, 0),
(15, 12, 'BLG_12_1455812248', 40, 1, 1455812248, 0, '3858ec7bb81d38eead687975ea69e368324779ea', '85.105.193.172', '2016-02-18 18:17:28', 'eoruc@kodar.com.tr', 'verified', 1, 0, '30.00', '30.00', '0.00', 'USD', 'Offline', NULL, 1, 1, 0, 0),
(16, 13, 'BLG_13_1455812755', 40, 1, 1455812755, 0, 'cd3a6c361ec72f1fbca15d7c2a3594f914a39843', '85.105.193.172', '2016-02-18 18:25:55', 'eoruc@kodar.com.tr', 'verified', 6, 0, '180.00', '30.00', '0.00', 'USD', 'Offline', NULL, 1, 1, 0, 0),
(17, 14, 'BLG_14_1469002380', 41, 22225, 1469002380, 0, '33ea27cc8318e6592947d95f1926169a845267c2', '213.143.224.130', '2016-07-20 11:13:00', 'edaistanbul@yahoo.com', 'verified', 1, 0, '20.00', '20.00', '0.00', 'USD', 'Offline', NULL, 19, 19, 0, 0),
(18, 15, 'BLG_15_1491493342', 41, 1, 1491493342, 0, 'f70eafa94aaba668debfb0baaf14e57b3616872c', '85.105.193.172', '2017-04-06 18:42:22', 'eoruc@kodar.com.tr', 'verified', 2, 0, '40.00', '20.00', '0.00', 'USD', 'Offline', NULL, 1, 1, 0, 0),
(19, 16, 'BLG_16_1491493486', 41, 1, 1491493486, 0, '5769d84e4c6a7265001d1a73d8b0883db054f4e2', '85.105.193.172', '2017-04-06 18:44:46', 'eoruc@kodar.com.tr', 'verified', 3, 0, '60.00', '20.00', '0.00', 'USD', 'Offline', NULL, 4, 4, 0, 0),
(20, 17, 'BLG_17_1491493585', 41, 1, 1491493585, 0, 'cfbc606cbe24c3b232faf5cf7e16194483550954', '85.105.193.172', '2017-04-06 18:46:25', 'eoruc@kodar.com.tr', 'verified', 3, 0, '60.00', '20.00', '0.00', 'USD', 'Offline', NULL, 20, 20, 1, 1),
(21, 18, 'BLG_18_1491493614', 41, 1, 1491493614, 0, '7e8ab0afbfa50c6d703e6b26f4503f10500261ec', '85.105.193.172', '2017-04-06 18:46:54', 'eoruc@kodar.com.tr', 'verified', 1, 0, '20.00', '20.00', '0.00', 'USD', 'Offline', NULL, 20, 20, 1, 1);

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `mod_digishop_transaction_addresses`
--

CREATE TABLE `mod_digishop_transaction_addresses` (
  `id` int(11) NOT NULL,
  `transaction_id` int(11) NOT NULL,
  `address1` text NOT NULL,
  `address2` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Tablo döküm verisi `mod_digishop_transaction_addresses`
--

INSERT INTO `mod_digishop_transaction_addresses` (`id`, `transaction_id`, `address1`, `address2`) VALUES
(1, 1, '', ''),
(2, 2, '', ''),
(3, 3, '', ''),
(4, 4, '', ''),
(5, 5, '', ''),
(6, 6, '', ''),
(7, 7, 'DENEME 84555 İSTANBUL Adana Turkey', 'DENEME 84555 İSTANBUL Adana Turkey'),
(8, 8, '', ''),
(9, 9, 'ddasdasdasd sdasdads asdasdasda Adrar Algeria', 'ddasdasdasd sdasdads asdasdasda Adrar Algeria'),
(10, 10, '', ''),
(11, 11, '', ''),
(12, 12, '', ''),
(13, 13, '', ''),
(14, 14, 'İş Bankası Kuleleri, Kule 3 kat:18 34330 4. Levent- İstanbul 34330 istanbul İstanbul Turkey', 'İş Bankası Kuleleri, Kule 3 kat:18 34330 4. Levent- İstanbul 34330 istanbul İstanbul Turkey'),
(15, 15, '', ''),
(16, 16, '', ''),
(17, 17, 'İÇERENKÖY MAH. ERTAŞ SOK. BARAN İŞ MERKEZİ NO:10 KAT:6 34844 İSTANBUL İstanbul Turkey', 'İÇERENKÖY MAH. ERTAŞ SOK. BARAN İŞ MERKEZİ NO:10 KAT:6 34844 İSTANBUL İstanbul Turkey'),
(18, 18, 'İÇERENKÖY MAH. ERTAŞ SOK. BARAN İŞ MERKEZİ NO:10 KAT:6 34844 İSTANBUL İstanbul Turkey', 'İÇERENKÖY MAH. ERTAŞ SOK. BARAN İŞ MERKEZİ NO:10 KAT:6 34844 İSTANBUL İstanbul Turkey');

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `mod_digishop_transaction_statuses`
--

CREATE TABLE `mod_digishop_transaction_statuses` (
  `id` int(11) NOT NULL,
  `transaction_id` int(11) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0-bekliyor, 1-kabuledildi, 2-kargolandı, 3-ulaştı',
  `description` text,
  `created` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Tablo döküm verisi `mod_digishop_transaction_statuses`
--

INSERT INTO `mod_digishop_transaction_statuses` (`id`, `transaction_id`, `status`, `description`, `created`) VALUES
(1, 1, 0, NULL, '2015-08-10 15:42:34'),
(2, 1, 1, NULL, '2015-08-10 15:42:34'),
(3, 2, 0, NULL, '2015-08-15 17:56:01'),
(4, 2, 1, NULL, '2015-08-15 17:56:01'),
(5, 3, 0, NULL, '2015-08-17 11:30:39'),
(6, 3, 1, NULL, '2015-08-17 11:30:39'),
(7, 4, 0, NULL, '2015-08-24 17:38:39'),
(8, 5, 0, NULL, '2015-08-25 10:34:36'),
(9, 6, 0, NULL, '2016-02-10 13:24:32'),
(10, 7, 0, NULL, '2016-02-18 01:36:14'),
(11, 8, 0, NULL, '2016-02-18 17:38:29'),
(12, 9, 0, NULL, '2016-02-18 17:40:43'),
(13, 10, 0, NULL, '2016-02-18 17:48:13'),
(14, 11, 0, NULL, '2016-02-18 17:50:23'),
(15, 12, 0, NULL, '2016-02-18 18:17:28'),
(16, 13, 0, NULL, '2016-02-18 18:25:55'),
(17, 14, 0, NULL, '2016-07-20 11:13:00'),
(18, 15, 0, NULL, '2017-04-06 18:42:22'),
(19, 16, 0, NULL, '2017-04-06 18:44:46'),
(20, 17, 0, NULL, '2017-04-06 18:46:25'),
(21, 18, 0, NULL, '2017-04-06 18:46:54');

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `mod_events`
--

CREATE TABLE `mod_events` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL DEFAULT '1',
  `title_en` varchar(150) NOT NULL,
  `title_tr` varchar(150) NOT NULL,
  `venue_en` varchar(150) DEFAULT NULL,
  `venue_tr` varchar(150) NOT NULL,
  `date_start` date NOT NULL,
  `date_end` date DEFAULT NULL,
  `time_start` time DEFAULT NULL,
  `time_end` time DEFAULT NULL,
  `body_en` text,
  `body_tr` text,
  `contact_person` varchar(100) DEFAULT NULL,
  `contact_email` varchar(80) DEFAULT NULL,
  `contact_phone` varchar(16) DEFAULT NULL,
  `color` varchar(20) DEFAULT NULL,
  `active` tinyint(1) DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Tablo döküm verisi `mod_events`
--

INSERT INTO `mod_events` (`id`, `user_id`, `title_en`, `title_tr`, `venue_en`, `venue_tr`, `date_start`, `date_end`, `time_start`, `time_end`, `body_en`, `body_tr`, `contact_person`, `contact_email`, `contact_phone`, `color`, `active`) VALUES
(1, 1, 'Free Coffee for Each Monday', 'Free Coffee for Each Monday', 'Office Rental Showroom', 'Office Rental Showroom', '2014-05-14', '2014-12-31', '00:00:00', '00:00:00', 'Vestibulum dictum elit eu risus porta egestas. Sed quis enim neque, sed  fringilla erat. Nunc feugiat tortor eu sem consequat aliquam. Cras non  nibh at lorem auctor interdum. Donec ut lacinia massa.', 'Vestibulum dictum elit eu risus porta egestas. Sed quis enim neque, sed  fringilla erat. Nunc feugiat tortor eu sem consequat aliquam. Cras non  nibh at lorem auctor interdum. Donec ut lacinia massa.', 'John Doe', 'john@gmail.com', '555-555-5555', '#b2d280', 1),
(2, 1, 'Lucky Draw', 'Lucky Draw', 'Office Rental Showroom', 'Office Rental Showroom', '2014-05-21', '2014-09-20', '13:30:00', '19:30:00', '\n&lt;p&gt;&lt;img src=&quot;uploads/images/pages/thumbs/thumb_demo_1.jpg&quot; alt=&quot;thumb_demo_1.jpg&quot; class=&quot;img-left&quot; /&gt;Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Nulla posuere nibh auctor urna tincidunt fringilla. &lt;br /&gt;\n	Donec imperdiet, orci quis aliquet laoreet, magna purus semper ligula, sit amet aliquam sapien enim in orci. Pellentesque at iaculis nibh.&lt;/p&gt; ', '\n&lt;p&gt;&lt;img src=&quot;uploads/images/pages/thumbs/thumb_demo_1.jpg&quot; alt=&quot;thumb_demo_1.jpg&quot; class=&quot;img-left&quot; /&gt;Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Nulla posuere nibh auctor urna tincidunt fringilla. &lt;br /&gt;\n	Donec imperdiet, orci quis aliquet laoreet, magna purus semper ligula, sit amet aliquam sapien enim in orci. Pellentesque at iaculis nibh.&lt;/p&gt; ', 'John Doe', 'john@gmail.com', '555-555-5555', '#FC9', 1),
(3, 1, 'E-Commerce Seminar', 'E-Commerce Seminar', 'Office Rental Showroom', 'Office Rental Showroom', '2011-12-19', '2011-12-26', '09:30:00', '13:30:00', 'Proin nec nisl est, id ornare lacus. Etiam mauris neque, scelerisque ut  ultrices vel, blandit et nisi. Nam commodo fermentum lectus vulputate  auctor. Maecenas hendrerit sapien sit amet erat mollis venenatis nec sit', 'Proin nec nisl est, id ornare lacus. Etiam mauris neque, scelerisque ut  ultrices vel, blandit et nisi. Nam commodo fermentum lectus vulputate  auctor. Maecenas hendrerit sapien sit amet erat mollis venenatis nec sit', 'John Doe', 'john@gmail.com', '555-555-5555', NULL, 1),
(4, 1, 'E-Commerce Seminar II', 'E-Commerce Seminar II', 'Office Rental Showroom', 'Office Rental Showroom', '2011-12-19', '2011-12-22', '17:00:00', '19:00:00', 'Aliquam auctor molestie ipsum ultricies tincidunt. Suspendisse potenti.  Nulla volutpat urna et mi consectetur placerat iaculis lacus lacinia.  Integer a nisi id diam tempus commodo eget a tellus. In consequat augue  nec tortor bibendum vel semper metus sodales. Donec ut dui nisi, id  posuere augue.', 'Aliquam auctor molestie ipsum ultricies tincidunt. Suspendisse potenti.  Nulla volutpat urna et mi consectetur placerat iaculis lacus lacinia.  Integer a nisi id diam tempus commodo eget a tellus. In consequat augue  nec tortor bibendum vel semper metus sodales. Donec ut dui nisi, id  posuere augue.', 'John Doe', 'john@gmail.com', '555-555-5555', NULL, 1),
(5, 1, 'New Year', 'New Year', 'New Year&#039;s Day in Canada', 'New Year&#039;s Day in Canada', '2012-01-01', '2012-01-01', '00:00:00', '00:00:00', 'According to the Gregorian calendar, used in Canada and many other countries, January 1 is the first day of a new year. This date is commonly known as New Year&#039;s Day and is a statutory holiday in all Canadian provinces and territories.', 'According to the Gregorian calendar, used in Canada and many other countries, January 1 is the first day of a new year. This date is commonly known as New Year&#039;s Day and is a statutory holiday in all Canadian provinces and territories.', 'John Doe', 'john@gmail.com', '555-555-5555', NULL, 1),
(6, 1, 'Epiphany', 'Epiphany', 'Epiphany in Canada', 'Epiphany in Canada', '2012-01-06', '2012-01-06', '00:00:00', '00:00:00', 'Epiphany is celebrated in Canada on January 6 each year. It remembers the three wise menÃƒÂ¢Ã¢â€šÂ¬Ã¢â€žÂ¢s visit to baby Jesus and his baptism, according to events in the Christian Bible. Mummers or naluyuks may visit homes in Newfoundland and Labrador at this time of the year.', 'Epiphany is celebrated in Canada on January 6 each year. It remembers the three wise menÃƒÂ¢Ã¢â€šÂ¬Ã¢â€žÂ¢s visit to baby Jesus and his baptism, according to events in the Christian Bible. Mummers or naluyuks may visit homes in Newfoundland and Labrador at this time of the year.', 'John Doe', 'john@gmail.com', '555-555-5555', NULL, 1),
(7, 1, 'Groundhog Day', 'Groundhog Day', 'Groundhog Day in Canada', 'Groundhog Day in Canada', '2012-12-27', '2012-09-26', '13:30:00', '00:00:00', 'Many Canadians take the time to observe Groundhog Day on February 2 each year, which is also Candlemas. Groundhog Day in Canada focuses on the concept of a groundhog coming out of its home in mid-winter to &quot;predictÃƒÂ¢Ã¢â€šÂ¬Ã‚Â if spring is on its way in the northern hemisphere.', 'Many Canadians take the time to observe Groundhog Day on February 2 each year, which is also Candlemas. Groundhog Day in Canada focuses on the concept of a groundhog coming out of its home in mid-winter to &quot;predictÃƒÂ¢Ã¢â€šÂ¬Ã‚Â if spring is on its way in the northern hemisphere.', 'John Doe', 'john@gmail.com', '555-555-5555', NULL, 1),
(8, 1, 'Valentine&#039;s Day', 'Valentine&#039;s Day', 'Valentine&#039;s Day in Canada', 'Valentine&#039;s Day in Canada', '2012-02-14', '2012-02-14', '00:00:00', '00:00:00', 'Valentine&#039;s Day is an opportunity for people in Canada to tell somebody that they love them in a romantic way. It falls on February 14, the name day of two saints, St Valentine of Rome and St Valentine of Terni. In pre-Christian times, the middle of February was a time of pagan fertility festivals in Europe and allegedly the time when birds chose a mate.', 'Valentine&#039;s Day is an opportunity for people in Canada to tell somebody that they love them in a romantic way. It falls on February 14, the name day of two saints, St Valentine of Rome and St Valentine of Terni. In pre-Christian times, the middle of February was a time of pagan fertility festivals in Europe and allegedly the time when birds chose a mate.', 'John Doe', 'john@gmail.com', '555-555-5555', NULL, 1),
(9, 1, 'Recurring Event 2', 'Recurring Event 2', 'Recurring Demo Event 2', 'Recurring Demo Event 2', '2013-04-10', '2014-12-26', '13:30:00', '16:30:00', 'Family Day is observed in the Canadian provinces of Alberta, Ontario and Saskatchewan on the third Monday of February. This holiday celebrates the importance of families and family life to people and their communities.', 'Family Day is observed in the Canadian provinces of Alberta, Ontario and Saskatchewan on the third Monday of February. This holiday celebrates the importance of families and family life to people and their communities.', 'John Doe', 'john@gmail.com', '555-555-5555', '#da8ba3', 1),
(10, 1, 'Recurring Event', 'Recurring Event', 'Recurring Demo Event', 'Recurring Demo Event', '2012-04-17', '2012-05-02', '11:00:00', '16:00:00', 'This event shows recurring feature in event manager&lt;br /&gt;\n', 'This event shows recurring feature in event manager&lt;br /&gt;\n', 'John Doe', 'john@gmail.com', '555-555-5555', NULL, 1);

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `mod_events_data`
--

CREATE TABLE `mod_events_data` (
  `id` int(11) NOT NULL,
  `event_id` int(11) NOT NULL DEFAULT '0',
  `event_date` date DEFAULT NULL,
  `color` varchar(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Tablo döküm verisi `mod_events_data`
--

INSERT INTO `mod_events_data` (`id`, `event_id`, `event_date`, `color`) VALUES
(1, 10, '2012-06-26', '#F00'),
(2, 10, '2012-06-25', '#F00'),
(3, 10, '2012-06-24', '#F00'),
(4, 10, '2012-06-23', '#F00'),
(5, 10, '2012-06-22', '#F00'),
(6, 10, '2012-06-21', '#F00'),
(7, 10, '2012-06-20', '#F00'),
(1275, 9, '2014-12-27', '#da8ba3'),
(1274, 9, '2014-12-26', '#da8ba3'),
(10, 10, '2012-04-17', '#F00'),
(11, 10, '2012-04-27', '#F00'),
(12, 10, '2012-04-28', '#F00'),
(13, 10, '2012-04-29', '#F00'),
(14, 10, '2012-04-30', '#F00'),
(15, 10, '2012-05-01', '#F00'),
(16, 10, '2012-05-02', '#F00'),
(1273, 9, '2014-12-25', '#da8ba3'),
(18, 7, '2011-12-27', '#ff9900'),
(19, 2, '2014-04-21', '#ffffff'),
(20, 7, '2012-12-27', '#ff9900'),
(1857, 1, '2015-01-01', '#b2d280'),
(1272, 9, '2014-12-24', '#da8ba3'),
(1271, 9, '2014-12-23', '#da8ba3'),
(1270, 9, '2014-12-22', '#da8ba3'),
(1269, 9, '2014-12-21', '#da8ba3'),
(1268, 9, '2014-12-20', '#da8ba3'),
(1267, 9, '2014-12-19', '#da8ba3'),
(1266, 9, '2014-12-18', '#da8ba3'),
(1265, 9, '2014-12-17', '#da8ba3'),
(1264, 9, '2014-12-16', '#da8ba3'),
(1263, 9, '2014-12-15', '#da8ba3'),
(1262, 9, '2014-12-14', '#da8ba3'),
(1261, 9, '2014-12-13', '#da8ba3'),
(1260, 9, '2014-12-12', '#da8ba3'),
(1259, 9, '2014-12-11', '#da8ba3'),
(1258, 9, '2014-12-10', '#da8ba3'),
(1257, 9, '2014-12-09', '#da8ba3'),
(1256, 9, '2014-12-08', '#da8ba3'),
(1255, 9, '2014-12-07', '#da8ba3'),
(1254, 9, '2014-12-06', '#da8ba3'),
(1253, 9, '2014-12-05', '#da8ba3'),
(1252, 9, '2014-12-04', '#da8ba3'),
(1251, 9, '2014-12-03', '#da8ba3'),
(1250, 9, '2014-12-02', '#da8ba3'),
(1249, 9, '2014-12-01', '#da8ba3'),
(1248, 9, '2014-11-30', '#da8ba3'),
(1247, 9, '2014-11-29', '#da8ba3'),
(1246, 9, '2014-11-28', '#da8ba3'),
(1245, 9, '2014-11-27', '#da8ba3'),
(1244, 9, '2014-11-26', '#da8ba3'),
(1243, 9, '2014-11-25', '#da8ba3'),
(1242, 9, '2014-11-24', '#da8ba3'),
(1241, 9, '2014-11-23', '#da8ba3'),
(1240, 9, '2014-11-22', '#da8ba3'),
(1239, 9, '2014-11-21', '#da8ba3'),
(1238, 9, '2014-11-20', '#da8ba3'),
(1237, 9, '2014-11-19', '#da8ba3'),
(1236, 9, '2014-11-18', '#da8ba3'),
(1235, 9, '2014-11-17', '#da8ba3'),
(1234, 9, '2014-11-16', '#da8ba3'),
(1233, 9, '2014-11-15', '#da8ba3'),
(1232, 9, '2014-11-14', '#da8ba3'),
(1231, 9, '2014-11-13', '#da8ba3'),
(1230, 9, '2014-11-12', '#da8ba3'),
(1229, 9, '2014-11-11', '#da8ba3'),
(1228, 9, '2014-11-10', '#da8ba3'),
(1227, 9, '2014-11-09', '#da8ba3'),
(1226, 9, '2014-11-08', '#da8ba3'),
(1225, 9, '2014-11-07', '#da8ba3'),
(1224, 9, '2014-11-06', '#da8ba3'),
(1223, 9, '2014-11-05', '#da8ba3'),
(1222, 9, '2014-11-04', '#da8ba3'),
(1221, 9, '2014-11-03', '#da8ba3'),
(1220, 9, '2014-11-02', '#da8ba3'),
(1219, 9, '2014-11-01', '#da8ba3'),
(1218, 9, '2014-10-31', '#da8ba3'),
(1217, 9, '2014-10-30', '#da8ba3'),
(1216, 9, '2014-10-29', '#da8ba3'),
(1215, 9, '2014-10-28', '#da8ba3'),
(1214, 9, '2014-10-27', '#da8ba3'),
(1213, 9, '2014-10-26', '#da8ba3'),
(1212, 9, '2014-10-25', '#da8ba3'),
(1211, 9, '2014-10-24', '#da8ba3'),
(1210, 9, '2014-10-23', '#da8ba3'),
(1209, 9, '2014-10-22', '#da8ba3'),
(1208, 9, '2014-10-21', '#da8ba3'),
(1207, 9, '2014-10-20', '#da8ba3'),
(1206, 9, '2014-10-19', '#da8ba3'),
(1205, 9, '2014-10-18', '#da8ba3'),
(1204, 9, '2014-10-17', '#da8ba3'),
(1203, 9, '2014-10-16', '#da8ba3'),
(1202, 9, '2014-10-15', '#da8ba3'),
(1201, 9, '2014-10-14', '#da8ba3'),
(1200, 9, '2014-10-13', '#da8ba3'),
(1199, 9, '2014-10-12', '#da8ba3'),
(1198, 9, '2014-10-11', '#da8ba3'),
(1197, 9, '2014-10-10', '#da8ba3'),
(1196, 9, '2014-10-09', '#da8ba3'),
(1195, 9, '2014-10-08', '#da8ba3'),
(1194, 9, '2014-10-07', '#da8ba3'),
(1193, 9, '2014-10-06', '#da8ba3'),
(1192, 9, '2014-10-05', '#da8ba3'),
(1191, 9, '2014-10-04', '#da8ba3'),
(1190, 9, '2014-10-03', '#da8ba3'),
(1189, 9, '2014-10-02', '#da8ba3'),
(1188, 9, '2014-10-01', '#da8ba3'),
(1187, 9, '2014-09-30', '#da8ba3'),
(1186, 9, '2014-09-29', '#da8ba3'),
(1185, 9, '2014-09-28', '#da8ba3'),
(1184, 9, '2014-09-27', '#da8ba3'),
(1183, 9, '2014-09-26', '#da8ba3'),
(1182, 9, '2014-09-25', '#da8ba3'),
(1181, 9, '2014-09-24', '#da8ba3'),
(1180, 9, '2014-09-23', '#da8ba3'),
(1179, 9, '2014-09-22', '#da8ba3'),
(1178, 9, '2014-09-21', '#da8ba3'),
(1177, 9, '2014-09-20', '#da8ba3'),
(1176, 9, '2014-09-19', '#da8ba3'),
(1175, 9, '2014-09-18', '#da8ba3'),
(1174, 9, '2014-09-17', '#da8ba3'),
(1173, 9, '2014-09-16', '#da8ba3'),
(1172, 9, '2014-09-15', '#da8ba3'),
(1171, 9, '2014-09-14', '#da8ba3'),
(1170, 9, '2014-09-13', '#da8ba3'),
(1169, 9, '2014-09-12', '#da8ba3'),
(1168, 9, '2014-09-11', '#da8ba3'),
(1167, 9, '2014-09-10', '#da8ba3'),
(1166, 9, '2014-09-09', '#da8ba3'),
(1165, 9, '2014-09-08', '#da8ba3'),
(1164, 9, '2014-09-07', '#da8ba3'),
(1163, 9, '2014-09-06', '#da8ba3'),
(1162, 9, '2014-09-05', '#da8ba3'),
(1161, 9, '2014-09-04', '#da8ba3'),
(1160, 9, '2014-09-03', '#da8ba3'),
(1159, 9, '2014-09-02', '#da8ba3'),
(1158, 9, '2014-09-01', '#da8ba3'),
(1157, 9, '2014-08-31', '#da8ba3'),
(1156, 9, '2014-08-30', '#da8ba3'),
(1155, 9, '2014-08-29', '#da8ba3'),
(1154, 9, '2014-08-28', '#da8ba3'),
(1153, 9, '2014-08-27', '#da8ba3'),
(1152, 9, '2014-08-26', '#da8ba3'),
(1151, 9, '2014-08-25', '#da8ba3'),
(1150, 9, '2014-08-24', '#da8ba3'),
(1149, 9, '2014-08-23', '#da8ba3'),
(1148, 9, '2014-08-22', '#da8ba3'),
(1147, 9, '2014-08-21', '#da8ba3'),
(1146, 9, '2014-08-20', '#da8ba3'),
(1145, 9, '2014-08-19', '#da8ba3'),
(1144, 9, '2014-08-18', '#da8ba3'),
(1143, 9, '2014-08-17', '#da8ba3'),
(1142, 9, '2014-08-16', '#da8ba3'),
(1141, 9, '2014-08-15', '#da8ba3'),
(1140, 9, '2014-08-14', '#da8ba3'),
(1139, 9, '2014-08-13', '#da8ba3'),
(1138, 9, '2014-08-12', '#da8ba3'),
(1137, 9, '2014-08-11', '#da8ba3'),
(1136, 9, '2014-08-10', '#da8ba3'),
(1135, 9, '2014-08-09', '#da8ba3'),
(1134, 9, '2014-08-08', '#da8ba3'),
(1133, 9, '2014-08-07', '#da8ba3'),
(1132, 9, '2014-08-06', '#da8ba3'),
(1131, 9, '2014-08-05', '#da8ba3'),
(1130, 9, '2014-08-04', '#da8ba3'),
(1129, 9, '2014-08-03', '#da8ba3'),
(1128, 9, '2014-08-02', '#da8ba3'),
(1127, 9, '2014-08-01', '#da8ba3'),
(1126, 9, '2014-07-31', '#da8ba3'),
(1125, 9, '2014-07-30', '#da8ba3'),
(1124, 9, '2014-07-29', '#da8ba3'),
(1123, 9, '2014-07-28', '#da8ba3'),
(1122, 9, '2014-07-27', '#da8ba3'),
(1121, 9, '2014-07-26', '#da8ba3'),
(1120, 9, '2014-07-25', '#da8ba3'),
(1119, 9, '2014-07-24', '#da8ba3'),
(1118, 9, '2014-07-23', '#da8ba3'),
(1117, 9, '2014-07-22', '#da8ba3'),
(1116, 9, '2014-07-21', '#da8ba3'),
(1115, 9, '2014-07-20', '#da8ba3'),
(1114, 9, '2014-07-19', '#da8ba3'),
(1113, 9, '2014-07-18', '#da8ba3'),
(1112, 9, '2014-07-17', '#da8ba3'),
(1111, 9, '2014-07-16', '#da8ba3'),
(1110, 9, '2014-07-15', '#da8ba3'),
(1109, 9, '2014-07-14', '#da8ba3'),
(1108, 9, '2014-07-13', '#da8ba3'),
(1107, 9, '2014-07-12', '#da8ba3'),
(1106, 9, '2014-07-11', '#da8ba3'),
(1105, 9, '2014-07-10', '#da8ba3'),
(1104, 9, '2014-07-09', '#da8ba3'),
(1103, 9, '2014-07-08', '#da8ba3'),
(1102, 9, '2014-07-07', '#da8ba3'),
(1101, 9, '2014-07-06', '#da8ba3'),
(1100, 9, '2014-07-05', '#da8ba3'),
(1099, 9, '2014-07-04', '#da8ba3'),
(1098, 9, '2014-07-03', '#da8ba3'),
(1097, 9, '2014-07-02', '#da8ba3'),
(1096, 9, '2014-07-01', '#da8ba3'),
(1095, 9, '2014-06-30', '#da8ba3'),
(1094, 9, '2014-06-29', '#da8ba3'),
(1093, 9, '2014-06-28', '#da8ba3'),
(1092, 9, '2014-06-27', '#da8ba3'),
(1091, 9, '2014-06-26', '#da8ba3'),
(1090, 9, '2014-06-25', '#da8ba3'),
(1089, 9, '2014-06-24', '#da8ba3'),
(1088, 9, '2014-06-23', '#da8ba3'),
(1087, 9, '2014-06-22', '#da8ba3'),
(1086, 9, '2014-06-21', '#da8ba3'),
(1085, 9, '2014-06-20', '#da8ba3'),
(1084, 9, '2014-06-19', '#da8ba3'),
(1083, 9, '2014-06-18', '#da8ba3'),
(1082, 9, '2014-06-17', '#da8ba3'),
(1081, 9, '2014-06-16', '#da8ba3'),
(1080, 9, '2014-06-15', '#da8ba3'),
(1079, 9, '2014-06-14', '#da8ba3'),
(1078, 9, '2014-06-13', '#da8ba3'),
(1077, 9, '2014-06-12', '#da8ba3'),
(1076, 9, '2014-06-11', '#da8ba3'),
(1075, 9, '2014-06-10', '#da8ba3'),
(1074, 9, '2014-06-09', '#da8ba3'),
(1073, 9, '2014-06-08', '#da8ba3'),
(1072, 9, '2014-06-07', '#da8ba3'),
(1071, 9, '2014-06-06', '#da8ba3'),
(1070, 9, '2014-06-05', '#da8ba3'),
(1069, 9, '2014-06-04', '#da8ba3'),
(1068, 9, '2014-06-03', '#da8ba3'),
(1067, 9, '2014-06-02', '#da8ba3'),
(1066, 9, '2014-06-01', '#da8ba3'),
(1065, 9, '2014-05-31', '#da8ba3'),
(1064, 9, '2014-05-30', '#da8ba3'),
(1063, 9, '2014-05-29', '#da8ba3'),
(1062, 9, '2014-05-28', '#da8ba3'),
(1061, 9, '2014-05-27', '#da8ba3'),
(1060, 9, '2014-05-26', '#da8ba3'),
(1059, 9, '2014-05-25', '#da8ba3'),
(1058, 9, '2014-05-24', '#da8ba3'),
(1057, 9, '2014-05-23', '#da8ba3'),
(1056, 9, '2014-05-22', '#da8ba3'),
(1055, 9, '2014-05-21', '#da8ba3'),
(1054, 9, '2014-05-20', '#da8ba3'),
(1053, 9, '2014-05-19', '#da8ba3'),
(1052, 9, '2014-05-18', '#da8ba3'),
(1051, 9, '2014-05-17', '#da8ba3'),
(1050, 9, '2014-05-16', '#da8ba3'),
(1049, 9, '2014-05-15', '#da8ba3'),
(1048, 9, '2014-05-14', '#da8ba3'),
(1047, 9, '2014-05-13', '#da8ba3'),
(1046, 9, '2014-05-12', '#da8ba3'),
(1045, 9, '2014-05-11', '#da8ba3'),
(1044, 9, '2014-05-10', '#da8ba3'),
(1043, 9, '2014-05-09', '#da8ba3'),
(1042, 9, '2014-05-08', '#da8ba3'),
(1041, 9, '2014-05-07', '#da8ba3'),
(1040, 9, '2014-05-06', '#da8ba3'),
(1039, 9, '2014-05-05', '#da8ba3'),
(1038, 9, '2014-05-04', '#da8ba3'),
(1037, 9, '2014-05-03', '#da8ba3'),
(1036, 9, '2014-05-02', '#da8ba3'),
(1035, 9, '2014-05-01', '#da8ba3'),
(1034, 9, '2014-04-30', '#da8ba3'),
(1033, 9, '2014-04-29', '#da8ba3'),
(1032, 9, '2014-04-28', '#da8ba3'),
(1031, 9, '2014-04-27', '#da8ba3'),
(1030, 9, '2014-04-26', '#da8ba3'),
(1029, 9, '2014-04-25', '#da8ba3'),
(1028, 9, '2014-04-24', '#da8ba3'),
(1027, 9, '2014-04-23', '#da8ba3'),
(1026, 9, '2014-04-22', '#da8ba3'),
(1025, 9, '2014-04-21', '#da8ba3'),
(1024, 9, '2014-04-20', '#da8ba3'),
(1023, 9, '2014-04-19', '#da8ba3'),
(1022, 9, '2014-04-18', '#da8ba3'),
(1021, 9, '2014-04-17', '#da8ba3'),
(1020, 9, '2014-04-16', '#da8ba3'),
(1019, 9, '2014-04-15', '#da8ba3'),
(1018, 9, '2014-04-14', '#da8ba3'),
(1017, 9, '2014-04-13', '#da8ba3'),
(1016, 9, '2014-04-12', '#da8ba3'),
(1015, 9, '2014-04-11', '#da8ba3'),
(1014, 9, '2014-04-10', '#da8ba3'),
(1013, 9, '2014-04-09', '#da8ba3'),
(1012, 9, '2014-04-08', '#da8ba3'),
(1011, 9, '2014-04-07', '#da8ba3'),
(1010, 9, '2014-04-06', '#da8ba3'),
(1009, 9, '2014-04-05', '#da8ba3'),
(1008, 9, '2014-04-04', '#da8ba3'),
(1007, 9, '2014-04-03', '#da8ba3'),
(1006, 9, '2014-04-02', '#da8ba3'),
(1005, 9, '2014-04-01', '#da8ba3'),
(1004, 9, '2014-03-31', '#da8ba3'),
(1003, 9, '2014-03-30', '#da8ba3'),
(1002, 9, '2014-03-29', '#da8ba3'),
(1001, 9, '2014-03-28', '#da8ba3'),
(1000, 9, '2014-03-27', '#da8ba3'),
(999, 9, '2014-03-26', '#da8ba3'),
(998, 9, '2014-03-25', '#da8ba3'),
(997, 9, '2014-03-24', '#da8ba3'),
(996, 9, '2014-03-23', '#da8ba3'),
(995, 9, '2014-03-22', '#da8ba3'),
(994, 9, '2014-03-21', '#da8ba3'),
(993, 9, '2014-03-20', '#da8ba3'),
(992, 9, '2014-03-19', '#da8ba3'),
(991, 9, '2014-03-18', '#da8ba3'),
(990, 9, '2014-03-17', '#da8ba3'),
(989, 9, '2014-03-16', '#da8ba3'),
(988, 9, '2014-03-15', '#da8ba3'),
(987, 9, '2014-03-14', '#da8ba3'),
(986, 9, '2014-03-13', '#da8ba3'),
(985, 9, '2014-03-12', '#da8ba3'),
(984, 9, '2014-03-11', '#da8ba3'),
(983, 9, '2014-03-10', '#da8ba3'),
(982, 9, '2014-03-09', '#da8ba3'),
(981, 9, '2014-03-08', '#da8ba3'),
(980, 9, '2014-03-07', '#da8ba3'),
(979, 9, '2014-03-06', '#da8ba3'),
(978, 9, '2014-03-05', '#da8ba3'),
(977, 9, '2014-03-04', '#da8ba3'),
(976, 9, '2014-03-03', '#da8ba3'),
(975, 9, '2014-03-02', '#da8ba3'),
(974, 9, '2014-03-01', '#da8ba3'),
(973, 9, '2014-02-28', '#da8ba3'),
(972, 9, '2014-02-27', '#da8ba3'),
(971, 9, '2014-02-26', '#da8ba3'),
(970, 9, '2014-02-25', '#da8ba3'),
(969, 9, '2014-02-24', '#da8ba3'),
(968, 9, '2014-02-23', '#da8ba3'),
(967, 9, '2014-02-22', '#da8ba3'),
(966, 9, '2014-02-21', '#da8ba3'),
(965, 9, '2014-02-20', '#da8ba3'),
(964, 9, '2014-02-19', '#da8ba3'),
(963, 9, '2014-02-18', '#da8ba3'),
(962, 9, '2014-02-17', '#da8ba3'),
(961, 9, '2014-02-16', '#da8ba3'),
(960, 9, '2014-02-15', '#da8ba3'),
(959, 9, '2014-02-14', '#da8ba3'),
(958, 9, '2014-02-13', '#da8ba3'),
(957, 9, '2014-02-12', '#da8ba3'),
(956, 9, '2014-02-11', '#da8ba3'),
(955, 9, '2014-02-10', '#da8ba3'),
(954, 9, '2014-02-09', '#da8ba3'),
(953, 9, '2014-02-08', '#da8ba3'),
(952, 9, '2014-02-07', '#da8ba3'),
(951, 9, '2014-02-06', '#da8ba3'),
(950, 9, '2014-02-05', '#da8ba3'),
(949, 9, '2014-02-04', '#da8ba3'),
(948, 9, '2014-02-03', '#da8ba3'),
(947, 9, '2014-02-02', '#da8ba3'),
(946, 9, '2014-02-01', '#da8ba3'),
(945, 9, '2014-01-31', '#da8ba3'),
(944, 9, '2014-01-30', '#da8ba3'),
(943, 9, '2014-01-29', '#da8ba3'),
(942, 9, '2014-01-28', '#da8ba3'),
(941, 9, '2014-01-27', '#da8ba3'),
(940, 9, '2014-01-26', '#da8ba3'),
(939, 9, '2014-01-25', '#da8ba3'),
(938, 9, '2014-01-24', '#da8ba3'),
(937, 9, '2014-01-23', '#da8ba3'),
(936, 9, '2014-01-22', '#da8ba3'),
(935, 9, '2014-01-21', '#da8ba3'),
(934, 9, '2014-01-20', '#da8ba3'),
(933, 9, '2014-01-19', '#da8ba3'),
(932, 9, '2014-01-18', '#da8ba3'),
(931, 9, '2014-01-17', '#da8ba3'),
(930, 9, '2014-01-16', '#da8ba3'),
(929, 9, '2014-01-15', '#da8ba3'),
(928, 9, '2014-01-14', '#da8ba3'),
(927, 9, '2014-01-13', '#da8ba3'),
(926, 9, '2014-01-12', '#da8ba3'),
(925, 9, '2014-01-11', '#da8ba3'),
(924, 9, '2014-01-10', '#da8ba3'),
(923, 9, '2014-01-09', '#da8ba3'),
(922, 9, '2014-01-08', '#da8ba3'),
(921, 9, '2014-01-07', '#da8ba3'),
(920, 9, '2014-01-06', '#da8ba3'),
(919, 9, '2014-01-05', '#da8ba3'),
(918, 9, '2014-01-04', '#da8ba3'),
(917, 9, '2014-01-03', '#da8ba3'),
(916, 9, '2014-01-02', '#da8ba3'),
(915, 9, '2014-01-01', '#da8ba3'),
(914, 9, '2013-12-31', '#da8ba3'),
(913, 9, '2013-12-30', '#da8ba3'),
(912, 9, '2013-12-29', '#da8ba3'),
(911, 9, '2013-12-28', '#da8ba3'),
(910, 9, '2013-12-27', '#da8ba3'),
(909, 9, '2013-12-26', '#da8ba3'),
(908, 9, '2013-12-25', '#da8ba3'),
(907, 9, '2013-12-24', '#da8ba3'),
(906, 9, '2013-12-23', '#da8ba3'),
(905, 9, '2013-12-22', '#da8ba3'),
(904, 9, '2013-12-21', '#da8ba3'),
(903, 9, '2013-12-20', '#da8ba3'),
(902, 9, '2013-12-19', '#da8ba3'),
(901, 9, '2013-12-18', '#da8ba3'),
(900, 9, '2013-12-17', '#da8ba3'),
(899, 9, '2013-12-16', '#da8ba3'),
(898, 9, '2013-12-15', '#da8ba3'),
(897, 9, '2013-12-14', '#da8ba3'),
(896, 9, '2013-12-13', '#da8ba3'),
(895, 9, '2013-12-12', '#da8ba3'),
(894, 9, '2013-12-11', '#da8ba3'),
(893, 9, '2013-12-10', '#da8ba3'),
(892, 9, '2013-12-09', '#da8ba3'),
(891, 9, '2013-12-08', '#da8ba3'),
(890, 9, '2013-12-07', '#da8ba3'),
(889, 9, '2013-12-06', '#da8ba3'),
(888, 9, '2013-12-05', '#da8ba3'),
(887, 9, '2013-12-04', '#da8ba3'),
(886, 9, '2013-12-03', '#da8ba3'),
(885, 9, '2013-12-02', '#da8ba3'),
(884, 9, '2013-12-01', '#da8ba3'),
(883, 9, '2013-11-30', '#da8ba3'),
(882, 9, '2013-11-29', '#da8ba3'),
(881, 9, '2013-11-28', '#da8ba3'),
(880, 9, '2013-11-27', '#da8ba3'),
(879, 9, '2013-11-26', '#da8ba3'),
(878, 9, '2013-11-25', '#da8ba3'),
(877, 9, '2013-11-24', '#da8ba3'),
(876, 9, '2013-11-23', '#da8ba3'),
(875, 9, '2013-11-22', '#da8ba3'),
(874, 9, '2013-11-21', '#da8ba3'),
(873, 9, '2013-11-20', '#da8ba3'),
(872, 9, '2013-11-19', '#da8ba3'),
(871, 9, '2013-11-18', '#da8ba3'),
(870, 9, '2013-11-17', '#da8ba3'),
(869, 9, '2013-11-16', '#da8ba3'),
(868, 9, '2013-11-15', '#da8ba3'),
(867, 9, '2013-11-14', '#da8ba3'),
(866, 9, '2013-11-13', '#da8ba3'),
(865, 9, '2013-11-12', '#da8ba3'),
(864, 9, '2013-11-11', '#da8ba3'),
(863, 9, '2013-11-10', '#da8ba3'),
(862, 9, '2013-11-09', '#da8ba3'),
(861, 9, '2013-11-08', '#da8ba3'),
(860, 9, '2013-11-07', '#da8ba3'),
(859, 9, '2013-11-06', '#da8ba3'),
(858, 9, '2013-11-05', '#da8ba3'),
(857, 9, '2013-11-04', '#da8ba3'),
(856, 9, '2013-11-03', '#da8ba3'),
(855, 9, '2013-11-02', '#da8ba3'),
(854, 9, '2013-11-01', '#da8ba3'),
(853, 9, '2013-10-31', '#da8ba3'),
(852, 9, '2013-10-30', '#da8ba3'),
(851, 9, '2013-10-29', '#da8ba3'),
(850, 9, '2013-10-28', '#da8ba3'),
(849, 9, '2013-10-27', '#da8ba3'),
(848, 9, '2013-10-26', '#da8ba3'),
(847, 9, '2013-10-25', '#da8ba3'),
(846, 9, '2013-10-24', '#da8ba3'),
(845, 9, '2013-10-23', '#da8ba3'),
(844, 9, '2013-10-22', '#da8ba3'),
(843, 9, '2013-10-21', '#da8ba3'),
(842, 9, '2013-10-20', '#da8ba3'),
(841, 9, '2013-10-19', '#da8ba3'),
(840, 9, '2013-10-18', '#da8ba3'),
(839, 9, '2013-10-17', '#da8ba3'),
(838, 9, '2013-10-16', '#da8ba3'),
(837, 9, '2013-10-15', '#da8ba3'),
(836, 9, '2013-10-14', '#da8ba3'),
(835, 9, '2013-10-13', '#da8ba3'),
(834, 9, '2013-10-12', '#da8ba3'),
(833, 9, '2013-10-11', '#da8ba3'),
(832, 9, '2013-10-10', '#da8ba3'),
(831, 9, '2013-10-09', '#da8ba3'),
(830, 9, '2013-10-08', '#da8ba3'),
(829, 9, '2013-10-07', '#da8ba3'),
(828, 9, '2013-10-06', '#da8ba3'),
(827, 9, '2013-10-05', '#da8ba3'),
(826, 9, '2013-10-04', '#da8ba3'),
(825, 9, '2013-10-03', '#da8ba3'),
(824, 9, '2013-10-02', '#da8ba3'),
(823, 9, '2013-10-01', '#da8ba3'),
(822, 9, '2013-09-30', '#da8ba3'),
(821, 9, '2013-09-29', '#da8ba3'),
(820, 9, '2013-09-28', '#da8ba3'),
(819, 9, '2013-09-27', '#da8ba3'),
(818, 9, '2013-09-26', '#da8ba3'),
(817, 9, '2013-09-25', '#da8ba3'),
(816, 9, '2013-09-24', '#da8ba3'),
(815, 9, '2013-09-23', '#da8ba3'),
(814, 9, '2013-09-22', '#da8ba3'),
(813, 9, '2013-09-21', '#da8ba3'),
(812, 9, '2013-09-20', '#da8ba3'),
(811, 9, '2013-09-19', '#da8ba3'),
(810, 9, '2013-09-18', '#da8ba3'),
(809, 9, '2013-09-17', '#da8ba3'),
(808, 9, '2013-09-16', '#da8ba3'),
(807, 9, '2013-09-15', '#da8ba3'),
(806, 9, '2013-09-14', '#da8ba3'),
(805, 9, '2013-09-13', '#da8ba3'),
(804, 9, '2013-09-12', '#da8ba3'),
(803, 9, '2013-09-11', '#da8ba3'),
(802, 9, '2013-09-10', '#da8ba3'),
(801, 9, '2013-09-09', '#da8ba3'),
(800, 9, '2013-09-08', '#da8ba3'),
(799, 9, '2013-09-07', '#da8ba3'),
(798, 9, '2013-09-06', '#da8ba3'),
(797, 9, '2013-09-05', '#da8ba3'),
(796, 9, '2013-09-04', '#da8ba3'),
(795, 9, '2013-09-03', '#da8ba3'),
(794, 9, '2013-09-02', '#da8ba3'),
(793, 9, '2013-09-01', '#da8ba3'),
(792, 9, '2013-08-31', '#da8ba3'),
(791, 9, '2013-08-30', '#da8ba3'),
(790, 9, '2013-08-29', '#da8ba3'),
(789, 9, '2013-08-28', '#da8ba3'),
(788, 9, '2013-08-27', '#da8ba3'),
(787, 9, '2013-08-26', '#da8ba3'),
(786, 9, '2013-08-25', '#da8ba3'),
(785, 9, '2013-08-24', '#da8ba3'),
(784, 9, '2013-08-23', '#da8ba3'),
(783, 9, '2013-08-22', '#da8ba3'),
(782, 9, '2013-08-21', '#da8ba3'),
(781, 9, '2013-08-20', '#da8ba3'),
(780, 9, '2013-08-19', '#da8ba3'),
(779, 9, '2013-08-18', '#da8ba3'),
(778, 9, '2013-08-17', '#da8ba3'),
(777, 9, '2013-08-16', '#da8ba3'),
(776, 9, '2013-08-15', '#da8ba3'),
(775, 9, '2013-08-14', '#da8ba3'),
(774, 9, '2013-08-13', '#da8ba3'),
(773, 9, '2013-08-12', '#da8ba3'),
(772, 9, '2013-08-11', '#da8ba3'),
(771, 9, '2013-08-10', '#da8ba3'),
(770, 9, '2013-08-09', '#da8ba3'),
(769, 9, '2013-08-08', '#da8ba3'),
(768, 9, '2013-08-07', '#da8ba3'),
(767, 9, '2013-08-06', '#da8ba3'),
(766, 9, '2013-08-05', '#da8ba3'),
(765, 9, '2013-08-04', '#da8ba3'),
(764, 9, '2013-08-03', '#da8ba3'),
(763, 9, '2013-08-02', '#da8ba3'),
(762, 9, '2013-08-01', '#da8ba3'),
(761, 9, '2013-07-31', '#da8ba3'),
(760, 9, '2013-07-30', '#da8ba3'),
(759, 9, '2013-07-29', '#da8ba3'),
(758, 9, '2013-07-28', '#da8ba3'),
(757, 9, '2013-07-27', '#da8ba3'),
(756, 9, '2013-07-26', '#da8ba3'),
(755, 9, '2013-07-25', '#da8ba3'),
(754, 9, '2013-07-24', '#da8ba3'),
(753, 9, '2013-07-23', '#da8ba3'),
(752, 9, '2013-07-22', '#da8ba3'),
(751, 9, '2013-07-21', '#da8ba3'),
(750, 9, '2013-07-20', '#da8ba3'),
(749, 9, '2013-07-19', '#da8ba3'),
(748, 9, '2013-07-18', '#da8ba3'),
(747, 9, '2013-07-17', '#da8ba3'),
(746, 9, '2013-07-16', '#da8ba3'),
(745, 9, '2013-07-15', '#da8ba3'),
(744, 9, '2013-07-14', '#da8ba3'),
(743, 9, '2013-07-13', '#da8ba3'),
(742, 9, '2013-07-12', '#da8ba3'),
(741, 9, '2013-07-11', '#da8ba3'),
(740, 9, '2013-07-10', '#da8ba3'),
(739, 9, '2013-07-09', '#da8ba3'),
(738, 9, '2013-07-08', '#da8ba3'),
(737, 9, '2013-07-07', '#da8ba3'),
(736, 9, '2013-07-06', '#da8ba3'),
(735, 9, '2013-07-05', '#da8ba3'),
(734, 9, '2013-07-04', '#da8ba3'),
(733, 9, '2013-07-03', '#da8ba3'),
(732, 9, '2013-07-02', '#da8ba3'),
(731, 9, '2013-07-01', '#da8ba3'),
(730, 9, '2013-06-30', '#da8ba3'),
(729, 9, '2013-06-29', '#da8ba3'),
(728, 9, '2013-06-28', '#da8ba3'),
(727, 9, '2013-06-27', '#da8ba3'),
(726, 9, '2013-06-26', '#da8ba3'),
(725, 9, '2013-06-25', '#da8ba3'),
(724, 9, '2013-06-24', '#da8ba3'),
(723, 9, '2013-06-23', '#da8ba3'),
(722, 9, '2013-06-22', '#da8ba3'),
(721, 9, '2013-06-21', '#da8ba3'),
(720, 9, '2013-06-20', '#da8ba3'),
(719, 9, '2013-06-19', '#da8ba3'),
(718, 9, '2013-06-18', '#da8ba3'),
(717, 9, '2013-06-17', '#da8ba3'),
(716, 9, '2013-06-16', '#da8ba3'),
(715, 9, '2013-06-15', '#da8ba3'),
(714, 9, '2013-06-14', '#da8ba3'),
(713, 9, '2013-06-13', '#da8ba3'),
(712, 9, '2013-06-12', '#da8ba3'),
(711, 9, '2013-06-11', '#da8ba3'),
(710, 9, '2013-06-10', '#da8ba3'),
(709, 9, '2013-06-09', '#da8ba3'),
(708, 9, '2013-06-08', '#da8ba3'),
(707, 9, '2013-06-07', '#da8ba3'),
(706, 9, '2013-06-06', '#da8ba3'),
(705, 9, '2013-06-05', '#da8ba3'),
(704, 9, '2013-06-04', '#da8ba3'),
(703, 9, '2013-06-03', '#da8ba3'),
(702, 9, '2013-06-02', '#da8ba3'),
(701, 9, '2013-06-01', '#da8ba3'),
(700, 9, '2013-05-31', '#da8ba3'),
(699, 9, '2013-05-30', '#da8ba3'),
(698, 9, '2013-05-29', '#da8ba3'),
(697, 9, '2013-05-28', '#da8ba3'),
(696, 9, '2013-05-27', '#da8ba3'),
(695, 9, '2013-05-26', '#da8ba3'),
(694, 9, '2013-05-25', '#da8ba3'),
(693, 9, '2013-05-24', '#da8ba3'),
(692, 9, '2013-05-23', '#da8ba3'),
(691, 9, '2013-05-22', '#da8ba3'),
(690, 9, '2013-05-21', '#da8ba3'),
(689, 9, '2013-05-20', '#da8ba3'),
(688, 9, '2013-05-19', '#da8ba3'),
(687, 9, '2013-05-18', '#da8ba3'),
(686, 9, '2013-05-17', '#da8ba3'),
(685, 9, '2013-05-16', '#da8ba3'),
(684, 9, '2013-05-15', '#da8ba3'),
(683, 9, '2013-05-14', '#da8ba3'),
(682, 9, '2013-05-13', '#da8ba3'),
(681, 9, '2013-05-12', '#da8ba3'),
(680, 9, '2013-05-11', '#da8ba3'),
(679, 9, '2013-05-10', '#da8ba3'),
(678, 9, '2013-05-09', '#da8ba3'),
(677, 9, '2013-05-08', '#da8ba3'),
(676, 9, '2013-05-07', '#da8ba3'),
(675, 9, '2013-05-06', '#da8ba3'),
(674, 9, '2013-05-05', '#da8ba3'),
(673, 9, '2013-05-04', '#da8ba3'),
(672, 9, '2013-05-03', '#da8ba3'),
(671, 9, '2013-05-02', '#da8ba3'),
(670, 9, '2013-05-01', '#da8ba3'),
(669, 9, '2013-04-30', '#da8ba3'),
(668, 9, '2013-04-29', '#da8ba3'),
(667, 9, '2013-04-28', '#da8ba3'),
(666, 9, '2013-04-27', '#da8ba3'),
(665, 9, '2013-04-26', '#da8ba3'),
(664, 9, '2013-04-25', '#da8ba3'),
(663, 9, '2013-04-24', '#da8ba3'),
(662, 9, '2013-04-23', '#da8ba3'),
(661, 9, '2013-04-22', '#da8ba3'),
(660, 9, '2013-04-21', '#da8ba3'),
(659, 9, '2013-04-20', '#da8ba3'),
(658, 9, '2013-04-19', '#da8ba3'),
(657, 9, '2013-04-18', '#da8ba3'),
(656, 9, '2013-04-17', '#da8ba3'),
(655, 9, '2013-04-16', '#da8ba3'),
(654, 9, '2013-04-15', '#da8ba3'),
(653, 9, '2013-04-14', '#da8ba3'),
(652, 9, '2013-04-13', '#da8ba3'),
(651, 9, '2013-04-12', '#da8ba3'),
(650, 9, '2013-04-11', '#da8ba3'),
(649, 9, '2013-04-10', '#da8ba3'),
(1856, 1, '2014-12-31', '#b2d280'),
(1855, 1, '2014-12-30', '#b2d280'),
(1854, 1, '2014-12-29', '#b2d280'),
(1853, 1, '2014-12-28', '#b2d280'),
(1852, 1, '2014-12-27', '#b2d280'),
(1851, 1, '2014-12-26', '#b2d280'),
(1850, 1, '2014-12-25', '#b2d280'),
(1849, 1, '2014-12-24', '#b2d280'),
(1848, 1, '2014-12-23', '#b2d280'),
(1847, 1, '2014-12-22', '#b2d280'),
(1846, 1, '2014-12-21', '#b2d280'),
(1845, 1, '2014-12-20', '#b2d280'),
(1844, 1, '2014-12-19', '#b2d280'),
(1843, 1, '2014-12-18', '#b2d280'),
(1842, 1, '2014-12-17', '#b2d280'),
(1841, 1, '2014-12-16', '#b2d280'),
(1840, 1, '2014-12-15', '#b2d280'),
(1839, 1, '2014-12-14', '#b2d280'),
(1838, 1, '2014-12-13', '#b2d280'),
(1837, 1, '2014-12-12', '#b2d280'),
(1836, 1, '2014-12-11', '#b2d280'),
(1835, 1, '2014-12-10', '#b2d280'),
(1834, 1, '2014-12-09', '#b2d280'),
(1833, 1, '2014-12-08', '#b2d280'),
(1832, 1, '2014-12-07', '#b2d280'),
(1831, 1, '2014-12-06', '#b2d280'),
(1830, 1, '2014-12-05', '#b2d280'),
(1829, 1, '2014-12-04', '#b2d280'),
(1828, 1, '2014-12-03', '#b2d280'),
(1827, 1, '2014-12-02', '#b2d280'),
(1826, 1, '2014-12-01', '#b2d280'),
(1825, 1, '2014-11-30', '#b2d280'),
(1824, 1, '2014-11-29', '#b2d280'),
(1823, 1, '2014-11-28', '#b2d280'),
(1822, 1, '2014-11-27', '#b2d280'),
(1821, 1, '2014-11-26', '#b2d280'),
(1820, 1, '2014-11-25', '#b2d280'),
(1819, 1, '2014-11-24', '#b2d280'),
(1818, 1, '2014-11-23', '#b2d280'),
(1817, 1, '2014-11-22', '#b2d280'),
(1816, 1, '2014-11-21', '#b2d280'),
(1815, 1, '2014-11-20', '#b2d280'),
(1814, 1, '2014-11-19', '#b2d280'),
(1813, 1, '2014-11-18', '#b2d280'),
(1812, 1, '2014-11-17', '#b2d280'),
(1811, 1, '2014-11-16', '#b2d280'),
(1810, 1, '2014-11-15', '#b2d280'),
(1809, 1, '2014-11-14', '#b2d280'),
(1808, 1, '2014-11-13', '#b2d280'),
(1807, 1, '2014-11-12', '#b2d280'),
(1806, 1, '2014-11-11', '#b2d280'),
(1805, 1, '2014-11-10', '#b2d280'),
(1804, 1, '2014-11-09', '#b2d280'),
(1803, 1, '2014-11-08', '#b2d280'),
(1802, 1, '2014-11-07', '#b2d280'),
(1801, 1, '2014-11-06', '#b2d280'),
(1800, 1, '2014-11-05', '#b2d280'),
(1799, 1, '2014-11-04', '#b2d280'),
(1798, 1, '2014-11-03', '#b2d280'),
(1797, 1, '2014-11-02', '#b2d280'),
(1796, 1, '2014-11-01', '#b2d280'),
(1795, 1, '2014-10-31', '#b2d280'),
(1794, 1, '2014-10-30', '#b2d280'),
(1793, 1, '2014-10-29', '#b2d280'),
(1792, 1, '2014-10-28', '#b2d280'),
(1791, 1, '2014-10-27', '#b2d280'),
(1790, 1, '2014-10-26', '#b2d280'),
(1789, 1, '2014-10-25', '#b2d280'),
(1788, 1, '2014-10-24', '#b2d280'),
(1787, 1, '2014-10-23', '#b2d280'),
(1786, 1, '2014-10-22', '#b2d280'),
(1785, 1, '2014-10-21', '#b2d280'),
(1784, 1, '2014-10-20', '#b2d280'),
(1783, 1, '2014-10-19', '#b2d280'),
(1782, 1, '2014-10-18', '#b2d280'),
(1781, 1, '2014-10-17', '#b2d280'),
(1780, 1, '2014-10-16', '#b2d280'),
(1779, 1, '2014-10-15', '#b2d280'),
(1778, 1, '2014-10-14', '#b2d280'),
(1777, 1, '2014-10-13', '#b2d280'),
(1776, 1, '2014-10-12', '#b2d280'),
(1775, 1, '2014-10-11', '#b2d280'),
(1774, 1, '2014-10-10', '#b2d280'),
(1773, 1, '2014-10-09', '#b2d280'),
(1772, 1, '2014-10-08', '#b2d280'),
(1771, 1, '2014-10-07', '#b2d280'),
(1770, 1, '2014-10-06', '#b2d280'),
(1769, 1, '2014-10-05', '#b2d280'),
(1768, 1, '2014-10-04', '#b2d280'),
(1767, 1, '2014-10-03', '#b2d280'),
(1766, 1, '2014-10-02', '#b2d280'),
(1765, 1, '2014-10-01', '#b2d280'),
(1764, 1, '2014-09-30', '#b2d280'),
(1763, 1, '2014-09-29', '#b2d280'),
(1762, 1, '2014-09-28', '#b2d280'),
(1761, 1, '2014-09-27', '#b2d280'),
(1760, 1, '2014-09-26', '#b2d280'),
(1759, 1, '2014-09-25', '#b2d280'),
(1758, 1, '2014-09-24', '#b2d280'),
(1757, 1, '2014-09-23', '#b2d280'),
(1756, 1, '2014-09-22', '#b2d280'),
(1755, 1, '2014-09-21', '#b2d280'),
(1754, 1, '2014-09-20', '#b2d280'),
(1753, 1, '2014-09-19', '#b2d280'),
(1752, 1, '2014-09-18', '#b2d280'),
(1751, 1, '2014-09-17', '#b2d280'),
(1750, 1, '2014-09-16', '#b2d280'),
(1749, 1, '2014-09-15', '#b2d280'),
(1748, 1, '2014-09-14', '#b2d280'),
(1747, 1, '2014-09-13', '#b2d280'),
(1746, 1, '2014-09-12', '#b2d280'),
(1745, 1, '2014-09-11', '#b2d280'),
(1744, 1, '2014-09-10', '#b2d280'),
(1743, 1, '2014-09-09', '#b2d280'),
(1742, 1, '2014-09-08', '#b2d280'),
(1741, 1, '2014-09-07', '#b2d280'),
(1740, 1, '2014-09-06', '#b2d280'),
(1739, 1, '2014-09-05', '#b2d280'),
(1738, 1, '2014-09-04', '#b2d280'),
(1737, 1, '2014-09-03', '#b2d280'),
(1736, 1, '2014-09-02', '#b2d280'),
(1735, 1, '2014-09-01', '#b2d280'),
(1734, 1, '2014-08-31', '#b2d280'),
(1733, 1, '2014-08-30', '#b2d280'),
(1732, 1, '2014-08-29', '#b2d280'),
(1731, 1, '2014-08-28', '#b2d280'),
(1730, 1, '2014-08-27', '#b2d280'),
(1729, 1, '2014-08-26', '#b2d280'),
(1728, 1, '2014-08-25', '#b2d280'),
(1727, 1, '2014-08-24', '#b2d280'),
(1726, 1, '2014-08-23', '#b2d280'),
(1725, 1, '2014-08-22', '#b2d280'),
(1724, 1, '2014-08-21', '#b2d280'),
(1723, 1, '2014-08-20', '#b2d280'),
(1722, 1, '2014-08-19', '#b2d280'),
(1721, 1, '2014-08-18', '#b2d280'),
(1720, 1, '2014-08-17', '#b2d280'),
(1719, 1, '2014-08-16', '#b2d280'),
(1718, 1, '2014-08-15', '#b2d280'),
(1717, 1, '2014-08-14', '#b2d280'),
(1716, 1, '2014-08-13', '#b2d280'),
(1715, 1, '2014-08-12', '#b2d280'),
(1714, 1, '2014-08-11', '#b2d280'),
(1713, 1, '2014-08-10', '#b2d280'),
(1712, 1, '2014-08-09', '#b2d280'),
(1711, 1, '2014-08-08', '#b2d280'),
(1710, 1, '2014-08-07', '#b2d280'),
(1709, 1, '2014-08-06', '#b2d280'),
(1708, 1, '2014-08-05', '#b2d280'),
(1707, 1, '2014-08-04', '#b2d280'),
(1706, 1, '2014-08-03', '#b2d280'),
(1705, 1, '2014-08-02', '#b2d280'),
(1704, 1, '2014-08-01', '#b2d280'),
(1703, 1, '2014-07-31', '#b2d280'),
(1702, 1, '2014-07-30', '#b2d280'),
(1701, 1, '2014-07-29', '#b2d280'),
(1700, 1, '2014-07-28', '#b2d280'),
(1699, 1, '2014-07-27', '#b2d280'),
(1698, 1, '2014-07-26', '#b2d280'),
(1697, 1, '2014-07-25', '#b2d280'),
(1696, 1, '2014-07-24', '#b2d280'),
(1695, 1, '2014-07-23', '#b2d280'),
(1694, 1, '2014-07-22', '#b2d280'),
(1693, 1, '2014-07-21', '#b2d280'),
(1692, 1, '2014-07-20', '#b2d280'),
(1691, 1, '2014-07-19', '#b2d280'),
(1690, 1, '2014-07-18', '#b2d280'),
(1689, 1, '2014-07-17', '#b2d280'),
(1688, 1, '2014-07-16', '#b2d280'),
(1687, 1, '2014-07-15', '#b2d280'),
(1686, 1, '2014-07-14', '#b2d280'),
(1685, 1, '2014-07-13', '#b2d280'),
(1684, 1, '2014-07-12', '#b2d280'),
(1683, 1, '2014-07-11', '#b2d280'),
(1682, 1, '2014-07-10', '#b2d280'),
(1681, 1, '2014-07-09', '#b2d280'),
(1680, 1, '2014-07-08', '#b2d280'),
(1679, 1, '2014-07-07', '#b2d280'),
(1678, 1, '2014-07-06', '#b2d280'),
(1677, 1, '2014-07-05', '#b2d280'),
(1676, 1, '2014-07-04', '#b2d280'),
(1675, 1, '2014-07-03', '#b2d280'),
(1674, 1, '2014-07-02', '#b2d280'),
(1673, 1, '2014-07-01', '#b2d280'),
(1672, 1, '2014-06-30', '#b2d280'),
(1671, 1, '2014-06-29', '#b2d280'),
(1670, 1, '2014-06-28', '#b2d280'),
(1669, 1, '2014-06-27', '#b2d280'),
(1668, 1, '2014-06-26', '#b2d280'),
(1667, 1, '2014-06-25', '#b2d280'),
(1666, 1, '2014-06-24', '#b2d280'),
(1665, 1, '2014-06-23', '#b2d280'),
(1664, 1, '2014-06-22', '#b2d280'),
(1663, 1, '2014-06-21', '#b2d280'),
(1662, 1, '2014-06-20', '#b2d280'),
(1661, 1, '2014-06-19', '#b2d280'),
(1660, 1, '2014-06-18', '#b2d280'),
(1659, 1, '2014-06-17', '#b2d280'),
(1658, 1, '2014-06-16', '#b2d280'),
(1657, 1, '2014-06-15', '#b2d280'),
(1656, 1, '2014-06-14', '#b2d280'),
(1655, 1, '2014-06-13', '#b2d280'),
(1654, 1, '2014-06-12', '#b2d280'),
(1653, 1, '2014-06-11', '#b2d280'),
(1652, 1, '2014-06-10', '#b2d280'),
(1651, 1, '2014-06-09', '#b2d280'),
(1650, 1, '2014-06-08', '#b2d280'),
(1649, 1, '2014-06-07', '#b2d280'),
(1648, 1, '2014-06-06', '#b2d280'),
(1647, 1, '2014-06-05', '#b2d280'),
(1646, 1, '2014-06-04', '#b2d280'),
(1645, 1, '2014-06-03', '#b2d280'),
(1644, 1, '2014-06-02', '#b2d280'),
(1643, 1, '2014-06-01', '#b2d280'),
(1642, 1, '2014-05-31', '#b2d280'),
(1641, 1, '2014-05-30', '#b2d280'),
(1640, 1, '2014-05-29', '#b2d280'),
(1639, 1, '2014-05-28', '#b2d280'),
(1638, 1, '2014-05-27', '#b2d280'),
(1637, 1, '2014-05-26', '#b2d280'),
(1636, 1, '2014-05-25', '#b2d280'),
(1635, 1, '2014-05-24', '#b2d280'),
(1634, 1, '2014-05-23', '#b2d280'),
(1633, 1, '2014-05-22', '#b2d280'),
(1632, 1, '2014-05-21', '#b2d280'),
(1631, 1, '2014-05-20', '#b2d280'),
(1630, 1, '2014-05-19', '#b2d280'),
(1629, 1, '2014-05-18', '#b2d280'),
(1628, 1, '2014-05-17', '#b2d280'),
(1627, 1, '2014-05-16', '#b2d280'),
(1626, 1, '2014-05-15', '#b2d280'),
(1625, 1, '2014-05-14', '#b2d280'),
(1624, 1, '2014-05-13', '#b2d280'),
(1623, 1, '2014-05-12', '#b2d280'),
(1622, 1, '2014-05-11', '#b2d280'),
(1621, 1, '2014-05-10', '#b2d280'),
(1620, 1, '2014-05-09', '#b2d280'),
(1619, 1, '2014-05-08', '#b2d280'),
(1618, 1, '2014-05-07', '#b2d280'),
(1617, 1, '2014-05-06', '#b2d280'),
(1616, 1, '2014-05-05', '#b2d280'),
(1615, 1, '2014-05-04', '#b2d280'),
(1614, 1, '2014-05-03', '#b2d280'),
(1613, 1, '2014-05-02', '#b2d280'),
(1612, 1, '2014-05-01', '#b2d280'),
(1611, 1, '2014-04-30', '#b2d280'),
(1610, 1, '2014-04-29', '#b2d280'),
(1609, 1, '2014-04-28', '#b2d280'),
(1608, 1, '2014-04-27', '#b2d280'),
(1607, 1, '2014-04-26', '#b2d280'),
(1606, 1, '2014-04-25', '#b2d280'),
(1605, 1, '2014-04-24', '#b2d280'),
(1604, 1, '2014-04-23', '#b2d280'),
(1603, 1, '2014-04-22', '#b2d280'),
(1602, 1, '2014-04-21', '#b2d280'),
(1601, 1, '2014-04-20', '#b2d280'),
(1600, 1, '2014-04-19', '#b2d280'),
(1599, 1, '2014-04-18', '#b2d280'),
(1598, 1, '2014-04-17', '#b2d280'),
(1597, 1, '2014-04-16', '#b2d280'),
(1596, 1, '2014-04-15', '#b2d280'),
(1595, 1, '2014-04-14', '#b2d280'),
(1594, 1, '2014-04-13', '#b2d280'),
(1593, 1, '2014-04-12', '#b2d280'),
(1592, 1, '2014-04-11', '#b2d280'),
(1591, 1, '2014-04-10', '#b2d280'),
(1590, 1, '2014-04-09', '#b2d280'),
(1589, 1, '2014-04-08', '#b2d280'),
(1588, 1, '2014-04-07', '#b2d280'),
(1587, 1, '2014-04-06', '#b2d280'),
(1586, 1, '2014-04-05', '#b2d280'),
(1585, 1, '2014-04-04', '#b2d280'),
(1584, 1, '2014-04-03', '#b2d280'),
(1583, 1, '2014-04-02', '#b2d280'),
(1582, 1, '2014-04-01', '#b2d280'),
(1581, 1, '2014-03-31', '#b2d280'),
(1580, 1, '2014-03-30', '#b2d280'),
(1579, 1, '2014-03-29', '#b2d280'),
(1578, 1, '2014-03-28', '#b2d280'),
(1577, 1, '2014-03-27', '#b2d280'),
(1576, 1, '2014-03-26', '#b2d280'),
(1575, 1, '2014-03-25', '#b2d280'),
(1574, 1, '2014-03-24', '#b2d280'),
(1573, 1, '2014-03-23', '#b2d280'),
(1572, 1, '2014-03-22', '#b2d280'),
(1571, 1, '2014-03-21', '#b2d280'),
(1570, 1, '2014-03-20', '#b2d280'),
(1569, 1, '2014-03-19', '#b2d280'),
(1568, 1, '2014-03-18', '#b2d280'),
(1567, 1, '2014-03-17', '#b2d280');

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `mod_faq`
--

CREATE TABLE `mod_faq` (
  `id` int(4) NOT NULL,
  `question_en` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `question_tr` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `answer_en` text COLLATE utf8_unicode_ci,
  `answer_tr` text COLLATE utf8_unicode_ci,
  `position` smallint(3) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='contains f.a.q. data';

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `mod_forum`
--

CREATE TABLE `mod_forum` (
  `id` int(11) NOT NULL,
  `title` varchar(150) DEFAULT NULL,
  `slug` varchar(150) DEFAULT NULL,
  `membership_id` varchar(30) NOT NULL DEFAULT '0',
  `access` varchar(30) DEFAULT NULL,
  `description` varchar(200) DEFAULT NULL,
  `icon` varchar(50) DEFAULT NULL,
  `publicpost` tinyint(1) NOT NULL DEFAULT '0',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `last_entry_date` datetime DEFAULT NULL,
  `last_entry_user_id` int(11) NOT NULL DEFAULT '0',
  `metakey_en` varchar(200) DEFAULT NULL,
  `metakey_tr` varchar(200) NOT NULL,
  `metadesc_en` varchar(200) DEFAULT NULL,
  `metadesc_tr` text,
  `active` tinyint(1) DEFAULT '0',
  `ordering` int(11) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Tablo döküm verisi `mod_forum`
--

INSERT INTO `mod_forum` (`id`, `title`, `slug`, `membership_id`, `access`, `description`, `icon`, `publicpost`, `created`, `modified`, `last_entry_date`, `last_entry_user_id`, `metakey_en`, `metakey_tr`, `metadesc_en`, `metadesc_tr`, `active`, `ordering`) VALUES
(1, 'General Topics', 'general-topics', '0', 'Public', 'Forums for general related discussions', 'users icon', 1, '2014-03-04 23:48:13', '2014-05-10 22:33:31', '2015-02-27 17:32:29', 0, '', '', 'Forums for general related discussions', 'Forums for general related discussions', 1, 1),
(2, 'CMS pro Modules', 'cms-pro-modules', '2,4,1', 'Membership', 'Forums for module related discussions', 'puzzle piece icon', 0, '2014-04-04 23:48:13', '2014-05-10 16:24:19', NULL, 0, '', '', '', '', 1, 2),
(3, 'CMS pro Plugins', 'cms-pro-plugins', '0', 'Registered', 'Forums for plugin related discussions', 'umbrella icon', 0, '2014-04-04 23:48:13', '2014-05-10 16:25:24', NULL, 0, '', '', '', '', 1, 3);

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `mod_forum_posts`
--

CREATE TABLE `mod_forum_posts` (
  `id` int(11) NOT NULL,
  `forum_id` int(11) NOT NULL DEFAULT '0',
  `thread_id` int(11) NOT NULL DEFAULT '0',
  `user_id` int(11) NOT NULL DEFAULT '0',
  `title` varchar(200) DEFAULT '',
  `body` text,
  `created` datetime DEFAULT NULL,
  `like` int(11) NOT NULL DEFAULT '0',
  `is_reply` tinyint(1) NOT NULL DEFAULT '0',
  `last_entry_date` datetime NOT NULL,
  `last_user_id` int(11) NOT NULL DEFAULT '0',
  `active` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Tablo döküm verisi `mod_forum_posts`
--

INSERT INTO `mod_forum_posts` (`id`, `forum_id`, `thread_id`, `user_id`, `title`, `body`, `created`, `like`, `is_reply`, `last_entry_date`, `last_user_id`, `active`) VALUES
(1, 1, 1, 1, 'Welcome to CMS pro Forum!', '&lt;p&gt;Lipa hoketo so ehe, em jam vato ator duona, ve orda foren kvadriliono tuj. Do vol anti multa kompreneble, dum gv timi eligi aligi. Jota alia senobjekta la ali, er horo alternativdemando nun, ireblakristnasko nk pri. Duto lumigi nelimigita eg kaj.&lt;/p&gt;', '2014-05-13 16:07:31', 1, 0, '2017-05-31 00:00:00', 1, 0),
(2, 1, 1, 0, 'test', '&lt;p&gt;123&lt;/p&gt;', '2015-02-27 17:32:29', 0, 0, '2017-05-31 00:00:00', 0, 0);

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `mod_forum_thread`
--

CREATE TABLE `mod_forum_thread` (
  `id` int(11) NOT NULL,
  `forum_id` int(11) NOT NULL DEFAULT '0',
  `title` varchar(150) DEFAULT NULL,
  `slug` varchar(150) NOT NULL,
  `user_id` int(11) NOT NULL DEFAULT '0',
  `totalposts` int(4) NOT NULL DEFAULT '0',
  `created` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Tablo döküm verisi `mod_forum_thread`
--

INSERT INTO `mod_forum_thread` (`id`, `forum_id`, `title`, `slug`, `user_id`, `totalposts`, `created`) VALUES
(1, 1, 'Welcome to CMS pro Forum!', 'welcome-to-cms-pro-forum', 1, 1, '2014-05-13 16:07:31');

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `mod_forum_users`
--

CREATE TABLE `mod_forum_users` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL DEFAULT '0',
  `thread_id` int(11) NOT NULL DEFAULT '0',
  `follow` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `mod_gallery_config`
--

CREATE TABLE `mod_gallery_config` (
  `id` int(6) NOT NULL,
  `title_en` varchar(100) DEFAULT NULL,
  `title_tr` varchar(100) NOT NULL,
  `folder` varchar(30) DEFAULT NULL,
  `cols` tinyint(4) NOT NULL DEFAULT '0',
  `watermark` tinyint(1) NOT NULL DEFAULT '0',
  `like` tinyint(1) NOT NULL DEFAULT '1',
  `created` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Tablo döküm verisi `mod_gallery_config`
--

INSERT INTO `mod_gallery_config` (`id`, `title_en`, `title_tr`, `folder`, `cols`, `watermark`, `like`, `created`) VALUES
(1, 'Fluid Gallery', 'Mobil Yakıt', 'demo', 3, 0, 1, '2014-04-19 01:28:10'),
(2, 'Portfolio Gallery', 'Portfolio Gallery', 'folio', 4, 0, 1, '2014-04-19 01:28:10'),
(3, 'Blog Demo Gallery', 'Blog Demo Gallery', 'blog', 3, 0, 1, '2014-05-05 17:37:49'),
(5, NULL, 'Blego', 'blego', 1, 0, 1, '2016-02-15 21:46:57'),
(6, NULL, 'Mine Mesh', 'mine-mesh-maden-takip-sistemi', 1, 0, 1, '2016-02-15 22:59:38'),
(7, NULL, 'Fuar Asist', 'fuar-asist-mobil-uygulama', 1, 0, 1, '2016-02-21 14:28:26'),
(8, NULL, 'OmniChannel', 'omnichannel', 1, 0, 1, '2016-02-28 23:47:26'),
(9, NULL, 'Veri Asist', 'veri-asist', 1, 0, 1, '2016-02-29 00:59:30'),
(10, NULL, 'NavBea', 'navbea', 1, 0, 1, '2016-03-05 00:26:55'),
(11, NULL, 'MüzeAsist', 'muzeasist', 1, 0, 1, '2016-03-05 00:44:42');

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `mod_gallery_images`
--

CREATE TABLE `mod_gallery_images` (
  `id` int(11) NOT NULL,
  `gallery_id` int(6) NOT NULL DEFAULT '0',
  `title_en` varchar(100) DEFAULT NULL,
  `title_tr` varchar(100) NOT NULL,
  `description_en` varchar(250) DEFAULT NULL,
  `description_tr` varchar(250) NOT NULL,
  `likes` int(6) NOT NULL DEFAULT '0',
  `thumb` varchar(100) DEFAULT NULL,
  `sorting` int(5) NOT NULL DEFAULT '0',
  `type` enum('i','v','') NOT NULL DEFAULT 'i' COMMENT 'Video or image'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Tablo döküm verisi `mod_gallery_images`
--

INSERT INTO `mod_gallery_images` (`id`, `gallery_id`, `title_en`, `title_tr`, `description_en`, `description_tr`, `likes`, `thumb`, `sorting`, `type`) VALUES
(64, 10, NULL, 'NAVBEA', NULL, 'Kapalı Alan Yönlendirme Sistemi', 0, 'IMG_F0535E-271850-AA4391-D4DACE-7A39F5-6A4830.jpg', 2, 'i'),
(67, 11, NULL, 'MüzeAsist', NULL, 'Yeni Nesil Müze Rehberi', 0, 'IMG_9D7BBA-229B2A-D5DA02-A12F4B-9C974C-E350C9.jpg', 2, 'i'),
(62, 10, NULL, 'NAVBEA', NULL, 'Kapalı Alan Yönlendirme Sistemi', 0, 'IMG_CBCD98-D8D4AB-8C087C-32B051-8C9FAC-692058.jpg', 4, 'i'),
(59, 10, NULL, 'NAVBEA', NULL, 'Kapalı Alan Yönlendirme Sistemi', 0, 'IMG_DAAAD1-F9E1FE-7B6958-3A6187-52DA77-8056E9.mp4', 1, 'v'),
(56, 8, NULL, 'Ibeacon ile Oyunlaştırma Deneyimi', NULL, 'OMNİ CHANNEL', 0, 'IMG_92B8CA-130FCF-865F75-DAEF59-0FA90C-D54BAF.jpg', 2, 'i'),
(58, 9, NULL, 'Kolay Yönetilebilir, gelişmiş içerik yönetim sistemi', NULL, 'VERİ ASİST', 0, 'IMG_5C5CAA-7E5342-E6981E-5AF9BD-199302-F548BE.jpg', 0, 'i'),
(54, 8, NULL, 'Eğlenceli + Eğitici + Çekici + Kazandırıcı', NULL, 'OMNİ CHANNEL', 0, 'IMG_CAB4F4-340BFF-1EA5C2-8C3D1F-1A45CA-119BE9.jpg', 3, 'i'),
(55, 8, NULL, 'Benzersiz + Yaratıcı + Yenilikçi + Sürükleyici', NULL, 'OMNİ CHANNEL', 0, 'IMG_CC2373-8F19BF-7452FA-33E191-DAA558-245368.jpg', 1, 'i'),
(50, 1, NULL, 'MOBİL YAKIT', NULL, 'AKARYAKIT SATIN ALIMINDA MOBİL KOLAYLIK', 0, 'IMG_E52DD0-3BA5AD-E29F5E-FF3FAC-C578CF-B724DF.jpg', 3, 'i'),
(51, 1, NULL, 'MOBİL YAKIT', NULL, 'AKARYAKIT SATIN ALIMINDA MOBİL KOLAYLIK', 0, 'IMG_05982B-5F8B9A-71C914-2B5E74-D0CAFE-506167.jpg', 2, 'i'),
(52, 1, NULL, 'MOBİL YAKIT', NULL, 'AKARYAKIT SATIN ALIMINDA MOBİL KOLAYLIK', 0, 'IMG_0E0E2B-4209BD-E20946-9518BC-D8EE78-24EF96.jpg', 1, 'i'),
(53, 8, NULL, 'SÜRÜKLEYİCİ ZİYARETÇİ DENEYİMİ', NULL, 'OMNİ CHANNEL', 0, 'IMG_5480FD-0ED002-949DBD-403232-ADEB54-86565C.jpg', 4, 'i'),
(12, 2, 'Robot Female', 'Robot Female', 'Hop duon tioma lumigi nv, if tiela poezio sezononomo fri, semi pleje lingvonomo ac unt.', 'Hop duon tioma lumigi nv, if tiela poezio sezononomo fri, semi pleje lingvonomo ac unt.', 320, 'IMG_455A88-783716-3B32A4-29CD0B-A96913-F119A1.jpg', 2, 'i'),
(13, 2, 'Under the Sea', 'Under the Sea', 'Hop duon tioma lumigi nv, if tiela poezio sezononomo fri, semi pleje lingvonomo ac unt.', 'Hop duon tioma lumigi nv, if tiela poezio sezononomo fri, semi pleje lingvonomo ac unt.', 1235, 'IMG_A5115A-C1B0FB-8C04DE-FD1F5B-C13C3D-AD4623.jpg', 4, 'i'),
(14, 2, 'Robot Female x2', 'Robot Female x2', 'Hop duon tioma lumigi nv, if tiela poezio sezononomo fri, semi pleje lingvonomo ac unt.', 'Hop duon tioma lumigi nv, if tiela poezio sezononomo fri, semi pleje lingvonomo ac unt.', 365, 'IMG_C8E100-9964CA-BFDA9D-A2E894-DF8D5D-EF823E.jpg', 5, 'i'),
(17, 2, 'Under the Bridge', 'Under the Bridge', 'Hop duon tioma lumigi nv, if tiela poezio sezononomo fri, semi pleje lingvonomo ac unt.', 'Hop duon tioma lumigi nv, if tiela poezio sezononomo fri, semi pleje lingvonomo ac unt.', 284, 'IMG_67350C-8202BB-71251A-151D61-3DA26A-92F6FB.jpg', 6, 'i'),
(18, 2, 'Taking a Break', 'Taking a Break', 'Hop duon tioma lumigi nv, if tiela poezio sezononomo fri, semi pleje lingvonomo ac unt.', 'Hop duon tioma lumigi nv, if tiela poezio sezononomo fri, semi pleje lingvonomo ac unt.', 85, 'IMG_E9C862-170677-D1463A-CD0D1D-18D316-808970.jpg', 3, 'i'),
(19, 3, 'Bridge View', 'Bridge View', 'Hop duon tioma lumigi nv, if tiela poezio sezononomo', 'Hop duon tioma lumigi nv, if tiela poezio sezononomo', 1, 'IMG_36298E-16DD1B-D9FA14-A9E6E4-AF31D0-CAE9B3.jpg', 0, 'i'),
(20, 3, 'On Vacation', 'On Vacation', 'Hop duon tioma lumigi nv, if tiela poezio sezononomo', 'Hop duon tioma lumigi nv, if tiela poezio sezononomo', 1, 'IMG_3DE7DE-749C65-FEB7DB-39FA54-E6AEBD-736796.jpg', 0, 'i'),
(21, 3, 'Like the View', 'Like the View', 'Hop duon tioma lumigi nv, if tiela poezio sezononomo', 'Hop duon tioma lumigi nv, if tiela poezio sezononomo', 1, 'IMG_B6E43E-9F10C4-0AA63F-AF4834-EB6B3E-A21C82.jpg', 0, 'i'),
(22, 3, 'Busy People', 'Busy People', 'Hop duon tioma lumigi nv, if tiela poezio sezononomo', 'Hop duon tioma lumigi nv, if tiela poezio sezononomo', 1, 'IMG_F1160A-DD9989-BC47AC-B330B0-480612-7F9833.jpg', 0, 'i'),
(23, 3, 'Happy Times', 'Happy Times', 'Hop duon tioma lumigi nv, if tiela poezio sezononomo', 'Hop duon tioma lumigi nv, if tiela poezio sezononomo', 1, 'IMG_7A9FCD-23DA22-ADE5E6-BA438F-42E4EF-BF7655.jpg', 0, 'i'),
(24, 3, 'Sunset', 'Sunset', 'Hop duon tioma lumigi nv, if tiela poezio sezononomo', 'Hop duon tioma lumigi nv, if tiela poezio sezononomo', 1, 'IMG_0EFA7B-5F431A-53A024-4B0DDF-608DA7-80B718.jpg', 0, 'i'),
(25, 3, 'Sharing', 'Sharing', 'Hop duon tioma lumigi nv, if tiela poezio sezononomo', 'Hop duon tioma lumigi nv, if tiela poezio sezononomo', 0, 'IMG_736F9B-319B96-48FE78-64B225-8454B8-980E91.jpg', 0, 'i'),
(26, 3, 'Modern Life', 'Modern Life', 'Hop duon tioma lumigi nv, if tiela poezio sezononomo', 'Hop duon tioma lumigi nv, if tiela poezio sezononomo', 0, 'IMG_3C99BC-D296DE-789B5B-D79029-81BF55-20F083.jpg', 0, 'i'),
(36, 2, ' Video', 'KODAR', 'Playing', 'KODAR', 0, 'IMG_9DD0DC-E878D7-AEC33D-751612-506D70-A790AF.mp4', 1, 'v'),
(38, 5, NULL, 'BLEGO', NULL, 'AIRPORT', 0, 'IMG_5A5F85-833B1F-12CEE9-2BE552-ADF6C2-E648B4.mp4', 2, 'v'),
(39, 5, NULL, 'BLEGO', NULL, 'BEACON TEKNOLOJİSİ', 0, 'IMG_F8281C-7A7B46-A645C4-88896A-EFAB26-9616B1.mp4', 1, 'v'),
(40, 6, NULL, 'Mine Mesh', NULL, 'Yeraltı Personel Takip Sistemi', 0, 'IMG_3CF434-537C0B-EA65C8-FB4107-FA660C-EDCF00.mp4', 0, 'v'),
(42, 6, NULL, 'Mine Mesh', NULL, 'Yeraltı Personel Takip Sistemi', 0, 'IMG_D75222-9D0599-3DD082-4168A2-594035-9905C2.jpg', 0, 'i'),
(65, 11, NULL, 'MüzeAsist', NULL, 'Yeni Nesil Müze Rehberi', 0, 'IMG_C2ECDD-67B7AB-3178C0-443143-6C4FDD-E5783B.mp4', 1, 'v'),
(46, 7, NULL, 'FUAR ASİST', NULL, 'Akıllı Fuar Teknolojisi', 0, 'IMG_940F7A-7DEAFD-A7B506-FD8AB7-FB58BC-E2726D.jpg', 2, 'i'),
(47, 7, NULL, 'FUAR ASİST', NULL, 'Akıllı Fuar Teknolojisi', 0, 'IMG_5B73B3-69BDC0-7A7BFB-56E6CF-A881F5-02B499.jpg', 1, 'i');

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `mod_gmaps`
--

CREATE TABLE `mod_gmaps` (
  `id` int(11) NOT NULL,
  `name` varchar(150) NOT NULL,
  `lat` decimal(10,6) NOT NULL,
  `lng` decimal(10,6) NOT NULL,
  `zoom` smallint(6) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Tablo döküm verisi `mod_gmaps`
--

INSERT INTO `mod_gmaps` (`id`, `name`, `lat`, `lng`, `zoom`) VALUES
(3, 'Kodar', '40.967442', '29.108114', 15);

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `mod_news`
--

CREATE TABLE `mod_news` (
  `id` int(11) NOT NULL,
  `cid` int(11) NOT NULL DEFAULT '0',
  `uid` int(11) NOT NULL DEFAULT '0',
  `membership_id` varchar(20) NOT NULL DEFAULT '0',
  `title_en` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `title_tr` varchar(100) NOT NULL,
  `slug` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `short_desc_en` text CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `short_desc_tr` text,
  `body_en` mediumtext,
  `body_tr` mediumtext,
  `thumb` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `caption_en` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `caption_tr` varchar(100) NOT NULL,
  `filename` varchar(50) DEFAULT NULL,
  `gallery` int(4) NOT NULL DEFAULT '0',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `expire` datetime DEFAULT NULL,
  `tags_en` varchar(120) NOT NULL DEFAULT '0',
  `tags_tr` varchar(100) NOT NULL,
  `hits` int(6) DEFAULT '0',
  `show_author` tinyint(1) NOT NULL DEFAULT '1',
  `show_ratings` tinyint(1) NOT NULL DEFAULT '1',
  `show_comments` tinyint(1) NOT NULL DEFAULT '1',
  `show_sharing` tinyint(1) NOT NULL DEFAULT '1',
  `show_created` tinyint(1) NOT NULL DEFAULT '1',
  `show_like` tinyint(1) NOT NULL DEFAULT '1',
  `layout` tinyint(1) NOT NULL DEFAULT '1',
  `rating` varchar(10) NOT NULL DEFAULT '0',
  `rate_number` varchar(10) NOT NULL DEFAULT '0',
  `like_up` int(11) NOT NULL DEFAULT '0',
  `like_down` int(11) NOT NULL DEFAULT '0',
  `metakey_en` varchar(200) DEFAULT NULL,
  `metakey_tr` varchar(200) NOT NULL,
  `metadesc_en` text,
  `metadesc_tr` text,
  `is_user` tinyint(1) NOT NULL DEFAULT '0',
  `active` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Tablo döküm verisi `mod_news`
--

INSERT INTO `mod_news` (`id`, `cid`, `uid`, `membership_id`, `title_en`, `title_tr`, `slug`, `short_desc_en`, `short_desc_tr`, `body_en`, `body_tr`, `thumb`, `caption_en`, `caption_tr`, `filename`, `gallery`, `created`, `modified`, `expire`, `tags_en`, `tags_tr`, `hits`, `show_author`, `show_ratings`, `show_comments`, `show_sharing`, `show_created`, `show_like`, `layout`, `rating`, `rate_number`, `like_up`, `like_down`, `metakey_en`, `metakey_tr`, `metadesc_en`, `metadesc_tr`, `is_user`, `active`) VALUES
(1, 6, 1, '0', '', 'Yeni Nesil Yazar Kasa Pos Nedir ?', 'yeni-nesil-yazar-kasa-pos-nedir', NULL, 'Gelir İdaresi Başkanlığı (GİB) ve Maliye Bakanlığı, Kayıt Dışı Ekonomi ile Mücadele Eylem Stratejisi kapsamında, &#039;yeni nesil yazar kasa pos&#039; sistemine geçilmesi ile ilgili bir karar almıştır.\r\n\r\nAlınan bu karar; Gelir İdaresi Başkanlığı Katma Değer Vergisi Mükellefleri&#039;nin Ödeme Kaydedici Cihazları Kullanmaları Mecburiyeti Hakkında Kanunla İlgili Genel Tebliğ (Seri No: 69) ile 21 Mart 2012 tarihinde, Resmi Gazete&#039;de yayınlanmıştır.', NULL, '&lt;p&gt;Gelir İdaresi Başkanlığı (GİB) ve Maliye Bakanlığı, Kayıt Dışı Ekonomi ile Mücadele Eylem Stratejisi kapsamında, ‘yeni nesil yazar kasa pos’ sistemine geçilmesi ile ilgili bir karar almıştır.&lt;/p&gt;&lt;p&gt;&lt;br&gt;Alınan bu karar; Gelir İdaresi Başkanlığı Katma Değer Vergisi Mükellefleri’nin Ödeme Kaydedici Cihazları Kullanmaları Mecburiyeti Hakkında Kanunla İlgili Genel Tebliğ (Seri No: 69) ile 21 Mart 2012 tarihinde, Resmi Gazete’de yayınlanmıştır.&lt;/p&gt;&lt;p&gt;&lt;br&gt;Bu tebliğ, aslında eski tip yazarkasaların hurdaya ayrılmasını zorunlu hale getirerek internet bağlantılı yeni nesil ‘Ödeme Kaydedici Cihazlar’ın (ÖKC) devreye alınmasını sağlamaktaydı. Böylelikle yazar kasa ve kredi kartı POS cihazı özellikleri aynı cihazda toplanacaktı. Kararın asıl amacı; vergi kaçaklarını önlemek, adil olarak kayıtların tutulmasını, fiş ve fatura bilgilerinin GİB’e iletilmesiyle kontrollerin elektronik ortamda ve kısa sürede yapılmasını sağlamaktı.&amp;nbsp;&lt;b&gt;YENİ NESİL EFT YAZARKASA,&lt;/b&gt;&amp;nbsp;işte bu böyle doğmuş oldu.&lt;/p&gt;&lt;p&gt;&lt;br&gt;Gelir İdaresi Başkanlığı’nın isteği olan akıllı yeni nesil yazarkasalar, ÖKC &amp;nbsp;ile banka POS cihazları birleşerek tek bir ürün olacak, dolayısıyla; her bir satış karşılığında ödeme tipi ne olursa olsun (kredi kartı, nakit) hem slip fişi hem de mali fiş üretecekti.&lt;/p&gt;&lt;p&gt;&lt;br&gt;Ancak böylesine bütünleşik çalışacak bir cihaz, henüz piyasada yoktu. Yazarkasa firmaları ve bankaların kısa bir sürede gerekli ArGe çalışmalarını yapması, mevcut yazarkasalardan daha fazla teknik özelliğe ve yüksek güvenirliğe sahip, yeni bir cihaz üretmesi gerekiyordu.&lt;/p&gt;&lt;p&gt;&lt;br&gt;Yapılan çalışmalarla hedeflenen; akıllı yeni nesil yazarkasaların,&amp;nbsp; çoklu banka POS yazılımı ile entegre olması, IP tabanlı iletişim yapısı sayesinde de sürekli olarak Maliye Bakanlığı ve bankalar arasındaki veri alışverişi sağlayabilmesiydi. Bu sistemle birlikte her cihaz, tek bir elektronik yapı altında toplanacak, senkronize&amp;nbsp; ve&amp;nbsp; mali denetim adı altında sorunsuz olarak çalışarak iş yerlerine zaman ve maliyet kazandıracaktı. Üstelik, ‘taşınabilirlik’ özelliği sayesinde, iş yeri dışında da kullanımı da mümkün olacaktı.&lt;/p&gt;&lt;p&gt;&lt;br&gt;Yazarkasa firmaları ile bankalar yoğun bir ArGe süreci sonunda; ‘yazarkasa firması, banka ve Gelir İdaresi Bakanlığı’ ile eş zamanlı çalışacak&amp;nbsp;&lt;b&gt;YENİ NESİL YAZAR KASA&amp;nbsp;&lt;/b&gt;ürünlerini sektöre sundular. Bu arada bilinen &amp;nbsp;yazarkasa markalarına ek olarak yeni markalar da sektöre girdi.&lt;/p&gt;&lt;p&gt;&lt;br&gt;Gelir İdaresi Bakanlığı, mükelleflere, akıllı yeni nesil yazarkasalara geçiş için son tarihi 1 Ocak 2016 olarak belirledi ve sektör ayırımı olmaksızın bu tarih itibariyle yeni nesil yazar kasa pos&amp;nbsp;kullanımını zorunlu hale getirdi.&lt;/p&gt;', 'IMG_D1B3D8-2E64DA-38DBAB-D334F2-BF8850-57D832.jpg', NULL, 'görsel manşet', NULL, 0, '2016-09-06 00:00:00', '2016-09-06 15:09:26', NULL, '0', '1', 155, 1, 1, 1, 1, 1, 1, 1, '0', '0', 0, 0, NULL, 'posmaks,kodar,', NULL, 'Gelir İdaresi Başkanlığı (GİB) ve Maliye Bakanlığı, Kayıt Dışı Ekonomi ile Mücadele Eylem Stratejisi kapsamında, &#039;yeni nesil yazar kasa pos&#039; sistemine geçilmesi ile ilgili bir karar almıştır.\r\n\r\nAlınan bu karar; Gelir İdaresi Başkanlığı Katma Değer Vergisi Mükellefleri&#039;nin Ödeme Kaydedici Cihazları Kullanmaları Mecburiyeti Hakkında Kanunla İlgili Genel Tebliğ (Seri No: 69) ile 21 Mart 2012 tarihinde, Resmi Gazete&#039;de yayınlanmıştır.', 0, 1),
(2, 6, 1, '0', '', 'Türkiye de Restaurant Sektörünün Gelişimi', 'turkiye-de-restaurant-sektorunun-gelisimi', NULL, 'Restoran sektöründe amaç sadece para kazanmak olmamalı, müşterilerin kalıcılığı da bu sektörün bir parçası olmalıdır. Amaç sadece para kazanmak olursa, uzun vadede firmalar sektörde kalıcılığını koruyamaz hale gelebilirler veu risklerin farkında olan gerçek ya da tüzel kişiler, sektöre adım atırken bu konuya daha ciddi bir şekilde yaklaşıyorlar. Bu sayede sektörde karşılaşabilecekleri problemleri erkenden çözmüş oluyor ve en az risk ile sektöre başlamış oluyorlar.​\r\nKişilerin bu dikkatli davranışları sonucu, restoran sektörü de hızla gelişmekte ve teknolojik olarak ileriye gitmektedir. İleriki yıllarda bu konu hakkındaki görüşlerimizi tekrar gözden geçirdiğimizde kar marjının da artmış olduğunu göreceğiz. Çünkü gelecekte sıçrama yapması beklenen sektörler arasında gıda sektörü de yer almaktadır.', NULL, '&lt;p&gt;Türkiye&#039;de restoran işletmeciliği gün geçtikçe gelişen bir sektör olmaya devam etmekte. Son&amp;nbsp;teknolojiyi yakalayan restoranlar hızla artmaya devam etmektedir. Artık restoranlar günümüz&amp;nbsp;standartlarını yakalayarak kurumsal bir yapıya bürünmektedir.&lt;/p&gt;&lt;p&gt;Restoran sektöründe amaç sadece para kazanmak olmamalı, müşterilerin kalıcılığı da bu&amp;nbsp;sektörün bir parçası olmalıdır. Amaç sadece para kazanmak olursa, uzun vadede firmalar&amp;nbsp;sektörde kalıcılığını koruyamaz hale gelebilirler veu risklerin farkında olan gerçek ya da tüzel&amp;nbsp;kişiler, sektöre adım atırken bu konuya daha ciddi bir şekilde yaklaşıyorlar. Bu sayede&amp;nbsp;sektörde karşılaşabilecekleri problemleri erkenden çözmüş oluyor ve en az risk ile sektöre&amp;nbsp;başlamış oluyorlar.&lt;/p&gt;&lt;p&gt;Kişilerin bu dikkatli davranışları sonucu, restoran sektörü de hızla&amp;nbsp;gelişmekte ve teknolojik olarak ileriye gitmektedir. İleriki yıllarda bu konu hakkındaki&amp;nbsp;görüşlerimizi tekrar gözden geçirdiğimizde kar marjının da artmış olduğunu göreceğiz.&amp;nbsp;Çünkü gelecekte sıçrama yapması beklenen sektörler arasında gıda sektörü de yer&amp;nbsp;almaktadır.&lt;/p&gt;&lt;p&gt;Son dönemlerde büyük girişimciler cafe,&amp;nbsp;restoran&amp;nbsp;ve konuk ağırlama sektörlerine olan&amp;nbsp;ilgilerini yaptıkları yatırımlarla göstermekteler.&lt;/p&gt;&lt;p&gt;Bu bilgilere bakarak, sektörün atılım içinde olduğunu ve daha da gelişeceğini söyleyebiliriz.&amp;nbsp;Restoran sektörüne girmeden önce araştırmaların iyi yapılması gerekmektedir.&lt;/p&gt;&lt;p&gt;Restoran&amp;nbsp;işletmelerinde hedefler iyi belirlenmelidir. Bu hedeflerden biri de teknolojik standarlara uyum&amp;nbsp;olmalıdır. Günümüzde büyüyen şirketlere baktığımızda teknoloji araştırmalarına önem&amp;nbsp;verdiklerini görmekteyiz.&lt;/p&gt;', 'IMG_CF7E8D-0D22CB-F147A2-A33178-D415A7-CFD72F.jpg', NULL, '', NULL, 0, '2016-09-06 14:30:00', NULL, NULL, '0', '5,6,7,8,9', 159, 1, 1, 1, 1, 1, 1, 1, '0', '0', 0, 0, NULL, 'türkiye de restarutant sektörünün gelişimi,restaurant sektörü,restaurantlar,posmaks,kodar,', NULL, 'Restoran sektöründe amaç sadece para kazanmak olmamalı, müşterilerin kalıcılığı da bu sektörün bir parçası olmalıdır. Amaç sadece para kazanmak olursa, uzun vadede firmalar sektörde kalıcılığını koruyamaz hale gelebilirler veu risklerin farkında olan gerçek ya da tüzel kişiler, sektöre adım atırken bu konuya daha ciddi bir şekilde yaklaşıyorlar. Bu sayede sektörde karşılaşabilecekleri problemleri erkenden çözmüş oluyor ve en az risk ile sektöre başlamış oluyorlar.​\r\nKişilerin bu dikkatli davranışları sonucu, restoran sektörü de hızla gelişmekte ve teknolojik olarak ileriye gitmektedir. İleriki yıllarda bu konu hakkındaki görüşlerimizi tekrar gözden geçirdiğimizde kar marjının da artmış olduğunu göreceğiz. Çünkü gelecekte sıçrama yapması beklenen sektörler arasında gıda sektörü de yer almaktadır.', 0, 1),
(3, 6, 1, '0', '', 'Bulut Tabanlı POS Sistemi', 'bulut-tabanli-pos-sistemi', NULL, '​Bulut tabanlı POS sistemi, gelişen teknolojinin daha ileri seviyelere taşındığını gösteren yeni bir teknoloji olarak karşımıza çıkmaktadır. Restoranlarda kullanılan ve büyük ayrıcalıklarla kolaylıklar sağlayan bu sistem gün geçtikçe klasik tabanlı pos sistemlerinden ayrılmaya başlamıştır.', NULL, '&lt;p&gt;Bulut tabanlı POS sistemi, gelişen teknolojinin daha ileri seviyelere taşındığını gösteren yeni bir teknoloji olarak karşımıza çıkmaktadır. Restoranlarda kullanılan ve büyük ayrıcalıklarla kolaylıklar sağlayan bu sistem gün geçtikçe klasik tabanlı pos sistemlerinden ayrılmaya başlamıştır.&lt;/p&gt;&lt;p&gt;POS sistemleri teknolojisi ortaya ilk çıktığında, hizmet sektöründe bu sistemi kullanan birçok restoran ve kafe tarafından ilgiyle karşılanmıştı. Bunun sebeplerinden biri de sistemin hizmet kalitesini yükseltiyor olmasıydı. POS sistemlerinde her geçen gün hizmette kalitenin arttırılmasına katkı sağlayacak ve insan hayatını kolaylaştıracak teknolojiler üretiliyor.&lt;/p&gt;&lt;p&gt;Bulut tabanlı POS sistemi olarak adlandırılan bu yeni teknoloji, gelişiyle beraber hizmet kalitelerini daha da yükseltmesi ve işlerinizi kolaylaştırmasıyla restoran ve kafelerin vazgeçilmezleri haline gelecek gibi görünüyor.&amp;nbsp;&lt;/p&gt;', 'IMG_532C1D-395B7C-F405D1-F7BD68-0BD455-BABA39.png', NULL, '', NULL, 0, '2016-09-06 00:00:00', '2016-09-06 15:22:38', NULL, '0', '3', 155, 1, 1, 1, 1, 1, 1, 1, '0', '0', 0, 0, NULL, 'posmaks,kodar,bulut tabanlı pos sistemi,pos sistemi', NULL, '​Bulut tabanlı POS sistemi, gelişen teknolojinin daha ileri seviyelere taşındığını gösteren yeni bir teknoloji olarak karşımıza çıkmaktadır. Restoranlarda kullanılan ve büyük ayrıcalıklarla kolaylıklar sağlayan bu sistem gün geçtikçe klasik tabanlı pos sistemlerinden ayrılmaya başlamıştır.', 0, 1),
(4, 6, 1, '0', '', 'Dünya&#039;da Restaurant Otomasyonu', 'dunyada-restaurant-otomasyonu', NULL, 'Dünya&#039;da restaurant otomasyonu  yazılımları, ülkemizde ki teknik işleyişle birlikte her geçen gün kendisini geliştirmeyi başarmaktadır. ​Sipariş ve adisyon  sistemlerinin çok daha hızlandırılması ve garsonların akıcı bir verimlilikle kullanılmasını sağlayan bu yazılımlar, müşterilerle kurulan iletişimde çok daha nitelikli hizmetler sunmanıza vesile olacaktır.', NULL, '&lt;p&gt;Dünya&#039;da restaurant otomasyonu&amp;nbsp; yazılımları, ülkemizde ki teknik işleyişle birlikte her geçen gün kendisini geliştirmeyi başarmaktadır.&amp;nbsp;Sipariş ve adisyon&amp;nbsp; sistemlerinin çok daha hızlandırılması ve garsonların akıcı bir verimlilikle kullanılmasını sağlayan bu yazılımlar, müşterilerle kurulan iletişimde çok daha nitelikli hizmetler sunmanıza vesile olacaktır.&lt;/p&gt;&lt;p&gt;Mevcut sisteminlerin işleyişinde müşterilerinizin siparişleri merkezi bir terminal veya dokunmatik ekranlı el bilgisayarları ile toplanır. Toplanan siparişler ise RF haberleşme yöntemi ile anında ana bilgisayar sistemine transfer edilir. Gerçekleştirilmekte olan bu aktarma dahilinde de ana bilgisayar sisteminde yiyecek, içecek veya tanımlanmış departmanlar olarak ayrılan sipariş bilgileri, ilgili birimlerde bulunan printerlerden sipariş olarak otomatik bir biçimde yazdırılır. Genel açıdan ele aldığımızda oldukça sistemli ve kaliteli bir işleyişe sahip olan bu yazılımlar sayesinde, müşterileriniz ile kuracağınız iletişimi her daim sağlıklı tutabilirsiniz. &lt;/p&gt;', 'IMG_3DD664-F373CF-CE5020-EA9E48-122F9D-8C3219.png', NULL, '', NULL, 0, '2016-09-08 18:00:00', NULL, NULL, '0', '5,6,12,13', 152, 1, 1, 1, 1, 1, 1, 1, '0', '0', 0, 0, NULL, 'posmaks,kodar,dünyada restaurant otomasyonu,restaurant otomasyonu', NULL, 'Dünya&#039;da restaurant otomasyonu  yazılımları, ülkemizde ki teknik işleyişle birlikte her geçen gün kendisini geliştirmeyi başarmaktadır. ​Sipariş ve adisyon  sistemlerinin çok daha hızlandırılması ve garsonların akıcı bir verimlilikle kullanılmasını sağlayan bu yazılımlar, müşterilerle kurulan iletişimde çok daha nitelikli hizmetler sunmanıza vesile olacaktır.', 0, 1),
(5, 6, 1, '0', '', 'POS VE YAZARKASALAR AKILLANDIKÇA VERGİ DENETİMLERİ KOLAYLAŞACAK', 'pos-ve-yazarkasalar-akillandikca-vergi-denetimleri', NULL, '\'Yeni Nesil Ödeme Kaydedici Cihazlar\' ile şirket bilgilerinin banka kayıtları gibi doğrudan ilgili mali birimlere gönderilerek vergi denetimlerinin kolaylaştırılması planlanıyor. Franchising sektörünün 200 bin kişi istihdam ile geçen yıl 43 milyar dolarlık ticaret hacmine ulaştığını söyleyen UFRAD Yönetim Kurulu Başkanı Dr. Mustafa Aydın, 2017 yılı hedeflerinin 50 milyar doları aşmak olduğunu ifade etti. Bu sektörde çalışanların teknolojik yeniliklere adapte olabildikleri sürece ayakta kalabileceklerini aksi halde yok olmaya mahkum olduklarını vurguladı.', NULL, '&lt;div&gt;’Yeni Nesil Ödeme Kaydedici Cihazlar’ ile şirket bilgilerinin banka kayıtları gibi doğrudan ilgili mali birimlere gönderilerek vergi denetimlerinin kolaylaştırılması planlanıyor. Franchising sektörünün 200 bin kişi istihdam ile geçen yıl 43 milyar dolarlık ticaret hacmine ulaştığını söyleyen UFRAD Yönetim Kurulu Başkanı Dr. Mustafa Aydın, 2017 yılı hedeflerinin 50 milyar doları aşmak olduğunu ifade etti. Bu sektörde çalışanların teknolojik yeniliklere adapte olabildikleri sürece ayakta kalabileceklerini aksi halde yok olmaya mahkum olduklarını vurguladı.&amp;nbsp;&lt;/div&gt;&lt;div&gt;&quot;TÜRKİYE , YENİ NESİL CİHAZLARIN UYGULAMASINDA ÖNCÜ ÜLKELER ARASINDA&quot;&lt;/div&gt;&lt;div&gt;&amp;nbsp;Seminerde Tunç Sarıbay ve Alper Uysaler, Türkiye’nin yeni nesil cihazlarda en son teknolojiyi yakalan öncü ülkelerin başında geldiğinin söyleyerek yeni nesil teknoloji ile birlikte, gelir idaresine bilgilerin aktarılması, kazanç ve vergi denetimlerinin kontrolü daha sistemli olacağını vurguladılar. Yazarkasaların, şirketlerde banka kayıtları gibi, detaylı bilgileri düzenli olarak ilgili mali birimlere iletecekleri bilgisi de verildi.&amp;nbsp;&lt;/div&gt;', 'IMG_145736-7F8F82-563D39-E56865-5DD20B-F1550A.jpg', NULL, '', NULL, 0, '2016-09-08 18:30:00', NULL, NULL, '0', '5,6,14,15', 181, 1, 1, 1, 1, 1, 1, 1, '0', '0', 0, 0, NULL, 'posmaks,kodar,akıllı pos ve yazar kasalar,pos ve yazarkasalar', NULL, '\'Yeni Nesil Ödeme Kaydedici Cihazlar\' ile şirket bilgilerinin banka kayıtları gibi doğrudan ilgili mali birimlere gönderilerek vergi denetimlerinin kolaylaştırılması planlanıyor. Franchising sektörünün 200 bin kişi istihdam ile geçen yıl 43 milyar dolarlık ticaret hacmine ulaştığını söyleyen UFRAD Yönetim Kurulu Başkanı Dr. Mustafa Aydın, 2017 yılı hedeflerinin 50 milyar doları aşmak olduğunu ifade etti. Bu sektörde çalışanların teknolojik yeniliklere adapte olabildikleri sürece ayakta kalabileceklerini aksi halde yok olmaya mahkum olduklarını vurguladı.', 0, 1);

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `mod_news_categories`
--

CREATE TABLE `mod_news_categories` (
  `id` int(6) NOT NULL,
  `parent_id` int(4) NOT NULL DEFAULT '0',
  `name_en` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `name_tr` varchar(100) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `description_en` text NOT NULL,
  `description_tr` text,
  `icon` varchar(100) DEFAULT NULL,
  `perpage` tinyint(3) NOT NULL DEFAULT '10',
  `layout` tinyint(1) NOT NULL DEFAULT '1',
  `position` int(4) NOT NULL DEFAULT '1',
  `metakey_en` varchar(200) DEFAULT NULL,
  `metakey_tr` varchar(200) NOT NULL,
  `metadesc_en` text,
  `metadesc_tr` text,
  `active` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Tablo döküm verisi `mod_news_categories`
--

INSERT INTO `mod_news_categories` (`id`, `parent_id`, `name_en`, `name_tr`, `slug`, `description_en`, `description_tr`, `icon`, `perpage`, `layout`, `position`, `metakey_en`, `metakey_tr`, `metadesc_en`, `metadesc_tr`, `active`) VALUES
(6, 0, '', 'HABERLER', 'kodar-haberleri', '', 'Kodar şirketler topluluğu haberleri', 'umbrella icon', 10, 4, 18, '', 'kodar, kodar bilişim, kodar teknoloji', '', 'Kodar şirketler topluluğu proje, ürün ve teknoloji haberleri', 1),
(7, 0, '', 'BLOG', 'teknoloji', '', 'Dünya&#039;daki teknoloji gelişmeleri ile ilgili haberler.', 'plane icon', 10, 4, 19, '', 'teknoloji, dünya', '', 'Dünya&#039;daki teknoloji gelişmeleri ile ilgili haberler.', 1);

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `mod_news_comments`
--

CREATE TABLE `mod_news_comments` (
  `id` int(11) NOT NULL,
  `parent_id` int(11) NOT NULL DEFAULT '0',
  `artid` int(11) NOT NULL DEFAULT '0',
  `username` varchar(24) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `user_id` int(11) NOT NULL DEFAULT '0',
  `email` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `body` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `www` varchar(220) DEFAULT NULL,
  `created` datetime NOT NULL,
  `ip` varchar(16) DEFAULT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `mod_news_related_categories`
--

CREATE TABLE `mod_news_related_categories` (
  `aid` int(11) NOT NULL,
  `cid` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Tablo döküm verisi `mod_news_related_categories`
--

INSERT INTO `mod_news_related_categories` (`aid`, `cid`) VALUES
(1, 6),
(2, 6),
(3, 6),
(4, 6),
(5, 6),
(14, 13),
(15, 7),
(42, 6);

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `mod_news_tags`
--

CREATE TABLE `mod_news_tags` (
  `aid` int(11) NOT NULL DEFAULT '0',
  `tid` int(11) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Tablo döküm verisi `mod_news_tags`
--

INSERT INTO `mod_news_tags` (`aid`, `tid`) VALUES
(2, 7),
(2, 6),
(2, 5),
(4, 12),
(1, 1),
(2, 8),
(2, 9),
(4, 6),
(4, 5),
(3, 3),
(4, 13),
(5, 5),
(5, 6),
(5, 14),
(5, 15);

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `mod_portfolio`
--

CREATE TABLE `mod_portfolio` (
  `id` int(6) NOT NULL,
  `cid` int(6) NOT NULL DEFAULT '0',
  `slug` varchar(100) DEFAULT NULL,
  `title_en` varchar(100) DEFAULT NULL,
  `title_tr` varchar(100) NOT NULL,
  `short_desc_en` text,
  `short_desc_tr` text,
  `detail_en` text,
  `detail_tr` text,
  `body_en` text,
  `body_tr` text,
  `result_en` text,
  `result_tr` text,
  `www` varchar(100) DEFAULT NULL,
  `location` varchar(100) DEFAULT NULL,
  `client` varchar(100) DEFAULT NULL,
  `thumb` varchar(60) DEFAULT NULL,
  `gallery` smallint(4) NOT NULL DEFAULT '0',
  `metakey_en` varchar(200) DEFAULT NULL,
  `metakey_tr` varchar(200) NOT NULL,
  `metadesc_en` text,
  `metadesc_tr` text,
  `created` date DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Tablo döküm verisi `mod_portfolio`
--

INSERT INTO `mod_portfolio` (`id`, `cid`, `slug`, `title_en`, `title_tr`, `short_desc_en`, `short_desc_tr`, `detail_en`, `detail_tr`, `body_en`, `body_tr`, `result_en`, `result_tr`, `www`, `location`, `client`, `thumb`, `gallery`, `metakey_en`, `metakey_tr`, `metadesc_en`, `metadesc_tr`, `created`) VALUES
(1, 3, 'super-car', 'Super Car', 'Super Car', 'Sed vitae nisi sapien, quis venenatis magna. Class aptent taciti sociosqu ad litora torquent per conubia nostr per inceptos himenaeos. ', 'Sed vitae nisi sapien, quis venenatis magna. Class aptent taciti sociosqu ad litora torquent per conubia nostr per inceptos himenaeos. ', '&lt;ul&gt;\r\n	\r\n&lt;li&gt;Client: &lt;a href=&quot;#&quot;&gt;Ambient Space Enterprise&lt;/a&gt;&lt;/li&gt;	\r\n&lt;li&gt;Website: &lt;a href=&quot;#&quot;&gt;www.domainname.com&lt;/a&gt;&lt;/li&gt;	\r\n&lt;li&gt;Date: 29.05.2011&lt;/li&gt;&lt;/ul&gt;\r\n&lt;div&gt;\r\n	&lt;h4&gt;About Idea&lt;/h4&gt;Sed vitae nisi sapien, quis venenatis magna, class aptent tacitisciosqu ad litora torquent per conubia nostr per inceptos himenaDuis in erat sit amet sem tincidi fringilla ut a risus. Cras commocommodo nequt facilisis risus aliquam id.&lt;/div&gt;', '&lt;ul&gt;\r\n	\r\n&lt;li&gt;Client: &lt;a href=&quot;#&quot;&gt;Ambient Space Enterprise&lt;/a&gt;&lt;/li&gt;	\r\n&lt;li&gt;Website: &lt;a href=&quot;#&quot;&gt;www.domainname.com&lt;/a&gt;&lt;/li&gt;	\r\n&lt;li&gt;Date: 29.05.2011&lt;/li&gt;&lt;/ul&gt;\r\n&lt;div&gt;\r\n	&lt;h4&gt;About Idea&lt;/h4&gt;Sed vitae nisi sapien, quis venenatis magna, class aptent tacitisciosqu ad litora torquent per conubia nostr per inceptos himenaDuis in erat sit amet sem tincidi fringilla ut a risus. Cras commocommodo nequt facilisis risus aliquam id.&lt;/div&gt;', '&lt;h2&gt;Creators Review&lt;/h2&gt;&lt;p&gt;Class aptent taciti sociosqu ad litora torquent per conubia nostr per inceptos himenaeos. Duis in erat sit amet sem tincidi fringilla ut a risus. Cras commodo commodonequt facilisis risus aliquam id. Mauris nec adipiscing quam. Aenean congue, mi a vehicula pharetra, lorem felis maximusj&lt;/p&gt;', '&lt;h2&gt;Creators Review&lt;/h2&gt;&lt;p&gt;Class aptent taciti sociosqu ad litora torquent per conubia nostr per inceptos himenaeos. Duis in erat sit amet sem tincidi fringilla ut a risus. Cras commodo commodonequt facilisis risus aliquam id. Mauris nec adipiscing quam. Aenean congue, mi a vehicula pharetra, lorem felis maximusj&lt;/p&gt;', '&lt;p&gt;Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#039;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. &lt;/p&gt;&lt;p&gt;It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.&lt;/p&gt;', NULL, 'http://www.wojoscripts.com', 'Toronto', 'Wojoscripts', 'IMG_CCE781-A2CA1F-DE0044-A17531-379988-CA81E1.jpg', 0, 'super car, super, car', 'super car, super, car', 'super car meta description', 'super car meta description', '2014-04-08'),
(2, 3, 'riding-a-t-rex', 'Riding a T-Rex', 'Riding a T-Rex', 'Sed vitae nisi sapien, quis venenatis magna. Class aptent taciti sociosqu ad litora torquent per conubia nostr per inceptos himenaeos. ', 'Sed vitae nisi sapien, quis venenatis magna. Class aptent taciti sociosqu ad litora torquent per conubia nostr per inceptos himenaeos. ', '&lt;ul&gt;\r\n	\r\n&lt;li&gt;Client: &lt;a href=&quot;#&quot;&gt;Ambient Space Enterprise&lt;/a&gt;&lt;/li&gt;	\r\n&lt;li&gt;Website: &lt;a href=&quot;#&quot;&gt;www.domainname.com&lt;/a&gt;&lt;/li&gt;	\r\n&lt;li&gt;Date: 29.05.2011&lt;/li&gt;&lt;/ul&gt;\r\n&lt;div&gt;\r\n	&lt;h4&gt;About Idea&lt;/h4&gt;Sed vitae nisi sapien, quis venenatis magna, class aptent tacitisciosqu ad litora torquent per conubia nostr per inceptos himenaDuis in erat sit amet sem tincidi fringilla ut a risus. Cras commocommodo nequt facilisis risus aliquam id.&lt;/div&gt;', '&lt;ul&gt;\r\n	\r\n&lt;li&gt;Client: &lt;a href=&quot;#&quot;&gt;Ambient Space Enterprise&lt;/a&gt;&lt;/li&gt;	\r\n&lt;li&gt;Website: &lt;a href=&quot;#&quot;&gt;www.domainname.com&lt;/a&gt;&lt;/li&gt;	\r\n&lt;li&gt;Date: 29.05.2011&lt;/li&gt;&lt;/ul&gt;\r\n&lt;div&gt;\r\n	&lt;h4&gt;About Idea&lt;/h4&gt;Sed vitae nisi sapien, quis venenatis magna, class aptent tacitisciosqu ad litora torquent per conubia nostr per inceptos himenaDuis in erat sit amet sem tincidi fringilla ut a risus. Cras commocommodo nequt facilisis risus aliquam id.&lt;/div&gt;', '&lt;h2&gt;Creators Review&lt;/h2&gt;&lt;p&gt;Class aptent taciti sociosqu ad litora torquent per conubia nostr per inceptos himenaeos. Duis in erat sit amet sem tincidi fringilla ut a risus. Cras commodo commodonequt facilisis risus aliquam id. Mauris nec adipiscing quam. Aenean congue, mi a vehicula pharetra, lorem felis maximusj&lt;/p&gt;', '&lt;h2&gt;Creators Review&lt;/h2&gt;&lt;p&gt;Class aptent taciti sociosqu ad litora torquent per conubia nostr per inceptos himenaeos. Duis in erat sit amet sem tincidi fringilla ut a risus. Cras commodo commodonequt facilisis risus aliquam id. Mauris nec adipiscing quam. Aenean congue, mi a vehicula pharetra, lorem felis maximusj&lt;/p&gt;', '&lt;p&gt;Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#039;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. &lt;/p&gt;&lt;p&gt;It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.&lt;/p&gt;', NULL, 'http://www.wojoscripts.com', 'Toronto', 'Wojoscripts', 'IMG_2D8C56-0F99D4-EB39D9-68E5B7-68B083-54C7EB.jpg', 0, 'quis,risus,commodo', 'quis,risus,commodo', 'Sed vitae nisi sapien, quis venenatis magna. Class aptent taciti sociosqu ad litora torquent per conubia&#8230;', 'Sed vitae nisi sapien, quis venenatis magna. Class aptent taciti sociosqu ad litora torquent per conubia&#8230;', '2014-04-08'),
(3, 3, 'just-vintage', 'Just Vintage', 'Just Vintage', 'Sed vitae nisi sapien, quis venenatis magna. Class aptent taciti sociosqu ad litora torquent per conubia nostr per inceptos himenaeos. ', 'Sed vitae nisi sapien, quis venenatis magna. Class aptent taciti sociosqu ad litora torquent per conubia nostr per inceptos himenaeos. ', '&lt;ul&gt;\r\n	\r\n&lt;li&gt;Client: &lt;a href=&quot;#&quot;&gt;Ambient Space Enterprise&lt;/a&gt;&lt;/li&gt;	\r\n&lt;li&gt;Website: &lt;a href=&quot;#&quot;&gt;www.domainname.com&lt;/a&gt;&lt;/li&gt;	\r\n&lt;li&gt;Date: 29.05.2011&lt;/li&gt;&lt;/ul&gt;\r\n&lt;div&gt;\r\n	&lt;h4&gt;About Idea&lt;/h4&gt;Sed vitae nisi sapien, quis venenatis magna, class aptent tacitisciosqu ad litora torquent per conubia nostr per inceptos himenaDuis in erat sit amet sem tincidi fringilla ut a risus. Cras commocommodo nequt facilisis risus aliquam id.&lt;/div&gt;', '&lt;ul&gt;\r\n	\r\n&lt;li&gt;Client: &lt;a href=&quot;#&quot;&gt;Ambient Space Enterprise&lt;/a&gt;&lt;/li&gt;	\r\n&lt;li&gt;Website: &lt;a href=&quot;#&quot;&gt;www.domainname.com&lt;/a&gt;&lt;/li&gt;	\r\n&lt;li&gt;Date: 29.05.2011&lt;/li&gt;&lt;/ul&gt;\r\n&lt;div&gt;\r\n	&lt;h4&gt;About Idea&lt;/h4&gt;Sed vitae nisi sapien, quis venenatis magna, class aptent tacitisciosqu ad litora torquent per conubia nostr per inceptos himenaDuis in erat sit amet sem tincidi fringilla ut a risus. Cras commocommodo nequt facilisis risus aliquam id.&lt;/div&gt;', '&lt;h2&gt;Creators Review&lt;/h2&gt;&lt;p&gt;Class aptent taciti sociosqu ad litora torquent per conubia nostr per inceptos himenaeos. Duis in erat sit amet sem tincidi fringilla ut a risus. Cras commodo commodonequt facilisis risus aliquam id. Mauris nec adipiscing quam. Aenean congue, mi a vehicula pharetra, lorem felis maximusj&lt;/p&gt;', '&lt;h2&gt;Creators Review&lt;/h2&gt;&lt;p&gt;Class aptent taciti sociosqu ad litora torquent per conubia nostr per inceptos himenaeos. Duis in erat sit amet sem tincidi fringilla ut a risus. Cras commodo commodonequt facilisis risus aliquam id. Mauris nec adipiscing quam. Aenean congue, mi a vehicula pharetra, lorem felis maximusj&lt;/p&gt;', '&lt;p&gt;Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#039;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. &lt;/p&gt;&lt;p&gt;It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.&lt;/p&gt;', NULL, 'http://www.wojoscripts.com', 'Toronto', 'Wojoscripts', 'IMG_74842C-CC3AA5-399E4E-E501E1-F62F1E-6935A9.jpg', 0, 'quis,risus,commodo', 'quis,risus,commodo', 'Sed vitae nisi sapien, quis venenatis magna. Class aptent taciti sociosqu ad litora torquent per conubia&#8230;', 'Sed vitae nisi sapien, quis venenatis magna. Class aptent taciti sociosqu ad litora torquent per conubia&#8230;', '2014-04-08'),
(4, 3, 'dark-woods', 'Dark Woods', 'Dark Woods', 'Sed vitae nisi sapien, quis venenatis magna. Class aptent taciti sociosqu ad litora torquent per conubia nostr per inceptos himenaeos. ', 'Sed vitae nisi sapien, quis venenatis magna. Class aptent taciti sociosqu ad litora torquent per conubia nostr per inceptos himenaeos. ', '&lt;ul&gt;\r\n	\r\n&lt;li&gt;Client: &lt;a href=&quot;#&quot;&gt;Ambient Space Enterprise&lt;/a&gt;&lt;/li&gt;	\r\n&lt;li&gt;Website: &lt;a href=&quot;#&quot;&gt;www.domainname.com&lt;/a&gt;&lt;/li&gt;	\r\n&lt;li&gt;Date: 29.05.2011&lt;/li&gt;&lt;/ul&gt;\r\n&lt;div&gt;\r\n	&lt;h4&gt;About Idea&lt;/h4&gt;Sed vitae nisi sapien, quis venenatis magna, class aptent tacitisciosqu ad litora torquent per conubia nostr per inceptos himenaDuis in erat sit amet sem tincidi fringilla ut a risus. Cras commocommodo nequt facilisis risus aliquam id.&lt;/div&gt;', '&lt;ul&gt;\r\n	\r\n&lt;li&gt;Client: &lt;a href=&quot;#&quot;&gt;Ambient Space Enterprise&lt;/a&gt;&lt;/li&gt;	\r\n&lt;li&gt;Website: &lt;a href=&quot;#&quot;&gt;www.domainname.com&lt;/a&gt;&lt;/li&gt;	\r\n&lt;li&gt;Date: 29.05.2011&lt;/li&gt;&lt;/ul&gt;\r\n&lt;div&gt;\r\n	&lt;h4&gt;About Idea&lt;/h4&gt;Sed vitae nisi sapien, quis venenatis magna, class aptent tacitisciosqu ad litora torquent per conubia nostr per inceptos himenaDuis in erat sit amet sem tincidi fringilla ut a risus. Cras commocommodo nequt facilisis risus aliquam id.&lt;/div&gt;', '&lt;h2&gt;Creators Review&lt;/h2&gt;&lt;p&gt;Class aptent taciti sociosqu ad litora torquent per conubia nostr per inceptos himenaeos. Duis in erat sit amet sem tincidi fringilla ut a risus. Cras commodo commodonequt facilisis risus aliquam id. Mauris nec adipiscing quam. Aenean congue, mi a vehicula pharetra, lorem felis maximusj&lt;/p&gt;', '&lt;h2&gt;Creators Review&lt;/h2&gt;&lt;p&gt;Class aptent taciti sociosqu ad litora torquent per conubia nostr per inceptos himenaeos. Duis in erat sit amet sem tincidi fringilla ut a risus. Cras commodo commodonequt facilisis risus aliquam id. Mauris nec adipiscing quam. Aenean congue, mi a vehicula pharetra, lorem felis maximusj&lt;/p&gt;', '&lt;p&gt;Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#039;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. &lt;/p&gt;&lt;p&gt;It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.&lt;/p&gt;', NULL, 'http://www.wojoscripts.com', 'Toronto', 'Wojoscripts', 'IMG_28A458-635917-8F7FA7-273207-90A455-833CA0.jpg', 0, 'quis,risus,commodo', 'quis,risus,commodo', 'Sed vitae nisi sapien, quis venenatis magna. Class aptent taciti sociosqu ad litora torquent per conubia&#8230;', 'Sed vitae nisi sapien, quis venenatis magna. Class aptent taciti sociosqu ad litora torquent per conubia&#8230;', '2014-04-08'),
(5, 2, 'in-the-garden', 'In The Garden', 'In The Garden', 'Sed vitae nisi sapien, quis venenatis magna. Class aptent taciti sociosqu ad litora torquent per conubia nostr per inceptos himenaeos. ', 'Sed vitae nisi sapien, quis venenatis magna. Class aptent taciti sociosqu ad litora torquent per conubia nostr per inceptos himenaeos. ', '&lt;ul&gt;\r\n	\r\n&lt;li&gt;Client: &lt;a href=&quot;#&quot;&gt;Ambient Space Enterprise&lt;/a&gt;&lt;/li&gt;	\r\n&lt;li&gt;Website: &lt;a href=&quot;#&quot;&gt;www.domainname.com&lt;/a&gt;&lt;/li&gt;	\r\n&lt;li&gt;Date: 29.05.2011&lt;/li&gt;&lt;/ul&gt;\r\n&lt;div&gt;\r\n	&lt;h4&gt;About Idea&lt;/h4&gt;Sed vitae nisi sapien, quis venenatis magna, class aptent tacitisciosqu ad litora torquent per conubia nostr per inceptos himenaDuis in erat sit amet sem tincidi fringilla ut a risus. Cras commocommodo nequt facilisis risus aliquam id.&lt;/div&gt;', '&lt;ul&gt;\r\n	\r\n&lt;li&gt;Client: &lt;a href=&quot;#&quot;&gt;Ambient Space Enterprise&lt;/a&gt;&lt;/li&gt;	\r\n&lt;li&gt;Website: &lt;a href=&quot;#&quot;&gt;www.domainname.com&lt;/a&gt;&lt;/li&gt;	\r\n&lt;li&gt;Date: 29.05.2011&lt;/li&gt;&lt;/ul&gt;\r\n&lt;div&gt;\r\n	&lt;h4&gt;About Idea&lt;/h4&gt;Sed vitae nisi sapien, quis venenatis magna, class aptent tacitisciosqu ad litora torquent per conubia nostr per inceptos himenaDuis in erat sit amet sem tincidi fringilla ut a risus. Cras commocommodo nequt facilisis risus aliquam id.&lt;/div&gt;', '&lt;h2&gt;Creators Review&lt;/h2&gt;&lt;p&gt;Class aptent taciti sociosqu ad litora torquent per conubia nostr per inceptos himenaeos. Duis in erat sit amet sem tincidi fringilla ut a risus. Cras commodo commodonequt facilisis risus aliquam id. Mauris nec adipiscing quam. Aenean congue, mi a vehicula pharetra, lorem felis maximusj&lt;/p&gt;', '&lt;h2&gt;Creators Review&lt;/h2&gt;&lt;p&gt;Class aptent taciti sociosqu ad litora torquent per conubia nostr per inceptos himenaeos. Duis in erat sit amet sem tincidi fringilla ut a risus. Cras commodo commodonequt facilisis risus aliquam id. Mauris nec adipiscing quam. Aenean congue, mi a vehicula pharetra, lorem felis maximusj&lt;/p&gt;', '&lt;p&gt;Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#039;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. &lt;/p&gt;&lt;p&gt;It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.&lt;/p&gt;', NULL, 'http://www.wojoscripts.com', 'Toronto', 'Wojoscripts', 'IMG_483575-4526B6-68F129-002C9A-20B3F3-62590D.jpg', 0, 'quis,risus,commodo', 'quis,risus,commodo', 'Sed vitae nisi sapien, quis venenatis magna. Class aptent taciti sociosqu ad litora torquent per conubia&#8230;', 'Sed vitae nisi sapien, quis venenatis magna. Class aptent taciti sociosqu ad litora torquent per conubia&#8230;', '2014-04-08'),
(6, 3, 'high-tech', 'High Tech', 'High Tech', 'Sed vitae nisi sapien, quis venenatis magna. Class aptent taciti sociosqu ad litora torquent per conubia nostr per inceptos himenaeos. ', 'Sed vitae nisi sapien, quis venenatis magna. Class aptent taciti sociosqu ad litora torquent per conubia nostr per inceptos himenaeos. ', '&lt;ul&gt;\r\n	\r\n&lt;li&gt;Client: &lt;a href=&quot;#&quot;&gt;Ambient Space Enterprise&lt;/a&gt;&lt;/li&gt;	\r\n&lt;li&gt;Website: &lt;a href=&quot;#&quot;&gt;www.domainname.com&lt;/a&gt;&lt;/li&gt;	\r\n&lt;li&gt;Date: 29.05.2011&lt;/li&gt;&lt;/ul&gt;\r\n&lt;div&gt;\r\n	&lt;h4&gt;About Idea&lt;/h4&gt;Sed vitae nisi sapien, quis venenatis magna, class aptent tacitisciosqu ad litora torquent per conubia nostr per inceptos himenaDuis in erat sit amet sem tincidi fringilla ut a risus. Cras commocommodo nequt facilisis risus aliquam id.&lt;/div&gt;', '&lt;ul&gt;\r\n	\r\n&lt;li&gt;Client: &lt;a href=&quot;#&quot;&gt;Ambient Space Enterprise&lt;/a&gt;&lt;/li&gt;	\r\n&lt;li&gt;Website: &lt;a href=&quot;#&quot;&gt;www.domainname.com&lt;/a&gt;&lt;/li&gt;	\r\n&lt;li&gt;Date: 29.05.2011&lt;/li&gt;&lt;/ul&gt;\r\n&lt;div&gt;\r\n	&lt;h4&gt;About Idea&lt;/h4&gt;Sed vitae nisi sapien, quis venenatis magna, class aptent tacitisciosqu ad litora torquent per conubia nostr per inceptos himenaDuis in erat sit amet sem tincidi fringilla ut a risus. Cras commocommodo nequt facilisis risus aliquam id.&lt;/div&gt;', '&lt;h2&gt;Creators Review&lt;/h2&gt;&lt;p&gt;Class aptent taciti sociosqu ad litora torquent per conubia nostr per inceptos himenaeos. Duis in erat sit amet sem tincidi fringilla ut a risus. Cras commodo commodonequt facilisis risus aliquam id. Mauris nec adipiscing quam. Aenean congue, mi a vehicula pharetra, lorem felis maximusj&lt;/p&gt;', '&lt;h2&gt;Creators Review&lt;/h2&gt;&lt;p&gt;Class aptent taciti sociosqu ad litora torquent per conubia nostr per inceptos himenaeos. Duis in erat sit amet sem tincidi fringilla ut a risus. Cras commodo commodonequt facilisis risus aliquam id. Mauris nec adipiscing quam. Aenean congue, mi a vehicula pharetra, lorem felis maximusj&lt;/p&gt;', '&lt;p&gt;Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#039;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. &lt;/p&gt;&lt;p&gt;It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.&lt;/p&gt;', NULL, 'http://www.wojoscripts.com', 'Toronto', 'Wojoscripts', 'IMG_679AAB-24F504-F9A6A0-A952A6-F03C41-6BF6C2.jpg', 0, 'quis,risus,commodo', 'quis,risus,commodo', 'Sed vitae nisi sapien, quis venenatis magna. Class aptent taciti sociosqu ad litora torquent per conubia&#8230;', 'Sed vitae nisi sapien, quis venenatis magna. Class aptent taciti sociosqu ad litora torquent per conubia&#8230;', '2014-04-08'),
(7, 3, 'glossy-icecubes', 'Glossy Icecubes', 'Glossy Icecubes', 'Sed vitae nisi sapien, quis venenatis magna. Class aptent taciti sociosqu ad litora torquent per conubia nostr per inceptos himenaeos. ', 'Sed vitae nisi sapien, quis venenatis magna. Class aptent taciti sociosqu ad litora torquent per conubia nostr per inceptos himenaeos. ', '&lt;ul&gt;\r\n	\r\n&lt;li&gt;Client: &lt;a href=&quot;#&quot;&gt;Ambient Space Enterprise&lt;/a&gt;&lt;/li&gt;	\r\n&lt;li&gt;Website: &lt;a href=&quot;#&quot;&gt;www.domainname.com&lt;/a&gt;&lt;/li&gt;	\r\n&lt;li&gt;Date: 29.05.2011&lt;/li&gt;&lt;/ul&gt;\r\n&lt;div&gt;\r\n	&lt;h4&gt;About Idea&lt;/h4&gt;Sed vitae nisi sapien, quis venenatis magna, class aptent tacitisciosqu ad litora torquent per conubia nostr per inceptos himenaDuis in erat sit amet sem tincidi fringilla ut a risus. Cras commocommodo nequt facilisis risus aliquam id.&lt;/div&gt;', '&lt;ul&gt;\r\n	\r\n&lt;li&gt;Client: &lt;a href=&quot;#&quot;&gt;Ambient Space Enterprise&lt;/a&gt;&lt;/li&gt;	\r\n&lt;li&gt;Website: &lt;a href=&quot;#&quot;&gt;www.domainname.com&lt;/a&gt;&lt;/li&gt;	\r\n&lt;li&gt;Date: 29.05.2011&lt;/li&gt;&lt;/ul&gt;\r\n&lt;div&gt;\r\n	&lt;h4&gt;About Idea&lt;/h4&gt;Sed vitae nisi sapien, quis venenatis magna, class aptent tacitisciosqu ad litora torquent per conubia nostr per inceptos himenaDuis in erat sit amet sem tincidi fringilla ut a risus. Cras commocommodo nequt facilisis risus aliquam id.&lt;/div&gt;', '&lt;h2&gt;Creators Review&lt;/h2&gt;&lt;p&gt;Class aptent taciti sociosqu ad litora torquent per conubia nostr per inceptos himenaeos. Duis in erat sit amet sem tincidi fringilla ut a risus. Cras commodo commodonequt facilisis risus aliquam id. Mauris nec adipiscing quam. Aenean congue, mi a vehicula pharetra, lorem felis maximusj&lt;/p&gt;', '&lt;h2&gt;Creators Review&lt;/h2&gt;&lt;p&gt;Class aptent taciti sociosqu ad litora torquent per conubia nostr per inceptos himenaeos. Duis in erat sit amet sem tincidi fringilla ut a risus. Cras commodo commodonequt facilisis risus aliquam id. Mauris nec adipiscing quam. Aenean congue, mi a vehicula pharetra, lorem felis maximusj&lt;/p&gt;', '&lt;p&gt;Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#039;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. &lt;/p&gt;&lt;p&gt;It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.&lt;/p&gt;', NULL, 'http://www.wojoscripts.com', 'Toronto', 'Wojoscripts', 'IMG_C600F9-6C21E6-CE01EF-C145FE-8F511F-F53D63.jpg', 0, 'quis,risus,commodo', 'quis,risus,commodo', 'Sed vitae nisi sapien, quis venenatis magna. Class aptent taciti sociosqu ad litora torquent per conubia&#8230;', 'Sed vitae nisi sapien, quis venenatis magna. Class aptent taciti sociosqu ad litora torquent per conubia&#8230;', '2014-04-08'),
(8, 3, 'river-reflections', 'River Reflections', 'River Reflections', 'Sed vitae nisi sapien, quis venenatis magna. Class aptent taciti sociosqu ad litora torquent per conubia nostr per inceptos himenaeos. ', 'Sed vitae nisi sapien, quis venenatis magna. Class aptent taciti sociosqu ad litora torquent per conubia nostr per inceptos himenaeos. ', '&lt;ul&gt;\r\n	\r\n&lt;li&gt;Client: &lt;a href=&quot;#&quot;&gt;Ambient Space Enterprise&lt;/a&gt;&lt;/li&gt;	\r\n&lt;li&gt;Website: &lt;a href=&quot;#&quot;&gt;www.domainname.com&lt;/a&gt;&lt;/li&gt;	\r\n&lt;li&gt;Date: 29.05.2011&lt;/li&gt;&lt;/ul&gt;\r\n&lt;div&gt;\r\n	&lt;h4&gt;About Idea&lt;/h4&gt;Sed vitae nisi sapien, quis venenatis magna, class aptent tacitisciosqu ad litora torquent per conubia nostr per inceptos himenaDuis in erat sit amet sem tincidi fringilla ut a risus. Cras commocommodo nequt facilisis risus aliquam id.&lt;/div&gt;', '&lt;ul&gt;\r\n	\r\n&lt;li&gt;Client: &lt;a href=&quot;#&quot;&gt;Ambient Space Enterprise&lt;/a&gt;&lt;/li&gt;	\r\n&lt;li&gt;Website: &lt;a href=&quot;#&quot;&gt;www.domainname.com&lt;/a&gt;&lt;/li&gt;	\r\n&lt;li&gt;Date: 29.05.2011&lt;/li&gt;&lt;/ul&gt;\r\n&lt;div&gt;\r\n	&lt;h4&gt;About Idea&lt;/h4&gt;Sed vitae nisi sapien, quis venenatis magna, class aptent tacitisciosqu ad litora torquent per conubia nostr per inceptos himenaDuis in erat sit amet sem tincidi fringilla ut a risus. Cras commocommodo nequt facilisis risus aliquam id.&lt;/div&gt;', '&lt;h2&gt;Creators Review&lt;/h2&gt;&lt;p&gt;Class aptent taciti sociosqu ad litora torquent per conubia nostr per inceptos himenaeos. Duis in erat sit amet sem tincidi fringilla ut a risus. Cras commodo commodonequt facilisis risus aliquam id. Mauris nec adipiscing quam. Aenean congue, mi a vehicula pharetra, lorem felis maximusj&lt;/p&gt;', '&lt;h2&gt;Creators Review&lt;/h2&gt;&lt;p&gt;Class aptent taciti sociosqu ad litora torquent per conubia nostr per inceptos himenaeos. Duis in erat sit amet sem tincidi fringilla ut a risus. Cras commodo commodonequt facilisis risus aliquam id. Mauris nec adipiscing quam. Aenean congue, mi a vehicula pharetra, lorem felis maximusj&lt;/p&gt;', '&lt;p&gt;Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#039;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. &lt;/p&gt;&lt;p&gt;It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.&lt;/p&gt;', NULL, 'http://www.wojoscripts.com', 'Toronto', 'Wojoscripts', 'IMG_8DFB97-D0EC3E-B950A4-0AB992-EE1AA9-6144F6.jpg', 0, 'quis,risus,commodo', 'quis,risus,commodo', 'Sed vitae nisi sapien, quis venenatis magna. Class aptent taciti sociosqu ad litora torquent per conubia&#8230;', 'Sed vitae nisi sapien, quis venenatis magna. Class aptent taciti sociosqu ad litora torquent per conubia&#8230;', '2014-04-08'),
(9, 1, 'crazy-kids', 'Crazy Kids - Gallery Demo', 'Crazy Kids - Gallery Demo', 'Sed vitae nisi sapien, quis venenatis magna. Class aptent taciti sociosqu ad litora torquent per conubia nostr per inceptos himenaeos. ', 'Sed vitae nisi sapien, quis venenatis magna. Class aptent taciti sociosqu ad litora torquent per conubia nostr per inceptos himenaeos. ', '&lt;ul&gt;\r\n	\r\n&lt;li&gt;Client: &lt;a href=&quot;#&quot;&gt;Ambient Space Enterprise&lt;/a&gt;&lt;/li&gt;	\r\n&lt;li&gt;Website: &lt;a href=&quot;#&quot;&gt;www.domainname.com&lt;/a&gt;&lt;/li&gt;	\r\n&lt;li&gt;Date: 29.05.2011&lt;/li&gt;&lt;/ul&gt;\r\n&lt;div&gt;\r\n	&lt;h4&gt;About Idea&lt;/h4&gt;Sed vitae nisi sapien, quis venenatis magna, class aptent tacitisciosqu ad litora torquent per conubia nostr per inceptos himenaDuis in erat sit amet sem tincidi fringilla ut a risus. Cras commocommodo nequt facilisis risus aliquam id.&lt;/div&gt;', '&lt;ul&gt;\r\n	\r\n&lt;li&gt;Client: &lt;a href=&quot;#&quot;&gt;Ambient Space Enterprise&lt;/a&gt;&lt;/li&gt;	\r\n&lt;li&gt;Website: &lt;a href=&quot;#&quot;&gt;www.domainname.com&lt;/a&gt;&lt;/li&gt;	\r\n&lt;li&gt;Date: 29.05.2011&lt;/li&gt;&lt;/ul&gt;\r\n&lt;div&gt;\r\n	&lt;h4&gt;About Idea&lt;/h4&gt;Sed vitae nisi sapien, quis venenatis magna, class aptent tacitisciosqu ad litora torquent per conubia nostr per inceptos himenaDuis in erat sit amet sem tincidi fringilla ut a risus. Cras commocommodo nequt facilisis risus aliquam id.&lt;/div&gt;', '&lt;h2&gt;Creators Review&lt;/h2&gt;&lt;p&gt;Class aptent taciti sociosqu ad litora torquent per conubia nostr per inceptos himenaeos. Duis in erat sit amet sem tincidi fringilla ut a risus. Cras commodo commodonequt facilisis risus aliquam id. Mauris nec adipiscing quam. Aenean congue, mi a vehicula pharetra, lorem felis maximusj&lt;/p&gt;', '&lt;h2&gt;Creators Review&lt;/h2&gt;&lt;p&gt;Class aptent taciti sociosqu ad litora torquent per conubia nostr per inceptos himenaeos. Duis in erat sit amet sem tincidi fringilla ut a risus. Cras commodo commodonequt facilisis risus aliquam id. Mauris nec adipiscing quam. Aenean congue, mi a vehicula pharetra, lorem felis maximusj&lt;/p&gt;', '&lt;p&gt;Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#039;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. &lt;/p&gt;&lt;p&gt;It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.&lt;/p&gt;', NULL, 'http://www.wojoscripts.com', 'Toronto', 'Wojoscripts', 'IMG_1FFB6A-B6887A-65759B-8209AE-DA5861-BA3173.jpg', 2, 'quis,risus,commodo', 'quis,risus,commodo', 'Sed vitae nisi sapien, quis venenatis magna. Class aptent taciti sociosqu ad litora torquent per conubia&#8230;', 'Sed vitae nisi sapien, quis venenatis magna. Class aptent taciti sociosqu ad litora torquent per conubia&#8230;', '2014-04-08'),
(10, 1, 'super-turtle', 'Super Turtle', 'Super Turtle', 'Sed vitae nisi sapien, quis venenatis magna. Class aptent taciti sociosqu ad litora torquent per conubia nostr per inceptos himenaeos. ', 'Sed vitae nisi sapien, quis venenatis magna. Class aptent taciti sociosqu ad litora torquent per conubia nostr per inceptos himenaeos. ', '&lt;ul&gt;\r\n	\r\n&lt;li&gt;Client: &lt;a href=&quot;#&quot;&gt;Ambient Space Enterprise&lt;/a&gt;&lt;/li&gt;	\r\n&lt;li&gt;Website: &lt;a href=&quot;#&quot;&gt;www.domainname.com&lt;/a&gt;&lt;/li&gt;	\r\n&lt;li&gt;Date: 29.05.2011&lt;/li&gt;&lt;/ul&gt;\r\n&lt;div&gt;\r\n	&lt;h4&gt;About Idea&lt;/h4&gt;Sed vitae nisi sapien, quis venenatis magna, class aptent tacitisciosqu ad litora torquent per conubia nostr per inceptos himenaDuis in erat sit amet sem tincidi fringilla ut a risus. Cras commocommodo nequt facilisis risus aliquam id.&lt;/div&gt;', '&lt;ul&gt;\r\n	\r\n&lt;li&gt;Client: &lt;a href=&quot;#&quot;&gt;Ambient Space Enterprise&lt;/a&gt;&lt;/li&gt;	\r\n&lt;li&gt;Website: &lt;a href=&quot;#&quot;&gt;www.domainname.com&lt;/a&gt;&lt;/li&gt;	\r\n&lt;li&gt;Date: 29.05.2011&lt;/li&gt;&lt;/ul&gt;\r\n&lt;div&gt;\r\n	&lt;h4&gt;About Idea&lt;/h4&gt;Sed vitae nisi sapien, quis venenatis magna, class aptent tacitisciosqu ad litora torquent per conubia nostr per inceptos himenaDuis in erat sit amet sem tincidi fringilla ut a risus. Cras commocommodo nequt facilisis risus aliquam id.&lt;/div&gt;', '&lt;h2&gt;Creators Review&lt;/h2&gt;&lt;p&gt;Class aptent taciti sociosqu ad litora torquent per conubia nostr per inceptos himenaeos. Duis in erat sit amet sem tincidi fringilla ut a risus. Cras commodo commodonequt facilisis risus aliquam id. Mauris nec adipiscing quam. Aenean congue, mi a vehicula pharetra, lorem felis maximusj&lt;/p&gt;', '&lt;h2&gt;Creators Review&lt;/h2&gt;&lt;p&gt;Class aptent taciti sociosqu ad litora torquent per conubia nostr per inceptos himenaeos. Duis in erat sit amet sem tincidi fringilla ut a risus. Cras commodo commodonequt facilisis risus aliquam id. Mauris nec adipiscing quam. Aenean congue, mi a vehicula pharetra, lorem felis maximusj&lt;/p&gt;', '&lt;p&gt;Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#039;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. &lt;/p&gt;&lt;p&gt;It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.&lt;/p&gt;', NULL, 'http://www.wojoscripts.com', 'Toronto', 'Wojoscripts', 'IMG_472512-B8EA41-8AA1D7-7C94A8-9D9004-2E943F.jpg', 0, 'quis,risus,commodo', 'quis,risus,commodo', 'Sed vitae nisi sapien, quis venenatis magna. Class aptent taciti sociosqu ad litora torquent per conubia&#8230;', 'Sed vitae nisi sapien, quis venenatis magna. Class aptent taciti sociosqu ad litora torquent per conubia&#8230;', '2014-04-08'),
(11, 1, 'pet-fish', 'Pet Fish', 'Pet Fish', 'Sed vitae nisi sapien, quis venenatis magna. Class aptent taciti sociosqu ad litora torquent per conubia nostr per inceptos himenaeos. ', 'Sed vitae nisi sapien, quis venenatis magna. Class aptent taciti sociosqu ad litora torquent per conubia nostr per inceptos himenaeos. ', '&lt;ul&gt;\r\n	\r\n&lt;li&gt;Client: &lt;a href=&quot;#&quot;&gt;Ambient Space Enterprise&lt;/a&gt;&lt;/li&gt;	\r\n&lt;li&gt;Website: &lt;a href=&quot;#&quot;&gt;www.domainname.com&lt;/a&gt;&lt;/li&gt;	\r\n&lt;li&gt;Date: 29.05.2011&lt;/li&gt;&lt;/ul&gt;\r\n&lt;div&gt;\r\n	&lt;h4&gt;About Idea&lt;/h4&gt;Sed vitae nisi sapien, quis venenatis magna, class aptent tacitisciosqu ad litora torquent per conubia nostr per inceptos himenaDuis in erat sit amet sem tincidi fringilla ut a risus. Cras commocommodo nequt facilisis risus aliquam id.&lt;/div&gt;', '&lt;ul&gt;\r\n	\r\n&lt;li&gt;Client: &lt;a href=&quot;#&quot;&gt;Ambient Space Enterprise&lt;/a&gt;&lt;/li&gt;	\r\n&lt;li&gt;Website: &lt;a href=&quot;#&quot;&gt;www.domainname.com&lt;/a&gt;&lt;/li&gt;	\r\n&lt;li&gt;Date: 29.05.2011&lt;/li&gt;&lt;/ul&gt;\r\n&lt;div&gt;\r\n	&lt;h4&gt;About Idea&lt;/h4&gt;Sed vitae nisi sapien, quis venenatis magna, class aptent tacitisciosqu ad litora torquent per conubia nostr per inceptos himenaDuis in erat sit amet sem tincidi fringilla ut a risus. Cras commocommodo nequt facilisis risus aliquam id.&lt;/div&gt;', '&lt;h2&gt;Creators Review&lt;/h2&gt;&lt;p&gt;Class aptent taciti sociosqu ad litora torquent per conubia nostr per inceptos himenaeos. Duis in erat sit amet sem tincidi fringilla ut a risus. Cras commodo commodonequt facilisis risus aliquam id. Mauris nec adipiscing quam. Aenean congue, mi a vehicula pharetra, lorem felis maximusj&lt;/p&gt;', '&lt;h2&gt;Creators Review&lt;/h2&gt;&lt;p&gt;Class aptent taciti sociosqu ad litora torquent per conubia nostr per inceptos himenaeos. Duis in erat sit amet sem tincidi fringilla ut a risus. Cras commodo commodonequt facilisis risus aliquam id. Mauris nec adipiscing quam. Aenean congue, mi a vehicula pharetra, lorem felis maximusj&lt;/p&gt;', '&lt;p&gt;Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#039;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. &lt;/p&gt;&lt;p&gt;It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.&lt;/p&gt;', NULL, 'http://www.wojoscripts.com', 'Toronto', 'Wojoscripts', 'IMG_BF9F92-BF3203-E3B1BF-6AA49D-CE49C0-F23908.jpg', 0, 'quis,risus,commodo', 'quis,risus,commodo', 'Sed vitae nisi sapien, quis venenatis magna. Class aptent taciti sociosqu ad litora torquent per conubia&#8230;', 'Sed vitae nisi sapien, quis venenatis magna. Class aptent taciti sociosqu ad litora torquent per conubia&#8230;', '2014-04-08'),
(12, 1, 'rock-baby', 'Rock Baby', 'Rock Baby', 'Sed vitae nisi sapien, quis venenatis magna. Class aptent taciti sociosqu ad litora torquent per conubia nostr per inceptos himenaeos. ', 'Sed vitae nisi sapien, quis venenatis magna. Class aptent taciti sociosqu ad litora torquent per conubia nostr per inceptos himenaeos. ', '&lt;ul&gt;\r\n	\r\n&lt;li&gt;Client: &lt;a href=&quot;#&quot;&gt;Ambient Space Enterprise&lt;/a&gt;&lt;/li&gt;	\r\n&lt;li&gt;Website: &lt;a href=&quot;#&quot;&gt;www.domainname.com&lt;/a&gt;&lt;/li&gt;	\r\n&lt;li&gt;Date: 29.05.2011&lt;/li&gt;&lt;/ul&gt;\r\n&lt;div&gt;\r\n	&lt;h4&gt;About Idea&lt;/h4&gt;Sed vitae nisi sapien, quis venenatis magna, class aptent tacitisciosqu ad litora torquent per conubia nostr per inceptos himenaDuis in erat sit amet sem tincidi fringilla ut a risus. Cras commocommodo nequt facilisis risus aliquam id.&lt;/div&gt;', '&lt;ul&gt;\r\n	\r\n&lt;li&gt;Client: &lt;a href=&quot;#&quot;&gt;Ambient Space Enterprise&lt;/a&gt;&lt;/li&gt;	\r\n&lt;li&gt;Website: &lt;a href=&quot;#&quot;&gt;www.domainname.com&lt;/a&gt;&lt;/li&gt;	\r\n&lt;li&gt;Date: 29.05.2011&lt;/li&gt;&lt;/ul&gt;\r\n&lt;div&gt;\r\n	&lt;h4&gt;About Idea&lt;/h4&gt;Sed vitae nisi sapien, quis venenatis magna, class aptent tacitisciosqu ad litora torquent per conubia nostr per inceptos himenaDuis in erat sit amet sem tincidi fringilla ut a risus. Cras commocommodo nequt facilisis risus aliquam id.&lt;/div&gt;', '&lt;h2&gt;Creators Review&lt;/h2&gt;&lt;p&gt;Class aptent taciti sociosqu ad litora torquent per conubia nostr per inceptos himenaeos. Duis in erat sit amet sem tincidi fringilla ut a risus. Cras commodo commodonequt facilisis risus aliquam id. Mauris nec adipiscing quam. Aenean congue, mi a vehicula pharetra, lorem felis maximusj&lt;/p&gt;', '&lt;h2&gt;Creators Review&lt;/h2&gt;&lt;p&gt;Class aptent taciti sociosqu ad litora torquent per conubia nostr per inceptos himenaeos. Duis in erat sit amet sem tincidi fringilla ut a risus. Cras commodo commodonequt facilisis risus aliquam id. Mauris nec adipiscing quam. Aenean congue, mi a vehicula pharetra, lorem felis maximusj&lt;/p&gt;', '&lt;p&gt;Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#039;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. &lt;/p&gt;&lt;p&gt;It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.&lt;/p&gt;', NULL, 'http://www.wojoscripts.com', 'Toronto', 'Wojoscripts', 'IMG_221D5E-E4DD87-C3180F-33064F-BB85D9-DA8468.jpg', 0, 'quis,risus,commodo', 'quis,risus,commodo', 'Sed vitae nisi sapien, quis venenatis magna. Class aptent taciti sociosqu ad litora torquent per conubia&#8230;', 'Sed vitae nisi sapien, quis venenatis magna. Class aptent taciti sociosqu ad litora torquent per conubia&#8230;', '2014-04-08'),
(13, 1, 'audition', 'Audition', 'Audition', 'Sed vitae nisi sapien, quis venenatis magna. Class aptent taciti sociosqu ad litora torquent per conubia nostr per inceptos himenaeos. ', 'Sed vitae nisi sapien, quis venenatis magna. Class aptent taciti sociosqu ad litora torquent per conubia nostr per inceptos himenaeos. ', '&lt;p&gt;Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#039;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. &lt;/p&gt;&lt;p&gt;It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.&lt;/p&gt;', '&lt;p&gt;Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#039;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. &lt;/p&gt;&lt;p&gt;It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.&lt;/p&gt;', '&lt;p&gt;Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#039;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. &lt;/p&gt;&lt;p&gt;It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.&lt;/p&gt;', '&lt;p&gt;Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#039;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. &lt;/p&gt;&lt;p&gt;It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.&lt;/p&gt;', '&lt;p&gt;Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#039;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. &lt;/p&gt;&lt;p&gt;It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.&lt;/p&gt;', NULL, 'http://www.wojoscripts.com', 'Toronto', 'Wojoscripts', 'IMG_3DEBD6-7EE88F-AE9EA7-ADD0CB-F33226-E2B37E.jpg', 0, 'quis,risus,commodo', 'quis,risus,commodo', 'Sed vitae nisi sapien, quis venenatis magna. Class aptent taciti sociosqu ad litora torquent per conubia&#8230;', 'Sed vitae nisi sapien, quis venenatis magna. Class aptent taciti sociosqu ad litora torquent per conubia&#8230;', '2014-04-08');

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `mod_portfolio_category`
--

CREATE TABLE `mod_portfolio_category` (
  `id` int(4) NOT NULL,
  `title_en` varchar(100) NOT NULL,
  `title_tr` varchar(100) NOT NULL,
  `slug` varchar(100) NOT NULL,
  `metakey_en` varchar(100) DEFAULT NULL,
  `metakey_tr` varchar(100) NOT NULL,
  `metadesc_en` tinytext,
  `metadesc_tr` tinytext,
  `position` smallint(6) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Tablo döküm verisi `mod_portfolio_category`
--

INSERT INTO `mod_portfolio_category` (`id`, `title_en`, `title_tr`, `slug`, `metakey_en`, `metakey_tr`, `metadesc_en`, `metadesc_tr`, `position`) VALUES
(1, 'Webdesign', 'Webdesign', 'webdesign', 'webdesign', 'webdesign', 'portfolio category webdesign', 'portfolio category webdesign', 1),
(2, 'Illustration', 'Illustration', 'illustration', NULL, '', NULL, '', 2),
(3, 'Photography', 'Photography', 'photography', NULL, '', NULL, '', 3);

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `mod_projects`
--

CREATE TABLE `mod_projects` (
  `id` int(6) NOT NULL,
  `cid` int(6) NOT NULL DEFAULT '0',
  `slug` varchar(100) DEFAULT NULL,
  `title_en` varchar(100) DEFAULT NULL,
  `title_tr` varchar(100) NOT NULL,
  `short_desc_en` text,
  `short_desc_tr` text,
  `detail_en` text,
  `detail_tr` text,
  `body_en` text,
  `body_tr` text,
  `result_en` text,
  `result_tr` text,
  `www` varchar(100) DEFAULT NULL,
  `web_text` varchar(100) NOT NULL,
  `location` varchar(100) DEFAULT NULL,
  `client` varchar(100) DEFAULT NULL,
  `thumb` varchar(60) DEFAULT NULL,
  `gallery` smallint(4) NOT NULL DEFAULT '0',
  `metakey_en` varchar(200) DEFAULT NULL,
  `metakey_tr` varchar(200) NOT NULL,
  `metadesc_en` text,
  `metadesc_tr` text,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `selected` tinyint(1) NOT NULL DEFAULT '1',
  `layout` enum('t','l','') NOT NULL COMMENT 'T: galeri yukarıda, L:galeri solda'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Tablo döküm verisi `mod_projects`
--

INSERT INTO `mod_projects` (`id`, `cid`, `slug`, `title_en`, `title_tr`, `short_desc_en`, `short_desc_tr`, `detail_en`, `detail_tr`, `body_en`, `body_tr`, `result_en`, `result_tr`, `www`, `web_text`, `location`, `client`, `thumb`, `gallery`, `metakey_en`, `metakey_tr`, `metadesc_en`, `metadesc_tr`, `created`, `modified`, `selected`, `layout`) VALUES
(9, 4, 'sektor4', NULL, 'OTEL , KULÜP , OKUL KANTİNLERİ VB.', NULL, 'Otel, kulüp, dernek, okul kantinleri v.b. kurumlarda, hem tezgah hem masa servisi vardır. Servis çok hızlı olmalıdır, aynı anda \r\nbirden fazla müşteri sırada bekleyebilir. Servisin çok hızlı olması sebebiyle sipariş alımlarda ki hataların giderilmesi, sipariş iptalleri \r\nönemlidir. Kulüp ve derneklerde üye takipleri, üyelere özel indirim ve promosyonlar önem kazanır. Üye kartı gibi üye takip sistemleri \r\nkullanılır. Merkezi idare ve kontrol önemlidir.', NULL, '&lt;p&gt;\r\n&lt;/p&gt;\r\n&lt;br&gt;\r\n&lt;h5&gt;POSMAKS / FASTFOOD ÖZELLİKLER&lt;/h5&gt;\r\n&lt;p&gt;\r\n&lt;/p&gt;\r\n&lt;div class=&quot;alert alert-info&quot; role=&quot;alert&quot;&gt;\r\n	&lt;b&gt;Esnek ve Kolay Arayüz&lt;/b&gt;\r\n&lt;/div&gt;\r\n&lt;div class=&quot;alert alert-info&quot; role=&quot;alert&quot;&gt;\r\n	&lt;b&gt;Kolay Anlaşılır Menü Yapısı&lt;/b&gt;&lt;br&gt;\r\n&lt;/div&gt;\r\n&lt;div class=&quot;alert alert-info&quot; role=&quot;alert&quot;&gt;\r\n	&lt;b&gt;Fatura / Adisyon Basımı&lt;/b&gt;\r\n&lt;/div&gt;\r\n&lt;div class=&quot;alert alert-info&quot; role=&quot;alert&quot;&gt;\r\n	&lt;b&gt;Sesli Sipariş Notu&lt;/b&gt;\r\n&lt;/div&gt;\r\n&lt;div class=&quot;alert alert-info&quot; role=&quot;alert&quot;&gt;\r\n	&lt;b&gt;Yazılı Sipariş Notu&lt;/b&gt;\r\n&lt;/div&gt;\r\n&lt;div class=&quot;alert alert-info&quot; role=&quot;alert&quot;&gt;\r\n	&lt;b&gt;Stok Takip Modülü&lt;/b&gt;\r\n&lt;/div&gt;\r\n&lt;div class=&quot;alert alert-info&quot; role=&quot;alert&quot;&gt;\r\n	&lt;b&gt;Personel Takip Modülü&lt;/b&gt;\r\n&lt;/div&gt;', NULL, '&lt;p&gt;\r\n&lt;/p&gt;\r\n&lt;p&gt;\r\n&lt;/p&gt;\r\n&lt;br&gt;\r\n&lt;h5&gt;POSMAKS / FASTFOOD AVANTAJLAR&lt;/h5&gt;\r\n&lt;p&gt;\r\n&lt;/p&gt;\r\n&lt;div class=&quot;alert alert-success&quot; role=&quot;success&quot;&gt;\r\n	&lt;b&gt;Aynı işletmede tek yazılım ile farklı donanım kullanabilme&lt;/b&gt;\r\n&lt;/div&gt;\r\n&lt;div class=&quot;alert alert-success&quot; role=&quot;success&quot;&gt;\r\n	&lt;b&gt;Akıllı giriş sistemi ile farklı fonksiyonları&amp;nbsp;personellere göre yetkilendirebilme&lt;/b&gt;\r\n&lt;/div&gt;\r\n&lt;div class=&quot;alert alert-success&quot; role=&quot;success&quot;&gt;\r\n	&lt;b&gt;Zincir işletmeler için tek noktadan tüm işletmelerin raporlarını görüntüleyebilme&lt;/b&gt;\r\n&lt;/div&gt;\r\n&lt;div class=&quot;alert alert-success&quot; role=&quot;success&quot;&gt;\r\n	&lt;b&gt;Gün içerisinde yapılan satışların ve stok durumunu görüntüleyebilme&lt;/b&gt;\r\n&lt;/div&gt;\r\n&lt;div class=&quot;alert alert-success&quot; role=&quot;success&quot;&gt;\r\n	&lt;b&gt;Günlük kazanç, gelir - gider tablosu görüntüleyebilme&lt;/b&gt;\r\n&lt;/div&gt;\r\n&lt;div class=&quot;alert alert-success&quot; role=&quot;success&quot;&gt;\r\n	&lt;b&gt;Tarih ve saat seçimi aralıkları ile toplu kazanç tablosu görüntüleyebilme&lt;/b&gt;\r\n&lt;/div&gt;\r\n&lt;div class=&quot;alert alert-success&quot; role=&quot;success&quot;&gt;\r\n	&lt;b&gt;Sesli sipariş özelliği ile daha ayrıntılı sipariş verebilme&lt;/b&gt;\r\n&lt;/div&gt;', NULL, '&lt;p&gt;DAHA FAZLA BİLGİ İÇİN&lt;/p&gt;', 'http://www.posmaks.com/page/iletisim/', 'TEKLİF İSTE', 'Toronto', 'Wojoscripts', 'IMG_997889-9286B2-F87595-A9B4DB-5F117E-DF1E09.jpg', 0, 'quis,risus,commodo', 'kantin,satış,posmaks,pos,nakit,otel,fastfood,cihaz,restaurant', NULL, 'Otel, kulüp, dernek, okul kantinleri v.b. kurumlarda, hem tezgah hem masa servisi vardır. Servis çok…', '2016-04-08 00:00:00', '2016-08-31 18:13:09', 1, 't'),
(7, 4, 'sektor3', NULL, 'KAHVE ZİNCİRLERİ', NULL, 'Self servis çalışan yerlerin yanısıra hem tezgah hem masa servisi olan yerler de vardır.Self servis olan yerlerde kasiyerin hızı önemlidir. \r\nİşletme alanları genelde çok büyük değildir bu sebeple müşterilerin sırada birikmemesi önemlidir. Yiyecek içecek ürünlerinin yanısıra \r\nfincan, kahve makinası gibi dayanıklı tüketim malzeme satışı da yapılır. Zincir işletme yapısındaolanlarda merkezden fiyat değişimi, \r\npromosyon tanımlama gibi özelliklerden faydalanılır.', NULL, '&lt;p&gt;\r\n&lt;/p&gt;\r\n&lt;br&gt;\r\n&lt;h5&gt;POSMAKS / FASTFOOD ÖZELLİKLER&lt;/h5&gt;\r\n&lt;p&gt;\r\n&lt;/p&gt;\r\n&lt;div class=&quot;alert alert-info&quot; role=&quot;alert&quot;&gt;\r\n	&lt;b&gt;Esnek ve Kolay Arayüz&lt;/b&gt;\r\n&lt;/div&gt;\r\n&lt;div class=&quot;alert alert-info&quot; role=&quot;alert&quot;&gt;\r\n	&lt;b&gt;Kolay Anlaşılır Menü Yapısı&lt;/b&gt;&lt;br&gt;\r\n&lt;/div&gt;\r\n&lt;div class=&quot;alert alert-info&quot; role=&quot;alert&quot;&gt;\r\n	&lt;b&gt;Fatura / Adisyon Basımı&lt;/b&gt;\r\n&lt;/div&gt;\r\n&lt;div class=&quot;alert alert-info&quot; role=&quot;alert&quot;&gt;\r\n	&lt;b&gt;Sesli Sipariş Notu&lt;/b&gt;\r\n&lt;/div&gt;\r\n&lt;div class=&quot;alert alert-info&quot; role=&quot;alert&quot;&gt;\r\n	&lt;b&gt;Yazılı Sipariş Notu&lt;/b&gt;\r\n&lt;/div&gt;\r\n&lt;div class=&quot;alert alert-info&quot; role=&quot;alert&quot;&gt;\r\n	&lt;b&gt;Stok Takip Modülü&lt;/b&gt;\r\n&lt;/div&gt;\r\n&lt;div class=&quot;alert alert-info&quot; role=&quot;alert&quot;&gt;\r\n	&lt;b&gt;Personel Takip Modülü&lt;/b&gt;\r\n&lt;/div&gt;', NULL, '&lt;p&gt;\r\n&lt;/p&gt;\r\n&lt;p&gt;\r\n&lt;/p&gt;\r\n&lt;br&gt;\r\n&lt;h5&gt;POSMAKS / FASTFOOD AVANTAJLAR&lt;/h5&gt;\r\n&lt;p&gt;\r\n&lt;/p&gt;\r\n&lt;div class=&quot;alert alert-success&quot; role=&quot;success&quot;&gt;\r\n	&lt;b&gt;Aynı işletmede tek yazılım ile farklı donanım kullanabilme&lt;/b&gt;\r\n&lt;/div&gt;\r\n&lt;div class=&quot;alert alert-success&quot; role=&quot;success&quot;&gt;\r\n	&lt;b&gt;Akıllı giriş sistemi ile farklı fonksiyonları&amp;nbsp;personellere göre yetkilendirebilme&lt;/b&gt;\r\n&lt;/div&gt;\r\n&lt;div class=&quot;alert alert-success&quot; role=&quot;success&quot;&gt;\r\n	&lt;b&gt;Zincir işletmeler için tek noktadan tüm işletmelerin raporlarını görüntüleyebilme&lt;/b&gt;\r\n&lt;/div&gt;\r\n&lt;div class=&quot;alert alert-success&quot; role=&quot;success&quot;&gt;\r\n	&lt;b&gt;Gün içerisinde yapılan satışların ve stok durumunu görüntüleyebilme&lt;/b&gt;\r\n&lt;/div&gt;\r\n&lt;div class=&quot;alert alert-success&quot; role=&quot;success&quot;&gt;\r\n	&lt;b&gt;Günlük kazanç, gelir - gider tablosu görüntüleyebilme&lt;/b&gt;\r\n&lt;/div&gt;\r\n&lt;div class=&quot;alert alert-success&quot; role=&quot;success&quot;&gt;\r\n	&lt;b&gt;Tarih ve saat seçimi aralıkları ile toplu kazanç tablosu görüntüleyebilme&lt;/b&gt;\r\n&lt;/div&gt;\r\n&lt;div class=&quot;alert alert-success&quot; role=&quot;success&quot;&gt;\r\n	&lt;b&gt;Sesli sipariş özelliği ile daha ayrıntılı sipariş verebilme&lt;/b&gt;\r\n&lt;/div&gt;', NULL, '&lt;p&gt;DAHA FAZLA BİLGİ İÇİN&lt;/p&gt;', 'http://www.posmaks.com/page/iletisim/', 'TEKLİF İSTE', 'Toronto', 'Wojoscripts', 'IMG_6D55E3-BDB97F-F1F184-DA1BC6-C8C10A-E94E15.jpg', 0, 'quis,risus,commodo', 'quis,risus,commodo', NULL, 'Sed vitae nisi sapien, quis venenatis magna. Class aptent taciti sociosqu ad litora torquent per conubia…', '2014-04-08 00:00:00', '2016-08-31 18:12:19', 1, 't'),
(8, 4, 'sektor2', NULL, 'RESTAURANT', NULL, 'Masa servisi uygulanır.Yiyeceklerin üretilme hızları ve diğer verilen siparişler ile uygun zamanda üretilmesi önemlidir.Mekanlar \r\ngeniştir ve müşterinin yanında hızlı sipariş almak önemlidir. Sipariş alımlarda ki hataların giderilmesi, sipariş iptalleri önemlidir.\r\nBar uygulamaları da mevcuttur. Klasik kasiyer uygulamasının yanısıra garsonların para tahsil etme uygulamasıda yaygındır. \r\nKullanıcı (garson) bazında detaylı raporlar önemlidir. Barda içecek alınıp daha sonra masaya geçme, dolayısı ile masalar arası\r\ntransferler ve masa ayırma/bölme gibi işlemler yoğun olarak uygulanır.', NULL, '&lt;p&gt;\r\n&lt;/p&gt;\r\n&lt;br&gt;\r\n&lt;h5&gt;POSMAKS / FASTFOOD ÖZELLİKLER&lt;/h5&gt;\r\n&lt;p&gt;\r\n&lt;/p&gt;\r\n&lt;div class=&quot;alert alert-info&quot; role=&quot;alert&quot;&gt;\r\n	&lt;b&gt;Esnek ve Kolay Arayüz&lt;/b&gt;\r\n&lt;/div&gt;\r\n&lt;div class=&quot;alert alert-info&quot; role=&quot;alert&quot;&gt;\r\n	&lt;b&gt;Kolay Anlaşılır Menü Yapısı&lt;/b&gt;&lt;br&gt;\r\n&lt;/div&gt;\r\n&lt;div class=&quot;alert alert-info&quot; role=&quot;alert&quot;&gt;\r\n	&lt;b&gt;Fatura / Adisyon Basımı&lt;/b&gt;\r\n&lt;/div&gt;\r\n&lt;div class=&quot;alert alert-info&quot; role=&quot;alert&quot;&gt;\r\n	&lt;b&gt;Sesli Sipariş Notu&lt;/b&gt;\r\n&lt;/div&gt;\r\n&lt;div class=&quot;alert alert-info&quot; role=&quot;alert&quot;&gt;\r\n	&lt;b&gt;Yazılı Sipariş Notu&lt;/b&gt;\r\n&lt;/div&gt;\r\n&lt;div class=&quot;alert alert-info&quot; role=&quot;alert&quot;&gt;\r\n	&lt;b&gt;Stok Takip Modülü&lt;/b&gt;\r\n&lt;/div&gt;\r\n&lt;div class=&quot;alert alert-info&quot; role=&quot;alert&quot;&gt;\r\n	&lt;b&gt;Personel Takip Modülü&lt;/b&gt;\r\n&lt;/div&gt;', NULL, '&lt;p&gt;\r\n&lt;/p&gt;\r\n&lt;p&gt;\r\n&lt;/p&gt;\r\n&lt;br&gt;\r\n&lt;h5&gt;POSMAKS / FASTFOOD AVANTAJLAR&lt;/h5&gt;\r\n&lt;p&gt;\r\n&lt;/p&gt;\r\n&lt;div class=&quot;alert alert-success&quot; role=&quot;success&quot;&gt;\r\n	&lt;b&gt;Aynı işletmede tek yazılım ile farklı donanım kullanabilme&lt;/b&gt;\r\n&lt;/div&gt;\r\n&lt;div class=&quot;alert alert-success&quot; role=&quot;success&quot;&gt;\r\n	&lt;b&gt;Akıllı giriş sistemi ile farklı fonksiyonları&amp;nbsp;personellere göre yetkilendirebilme&lt;/b&gt;\r\n&lt;/div&gt;\r\n&lt;div class=&quot;alert alert-success&quot; role=&quot;success&quot;&gt;\r\n	&lt;b&gt;Zincir işletmeler için tek noktadan tüm işletmelerin raporlarını görüntüleyebilme&lt;/b&gt;\r\n&lt;/div&gt;\r\n&lt;div class=&quot;alert alert-success&quot; role=&quot;success&quot;&gt;\r\n	&lt;b&gt;Gün içerisinde yapılan satışların ve stok durumunu görüntüleyebilme&lt;/b&gt;\r\n&lt;/div&gt;\r\n&lt;div class=&quot;alert alert-success&quot; role=&quot;success&quot;&gt;\r\n	&lt;b&gt;Günlük kazanç, gelir - gider tablosu görüntüleyebilme&lt;/b&gt;\r\n&lt;/div&gt;\r\n&lt;div class=&quot;alert alert-success&quot; role=&quot;success&quot;&gt;\r\n	&lt;b&gt;Tarih ve saat seçimi aralıkları ile toplu kazanç tablosu görüntüleyebilme&lt;/b&gt;\r\n&lt;/div&gt;\r\n&lt;div class=&quot;alert alert-success&quot; role=&quot;success&quot;&gt;\r\n	&lt;b&gt;Sesli sipariş özelliği ile daha ayrıntılı sipariş verebilme&lt;/b&gt;\r\n&lt;/div&gt;', NULL, '&lt;p&gt;DAHA FAZLA BİLGİ İÇİN&lt;/p&gt;', 'http://www.posmaks.com/page/iletisim/', 'TEKLİF İSTE', 'Toronto', 'Wojoscripts', 'IMG_EB86ED-039FE8-D2392F-2B0B93-F4E3B3-D8EC83.jpg', 0, 'quis,risus,commodo', 'quis,risus,commodo', NULL, 'Sed vitae nisi sapien, quis venenatis magna. Class aptent taciti sociosqu ad litora torquent per conubia…', '2014-04-08 00:00:00', '2016-08-31 18:13:26', 1, 't'),
(5, 4, 'sektor1', NULL, 'FASTFOOD', NULL, 'Genelde birden fazla kasa ve kasiyer bulunmaktadır. Bir numaralı amaç hızlı servistir. Kasiyerlerin servis hızı, gerektiğinde bir kaç müşteriye aynı anda hizmet verebilmesi önemlidir.Merkezi idare önemlidir. Servisin çok hızlı olması sebebiyle sipariş alımlarda ki hataların giderilmesi, sipariş iptalleri önemlidir.', NULL, '&lt;p&gt;\r\n&lt;/p&gt;\r\n&lt;br&gt;\r\n&lt;h5&gt;POSMAKS / FASTFOOD ÖZELLİKLER&lt;/h5&gt;\r\n&lt;p&gt;\r\n&lt;/p&gt;\r\n&lt;div class=&quot;alert alert-info&quot; role=&quot;alert&quot;&gt;\r\n	&lt;b&gt;Esnek ve Kolay Arayüz&lt;/b&gt;\r\n&lt;/div&gt;\r\n&lt;div class=&quot;alert alert-info&quot; role=&quot;alert&quot;&gt;\r\n	&lt;b&gt;Kolay Anlaşılır Menü Yapısı&lt;/b&gt;&lt;br&gt;\r\n&lt;/div&gt;\r\n&lt;div class=&quot;alert alert-info&quot; role=&quot;alert&quot;&gt;\r\n	&lt;b&gt;Fatura / Adisyon Basımı&lt;/b&gt;\r\n&lt;/div&gt;\r\n&lt;div class=&quot;alert alert-info&quot; role=&quot;alert&quot;&gt;\r\n	&lt;b&gt;Sesli Sipariş Notu&lt;/b&gt;\r\n&lt;/div&gt;\r\n&lt;div class=&quot;alert alert-info&quot; role=&quot;alert&quot;&gt;\r\n	&lt;b&gt;Yazılı Sipariş Notu&lt;/b&gt;\r\n&lt;/div&gt;\r\n&lt;div class=&quot;alert alert-info&quot; role=&quot;alert&quot;&gt;\r\n	&lt;b&gt;Stok Takip Modülü&lt;/b&gt;\r\n&lt;/div&gt;\r\n&lt;div class=&quot;alert alert-info&quot; role=&quot;alert&quot;&gt;\r\n	&lt;b&gt;Personel Takip Modülü&lt;/b&gt;\r\n&lt;/div&gt;', NULL, '&lt;p&gt;\r\n&lt;/p&gt;\r\n&lt;p&gt;\r\n&lt;/p&gt;\r\n&lt;br&gt;\r\n&lt;h5&gt;POSMAKS / FASTFOOD AVANTAJLAR&lt;/h5&gt;\r\n&lt;p&gt;\r\n&lt;/p&gt;\r\n&lt;div class=&quot;alert alert-success&quot; role=&quot;success&quot;&gt;\r\n	&lt;b&gt;Aynı işletmede tek yazılım ile farklı donanım kullanabilme&lt;/b&gt;&lt;/div&gt;\r\n&lt;div class=&quot;alert alert-success&quot; role=&quot;success&quot;&gt;\r\n	&lt;b&gt;Akıllı giriş sistemi ile farklı fonksiyonları&amp;nbsp;personellere göre yetkilendirebilme&lt;/b&gt;&lt;/div&gt;\r\n&lt;div class=&quot;alert alert-success&quot; role=&quot;success&quot;&gt;\r\n	&lt;b&gt;Zincir işletmeler için tek noktadan tüm işletmelerin raporlarını görüntüleyebilme&lt;/b&gt;&lt;/div&gt;\r\n&lt;div class=&quot;alert alert-success&quot; role=&quot;success&quot;&gt;\r\n	&lt;b&gt;Gün içerisinde yapılan satışların ve stok durumunu görüntüleyebilme&lt;/b&gt;&lt;/div&gt;\r\n&lt;div class=&quot;alert alert-success&quot; role=&quot;success&quot;&gt;\r\n	&lt;b&gt;Günlük kazanç, gelir - gider tablosu görüntüleyebilme&lt;/b&gt;&lt;/div&gt;\r\n&lt;div class=&quot;alert alert-success&quot; role=&quot;success&quot;&gt;&lt;b&gt;Tarih ve saat seçimi aralıkları ile toplu kazanç tablosu görüntüleyebilme&lt;/b&gt;&lt;/div&gt;\r\n&lt;div class=&quot;alert alert-success&quot; role=&quot;success&quot;&gt;&lt;b&gt;Sesli sipariş özelliği ile daha ayrıntılı sipariş verebilme&lt;/b&gt;&lt;/div&gt;', NULL, '&lt;p&gt;&lt;/p&gt;', 'http://www.posmaks.com/page/iletisim/', 'TEKLİF İSTE', '', '', 'IMG_D74BF6-3BCAB4-6BE9BD-CBE801-010088-F40F4E.jpg', 0, NULL, 'akıllı', NULL, 'Posmaks, satış noktası çözümleri, pos cihazları.', '2016-02-01 16:55:10', '2016-08-31 18:11:17', 1, 't');

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `mod_projects_category`
--

CREATE TABLE `mod_projects_category` (
  `id` int(4) NOT NULL,
  `title_en` varchar(100) NOT NULL,
  `title_tr` varchar(100) NOT NULL,
  `slug` varchar(100) NOT NULL,
  `metakey_en` varchar(100) DEFAULT NULL,
  `metakey_tr` varchar(100) NOT NULL,
  `metadesc_en` tinytext,
  `metadesc_tr` tinytext,
  `position` smallint(6) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Tablo döküm verisi `mod_projects_category`
--

INSERT INTO `mod_projects_category` (`id`, `title_en`, `title_tr`, `slug`, `metakey_en`, `metakey_tr`, `metadesc_en`, `metadesc_tr`, `position`) VALUES
(4, '', 'Sektörel Çözümler', 'sektorelcozumler', NULL, '', NULL, '', 0);

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `mod_solutions`
--

CREATE TABLE `mod_solutions` (
  `id` int(6) NOT NULL,
  `cid` int(6) NOT NULL DEFAULT '0',
  `slug` varchar(100) DEFAULT NULL,
  `title_en` varchar(100) DEFAULT NULL,
  `title_tr` varchar(100) NOT NULL,
  `short_desc_en` text,
  `short_desc_tr` text,
  `detail_en` text,
  `detail_tr` text,
  `body_en` text,
  `body_tr` text,
  `result_en` text,
  `result_tr` text,
  `www` varchar(100) DEFAULT NULL,
  `web_text` varchar(100) NOT NULL,
  `location` varchar(100) DEFAULT NULL,
  `client` varchar(100) DEFAULT NULL,
  `thumb` varchar(60) DEFAULT NULL,
  `gallery` smallint(4) NOT NULL DEFAULT '0',
  `metakey_en` varchar(200) DEFAULT NULL,
  `metakey_tr` varchar(200) NOT NULL,
  `metadesc_en` text,
  `metadesc_tr` text,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `selected` tinyint(1) NOT NULL DEFAULT '1',
  `layout` enum('t','l','') NOT NULL COMMENT 'T: galeri yukarıda, L:galeri solda'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Tablo döküm verisi `mod_solutions`
--

INSERT INTO `mod_solutions` (`id`, `cid`, `slug`, `title_en`, `title_tr`, `short_desc_en`, `short_desc_tr`, `detail_en`, `detail_tr`, `body_en`, `body_tr`, `result_en`, `result_tr`, `www`, `web_text`, `location`, `client`, `thumb`, `gallery`, `metakey_en`, `metakey_tr`, `metadesc_en`, `metadesc_tr`, `created`, `modified`, `selected`, `layout`) VALUES
(5, 1, 'gomulu-sistem-cozumleri', NULL, 'GÖMÜLÜ SİSTEM ÇÖZÜMLERİ', '', 'Gömülü sistemler, kendileri için özel olarak tanımlanmış görevleri çekirdeği olan mikroişlemciler ya da mikrodenetleyiciler tarafından yerine getirir. Kodar, Gömülü sistem tasarımı, gömülü sistem yazılımı ve tüm bunların ötesinde projeye uygun devre elemanlarının belirlenmesi noktasında ileri teknoloji ürünler ile paydaşlarına düşük maliyetlerde yüksek teknolojik çözümler sunar.', '', '&lt;p&gt;\r\n	Gömülü sistemlerde, projeye uygun haberleşme frekanslarının kullanılması büyük önem taşır. Örneğin: 50 dönümlük bir&amp;nbsp;fabrikada yüzlerce sensörden bir noktaya ya da internete&amp;nbsp;veri aktarılması gerekiyor ise bunun birçok yöntemi vardır. Bu yöntemlerden biri kablo çekilmesi, bir diğeri personelin elinde mobil cihaz ile tüm sensörlerin çevresinde gezinmesi gibi birçok yöntem bulunabilir. Ancak&amp;nbsp;en düşük maliyetli ve en yüksek performansta çalışacak bir sistem kurgulamak, gömülü sistemler ve haberleşme protokollerinde Dünya&#039;daki gelişmeleri yakından takip etmenizi,&amp;nbsp;tecrübe ve deneyim edinmenizi&amp;nbsp;gerektirir. Bahsedilen örneğe mantıklı çözümlerden biri&amp;nbsp;ise KODAR&#039;ın geliştirme yaptığı protokollerden,LORA WAN network mimarisidir. LORA WAN network mimarisi ile kablosuz olarak kilometrelerce haberleşme sağlanabiliyor. Gömülü sistemler alanında özellikle haberleşme, konum tespiti, takip ve veri toplama gibi alanlarda deneyimlere sahip KODAR, projelerinizde&amp;nbsp;katma değeri yüksek çözümler üretmektedir.\r\n&lt;/p&gt;&lt;p&gt;&lt;/p&gt;', NULL, '&lt;h5 style=&quot;text-align: center;&quot;&gt;Kablosuz Haberleşme Alanında&amp;nbsp;Geliştirme Yaptığımız Protokoller&lt;/h5&gt;\r\n&lt;p style=&quot;text-align: center;&quot;&gt;\r\n	&lt;img src=&quot;uploads/images/gomulu-sistem-protokol.jpg&quot; alt=&quot;gomulu-sistem-protokol-zigbee-bluetooth-wi-fi-lora&quot;&gt;\r\n&lt;/p&gt;', NULL, '&lt;p style=&quot;text-align:center&quot;&gt;\r\n	DAHA FAZLA BİLGİ İÇİN\r\n&lt;/p&gt;', 'http://www.kodar.com.tr/page/iletisim/', 'BİZİMLE İLETİŞİME GEÇİN', 'Toronto', '', 'IMG_39A746-0E6B61-41CA13-E11328-D2E585-614D1A.jpg', 0, NULL, 'gömülü sistem çözümleri, zigbee, bloetooth, wi-fi, lora, kablosuz haberşme, personel takip, izleme', NULL, 'Gömülü sistemler, kendileri için özel olarak tanımlanmış görevleri çekirdeği olan mikroişlemciler ya da mikrodenetleyiciler tarafından yerine getirir. Kodar, Gömülü sistem tasarımı, gömülü sistem yazılımı ve tüm bunların ötesinde projeye uygun devre elemanlarının belirlenmesi noktasında ileri teknoloji ürünler ile paydaşlarına düşük maliyetlerde yüksek teknolojik çözümler sunar.', '2014-04-08 00:00:00', '2016-03-04 03:07:35', 0, 'l'),
(9, 1, 'tasarim-cozumleri', NULL, 'TASARIM ÇÖZÜMLERİ', '', 'Tasarım bir mesajı iletmek, bir görseli geliştirmek veya bir düşünceyi görselleştirmek için metnin ve görsellerin algılanabilir ve görülebilir bir düzlemde, iki boyutlu veya üç boyutlu olarak organize edilmesini içeren yaratıcı bir süreçtir. Kodar Creative ekibi, basılı, dijital, hareketli film, animasyon, ambalaj tasarımı gibi birçok ortamda dijital veya dijital olmayan tasarımlar geliştirmektedir.', NULL, '&lt;p&gt;Tasarımın çıkış noktası ise; sanayileşme ve modern yaşama geçiş ile, özellikle de fotoğrafın keşfi ile ortaya çıkan İzlenimcilik ve Post-İzlenimcilik akımlarının sonrasında başlamıştır. Çünkü resim sanatı farklı bir yöne ilerlemeye başlamış ve grafik, afiş, ürün katalogları vb. öne çıkmaya başlamıştır. Gazetenin ortaya çıkmasıyla reklam ve tanıtım öne çıkmıştır. Örneğin; ürün katalogları ilk önceleri fotoğraflarla değil gravür baskılar ile yapılmaktaydı. İşlerin tanıtımını ve duyurusunu yapan afişler de kendi içerisinde ayrı bir alan haline geliyordu. Bu alanlarda ilk çalışanlar da grafiker, grafik sanatçısı veya tasarımcı değil ressamlardı. Bu yüzden resimsel özellikleri önde, tipografik özellikleri geri planda kalıyordu. Fakat baskı tekniklerinin ilerlemesi, fotoğrafın geliştirilmesi ve tipografinin önem kazanması ile özellikle afiş tasarımı ve dolayısıyla grafik sanatlar resimden ayrı, tasarımın birer dalı olarak ortaya çıkmıştır. &lt;/p&gt;&lt;p&gt;Tasarımlar,&amp;nbsp;görsel sanatların temel ilkeleri olan hizalama, denge, karşıtlık, vurgulama, hareket, görüntü, oran, yakınlık, tekrarlama, ritim ve birlik kurgularına büyük&amp;nbsp;özen göstermek kaydıyla içeriğe uygun olarak üretilmelidir.&lt;/p&gt;&lt;p&gt;Günümüzde ise teknolojinin gelişimi ile rekabet edebilme, müşterinin kalbinde yer edinebilmek için&amp;nbsp;reklam,&amp;nbsp;tanıtım faaliyetlerinde dijital ve basılı&amp;nbsp;tasarımlar ihtiyaç haline dönüşmüştür.&amp;nbsp;Marka, ürün ve hizmetlerin müşteriye sunumu açısından büyük önem arz eden tasarım çalışmaları,&amp;nbsp;müşteri ile&amp;nbsp;iletişimi güçlendirmeye yönelik tanıtım faaliyetlerinde doğru uygulandığında marka değeri ve pazarlama açısından&amp;nbsp;oldukça etkilidir. &lt;/p&gt;', NULL, '&lt;p&gt;Kodar Creative, tüm bu süreçlerde uzman ekibi ile özellikle&amp;nbsp;dijital tasarım hizmetleri kapsamında&amp;nbsp;web site tasarımları, kullanıcı dostu yazılım arayüzleri, mobil uygulama tasarımı&amp;nbsp;ve&amp;nbsp;sosyal medya&amp;nbsp;tasarım&amp;nbsp;yönetimi gibi&amp;nbsp;konularında hizmet sunmaktadır. Ayrıca&amp;nbsp;kurumsal kimlik kimlik bütünlüğünü sağlamak amacı ile&amp;nbsp;video, animasyon ve basılı tasarım hizmetleri ile uçtan uca etkili tasarım ajansı görevi üstlenmektedir.&lt;/p&gt;', NULL, '&lt;p style=&quot;text-align:center&quot;&gt;\r\n	DAHA FAZLA BİLGİ İÇİN\r\n&lt;/p&gt;', 'http://www.kodar.com.tr/page/iletisim/', 'BİZİMLE İLETİŞİME GEÇİN', 'Toronto', '', 'IMG_3D00EC-D510DD-A6FA56-1E7CE9-5908DB-CBA180.jpg', 0, NULL, 'video, animasyon, basılı tasarım hizmetleri, web site tasarımı, kullanıcı dostu, yazılım tasarımı, mobil uygulama tasarımı, sosyal medya tasarım', NULL, 'Tasarım bir mesajı iletmek, bir görseli geliştirmek veya bir düşünceyi görselleştirmek için metnin ve görsellerin algılanabilir ve görülebilir bir düzlemde, iki boyutlu veya üç boyutlu olarak organize edilmesini içeren yaratıcı bir süreçtir. Kodar Creative ekibi, basılı, dijital, hareketli film, animasyon, ambalaj tasarımı gibi birçok ortamda dijital veya dijital olmayan tasarımlar geliştirmektedir.', '2014-04-08 00:00:00', '2016-03-04 01:37:23', 0, 'l'),
(10, 1, 'mobil-cozumler', NULL, 'MOBİL ÇÖZÜMLER', '', 'Akıllı telefonların yaygınlaşmasıyla beraber insanların bilgiye daha kolay ulaşabilmesi noktasında önemli bir konuma gelen mobil uygulamaların pazarı da her geçen gün hızla büyümektedir. Mobil cihaz kullanıcılarının hızla artması, şirketleri akıllı telefon-tablet kullanıcılarına yönelik, kullanıcı dostu arayüzlere sahip mobil uygulamaları pazara sunmaya itmektedir.', NULL, '&lt;p&gt;Dünya&#039;nın en yeni teknolojilerini yakından takip eden, ulusal ve uluslararası alanda çeşitli proje deneyimlerine sahip KODAR, mobil uygulama geliştirme konusunda paydaşlarına benzersiz çözümler sunuyor. &lt;/p&gt;&lt;p&gt;KODAR;&amp;nbsp;Iphone, Ipad, Android, Blackberry, Windows Mobile, Windows Phone 8 platformlarında çalışan yerel (native) mobil uygulamaların yanında, Html 5 teknolojisini kullanarak (Hybrid) mobil uygulamalar da geliştirmektedir.&amp;nbsp;&lt;/p&gt;&lt;p&gt;KODAR; yaratıcı, yenilikçi ve kullanışlı mobil uygulamalar geliştirme konusunda, projenin tüm süreçlerinde (fikir aşamasından, tasarım, geliştirme ve yayınlanması) uzman kadrosu ile hizmet vermektedir.&lt;/p&gt;', NULL, '&lt;p&gt;Navigasyon, Mikro&amp;nbsp;Lokasyon Bazlı Servisler, Kapalı Alan Yönlendirme ve Bilgilendirme Sistemleri,&amp;nbsp;Sosyal Medya Uygulamaları, Veri Toplama, Kurumsal Mobil Uygulamalar,&amp;nbsp;Mobil E-Ticaret Uygulamaları, Mobil Ödeme Sistemleri vb. konularında yazılımlar ve çözümler sunmaktayız. &lt;/p&gt;', NULL, '&lt;p style=&quot;text-align:center&quot;&gt;\r\n	DAHA FAZLA BİLGİ İÇİN\r\n&lt;/p&gt;', 'http://www.kodar.com.tr/page/iletisim/', 'BİZİMLE İLETİŞİME GEÇİN', 'Toronto', '', 'IMG_6EBA0A-B70D4C-6E8C04-56144D-220427-DC57B4.jpg', 0, NULL, 'mobil yazılım, mobil uygulama geliştirme, ios, android, windows phone, mobil çözümler', NULL, 'Akıllı telefonların yaygınlaşmasıyla beraber insanların bilgiye daha kolay ulaşabilmesi noktasında önemli bir konuma gelen mobil uygulamaların pazarı da her geçen gün hızla büyümektedir. Mobil cihaz kullanıcılarının hızla artması, şirketleri akıllı telefon-tablet kullanıcılarına yönelik, kullanıcı dostu arayüzlere sahip mobil uygulamaları pazara sunmaya itmektedir.', '2014-04-08 00:00:00', '2016-03-03 21:14:51', 1, 'l'),
(12, 1, 'izleme-ve-takip-sistemleri', NULL, 'İZLEME VE TAKİP SİSTEMLERİ', '', '​İzleme ve Takip sistemleri alanında çeşitli çözümlerimiz ile kişi ve nesnelerin izlenmesi, envanter hareketlerinin anlık takibi gibi konularda işletmelere veri ve kontrol mekanizmalarının geliştirilmesi anlamında büyük destek sağlamaktayız.', NULL, '&lt;p&gt;KODAR, mikro lokasyonlarda kişi ve nesne takibi konusunda Bluetooth Low Energy protokolünde sunduğu çözümler ile sadece giriş çıkış takibi, demirbaş sayımı, nesne hareketlerini izleme konusunda değil, aynı zamanda ortam verilerinin takibi konusunda da çok yönlü çözümler sunmaktadır.&lt;/p&gt;  &lt;p&gt;Sıcaklık, nem, hava değişimleri ve benzer değerlerin belirli bir referans aralığında tutulması ilaç sektöründen tarıma, sağlıktan kütüphaneciliğe kadar çok farklı sektörlerde önemli etkilere sahip. Gelişen teknoloji, yakın bir geçmişe kadar manuel olarak ölçülen değerlerin günümüzde sensörler tarafından otomatik olarak ölçülmesinin, hatta gerekli durumlarda otomatik olarak müdahale edilebilmesinin de önünü açtı. Yeni dönemde kullanılan bu sistemler aynı zamanda insandan kaynaklı hataları da en aza indirerek, etkin izleme ve raporlama çözümleri imkanını da beraberinde getirdi.&lt;/p&gt;', NULL, '&lt;p&gt;Birbiriyle veri iletişimi içinde olan makinelerden oluşan bütünleşik bir sistemi kullanıcılarına sunabilen KODAR, kapsamlı ortam değer takibi çözümlerini M2M teknolojileriyle birleştirerek paydaşlarına saha izleme çözümleri ile&amp;nbsp;eşsiz deneyimler yaşatıyor. &lt;/p&gt;', NULL, '&lt;p style=&quot;text-align:center&quot;&gt;\r\n	DAHA FAZLA BİLGİ İÇİN\r\n&lt;/p&gt;', 'http://www.kodar.com.tr/page/iletisim/', 'BİZİMLE İLETİŞİME GEÇİN', 'Toronto', '', 'IMG_02D61E-C9DD6E-63092A-796BC8-8089A6-ED98E8.jpg', 0, NULL, 'izleme ve takip, personel izleme, kişi izleme, envanter takip, depo takip, stok takip', NULL, '​İzleme ve Takip sistemleri alanında çeşitli çözümleri ile KODAR, Kişi ve nesnelerin izlenmesi, envanter hareketlerinin anlık takibi gibi konularda işletmelere veri ve kontrol mekanizmalarının geliştirilmesi anlamında büyük destek sağlamaktadır.', '2014-04-08 00:00:00', '2016-03-03 03:52:00', 1, 'l'),
(13, 1, 'yazilim-cozumleri', NULL, 'YAZILIM ÇÖZÜMLERİ', '', 'Marka değerinin arttırılması, iş süreçlerinin kolaylaştırılması ve hızlandırılması için ulusal ve uluslararası piyasalarda rekabet avantajı sağlamak isteyen şirketler, iş yapısına ve teknolojik olanakların işe yönelik sunabileceği avantajlara dair özel yazılım çözümleri arayışı içindedir.', NULL, '&lt;p&gt;İşe özgü yenilikçi fikirleri ile yazılım geliştirme ve proje yönetiminde uzman ekiplere sahip KODAR, şirketlere özel yazılım çözümleri geliştirirken sağlam bir teknolojik altyapının yanı sıra kullanıcı dostu, kolay kullanılabilir, profesyonel tasarım hizmeti de sunmaktadır. &lt;/p&gt;&lt;p&gt;Yazılım çözümleri kapsamında aşağıdaki hizmetleri sunuyoruz;&lt;/p&gt;', NULL, '&lt;div class=&quot;alert-style2&quot;&gt;\r\n\r\n\r\n&lt;div class=&quot;alert alert-info&quot; role=&quot;alert&quot;&gt;\r\n&lt;strong&gt;\r\nWeb Sitesi Geliştirme\r\n&lt;/strong&gt;\r\n &lt;/div&gt;\r\n\r\n&lt;div class=&quot;alert alert-info&quot; role=&quot;alert&quot;&gt;\r\n&lt;strong&gt;\r\nİçerik Yönetim Sistemi (VeriAsist)\r\n&lt;/strong&gt;\r\n &lt;/div&gt;\r\n\r\n&lt;div class=&quot;alert alert-info&quot; role=&quot;alert&quot;&gt;\r\n&lt;strong&gt;\r\nAnaliz\r\n&lt;/strong&gt;\r\n &lt;/div&gt;\r\n\r\n&lt;div class=&quot;alert alert-info&quot; role=&quot;alert&quot;&gt;\r\n&lt;strong&gt;\r\nİş Zekası Uygulamaları\r\n&lt;/strong&gt;\r\n &lt;/div&gt;\r\n\r\n&lt;div class=&quot;alert alert-info&quot; role=&quot;alert&quot;&gt;\r\n&lt;strong&gt;\r\nOnline Pazarlama ve E-ticaret\r\n&lt;/strong&gt;\r\n &lt;/div&gt;\r\n\r\n&lt;div class=&quot;alert alert-info&quot; role=&quot;alert&quot;&gt;\r\n&lt;strong&gt;\r\nBakım Destek ve Geliştirme\r\n&lt;/strong&gt;\r\n &lt;/div&gt;\r\n\r\n &lt;/div&gt;\r\n', NULL, '&lt;p style=&quot;text-align:center&quot;&gt;\r\n	DAHA FAZLA BİLGİ İÇİN\r\n&lt;/p&gt;', 'http://www.kodar.com.tr/page/iletisim/', 'BİZİMLE İLETİŞİME GEÇİN', 'Toronto', '', 'IMG_2DF28B-591B53-F72B56-F61446-FA6899-E9C872.jpg', 0, NULL, 'YAZILIM ÇÖZÜMLERİ', NULL, 'YAZILIM ÇÖZÜMLERİ', '2017-05-31 00:00:00', '2016-03-01 00:45:02', 0, 'l'),
(14, 1, 'iot-m2m-cozumleri', NULL, 'IOT &amp; M2M ÇÖZÜMLERI', NULL, 'Dünya&#039;daki milyarlarca kişinin birbirleri ile etkileşim içinde olmalarını ve iletişimin hızını arttıran internet ve gelişen teknolojiler aynı zamanda nesnelerin de birbirleriyle iletişim kurabileceği yeni bir dünyanın kapılarını açtı.', NULL, '&lt;p&gt;IOT &amp;amp; M2M projeleri, günümüzde sadece ileri teknolojiye sahip ya da büyük bütçeler ile gerçekleştirilebilecek projeler olduğu sanılsada Nesnelerin İnterneti (IoT), çok yakın gelecekte herkesin konuşacağı ve hayatını şekillendirici bir teknoloji olma yolunda ilerliyor. Internet of Things (Nesnelerin İnterneti) sayesinde birbirleri ile iletişim kurabilen nesneler araçlarımızda, evlerimizde ve işimizde büyük değişiklikler yaratacak.&lt;/p&gt;  &lt;p&gt;Makinalar arası iletişim (M2M) ise yakın gelecekte sıkça duyacağımız kavramlardan biri olacaktır. Çeşitli üretim endüstrilerinde kullanımı hızla yaygınlaşacak olan Makinalar arası iletişim (M2M) ile üretim maliyetleri azalacak, üretim izleme ve kontrol sistemleri gelişecek ve son tüketiciye daha uygun fiyatla sunulabilecek ürünler üretilecektir.&lt;/p&gt;', NULL, '&lt;p&gt;KODAR’ın IOT &amp;amp; M2M ÇÖZÜMLERİ işletmelerin çeşitli alanlarda ihtiyaçlarına odaklanarak katma değer odaklı inovatif projelere dönüşmektedir.&amp;nbsp;&lt;/p&gt;', NULL, '&lt;p style=&quot;text-align:center&quot;&gt;\r\n	DAHA FAZLA BİLGİ İÇİN\r\n&lt;/p&gt;', 'http://www.kodar.com.tr/page/iletisim/', 'BİZİMLE İLETİŞİME GEÇİN', '', '', 'IMG_D79633-80915C-AD3BB5-872C9C-A01B59-068100.jpg', 0, NULL, 'internet of things, iot, nesnelerin interneti, m2m, makinaların haberleşmesi, sensör', NULL, 'IOT &amp; M2M projeleri, günümüzde sadece ileri teknolojiye sahip ya da büyük bütçeler ile gerçekleştirilebilecek projeler olduğu sanılsada Nesnelerin İnterneti (IoT), çok yakın gelecekte herkesin konuşacağı ve hayatını şekillendirici bir teknoloji olma yolunda ilerliyor. Ineternet of Things (Nesnelerin İnterneti) sayesinde birbirleri ile iletişim kurabilen nesneler araçlarımızda, evlerimizde ve işimizde büyük değişiklikler yaratacak.', '2016-02-01 16:55:10', '2016-02-29 23:47:39', 1, 'l');

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `mod_solutions_category`
--

CREATE TABLE `mod_solutions_category` (
  `id` int(4) NOT NULL,
  `title_en` varchar(100) NOT NULL,
  `title_tr` varchar(100) NOT NULL,
  `slug` varchar(100) NOT NULL,
  `metakey_en` varchar(100) DEFAULT NULL,
  `metakey_tr` varchar(100) NOT NULL,
  `metadesc_en` tinytext,
  `metadesc_tr` tinytext,
  `position` smallint(6) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Tablo döküm verisi `mod_solutions_category`
--

INSERT INTO `mod_solutions_category` (`id`, `title_en`, `title_tr`, `slug`, `metakey_en`, `metakey_tr`, `metadesc_en`, `metadesc_tr`, `position`) VALUES
(1, 'Webdesign', 'TÜM ÇÖZÜMLER', 'tum-cozumler', 'webdesign', 'TÜM ÇÖZÜMLER', 'portfolio category webdesign', 'TÜM ÇÖZÜMLER', 1),
(2, 'Illustration', 'Illustration', 'illustration', NULL, '', NULL, '', 2),
(3, 'Photography', 'Photography', 'photography', NULL, '', NULL, '', 3);

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `mod_timeline`
--

CREATE TABLE `mod_timeline` (
  `id` int(6) NOT NULL,
  `name` varchar(100) DEFAULT NULL,
  `plugin_id` varchar(100) DEFAULT NULL,
  `type` varchar(50) DEFAULT NULL,
  `limiter` int(1) NOT NULL DEFAULT '0',
  `showmore` tinyint(1) NOT NULL DEFAULT '0',
  `maxitems` tinyint(1) NOT NULL DEFAULT '0',
  `colmode` varchar(20) DEFAULT 'dual',
  `readmore` varchar(150) DEFAULT NULL,
  `rssurl` varchar(200) DEFAULT NULL,
  `fbpage` bigint(2) DEFAULT '0',
  `fbtoken` varchar(150) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `mod_timeline_items`
--

CREATE TABLE `mod_timeline_items` (
  `id` int(11) NOT NULL,
  `tid` int(11) NOT NULL DEFAULT '0',
  `type` varchar(30) DEFAULT NULL,
  `title_en` varchar(100) DEFAULT NULL,
  `title_tr` varchar(100) NOT NULL,
  `body_en` text,
  `body_tr` text,
  `images` text,
  `dataurl` text,
  `height` smallint(3) NOT NULL DEFAULT '300',
  `readmore` varchar(200) DEFAULT NULL,
  `sorting` int(1) NOT NULL DEFAULT '0',
  `created` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `mod_websites`
--

CREATE TABLE `mod_websites` (
  `id` int(11) NOT NULL,
  `folder` varchar(150) NOT NULL,
  `desc` text,
  `customer` text,
  `deleted` datetime DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `status` tinyint(1) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Tablo döküm verisi `mod_websites`
--

INSERT INTO `mod_websites` (`id`, `folder`, `desc`, `customer`, `deleted`, `created`, `modified`, `status`) VALUES
(1, 'kodar', NULL, '{"name":"Murat Dikmen","company":"KODAR","phone":"5325151443","email":"mdikmen@kodarbilisim.com"}', NULL, '2016-11-26 15:11:48', '2016-11-26 15:12:15', 1),
(2, 'demo', NULL, '{"name":"DENEME","company":"DENEME","phone":"4445555","email":"teraphi@gmail.com"}', '2016-11-26 15:29:00', '2016-11-26 15:27:17', NULL, -1),
(3, 'demo', '', '{"name":"teneme deneme","company":"detee","phone":"5325151443","email":"muratdikmen@kodar.com.tr"}', '2017-01-06 11:49:09', '2016-11-29 03:39:51', '2016-11-29 03:40:20', 1),
(4, 'serhan', NULL, '{"name":"Serhan YAV\\u00c7\\u0130N","company":"SERHAN RESTAURANT","phone":"5330413660","email":"serhan.yavcin@kodar.com.tr"}', NULL, '2016-12-01 16:43:59', '2016-12-01 16:44:34', 1),
(5, 'emrah', NULL, '{"name":"Ermah Oru\\u00e7","company":"Kodar","phone":"05375522706","email":"eoruc@kodar.com.tr"}', NULL, '2016-12-05 11:15:19', '2016-12-05 11:15:54', 1),
(6, 'abdullah', '', '{"name":"ABDULLAH BOZOK","company":"ABDULLAH","phone":"05542947180","email":"abdullahtugce@gmail.com"}', NULL, '2016-12-19 22:53:22', '2016-12-19 22:53:42', 1),
(7, 'demo', '', '{"name":"Demo Kullan\\u0131c\\u0131","company":"Demo \\u015eirket","phone":"00902163520202","email":"bilgi@posmaks.com"}', '2017-01-06 12:12:14', '2017-01-06 12:04:40', '2017-01-06 12:11:38', 1),
(8, 'demo', NULL, '{"name":"Demo Kullan\\u0131c\\u0131","company":"Demo \\u015eirket","phone":"00902163520202","email":"bilgi@posmaks.com"}', NULL, '2017-01-06 12:12:55', '2017-01-06 12:13:07', 1),
(9, 'prelect', NULL, '{"name":"Levent Sa\\u011f\\u0131ro\\u011flu","company":"prelect","phone":"02163607484","email":"levent.sag1773@gmail.com"}', NULL, '2017-01-06 16:12:43', '2017-01-06 16:13:52', 1),
(10, 'piro', NULL, '{"name":"murat piro","company":"piro","phone":"02163607484","email":"murat.piro@tr.anadoluefes.com"}', '2017-01-12 11:20:04', '2017-01-09 13:05:07', NULL, -1),
(11, 'deneme3', NULL, '{"name":"gjg","company":"jhhj","phone":"65667657675","email":"info@tasrakabare.com"}', NULL, '2017-01-26 18:16:20', '2017-01-26 18:16:28', 1),
(12, '911', NULL, '{"name":"Emrah Oru\\u00e7","company":"Kodar","phone":"05375522706","email":"orucemrahis@gmail.com"}', NULL, '2017-02-04 11:26:56', '2017-02-04 11:27:39', 1),
(13, 'test', 'test', '{"name":"test","company":"test","phone":"","email":""}', NULL, '2017-05-08 20:31:16', '2017-05-08 20:31:35', 1),
(14, 'demoeoruc', NULL, '{"name":"emrah oru\\u00e7","company":"kodar","phone":"0036520234","email":"eoruc@kodar.com.tr"}', NULL, '2017-05-25 12:11:17', '2017-05-25 12:12:59', 1),
(15, 'posmakskodar', NULL, '{"name":"MURAT D\\u0130KMEN","company":"KODAR B\\u0130L\\u0130\\u015e\\u0130M","phone":"05325151443","email":"bilgi@kodar.com.tr"}', NULL, '2017-05-25 19:05:43', '2017-05-26 01:06:29', 1),
(16, 'eoruc1', NULL, '{"name":"e","company":"o","phone":"0036520234","email":"orucemrahis@gmail.com"}', NULL, '2017-05-26 15:28:08', NULL, -1),
(17, 'eoruc2', NULL, '{"name":"e","company":"o","phone":"05375522706","email":"orucemrahis@gmail.com"}', NULL, '2017-05-26 15:29:30', NULL, -1);

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `notes`
--

CREATE TABLE `notes` (
  `id` int(11) NOT NULL,
  `username` varchar(40) DEFAULT NULL,
  `body_en` varchar(200) DEFAULT NULL,
  `color` varchar(7) DEFAULT NULL,
  `created` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `pages`
--

CREATE TABLE `pages` (
  `id` int(11) NOT NULL,
  `title_en` varchar(200) NOT NULL,
  `title_tr` varchar(200) NOT NULL,
  `slug` varchar(150) NOT NULL,
  `caption_en` varchar(150) DEFAULT NULL,
  `caption_tr` varchar(200) NOT NULL,
  `is_admin` tinyint(1) NOT NULL DEFAULT '1',
  `home_page` tinyint(1) NOT NULL DEFAULT '0',
  `contact_form` tinyint(1) NOT NULL DEFAULT '0',
  `login` tinyint(1) NOT NULL DEFAULT '0',
  `activate` tinyint(1) NOT NULL DEFAULT '0',
  `account` tinyint(1) NOT NULL DEFAULT '0',
  `register` tinyint(1) NOT NULL DEFAULT '0',
  `search` tinyint(1) NOT NULL DEFAULT '0',
  `sitemap` tinyint(1) NOT NULL DEFAULT '0',
  `profile` tinyint(1) NOT NULL DEFAULT '0',
  `membership_id` varchar(20) NOT NULL DEFAULT '0',
  `module_id` int(4) NOT NULL DEFAULT '0',
  `module_data` varchar(100) NOT NULL DEFAULT '0',
  `module_name` varchar(50) DEFAULT NULL,
  `custom_bg` varchar(100) DEFAULT NULL,
  `theme` varchar(60) DEFAULT NULL,
  `access` enum('Public','Registered','Membership') NOT NULL DEFAULT 'Public',
  `body_en` text,
  `body_tr` text,
  `jscode` text,
  `keywords_en` text NOT NULL,
  `keywords_tr` text,
  `description_en` text NOT NULL,
  `description_tr` text,
  `created` datetime NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Tablo döküm verisi `pages`
--

INSERT INTO `pages` (`id`, `title_en`, `title_tr`, `slug`, `caption_en`, `caption_tr`, `is_admin`, `home_page`, `contact_form`, `login`, `activate`, `account`, `register`, `search`, `sitemap`, `profile`, `membership_id`, `module_id`, `module_data`, `module_name`, `custom_bg`, `theme`, `access`, `body_en`, `body_tr`, `jscode`, `keywords_en`, `keywords_tr`, `description_en`, `description_tr`, `created`, `active`) VALUES
(15, 'Login Page', 'Login Page', 'login', NULL, '', 1, 0, 0, 1, 0, 0, 0, 0, 0, 0, '0', 0, '0', NULL, NULL, NULL, 'Public', NULL, '', NULL, '', '', '', '', '2014-04-26 22:11:36', 1),
(16, 'User Registration', 'User Registration', 'registration', NULL, '', 1, 0, 0, 0, 0, 0, 1, 0, 0, 0, '0', 0, '0', NULL, NULL, NULL, 'Public', NULL, '', NULL, '', '', '', '', '2014-04-27 01:22:53', 1),
(18, 'User Dashboard', 'User Dashboard', 'dashborad', NULL, '', 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, '0', 0, '0', NULL, NULL, NULL, 'Public', NULL, '', NULL, '', '', '', '', '2014-04-27 14:06:43', 1),
(19, 'Search Results', 'Search Results', 'search', NULL, '', 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, '0', 0, '0', NULL, NULL, NULL, 'Public', '', '', NULL, '', '', '', '', '2014-04-28 23:32:44', 1),
(20, 'Sitemap', 'Sitemap', 'sitemap', NULL, '', 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, '0', 0, '0', NULL, NULL, NULL, 'Public', '', '', NULL, '', '', '', '', '2014-05-07 17:00:53', 1),
(1, 'Welcome To Cms pro', 'ANASAYFA', 'home', '', 'ANASAYFA', 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, '0', 0, '0', NULL, NULL, NULL, 'Public', '', '&lt;p&gt;&lt;/p&gt;', NULL, 'CMS pro, Content Menagement System, Lightweight CMS', 'restaurant otomasyon sistemi, restaurant yazılımı, cafe yazılımı, adisyon yazılımı, restoran programı, restaurant programı', 'Cms pro is a web content management system made for the peoples who don&#039;t have much technical knowledge of HTML or PHP but know how to use a simple notepad with computer keyboard.', 'Posmaks Bulut Tabanlı Pos Sistemi, kurulum gerektirmez. Bulut tabanlı (Cloud based) özelliği sayesinde internet aracılığı ile dünyanın heryerinden ulaşabilme imkanı tanır. Verileriniz çevrimiçi ortamda yani bulutta depolanır. İşlemlerinizi herhangi bir cihaz üzerinden yönetilebilme imkanına sahip olursunuz.', '2014-01-26 22:11:36', 1),
(2, 'What is CMS pro!', 'ÖZELLİKLER', 'ozellikler', '', '', 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, '0', 0, '0', NULL, 'maxresdefault.jpg', NULL, 'Public', '&lt;div&gt;\n	 &lt;img class=&quot;wojo image&quot; alt=&quot;sampleimage_4.jpg&quot; src=&quot;uploads/images/pages/sampleimage_4.jpg&quot; title=&quot;sampleimage_4.jpg&quot;&gt;\n&lt;/div&gt;\n&lt;hr&gt;\n&lt;p&gt;\n	 Morbi sodales accumsan arcu sed venenatis. Vivamus leo diam, dignissim eu convallis in, posuere quis magna. Curabitur mollis, lectus sit amet bibendum faucibus, nisi ligula ultricies purus, in malesuada arcu sem ut mauris. Proin lobortis rutrum ultrices.\n&lt;/p&gt;\n&lt;hr&gt;\n&lt;div class=&quot;two columns gutters&quot;&gt;\n	&lt;div class=&quot;row&quot;&gt;\n		&lt;h3&gt;What do we do?&lt;/h3&gt;\n		&lt;div class=&quot;accordion&quot;&gt;\n			&lt;div class=&quot;header&quot;&gt;\n				 &lt;i class=&quot;icon lab&quot;&gt;&lt;/i&gt; Webdesign\n			&lt;/div&gt;\n			&lt;div class=&quot;content&quot;&gt;\n				&lt;p&gt;\n					 Quisque sagittis consequat elit non scelerisque. Cum sociis natoque et magnis dis montes, nascetur ridiculus mus. Sed sed leo, et risus consequat.\n				&lt;/p&gt;\n			&lt;/div&gt;\n			&lt;div class=&quot;header&quot;&gt;\n				 &lt;i class=&quot;icon setting&quot;&gt;&lt;/i&gt; Web Dewelopment\n			&lt;/div&gt;\n			&lt;div class=&quot;content&quot;&gt;\n				&lt;p&gt;\n					 Quisque sagittis consequat elit non scelerisque. Cum sociis natoque et magnis dis montes, nascetur ridiculus mus. Sed sed leo, et risus.\n				&lt;/p&gt;\n			&lt;/div&gt;\n			&lt;div class=&quot;header&quot;&gt;\n				 &lt;i class=&quot;icon umbrella&quot;&gt;&lt;/i&gt; Responsivness\n			&lt;/div&gt;\n			&lt;div class=&quot;content&quot;&gt;\n				&lt;p&gt;\n					 Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Sed sed leo, et risus. Mauris tincidunt interdum adipiscing. Cum sociis natoque et magnis dis montes, nascetur ridiculus mus..\n				&lt;/p&gt;\n				&lt;p&gt;\n					 Phasellus dolor enim, faucibus egestas scelerisque. Cum sociis natoque et magnis dis montes nascetur.\n				&lt;/p&gt;\n			&lt;/div&gt;\n			&lt;div class=&quot;header&quot;&gt;\n				 &lt;i class=&quot;icon star&quot;&gt;&lt;/i&gt; Internet Marketing\n			&lt;/div&gt;\n			&lt;div class=&quot;content&quot;&gt;\n				&lt;p&gt;\n					 Quisque sagittis consequat elit non scelerisque. Cum sociis natoque et magnis dis montes, nascetur ridiculus mus. Sed sed leo, et risus. Sed sed leo risus mauris.\n				&lt;/p&gt;\n			&lt;/div&gt;\n		&lt;/div&gt;\n	&lt;/div&gt;\n	&lt;div class=&quot;row&quot;&gt;\n		&lt;h3&gt;Skills and expertise&lt;/h3&gt;\n		&lt;h4 class=&quot;wojo info header&quot;&gt;Web Design &lt;span class=&quot;push-right&quot;&gt;92%&lt;/span&gt;&lt;/h4&gt;\n		&lt;div data-percent=&quot;92&quot; class=&quot;wojo thin striped info progress&quot;&gt;\n			&lt;div class=&quot;bar&quot;&gt;\n			&lt;/div&gt;\n		&lt;/div&gt;\n		&lt;h4 class=&quot;wojo purple header&quot;&gt;jQuery &lt;span class=&quot;push-right&quot;&gt;74%&lt;/span&gt;&lt;/h4&gt;\n		&lt;div data-percent=&quot;74&quot; class=&quot;wojo thin striped purple progress&quot;&gt;\n			&lt;div class=&quot;bar&quot;&gt;\n			&lt;/div&gt;\n		&lt;/div&gt;\n		&lt;h4 class=&quot;wojo warning header&quot;&gt;PHP&lt;span class=&quot;push-right&quot;&gt;85%&lt;/span&gt;&lt;/h4&gt;\n		&lt;div data-percent=&quot;85&quot; class=&quot;wojo thin striped warning progress&quot;&gt;\n			&lt;div class=&quot;bar&quot;&gt;\n			&lt;/div&gt;\n		&lt;/div&gt;\n		&lt;h4 class=&quot;wojo success header&quot;&gt;SEO Optimisation &lt;span class=&quot;push-right&quot;&gt;75%&lt;/span&gt;&lt;/h4&gt;\n		&lt;div data-percent=&quot;75&quot; class=&quot;wojo thin striped success progress&quot;&gt;\n			&lt;div class=&quot;bar&quot;&gt;\n			&lt;/div&gt;\n		&lt;/div&gt;\n		&lt;h4 class=&quot;wojo negative header&quot;&gt;Marketing &amp; PR &lt;span class=&quot;push-right&quot;&gt;63%&lt;/span&gt;&lt;/h4&gt;\n		&lt;div data-percent=&quot;63&quot; class=&quot;wojo thin striped negative progress&quot;&gt;\n			&lt;div class=&quot;bar&quot;&gt;\n			&lt;/div&gt;\n		&lt;/div&gt;\n	&lt;/div&gt;\n&lt;/div&gt;\n&lt;hr&gt;\n&lt;h3&gt;More then 10 years of experience&lt;/h3&gt;\n&lt;p&gt;\n	 Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sed cumque distinctio labore minima ipsum fugit aspernatur dolore deserunt iure sunt officiis iusto.\n\n	&lt;/p&gt;\n  &lt;p&gt;Quisque sagittis consequat elit non scelerisque. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Sed sed consectetur leo, et fringilla risus. Mauris tincidunt cumque sed leo distinctio labore minima ipsum interdum adipiscing.\n&lt;/p&gt;\n\n&lt;div class=&quot;wojo-carousel&quot; data-pagination=&quot;false&quot; data-navigation=&quot;true&quot; data-items=&quot;4&quot; data-auto-play=&quot;false&quot;&gt;\n  &lt;div class=&quot;item&quot;&gt; &lt;img src=&quot;uploads/images/pages/clients/logo-charity_html.png&quot; alt=&quot;&quot;&gt; &lt;/div&gt;\n  &lt;div class=&quot;item&quot;&gt; &lt;img src=&quot;uploads/images/pages/clients/logo-cleanspace.png&quot; alt=&quot;&quot;&gt; &lt;/div&gt;\n  &lt;div class=&quot;item&quot;&gt; &lt;img src=&quot;uploads/images/pages/clients/logo-dixit.png&quot; alt=&quot;&quot;&gt; &lt;/div&gt;\n  &lt;div class=&quot;item&quot;&gt; &lt;img src=&quot;uploads/images/pages/clients/logo-galaxy.png&quot; alt=&quot;&quot;&gt; &lt;/div&gt;\n  &lt;div class=&quot;item&quot;&gt; &lt;img src=&quot;uploads/images/pages/clients/logo-gt3.png&quot; alt=&quot;&quot;&gt; &lt;/div&gt;\n  &lt;div class=&quot;item&quot;&gt; &lt;img src=&quot;uploads/images/pages/clients/logo-hq.png&quot; alt=&quot;&quot;&gt; &lt;/div&gt;\n  &lt;div class=&quot;item&quot;&gt; &lt;img src=&quot;uploads/images/pages/clients/logo-incipiens.png&quot; alt=&quot;&quot;&gt; &lt;/div&gt;\n  &lt;div class=&quot;item&quot;&gt; &lt;img src=&quot;uploads/images/pages/clients/logo-johndoe.png&quot; alt=&quot;&quot;&gt; &lt;/div&gt;\n  &lt;div class=&quot;item&quot;&gt; &lt;img src=&quot;uploads/images/pages/clients/logo-output.png&quot; alt=&quot;&quot;&gt; &lt;/div&gt;\n  &lt;div class=&quot;item&quot;&gt; &lt;img src=&quot;uploads/images/pages/clients/logo-point.png&quot; alt=&quot;&quot;&gt; &lt;/div&gt;\n  &lt;div class=&quot;item&quot;&gt; &lt;img src=&quot;uploads/images/pages/clients/logo-showroom.png&quot; alt=&quot;&quot;&gt; &lt;/div&gt;\n  &lt;div class=&quot;item&quot;&gt; &lt;img src=&quot;uploads/images/pages/clients/logo-yellow.png&quot; alt=&quot;&quot;&gt; &lt;/div&gt;\n&lt;/div&gt;\n', '&lt;p&gt;&lt;/p&gt;', NULL, 'CMS Pro!, Content Management System, Lightweight CMS', 'pos, sale', 'CMS Pro! is a php based database dependent CMS which require one database and obviously php language support on your web hosting server.', '', '2014-01-27 22:11:36', 1),
(3, 'Our Contact Info', 'İLETİŞİM', 'iletisim', 'Contact Us', '', 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, '0', 0, '0', NULL, 'posmaks/iletisim.jpg', NULL, 'Public', '&lt;div class=&quot;row&quot;&gt;\n	&lt;div class=&quot;col-md-12 col-sm-12&quot;&gt;\n		&lt;h2 class=&quot;alt-font font-weight-600 title-thick-underline border-color-fast-red display-inline-block letter-spacing-2 margin-six no-margin-lr no-margin-top&quot;&gt;BİZE ULAŞIN&lt;/h2&gt;\n	&lt;/div&gt;\n&lt;/div&gt;', '&lt;p&gt;&lt;/p&gt;', NULL, 'lacus,turpis,nisi,mauris,feugiat,amet,tortor,nulla,risus,commodo,odio,auctor,quis,vitae,viverra,libero', '', 'Where to Find UsEtiam non lacus ac velit lobortis rutrum sed id turpis. Ut dictum, eros eu blandit pellentesque,â€¦', 'BİZE ULAŞIN', '2010-07-22 20:11:55', 1),
(5, 'Demo Gallery Page', 'ÜRÜNLER', 'urunler', 'Responsive fluid gallery...', '', 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, '0', 0, '0', NULL, 'posmaks/posmaks-urunler-header.jpg', NULL, 'Public', '&lt;p&gt;Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed ut  tempor eros. Proin bibendum, lacus vitae venenatis convallis, libero  ipsum imperdiet sem, ac consequat massa risus vel sem. Nunc nec ante non  arcu mattis viverra. Morbi accumsan, augue ac dignissim tempus, lacus  libero molestie est, in eleifend lorem purus eu mauris. Nulla at metus a  enim faucibus placerat vitae a justo. Maecenas rhoncus ante libero.&lt;/p&gt;', '&lt;p&gt;&lt;/p&gt;', NULL, 'lorem,ipsum,lacus,vitae,libero,ante', 'posmaks, ürünler', '&lt;p&gt;Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed ut  tempor eros. Proin bibendum, lacus vitae venenatis convallis, libero  ipsum imperdiet sem, ac consequat massa risus vel sem. Nunc nec ante non  arcu mattis viverra. Morbi accumsan, augue ac dignissim tempus, lacus  libero molestie est, in eleifend lorem purus eu mauris. Nulla at metus a  enim faucibus placerat vitae a justo. Maecenas rhoncus ante libero.&lt;/p&gt;', '', '2010-07-22 20:11:55', 1),
(6, 'Visual Forms', 'Visual Forms', 'visual-forms', 'Responsive visual forms', '', 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, '0', 8, '2', 'forms', NULL, NULL, 'Public', '&lt;p&gt;&lt;/p&gt;', '&lt;p&gt;&lt;/p&gt;', NULL, '', '', '', '', '2010-07-22 20:26:17', 1),
(7, 'Three Columns', 'Three Columns', 'three-columns', 'Featuring three columns layout', '', 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, '0', 0, '0', NULL, NULL, NULL, 'Public', '&lt;p&gt;\nAliquam vitae metus non elit laoreet varius. Pellentesque et enim lorem. Suspendisse potenti. Nam ut iaculis lectus. Ut et leo odio. In euismod lobortis nisi, eu placerat nisi laoreet a.\n&lt;/p&gt;\n&lt;p&gt;\n	Cras lobortis lobortis elit, at pellentesque erat vulputate ac. Phasellus in sapien non elit semper pellentesque ut a turpis. Quisque mollis auctor feugiat. Fusce a nisi diam, eu dapibus nibh.Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Etiam a justo libero, aliquam auctor felis. Nulla a odio ut magna ultrices vestibulum.\n&lt;/p&gt;\n&lt;p&gt;\n	Integer urna magna, euismod sed pharetra eget, ornare in dolor. Etiam bibendum mi ut nisi facilisis lobortis. Phasellus turpis orci, interdum adipiscing aliquam ut, convallis volutpat tellus. Nunc massa nunc, dapibus eget scelerisque ac, eleifend eget ligula. Maecenas accumsan tortor in quam adipiscing hendrerit. Donec ac risus nec est molestie malesuada ac id risus. In hac habitasse platea dictumst. In quam dui, blandit id interdum id, facilisis a leo.\n&lt;/p&gt;\n&lt;p&gt;\n	Nullam fringilla quam pharetra enim interdum accumsan. Phasellus nec euismod quam. Donec tempor accumsan posuere. Phasellus ac metus orci, ac venenatis magna. Suspendisse sit amet odio at enim ultricies pellentesque eget ac risus. Vestibulum eleifend odio ut tellus faucibus malesuada feugiat nisi rhoncus. Proin nec sem ut augue placerat blandit ut ut orci. Cras aliquet venenatis enim, quis rutrum urna sollicitudin vel.\n&lt;/p&gt;', '&lt;p&gt;\nAliquam vitae metus non elit laoreet varius. Pellentesque et enim lorem. Suspendisse potenti. Nam ut iaculis lectus. Ut et leo odio. In euismod lobortis nisi, eu placerat nisi laoreet a.\n&lt;/p&gt;\n&lt;p&gt;\n	Cras lobortis lobortis elit, at pellentesque erat vulputate ac. Phasellus in sapien non elit semper pellentesque ut a turpis. Quisque mollis auctor feugiat. Fusce a nisi diam, eu dapibus nibh.Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Etiam a justo libero, aliquam auctor felis. Nulla a odio ut magna ultrices vestibulum.\n&lt;/p&gt;\n&lt;p&gt;\n	Integer urna magna, euismod sed pharetra eget, ornare in dolor. Etiam bibendum mi ut nisi facilisis lobortis. Phasellus turpis orci, interdum adipiscing aliquam ut, convallis volutpat tellus. Nunc massa nunc, dapibus eget scelerisque ac, eleifend eget ligula. Maecenas accumsan tortor in quam adipiscing hendrerit. Donec ac risus nec est molestie malesuada ac id risus. In hac habitasse platea dictumst. In quam dui, blandit id interdum id, facilisis a leo.\n&lt;/p&gt;\n&lt;p&gt;\n	Nullam fringilla quam pharetra enim interdum accumsan. Phasellus nec euismod quam. Donec tempor accumsan posuere. Phasellus ac metus orci, ac venenatis magna. Suspendisse sit amet odio at enim ultricies pellentesque eget ac risus. Vestibulum eleifend odio ut tellus faucibus malesuada feugiat nisi rhoncus. Proin nec sem ut augue placerat blandit ut ut orci. Cras aliquet venenatis enim, quis rutrum urna sollicitudin vel.\n&lt;/p&gt;', NULL, 'quot,thumb,tahjpg,aliquam,metus,elit,laoreet,pellentesque,enim,suspendisse,odio,euismod,lobortis,nisi,placerat,cras,phasellus,turpis,auctor,feugiat,dapibus,faucibus,orci,ultrices,posuere,etiam,magna,vestibulum,urna,pharetra,eget,facilisis,interdum,adipiscing,tellus,nunc,eleifend,accumsan,quam,donec,risus,malesuada,blandit,venenatis', 'quot,thumb,tahjpg,aliquam,metus,elit,laoreet,pellentesque,enim,suspendisse,odio,euismod,lobortis,nisi,placerat,cras,phasellus,turpis,auctor,feugiat,dapibus,faucibus,orci,ultrices,posuere,etiam,magna,vestibulum,urna,pharetra,eget,facilisis,interdum,adipiscing,tellus,nunc,eleifend,accumsan,quam,donec,risus,malesuada,blandit,venenatis', 'Aliquam vitae metus non elit laoreet varius. Pellentesque et enim lorem. Suspendisse potenti. Nam utâ€¦', 'Aliquam vitae metus non elit laoreet varius. Pellentesque et enim lorem. Suspendisse potenti. Nam utâ€¦', '2010-07-22 20:40:19', 1),
(8, 'Image SLider', 'Image SLider', 'image-slider', 'Responsive image slider demo...', '', 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, '0', 2, '1', 'gallery', NULL, NULL, 'Public', '&lt;p&gt;\n	Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Etiam a justo libero, aliquam auctor felis. Nulla a odio ut magna ultrices vestibulum. Integer urna magna, euismod sed pharetra eget, ornare in dolor. Etiam bibendum mi ut nisi facilisis lobortis. Phasellus turpis orci, interdum adipiscing aliquam ut, convallis volutpat tellus.\n&lt;/p&gt;\n&lt;hr&gt;\n&lt;p&gt;\n	Pellentesque et enim lorem. Suspendisse potenti. Nam ut iaculis lectus. Ut et leo odio. In euismod lobortis nisi, eu placerat nisi laoreet a. Cras lobortis lobortis elit, at pellentesque erat vulputate ac. Phasellus in sapien non elit semper pellentesque ut a turpis. Quisque mollis auctor feugiat. Fusce a nisi diam, eu dapibus nibh.\n&lt;/p&gt;\n&lt;div class=&quot;responsive-video&quot;&gt;\n	&lt;iframe src=&quot;//www.youtube.com/embed/BYm_Mn7Hxag&quot; allowfullscreen=&quot;&quot; frameborder=&quot;0&quot;&gt;\n	&lt;/iframe&gt;\n&lt;/div&gt;\n&lt;hr&gt;\n&lt;div class=&quot;two columns gutters&quot;&gt;\n  &lt;div class=&quot;row&quot;&gt;\n    &lt;h3&gt;What do we do?&lt;/h3&gt;\n    &lt;div class=&quot;accordion&quot;&gt;\n      &lt;div class=&quot;header&quot;&gt; &lt;i class=&quot;icon lab&quot;&gt;&lt;/i&gt; Webdesign &lt;/div&gt;\n      &lt;div class=&quot;content&quot;&gt;\n        &lt;p&gt; Quisque sagittis consequat elit non scelerisque. Cum sociis natoque et magnis dis montes, nascetur ridiculus mus. Sed sed leo, et risus consequat. &lt;/p&gt;\n      &lt;/div&gt;\n      &lt;div class=&quot;header&quot;&gt; &lt;i class=&quot;icon setting&quot;&gt;&lt;/i&gt; Web Dewelopment &lt;/div&gt;\n      &lt;div class=&quot;content&quot;&gt;\n        &lt;p&gt; Quisque sagittis consequat elit non scelerisque. Cum sociis natoque et magnis dis montes, nascetur ridiculus mus. Sed sed leo, et risus. &lt;/p&gt;\n      &lt;/div&gt;\n      &lt;div class=&quot;header&quot;&gt; &lt;i class=&quot;icon umbrella&quot;&gt;&lt;/i&gt; Responsivness &lt;/div&gt;\n      &lt;div class=&quot;content&quot;&gt;\n        &lt;p&gt; Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Sed sed leo, et risus. Mauris tincidunt interdum adipiscing. Cum sociis natoque et magnis dis montes, nascetur ridiculus mus.. &lt;/p&gt;\n        &lt;p&gt; Phasellus dolor enim, faucibus egestas scelerisque. Cum sociis natoque et magnis dis montes nascetur. &lt;/p&gt;\n      &lt;/div&gt;\n      &lt;div class=&quot;header&quot;&gt; &lt;i class=&quot;icon star&quot;&gt;&lt;/i&gt; Internet Marketing &lt;/div&gt;\n      &lt;div class=&quot;content&quot;&gt;\n        &lt;p&gt; Quisque sagittis consequat elit non scelerisque. Cum sociis natoque et magnis dis montes, nascetur ridiculus mus. Sed sed leo, et risus. Sed sed leo risus mauris. &lt;/p&gt;\n      &lt;/div&gt;\n    &lt;/div&gt;\n  &lt;/div&gt;\n  &lt;div class=&quot;row&quot;&gt;\n    &lt;h3&gt;Skills and expertise&lt;/h3&gt;\n    &lt;h4 class=&quot;wojo info header&quot;&gt;Web Design &lt;span class=&quot;push-right&quot;&gt;92%&lt;/span&gt;&lt;/h4&gt;\n    &lt;div data-percent=&quot;92&quot; class=&quot;wojo thin striped info progress&quot;&gt;\n      &lt;div class=&quot;bar&quot;&gt; &lt;/div&gt;\n    &lt;/div&gt;\n    &lt;h4 class=&quot;wojo purple header&quot;&gt;jQuery &lt;span class=&quot;push-right&quot;&gt;74%&lt;/span&gt;&lt;/h4&gt;\n    &lt;div data-percent=&quot;74&quot; class=&quot;wojo thin striped purple progress&quot;&gt;\n      &lt;div class=&quot;bar&quot;&gt; &lt;/div&gt;\n    &lt;/div&gt;\n    &lt;h4 class=&quot;wojo warning header&quot;&gt;PHP&lt;span class=&quot;push-right&quot;&gt;85%&lt;/span&gt;&lt;/h4&gt;\n    &lt;div data-percent=&quot;85&quot; class=&quot;wojo thin striped warning progress&quot;&gt;\n      &lt;div class=&quot;bar&quot;&gt; &lt;/div&gt;\n    &lt;/div&gt;\n    &lt;h4 class=&quot;wojo success header&quot;&gt;SEO Optimisation &lt;span class=&quot;push-right&quot;&gt;75%&lt;/span&gt;&lt;/h4&gt;\n    &lt;div data-percent=&quot;75&quot; class=&quot;wojo thin striped success progress&quot;&gt;\n      &lt;div class=&quot;bar&quot;&gt; &lt;/div&gt;\n    &lt;/div&gt;\n    &lt;h4 class=&quot;wojo negative header&quot;&gt;Marketing &amp; PR &lt;span class=&quot;push-right&quot;&gt;63%&lt;/span&gt;&lt;/h4&gt;\n    &lt;div data-percent=&quot;63&quot; class=&quot;wojo thin striped negative progress&quot;&gt;\n      &lt;div class=&quot;bar&quot;&gt; &lt;/div&gt;\n    &lt;/div&gt;\n  &lt;/div&gt;\n&lt;/div&gt;', '', NULL, 'vestibulum,ante,ipsum,orci,luctus,ultrices,etiam,aliquam,aucto', 'vestibulum,ante,ipsum,orci,luctus,ultrices,etiam,aliquam,aucto', 'Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Etiam a justoâ€¦', 'Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Etiam a justoâ€¦', '2010-08-09 22:06:58', 1),
(9, 'Members Only', 'Members Only', 'members-only', NULL, '', 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, '0', 0, '0', NULL, NULL, NULL, 'Registered', '&lt;p&gt;&lt;span style=&quot;font-weight: bold; font-style: italic;&quot;&gt;This page is for Registered users only&lt;/span&gt;&lt;/p&gt;', '&lt;p&gt;&lt;span style=&quot;font-weight: bold; font-style: italic;&quot;&gt;This page is for Registered users only&lt;/span&gt;&lt;/p&gt;', NULL, '', '', 'This page is for Registered users only', 'This page is for Registered users only', '2011-05-19 15:28:29', 1),
(10, 'Membership Only', 'Membership Only', 'membership-only', NULL, '', 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, '2,4,5', 0, '0', NULL, NULL, NULL, 'Membership', '&lt;p&gt;&lt;span style=&quot;font-weight: bold; font-style: italic;&quot;&gt;This page can be accessed with valid membership only!&lt;/span&gt;&lt;/p&gt;', '&lt;p&gt;&lt;span style=&quot;font-weight: bold; font-style: italic;&quot;&gt;This page can be accessed with valid membership only!&lt;/span&gt;&lt;/p&gt;', NULL, '', '', 'This page can be accessed with valid membership only!', 'This page can be accessed with valid membership only!', '2011-05-19 15:28:48', 1),
(14, 'Video Slider', 'Video Slider', 'video-slider', '', '', 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, '0', 0, '0', NULL, NULL, NULL, 'Public', '&lt;div class=&quot;wojo basic segment&quot;&gt;\n	%%videoslider%%\n&lt;/div&gt;\n&lt;p&gt;\n	Itaque earum rerum hic tenetur a sapiente delectus, ut aut reiciendis voluptatibus maiores alias consequatur \n	&lt;span class=&quot;wojo info label&quot;&gt;This plugin is included in CMS pro v4.0&lt;/span&gt; aut perferendis doloribus asperiores repellat accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque.\n&lt;/p&gt;\n&lt;p&gt;\nIn erat. Pellentesque erat. Mauris vehicula vestibulum justo. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nulla pulvinar est. Integer urna. Pellentesque pulvinar dui a magna. Nulla facilisi. Proin imperdiet. Aliquam ornare, metus vitae gravida dignissim, nisi nisl ultricies felis, ac tristique enim pede eget elit. Integer non erat nec turpis sollicitudin malesuada. Vestibulum dapibus. Nulla facilisi. Nulla iaculis, leo sit amet mollis luctus, sapien eros consectetur dolor, eu faucibus elit nibh eu nibh. Maecenas lacus pede, lobortis non, rhoncus id, tristique a, mi. Cras auctor libero vitae sem vestibulum euismod. Nunc fermentum.\n&lt;/p&gt;\n&lt;p&gt;\n Aliquam ornare, metus vitae gravida dignissim, nisi nisl ultricies felis, ac tristique enim pede eget elit. Integer non erat nec turpis sollicitudin malesuada. Vestibulum dapibus\n&lt;/p&gt;\n&lt;blockquote class=&quot;pullquote alignright&quot;&gt;\n&lt;p&gt;Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.&lt;/p&gt;\n&lt;/blockquote&gt;\n&lt;p&gt;\n	 Integer fermentum elit in tellus. Integer ligula ipsum, gravida aliquet, fringilla non, interdum eget, ipsum. Praesent id dolor non erat viverra volutpat. Fusce tellus libero, luctus adipiscing, tincidunt vel, egestas vitae, eros. Vestibulum mollis, est id rhoncus volutpat, dolor velit tincidunt neque, vitae pellentesque ante sem eu nisl. eget convallis mauris ante quis magna. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Aenean et libero. Nam aliquam. Quisque vitae tortor id neque dignissim laoreet. &lt;/p&gt;&lt;p&gt;Duis eu ante. Integer at sapien. Praesent sed nisl tempor est pulvinar tristique. Maecenas non lorem quis mi laoreet adipiscing. Sed ac arcu. Sed tincidunt libero eu dolor. Cras pharetra posuere eros. Donec ac eros id diam tempor faucibus. Fusce feugiat consequat nulla. Vestibulum tincidunt vulputate ipsum.\n&lt;/p&gt;\n&lt;h2&gt;Praesent metus velit, imperdiet a aliquam et, imperdiet ac dolor.&lt;/h2&gt;\n\n&lt;p&gt;Vivamus commodo turpis vitae ligula luctus malesuada. Quisque non turpis ac felis molestie bibendum nec eget sem. Mauris feugiat pretium est, at iaculis est. Integer nec eros velit. Aenean rutrum, sapien non consectetur gravida, lectus velit tristique ligula, vel sagittis est nulla et velit. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Sed porttitor tincidunt urna, vel placerat dui commodo a. Integer placerat arcu et neque sollicitudin vestibulum. Etiam ullamcorper sodales lectus, non condimentum nisi vehicula ut. Fusce pretium nisi purus, vitae blandit velit accumsan non. In ultricies rhoncus nunc, et lobortis erat dapibus in. Ut nec diam non nulla bibendum ornare. Maecenas porta pharetra consequat. Proin pulvinar viverra dictum.&lt;/p&gt;\n\n&lt;ul class=&quot;wojo list&quot;&gt;\n&lt;li&gt;Lorem ipsum dolor sit amet, consectetur adipiscing elit.&lt;/li&gt;\n&lt;li&gt;Donec scelerisque ante tempus nisi congue porttitor.&lt;/li&gt;\n&lt;li&gt;Morbi eu diam semper, elementum turpis eu, egestas magna.&lt;/li&gt;\n&lt;li&gt;Nulla eget est ut risus molestie varius.&lt;/li&gt;\n&lt;/ul&gt;', '&lt;div class=&quot;wojo basic segment&quot;&gt;\n	%%videoslider%%\n&lt;/div&gt;\n&lt;p&gt;\n	Itaque earum rerum hic tenetur a sapiente delectus, ut aut reiciendis voluptatibus maiores alias consequatur \n	&lt;span class=&quot;wojo info label&quot;&gt;This plugin is included in CMS pro v4.0&lt;/span&gt; aut perferendis doloribus asperiores repellat accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque.\n&lt;/p&gt;\n&lt;p&gt;\nIn erat. Pellentesque erat. Mauris vehicula vestibulum justo. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nulla pulvinar est. Integer urna. Pellentesque pulvinar dui a magna. Nulla facilisi. Proin imperdiet. Aliquam ornare, metus vitae gravida dignissim, nisi nisl ultricies felis, ac tristique enim pede eget elit. Integer non erat nec turpis sollicitudin malesuada. Vestibulum dapibus. Nulla facilisi. Nulla iaculis, leo sit amet mollis luctus, sapien eros consectetur dolor, eu faucibus elit nibh eu nibh. Maecenas lacus pede, lobortis non, rhoncus id, tristique a, mi. Cras auctor libero vitae sem vestibulum euismod. Nunc fermentum.\n&lt;/p&gt;\n&lt;p&gt;\n Aliquam ornare, metus vitae gravida dignissim, nisi nisl ultricies felis, ac tristique enim pede eget elit. Integer non erat nec turpis sollicitudin malesuada. Vestibulum dapibus\n&lt;/p&gt;\n&lt;blockquote class=&quot;pullquote alignright&quot;&gt;\n&lt;p&gt;Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.&lt;/p&gt;\n&lt;/blockquote&gt;\n&lt;p&gt;\n	 Integer fermentum elit in tellus. Integer ligula ipsum, gravida aliquet, fringilla non, interdum eget, ipsum. Praesent id dolor non erat viverra volutpat. Fusce tellus libero, luctus adipiscing, tincidunt vel, egestas vitae, eros. Vestibulum mollis, est id rhoncus volutpat, dolor velit tincidunt neque, vitae pellentesque ante sem eu nisl. eget convallis mauris ante quis magna. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Aenean et libero. Nam aliquam. Quisque vitae tortor id neque dignissim laoreet. &lt;/p&gt;&lt;p&gt;Duis eu ante. Integer at sapien. Praesent sed nisl tempor est pulvinar tristique. Maecenas non lorem quis mi laoreet adipiscing. Sed ac arcu. Sed tincidunt libero eu dolor. Cras pharetra posuere eros. Donec ac eros id diam tempor faucibus. Fusce feugiat consequat nulla. Vestibulum tincidunt vulputate ipsum.\n&lt;/p&gt;\n&lt;h2&gt;Praesent metus velit, imperdiet a aliquam et, imperdiet ac dolor.&lt;/h2&gt;\n\n&lt;p&gt;Vivamus commodo turpis vitae ligula luctus malesuada. Quisque non turpis ac felis molestie bibendum nec eget sem. Mauris feugiat pretium est, at iaculis est. Integer nec eros velit. Aenean rutrum, sapien non consectetur gravida, lectus velit tristique ligula, vel sagittis est nulla et velit. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Sed porttitor tincidunt urna, vel placerat dui commodo a. Integer placerat arcu et neque sollicitudin vestibulum. Etiam ullamcorper sodales lectus, non condimentum nisi vehicula ut. Fusce pretium nisi purus, vitae blandit velit accumsan non. In ultricies rhoncus nunc, et lobortis erat dapibus in. Ut nec diam non nulla bibendum ornare. Maecenas porta pharetra consequat. Proin pulvinar viverra dictum.&lt;/p&gt;\n\n&lt;ul class=&quot;wojo list&quot;&gt;\n&lt;li&gt;Lorem ipsum dolor sit amet, consectetur adipiscing elit.&lt;/li&gt;\n&lt;li&gt;Donec scelerisque ante tempus nisi congue porttitor.&lt;/li&gt;\n&lt;li&gt;Morbi eu diam semper, elementum turpis eu, egestas magna.&lt;/li&gt;\n&lt;li&gt;Nulla eget est ut risus molestie varius.&lt;/li&gt;\n&lt;/ul&gt;', NULL, 'erat,pellentesque,mauris,vehicula,vestibulum,sociis,natoque,penatibus,magnis,parturient,montes,nascetur,ridiculus,nulla,pulvinar,integer,urna,magna,facilisi,proin,imperdiet,aliquam,ornare,metus,vitae,gravida,dignissim,nisi,nisl,ultricies,felis,tristique,enim,pede,eget,elit,turpis,sollicitudin,malesuada,dapibus,iaculis,amet,mollis,luctus,sapien,eros,consectetur,dolor,faucibus,nibh,maecenas,lobortis,rhoncus,cras,libero,nunc,fermentum,lorem,ipsum,adipiscing,aenean,commodo,ligula,tellus,praesent,viverra,volutpat,fusce,tincidunt,egestas,velit,neque,ante,quis,habitant,morbi,senectus,netus,fames,quisque,laoreet,tempor,arcu,pharetra,donec,diam,feugiat,consequat,molestie,bibendum,pretium,lectus,porttitor,placerat', 'erat,pellentesque,mauris,vehicula,vestibulum,sociis,natoque,penatibus,magnis,parturient,montes,nascetur,ridiculus,nulla,pulvinar,integer,urna,magna,facilisi,proin,imperdiet,aliquam,ornare,metus,vitae,gravida,dignissim,nisi,nisl,ultricies,felis,tristique,enim,pede,eget,elit,turpis,sollicitudin,malesuada,dapibus,iaculis,amet,mollis,luctus,sapien,eros,consectetur,dolor,faucibus,nibh,maecenas,lobortis,rhoncus,cras,libero,nunc,fermentum,lorem,ipsum,adipiscing,aenean,commodo,ligula,tellus,praesent,viverra,volutpat,fusce,tincidunt,egestas,velit,neque,ante,quis,habitant,morbi,senectus,netus,fames,quisque,laoreet,tempor,arcu,pharetra,donec,diam,feugiat,consequat,molestie,bibendum,pretium,lectus,porttitor,placerat', '%%videoslider%% Itaque earum rerum hic tenetur a sapiente delectus, ut aut reiciendis voluptatibus maiores&#8230;', '%%videoslider%% Itaque earum rerum hic tenetur a sapiente delectus, ut aut reiciendis voluptatibus maiores&#8230;', '2012-01-01 22:09:08', 1),
(21, 'Profile', 'Profile', 'profile', 'Public User Profile', '', 1, 0, 0, 0, 0, 0, 0, 0, 0, 1, '0', 0, '0', NULL, NULL, NULL, 'Public', NULL, '', NULL, '', '', '', '', '2014-11-13 17:27:25', 1),
(23, 'Corporate', 'HAKKIMIZDA', 'hakkimizda', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '0', 0, '0', NULL, 'posmaks/posmaks-hakkinda.jpg', NULL, 'Public', '', '&lt;p&gt;&lt;/p&gt;', NULL, 'kodar,olarak,veren,yazılım,hizmet,alanında,uzman,müşterilerimizin,ortak,yenilikçi,ürünleri,teknoloji,firmamız,kuruluşumuzda,belirlediğimiz,rotamızdan,sapmadan,türkiye,komşu,ülkelerde,marka,değerimizi,daha,tanınır,hale,getirmek,insanlara,yüksek,kullanıcı,deneyimi,yaşatıp,mutlu,olmalarını,sağlamaktır,kalite,entegre,şirket,sürekli,ekonomik,avantajlarımızı,paylaşıyoruz,branding,lorem,ipsum,simply,dummy,text,printing,typesetting,industry,standard,photography,industry lorem,typesetting industry lorem,industry lorem ipsum', 'kodar, ar-ge, ibeacon, blego, teknoloji, yazılım, mobil uygulama, kodar bilişim', 'Hakkımızda KODAR olarak Ar-Ge çalışmalarına büyük önem veren, Elektronik ve Mobil teknolojileri…', 'KODAR olarak Ar-Ge çalışmalarına büyük önem veren, Elektronik ve Mobil teknolojileri birleştirmeye yönelik geliştirdiğimiz donanım ve yazılım çözümleri ile tanınmaktayız. Hedefimiz ülkemizi ve sektörümüzü en üst seviyede temsil etmek ve hizmet vermektir.', '2016-02-05 09:54:47', 1),
(24, '', 'Kariyer', 'career', NULL, '', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '0', 13, '0', 'career', 'images/pages/banner-3.jpg', NULL, 'Public', NULL, '&lt;p&gt;İş Başvurusu / Açık Pozisyonlar&lt;/p&gt;', NULL, '', '', '', 'İş Başvurusu Açık Pozisyonlar', '2016-02-05 17:34:56', 1),
(27, '', 'POSMAKS BULUT', 'posmaksbulut', NULL, 'POSMAKS BULUT', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '0', 0, '0', NULL, NULL, NULL, 'Public', NULL, 'erterterte\nertert\ne\nrt\nert\ne\nrt\ne\nrt\nert\nert\ne\nrt\ner\nt\ne\nrt\netr', NULL, '', '', '', 'denemememememem', '2017-04-26 21:20:05', 1),
(29, '', 'POSMAKS YEREL', 'posmaksyerel', NULL, 'POSMAKS YEREL', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '0', 0, '0', NULL, NULL, NULL, 'Public', NULL, '&lt;p&gt;&lt;/p&gt;', NULL, '', 'navbea,kapalı,müşteri,yönlendirme,sadakat,arasında,sağlar,gitmek,nokta,gibi', '', 'Navbea, büyük ve kapalı yapılarda müşteri deneyimleri izleme, yönlendirme ve kişiye özel fırsatlar…', '2017-04-27 12:32:31', 1);

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `payments`
--

CREATE TABLE `payments` (
  `id` int(11) NOT NULL,
  `txn_id` varchar(100) DEFAULT NULL,
  `membership_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `rate_amount` varchar(255) NOT NULL,
  `currency` varchar(4) DEFAULT NULL,
  `created` datetime NOT NULL,
  `pp` enum('PayPal','MoneyBookers','Offline','Stripe','FastPay','IyziPay') DEFAULT NULL,
  `ip` varchar(20) DEFAULT NULL,
  `status` int(1) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Tablo döküm verisi `payments`
--

INSERT INTO `payments` (`id`, `txn_id`, `membership_id`, `user_id`, `rate_amount`, `currency`, `created`, `pp`, `ip`, `status`) VALUES
(1, '123456', 2, 1, '5.00', NULL, '2014-01-09 14:12:32', 'PayPal', NULL, 1),
(2, NULL, 2, 2, '5.00', NULL, '2014-01-03 14:12:32', 'PayPal', NULL, 1),
(3, NULL, 3, 2, '10.00', NULL, '2014-01-04 16:47:36', 'MoneyBookers', NULL, 1),
(4, '316307', 5, 2, '2', 'TRY', '2017-06-02 15:39:07', '', '::1', 1),
(5, '316307', 5, 2, '2', 'TRY', '2017-06-02 15:39:26', '', '::1', 1),
(6, '316307', 5, 2, '2', 'TRY', '2017-06-02 15:40:43', '', '::1', 1),
(7, '317079', 2, 2, '2.99', 'TRY', '2017-06-02 16:04:38', '', '::1', 1),
(8, '317082', 5, 2, '2', 'TRY', '2017-06-02 16:07:27', '', '::1', 1),
(9, '317084', 5, 2, '2', 'TRY', '2017-06-02 16:09:28', '', '::1', 1),
(10, '317130', 5, 2, '2', 'TRY', '2017-06-02 16:17:43', '', '::1', 1),
(11, '317133', 5, 2, '2', 'TRY', '2017-06-02 16:19:47', '', '::1', 1),
(12, '317135', 5, 2, '2.19', 'TRY', '2017-06-02 16:22:07', '', '::1', 1),
(13, '317141', 5, 2, '2', 'TRY', '2017-06-02 16:26:47', '', '::1', 1),
(14, '317142', 5, 2, '2', 'TRY', '2017-06-02 16:28:21', '', '::1', 1),
(15, '317150', 5, 2, '2', 'TRY', '2017-06-02 16:42:40', 'IyziPay', '::1', 1),
(16, '317167', 5, 2, '2', 'TRY', '2017-06-02 16:57:34', 'IyziPay', '::1', 1),
(17, '317168', 5, 2, '2', 'TRY', '2017-06-02 16:58:30', 'IyziPay', '::1', 1),
(18, '317173', 5, 2, '2', 'TRY', '2017-06-02 16:59:19', 'IyziPay', '::1', 1),
(19, '317173', 5, 2, '2', 'TRY', '2017-06-02 17:08:16', 'IyziPay', '::1', 1),
(20, '317173', 5, 2, '2', 'TRY', '2017-06-02 17:08:55', 'IyziPay', '::1', 1),
(21, '317173', 5, 2, '2', 'TRY', '2017-06-02 17:10:05', 'IyziPay', '::1', 1),
(22, '317214', 2, 2, '2.99', 'TRY', '2017-06-02 17:18:09', 'IyziPay', '::1', 1);

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `plugins`
--

CREATE TABLE `plugins` (
  `id` int(11) NOT NULL,
  `title_en` varchar(120) NOT NULL,
  `title_tr` varchar(120) NOT NULL,
  `body_en` text,
  `body_tr` text,
  `jscode` text,
  `show_title` tinyint(1) NOT NULL DEFAULT '0',
  `alt_class` varchar(100) NOT NULL DEFAULT '',
  `system` tinyint(1) NOT NULL DEFAULT '0',
  `cplugin` tinyint(1) NOT NULL DEFAULT '0',
  `info_en` text,
  `info_tr` text,
  `plugalias` varchar(50) NOT NULL,
  `hasconfig` tinyint(1) NOT NULL DEFAULT '0',
  `created` datetime NOT NULL,
  `ver` varchar(4) NOT NULL DEFAULT '1.00',
  `active` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Tablo döküm verisi `plugins`
--

INSERT INTO `plugins` (`id`, `title_en`, `title_tr`, `body_en`, `body_tr`, `jscode`, `show_title`, `alt_class`, `system`, `cplugin`, `info_en`, `info_tr`, `plugalias`, `hasconfig`, `created`, `ver`, `active`) VALUES
(2, 'News Slider', 'News Slider', NULL, '', NULL, 1, '', 1, 1, 'Displays latest news items', 'Displays latest news items', 'newsslider', 1, '2014-03-20 14:10:15', '4.0', 1),
(6, 'Image Slider', 'Image Slider', NULL, '&lt;p&gt;&lt;/p&gt;', NULL, 0, '', 1, 1, 'jQuery Slider is one great way to display portfolio pieces, eCommerce product images, or even as an image gallery.', 'jQuery Slider is one great way to display portfolio pieces, eCommerce product images, or even as an image gallery.', 'slider', 1, '2014-03-20 14:10:15', '4.0', 1),
(10, 'Latest Twitts', 'Latest Twitts', NULL, '', NULL, 1, '', 1, 1, 'Shows your latest twitts', 'Shows your latest twitts', 'twitts', 1, '2014-02-11 11:42:08', '4.0', 1),
(13, 'Ajax Poll', 'Ajax Poll', NULL, '', NULL, 1, '', 1, 1, 'jQuery Ajax poll module.', 'jQuery Ajax poll module.', 'poll', 1, '2014-02-21 11:42:08', '4.0', 1),
(7, 'jQuery Tabs', 'jQuery Tabs', NULL, '', NULL, 0, '', 1, 1, 'jQuery Dynamic Tabs', 'jQuery Dynamic Tabs', 'jtabs', 1, '2014-03-25 13:12:20', '4.0', 1),
(12, 'Event Manager', 'Event Manager', NULL, '', NULL, 1, '', 1, 1, 'Easily publish and manage your company events.', 'Easily publish and manage your company events.', 'events', 0, '2014-04-10 10:12:14', '4.0', 1),
(14, 'Vertical Navigation', 'Vertical Navigation', NULL, '', NULL, 1, '', 1, 0, 'Vertical flyout menu module', 'Vertical flyout menu module', 'vmenu', 0, '2014-04-15 11:12:14', '4.0', 1),
(16, 'Rss Parser', 'SEKTOREL COZUMLER', NULL, '&lt;div class=&quot;container&quot;&gt;\n	&lt;div class=&quot;row&quot;&gt;\n&lt;div class=&quot;row margin-fifteen no-margin-lr no-margin-bottom blog-post-style2&quot;&gt;\n	&lt;!-- special offer item --&gt;\n	&lt;article class=&quot;col-md-6 col-sm-6 col-xs-12 margin-four no-margin-lr no-margin-top&quot;&gt;\n	&lt;div class=&quot;col-md-12 col-sm-12 col-xs-12 no-padding bg-white&quot;&gt;\n		&lt;div class=&quot;col-md-6 col-sm-6 col-xs-6 no-padding post-thumbnail overflow-hidden&quot;&gt;\n			&lt;a href=&quot;#&quot;&gt;&lt;img src=&quot;uploads/images/fastfood.jpg&quot; alt=&quot;&quot;&gt;&lt;/a&gt;\n		&lt;/div&gt;\n		&lt;div class=&quot;col-md-6 col-sm-6 col-xs-6 no-padding post-details-arrow&quot;&gt;\n			&lt;div class=&quot;post-details&quot;&gt;\n				&lt;span class=&quot;text-extra-large text-uppercase display-block alt-font margin-ten no-margin-lr sm-text-small xs-text-small&quot;&gt;&lt;a href=&quot;../posmaks/sektor1&quot;&gt;FASTFOOD&lt;/a&gt;&lt;/span&gt;\n				&lt;span class=&quot;text-medium text-uppercase letter-spacing-2 alt-font light-gray-text sm-text-extra-small&quot;&gt;8 Days / $350&lt;/span&gt;\n				&lt;div class=&quot;separator-line-thick bg-dark-blue margin-fifteen no-margin-bottom margin-lr-auto&quot;&gt;\n				\n			&lt;/div&gt;\n		&lt;/div&gt;\n	&lt;/div&gt;\n	&lt;/article&gt;\n	&lt;!-- end special offer item --&gt;\n	&lt;!-- special offer item --&gt;\n	&lt;article class=&quot;col-md-6 col-sm-6 col-xs-12 margin-four no-margin-lr no-margin-top&quot;&gt;\n	&lt;div class=&quot;col-md-12 col-sm-12 col-xs-12 no-padding bg-white&quot;&gt;\n		&lt;div class=&quot;col-md-6 col-sm-6 col-xs-6 no-padding post-thumbnail overflow-hidden&quot;&gt;\n			&lt;a href=&quot;#&quot;&gt;&lt;img src=&quot;images/travel-img26.jpg&quot; alt=&quot;&quot;&gt;&lt;/a&gt;\n		&lt;/div&gt;\n		&lt;div class=&quot;col-md-6 col-sm-6 col-xs-6 no-padding post-details-arrow&quot;&gt;\n			&lt;div class=&quot;post-details&quot;&gt;\n				&lt;span class=&quot;text-extra-large text-uppercase display-block alt-font margin-ten no-margin-lr sm-text-small xs-text-small&quot;&gt;&lt;a href=&quot;#&quot;&gt;Weekdays in Thailand Beauty&lt;/a&gt;&lt;/span&gt;\n				&lt;span class=&quot;text-medium text-uppercase letter-spacing-2 alt-font light-gray-text sm-text-extra-small&quot;&gt;8 Days / $350&lt;/span&gt;\n				&lt;div class=&quot;separator-line-thick bg-dark-blue margin-fifteen no-margin-bottom margin-lr-auto&quot;&gt;\n				&lt;/div&gt;\n				&lt;div class=&quot;travel-special-off bg-deep-orange position-absolute alt-font white-text sm-text-extra-small&quot;&gt;\n					30% OFF\n				&lt;/div&gt;\n			&lt;/div&gt;\n		&lt;/div&gt;\n	&lt;/div&gt;\n	&lt;/article&gt;\n	&lt;!-- end special offer item --&gt;\n	&lt;!-- special offer item --&gt;\n	&lt;article class=&quot;col-md-6 col-sm-6 col-xs-12 xs-margin-four xs-no-margin-lr xs-no-margin-top&quot;&gt;\n	&lt;div class=&quot;col-md-12 col-sm-12 col-xs-12 no-padding bg-white&quot;&gt;\n		&lt;div class=&quot;col-md-6 col-sm-6 col-xs-6 no-padding post-thumbnail overflow-hidden&quot;&gt;\n			&lt;a href=&quot;#&quot;&gt;&lt;img src=&quot;images/travel-img27.jpg&quot; alt=&quot;&quot;&gt;&lt;/a&gt;\n		&lt;/div&gt;\n		&lt;div class=&quot;col-md-6 col-sm-6 col-xs-6 no-padding post-details-arrow&quot;&gt;\n			&lt;div class=&quot;post-details&quot;&gt;\n				&lt;span class=&quot;text-extra-large text-uppercase display-block alt-font margin-ten no-margin-lr sm-text-small xs-text-small&quot;&gt;&lt;a href=&quot;#&quot;&gt;South Africa Adventures&lt;/a&gt;&lt;/span&gt;\n				&lt;span class=&quot;text-medium text-uppercase letter-spacing-2 alt-font light-gray-text sm-text-extra-small&quot;&gt;8 Days / $350&lt;/span&gt;\n				&lt;div class=&quot;separator-line-thick bg-dark-blue margin-fifteen no-margin-bottom margin-lr-auto&quot;&gt;\n				&lt;/div&gt;\n				&lt;div class=&quot;travel-special-off bg-deep-orange position-absolute alt-font white-text sm-text-extra-small&quot;&gt;\n					30% OFF\n				&lt;/div&gt;\n			&lt;/div&gt;\n		&lt;/div&gt;\n	&lt;/div&gt;\n	&lt;/article&gt;\n	&lt;!-- end special offer item --&gt;\n	&lt;!-- special offer item --&gt;\n	&lt;article class=&quot;col-md-6 col-sm-6 col-xs-12&quot;&gt;\n	&lt;div class=&quot;col-md-12 col-sm-12 col-xs-12 no-padding bg-white&quot;&gt;\n		&lt;div class=&quot;col-md-6 col-sm-6 col-xs-6 no-padding post-thumbnail overflow-hidden&quot;&gt;\n			&lt;a href=&quot;#&quot;&gt;&lt;img src=&quot;images/travel-img28.jpg&quot; alt=&quot;&quot;&gt;&lt;/a&gt;\n		&lt;/div&gt;\n		&lt;div class=&quot;col-md-6 col-sm-6 col-xs-6 no-padding post-details-arrow&quot;&gt;\n			&lt;div class=&quot;post-details&quot;&gt;\n				&lt;span class=&quot;text-extra-large text-uppercase display-block alt-font margin-ten no-margin-lr sm-text-small xs-text-small&quot;&gt;&lt;a href=&quot;#&quot;&gt;Hot Baloon Special Journey&lt;/a&gt;&lt;/span&gt;\n				&lt;span class=&quot;text-medium text-uppercase letter-spacing-2 alt-font light-gray-text sm-text-extra-small&quot;&gt;8 Days / $350&lt;/span&gt;\n				&lt;div class=&quot;separator-line-thick bg-dark-blue margin-fifteen no-margin-bottom margin-lr-auto&quot;&gt;\n				&lt;/div&gt;\n				&lt;div class=&quot;travel-special-off bg-deep-orange position-absolute alt-font white-text sm-text-extra-small&quot;&gt;\n					25% OFF\n				&lt;/div&gt;\n			&lt;/div&gt;\n		&lt;/div&gt;\n	&lt;/div&gt;\n	&lt;/article&gt;\n	&lt;!-- end special offer item --&gt;\n&lt;/div&gt;\n&lt;/div&gt;', NULL, 0, '', 1, 0, 'Show rss feeds (RSS 0.9 / RSS 1.0). Also RSS 2.0, and Atom a with few exceptions.', '', 'rss', 1, '2014-04-07 08:11:55', '4.0', 1),
(18, 'User Login', 'User Login', NULL, '', NULL, 1, '', 1, 0, 'Shows login form.', 'Shows login form.', 'login', 0, '2014-02-11 11:12:15', '4.0', 1),
(20, 'Smooth Content Slider', 'Smooth Content Slider', NULL, '&lt;p&gt;&lt;/p&gt;', NULL, 0, '', 1, 1, 'Any type of content slider', '', 'contentslider', 1, '2014-01-24 14:18:58', '4.0', 1),
(21, 'Video Slider', 'Video Slider', NULL, '', NULL, 0, '', 1, 1, NULL, '', 'videoslider', 1, '2014-04-11 18:14:51', '4.0', 1),
(22, 'Upcoming Event', 'Upcoming Event', NULL, '', NULL, 1, '', 1, 1, NULL, '', 'upevent', 1, '2014-02-20 16:47:10', '4.0', 1),
(23, 'You&#039;ve Helped Raise…', 'You&#039;ve Helped Raise…', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit Nam pulvinar. ', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit Nam pulvinar. ', NULL, 1, '', 1, 1, '', '', 'donate', 1, '2014-03-30 00:21:32', '4.0', 1),
(24, 'Elastic Slider', 'Elastic Slider', NULL, '', NULL, 0, '', 1, 1, 'Elastic image slideshow', 'Elastic image slideshow', 'elastic', 1, '2014-03-12 00:21:32', '4.0', 1),
(1, 'Testimonials', 'Testimonials', '&lt;p class=&quot;testimonial&quot;&gt;&lt;em&gt;Etiam non lacus ac velit lobortis rutrum sed id turpis. Ut dictum, eros eu blandit pellentesque, nisi nisl dapibus mauris, sed feugiat enim urna sit amet nibh. Suspendisse sed tortor nisi.&lt;/em&gt;&lt;/p&gt;\r\n&lt;em&gt;John Smith&lt;/em&gt;, &lt;strong&gt;www.somesite.com&lt;/strong&gt;', '&lt;p class=&quot;testimonial&quot;&gt;&lt;em&gt;Etiam non lacus ac velit lobortis rutrum sed id turpis. Ut dictum, eros eu blandit pellentesque, nisi nisl dapibus mauris, sed feugiat enim urna sit amet nibh. Suspendisse sed tortor nisi.&lt;/em&gt;&lt;/p&gt;\r\n&lt;em&gt;John Smith&lt;/em&gt;, &lt;strong&gt;www.somesite.com&lt;/strong&gt;', NULL, 1, '', 0, 0, '', '', '', 0, '2014-04-16 10:12:14', '1.00', 1),
(8, 'Just an Image', 'Just an Image', '&lt;div class=&quot;wojo fitted positive segment&quot;&gt;\n	&lt;div class=&quot;header&quot;&gt;\n		&lt;h4&gt;Rick DeMarco&lt;/h4&gt;\n	&lt;/div&gt;\n	&lt;div class=&quot;image-overlay&quot;&gt;\n		&lt;img src=&quot;uploads/images/pages/team/team-member5-big.jpg&quot; alt=&quot;Staff Member&quot;&gt;\n		&lt;div class=&quot;overlay-fade&quot;&gt;\n			&lt;a title=&quot;Rick DeMarco&quot; class=&quot;lightbox&quot; href=&quot;uploads/images/pages/team/team-member5-big.jpg&quot;&gt;&lt;i class=&quot;icon-overlay icon information&quot;&gt;&lt;/i&gt;&lt;/a&gt;\n		&lt;/div&gt;\n	&lt;/div&gt;\n	&lt;div class=&quot;footer&quot;&gt;\n        Web Designer\n	&lt;/div&gt;\n&lt;/div&gt;', '&lt;div class=&quot;wojo fitted positive segment&quot;&gt;\n	&lt;div class=&quot;header&quot;&gt;\n		&lt;h4&gt;Rick DeMarco&lt;/h4&gt;\n	&lt;/div&gt;\n	&lt;div class=&quot;image-overlay&quot;&gt;\n		&lt;img src=&quot;uploads/images/pages/team/team-member5-big.jpg&quot; alt=&quot;Staff Member&quot;&gt;\n		&lt;div class=&quot;overlay-fade&quot;&gt;\n			&lt;a title=&quot;Rick DeMarco&quot; class=&quot;lightbox&quot; href=&quot;uploads/images/pages/team/team-member5-big.jpg&quot;&gt;&lt;i class=&quot;icon-overlay icon information&quot;&gt;&lt;/i&gt;&lt;/a&gt;\n		&lt;/div&gt;\n	&lt;/div&gt;\n	&lt;div class=&quot;footer&quot;&gt;\n        Web Designer\n	&lt;/div&gt;\n&lt;/div&gt;', NULL, 0, '', 0, 0, '', '', '', 0, '2014-04-10 10:12:14', '1.00', 1),
(3, 'An unordered list', 'An unordered list', 'This plugin contains a dummy list of items\n&lt;ul&gt;    \n	&lt;li&gt;List item 1&lt;/li&gt;    \n	&lt;li&gt;List item 2&lt;/li&gt;    \n	&lt;li&gt;List item 3&lt;/li&gt;    \n	&lt;li&gt;List item 4&lt;/li&gt;\n&lt;/ul&gt;', 'This plugin contains a dummy list of items\n&lt;ul&gt;    \n	&lt;li&gt;List item 1&lt;/li&gt;    \n	&lt;li&gt;List item 2&lt;/li&gt;    \n	&lt;li&gt;List item 3&lt;/li&gt;    \n	&lt;li&gt;List item 4&lt;/li&gt;\n&lt;/ul&gt;', NULL, 1, '', 0, 0, '', '', '', 0, '2014-04-14 10:12:14', '1.00', 1),
(5, 'Contact Info', 'Contact Info', '&lt;div class=&quot;wojo divided list&quot;&gt;\n	&lt;div class=&quot;item&quot;&gt;\n		&lt;i class=&quot;home icon&quot;&gt;&lt;/i&gt;\n		3254 Main Street Canada\n	&lt;/div&gt;\n	&lt;div class=&quot;item&quot;&gt;\n		&lt;i class=&quot;phone icon&quot;&gt;&lt;/i&gt;\n		+1 123 546 7890\n	&lt;/div&gt;\n	&lt;div class=&quot;item&quot;&gt;\n		&lt;i class=&quot;print icon&quot;&gt;&lt;/i&gt;\n		+1 123 546 7890\n	&lt;/div&gt;\n	&lt;div class=&quot;item&quot;&gt;\n		&lt;i class=&quot;mail icon&quot;&gt;&lt;/i&gt;\n		support@yourcompany.com\n	&lt;/div&gt;\n&lt;/div&gt;', '&lt;div class=&quot;wojo divided list&quot;&gt;\n	&lt;div class=&quot;item&quot;&gt;\n		&lt;i class=&quot;home icon&quot;&gt;&lt;/i&gt;\n		3254 Main Street Canada\n	&lt;/div&gt;\n	&lt;div class=&quot;item&quot;&gt;\n		&lt;i class=&quot;phone icon&quot;&gt;&lt;/i&gt;\n		+1 123 546 7890\n	&lt;/div&gt;\n	&lt;div class=&quot;item&quot;&gt;\n		&lt;i class=&quot;print icon&quot;&gt;&lt;/i&gt;\n		+1 123 546 7890\n	&lt;/div&gt;\n	&lt;div class=&quot;item&quot;&gt;\n		&lt;i class=&quot;mail icon&quot;&gt;&lt;/i&gt;\n		support@yourcompany.com\n	&lt;/div&gt;\n&lt;/div&gt;', NULL, 1, '', 0, 0, '', '', '', 0, '2014-04-10 10:12:14', '1.00', 1),
(11, 'Our Clients', 'Our Clients', '&lt;div class=&quot;wojo basic secondary segment&quot;&gt;\n&lt;div class=&quot;wojo-carousel&quot; data-auto-play=&quot;true&quot; data-pagination=&quot;true&quot; data-navigation=&quot;false&quot; data-slide-speed=&quot;200&quot; data-rewind-speed=&quot;1000&quot; data-transition-style=&quot;fade&quot; data-single-item=&quot;true&quot;&gt;\n	&lt;img src=&quot;uploads/images/pages/client1.png&quot; alt=&quot;client1&quot;&gt;\n	&lt;img src=&quot;uploads/images/pages/client2.png&quot; alt=&quot;client2&quot;&gt;\n	&lt;img src=&quot;uploads/images/pages/client3.png&quot; alt=&quot;client3&quot;&gt;\n	&lt;img src=&quot;uploads/images/pages/client4.png&quot; alt=&quot;client4&quot;&gt;\n	&lt;img src=&quot;uploads/images/pages/client5.png&quot; alt=&quot;client5&quot;&gt;\n	&lt;img src=&quot;uploads/images/pages/client6.png&quot; alt=&quot;client6&quot;&gt;\n&lt;/div&gt;\n&lt;/div&gt;', '&lt;div class=&quot;wojo basic secondary segment&quot;&gt;\n&lt;div class=&quot;wojo-carousel&quot; data-auto-play=&quot;true&quot; data-pagination=&quot;true&quot; data-navigation=&quot;false&quot; data-slide-speed=&quot;200&quot; data-rewind-speed=&quot;1000&quot; data-transition-style=&quot;fade&quot; data-single-item=&quot;true&quot;&gt;\n	&lt;img src=&quot;uploads/images/pages/client1.png&quot; alt=&quot;client1&quot;&gt;\n	&lt;img src=&quot;uploads/images/pages/client2.png&quot; alt=&quot;client2&quot;&gt;\n	&lt;img src=&quot;uploads/images/pages/client3.png&quot; alt=&quot;client3&quot;&gt;\n	&lt;img src=&quot;uploads/images/pages/client4.png&quot; alt=&quot;client4&quot;&gt;\n	&lt;img src=&quot;uploads/images/pages/client5.png&quot; alt=&quot;client5&quot;&gt;\n	&lt;img src=&quot;uploads/images/pages/client6.png&quot; alt=&quot;client6&quot;&gt;\n&lt;/div&gt;\n&lt;/div&gt;', NULL, 1, '', 0, 0, '', '', '', 0, '2014-04-23 10:12:14', '1.00', 1),
(9, 'Even More Pages', 'Even More Pages', '&lt;ul class=&quot;lists&quot;&gt;\r\n    &lt;li&gt;&lt;a href=&quot;#&quot;&gt;Updates&lt;/a&gt;&lt;/li&gt;\r\n    &lt;li&gt;&lt;a href=&quot;#&quot;&gt;News&lt;/a&gt;&lt;/li&gt;\r\n    &lt;li&gt;&lt;a href=&quot;#&quot;&gt;Press Releases&lt;/a&gt;&lt;/li&gt;\r\n    &lt;li&gt;&lt;a href=&quot;#&quot;&gt;New Offers&lt;/a&gt;&lt;/li&gt;\r\n    &lt;li&gt;&lt;a href=&quot;#&quot;&gt;Our Staff &lt;/a&gt;&lt;/li&gt;\r\n    &lt;li&gt;&lt;a href=&quot;#&quot;&gt;Policy&lt;/a&gt;&lt;/li&gt;\r\n    &lt;li&gt;&lt;a href=&quot;#&quot;&gt;Events&lt;/a&gt;&lt;/li&gt;\r\n&lt;/ul&gt;', '&lt;ul class=&quot;lists&quot;&gt;\r\n    &lt;li&gt;&lt;a href=&quot;#&quot;&gt;Updates&lt;/a&gt;&lt;/li&gt;\r\n    &lt;li&gt;&lt;a href=&quot;#&quot;&gt;News&lt;/a&gt;&lt;/li&gt;\r\n    &lt;li&gt;&lt;a href=&quot;#&quot;&gt;Press Releases&lt;/a&gt;&lt;/li&gt;\r\n    &lt;li&gt;&lt;a href=&quot;#&quot;&gt;New Offers&lt;/a&gt;&lt;/li&gt;\r\n    &lt;li&gt;&lt;a href=&quot;#&quot;&gt;Our Staff &lt;/a&gt;&lt;/li&gt;\r\n    &lt;li&gt;&lt;a href=&quot;#&quot;&gt;Policy&lt;/a&gt;&lt;/li&gt;\r\n    &lt;li&gt;&lt;a href=&quot;#&quot;&gt;Events&lt;/a&gt;&lt;/li&gt;\r\n&lt;/ul&gt;', NULL, 1, '', 0, 0, '', '', '', 0, '2014-04-10 10:12:14', '1.00', 1),
(34, 'Static BG Image', 'Static BG Image', '&lt;div class=&quot;static-image&quot;&gt;\n	&lt;h1&gt;A lot of new features&lt;/h1&gt;\n	&lt;p&gt;\n		In facilisis eget nisi nec consectetur. Maecenas laoreet tellus varius, aliquet justo non, interdum metus.\n	&lt;/p&gt;\n	&lt;p&gt;\n		Lorem ipsum dolor sit amet, consectetur adipiscing elit. \n		&lt;a&gt;Morbi sodales &lt;/a&gt;vehicula odio quis lobortis. Aenean lacinia pulvinar hendrerit.\n	&lt;/p&gt;\n	&lt;p&gt;\n		Donec id elit non mi porta gravida at eget\n	&lt;/p&gt;\n	&lt;div class=&quot;wojo basic inverted button&quot;&gt;\n		Discover CMS pro\n	&lt;/div&gt;\n&lt;/div&gt;', '&lt;div class=&quot;static-image&quot;&gt;\n	&lt;h1&gt;A lot of new features&lt;/h1&gt;\n	&lt;p&gt;\n		In facilisis eget nisi nec consectetur. Maecenas laoreet tellus varius, aliquet justo non, interdum metus.\n	&lt;/p&gt;\n	&lt;p&gt;\n		Lorem ipsum dolor sit amet, consectetur adipiscing elit. \n		&lt;a&gt;Morbi sodales &lt;/a&gt;vehicula odio quis lobortis. Aenean lacinia pulvinar hendrerit.\n	&lt;/p&gt;\n	&lt;p&gt;\n		Donec id elit non mi porta gravida at eget\n	&lt;/p&gt;\n	&lt;div class=&quot;wojo basic inverted button&quot;&gt;\n		Discover CMS pro\n	&lt;/div&gt;\n&lt;/div&gt;', NULL, 0, '', 0, 0, '', '', '', 0, '2014-04-22 23:01:31', '1.00', 1),
(36, 'wojo-advert', 'wojo-advert', NULL, '', NULL, 0, '', 1, 0, NULL, '', 'adblock/wojo-advert', 0, '2014-04-24 22:27:23', '1.00', 1),
(37, 'DigiShop Categories', 'DigiShop Categories', NULL, '', NULL, 0, '', 1, 0, NULL, '', 'digicats', 0, '2014-04-30 13:48:13', '4.0', 1),
(38, 'DigiShop Cart', 'DigiShop Cart', NULL, '', NULL, 0, '', 1, 0, NULL, '', 'digicart', 0, '2014-04-29 16:18:43', '4.0', 1),
(39, 'Blog Categories', 'Blog Categories', NULL, '', NULL, 0, '', 1, 0, 'Displays blog categories', 'Displays blog categories', 'blogcats', 0, '2014-03-13 21:08:18', '4.0', 1),
(40, 'Blog Combo Box', 'Blog Combo Box', NULL, '', NULL, 0, '', 1, 0, 'Displays tabbed content of Blog Latest Comments, Blog Most Popular and Blog Archieve', 'Displays tabbed content of Blog Latest Comments, Blog Most Popular and Blog Archieve', 'blogcombo', 0, '2014-03-20 21:08:18', '4.0', 1),
(41, 'Blog Search', 'Blog Search', NULL, '', NULL, 0, '', 1, 0, 'Full article search', 'Full article search', 'blogsearch', 0, '2014-03-26 14:10:17', '4.0', 1),
(42, 'Latest Articles', 'Latest Articles', NULL, '', NULL, 1, '', 1, 1, 'Displays latest articles in carousel view', 'Displays latest articles in carousel view', 'bloglatest', 0, '2014-05-04 16:14:08', '4.0', 1),
(43, 'Who is Online', 'Who is Online', NULL, '', NULL, 1, '', 1, 0, NULL, '', 'online', 0, '2014-06-08 00:13:27', '4.00', 1),
(45, 'Image Slider Brando', 'SLIDER', NULL, '&lt;p&gt;&lt;/p&gt;', NULL, 0, '', 1, 1, 'jQuery Slider is one great way to display portfolio pieces, eCommerce product images, or even as an image gallery.', '', 'sliderbrando', 1, '2014-03-20 14:10:15', '4.0', 1),
(50, '', 'Google Maps - Kodar', NULL, NULL, NULL, 0, '', 1, 0, NULL, NULL, 'gmaps/Kodar', 0, '2016-02-03 13:20:38', '1.00', 1),
(51, 'Kurumsal', 'HAKKIMIZDA', '&lt;section class=&quot;wow border-bottom bg-white no-padding-top&quot;&gt;\n&lt;div class=&quot;container&quot;&gt;\n	&lt;div class=&quot;row&quot;&gt;\n		&lt;!-- end side bar --&gt;\n		&lt;div class=&quot;col-md-12 col-sm-12 &quot;&gt;\n			&lt;!-- tab --&gt;\n			&lt;div class=&quot;col-md-12 col-sm-12 text-center tab-style9&quot; id=&quot;animated-tab&quot;&gt;\n				&lt;!-- tab navigation --&gt;\n				&lt;ul class=&quot;nav nav-tabs margin-six no-margin-lr no-margin-top xs-margin-ten xs-no-margin-lr xs-no-margin-top&quot;&gt;\n					&lt;li class=&quot;nav active&quot;&gt;&lt;a href=&quot;#tab6_sec1&quot; data-toggle=&quot;tab&quot; aria-expanded=&quot;true&quot;&gt;&lt;i class=&quot;icon-bizi-taniyin&quot;&gt;&lt;/i&gt;&lt;/a&gt;&lt;/li&gt;\n					&lt;li class=&quot;nav&quot;&gt;&lt;a href=&quot;#tab6_sec2&quot; data-toggle=&quot;tab&quot; aria-expanded=&quot;false&quot;&gt;&lt;i class=&quot;icon-neden-kodar&quot;&gt;&lt;/i&gt;&lt;/a&gt;&lt;/li&gt;\n					&lt;li class=&quot;nav&quot;&gt;&lt;a href=&quot;#tab6_sec5&quot; data-toggle=&quot;tab&quot; aria-expanded=&quot;false&quot;&gt;&lt;i class=&quot;icon-basin-odasi&quot;&gt;&lt;/i&gt;&lt;/a&gt;&lt;/li&gt;\n				&lt;/ul&gt;\n				&lt;!-- end tab navigation --&gt;\n				&lt;!-- tab content section --&gt;\n				&lt;div class=&quot;tab-content&quot;&gt;\n					&lt;!-- tab content --&gt;\n					&lt;div id=&quot;tab6_sec1&quot; class=&quot;col-md-12 col-sm-12 text-center center-col tab-pane fade active in&quot;&gt;\n						&lt;div class=&quot;tab-pane fade in&quot;&gt;\n							&lt;div class=&quot;row&quot;&gt;\n								&lt;div class=&quot;col-md-3 col-sm-12 text-left gray-text&quot;&gt;\n									&lt;span class=&quot;alt-font text-uppercase title-medium black-text element-title&quot;&gt;Hakkımızda&lt;/span&gt;\n									&lt;div class=&quot;separator-line bg-yellow no-margin-lr sm-margin-five sm-no-margin-lr xs-no-margin-lr&quot;&gt;\n									&lt;/div&gt;\n									&lt;p class=&quot;text-large&quot;&gt;\n									&lt;/p&gt;\n								&lt;/div&gt;\n								&lt;div class=&quot;col-md-9 col-sm-12 text-left gray-text&quot;&gt;\n									&lt;p class=&quot;title-kodar no-margin-bottom&quot;&gt;\n                                            KODAR olarak Ar-Ge çalışmalarına büyük önem veren, Elektronik ve Mobil teknolojileri birleştirmeye yönelik geliştirdiğimiz donanım ve yazılım çözümleri ile tanınmaktayız. Hedefimiz ülkemizi ve sektörümüzü en üst seviyede temsil etmek ve hizmet vermektir.\n										&lt;br&gt;\n										&lt;br&gt;\n                                            Deneyimli ve alanında uzman kişilerden oluşan KODAR ekibi olarak, müşterilerimizin ve iş ortaklarımızın ihtiyaçları doğrultusunda anahtar teslim projelerden, ortak geliştirmeye, sistem entegrasyonundan, proje danışmanlığına kadar gereken hizmetleri üstlenmekteyiz. Amacımız en yenilikçi ürünleri, en iyi teknoloji işbirliğiyle, en iyi hizmet ve desteği sağlayarak sunmaktır. \n										&lt;br&gt;\n										&lt;br&gt;\n                                            2013 yılında kurulan firmamız, İstanbul’daki merkezinde 10+ uzman çalışanı ile hizmet vermektedir. Yazılım geliştirme, teknik ve idari destek alanında genç ve dinamik bir kadroya yer veren firmamız internet of things çözümlerinde ilklere imza atmayı hedeflemektedir. Bu bağlamda üniversiteler ve kurumlarla ortak yürüttüğümüz projelerde, akademisyen ve uzmanların danışmanlığı ile firmamızın bilgi birikimi ve tecrübesini birleştirerek başarılı çalışmalar gerçekleştirmekteyiz.\n										&lt;br&gt;\n										&lt;br&gt;\n									&lt;/p&gt;\n									&lt;div class=&quot;text-center&quot;&gt;\n										&lt;img src=&quot;../../uploads/vizyon-misyon.png&quot; alt=&quot;kodar vizyonu&quot;&gt;\n									&lt;/div&gt;\n									&lt;br&gt;\n									&lt;br&gt;\n									&lt;span class=&quot;alt-font text-uppercase title-medium black-text element-title&quot;&gt;VİZYONUMUZ&lt;/span&gt;\n									&lt;p class=&quot;title-kodar no-margin-bottom&quot;&gt;\n                                                Kuruluşumuzda belirlediğimiz rotamızdan sapmadan Türkiye ve komşu ülkelerde marka değerimizi daha tanınır hale getirmek, insanlara daha yüksek bir kullanıcı deneyimi yaşatıp, mutlu olmalarını sağlamaktır.\n									&lt;/p&gt;\n									&lt;br&gt;\n									&lt;br&gt;\n									&lt;span class=&quot;alt-font text-uppercase title-medium black-text element-title&quot;&gt;MİSYONUMUZ&lt;/span&gt;\n									&lt;p class=&quot;title-kodar no-margin-bottom&quot;&gt;\n                                                Kuruluşumuzda belirlediğimiz rotamızdan sapmadan Türkiye ve komşu ülkelerde marka değerimizi daha tanınır hale getirmek, insanlara, daha yüksek bir kullanıcı deneyimi yaşatıp; mutlu olmalarını sağlamaktır.\n									&lt;/p&gt;\n									&lt;br&gt;\n									&lt;br&gt;\n									&lt;span class=&quot;alt-font text-uppercase title-medium black-text element-title&quot;&gt;KALİTE POLİTİKAMIZ&lt;/span&gt;\n									&lt;p class=&quot;title-medium no-margin-bottom&quot;&gt;\n                                                Ürettiğimiz yüksek teknoloji ürünleri, işlevsel ve kaliteli yazılım çözümlerimizle müşterilerimizin ihtiyaç ve beklentilerini karşılarken;\n									&lt;/p&gt;\n									&lt;br&gt;\n									&lt;ol class=&quot;title-kodar no-margin-bottom&quot;&gt;\n										&lt;li&gt;Nitelikli ve entegre çalışabilen ekip yapısına sahip olmak,&lt;/li&gt;\n										&lt;li&gt;Verimliliğimizi ve rekabet gücümüzü arttırmak,&lt;/li&gt;\n										&lt;li&gt;Çevreye ve yasalara duyarlı bir şirket olarak topluma ve ülkeye katkıda bulunmak,&lt;/li&gt;\n										&lt;li&gt;Çalışma ortamı ve koşulları ile ürün ve süreçlerimizi sürekli iyileştirmek,&lt;/li&gt;\n										&lt;li&gt;Kalite yönetim sistemi standardının gereklerini karşılamak ve etkinliğini sürekli geliştirmek için gerekli kaynakları temin etmektir.&lt;/li&gt;\n									&lt;/ol&gt;\n									&lt;p&gt;\n									&lt;/p&gt;\n								&lt;/div&gt;\n							&lt;/div&gt;\n							&lt;div class=&quot;row&quot;&gt;\n								&lt;div class=&quot;wide-separator-line&quot;&gt;\n								&lt;/div&gt;\n							&lt;/div&gt;\n						&lt;/div&gt;\n					&lt;/div&gt;\n					&lt;!-- end tab content --&gt;\n					&lt;!-- tab content --&gt;\n					&lt;div id=&quot;tab6_sec2&quot; class=&quot;col-md-12 col-sm-12 text-center center-col tab-pane fade&quot;&gt;\n						&lt;div class=&quot;tab-pane fade in&quot;&gt;\n							&lt;span class=&quot;alt-font text-uppercase title-medium black-text element-title&quot;&gt;NEDEN KODAR?&lt;/span&gt;\n							&lt;div class=&quot;row&quot;&gt;\n								&lt;div class=&quot;no-margin-bottom blog-post blog-post-style5 no-margin-lr&quot;&gt;\n									&lt;!-- blog item --&gt;\n									&lt;article&gt;\n									&lt;div class=&quot;col-lg-4 col-md-6 col-sm-6 no-padding bg-fast-yellow new-post overflow-hidden&quot;&gt;\n										&lt;div class=&quot;post-details alt-font&quot;&gt;\n											&lt;span class=&quot;text-extra-large font-weight-600 z-index-1 position-relative text-uppercase post-title display-block&quot;&gt;\n											&lt;p class=&quot;black-text&quot;&gt;\n												Esneklik :\n												&lt;br&gt;\n												 Alışkanlıklarınıza saygı duyuyoruz\n											&lt;/p&gt;\n											&lt;/span&gt;\n											&lt;div class=&quot;img-border-medium border-transperent-white z-index-0&quot;&gt;\n											&lt;/div&gt;\n										&lt;/div&gt;\n									&lt;/div&gt;\n									&lt;/article&gt;\n									&lt;!-- end blog item --&gt;\n									&lt;!-- blog item --&gt;\n									&lt;article&gt;\n									&lt;div class=&quot;col-lg-4 col-md-6 col-sm-6 no-padding overflow-hidden&quot;&gt;\n										&lt;div class=&quot;post-details alt-font&quot;&gt;\n											&lt;span class=&quot;text-extra-large font-weight-600 z-index-1 position-relative text-uppercase post-title display-block&quot;&gt;\n											&lt;p class=&quot;black-text&quot;&gt;\n												Yenilikçi :\n												&lt;br&gt;\n												&#039;Yarının teknolojisini bugün kullanıyoruz&#039;\n											&lt;/p&gt;\n											&lt;/span&gt;\n										&lt;/div&gt;\n									&lt;/div&gt;\n									&lt;/article&gt;\n									&lt;!-- end blog item --&gt;\n									&lt;article&gt;\n									&lt;div class=&quot;col-lg-4 col-md-6 col-sm-6 no-padding overflow-hidden&quot;&gt;\n										&lt;div class=&quot;post-details alt-font&quot;&gt;\n											&lt;span class=&quot;text-extra-large font-weight-600 z-index-1 position-relative text-uppercase post-title display-block&quot;&gt;\n											&lt;p class=&quot;gray-text&quot;&gt;\n												Ekonomik : &#039;Avantajlarımızı paylaşıyoruz&#039;\n											&lt;/p&gt;\n											&lt;/span&gt;\n											&lt;div class=&quot;img-border-medium border-transperent-black-light z-index-0&quot;&gt;\n											&lt;/div&gt;\n										&lt;/div&gt;\n									&lt;/div&gt;\n									&lt;/article&gt;\n									&lt;article&gt;\n									&lt;div class=&quot;col-lg-4 col-md-6 col-sm-6 no-padding overflow-hidden&quot;&gt;\n										&lt;div class=&quot;post-details alt-font&quot;&gt;\n											&lt;span class=&quot;text-extra-large font-weight-600 z-index-1 position-relative text-uppercase post-title display-block&quot;&gt;\n											&lt;p class=&quot;gray-text&quot;&gt;\n												Ekonomik : &#039;Avantajlarımızı paylaşıyoruz&#039;\n											&lt;/p&gt;\n											&lt;/span&gt;\n											&lt;div class=&quot;img-border-medium border-transperent-black-light z-index-0&quot;&gt;\n											&lt;/div&gt;\n										&lt;/div&gt;\n									&lt;/div&gt;\n									&lt;/article&gt;\n									&lt;!-- blog item --&gt;\n									&lt;article&gt;\n									&lt;div class=&quot;col-lg-4 col-md-6 col-sm-6 no-padding bg-fast-pink-dark popular-post overflow-hidden&quot;&gt;\n										&lt;div class=&quot;post-details alt-font&quot;&gt;\n											&lt;span class=&quot;text-extra-large font-weight-600 z-index-1 position-relative text-uppercase post-title display-block&quot;&gt;\n											&lt;p class=&quot;white-text&quot;&gt;\n												Entegre : &#039;Sistemlerinizle uyumluyuz&#039;\n											&lt;/p&gt;\n											&lt;/span&gt;\n											&lt;div class=&quot;img-border-medium border-transperent-white z-index-0&quot;&gt;\n											&lt;/div&gt;\n										&lt;/div&gt;\n									&lt;/div&gt;\n									&lt;/article&gt;\n									&lt;!-- end blog item --&gt;\n									&lt;article&gt;\n									&lt;div class=&quot;col-lg-4 col-md-6 col-sm-6 no-padding overflow-hidden&quot;&gt;\n										&lt;div class=&quot;post-details alt-font&quot;&gt;\n											&lt;span class=&quot;text-extra-large font-weight-600 z-index-1 position-relative text-uppercase post-title display-block&quot;&gt;\n											&lt;p class=&quot;gray-text&quot;&gt;\n												Ekonomik : &#039;Avantajlarımızı paylaşıyoruz&#039;\n											&lt;/p&gt;\n											&lt;/span&gt;\n											&lt;div class=&quot;img-border-medium border-transperent-black-light z-index-0&quot;&gt;\n											&lt;/div&gt;\n										&lt;/div&gt;\n									&lt;/div&gt;\n									&lt;/article&gt;\n								&lt;/div&gt;\n							&lt;/div&gt;\n							&lt;div class=&quot;row&quot;&gt;\n								&lt;div class=&quot;wide-separator-line&quot;&gt;\n								&lt;/div&gt;\n							&lt;/div&gt;\n						&lt;/div&gt;\n					&lt;/div&gt;\n					&lt;!-- end tab content --&gt;\n					&lt;!-- tab content --&gt;\n					&lt;div id=&quot;tab6_sec3&quot; class=&quot;col-md-12 col-sm-12 text-center center-col tab-pane fade&quot;&gt;\n						&lt;div class=&quot;tab-pane fade in&quot;&gt;\n							&lt;div class=&quot;row&quot;&gt;\n								&lt;div class=&quot;col-md-6 col-sm-12 text-left gray-text&quot;&gt;\n									&lt;span class=&quot;text-medium text-uppercase black-text&quot;&gt;Branding&lt;/span&gt;\n									&lt;div class=&quot;separator-line bg-yellow no-margin-lr sm-margin-five sm-no-margin-lr xs-no-margin-lr&quot;&gt;\n									&lt;/div&gt;\n									&lt;p class=&quot;text-medium text-uppercase&quot;&gt;\n										We create meaningful experiences through innovation in storytelling, technology, entertainment and products.\n									&lt;/p&gt;\n								&lt;/div&gt;\n								&lt;div class=&quot;col-md-6 col-sm-12 text-left gray-text&quot;&gt;\n									&lt;p class=&quot;margin-fifteen no-margin-lr no-margin-top text-uppercase text-medium&quot;&gt;\n										Lorem Ipsum is simply dummy text of the printing and typesetting industry.\n									&lt;/p&gt;\n									&lt;p&gt;\n										Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the standard dummy text. Lorem Ipsum is simply dummy text of the printing and typesetting industry.\n									&lt;/p&gt;\n								&lt;/div&gt;\n							&lt;/div&gt;\n							&lt;div class=&quot;row&quot;&gt;\n								&lt;div class=&quot;wide-separator-line&quot;&gt;\n								&lt;/div&gt;\n							&lt;/div&gt;\n							&lt;div class=&quot;row&quot;&gt;\n								&lt;div class=&quot;col-md-12 col-sm-12 text-center alt-font text-uppercase&quot;&gt;\n									&lt;span class=&quot;text-extra-large&quot;&gt;&lt;b&gt;1997 to 2014&lt;/b&gt; - Branding&lt;/span&gt;\n								&lt;/div&gt;\n							&lt;/div&gt;\n						&lt;/div&gt;\n					&lt;/div&gt;\n					&lt;!-- end tab content --&gt;\n					&lt;!-- tab content --&gt;\n					&lt;div id=&quot;tab6_sec4&quot; class=&quot;col-md-12 col-sm-12 text-center center-col tab-pane fade&quot;&gt;\n						&lt;div class=&quot;tab-pane fade in&quot;&gt;\n							&lt;div class=&quot;row&quot;&gt;\n								&lt;div class=&quot;col-md-6 col-sm-12 text-left gray-text&quot;&gt;\n									&lt;span class=&quot;text-medium text-uppercase black-text&quot;&gt;Photography&lt;/span&gt;\n									&lt;div class=&quot;separator-line bg-yellow no-margin-lr sm-margin-five sm-no-margin-lr xs-no-margin-lr&quot;&gt;\n									&lt;/div&gt;\n									&lt;p class=&quot;text-medium text-uppercase&quot;&gt;\n										A first impression can make or break you. we can help you find the precise message to clearly.\n									&lt;/p&gt;\n								&lt;/div&gt;\n								&lt;div class=&quot;col-md-6 col-sm-12 text-left gray-text&quot;&gt;\n									&lt;p class=&quot;margin-fifteen no-margin-lr no-margin-top text-uppercase text-medium&quot;&gt;\n										Lorem Ipsum is simply dummy text of the printing and typesetting industry.\n									&lt;/p&gt;\n									&lt;p&gt;\n										Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the standard dummy text. Lorem Ipsum is simply dummy text of the printing and typesetting industry.\n									&lt;/p&gt;\n								&lt;/div&gt;\n							&lt;/div&gt;\n							&lt;div class=&quot;row&quot;&gt;\n								&lt;div class=&quot;wide-separator-line&quot;&gt;\n								&lt;/div&gt;\n							&lt;/div&gt;\n							&lt;div class=&quot;row&quot;&gt;\n								&lt;div class=&quot;col-md-12 col-sm-12 text-center alt-font text-uppercase&quot;&gt;\n									&lt;span class=&quot;text-extra-large&quot;&gt;&lt;b&gt;1999 to 2014&lt;/b&gt; - Photography&lt;/span&gt;\n								&lt;/div&gt;\n							&lt;/div&gt;\n						&lt;/div&gt;\n					&lt;/div&gt;\n					&lt;!-- end tab content --&gt;\n					&lt;!-- tab content --&gt;\n					&lt;div id=&quot;tab6_sec5&quot; class=&quot;col-md-12 col-sm-12 text-center center-col tab-pane fade&quot;&gt;\n						&lt;span class=&quot;alt-font title-medium black-text&quot;&gt;Şirket ve Tescilli Marka Logolarımız&lt;/span&gt;\n						&lt;div class=&quot;line&quot; style=&quot;margin-bottom:20px;&quot;&gt;\n						&lt;/div&gt;\n						&lt;div class=&quot;tab-pane fade in&quot;&gt;\n							&lt;div class=&quot;row&quot;&gt;\n								&lt;div class=&quot;col-md-6 col-sm-12 text-left gray-text&quot;&gt;\n									&lt;p class=&quot;text-medium text-uppercase&quot;&gt;\n										&lt;img src=&quot;../uploads/kurumsal-logolar/logo-v1.png&quot; alt=&quot;&quot;&gt;\n									&lt;/p&gt;\n									&lt;ul class=&quot;text-center center-col nav nav-tabs margin-six no-margin-lr no-margin-top xs-margin-ten xs-no-margin-lr xs-no-margin-top&quot;&gt;\n										&lt;li class=&quot;nav&quot;&gt;&lt;a href=&quot;&quot;&gt;&lt;i class=&quot;icon-pdf&quot;&gt;&lt;/i&gt;&lt;/a&gt;&lt;/li&gt;\n										&lt;li class=&quot;nav&quot;&gt;&lt;a href=&quot;&quot;&gt;&lt;i class=&quot;icon-ai&quot;&gt;&lt;/i&gt;&lt;/a&gt;&lt;/li&gt;\n										&lt;li class=&quot;nav&quot;&gt;&lt;a href=&quot;&quot;&gt;&lt;i class=&quot;icon-jpg&quot;&gt;&lt;/i&gt;&lt;/a&gt;&lt;/li&gt;\n										&lt;li class=&quot;nav&quot;&gt;&lt;a href=&quot;&quot;&gt;&lt;i class=&quot;icon-png&quot;&gt;&lt;/i&gt;&lt;/a&gt;&lt;/li&gt;\n									&lt;/ul&gt;\n									&lt;p&gt;\n									&lt;/p&gt;\n									&lt;p class=&quot;text-medium text-uppercase&quot;&gt;\n										&lt;img src=&quot;../uploads/kurumsal-logolar/minemesh-logo.png&quot; alt=&quot;&quot;&gt;\n									&lt;/p&gt;\n									&lt;ul class=&quot;text-center center-col nav nav-tabs margin-six no-margin-lr no-margin-top xs-margin-ten xs-no-margin-lr xs-no-margin-top&quot;&gt;\n										&lt;li class=&quot;nav&quot;&gt;&lt;a href=&quot;&quot;&gt;&lt;i class=&quot;icon-pdf&quot;&gt;&lt;/i&gt;&lt;/a&gt;&lt;/li&gt;\n										&lt;li class=&quot;nav&quot;&gt;&lt;a href=&quot;&quot;&gt;&lt;i class=&quot;icon-ai&quot;&gt;&lt;/i&gt;&lt;/a&gt;&lt;/li&gt;\n										&lt;li class=&quot;nav&quot;&gt;&lt;a href=&quot;&quot;&gt;&lt;i class=&quot;icon-jpg&quot;&gt;&lt;/i&gt;&lt;/a&gt;&lt;/li&gt;\n										&lt;li class=&quot;nav&quot;&gt;&lt;a href=&quot;&quot;&gt;&lt;i class=&quot;icon-png&quot;&gt;&lt;/i&gt;&lt;/a&gt;&lt;/li&gt;\n									&lt;/ul&gt;\n									&lt;p&gt;\n									&lt;/p&gt;\n									&lt;p class=&quot;text-medium text-uppercase&quot;&gt;\n										&lt;img src=&quot;../uploads/kurumsal-logolar/omnichannel-logo.png&quot; alt=&quot;&quot;&gt;\n									&lt;/p&gt;\n									&lt;ul class=&quot;text-center center-col nav nav-tabs margin-six no-margin-lr no-margin-top xs-margin-ten xs-no-margin-lr xs-no-margin-top&quot;&gt;\n										&lt;li class=&quot;nav&quot;&gt;&lt;a href=&quot;&quot;&gt;&lt;i class=&quot;icon-pdf&quot;&gt;&lt;/i&gt;&lt;/a&gt;&lt;/li&gt;\n										&lt;li class=&quot;nav&quot;&gt;&lt;a href=&quot;&quot;&gt;&lt;i class=&quot;icon-ai&quot;&gt;&lt;/i&gt;&lt;/a&gt;&lt;/li&gt;\n										&lt;li class=&quot;nav&quot;&gt;&lt;a href=&quot;&quot;&gt;&lt;i class=&quot;icon-jpg&quot;&gt;&lt;/i&gt;&lt;/a&gt;&lt;/li&gt;\n										&lt;li class=&quot;nav&quot;&gt;&lt;a href=&quot;&quot;&gt;&lt;i class=&quot;icon-png&quot;&gt;&lt;/i&gt;&lt;/a&gt;&lt;/li&gt;\n									&lt;/ul&gt;\n									&lt;p&gt;\n									&lt;/p&gt;\n									&lt;p class=&quot;text-medium text-uppercase&quot;&gt;\n										&lt;img src=&quot;../uploads/kurumsal-logolar/veriasist-logo.png&quot; alt=&quot;&quot;&gt;\n									&lt;/p&gt;\n									&lt;ul class=&quot;text-center center-col nav nav-tabs margin-six no-margin-lr no-margin-top xs-margin-ten xs-no-margin-lr xs-no-margin-top&quot;&gt;\n										&lt;li class=&quot;nav&quot;&gt;&lt;a href=&quot;&quot;&gt;&lt;i class=&quot;icon-pdf&quot;&gt;&lt;/i&gt;&lt;/a&gt;&lt;/li&gt;\n										&lt;li class=&quot;nav&quot;&gt;&lt;a href=&quot;&quot;&gt;&lt;i class=&quot;icon-ai&quot;&gt;&lt;/i&gt;&lt;/a&gt;&lt;/li&gt;\n										&lt;li class=&quot;nav&quot;&gt;&lt;a href=&quot;&quot;&gt;&lt;i class=&quot;icon-jpg&quot;&gt;&lt;/i&gt;&lt;/a&gt;&lt;/li&gt;\n										&lt;li class=&quot;nav&quot;&gt;&lt;a href=&quot;&quot;&gt;&lt;i class=&quot;icon-png&quot;&gt;&lt;/i&gt;&lt;/a&gt;&lt;/li&gt;\n									&lt;/ul&gt;\n									&lt;p&gt;\n									&lt;/p&gt;\n								&lt;/div&gt;\n								&lt;div class=&quot;col-md-6 col-sm-12 text-left gray-text&quot;&gt;\n									&lt;p class=&quot;text-medium text-uppercase&quot;&gt;\n										&lt;img src=&quot;../uploads/kurumsal-logolar/blego-logo.png&quot; alt=&quot;&quot;&gt;\n									&lt;/p&gt;\n									&lt;ul class=&quot;text-center center-col nav nav-tabs margin-six no-margin-lr no-margin-top xs-margin-ten xs-no-margin-lr xs-no-margin-top&quot;&gt;\n										&lt;li class=&quot;nav&quot;&gt;&lt;a href=&quot;&quot;&gt;&lt;i class=&quot;icon-pdf&quot;&gt;&lt;/i&gt;&lt;/a&gt;&lt;/li&gt;\n										&lt;li class=&quot;nav&quot;&gt;&lt;a href=&quot;&quot;&gt;&lt;i class=&quot;icon-ai&quot;&gt;&lt;/i&gt;&lt;/a&gt;&lt;/li&gt;\n										&lt;li class=&quot;nav&quot;&gt;&lt;a href=&quot;&quot;&gt;&lt;i class=&quot;icon-jpg&quot;&gt;&lt;/i&gt;&lt;/a&gt;&lt;/li&gt;\n										&lt;li class=&quot;nav&quot;&gt;&lt;a href=&quot;&quot;&gt;&lt;i class=&quot;icon-png&quot;&gt;&lt;/i&gt;&lt;/a&gt;&lt;/li&gt;\n									&lt;/ul&gt;\n									&lt;p&gt;\n									&lt;/p&gt;\n									&lt;p class=&quot;text-medium text-uppercase&quot;&gt;\n										&lt;img src=&quot;../uploads/kurumsal-logolar/muzeasist-logo.png&quot; alt=&quot;&quot;&gt;\n									&lt;/p&gt;\n									&lt;ul class=&quot;text-center center-col nav nav-tabs margin-six no-margin-lr no-margin-top xs-margin-ten xs-no-margin-lr xs-no-margin-top&quot;&gt;\n										&lt;li class=&quot;nav&quot;&gt;&lt;a href=&quot;&quot;&gt;&lt;i class=&quot;icon-pdf&quot;&gt;&lt;/i&gt;&lt;/a&gt;&lt;/li&gt;\n										&lt;li class=&quot;nav&quot;&gt;&lt;a href=&quot;&quot;&gt;&lt;i class=&quot;icon-ai&quot;&gt;&lt;/i&gt;&lt;/a&gt;&lt;/li&gt;\n										&lt;li class=&quot;nav&quot;&gt;&lt;a href=&quot;&quot;&gt;&lt;i class=&quot;icon-jpg&quot;&gt;&lt;/i&gt;&lt;/a&gt;&lt;/li&gt;\n										&lt;li class=&quot;nav&quot;&gt;&lt;a href=&quot;&quot;&gt;&lt;i class=&quot;icon-png&quot;&gt;&lt;/i&gt;&lt;/a&gt;&lt;/li&gt;\n									&lt;/ul&gt;\n									&lt;p&gt;\n									&lt;/p&gt;\n									&lt;p class=&quot;text-medium text-uppercase&quot;&gt;\n										&lt;img src=&quot;../uploads/kurumsal-logolar/navbea-logo.png&quot; alt=&quot;&quot;&gt;\n									&lt;/p&gt;\n									&lt;ul class=&quot;text-center center-col nav nav-tabs margin-six no-margin-lr no-margin-top xs-margin-ten xs-no-margin-lr xs-no-margin-top&quot;&gt;\n										&lt;li class=&quot;nav&quot;&gt;&lt;a href=&quot;&quot;&gt;&lt;i class=&quot;icon-pdf&quot;&gt;&lt;/i&gt;&lt;/a&gt;&lt;/li&gt;\n										&lt;li class=&quot;nav&quot;&gt;&lt;a href=&quot;&quot;&gt;&lt;i class=&quot;icon-ai&quot;&gt;&lt;/i&gt;&lt;/a&gt;&lt;/li&gt;\n										&lt;li class=&quot;nav&quot;&gt;&lt;a href=&quot;&quot;&gt;&lt;i class=&quot;icon-jpg&quot;&gt;&lt;/i&gt;&lt;/a&gt;&lt;/li&gt;\n										&lt;li class=&quot;nav&quot;&gt;&lt;a href=&quot;&quot;&gt;&lt;i class=&quot;icon-png&quot;&gt;&lt;/i&gt;&lt;/a&gt;&lt;/li&gt;\n									&lt;/ul&gt;\n									&lt;p&gt;\n									&lt;/p&gt;\n									&lt;p class=&quot;text-medium text-uppercase&quot;&gt;\n										&lt;img src=&quot;../uploads/kurumsal-logolar/mobilyakit-logo.png&quot; alt=&quot;&quot;&gt;\n									&lt;/p&gt;\n									&lt;ul class=&quot;text-center center-col nav nav-tabs margin-six no-margin-lr no-margin-top xs-margin-ten xs-no-margin-lr xs-no-margin-top&quot;&gt;\n										&lt;li class=&quot;nav&quot;&gt;&lt;a href=&quot;&quot;&gt;&lt;i class=&quot;icon-pdf&quot;&gt;&lt;/i&gt;&lt;/a&gt;&lt;/li&gt;\n										&lt;li class=&quot;nav&quot;&gt;&lt;a href=&quot;&quot;&gt;&lt;i class=&quot;icon-ai&quot;&gt;&lt;/i&gt;&lt;/a&gt;&lt;/li&gt;\n										&lt;li class=&quot;nav&quot;&gt;&lt;a href=&quot;&quot;&gt;&lt;i class=&quot;icon-jpg&quot;&gt;&lt;/i&gt;&lt;/a&gt;&lt;/li&gt;\n										&lt;li class=&quot;nav&quot;&gt;&lt;a href=&quot;&quot;&gt;&lt;i class=&quot;icon-png&quot;&gt;&lt;/i&gt;&lt;/a&gt;&lt;/li&gt;\n									&lt;/ul&gt;\n									&lt;p&gt;\n									&lt;/p&gt;\n								&lt;/div&gt;\n							&lt;/div&gt;\n							&lt;div class=&quot;row&quot;&gt;\n								&lt;div class=&quot;wide-separator-line&quot;&gt;\n								&lt;/div&gt;\n							&lt;/div&gt;\n							&lt;div class=&quot;row&quot;&gt;\n							&lt;/div&gt;\n						&lt;/div&gt;\n					&lt;/div&gt;\n					&lt;!-- end tab content --&gt;\n				&lt;/div&gt;\n				&lt;!-- end tab content section --&gt;\n			&lt;/div&gt;\n			&lt;!-- end tab --&gt;\n		&lt;/div&gt;\n	&lt;/div&gt;\n&lt;/div&gt;\n&lt;/section&gt;', '&lt;section class=&quot;wow border-bottom bg-white no-padding-top&quot; style=&quot;padding: 0px 0;&quot;&gt;\n&lt;div class=&quot;container&quot;&gt;\n	&lt;div class=&quot;row&quot;&gt;\n		&lt;!-- end side bar --&gt;\n		&lt;div class=&quot;col-md-12 col-sm-12 &quot;&gt;\n			&lt;!-- tab --&gt;\n			&lt;div class=&quot;col-md-12 col-sm-12 text-center tab-style9&quot; id=&quot;animated-tab&quot;&gt;\n				&lt;!-- tab navigation --&gt;\n				&lt;ul class=&quot;nav nav-tabs margin-six no-margin-lr no-margin-top xs-margin-ten xs-no-margin-lr xs-no-margin-top&quot;&gt;\n				&lt;/ul&gt;\n				&lt;!-- end tab navigation --&gt;\n				&lt;!-- tab content section --&gt;\n				&lt;div class=&quot;tab-content&quot;&gt;\n					&lt;!-- tab content --&gt;\n					&lt;div id=&quot;tab6_sec1&quot; class=&quot;col-md-12 col-sm-12 text-center center-col tab-pane fade active in&quot;&gt;\n						&lt;div class=&quot;tab-pane fade in&quot;&gt;\n							&lt;div class=&quot;row&quot;&gt;\n								&lt;div class=&quot;col-md-3 col-sm-12 text-left gray-text&quot;&gt;\n									&lt;span class=&quot;alt-font text-uppercase title-medium black-text element-title&quot;&gt;Hakkımızda&lt;/span&gt;\n									&lt;p class=&quot;text-large&quot;&gt;\n									&lt;/p&gt;\n								&lt;/div&gt;\n								&lt;div class=&quot;col-md-9 col-sm-12 text-left gray-text&quot;&gt;\n									&lt;div class=&quot;text-center&quot;&gt;\n										&lt;img src=&quot;../../uploads/vizyon-misyon.png&quot; alt=&quot;kodar vizyonu&quot;&gt;\n									&lt;/div&gt;\n									&lt;p class=&quot;title-kodar no-margin-bottom&quot;&gt;\n										POSMAKS, Kodar grubunun nokta satış çözümleri ürünüdür.&amp;nbsp;\n										&lt;br&gt;\n										&lt;br&gt;\n                                            Deneyimli ve alanında uzman kişilerden oluşan Kodar ekibi olarak, müşterilerimizin ve iş ortaklarımızın ihtiyaçları doğrultusunda anahtar teslim projelerden, ortak geliştirmeye, sistem entegrasyonundan, proje danışmanlığına kadar gereken hizmetleri üstlenmekteyiz. Amacımız en yenilikçi ürünleri, en iyi teknoloji işbirliğiyle, en iyi hizmet ve desteği sağlayarak sunmaktır. \n										&lt;br&gt;\n										&lt;br&gt;\n                                            2013 yılında kurulan firmamız, İstanbul’daki merkezinde 10+ uzman çalışanı ile hizmet vermektedir.\n										&lt;br&gt;\n										&lt;br&gt;\n									&lt;/p&gt;\n									&lt;span class=&quot;alt-font text-uppercase title-medium black-text element-title&quot;&gt;VİZYONUMUZ&lt;/span&gt;\n									&lt;p class=&quot;title-kodar no-margin-bottom&quot;&gt;\n                                                Kuruluşumuzda belirlediğimiz rotamızdan sapmadan Türkiye ve komşu ülkelerde marka değerimizi daha tanınır hale getirmek, insanlara daha yüksek bir kullanıcı deneyimi yaşatıp, mutlu olmalarını sağlamaktır.\n									&lt;/p&gt;\n									&lt;br&gt;\n									&lt;br&gt;\n									&lt;span class=&quot;alt-font text-uppercase title-medium black-text element-title&quot;&gt;MİSYONUMUZ&lt;/span&gt;\n									&lt;p class=&quot;title-kodar no-margin-bottom&quot;&gt;\n                                                Türk teknoloji ve Ar-Ge sektöründe kaliteli, sürdürülebilir, yenilikçi ve fark yaratan ürünlerin yer aldığı bir marka devamlılığı sağlamaktır. İşlevden uzaklaşmadan kullanıcılar ile duygusal etkileşim kuran ürünler sunan firmamız, sahip olduğu bu değerleri hiçbir başarının bedelsiz olmayacağı bilinci ile geleceğe de aktarmaya kararlıdır.\n									&lt;/p&gt;\n									&lt;br&gt;\n									&lt;br&gt;\n									&lt;span class=&quot;alt-font text-uppercase title-medium black-text element-title&quot;&gt;KALİTE POLİTİKAMIZ&lt;/span&gt;\n									&lt;p class=&quot;title-kodar no-margin-bottom&quot;&gt;\n                                                Ürettiğimiz yüksek teknoloji ürünleri, işlevsel ve kaliteli yazılım çözümlerimizle müşterilerimizin ihtiyaç ve beklentilerini karşılarken;\n									&lt;/p&gt;\n									&lt;br&gt;\n									&lt;div class=&quot;alert alert-success&quot; role=&quot;alert&quot;&gt;\n										1.Nitelikli ve entegre çalışabilen ekip yapısına sahip olmak,\n									&lt;/div&gt;\n									&lt;div class=&quot;alert alert-success&quot; role=&quot;alert&quot;&gt;\n										2.Verimliliğimizi ve rekabet gücümüzü arttırmak,\n									&lt;/div&gt;\n									&lt;div class=&quot;alert alert-success&quot; role=&quot;alert&quot;&gt;\n										3.Çevreye ve yasalara duyarlı bir şirket olarak topluma ve ülkeye katkıda bulunmak,\n									&lt;/div&gt;\n									&lt;div class=&quot;alert alert-success&quot; role=&quot;alert&quot;&gt;\n										4.Çalışma ortamı ve koşulları ile ürün ve süreçlerimizi sürekli iyileştirmek,\n									&lt;/div&gt;\n									&lt;div class=&quot;alert alert-success&quot; role=&quot;alert&quot;&gt;\n										5.Kalite yönetim sistemi standardının gereklerini karşılamak ve etkinliğini sürekli geliştirmek için gerekli kaynakları temin etmektir.\n									&lt;/div&gt;\n									&lt;p&gt;\n									&lt;/p&gt;\n									&lt;br&gt;\n									&lt;br&gt;\n								&lt;/div&gt;\n							&lt;/div&gt;\n						&lt;/div&gt;\n					&lt;/div&gt;\n					&lt;!-- end tab content --&gt;\n				&lt;/div&gt;\n			&lt;/div&gt;\n		&lt;/div&gt;\n	&lt;/div&gt;\n&lt;/div&gt;\n&lt;/section&gt;', '', 0, '', 0, 0, '', '', '', 0, '2016-02-06 12:01:55', '1.00', 1),
(48, 'Projects Showcase', 'SEKTÖREL', NULL, '&lt;p&gt;&lt;/p&gt;', NULL, 0, '', 1, 1, 'Displays selected projects in carousel view', 'Seçili projeleri gösterir.', 'projectshowcase', 0, '2016-02-02 12:12:08', '4.0', 1),
(52, 'Solutions Showcase', 'Çözümler Vitrini', NULL, '', NULL, 0, '', 1, 1, 'Displays selected solutions in carousel view', 'Seçili çözümlerigösterir.', 'solutionsshowcase', 0, '2016-02-02 12:12:08', '4.0', 1);
INSERT INTO `plugins` (`id`, `title_en`, `title_tr`, `body_en`, `body_tr`, `jscode`, `show_title`, `alt_class`, `system`, `cplugin`, `info_en`, `info_tr`, `plugalias`, `hasconfig`, `created`, `ver`, `active`) VALUES
(53, '', 'ANA SAYFA KURUMSAL', NULL, '&lt;section style=&quot;visibility: visible; animation-name: fadeIn;&quot; id=&quot;blog&quot; class=&quot;wow fadeIn animated&quot;&gt;\n&lt;div class=&quot;container&quot;&gt;\n	&lt;div class=&quot;row&quot;&gt;\n		&lt;!-- section title --&gt;\n		&lt;div class=&quot;col-md-12 col-sm-12 text-center&quot;&gt;\n			&lt;span class=&quot;alt-font text-uppercase title-medium black-text element-title&quot;&gt;SEKTÖREL ÇÖZÜMLER&lt;/span&gt;\n		&lt;/div&gt;\n		&lt;!-- end section title --&gt;\n	&lt;/div&gt;\n	&lt;div class=&quot;row blog-post-style7&quot;&gt;\n		&lt;div class=&quot;margin-fifteen no-margin-lr no-margin-bottom clearfix&quot;&gt;\n			&lt;!-- blog item --&gt;\n			&lt;article class=&quot;col-md-3 col-sm-3 xs-margin-ten xs-no-margin-lr xs-no-margin-top&quot;&gt;\n			&lt;div class=&quot;post-thumbnail overflow-hidden&quot;&gt;\n				&lt;a href=&quot;blog-details.html&quot;&gt;&lt;img src=&quot;uploads/images/fastfood.jpg&quot; alt=&quot;&quot;&gt;&lt;/a&gt;\n			&lt;/div&gt;\n			&lt;div class=&quot;post-details margin-fifteen no-margin-lr no-margin-bottom&quot;&gt;\n				&lt;span class=&quot;text-large text-uppercase display-block alt-font margin-seven no-margin-lr sm-text-medium xs-text-medium alt-font font-weight-600&quot;&gt;&lt;a href=&quot;blog-details.html&quot; class=&quot;dark-gray-text&quot;&gt;FASTFOOD&lt;/a&gt;&lt;/span&gt;\n				&lt;p&gt;\n					Genelde birden fazla kasa ve kasiyer bulunmaktadır. Bir numaralı amaç hızlı servistir. Kasiyerlerin servis hızı, gerektiğinde bir kaç müşteriye aynı anda hizmet verebilmesi önemlidir.\n				&lt;/p&gt;\n				&lt;a href=&quot;../posmaks/projects/sektor1/&quot; class=&quot;highlight-button btn btn-medium button&quot;&gt;İNCELE&lt;/a&gt;\n				&lt;div class=&quot;separator-line-full bg-mid-gray3 margin-thirteen no-margin-lr&quot;&gt;\n				&lt;/div&gt;\n			&lt;/div&gt;\n			&lt;/article&gt;\n			&lt;!-- end blog item --&gt;\n			&lt;!-- blog item --&gt;\n			&lt;article class=&quot;col-md-3 col-sm-3 xs-margin-ten xs-no-margin-lr xs-no-margin-top&quot;&gt;\n			&lt;div class=&quot;post-thumbnail overflow-hidden&quot;&gt;\n				&lt;a href=&quot;blog-details.html&quot;&gt;&lt;img src=&quot;uploads/images/restaurantsmall.jpg&quot; alt=&quot;&quot;&gt;&lt;/a&gt;\n			&lt;/div&gt;\n			&lt;div class=&quot;post-details margin-fifteen no-margin-lr no-margin-bottom&quot;&gt;\n				&lt;span class=&quot;text-large text-uppercase display-block alt-font margin-seven no-margin-lr sm-text-medium xs-text-medium alt-font font-weight-600&quot;&gt;&lt;a href=&quot;blog-details.html&quot; class=&quot;dark-gray-text&quot;&gt;RESTAURANT&lt;/a&gt;&lt;/span&gt;\n				&lt;p&gt;\n					Masa servisi uygulanır. Yiyeceklerin hazırlanma hızları ve diğer verilen siparişler ile uygun zamanda hazırlanması önemlidir.Mekanlar \n					geniştir ve müşterinin yanında hızlı sipariş almak önemlidir.\n				&lt;/p&gt;\n				&lt;a href=&quot;../posmaks/projects/sektor2/&quot; class=&quot;highlight-button btn btn-medium button&quot;&gt;İNCELE&lt;/a&gt;\n				&lt;div class=&quot;separator-line-full bg-mid-gray3 margin-thirteen no-margin-lr&quot;&gt;\n				&lt;/div&gt;\n			&lt;/div&gt;\n			&lt;/article&gt;\n			&lt;!-- end blog item --&gt;\n			&lt;!-- blog item --&gt;\n			&lt;article class=&quot;col-md-3 col-sm-3&quot;&gt;\n			&lt;div class=&quot;post-thumbnail overflow-hidden&quot;&gt;\n				&lt;a href=&quot;blog-details.html&quot;&gt;&lt;img src=&quot;uploads/images/coffee.jpg&quot; alt=&quot;&quot;&gt;&lt;/a&gt;\n			&lt;/div&gt;\n			&lt;div class=&quot;post-details margin-fifteen no-margin-lr no-margin-bottom&quot;&gt;\n				&lt;span class=&quot;text-large text-uppercase display-block alt-font margin-seven no-margin-lr sm-text-medium xs-text-medium alt-font font-weight-600&quot;&gt;&lt;a href=&quot;blog-details.html&quot; class=&quot;dark-gray-text&quot;&gt;KAHVE ZİNCİRLERİ&lt;/a&gt;&lt;/span&gt;\n				&lt;p&gt;\n					Self servis çalışan işletmelerin yanısıra hem tezgah hem masa servisi olan işletmeler de mevcuttur. Self servis için kasiyerin hızı önemlidir. İşletme alanları genelde çok büyük değildir...\n				&lt;/p&gt;\n				&lt;a href=&quot;../posmaks/projects/sektor3/&quot; class=&quot;highlight-button btn btn-medium button&quot;&gt;İNCELE&lt;/a&gt;\n				&lt;div class=&quot;separator-line-full bg-mid-gray3 margin-thirteen no-margin-lr&quot;&gt;\n				&lt;/div&gt;\n			&lt;/div&gt;\n			&lt;/article&gt;\n			&lt;!-- end blog item --&gt;\n			&lt;!-- blog item --&gt;\n			&lt;article class=&quot;col-md-3 col-sm-3&quot;&gt;\n			&lt;div class=&quot;post-thumbnail overflow-hidden&quot;&gt;\n				&lt;a href=&quot;blog-details.html&quot;&gt;&lt;img src=&quot;uploads/images/hotel.jpg&quot; alt=&quot;&quot;&gt;&lt;/a&gt;\n			&lt;/div&gt;\n			&lt;div class=&quot;post-details margin-fifteen no-margin-lr no-margin-bottom&quot;&gt;\n				&lt;span class=&quot;text-large text-uppercase display-block alt-font margin-seven no-margin-lr sm-text-medium xs-text-medium alt-font font-weight-600&quot;&gt;&lt;a href=&quot;blog-details.html&quot; class=&quot;dark-gray-text&quot;&gt;OTEL-KULÜP-OKUL KANTİNLERİ VB.&lt;/a&gt;&lt;/span&gt;\n				&lt;p&gt;\n					Hotel, kulüp, dernek, okul kantinleri v.b. kurumlarda, hem tezgah hem masa servisi vardır. Servis çok hızlı olmalıdır, aynı anda \n					birden fazla müşteri sırada bekleyebilir.\n				&lt;/p&gt;\n				&lt;a href=&quot;../posmaks/projects/sektor4/&quot; class=&quot;center highlight-button btn btn-medium button&quot;&gt;İNCELE&lt;/a&gt;\n				&lt;div class=&quot;separator-line-full bg-mid-gray3 margin-thirteen no-margin-lr&quot;&gt;\n				&lt;/div&gt;\n			&lt;/div&gt;\n			&lt;/article&gt;\n			&lt;!-- end blog item --&gt;\n		&lt;/div&gt;\n	&lt;/div&gt;\n&lt;/div&gt;\n&lt;/section&gt;', '', 0, '', 0, 0, NULL, '', '', 0, '2016-02-13 15:35:48', '1.00', 1),
(54, '', 'İnsan Kaynakları', NULL, '&lt;div style=&quot;visibility: visible; animation-name: fadeIn; background-color:white;&quot; id=&quot;about&quot; class=&quot;wow fadeIn animated&quot; &gt;\n&lt;div class=&quot;container&quot;&gt;\n&lt;h3 style=&quot;padding-top:20px;&quot;&gt;Kodar Ailesi&lt;/h3&gt;\n&lt;br&gt;\n\n\nKodar, en değerli sermayesini insan kaynakları olarak kabul etmektedir. Bu nedenle çalışma arkadaşlarımıza, mutlu olacakları, mesleki ve insani açılardan kendilerini geliştirebilecekleri bir çalışma ortamı sunmayı birinci hedefimiz olarak görüyoruz.&amp;nbsp;\n\n\nİnsan potansiyelinin sınırı yoktur. Başarı, beraberinde daha iyisini yapma arzusu getirir. Sizi, motive eden, başarıya yönlendiren ve sürekli gelişim sağlayan, samimi bir çalışma ortamına davet ediyoruz.\n&lt;br&gt;&lt;br&gt;\n&lt;h3&gt;Kariyer&lt;/h3&gt;\n&lt;br&gt;\nTakım çalışmasını iyi bilen, bireysel inisiyatif ve sorumluluk alan, dürüstlük ve açıklık ilkesi ile hareket eden, kendisini geliştirmek için çalışan, kurum değerlerine sahip çıkan her çalışanımız; Kodar&#039;da sürekli gelişim ve ileri kariyer fırsatlarına sahip olur.\n&lt;br&gt;&lt;br&gt;&lt;br&gt;\n&lt;p&gt;\n&lt;/p&gt;\n&lt;/div&gt;\n&lt;/div&gt;', '', 0, '', 0, 0, NULL, '', '', 0, '2016-02-14 01:44:32', '1.00', 1),
(55, '', 'Ana Sayfa Biz Kimiz', NULL, '&lt;section style=&quot;visibility: visible; animation-name: fadeIn;&quot; class=&quot;wow fadeIn bg-fast-yellow work-with-us animated&quot;&gt;\n   &lt;div class=&quot;container&quot;&gt;\n                &lt;div class=&quot;row blog-post-style7&quot;&gt;\n                    &lt;div class=&quot;margin-fifteen no-margin-lr no-margin-bottom clearfix&quot;&gt;\n                        &lt;!-- blog item --&gt;\n                        &lt;article class=&quot;col-md-4 col-sm-4 xs-margin-ten xs-no-margin-lr xs-no-margin-top&quot;&gt;\n                            &lt;div class=&quot;post-thumbnail overflow-hidden&quot;&gt;\n                                &lt;a href=&quot;blog-details.html&quot;&gt;&lt;img src=&quot;images/wedding-img22.jpg&quot; alt=&quot;&quot;&gt;&lt;/a&gt;\n                            &lt;/div&gt;\n                            &lt;div class=&quot;post-details margin-fifteen no-margin-lr no-margin-bottom&quot;&gt;\n                                &lt;span class=&quot;text-extra-small post-categories text-uppercase letter-spacing-1 alt-font bg-black display-inline-block&quot;&gt;&lt;a href=&quot;blog.html&quot; class=&quot;white-text&quot;&gt;The Couple&lt;/a&gt;&lt;/span&gt;\n                                &lt;span class=&quot;text-large text-uppercase display-block alt-font margin-seven no-margin-lr sm-text-medium xs-text-medium alt-font font-weight-600&quot;&gt;&lt;a href=&quot;blog-details.html&quot; class=&quot;dark-gray-text&quot;&gt;Weddings are important because they celebrate life and possibility.&lt;/a&gt;&lt;/span&gt;\n                                &lt;p&gt;Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#039;s standard dummy text.&lt;/p&gt;\n                                &lt;div class=&quot;separator-line-full bg-mid-gray3 margin-thirteen no-margin-lr&quot;&gt;&lt;/div&gt;\n                                &lt;span class=&quot;text-small text-uppercase alt-font light-gray-text&quot;&gt;&lt;a href=&quot;#&quot; class=&quot;light-gray-text&quot;&gt;26 July 2015&lt;/a&gt;   /   Posted By&lt;a href=&quot;blog.html&quot; class=&quot;light-gray-text&quot;&gt; Nathan Ford&lt;/a&gt;&lt;/span&gt;\n                            &lt;/div&gt;\n                        &lt;/article&gt;\n                        &lt;!-- end blog item --&gt;\n                        &lt;!-- blog item --&gt;\n                        &lt;article class=&quot;col-md-4 col-sm-4 xs-margin-ten xs-no-margin-lr xs-no-margin-top&quot;&gt;\n                            &lt;div class=&quot;post-thumbnail overflow-hidden&quot;&gt;\n                                &lt;a href=&quot;blog-details.html&quot;&gt;&lt;img src=&quot;images/wedding-img23.jpg&quot; alt=&quot;&quot;&gt;&lt;/a&gt;\n                            &lt;/div&gt;\n                            &lt;div class=&quot;post-details margin-fifteen no-margin-lr no-margin-bottom&quot;&gt;\n                                &lt;span class=&quot;text-extra-small post-categories text-uppercase letter-spacing-1 alt-font bg-black display-inline-block&quot;&gt;&lt;a href=&quot;blog.html&quot; class=&quot;white-text&quot;&gt;Wedding&lt;/a&gt;&lt;/span&gt;\n                                &lt;span class=&quot;text-large text-uppercase display-block alt-font margin-seven no-margin-lr sm-text-medium xs-text-medium alt-font font-weight-600&quot;&gt;&lt;a href=&quot;blog-details.html&quot; class=&quot;dark-gray-text&quot;&gt;A woman with a fine prospect of happiness behind her.&lt;/a&gt;&lt;/span&gt;\n                                &lt;p&gt;Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#039;s standard dummy text.&lt;/p&gt;\n                                &lt;div class=&quot;separator-line-full bg-mid-gray3 margin-thirteen no-margin-lr&quot;&gt;&lt;/div&gt;\n                                &lt;span class=&quot;text-small text-uppercase alt-font light-gray-text&quot;&gt;&lt;a href=&quot;#&quot; class=&quot;light-gray-text&quot;&gt;26 July 2015&lt;/a&gt;   /   Posted By&lt;a href=&quot;blog.html&quot; class=&quot;light-gray-text&quot;&gt; Nathan Ford&lt;/a&gt;&lt;/span&gt;\n                            &lt;/div&gt;\n                        &lt;/article&gt;\n                        &lt;!-- end blog item --&gt;\n                        &lt;!-- blog item --&gt;\n                        &lt;article class=&quot;col-md-4 col-sm-4&quot;&gt;\n                            &lt;div class=&quot;post-thumbnail overflow-hidden&quot;&gt;\n                                &lt;a href=&quot;blog-details.html&quot;&gt;&lt;img src=&quot;images/wedding-img24.jpg&quot; alt=&quot;&quot;&gt;&lt;/a&gt;\n                            &lt;/div&gt;\n                            &lt;div class=&quot;post-details margin-fifteen no-margin-lr no-margin-bottom&quot;&gt;\n                                &lt;span class=&quot;text-extra-small post-categories text-uppercase letter-spacing-1 alt-font bg-black display-inline-block&quot;&gt;&lt;a href=&quot;blog.html&quot; class=&quot;white-text&quot;&gt;Wedding&lt;/a&gt;&lt;/span&gt;\n                                &lt;span class=&quot;text-large text-uppercase display-block alt-font margin-seven no-margin-lr sm-text-medium xs-text-medium alt-font font-weight-600&quot;&gt;&lt;a href=&quot;blog-details.html&quot; class=&quot;dark-gray-text&quot;&gt;I was married by a judge. I should have asked for a jury.&lt;/a&gt;&lt;/span&gt;\n                                &lt;p&gt;Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#039;s standard dummy text.&lt;/p&gt;\n                                &lt;div class=&quot;separator-line-full bg-mid-gray3 margin-thirteen no-margin-lr&quot;&gt;&lt;/div&gt;\n                                &lt;span class=&quot;text-small text-uppercase alt-font light-gray-text&quot;&gt;&lt;a href=&quot;#&quot; class=&quot;light-gray-text&quot;&gt;26 July 2015&lt;/a&gt;   /   Posted By&lt;a href=&quot;blog.html&quot; class=&quot;light-gray-text&quot;&gt; Nathan Ford&lt;/a&gt;&lt;/span&gt;\n                            &lt;/div&gt;\n                        &lt;/article&gt;\n                        &lt;!-- end blog item --&gt;\n                    &lt;/div&gt;\n                &lt;/div&gt;\n            &lt;/div&gt;\n&lt;/section&gt;', '', 0, '', 0, 0, NULL, '', '', 0, '2016-02-14 03:18:49', '1.00', 1),
(56, '', 'Ar-Ge', NULL, '\n&lt;section class=&quot;wow border-bottom bg-white padding-two animated&quot; style=&quot;visibility: visible;&quot;&gt;\n&lt;div class=&quot;container&quot;&gt;\n	&lt;div class=&quot;row&quot;&gt;\n		&lt;div class=&quot;col-md-12 col-sm-12&quot;&gt;\n&lt;div class=&quot;col-md-12 col-sm-12 tab-style9&quot; id=&quot;animated-tab&quot;&gt;\n			&lt;p class=&quot;text-extra-large margin-four no-margin-lr no-margin-top display-inline-block alt-font black-text no-padding-bottom cursor-default&quot;&gt;\n				Kodar, kurulduğu yıldan bugüne kadar son teknolojiyi takip eden ve yenilikçi ürünler üretebilmek için yerli Ar-Ge’yi birinci önceliğine alan bir bakış açısı geliştirmiştir. Ürünlerinden sadece kendisinin sorumlu olduğunun bilinciyle, yaptığı eylemin kalitesini omuzlanarak ve hedeflenen uygun maliyet ile piyasa öncüsü standartları sayesinde, yenilikçi ve farklı teknolojik atılımlar yapma imkanı bulmuştur.\n			&lt;/p&gt;\n			&lt;div class=&quot;col-md-12 col-sm-12 &quot;&gt;\n				&lt;div class=&quot;col-md-6 col-sm-12 &quot;&gt;\n					&lt;img src=&quot;uploads/ar-ge-kodar-01.jpg&quot; alt=&quot;ar-ge-kodar-01&quot;&gt;\n				&lt;/div&gt;\n				&lt;div class=&quot;col-md-6 col-sm-12 &quot;&gt;\n					&lt;p&gt;\n				Kodar teknik ekibi ve çalışanları; yeni şartlara uygun,&amp;nbsp;kurumsal çalışmalar ile web, mobil, elektronik, haberleşme ve yazılım alanlarında projeler geliştirmekte, kullanıcılara yönelik işlevsel ürünler ortaya koymaktadır. Firma içerisinde çeşitli sektörlere ayrılmış, birbiri ile entegre çözümler üretilmesi, normal şartlar altında beklenmeyen katma değerlerin oluşmasına imkan sağlamakta, kurum ve kişiler için yüksek memnuniyet taşıyan teknolojik deneyimlerin oluşmasına sebep olmaktadır.\n					&lt;/p&gt;\n					&lt;p&gt;\n				Müşteri ile ilişkilerin ürünün henüz tasarımı esnasında başladığına inanan ekibimiz, en başından itibaren kullanıcıların değerlendirme ve kullanım alışkanlıklarını analiz ederek, talep ve beklentilere en uygun çözümleri oluşturmaya çalışmaktadır. Bu çözümler; efektif maliyet ve yüksek kalitede malzeme&amp;nbsp;/&amp;nbsp;kaynak&amp;nbsp;/&amp;nbsp;k\n						&lt;wbr&gt;od ile birleşerek, insan ihtiyaçlarına uygun&amp;nbsp;bir tercih olarak planlanmaktadır. Özel projelerde ise sürecin en başından itibaren karşılıklı bir stratejik ortaklığın başladığı kabul edilerek, Ar-Ge süresince kullanıcı ile karşılıklı koordinasyon ve plana uygunluk firmanın değişmez prensipleridir.\n					&lt;/p&gt;\n					&lt;p&gt;\n				Kodar, %100 yerli üretim oranını tutturarak başarılı Türkiye markalarını oluşturmak, ülkemiz ve milletimiz için faydalı ve katma değeri yüksek projeleri hayata geçirmek istemektedir. Bunun için mevcut faaliyetlerininin Ar-Ge yoğunluğunu arttırarak devam ettirmektedir.\n					&lt;/p&gt;\n				&lt;/div&gt;\n			&lt;/div&gt;\n		&lt;/div&gt;\n	&lt;/div&gt;\n&lt;/div&gt;&lt;/div&gt;\n&lt;/section&gt;', '', 0, '', 0, 0, NULL, '', '', 0, '2016-02-29 03:08:08', '1.00', 1),
(57, '', 'NASIL ÇALIŞIR EKLENTİ', NULL, '&lt;section id=&quot;nasil-calisir&quot; class=&quot;wow border-bottom bg-white padding-two animated&quot; style=&quot;visibility: visible; padding-top: 130px !important;&quot;&gt;\n&lt;div class=&quot;container&quot;&gt;\n	&lt;div&gt;\n		&lt;img src=&quot;../posmaks/uploads/images/restoransema.jpg&quot;&gt;\n	&lt;/div&gt;\n	&lt;div class=&quot;row&quot;&gt;\n		&lt;!-- features box --&gt;\n		&lt;div class=&quot;row&quot;&gt;\n			&lt;!-- features box --&gt;\n			&lt;div class=&quot;row&quot;&gt;\n				&lt;div class=&quot;container&quot;&gt;\n					&lt;!-- features box --&gt;\n					&lt;div class=&quot;col-md-4 col-sm-4 service-sub text-center xs-margin-five xs-no-margin-tb&quot;&gt;\n						&lt;i class=&quot;icon-wine icon-extra-large gray-text margin-seven no-margin-lr no-margin-top&quot;&gt;&lt;/i&gt;\n						&lt;span class=&quot;text-medium font-weight-600 letter-spacing-2 text-uppercase gray-text margin-one no-margin-lr no-margin-top display-block alt-font&quot;&gt;RESTAURANT&lt;/span&gt;\n						&lt;p class=&quot;text-medium center-col&quot;&gt;\n						Birden fazla restaurantın tek noktadan verilerine ulaşabilir, raporlarını takip edebilir ve yönetebilirsiniz.\n						&lt;/p&gt;\n					&lt;/div&gt;\n					&lt;!-- end features box --&gt;\n					&lt;!-- features box --&gt;\n					&lt;div class=&quot;row&quot;&gt;\n						&lt;div class=&quot;container&quot;&gt;\n							&lt;!-- features box --&gt;\n							&lt;div class=&quot;col-md-4 col-sm-4 service-sub text-center xs-margin-five xs-no-margin-tb&quot;&gt;\n								&lt;i class=&quot;icon-briefcase icon-extra-large gray-text margin-seven no-margin-lr no-margin-top&quot;&gt;&lt;/i&gt;\n								&lt;span class=&quot;text-medium font-weight-600 letter-spacing-2 text-uppercase gray-text margin-one no-margin-lr no-margin-top display-block alt-font&quot;&gt;KASA&lt;/span&gt;\n								&lt;p class=&quot;text-medium center-col&quot;&gt;\n						Nakit, kredi kartı, promosyon ve Ticket olmak üzere 4 farklı ödeme yapabilir. İstediğiniz zaman adisyon ve fatura alabilirsiniz.\n								&lt;/p&gt;\n							&lt;/div&gt;\n							&lt;!-- end features box --&gt;\n							&lt;!-- features box --&gt;\n							&lt;div class=&quot;row&quot;&gt;\n								&lt;div class=&quot;container&quot;&gt;\n									&lt;!-- features box --&gt;\n									&lt;div class=&quot;col-md-4 col-sm-4 service-sub text-center xs-margin-five xs-no-margin-tb&quot;&gt;\n										&lt;i class=&quot;icon-map-pin icon-extra-large gray-text margin-seven no-margin-lr no-margin-top&quot;&gt;&lt;/i&gt;\n										&lt;span class=&quot;text-medium font-weight-600 letter-spacing-2 text-uppercase gray-text margin-one no-margin-lr no-margin-top display-block alt-font&quot;&gt;MASA TAKİP YÖNETİMİ&lt;/span&gt;\n										&lt;p class=&quot;text-medium center-col&quot;&gt;\n						İstediğiniz sayıda masa ekleyip çıkarabilir, boş ve dolu masaları görüntüleyebilirsiniz. İstediğiniz dolu masaları boş masalara veya birden fazla dolu masayı birleştirip tek masaya dönüştürebilirsiniz.\n										&lt;/p&gt;\n									&lt;/div&gt;\n									&lt;!-- end features box --&gt;\n									&lt;!-- features box --&gt;\n									&lt;div class=&quot;row&quot;&gt;\n										&lt;!-- features box --&gt;\n										&lt;div class=&quot;row&quot;&gt;\n											&lt;div class=&quot;container&quot;&gt;\n												&lt;!-- features box --&gt;\n												&lt;div class=&quot;col-md-4 col-sm-4 service-sub text-center xs-margin-five xs-no-margin-tb&quot;&gt;\n													&lt;i class=&quot;icon-basket icon-extra-large gray-text margin-seven no-margin-lr no-margin-top&quot;&gt;&lt;/i&gt;\n													&lt;span class=&quot;text-medium font-weight-600 letter-spacing-2 text-uppercase gray-text margin-one no-margin-lr no-margin-top display-block alt-font&quot;&gt;MENÜ VE ÜRÜN YÖNETİMİ&lt;/span&gt;\n													&lt;p class=&quot;text-medium center-col&quot;&gt;\n						Sınırsız sayıda ürün ekleyebilir, durum kontrolü yapabilir, yanlış girdiğiniz veya değiştirmek istediğiniz ürünleri güncelleyebilirsiniz.\n													&lt;/p&gt;\n												&lt;/div&gt;\n												&lt;!-- end features box --&gt;\n												&lt;!-- features box --&gt;\n												&lt;div class=&quot;row&quot;&gt;\n													&lt;div class=&quot;container&quot;&gt;\n														&lt;!-- features box --&gt;\n														&lt;div class=&quot;col-md-4 col-sm-4 service-sub text-center xs-margin-five xs-no-margin-tb&quot;&gt;\n															&lt;i class=&quot;icon-profile-male icon-extra-large gray-text margin-seven no-margin-lr no-margin-top&quot;&gt;&lt;/i&gt;\n															&lt;span class=&quot;text-medium font-weight-600 letter-spacing-2 text-uppercase gray-text margin-one no-margin-lr no-margin-top display-block alt-font&quot;&gt;PERSONEL YÖNETİMİ&lt;/span&gt;\n															&lt;p class=&quot;text-medium center-col&quot;&gt;\n						İşletmenizde her çalışan için ayrı kullanıcı adı ve şifre verebilirsiniz. Oluşturduğunuz kullanıcılara yetkiler ve rol verebilirsiniz. Böylelikle kullanıcıları kısıtlamış olursunuz.\n															&lt;/p&gt;\n														&lt;/div&gt;\n														&lt;!-- end features box --&gt;\n														&lt;!-- features box --&gt;\n														&lt;div class=&quot;row&quot;&gt;\n															&lt;div class=&quot;container&quot;&gt;\n																&lt;!-- features box --&gt;\n																&lt;div class=&quot;col-md-4 col-sm-4 service-sub text-center xs-margin-five xs-no-margin-tb&quot;&gt;\n																	&lt;i class=&quot;icon-recycle icon-extra-large gray-text margin-seven no-margin-lr no-margin-top&quot;&gt;&lt;/i&gt;\n																	&lt;span class=&quot;text-medium font-weight-600 letter-spacing-2 text-uppercase gray-text margin-one no-margin-lr no-margin-top display-block alt-font&quot;&gt;STOK YÖNETİMİ&lt;/span&gt;\n																	&lt;p class=&quot;text-medium center-col&quot;&gt;\n						Ürün stoklarınızın takibini online olarak yapabilir, yeni stok oluşturabilir ve stoklar üzerinde ekleme veya çıkarma yapabilirsiniz. Ayrıca stoklarınız azaldığında sistem size stok durumuyla ilgili uyarı verir.\n																	&lt;/p&gt;\n																&lt;/div&gt;\n																&lt;!-- end features box --&gt;\n																&lt;!-- features box --&gt;\n																&lt;div class=&quot;row&quot;&gt;\n																	&lt;!-- features box --&gt;\n																	&lt;div class=&quot;row&quot;&gt;\n																		&lt;div class=&quot;container&quot;&gt;\n																			&lt;!-- features box --&gt;\n																			&lt;div class=&quot;col-md-4 col-sm-4 service-sub text-center xs-margin-five xs-no-margin-tb&quot;&gt;\n																				&lt;i class=&quot;icon-desktop icon-extra-large gray-text margin-seven no-margin-lr no-margin-top&quot;&gt;&lt;/i&gt;\n																				&lt;span class=&quot;text-medium font-weight-600 letter-spacing-2 text-uppercase gray-text margin-one no-margin-lr no-margin-top display-block alt-font&quot;&gt;KOLAY KULLANIM ESNEK ARAYÜZ&lt;/span&gt;\n																				&lt;p class=&quot;text-medium center-col&quot;&gt;\n						Yetkili kullanıcıların ve personellerin kısa sürede kavrayabileceği , kullanımı basit ve esnek arayüz.\n																				&lt;/p&gt;\n																			&lt;/div&gt;\n																			&lt;!-- end features box --&gt;\n																			&lt;!-- features box --&gt;\n																			&lt;div class=&quot;row&quot;&gt;\n																				&lt;div class=&quot;container&quot;&gt;\n																					&lt;!-- features box --&gt;\n																					&lt;div class=&quot;col-md-4 col-sm-4 service-sub text-center xs-margin-five xs-no-margin-tb&quot;&gt;\n																						&lt;i class=&quot;icon-presentation icon-extra-large gray-text margin-seven no-margin-lr no-margin-top&quot;&gt;&lt;/i&gt;\n																						&lt;span class=&quot;text-medium font-weight-600 letter-spacing-2 text-uppercase gray-text margin-one no-margin-lr no-margin-top display-block alt-font&quot;&gt;RAPORLAMA MODÜLÜ&lt;/span&gt;\n																						&lt;p class=&quot;text-medium center-col&quot;&gt;\n						İstediğiniz tarih aralığındaki gelir – giderlerinizi , kart – nakit – puan satışınızın ayrı ayrı ne kadar olduğunu görebilir. Tek tuşla gün sonu alabilirsiniz.\n																						&lt;/p&gt;\n																					&lt;/div&gt;\n																					&lt;!-- end features box --&gt;\n																					&lt;!-- features box --&gt;\n																					&lt;div class=&quot;row&quot;&gt;\n																						&lt;div class=&quot;container&quot;&gt;\n																							&lt;!-- features box --&gt;\n																							&lt;div class=&quot;col-md-4 col-sm-4 service-sub text-center xs-margin-five xs-no-margin-tb&quot;&gt;\n																								&lt;i class=&quot;icon-shield icon-extra-large gray-text margin-seven no-margin-lr no-margin-top&quot;&gt;&lt;/i&gt;\n																								&lt;span class=&quot;text-medium font-weight-600 letter-spacing-2 text-uppercase gray-text margin-one no-margin-lr no-margin-top display-block alt-font&quot;&gt;GÜVENLİK&lt;/span&gt;\n																								&lt;p class=&quot;text-medium center-col&quot;&gt;\n						Sistem önceden belirlenmiş belirli periyodlarda yedeklenir, herhangi bir problemde tüm kayıtlar güvenlik altında tutulur.\n																								&lt;/p&gt;\n																							&lt;/div&gt;\n																							&lt;!-- end features box --&gt;\n																						&lt;/div&gt;\n																					&lt;/div&gt;\n																				&lt;/div&gt;\n																			&lt;/div&gt;\n																		&lt;/div&gt;\n																	&lt;/div&gt;\n																&lt;/div&gt;\n															&lt;/div&gt;\n														&lt;/div&gt;\n													&lt;/div&gt;\n												&lt;/div&gt;\n											&lt;/div&gt;\n										&lt;/div&gt;\n									&lt;/div&gt;\n								&lt;/div&gt;\n							&lt;/div&gt;\n						&lt;/div&gt;\n					&lt;/div&gt;\n				&lt;/div&gt;\n			&lt;/div&gt;\n		&lt;/div&gt;\n	&lt;/div&gt;\n&lt;/div&gt;\n&lt;/section&gt;', '', 0, '', 0, 0, NULL, '', '', 0, '2016-03-25 16:22:05', '1.00', 1),
(58, '', 'ÖZELLİKLER EKLENTİ', NULL, '\n\n\n&lt;section id=&quot;ozellikler&quot; class=&quot;wow border-bottom bg-white padding-two animated&quot; style=&quot;visibility: visible; padding-top: 130px !important;&quot;&gt;\n&lt;div class=&quot;container&quot;&gt;\n	&lt;div&gt;\n		&lt;img src=&quot;../posmaks/uploads/images/restoransema.jpg&quot;&gt;\n	&lt;/div&gt;\n	&lt;div class=&quot;row&quot;&gt;\n		&lt;!-- features box --&gt;\n		&lt;div class=&quot;row&quot;&gt;\n			&lt;!-- features box --&gt;\n			&lt;div class=&quot;row&quot;&gt;\n				&lt;div class=&quot;container&quot;&gt;\n					&lt;!-- features box --&gt;\n					&lt;div class=&quot;col-md-4 col-sm-4 service-sub text-center xs-margin-five xs-no-margin-tb&quot;&gt;\n						&lt;i class=&quot;icon-wine icon-extra-large gray-text margin-seven no-margin-lr no-margin-top&quot;&gt;&lt;/i&gt;\n						&lt;span class=&quot;text-medium font-weight-600 letter-spacing-2 text-uppercase gray-text margin-one no-margin-lr no-margin-top display-block alt-font&quot;&gt;RESTAURANT&lt;/span&gt;\n						&lt;p class=&quot;text-medium center-col&quot;&gt;\n						Birden fazla restaurantın tek noktadan verilerine ulaşabilir, raporlarını takip edebilir ve yönetebilirsiniz.\n						&lt;/p&gt;\n					&lt;/div&gt;\n					&lt;!-- end features box --&gt;\n					&lt;!-- features box --&gt;\n					&lt;div class=&quot;row&quot;&gt;\n						&lt;div class=&quot;container&quot;&gt;\n							&lt;!-- features box --&gt;\n							&lt;div class=&quot;col-md-4 col-sm-4 service-sub text-center xs-margin-five xs-no-margin-tb&quot;&gt;\n								&lt;i class=&quot;icon-briefcase icon-extra-large gray-text margin-seven no-margin-lr no-margin-top&quot;&gt;&lt;/i&gt;\n								&lt;span class=&quot;text-medium font-weight-600 letter-spacing-2 text-uppercase gray-text margin-one no-margin-lr no-margin-top display-block alt-font&quot;&gt;KASA&lt;/span&gt;\n								&lt;p class=&quot;text-medium center-col&quot;&gt;\n						Nakit, kredi kartı, promosyon ve Ticket olmak üzere 4 farklı ödeme yapabilir. İstediğiniz zaman adisyon ve fatura alabilirsiniz.\n								&lt;/p&gt;\n							&lt;/div&gt;\n							&lt;!-- end features box --&gt;\n							&lt;!-- features box --&gt;\n							&lt;div class=&quot;row&quot;&gt;\n								&lt;div class=&quot;container&quot;&gt;\n									&lt;!-- features box --&gt;\n									&lt;div class=&quot;col-md-4 col-sm-4 service-sub text-center xs-margin-five xs-no-margin-tb&quot;&gt;\n										&lt;i class=&quot;icon-map-pin icon-extra-large gray-text margin-seven no-margin-lr no-margin-top&quot;&gt;&lt;/i&gt;\n										&lt;span class=&quot;text-medium font-weight-600 letter-spacing-2 text-uppercase gray-text margin-one no-margin-lr no-margin-top display-block alt-font&quot;&gt;MASA TAKİP YÖNETİMİ&lt;/span&gt;\n										&lt;p class=&quot;text-medium center-col&quot;&gt;\n						İstediğiniz sayıda masa ekleyip çıkarabilir, boş ve dolu masaları görüntüleyebilirsiniz. İstediğiniz dolu masaları boş masalara veya birden fazla dolu masayı birleştirip tek masaya dönüştürebilirsiniz.\n										&lt;/p&gt;\n									&lt;/div&gt;\n									&lt;!-- end features box --&gt;\n									&lt;!-- features box --&gt;\n									&lt;div class=&quot;row&quot;&gt;\n										&lt;!-- features box --&gt;\n										&lt;div class=&quot;row&quot;&gt;\n											&lt;div class=&quot;container&quot;&gt;\n												&lt;!-- features box --&gt;\n												&lt;div class=&quot;col-md-4 col-sm-4 service-sub text-center xs-margin-five xs-no-margin-tb&quot;&gt;\n													&lt;i class=&quot;icon-basket icon-extra-large gray-text margin-seven no-margin-lr no-margin-top&quot;&gt;&lt;/i&gt;\n													&lt;span class=&quot;text-medium font-weight-600 letter-spacing-2 text-uppercase gray-text margin-one no-margin-lr no-margin-top display-block alt-font&quot;&gt;MENÜ VE ÜRÜN YÖNETİMİ&lt;/span&gt;\n													&lt;p class=&quot;text-medium center-col&quot;&gt;\n						Sınırsız sayıda ürün ekleyebilir, durum kontrolü yapabilir, yanlış girdiğiniz veya değiştirmek istediğiniz ürünleri güncelleyebilirsiniz.\n													&lt;/p&gt;\n												&lt;/div&gt;\n												&lt;!-- end features box --&gt;\n												&lt;!-- features box --&gt;\n												&lt;div class=&quot;row&quot;&gt;\n													&lt;div class=&quot;container&quot;&gt;\n														&lt;!-- features box --&gt;\n														&lt;div class=&quot;col-md-4 col-sm-4 service-sub text-center xs-margin-five xs-no-margin-tb&quot;&gt;\n															&lt;i class=&quot;icon-profile-male icon-extra-large gray-text margin-seven no-margin-lr no-margin-top&quot;&gt;&lt;/i&gt;\n															&lt;span class=&quot;text-medium font-weight-600 letter-spacing-2 text-uppercase gray-text margin-one no-margin-lr no-margin-top display-block alt-font&quot;&gt;PERSONEL YÖNETİMİ&lt;/span&gt;\n															&lt;p class=&quot;text-medium center-col&quot;&gt;\n						İşletmenizde her çalışan için ayrı kullanıcı adı ve şifre verebilirsiniz. Oluşturduğunuz kullanıcılara yetkiler ve rol verebilirsiniz. Böylelikle kullanıcıları kısıtlamış olursunuz.\n															&lt;/p&gt;\n														&lt;/div&gt;\n														&lt;!-- end features box --&gt;\n														&lt;!-- features box --&gt;\n														&lt;div class=&quot;row&quot;&gt;\n															&lt;div class=&quot;container&quot;&gt;\n																&lt;!-- features box --&gt;\n																&lt;div class=&quot;col-md-4 col-sm-4 service-sub text-center xs-margin-five xs-no-margin-tb&quot;&gt;\n																	&lt;i class=&quot;icon-recycle icon-extra-large gray-text margin-seven no-margin-lr no-margin-top&quot;&gt;&lt;/i&gt;\n																	&lt;span class=&quot;text-medium font-weight-600 letter-spacing-2 text-uppercase gray-text margin-one no-margin-lr no-margin-top display-block alt-font&quot;&gt;STOK YÖNETİMİ&lt;/span&gt;\n																	&lt;p class=&quot;text-medium center-col&quot;&gt;\n						Ürün stoklarınızın takibini online olarak yapabilir, yeni stok oluşturabilir ve stoklar üzerinde ekleme veya çıkarma yapabilirsiniz. Ayrıca stoklarınız azaldığında sistem size stok durumuyla ilgili uyarı verir.\n																	&lt;/p&gt;\n																&lt;/div&gt;\n																&lt;!-- end features box --&gt;\n																&lt;!-- features box --&gt;\n																&lt;div class=&quot;row&quot;&gt;\n																	&lt;!-- features box --&gt;\n																	&lt;div class=&quot;row&quot;&gt;\n																		&lt;div class=&quot;container&quot;&gt;\n																			&lt;!-- features box --&gt;\n																			&lt;div class=&quot;col-md-4 col-sm-4 service-sub text-center xs-margin-five xs-no-margin-tb&quot;&gt;\n																				&lt;i class=&quot;icon-desktop icon-extra-large gray-text margin-seven no-margin-lr no-margin-top&quot;&gt;&lt;/i&gt;\n																				&lt;span class=&quot;text-medium font-weight-600 letter-spacing-2 text-uppercase gray-text margin-one no-margin-lr no-margin-top display-block alt-font&quot;&gt;KOLAY KULLANIM ESNEK ARAYÜZ&lt;/span&gt;\n																				&lt;p class=&quot;text-medium center-col&quot;&gt;\n						Yetkili kullanıcıların ve personellerin kısa sürede kavrayabileceği , kullanımı basit ve esnek arayüz.\n																				&lt;/p&gt;\n																			&lt;/div&gt;\n																			&lt;!-- end features box --&gt;\n																			&lt;!-- features box --&gt;\n																			&lt;div class=&quot;row&quot;&gt;\n																				&lt;div class=&quot;container&quot;&gt;\n																					&lt;!-- features box --&gt;\n																					&lt;div class=&quot;col-md-4 col-sm-4 service-sub text-center xs-margin-five xs-no-margin-tb&quot;&gt;\n																						&lt;i class=&quot;icon-presentation icon-extra-large gray-text margin-seven no-margin-lr no-margin-top&quot;&gt;&lt;/i&gt;\n																						&lt;span class=&quot;text-medium font-weight-600 letter-spacing-2 text-uppercase gray-text margin-one no-margin-lr no-margin-top display-block alt-font&quot;&gt;RAPORLAMA MODÜLÜ&lt;/span&gt;\n																						&lt;p class=&quot;text-medium center-col&quot;&gt;\n						İstediğiniz tarih aralığındaki gelir – giderlerinizi , kart – nakit – puan satışınızın ayrı ayrı ne kadar olduğunu görebilir. Tek tuşla gün sonu alabilirsiniz.\n																						&lt;/p&gt;\n																					&lt;/div&gt;\n																					&lt;!-- end features box --&gt;\n																					&lt;!-- features box --&gt;\n																					&lt;div class=&quot;row&quot;&gt;\n																						&lt;div class=&quot;container&quot;&gt;\n																							&lt;!-- features box --&gt;\n																							&lt;div class=&quot;col-md-4 col-sm-4 service-sub text-center xs-margin-five xs-no-margin-tb&quot;&gt;\n																								&lt;i class=&quot;icon-shield icon-extra-large gray-text margin-seven no-margin-lr no-margin-top&quot;&gt;&lt;/i&gt;\n																								&lt;span class=&quot;text-medium font-weight-600 letter-spacing-2 text-uppercase gray-text margin-one no-margin-lr no-margin-top display-block alt-font&quot;&gt;GÜVENLİK&lt;/span&gt;\n																								&lt;p class=&quot;text-medium center-col&quot;&gt;\n						Sistem önceden belirlenmiş belirli periyodlarda yedeklenir, herhangi bir problemde tüm kayıtlar güvenlik altında tutulur.\n																								&lt;/p&gt;\n																							&lt;/div&gt;\n																							&lt;!-- end features box --&gt;\n																						&lt;/div&gt;\n																					&lt;/div&gt;\n																				&lt;/div&gt;\n																			&lt;/div&gt;\n																		&lt;/div&gt;\n																	&lt;/div&gt;\n																&lt;/div&gt;\n															&lt;/div&gt;\n														&lt;/div&gt;\n													&lt;/div&gt;\n												&lt;/div&gt;\n											&lt;/div&gt;\n										&lt;/div&gt;\n									&lt;/div&gt;\n								&lt;/div&gt;\n							&lt;/div&gt;\n						&lt;/div&gt;\n					&lt;/div&gt;\n				&lt;/div&gt;\n			&lt;/div&gt;\n		&lt;/div&gt;\n	&lt;/div&gt;\n&lt;/div&gt;\n&lt;/section&gt;', '', 0, '', 0, 0, NULL, '', '', 0, '2016-11-08 01:46:10', '1.00', 1),
(60, '', 'ÜRÜNLER', NULL, '&lt;div class=&quot;container&quot;&gt;\n	&lt;div class=&quot;row margin-fifteen no-margin-lr no-margin-bottom blog-post-style2&quot;&gt;\n		&lt;!-- special offer item --&gt;\n		&lt;article class=&quot;col-md-6 col-sm-6 col-xs-12 margin-four no-margin-lr no-margin-top&quot;&gt;\n		&lt;div class=&quot;col-md-12 col-sm-12 col-xs-12 no-padding bg-white&quot;&gt;\n			&lt;div class=&quot;col-md-12 col-sm-12 col-xs-12 no-padding post-thumbnail overflow-hidden&quot;&gt;\n				&lt;a href=&quot;../posmaksbulut&quot;&gt;&lt;img src=&quot;uploads/posmaks-bulut.png&quot; alt=&quot;&quot;&gt;&lt;/a&gt;\n			&lt;/div&gt;\n		&lt;/div&gt;\n		&lt;/article&gt;\n		&lt;!-- end special offer item --&gt;\n		&lt;!-- special offer item --&gt;\n		&lt;article class=&quot;col-md-6 col-sm-6 col-xs-12 margin-four no-margin-lr no-margin-top&quot;&gt;\n		&lt;div class=&quot;col-md-12 col-sm-12 col-xs-12 no-padding bg-white&quot;&gt;\n			&lt;div class=&quot;col-md-12 col-sm-12 col-xs-12 no-padding post-thumbnail overflow-hidden&quot;&gt;\n				&lt;a href=&quot;../posmaksyerel&quot;&gt;&lt;img src=&quot;uploads/posmaks-yerel.png&quot; alt=&quot;&quot;&gt;&lt;/a&gt;\n			&lt;/div&gt;\n		&lt;/div&gt;\n		&lt;/article&gt;\n		&lt;div&gt;\n			&lt;h5&gt;&lt;center&gt;\n			Yerel POS Sistemleri ve Bulut Tabanlı POS Sistemleri\n			&lt;/center&gt;&lt;/h5&gt;\n			&lt;p&gt;\n			&lt;/p&gt;\n			&lt;p&gt;\n				Bir POS cihazından beklediğiniz işlevler ve özellikler konusunda bilgi sahibi olmanız, satın alma noktasında\nsize büyük kolaylık sağlayacaktır. Bilindiği gibi piyasada farklı özellikler ve işlevler taşıyan onlarca POS sistemi mevcuttur. Bu noktada işletmenizin ihtiyaçları noktasında bir seçim yapmanız gerekir.\n			&lt;/p&gt;\n			&lt;p&gt;\nPOS sistemleri arasında ek hizmetler dışında temel iki fark mevcuttur. Bunlar Yerel POS Sistemleri ve Bulut tabanlı POS sitemleridir. Öncelikle kısaca bu sistemleri bir tanıyalım.\n			&lt;/p&gt;\n			&lt;br&gt;\n			&lt;b&gt;Yerel POS Sistemleri&lt;/b&gt;\n			Yerel Pos Sistemi, yerleşik tabanlı pos sistemidir. Tüm verileriniz kendinize ait yerel sunucuda saklanır. İnternet gerektirmez. İletişim access pointler üzerinden sağlanır.\n			&lt;p&gt;\n			&lt;/p&gt;\n			&lt;p&gt;\n				&lt;b&gt;Bulut Tabanlı POS Sistemleri&lt;/b&gt;\n				Posmaks Bulut Tabanlı Pos Sistemi, kurulum gerektirmez. Bulut tabanlı (Cloud based) özelliği sayesinde internet aracılığı ile dünyanın heryerinden ulaşabilme imkanı tanır. Verileriniz çevrimiçi ortamda yani bulutta depolanır. İşlemlerinizi herhangi bir cihaz üzerinden yönetilebilme imkanına sahip olursunuz.\n				&lt;br&gt;\n			&lt;/p&gt;\n&lt;table&gt;\n&lt;tbody&gt;\n&lt;tr&gt;\n&lt;td&gt;&lt;/td&gt;\n&lt;p&gt;\n&lt;td&gt;&lt;b&gt;Yerel POS Sistemleri&lt;/b&gt;&lt;/td&gt;\n&lt;/p&gt;\n&lt;p&gt;\n&lt;td&gt;&lt;b&gt;Bulut Tabanlı POS Sistemleri&lt;/b&gt;&lt;/td&gt;\n&lt;/p&gt;\n&lt;/tr&gt;\n&lt;tr&gt;\n&lt;td&gt;&lt;b&gt;Maliyet&lt;/b&gt;&lt;/td&gt;\n&lt;/p&gt;\n&lt;td&gt;&lt;span style=&quot;font-weight: 400;&quot;&gt;5,000 – 30,000 (Yıllık)&lt;/span&gt;&lt;/td&gt;\n&lt;td&gt;&lt;span style=&quot;font-weight: 400;&quot;&gt;600 – 10,000 (Yıllık)&lt;/span&gt;&lt;/td&gt;\n&lt;/tr&gt;\n&lt;tr&gt;\n&lt;td&gt;&lt;b&gt;Ödeme Şekli&lt;/b&gt;&lt;/td&gt;\n&lt;td&gt;&lt;span style=&quot;font-weight: 400;&quot;&gt;Lisans ücreti + Aylık bakım ücreti&lt;/span&gt;&lt;/td&gt;\n&lt;td&gt;&lt;span style=&quot;font-weight: 400;&quot;&gt;Aylık ödemeler&lt;/span&gt;&lt;/td&gt;\n&lt;/tr&gt;\n&lt;tr&gt;\n&lt;td&gt;&lt;b&gt;Yazılım&lt;/b&gt;&lt;/td&gt;\n&lt;td&gt;&lt;span style=&quot;font-weight: 400;&quot;&gt;Cihaza yüklü yazılım&lt;/span&gt;&lt;/td&gt;\n&lt;td&gt;&lt;span style=&quot;font-weight: 400;&quot;&gt;Çevrim içi saklama&lt;/span&gt;&lt;/td&gt;\n&lt;/tr&gt;\n&lt;tr&gt;\n&lt;td&gt;&lt;b&gt;Donanım&lt;/b&gt;&lt;/td&gt;\n&lt;td&gt;&lt;span style=&quot;font-weight: 400;&quot;&gt;Yerleşik terminaller&lt;/span&gt;&lt;/td&gt;\n&lt;td&gt;&lt;span style=&quot;font-weight: 400;&quot;&gt;Mobil cihazlar ve yerleşik tabletler&lt;/span&gt;&lt;/td&gt;\n&lt;/tr&gt;\n&lt;tr&gt;\n&lt;td&gt;&lt;b&gt;Veri Saklama&lt;/b&gt;&lt;/td&gt;\n&lt;td&gt;&lt;span style=&quot;font-weight: 400;&quot;&gt;Kurum içi sunucu&lt;/span&gt;&lt;/td&gt;\n&lt;td&gt;&lt;span style=&quot;font-weight: 400;&quot;&gt;Uzaktaki bulut sunucusu&lt;/span&gt;&lt;/td&gt;\n&lt;/tr&gt;\n&lt;tr&gt;\n&lt;td&gt;&lt;b&gt;Bakım – Onarım&lt;/b&gt;&lt;/td&gt;\n&lt;td&gt;&lt;span style=&quot;font-weight: 400;&quot;&gt;Yerinde ziyaret&lt;/span&gt;&lt;/td&gt;\n&lt;td&gt;&lt;span style=&quot;font-weight: 400;&quot;&gt;Uzaktan erişim&lt;/span&gt;&lt;/td&gt;\n&lt;/tr&gt;\n&lt;tr&gt;\n&lt;td&gt;&lt;b&gt;Kısıtlamalar&lt;/b&gt;&lt;/td&gt;\n&lt;td&gt;&lt;span style=&quot;font-weight: 400;&quot;&gt;Veri erişimi, sistem uyumluluğu; yerleşik donanım (maliyet, bakım-onarım)&lt;/span&gt;&lt;/td&gt;\n&lt;td&gt;&lt;span style=&quot;font-weight: 400;&quot;&gt;İnternet kaynaklı erişim problemleri&lt;/span&gt;&lt;/td&gt;\n&lt;/tr&gt;\n&lt;/tbody&gt;\n&lt;/table&gt;\n		&lt;/div&gt;\n	&lt;/div&gt;\n&lt;/div&gt;\n\n&lt;div style=&quot;text-align: center;&quot;&gt;\n	&lt;p&gt;\n		&lt;a href=&quot;../home/#modal-demo_request&quot; class=&quot;highlight-button btn btn-medium button text-center&quot;&gt;DEMO TALEBİ&lt;/a&gt;\n	&lt;/p&gt;\n&lt;/div&gt;', '', 0, '', 0, 0, NULL, '', '', 0, '2016-11-21 23:48:06', '1.00', 1),
(61, '', 'POSMAKS BULUT', NULL, '&lt;div style=&quot;visibility: visible; animation-name: fadeIn; background-color:white;&quot; id=&quot;about&quot; class=&quot;wow fadeIn animated&quot;&gt;\n	&lt;div class=&quot;container&quot;&gt;\n		&lt;div class=&quot;col-md-12 col-sm-12 col-xs-12 no-padding post-thumbnail overflow-hidden&quot;&gt;\n			&lt;a&gt;&lt;center&gt;&lt;img src=&quot;uploads/posmaksbulutsablon.png&quot; alt=&quot;&quot;&gt;&lt;/a&gt;&lt;/center&gt;\n		&lt;/div&gt;\n		&lt;h4 style=&quot;padding-top:20px;&quot;&gt;POSMAKS BULUT&lt;/h4&gt;\n		&lt;br&gt;\n		&lt;h8&gt;\n		Posmaks Bulut Tabanlı Pos Sistemi, kurulum gerektirmez. Bulut tabanlı (Cloud based) özelliği sayesinde internet aracılığı ile dünyanın heryerinden ulaşabilme imkanı tanır. Verileriniz çevrimiçi ortamda yani bulutta depolanır. İşlemlerinizi herhangi bir cihaz üzerinden yönetilebilme imkanına sahip olursunuz.\n		&lt;br&gt;\n		&lt;/h8&gt;\n		&lt;p&gt;\n		&lt;/p&gt;\n	&lt;/div&gt;\n&lt;/div&gt;', '', 0, '', 0, 0, NULL, 'posmaks,otomasyon,restoran otomasyonu,restoran,yemek,pos,ödeme sistemleri,ödeme', '', 0, '2017-04-27 14:14:06', '1.00', 1),
(62, '', 'POSMAKS YEREL', NULL, '&lt;div style=&quot;visibility: visible; animation-name: fadeIn; background-color:white;&quot; id=&quot;about&quot; class=&quot;wow fadeIn animated&quot;&gt;\n	&lt;div class=&quot;container&quot;&gt;\n		&lt;div class=&quot;col-md-12 col-sm-12 col-xs-12 no-padding post-thumbnail overflow-hidden&quot;&gt;\n			&lt;a&gt;&lt;center&gt;&lt;img src=&quot;uploads/posmaksyerelsablon.png&quot; alt=&quot;&quot;&gt;&lt;/a&gt;&lt;/center&gt;\n		&lt;/div&gt;\n		&lt;h4 style=&quot;padding-top:20px;&quot;&gt;POSMAKS YEREL&lt;/h4&gt;\n		&lt;br&gt;\n		&lt;h8&gt;\n		Posmaks Yerel Pos Sistemi, yerleşik tabanlı pos sistemidir. Tüm verileriniz kendinize ait yerel sunucuda saklanır. İnternet gerektirmez. İletişim access pointler üzerinden sağlanır.\n		&lt;br&gt;\n		&lt;/h8&gt;\n		&lt;p&gt;\n		&lt;/p&gt;\n	&lt;/div&gt;\n&lt;/div&gt;', '', 0, '', 0, 0, NULL, 'posmaks,otomasyon,restoran otomasyonu,restoran,yemek,pos,ödeme sistemleri,ödeme', '', 0, '2017-04-28 11:51:26', '1.00', 1);
INSERT INTO `plugins` (`id`, `title_en`, `title_tr`, `body_en`, `body_tr`, `jscode`, `show_title`, `alt_class`, `system`, `cplugin`, `info_en`, `info_tr`, `plugalias`, `hasconfig`, `created`, `ver`, `active`) VALUES
(59, '', 'POSMAKS ÖZET', NULL, '\n&lt;div class=&quot;container full-screen position-relative&quot; style=&quot;min-height: 595px;&quot;&gt;\n	&lt;div class=&quot;row&quot;&gt;\n		&lt;div class=&quot;col-md-12 col-sm-12&quot;&gt;\n			&lt;!-- tab --&gt;\n			&lt;div class=&quot;tab-style8&quot;&gt;\n				&lt;div class=&quot;row&quot;&gt;\n					&lt;div class=&quot;tabs-left col-md-12 col-sm-12 col-xs-12&quot;&gt;\n						&lt;div class=&quot;col-md-4 col-sm-4&quot;&gt;\n							&lt;div class=&quot;widget xs-margin-twenty xs-no-margin-lr xs-no-margin-top affix&quot; data-spy=&quot;affix&quot; data-offset-top=&quot;1200&quot;&gt;\n								&lt;!-- tab navigation --&gt;\n								&lt;ul class=&quot;nav nav-tabs nav-tabs-light pull-left text-uppercase xs-margin-five xs-no-margin-lr xs-no-margin-top&quot;&gt;\n									&lt;li class=&quot;xs-width-100 active&quot;&gt;&lt;a href=&quot;#tab4_sec1&quot; data-toggle=&quot;tab&quot; aria-expanded=&quot;true&quot; onclick=&quot;javascript:location.href=&#039;#ozet&#039;&quot;&gt;&lt;span aria-hidden=&quot;true&quot; class=&quot;icon-expand&quot; style=&quot;width:30px;&quot;&gt;&lt;/span&gt;NERELERDE KULLANILIR?&lt;/a&gt;&lt;/li&gt;\n									&lt;li class=&quot;xs-width-100&quot;&gt;&lt;a href=&quot;#tab4_sec2&quot; data-toggle=&quot;tab&quot; onclick=&quot;javascript:location.href=&#039;#ozet&#039;&quot;&gt;&lt;span aria-hidden=&quot;true&quot; class=&quot;icon-search&quot; style=&quot;width:30px;&quot;&gt;&lt;/span&gt;ÖZET BİLGİ&lt;/a&gt;&lt;/li&gt;\n									&lt;li class=&quot;xs-width-100&quot;&gt;&lt;a href=&quot;#tab4_sec3&quot; data-toggle=&quot;tab&quot; onclick=&quot;javascript:location.href=&#039;#ozet&#039;&quot;&gt;&lt;span aria-hidden=&quot;true&quot; class=&quot;icon-upload&quot; style=&quot;width:30px;&quot;&gt;&lt;/span&gt;POS SİPARİŞ YÖNETİMİ&lt;/a&gt;&lt;/li&gt;\n									&lt;li class=&quot;xs-width-100&quot;&gt;&lt;a href=&quot;#tab4_sec4&quot; data-toggle=&quot;tab&quot; onclick=&quot;javascript:location.href=&#039;#ozet&#039;&quot;&gt;&lt;span aria-hidden=&quot;true&quot; class=&quot;icon-gears&quot; style=&quot;width:30px;&quot;&gt;&lt;/span&gt;POS SATIŞ YÖNETİMİ&lt;/a&gt;&lt;/li&gt;\n									&lt;li class=&quot;xs-width-100&quot;&gt;&lt;a href=&quot;#tab4_sec5&quot; data-toggle=&quot;tab&quot; onclick=&quot;javascript:location.href=&#039;#ozet&#039;&quot;&gt;&lt;span aria-hidden=&quot;true&quot; class=&quot;icon-alarmclock&quot; style=&quot;width:30px;&quot;&gt;&lt;/span&gt;VARDİYA YÖNETİMİ&lt;/a&gt;&lt;/li&gt;\n									&lt;li class=&quot;xs-width-100&quot;&gt;&lt;a href=&quot;#tab4_sec6&quot; data-toggle=&quot;tab&quot; onclick=&quot;javascript:location.href=&#039;#ozet&#039;&quot;&gt;&lt;span aria-hidden=&quot;true&quot; class=&quot;icon-wine&quot; style=&quot;width:30px;&quot;&gt;&lt;/span&gt;ÜRÜN YÖNETİMİ&lt;/a&gt;&lt;/li&gt;\n									&lt;li class=&quot;xs-width-100&quot;&gt;&lt;a href=&quot;#tab4_sec7&quot; data-toggle=&quot;tab&quot; onclick=&quot;javascript:location.href=&#039;#ozet&#039;&quot;&gt;&lt;span aria-hidden=&quot;true&quot; class=&quot;icon-caution&quot; style=&quot;width:30px;&quot;&gt;&lt;/span&gt;STOK YÖNETİMİ&lt;/a&gt;&lt;/li&gt;\n									&lt;li class=&quot;xs-width-100&quot;&gt;&lt;a href=&quot;#tab4_sec8&quot; data-toggle=&quot;tab&quot; onclick=&quot;javascript:location.href=&#039;#ozet&#039;&quot;&gt;&lt;span aria-hidden=&quot;true&quot; class=&quot;icon-puzzle&quot; style=&quot;width:30px;&quot;&gt;&lt;/span&gt;ENVANTER YÖNETİMİ&lt;/a&gt;&lt;/li&gt;\n									&lt;li class=&quot;xs-width-100&quot;&gt;&lt;a href=&quot;#tab4_sec9&quot; data-toggle=&quot;tab&quot; onclick=&quot;javascript:location.href=&#039;#ozet&#039;&quot;&gt;&lt;span aria-hidden=&quot;true&quot; class=&quot;icon-basket&quot; style=&quot;width:30px;&quot;&gt;&lt;/span&gt;GİDER YÖNETİMİ&lt;/a&gt;&lt;/li&gt;\n									&lt;li class=&quot;xs-width-100&quot;&gt;&lt;a href=&quot;#tab4_sec10&quot; data-toggle=&quot;tab&quot; onclick=&quot;javascript:location.href=&#039;#ozet&#039;&quot;&gt;&lt;span aria-hidden=&quot;true&quot; class=&quot;icon-wallet&quot; style=&quot;width:30px;&quot;&gt;&lt;/span&gt;KASA YÖNETİMİ&lt;/a&gt;&lt;/li&gt;\n									&lt;li class=&quot;xs-width-100&quot;&gt;&lt;a href=&quot;#tab4_sec11&quot; data-toggle=&quot;tab&quot; onclick=&quot;javascript:location.href=&#039;#ozet&#039;&quot;&gt;&lt;span aria-hidden=&quot;true&quot; class=&quot;icon-lock&quot; style=&quot;width:30px;&quot;&gt;&lt;/span&gt;YETKİ YÖNETİMİ&lt;/a&gt;&lt;/li&gt;\n									&lt;li class=&quot;xs-width-100&quot;&gt;&lt;a href=&quot;#tab4_sec12&quot; data-toggle=&quot;tab&quot; onclick=&quot;javascript:location.href=&#039;#ozet&#039;&quot;&gt;&lt;span aria-hidden=&quot;true&quot; class=&quot;icon-piechart&quot; style=&quot;width:30px;&quot;&gt;&lt;/span&gt;RAPORLAR&lt;/a&gt;&lt;/li&gt;\n								&lt;/ul&gt;\n							&lt;/div&gt;\n						&lt;/div&gt;\n						&lt;!-- end tab navigation --&gt;\n						&lt;!-- tab content section --&gt;\n						&lt;div class=&quot;col-md-8 col-sm-8&quot;&gt;\n							&lt;div class=&quot;tab-content position-relative overflow-hidden&quot;&gt;\n								&lt;!-- tab content --&gt;\n								&lt;div class=&quot;tab-pane fade active in&quot; id=&quot;tab4_sec1&quot;&gt;\n									&lt;div class=&quot;row&quot;&gt;\n										&lt;img src=&quot;uploads/posmaks/gururla-posmaks.png&quot; alt=&quot;&quot; class=&quot;no-margin-lr sm-margin-eleven sm-no-margin-lr&quot;&gt;\n										&lt;div class=&quot;col-md-12 col-sm-12 alert-style1 text-center posmaks-title-ozel padding-two&quot;&gt;\n											&lt;h1&gt;Nerelerde Kullanılır?&lt;/h1&gt;\n										&lt;/div&gt;\n										&lt;img src=&quot;uploads/posmaks/nerelerde-kullanilir.png&quot; alt=&quot;&quot; class=&quot;no-margin-lr sm-margin-eleven sm-no-margin-lr&quot;&gt;\n										&lt;!-- end features box --&gt;\n										&lt;!-- end features box --&gt;\n									&lt;/div&gt;\n									&lt;p class=&quot;no-margin-bottom&quot;&gt;\n										PosMaks: Restaurant, Cafe - Bar, Fast Food, Pastaneler, Oteller ve çok noktalı zincir işletmeler başta olmak üzere nokta satışının gerçekleştirildiği sektörlerde kullanım için uygundur. \n										&lt;br&gt;\n										&lt;br&gt;\n										İşletmelere sağlamış olduğu operasyonel kolaylıklar ile izleme-takip ve raporlama özellikleri sayesinde, doğru yönetim ve doğru işletme anlaşında veriyi görsel bilgiye dönüştüren raporlar sunmaktadır.\n										&lt;br&gt;\n										&lt;br&gt;\n										Bu sayede tüm işletme ekibinin daha performanslı ve müşteri memnuniyeti odaklı çalışmalarını teşvik ederek, işletme yönetimine uçtan uca ölçme, değerlendirme ve izleme ortamı sunar.\n									&lt;/p&gt;\n									&lt;br&gt;\n									&lt;br&gt;\n								&lt;/div&gt;\n								&lt;!-- end tab content --&gt;\n								&lt;!-- tab content --&gt;\n								&lt;div class=&quot;tab-pane fade&quot; id=&quot;tab4_sec2&quot;&gt;\n									&lt;div class=&quot;col-md-12 col-sm-12 alert-style1 text-center posmaks-title-ozel&quot;&gt;\n										&lt;h1&gt;İşletmenize Kısaca Nasıl Fayda Sağlar?&lt;/h1&gt;\n									&lt;/div&gt;\n									&lt;!-- features box --&gt;\n									&lt;div class=&quot;col-md-12 col-sm-12 xs-margin-two xs-no-margin-top xs-no-margin-lr text-center&quot;&gt;\n										&lt;span class=&quot;text-uppercase text-large alt-font font-weight-600 deep-gray-text&quot;&gt;&lt;span class=&quot;crimson-red-text&quot;&gt;01.&lt;/span&gt;&amp;nbsp;&amp;nbsp;SİPARİŞ SÜRECİ&lt;/span&gt;\n										&lt;img src=&quot;uploads/posmaks/siparis-sureci.jpg&quot; alt=&quot;&quot; class=&quot;margin-two no-margin-lr sm-margin-eleven sm-no-margin-lr&quot;&gt;\n										&lt;p&gt;\n											İşletmenizde siparişin alınmasından hazırlanma bölümüne iletilmesi ve hazır olduğu bilgisinin siparişi alan personele bildirilmesine kadar tüm süreç kontrol altında.\n										&lt;/p&gt;\n									&lt;/div&gt;\n									&lt;!-- end features box --&gt;\n									&lt;!-- features box --&gt;\n									&lt;div class=&quot;col-md-12 col-sm-12 xs-margin-two xs-no-margin-top xs-no-margin-lr text-center&quot;&gt;\n										&lt;span class=&quot;text-uppercase text-large alt-font font-weight-600 deep-gray-text&quot;&gt;&lt;span class=&quot;crimson-red-text&quot;&gt;02.&lt;/span&gt;&amp;nbsp;&amp;nbsp;KASA İZLEME&lt;/span&gt;\n										&lt;img src=&quot;uploads/posmaks/kasa-takip.jpg&quot; alt=&quot;&quot; class=&quot;margin-two no-margin-lr sm-margin-eleven sm-no-margin-lr&quot;&gt;\n										&lt;p&gt;\n											İşletmenizdeki kasa nakit durumunu, pos cihazlarını ve yemek çeklerini anlık olarak internete bağlı olduğunuz heryerden takip edebilirsiniz.\n										&lt;/p&gt;\n									&lt;/div&gt;\n									&lt;!-- end features box --&gt;\n									&lt;div class=&quot;margin-four&quot;&gt;\n									&lt;/div&gt;\n									&lt;!-- features box --&gt;\n									&lt;div class=&quot;col-md-12 col-sm-12 xs-margin-two xs-no-margin-top xs-no-margin-lr text-center&quot;&gt;\n										&lt;span class=&quot;text-uppercase text-large alt-font font-weight-600 deep-gray-text&quot;&gt;&lt;span class=&quot;crimson-red-text&quot;&gt;03.&lt;/span&gt;&amp;nbsp;&amp;nbsp;STOK TAKİP&lt;/span&gt;\n										&lt;img src=&quot;uploads/posmaks/stok-takip.jpg&quot; alt=&quot;&quot; class=&quot;margin-two no-margin-lr sm-margin-eleven sm-no-margin-lr&quot;&gt;\n										&lt;p&gt;\n											PosMaks Akıllı Stok Yönetimi ile farklı birimlerde girişi yapılan stoklarınız ürün ile ilişkilendirilmiş stok miktarına göre hesap alma işlemi sonrası anlık olarak azalır ve stoklarınız belirlediğiniz miktarın altına düştüğünde sizi bilgilendirir.\n										&lt;/p&gt;\n									&lt;/div&gt;\n									&lt;!-- end features box --&gt;\n									&lt;div class=&quot;col-md-12 col-sm-12 xs-margin-two xs-no-margin-top xs-no-margin-lr text-center&quot;&gt;\n										&lt;span class=&quot;text-uppercase text-large alt-font font-weight-600 deep-gray-text&quot;&gt;&lt;span class=&quot;crimson-red-text&quot;&gt;04.&lt;/span&gt;&amp;nbsp;&amp;nbsp;RAPORLAMA&lt;/span&gt;\n										&lt;img src=&quot;uploads/posmaks/raporlama.jpg&quot; alt=&quot;&quot; class=&quot;margin-two no-margin-lr sm-margin-eleven sm-no-margin-lr&quot;&gt;\n										&lt;p&gt;\n											PosMaks zengin raporlama araçları ile işletmenizdeki tüm durumu anlık ve belirlediğiniz zaman aralıklarında kontrol edin.\n										&lt;/p&gt;\n									&lt;/div&gt;\n								&lt;/div&gt;\n								&lt;!-- end tab content --&gt;\n								&lt;!-- tab content --&gt;\n								&lt;div class=&quot;tab-pane fade in&quot; id=&quot;tab4_sec3&quot;&gt;\n									&lt;div class=&quot;col-md-12 col-sm-12 alert-style1 text-center posmaks-title-ozel padding-two&quot;&gt;\n										&lt;h1&gt;Pos Sipariş Yönetimi&lt;/h1&gt;\n									&lt;/div&gt;\n									&lt;img src=&quot;uploads/posmaks/siparis-yonetimi.png&quot; alt=&quot;&quot; class=&quot;margin-two no-margin-lr sm-margin-eleven sm-no-margin-lr&quot;&gt;\n								&lt;/div&gt;\n								&lt;!-- end tab content --&gt;\n								&lt;!-- tab content --&gt;\n								&lt;div class=&quot;tab-pane fade in&quot; id=&quot;tab4_sec4&quot;&gt;\n									&lt;div class=&quot;col-md-12 col-sm-12 alert-style1 text-center posmaks-title-ozel padding-two&quot;&gt;\n										&lt;h1&gt;Pos Satış Yönetimi&lt;/h1&gt;\n									&lt;/div&gt;\n									&lt;img src=&quot;uploads/posmaks/satis-yonetimi.png&quot; alt=&quot;&quot; class=&quot;margin-two no-margin-lr sm-margin-eleven sm-no-margin-lr&quot;&gt;\n								&lt;/div&gt;\n								&lt;!-- end tab content --&gt;\n								&lt;!-- tab content --&gt;\n								&lt;div class=&quot;tab-pane fade in&quot; id=&quot;tab4_sec5&quot;&gt;\n									&lt;div class=&quot;col-md-12 col-sm-12 alert-style1 text-center posmaks-title-ozel padding-two&quot;&gt;\n										&lt;h1&gt;Vardiya Yönetimi&lt;/h1&gt;\n									&lt;/div&gt;\n									&lt;img src=&quot;uploads/posmaks/vardiya-yonetimi.png&quot; alt=&quot;&quot; class=&quot;margin-two no-margin-lr sm-margin-eleven sm-no-margin-lr&quot;&gt;\n								&lt;/div&gt;\n								&lt;!-- end tab content --&gt;\n								&lt;!-- tab content --&gt;\n								&lt;div class=&quot;tab-pane fade in&quot; id=&quot;tab4_sec6&quot;&gt;\n									&lt;div class=&quot;col-md-12 col-sm-12 alert-style1 text-center posmaks-title-ozel padding-two&quot;&gt;\n										&lt;h1&gt;Ürün Yönetimi&lt;/h1&gt;\n									&lt;/div&gt;\n									&lt;img src=&quot;uploads/posmaks/urun-yonetimi.png&quot; alt=&quot;&quot; class=&quot;margin-two no-margin-lr sm-margin-eleven sm-no-margin-lr&quot;&gt;\n								&lt;/div&gt;\n								&lt;!-- end tab content --&gt;\n								&lt;!-- tab content --&gt;\n								&lt;div class=&quot;tab-pane fade in&quot; id=&quot;tab4_sec7&quot;&gt;\n									&lt;div class=&quot;col-md-12 col-sm-12 alert-style1 text-center posmaks-title-ozel padding-two&quot;&gt;\n										&lt;h1&gt;Stok Yönetimi&lt;/h1&gt;\n									&lt;/div&gt;\n									&lt;img src=&quot;uploads/posmaks/stok-yonetimi.png&quot; alt=&quot;&quot; class=&quot;margin-two no-margin-lr sm-margin-eleven sm-no-margin-lr&quot;&gt;\n								&lt;/div&gt;\n								&lt;!-- end tab content --&gt;\n								&lt;!-- tab content --&gt;\n								&lt;div class=&quot;tab-pane fade in&quot; id=&quot;tab4_sec8&quot;&gt;\n									&lt;div class=&quot;col-md-12 col-sm-12 alert-style1 text-center posmaks-title-ozel padding-two&quot;&gt;\n										&lt;h1&gt;Envanter Yönetimi&lt;/h1&gt;\n									&lt;/div&gt;\n									&lt;img src=&quot;uploads/posmaks/demirbas-takip.png&quot; alt=&quot;&quot; class=&quot;margin-two no-margin-lr sm-margin-eleven sm-no-margin-lr&quot;&gt;\n								&lt;/div&gt;\n								&lt;!-- end tab content --&gt;\n								&lt;!-- tab content --&gt;\n								&lt;div class=&quot;tab-pane fade in&quot; id=&quot;tab4_sec9&quot;&gt;\n									&lt;div class=&quot;col-md-12 col-sm-12 alert-style1 text-center posmaks-title-ozel padding-two&quot;&gt;\n										&lt;h1&gt;Gider Yönetimi&lt;/h1&gt;\n									&lt;/div&gt;\n									&lt;img src=&quot;uploads/posmaks/gider-yonetimi.png&quot; alt=&quot;&quot; class=&quot;margin-two no-margin-lr sm-margin-eleven sm-no-margin-lr&quot;&gt;\n								&lt;/div&gt;\n								&lt;!-- end tab content --&gt;\n								&lt;!-- tab content --&gt;\n								&lt;div class=&quot;tab-pane fade in&quot; id=&quot;tab4_sec10&quot;&gt;\n									&lt;div class=&quot;col-md-12 col-sm-12 alert-style1 text-center posmaks-title-ozel padding-two&quot;&gt;\n										&lt;h1&gt;Kasa Yönetimi&lt;/h1&gt;\n									&lt;/div&gt;\n									&lt;img src=&quot;uploads/posmaks/kasa-yonetimi.png&quot; alt=&quot;&quot; class=&quot;margin-two no-margin-lr sm-margin-eleven sm-no-margin-lr&quot;&gt;\n								&lt;/div&gt;\n								&lt;!-- end tab content --&gt;\n								&lt;!-- tab content --&gt;\n								&lt;div class=&quot;tab-pane fade in&quot; id=&quot;tab4_sec11&quot;&gt;\n									&lt;div class=&quot;col-md-12 col-sm-12 alert-style1 text-center posmaks-title-ozel padding-two&quot;&gt;\n										&lt;h1&gt;Yetki Yönetimi&lt;/h1&gt;\n									&lt;/div&gt;\n									&lt;img src=&quot;uploads/posmaks/yetki-yonetimi.png&quot; alt=&quot;&quot; class=&quot;margin-two no-margin-lr sm-margin-eleven sm-no-margin-lr&quot;&gt;\n								&lt;/div&gt;\n								&lt;!-- end tab content --&gt;\n								&lt;!-- tab content --&gt;\n								&lt;div class=&quot;tab-pane fade in&quot; id=&quot;tab4_sec12&quot;&gt;\n									&lt;div class=&quot;col-md-12 col-sm-12 alert-style1 text-center posmaks-title-ozel padding-two&quot;&gt;\n										&lt;h1&gt;Raporlar&lt;/h1&gt;\n									&lt;/div&gt;\n									&lt;img src=&quot;uploads/posmaks/raporlar.png&quot; alt=&quot;&quot; class=&quot;margin-two no-margin-lr sm-margin-eleven sm-no-margin-lr&quot;&gt;\n								&lt;/div&gt;\n								&lt;!-- end tab content --&gt;\n							&lt;/div&gt;\n							&lt;!-- end tab content section --&gt;\n						&lt;/div&gt;\n					&lt;/div&gt;\n				&lt;/div&gt;\n			&lt;/div&gt;\n			&lt;!-- end tab --&gt;\n		&lt;/div&gt;\n	&lt;/div&gt;\n	&lt;div class=&quot;slider-text-middle-main&quot;&gt;\n		&lt;div class=&quot;slider-text-middle slider-typography-option4&quot;&gt;\n		&lt;/div&gt;\n	&lt;/div&gt;\n&lt;/div&gt;\n&lt;/section&gt;', '', 0, '', 0, 0, NULL, '', '', 0, '2016-11-09 21:51:02', '1.00', 1);

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `plug_content_slider`
--

CREATE TABLE `plug_content_slider` (
  `id` int(4) NOT NULL,
  `title_en` varchar(100) DEFAULT NULL,
  `title_tr` varchar(150) NOT NULL,
  `body_en` text,
  `body_tr` text,
  `position` smallint(6) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Tablo döküm verisi `plug_content_slider`
--

INSERT INTO `plug_content_slider` (`id`, `title_en`, `title_tr`, `body_en`, `body_tr`, `position`) VALUES
(1, 'Just a basic content', 'Just a basic content', '&lt;h2&gt;Main Services&lt;/h2&gt;\n&lt;p&gt; Suspendisse potenti.  In elit augue, cursus sit amet dignissim id, mattis eu felis.  Suspendisse non leo purus. Etiam odio est, porta nec faucibus at.  Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere  cubilia Curae. &lt;/p&gt;\n&lt;div class=&quot;columns horizontal-gutters&quot;&gt;\n  &lt;div class=&quot;screen-50 tablet-50 phone-100&quot;&gt;\n    &lt;h4&gt; &lt;i class=&quot;icon globe&quot;&gt;&lt;/i&gt; Webdesign&lt;/h4&gt;\n    &lt;p&gt;Mauris rhoncus pretium porttitor. Cras consectetuer commodo odio. Phasellus dolor enim, faucibus egestas scelerisque.&lt;/p&gt;\n    &lt;h4&gt; &lt;i class=&quot;icon rocket&quot;&gt;&lt;/i&gt; Search Engine Marketing&lt;/h4&gt;\n    &lt;p&gt;Mauris rhoncus pretium porttitor. Cras consectetuer commodo odio. Phasellus dolor enim, faucibus egestas scelerisque.&lt;/p&gt;\n    &lt;h4&gt; &lt;i class=&quot;icon star&quot;&gt;&lt;/i&gt; Branding&lt;/h4&gt;\n    &lt;p&gt;Mauris rhoncus pretium porttitor. Cras consectetuer commodo odio. Phasellus dolor enim, faucibus egestas scelerisque.&lt;/p&gt;\n  &lt;/div&gt;\n  &lt;div class=&quot;screen-50 tablet-50 phone-100&quot;&gt;\n    &lt;h4&gt; &lt;i class=&quot;icon trophy&quot;&gt;&lt;/i&gt; Search Engine Optimisation&lt;/h4&gt;\n    &lt;p&gt;Mauris rhoncus pretium porttitor. Cras consectetuer commodo odio. Phasellus dolor enim, faucibus egestas scelerisque. &lt;/p&gt;\n    &lt;h4&gt; &lt;i class=&quot;icon code&quot;&gt;&lt;/i&gt; Web Development&lt;/h4&gt;\n    &lt;p&gt;Mauris rhoncus pretium porttitor. Cras consectetuer commodo odio. Phasellus dolor enim, faucibus egestas scelerisque.&lt;/p&gt;\n    &lt;h4&gt; &lt;i class=&quot;icon photo&quot;&gt;&lt;/i&gt; Photography&lt;/h4&gt;\n    &lt;p&gt;Mauris rhoncus pretium porttitor. Cras consectetuer commodo odio. Phasellus dolor enim, faucibus egestas scelerisque.&lt;/p&gt;\n  &lt;/div&gt;\n&lt;/div&gt;', '&lt;div class=&quot;widget-body&quot;&gt;&lt;section style=&quot;visibility: visible; animation-name: fadeIn;&quot; class=&quot;wow fadeIn bg-fast-yellow work-with-us animated animated&quot;&gt;\n   &lt;div class=&quot;container&quot;&gt;\n                &lt;div class=&quot;row blog-post-style7&quot;&gt;\n                    &lt;div class=&quot;margin-fifteen no-margin-lr no-margin-bottom clearfix&quot;&gt;\n                        &lt;!-- blog item --&gt;\n                        &lt;article class=&quot;col-md-4 col-sm-4 xs-margin-ten xs-no-margin-lr xs-no-margin-top&quot;&gt;\n                            &lt;div class=&quot;post-thumbnail overflow-hidden&quot;&gt;\n                             &lt;iframe width=&quot;100%&quot; height=&quot;250px&quot; src=&quot;https://www.youtube.com/embed/gaDE7H22kTo&quot; frameborder=&quot;0&quot; allowfullscreen&gt;&lt;/iframe&gt;\n                            &lt;/div&gt;\n                            &lt;div class=&quot;post-details margin-fifteen no-margin-lr no-margin-bottom&quot;&gt;\n                                &lt;span class=&quot;text-extra-small post-categories text-uppercase letter-spacing-1 alt-font bg-black display-inline-block&quot;&gt;&lt;a href=&quot;blog.html&quot; class=&quot;white-text&quot;&gt;The Couple&lt;/a&gt;&lt;/span&gt;\n                                &lt;span class=&quot;text-large text-uppercase display-block alt-font margin-seven no-margin-lr sm-text-medium xs-text-medium alt-font font-weight-600&quot;&gt;&lt;a href=&quot;blog-details.html&quot; class=&quot;dark-gray-text&quot;&gt;Weddings are important because they celebrate life and possibility.&lt;/a&gt;&lt;/span&gt;\n                                &lt;p&gt;Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#039;s standard dummy text.&lt;/p&gt;\n                                &lt;div class=&quot;separator-line-full bg-mid-gray3 margin-thirteen no-margin-lr&quot;&gt;&lt;/div&gt;\n                                &lt;span class=&quot;text-small text-uppercase alt-font light-gray-text&quot;&gt;&lt;a href=&quot;#&quot; class=&quot;light-gray-text&quot;&gt;26 July 2015&lt;/a&gt;   /   Posted By&lt;a href=&quot;blog.html&quot; class=&quot;light-gray-text&quot;&gt; Nathan Ford&lt;/a&gt;&lt;/span&gt;\n                            &lt;/div&gt;\n                        &lt;/article&gt;\n                        &lt;!-- end blog item --&gt;\n                        &lt;!-- blog item --&gt;\n                        &lt;article class=&quot;col-md-4 col-sm-4 xs-margin-ten xs-no-margin-lr xs-no-margin-top&quot;&gt;\n                            &lt;div class=&quot;post-thumbnail overflow-hidden&quot;&gt;\n                            &lt;iframe width=&quot;100%&quot; height=&quot;250px&quot; src=&quot;https://www.youtube.com/embed/gaDE7H22kTo&quot; frameborder=&quot;0&quot; allowfullscreen&gt;&lt;/iframe&gt;\n                            &lt;/div&gt;\n                            &lt;div class=&quot;post-details margin-fifteen no-margin-lr no-margin-bottom&quot;&gt;\n                                &lt;span class=&quot;text-extra-small post-categories text-uppercase letter-spacing-1 alt-font bg-black display-inline-block&quot;&gt;&lt;a href=&quot;blog.html&quot; class=&quot;white-text&quot;&gt;Wedding&lt;/a&gt;&lt;/span&gt;\n                                &lt;span class=&quot;text-large text-uppercase display-block alt-font margin-seven no-margin-lr sm-text-medium xs-text-medium alt-font font-weight-600&quot;&gt;&lt;a href=&quot;blog-details.html&quot; class=&quot;dark-gray-text&quot;&gt;A woman with a fine prospect of happiness behind her.&lt;/a&gt;&lt;/span&gt;\n                                &lt;p&gt;Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#039;s standard dummy text.&lt;/p&gt;\n                                &lt;div class=&quot;separator-line-full bg-mid-gray3 margin-thirteen no-margin-lr&quot;&gt;&lt;/div&gt;\n                                &lt;span class=&quot;text-small text-uppercase alt-font light-gray-text&quot;&gt;&lt;a href=&quot;#&quot; class=&quot;light-gray-text&quot;&gt;26 July 2015&lt;/a&gt;   /   Posted By&lt;a href=&quot;blog.html&quot; class=&quot;light-gray-text&quot;&gt; Nathan Ford&lt;/a&gt;&lt;/span&gt;\n                            &lt;/div&gt;\n                        &lt;/article&gt;\n                        &lt;!-- end blog item --&gt;\n                        &lt;!-- blog item --&gt;\n                        &lt;article class=&quot;col-md-4 col-sm-4&quot;&gt;\n                            &lt;div class=&quot;post-thumbnail overflow-hidden&quot;&gt;\n                              &lt;iframe width=&quot;100%&quot; height=&quot;250px&quot; src=&quot;https://www.youtube.com/embed/gaDE7H22kTo&quot; frameborder=&quot;0&quot; allowfullscreen&gt;&lt;/iframe&gt;\n                            &lt;/div&gt;\n                            &lt;div class=&quot;post-details margin-fifteen no-margin-lr no-margin-bottom&quot;&gt;\n                                &lt;span class=&quot;text-extra-small post-categories text-uppercase letter-spacing-1 alt-font bg-black display-inline-block&quot;&gt;&lt;a href=&quot;blog.html&quot; class=&quot;white-text&quot;&gt;Wedding&lt;/a&gt;&lt;/span&gt;\n                                &lt;span class=&quot;text-large text-uppercase display-block alt-font margin-seven no-margin-lr sm-text-medium xs-text-medium alt-font font-weight-600&quot;&gt;&lt;a href=&quot;blog-details.html&quot; class=&quot;dark-gray-text&quot;&gt;I was married by a judge. I should have asked for a jury.&lt;/a&gt;&lt;/span&gt;\n                                &lt;p&gt;Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#039;s standard dummy text.&lt;/p&gt;\n                                &lt;div class=&quot;separator-line-full bg-mid-gray3 margin-thirteen no-margin-lr&quot;&gt;&lt;/div&gt;\n                                &lt;span class=&quot;text-small text-uppercase alt-font light-gray-text&quot;&gt;&lt;a href=&quot;#&quot; class=&quot;light-gray-text&quot;&gt;26 July 2015&lt;/a&gt;   /   Posted By&lt;a href=&quot;blog.html&quot; class=&quot;light-gray-text&quot;&gt; Nathan Ford&lt;/a&gt;&lt;/span&gt;\n                            &lt;/div&gt;\n                        &lt;/article&gt;\n                        &lt;!-- end blog item --&gt;\n                    &lt;/div&gt;\n                &lt;/div&gt;\n            &lt;/div&gt;\n&lt;/section&gt;&lt;/div&gt;', 2),
(3, 'Content with linked button', 'Content with linked button', '\n    &lt;h3&gt;Skills and expertise&lt;/h3&gt;\n    &lt;h4 class=&quot;wojo info header&quot;&gt;Web Design &lt;span class=&quot;push-right&quot;&gt;92%&lt;/span&gt;&lt;/h4&gt;\n    &lt;div data-percent=&quot;92&quot; class=&quot;wojo thin striped info progress&quot;&gt;\n      &lt;div class=&quot;bar&quot;&gt; &lt;/div&gt;\n    &lt;/div&gt;\n    &lt;h4 class=&quot;wojo purple header&quot;&gt;jQuery &lt;span class=&quot;push-right&quot;&gt;74%&lt;/span&gt;&lt;/h4&gt;\n    &lt;div data-percent=&quot;74&quot; class=&quot;wojo thin striped purple progress&quot;&gt;\n      &lt;div class=&quot;bar&quot;&gt; &lt;/div&gt;\n    &lt;/div&gt;\n    &lt;h4 class=&quot;wojo warning header&quot;&gt;PHP&lt;span class=&quot;push-right&quot;&gt;85%&lt;/span&gt;&lt;/h4&gt;\n    &lt;div data-percent=&quot;85&quot; class=&quot;wojo thin striped warning progress&quot;&gt;\n      &lt;div class=&quot;bar&quot;&gt; &lt;/div&gt;\n    &lt;/div&gt;\n    &lt;h4 class=&quot;wojo success header&quot;&gt;SEO Optimisation &lt;span class=&quot;push-right&quot;&gt;75%&lt;/span&gt;&lt;/h4&gt;\n    &lt;div data-percent=&quot;75&quot; class=&quot;wojo thin striped success progress&quot;&gt;\n      &lt;div class=&quot;bar&quot;&gt; &lt;/div&gt;\n    &lt;/div&gt;\n    &lt;h4 class=&quot;wojo negative header&quot;&gt;Marketing &amp; PR &lt;span class=&quot;push-right&quot;&gt;63%&lt;/span&gt;&lt;/h4&gt;\n    &lt;div data-percent=&quot;63&quot; class=&quot;wojo thin striped negative progress&quot;&gt;\n      &lt;div class=&quot;bar&quot;&gt; &lt;/div&gt;\n    &lt;/div&gt;\n&lt;p&gt;\n	&lt;a class=&quot;wojo info button&quot;&gt;Get in Touch&lt;/a&gt;\n&lt;/p&gt;\n', '&lt;div class=&quot;row margin-nine no-margin-bottom no-margin-top no-margin-lr&quot;&gt;\n      &lt;div class=&quot;col-md-12 col-sm-12 text-center&quot;&gt;\n         &lt;span class=&quot;title-medium deep-green-text text-uppercase alt-font title-dividers font-weight-600 position-relative&quot;&gt;Çözümlerimiz&lt;/span&gt;\n      &lt;/div&gt;\n    &lt;/div&gt;\n\n	\n\n\n\n		&lt;div class=&quot;row blog-post-style7&quot;&gt;\n			&lt;div class=&quot;margin-fifteen no-margin-lr no-margin-bottom clearfix&quot;&gt;\n\n				&lt;article class=&quot;col-md-4 col-sm-4 xs-margin-ten xs-no-margin-lr xs-no-margin-top&quot;&gt;\n				&lt;div class=&quot;post-thumbnail overflow-hidden&quot;&gt;\n					&lt;iframe width=&quot;100%&quot; height=&quot;250px&quot; src=&quot;https://www.youtube.com/embed/gaDE7H22kTo&quot; frameborder=&quot;0&quot; allowfullscreen=&quot;&quot;&gt;\n					&lt;/iframe&gt;\n				&lt;/div&gt;\n				\n				&lt;/article&gt;\n				&lt;!-- end blog item --&gt;\n				&lt;!-- blog item --&gt;\n				&lt;article class=&quot;col-md-4 col-sm-4 xs-margin-ten xs-no-margin-lr xs-no-margin-top&quot;&gt;\n				&lt;div class=&quot;post-thumbnail overflow-hidden&quot;&gt;\n					&lt;iframe width=&quot;100%&quot; height=&quot;250px&quot; src=&quot;https://www.youtube.com/embed/gaDE7H22kTo&quot; frameborder=&quot;0&quot; allowfullscreen=&quot;&quot;&gt;\n					&lt;/iframe&gt;\n				&lt;/div&gt;\n				\n				&lt;/article&gt;\n				&lt;!-- end blog item --&gt;\n				&lt;!-- blog item --&gt;\n				&lt;article class=&quot;col-md-4 col-sm-4&quot;&gt;\n				&lt;div class=&quot;post-thumbnail overflow-hidden&quot;&gt;\n					&lt;iframe width=&quot;100%&quot; height=&quot;250px&quot; src=&quot;https://www.youtube.com/embed/gaDE7H22kTo&quot; frameborder=&quot;0&quot; allowfullscreen=&quot;&quot;&gt;\n					&lt;/iframe&gt;\n				&lt;/div&gt;\n				\n				&lt;/article&gt;\n				&lt;!-- end blog item --&gt;\n			&lt;/div&gt;\n		&lt;/div&gt;\n', 1);

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `plug_donate`
--

CREATE TABLE `plug_donate` (
  `id` int(11) NOT NULL,
  `name` varchar(100) DEFAULT NULL,
  `email` varchar(60) DEFAULT NULL,
  `amount` decimal(10,2) NOT NULL DEFAULT '0.00',
  `created` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Tablo döküm verisi `plug_donate`
--

INSERT INTO `plug_donate` (`id`, `name`, `email`, `amount`, `created`) VALUES
(1, 'Web Master', 'webmaster@domain.com', '125.00', '2012-06-28 10:41:55'),
(2, 'Web Master', 'webmaster@domain.com', '15.00', '2012-06-28 10:53:56');

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `plug_elastic`
--

CREATE TABLE `plug_elastic` (
  `id` int(6) NOT NULL,
  `title_en` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `title_tr` varchar(150) NOT NULL,
  `body_en` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `body_tr` text,
  `thumb` varchar(150) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `position` smallint(6) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='contains slider data';

--
-- Tablo döküm verisi `plug_elastic`
--

INSERT INTO `plug_elastic` (`id`, `title_en`, `title_tr`, `body_en`, `body_tr`, `thumb`, `position`) VALUES
(1, 'Piko', 'Piko', 'Verbatium', 'Verbatium', 'FILE_E7AEC0-C0241E-1D9D71-050CE5-BEAD8F-EFAD10.jpg', 1),
(2, 'Simil', 'Simil', 'Homonimo', 'Homonimo', 'FILE_8DD2CF-0FED2E-266BB1-79A361-3E5A6F-BF40B8.jpg', 3),
(3, 'Kioma', 'Kioma', 'Volitivo', 'Volitivo', 'FILE_AEDA15-431B10-DAC55E-E6A729-6BEA65-422DFB.jpg', 4),
(4, 'Memmortigo', 'Memmortigo', 'Multiplikite', 'Multiplikite', 'FILE_D767B5-F6A577-C95480-6CDA88-53EEA8-A3DEB8.jpg', 2),
(5, 'Eligi', 'Eligi', 'Verbatium', 'Verbatium', 'FILE_E88FC5-A51E90-97FF86-6BD41C-BAE1C0-63B13A.jpg', 5);

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `plug_newsslider`
--

CREATE TABLE `plug_newsslider` (
  `id` int(11) NOT NULL,
  `title_en` varchar(150) DEFAULT NULL,
  `title_tr` varchar(150) NOT NULL,
  `body_en` text,
  `body_tr` text,
  `show_title` tinyint(1) NOT NULL DEFAULT '0',
  `created` datetime DEFAULT NULL,
  `show_created` tinyint(1) NOT NULL DEFAULT '0',
  `position` int(11) NOT NULL DEFAULT '0',
  `active` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Tablo döküm verisi `plug_newsslider`
--

INSERT INTO `plug_newsslider` (`id`, `title_en`, `title_tr`, `body_en`, `body_tr`, `show_title`, `created`, `show_created`, `position`, `active`) VALUES
(1, 'Etiam non lacus', 'Etiam non lacus', 'Morbi sodales accumsan arcu sed venenatis. Vivamus leo diam, dignissim  eu convallis in, posuere quis magna. Curabitur mollis, lectus sit amet  bibendum faucibus, nisi ligula ultricies purus', 'Morbi sodales accumsan arcu sed venenatis. Vivamus leo diam, dignissim  eu convallis in, posuere quis magna. Curabitur mollis, lectus sit amet  bibendum faucibus, nisi ligula ultricies purus', 1, '2010-10-28 04:14:11', 1, 1, 1),
(2, 'Cras ullamcorper', 'Cras ullamcorper', '&lt;p&gt;Etiam non lacus ac velit lobortis rutrum sed id turpis. Ut dictum, eros  eu blandit pellentesque, nisi nisl dapibus mauris, sed feugiat enim urna  sit amet nibh. Suspendisse sed tortor nisi. Nulla facilisi. In sed  risus in est cursus ornare.&lt;/p&gt;', '&lt;p&gt;Etiam non lacus ac velit lobortis rutrum sed id turpis. Ut dictum, eros  eu blandit pellentesque, nisi nisl dapibus mauris, sed feugiat enim urna  sit amet nibh. Suspendisse sed tortor nisi. Nulla facilisi. In sed  risus in est cursus ornare.&lt;/p&gt;', 1, '2010-10-28 04:14:33', 1, 2, 1),
(3, 'Vivamus vitae', 'Vivamus vitae', 'Lusce pulvinar velit sit amet ligula ornare tempus vulputate ipsum  semper. Praesent non lorem odio. Fusce sed dui massa, eu viverra erat.  Proin posuere nulla in lectus malesuada volutpat. Cras tristique blandit  tellus, eu consequat ante', 'Lusce pulvinar velit sit amet ligula ornare tempus vulputate ipsum  semper. Praesent non lorem odio. Fusce sed dui massa, eu viverra erat.  Proin posuere nulla in lectus malesuada volutpat. Cras tristique blandit  tellus, eu consequat ante', 1, '2010-10-28 04:21:34', 1, 3, 1),
(4, 'Another News', 'Another News', 'Vivamus vitae augue sed lacus placerat sollicitudin quis vel arcu. Vestibulum auctor, magna sit amet pulvinar tristique, nunc felis viverra tortor, venenatis convallis leo mauris eu massa. Intege', 'Vivamus vitae augue sed lacus placerat sollicitudin quis vel arcu. Vestibulum auctor, magna sit amet pulvinar tristique, nunc felis viverra tortor, venenatis convallis leo mauris eu massa. Intege', 1, '2010-10-28 04:43:36', 1, 4, 1);

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `plug_news_tags`
--

CREATE TABLE `plug_news_tags` (
  `id` int(11) NOT NULL,
  `tagname_en` varchar(60) NOT NULL,
  `tagname_tr` varchar(100) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Tablo döküm verisi `plug_news_tags`
--

INSERT INTO `plug_news_tags` (`id`, `tagname_en`, `tagname_tr`) VALUES
(1, '', 'etiket1'),
(2, '', 'ototmasyon'),
(3, '', 'otomasyon nedir'),
(4, '', 'otomasyon avantajları'),
(5, '', 'posmaks'),
(6, '', 'kodar'),
(7, '', 'türkiye de restarutant sektörünün gelişimi'),
(8, '', 'restaurant sektörü'),
(9, '', 'restaurantlar'),
(10, '', 'bulut tabanlı pos sistemi'),
(11, '', 'pos sistemi'),
(12, '', 'dünyada restaurant otomasyonu'),
(13, '', 'restaurant otomasyonu'),
(14, '', 'akıllı pos ve yazar kasalar'),
(15, '', 'pos ve yazarkasalar');

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `plug_online`
--

CREATE TABLE `plug_online` (
  `id` int(11) NOT NULL,
  `ip` int(11) NOT NULL DEFAULT '0',
  `country` varchar(64) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `countrycode` varchar(2) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `city` varchar(64) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `plug_poll_options`
--

CREATE TABLE `plug_poll_options` (
  `id` int(11) NOT NULL,
  `question_id` int(11) NOT NULL,
  `value_en` varchar(250) NOT NULL,
  `value_tr` varchar(250) NOT NULL,
  `position` tinyint(2) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Tablo döküm verisi `plug_poll_options`
--

INSERT INTO `plug_poll_options` (`id`, `question_id`, `value_en`, `value_tr`, `position`) VALUES
(5, 1, 'Very Hard', 'Very Hard 4', 5),
(4, 1, 'Hard', 'Hard', 4),
(3, 1, 'Easy', 'Easy', 3),
(2, 1, 'Very Easy', 'Very Easy', 2),
(1, 1, 'Piece of cake', 'Piece of cake', 1),
(6, 2, '', '1', 1),
(7, 2, '', '2', 2),
(8, 2, '', '3', 3),
(9, 2, '', '4', 4);

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `plug_poll_questions`
--

CREATE TABLE `plug_poll_questions` (
  `id` int(11) NOT NULL,
  `question_en` varchar(250) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `question_tr` varchar(250) NOT NULL,
  `created` datetime NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Tablo döküm verisi `plug_poll_questions`
--

INSERT INTO `plug_poll_questions` (`id`, `question_en`, `question_tr`, `created`, `status`) VALUES
(1, 'How do you find CMS pro! Installation?', 'How do you find CMS pro! Installation?', '2010-10-13 07:42:18', 0),
(2, '', 'test', '2015-12-08 17:17:23', 1);

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `plug_poll_votes`
--

CREATE TABLE `plug_poll_votes` (
  `id` int(11) NOT NULL,
  `option_id` int(11) NOT NULL,
  `voted_on` datetime NOT NULL,
  `ip` varchar(16) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Tablo döküm verisi `plug_poll_votes`
--

INSERT INTO `plug_poll_votes` (`id`, `option_id`, `voted_on`, `ip`) VALUES
(1, 2, '2010-10-14 14:00:55', '127.0.0.1'),
(2, 1, '2010-10-14 14:01:27', '127.0.0.1'),
(3, 1, '2010-10-14 14:02:04', '127.0.0.1'),
(4, 1, '2010-10-14 14:02:13', '127.0.0.1'),
(5, 3, '2010-10-14 14:02:16', '127.0.0.1'),
(6, 4, '2010-10-14 14:02:21', '127.0.0.1'),
(7, 3, '2010-10-14 14:02:24', '127.0.0.1'),
(8, 1, '2010-10-14 14:02:27', '127.0.0.1'),
(9, 2, '2010-10-14 14:02:31', '127.0.0.1'),
(10, 5, '2010-10-14 14:02:35', '127.0.0.1'),
(11, 1, '2010-10-14 14:02:38', '127.0.0.1'),
(12, 2, '2010-10-14 14:02:43', '127.0.0.1'),
(13, 1, '2010-10-14 14:02:46', '127.0.0.1'),
(14, 1, '2010-10-14 14:02:50', '127.0.0.1'),
(15, 1, '2010-10-14 14:05:26', '127.0.0.1'),
(16, 1, '2010-10-14 14:05:29', '127.0.0.1'),
(17, 4, '2010-10-14 14:05:33', '127.0.0.1'),
(18, 2, '2010-10-14 14:05:36', '127.0.0.1'),
(19, 1, '2010-10-14 14:05:40', '127.0.0.1'),
(20, 3, '2010-10-14 14:05:46', '127.0.0.1'),
(21, 2, '2010-10-14 14:05:49', '127.0.0.1'),
(22, 2, '2010-10-14 14:21:37', '127.0.0.1'),
(23, 1, '2010-10-14 14:21:53', '127.0.0.1'),
(24, 5, '2010-10-14 14:21:59', '127.0.0.1'),
(25, 1, '2010-10-14 14:35:27', '127.0.0.1'),
(26, 1, '2010-10-15 00:42:05', '127.0.0.1'),
(27, 3, '2010-10-15 00:49:42', '127.0.0.1'),
(28, 2, '2010-10-15 01:22:00', '127.0.0.1'),
(29, 2, '2010-10-15 01:24:51', '127.0.0.1'),
(30, 1, '2010-10-15 01:37:21', '127.0.0.1'),
(31, 1, '2010-10-15 01:38:48', '127.0.0.1'),
(32, 1, '2010-10-15 01:41:30', '127.0.0.1'),
(33, 1, '2010-10-15 01:42:21', '127.0.0.1'),
(34, 1, '2010-10-15 04:53:42', '127.0.0.1'),
(35, 3, '2010-10-15 05:09:14', '127.0.0.1'),
(36, 3, '2010-11-24 21:00:27', '127.0.0.1'),
(37, 3, '2010-11-28 00:56:07', '127.0.0.1'),
(38, 3, '2012-12-22 21:57:05', '127.0.0.1'),
(39, 1, '2012-12-22 22:46:26', '127.0.0.1'),
(40, 5, '2012-12-24 15:20:53', '127.0.0.1'),
(41, 1, '2012-12-26 20:20:01', '127.0.0.1');

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `plug_slider`
--

CREATE TABLE `plug_slider` (
  `id` int(11) NOT NULL,
  `title_en` varchar(150) NOT NULL,
  `title_tr` varchar(150) NOT NULL,
  `body_en` tinytext,
  `body_tr` text,
  `thumb` varchar(150) NOT NULL,
  `url` varchar(150) NOT NULL,
  `page_id` smallint(6) DEFAULT '0',
  `urltype` enum('int','ext','nourl') DEFAULT 'nourl',
  `position` smallint(6) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Tablo döküm verisi `plug_slider`
--

INSERT INTO `plug_slider` (`id`, `title_en`, `title_tr`, `body_en`, `body_tr`, `thumb`, `url`, `page_id`, `urltype`, `position`) VALUES
(1, 'Via o basate nomina proposito', 'Via o basate nomina proposito', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna', 'FILE_482821-DD455E-AEBC7B-39F656-8D1C99-EE88B2.jpg', '#', 0, 'ext', 1),
(2, 'Infra latino appellate le sia', 'Infra latino appellate le sia', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna', 'FILE_A3C78C-341143-035C85-3731CA-EE9BBA-ED6D4D.jpg', 'three-columns', 6, 'int', 3),
(3, 'Il via unic populos', 'Il via unic populos', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna', 'FILE_D8D942-5C8092-D5757A-B57AEB-EDED1D-D5E165.jpg', '#', 0, 'ext', 2),
(4, 'In anque svedese abstracte del', 'In anque svedese abstracte del', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna', 'FILE_DE99C5-257B9B-E1580E-8D3070-70ABC3-732634.jpg', '#', 0, 'ext', 4);

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `plug_sliderbrando`
--

CREATE TABLE `plug_sliderbrando` (
  `id` int(11) NOT NULL,
  `title_en` varchar(150) NOT NULL,
  `title_tr` varchar(150) NOT NULL,
  `body_en` tinytext,
  `body_tr` text,
  `thumb` varchar(150) NOT NULL,
  `thumb_1` varchar(150) DEFAULT NULL,
  `url` varchar(150) NOT NULL,
  `page_id` smallint(6) DEFAULT '0',
  `urltype` enum('int','ext','nourl') DEFAULT 'nourl',
  `position` smallint(6) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Tablo döküm verisi `plug_sliderbrando`
--

INSERT INTO `plug_sliderbrando` (`id`, `title_en`, `title_tr`, `body_en`, `body_tr`, `thumb`, `thumb_1`, `url`, `page_id`, `urltype`, `position`) VALUES
(1, 'Via o basate nomina proposito', 'SLAYT 1', '', '&lt;link href=&quot;https://fonts.googleapis.com/css?family=Lato&quot; rel=&quot;stylesheet&quot;&gt;\r\n&lt;div class=&quot;col-md-12 col-sm-12 &quot;&gt;\r\n&lt;div class=&quot;col-md-6 col-sm-12 &quot;&gt;\r\n &lt;/div&gt;\r\n&lt;div class=&quot;col-md-6 col-sm-12 &quot;&gt;\r\n&lt;span class=&quot;title-large alt-font text-uppercase letter-spacing-2 white-text&quot;&gt;\r\nPosMaks Satış Noktası Çözümleri\r\n&lt;/span&gt;\r\n&lt;br&gt;&lt;br&gt;\r\n&lt;span class=&quot;title-large-alt-font letter-spacing-2 white-text&quot;&gt;\r\nPosMaks yazılım paketi, her satış noktasının ihtiyaçlarını karşılayan çözümler sunar. Tüm işletmelere uygun, kullanımı kolay ve zengin özelliklere sahiptir. Posmaks ile operasyonlarınızı düzene sokmak için sorunsuz bir sisteme sahip olun.\r\n&lt;/span&gt;\r\n&lt;/div&gt;\r\n&lt;/div&gt;\r\n&lt;div style=&quot;position:absolute; bottom: 20%&quot; class=&quot;col-md-12 col-sm-12 center-col text-center&quot;&gt;\r\n\r\n&lt;a class=&quot;btn-small-white-background btn btn-large btn-round button inner-link popup-youtube&quot; \r\nhref=&quot;https://www.youtube.com/watch?v=0Ovnw2r7ElA&quot;&gt;&lt;i class=&quot;fa fa-fw&quot;&gt;&lt;/i&gt;İZLE&lt;/a&gt;\r\n\r\n\r\n\r\n&lt;a href=&quot;#modal-demo_request&quot; data-toggle=&quot;modal&quot; class=&quot;btn-small-white-background btn btn-large btn-round button&quot;&gt;DEMO TALEBİ&lt;/a&gt;\r\n                                    &lt;/div&gt;', 'FILE_D76285-1F725F-2D5C8B-92286A-8A4373-61302E.jpg', 'FILE_B428A3-60993B-25C039-744FBD-A529A7-E95BEA.jpg', '#', 0, 'nourl', 1),
(6, '', 'KAMPANYA', NULL, '&lt;div class=&quot;col-md-12 col-sm-12 &quot;&gt;\n&lt;div class=&quot;col-md-6 col-sm-12 &quot;&gt;\n &lt;/div&gt;\n&lt;div class=&quot;col-md-6 col-sm-12 &quot;&gt;\n&lt;img src=&quot;../../../uploads/slides/posmaks-cloud-slide1-content.png&quot; alt=&quot;&quot;&gt;\n&lt;div style=&quot;text-align:center&quot;&gt;\n&lt;a class=&quot;item&quot; href=&quot;#modal-demo_request&quot; data-toggle=&quot;modal&quot;&gt;&lt;li class=&quot;satin-al btn btn-medium button letter-spacing-1 inner-link&quot; style=&quot;height: 40px;&quot;&gt;ÜCRETSİZ DENEYİN, MEMNUN KALIRSANIZ PAKETİNİZİ SEÇİN&lt;/li&gt;&lt;/a&gt;\n&lt;/div&gt;\n\n&lt;/div&gt;\n&lt;/div&gt;', 'FILE_8807DF-3B1BEF-A33886-17D0FB-EB0245-7F8474.jpg', 'FILE_592249-F08AF6-018577-CE751E-7BC334-A4D32A.jpg', '#', 0, 'nourl', 0);

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `plug_tabs`
--

CREATE TABLE `plug_tabs` (
  `id` int(6) NOT NULL,
  `title_en` varchar(50) NOT NULL DEFAULT '',
  `title_tr` varchar(50) NOT NULL,
  `body_en` text,
  `body_tr` text,
  `position` tinyint(1) NOT NULL DEFAULT '0',
  `active` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Tablo döküm verisi `plug_tabs`
--

INSERT INTO `plug_tabs` (`id`, `title_en`, `title_tr`, `body_en`, `body_tr`, `position`, `active`) VALUES
(1, 'Website Design', 'Website Design', '&lt;h2&gt;Website Design&lt;/h2&gt;&lt;p&gt;\n	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis facilisis dapibus tincidunt. Aliquam non mauris ac urna pretium malesuada. Mauris viverra fringilla lectus, nec congue neque adipiscing ultrices.&lt;/p&gt;&lt;p&gt;\n	Nulla vel magna in leo mattis congue in eget quam. Proin dignissim nunc vitae nunc euismod sollicitudin. Nullam pretium placerat eleifend. Aliquam erat volutpat. Nunc et massa nisl, lacinia pharetra eros. In sit amet augue a ante tincidunt viverra.&lt;/p&gt;', '&lt;h2&gt;Website Design&lt;/h2&gt;&lt;p&gt;\n	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis facilisis dapibus tincidunt. Aliquam non mauris ac urna pretium malesuada. Mauris viverra fringilla lectus, nec congue neque adipiscing ultrices.&lt;/p&gt;&lt;p&gt;\n	Nulla vel magna in leo mattis congue in eget quam. Proin dignissim nunc vitae nunc euismod sollicitudin. Nullam pretium placerat eleifend. Aliquam erat volutpat. Nunc et massa nisl, lacinia pharetra eros. In sit amet augue a ante tincidunt viverra.&lt;/p&gt;', 1, 1),
(2, 'Content Management', 'Content Management', '&lt;h2&gt;Content Management&lt;/h2&gt;\n&lt;p&gt;\n	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis facilisis dapibus tincidunt. Aliquam non mauris ac urna pretium malesuada. Mauris viverra fringilla lectus, nec congue neque adipiscing ultrices.\n&lt;/p&gt;\n&lt;p&gt;\n	Nulla vel magna in leo mattis congue in eget quam. Proin dignissim nunc vitae nunc euismod sollicitudin. Nullam pretium placerat eleifend. Aliquam erat volutpat. Nunc et massa nisl, lacinia pharetra eros. In sit amet augue a ante tincidunt viverra.\n&lt;/p&gt;', '&lt;h2&gt;Content Management&lt;/h2&gt;\n&lt;p&gt;\n	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis facilisis dapibus tincidunt. Aliquam non mauris ac urna pretium malesuada. Mauris viverra fringilla lectus, nec congue neque adipiscing ultrices.\n&lt;/p&gt;\n&lt;p&gt;\n	Nulla vel magna in leo mattis congue in eget quam. Proin dignissim nunc vitae nunc euismod sollicitudin. Nullam pretium placerat eleifend. Aliquam erat volutpat. Nunc et massa nisl, lacinia pharetra eros. In sit amet augue a ante tincidunt viverra.\n&lt;/p&gt;', 2, 1),
(3, 'E-Commerce', 'E-Commerce', '&lt;h2&gt;E-Commerce&lt;/h2&gt;\n&lt;p&gt;\n	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis  facilisis dapibus tincidunt. Aliquam non mauris ac urna pretium  malesuada. Mauris viverra fringilla lectus, nec congue neque adipiscing  ultrices.\n&lt;/p&gt;\n&lt;p&gt;\n	Nulla vel magna in leo mattis congue in eget quam. Proin  dignissim nunc vitae nunc euismod sollicitudin. Nullam pretium placerat  eleifend. Aliquam erat volutpat. Nunc et massa nisl, lacinia pharetra  eros. In sit amet augue a ante tincidunt viverra.\n&lt;/p&gt;', '&lt;h2&gt;E-Commerce&lt;/h2&gt;\n&lt;p&gt;\n	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis  facilisis dapibus tincidunt. Aliquam non mauris ac urna pretium  malesuada. Mauris viverra fringilla lectus, nec congue neque adipiscing  ultrices.\n&lt;/p&gt;\n&lt;p&gt;\n	Nulla vel magna in leo mattis congue in eget quam. Proin  dignissim nunc vitae nunc euismod sollicitudin. Nullam pretium placerat  eleifend. Aliquam erat volutpat. Nunc et massa nisl, lacinia pharetra  eros. In sit amet augue a ante tincidunt viverra.\n&lt;/p&gt;', 4, 1),
(4, 'Search Engines', 'Search Engines', '&lt;h2&gt;Search Engines&lt;/h2&gt;\n&lt;p&gt;\n	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis  facilisis dapibus tincidunt. Aliquam non mauris ac urna pretium  malesuada. Mauris viverra fringilla lectus, nec congue neque adipiscing  ultrices.\n&lt;/p&gt;\n&lt;p&gt;\n	Nulla vel magna in leo mattis congue in eget quam. Proin  dignissim nunc vitae nunc euismod sollicitudin. Nullam pretium placerat  eleifend. Aliquam erat volutpat. Nunc et massa nisl, lacinia pharetra  eros. In sit amet augue a ante tincidunt viverra.\n&lt;/p&gt;\n&lt;br&gt;\n&lt;p&gt;\n	&lt;a href=&quot;#&quot; class=&quot;wojo positive button&quot;&gt;Read More&lt;/a&gt;\n&lt;/p&gt;\n', '&lt;h2&gt;Search Engines&lt;/h2&gt;\n&lt;p&gt;\n	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis  facilisis dapibus tincidunt. Aliquam non mauris ac urna pretium  malesuada. Mauris viverra fringilla lectus, nec congue neque adipiscing  ultrices.\n&lt;/p&gt;\n&lt;p&gt;\n	Nulla vel magna in leo mattis congue in eget quam. Proin  dignissim nunc vitae nunc euismod sollicitudin. Nullam pretium placerat  eleifend. Aliquam erat volutpat. Nunc et massa nisl, lacinia pharetra  eros. In sit amet augue a ante tincidunt viverra.\n&lt;/p&gt;\n&lt;br&gt;\n&lt;p&gt;\n	&lt;a href=&quot;#&quot; class=&quot;wojo positive button&quot;&gt;Read More&lt;/a&gt;\n&lt;/p&gt;\n', 3, 1);

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `plug_videoslider`
--

CREATE TABLE `plug_videoslider` (
  `id` int(11) NOT NULL,
  `title_en` varchar(150) NOT NULL,
  `title_tr` varchar(150) NOT NULL,
  `vidurl` varchar(150) DEFAULT NULL,
  `position` int(5) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Tablo döküm verisi `plug_videoslider`
--

INSERT INTO `plug_videoslider` (`id`, `title_en`, `title_tr`, `vidurl`, `position`) VALUES
(1, 'Megamind', 'Megamind', 'https://www.youtube.com/watch?v=CzzPbEA6vVU', 1),
(2, 'Ice Age 4', 'Ice Age 4', 'https://www.youtube.com/watch?v=hzixp8s4pyg', 8),
(3, 'Toy Story 3', 'Toy Story 3', 'https://www.youtube.com/watch?v=roADdYWAv4A', 3),
(4, 'Big Buck Bunny animation', 'Big Buck Bunny animation', 'https://www.youtube.com/watch?v=XSGBVzeBUbk', 7),
(5, 'Married Life - Carl &amp; Ellie', 'Married Life - Carl &amp; Ellie', 'https://www.youtube.com/watch?v=GroDErHIM_0', 5),
(6, 'Pixar For the birds', 'Pixar For the birds', 'https://www.youtube.com/watch?v=zqmrEa5DLig', 6),
(7, 'Logo Animation', 'Logo Animation', 'https://www.vimeo.com/47375194', 4),
(8, 'Voodoo', 'Voodoo', 'http://vimeo.com/89396394', 2);

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `settings`
--

CREATE TABLE `settings` (
  `site_name` varchar(100) NOT NULL,
  `company` varchar(100) NOT NULL,
  `site_url` varchar(150) NOT NULL,
  `site_dir` varchar(50) DEFAULT NULL,
  `site_email` varchar(50) NOT NULL,
  `theme` varchar(32) NOT NULL,
  `theme_var` varchar(32) DEFAULT NULL,
  `seo` tinyint(1) NOT NULL DEFAULT '0',
  `perpage` tinyint(4) NOT NULL DEFAULT '10',
  `backup` varchar(64) NOT NULL,
  `thumb_w` varchar(5) NOT NULL,
  `thumb_h` varchar(5) NOT NULL,
  `img_w` varchar(5) NOT NULL,
  `img_h` varchar(5) NOT NULL,
  `avatar_w` varchar(3) DEFAULT '80',
  `avatar_h` varchar(3) DEFAULT '80',
  `short_date` varchar(50) NOT NULL,
  `long_date` varchar(50) NOT NULL,
  `time_format` varchar(10) DEFAULT NULL,
  `dtz` varchar(120) DEFAULT NULL,
  `locale` varchar(200) DEFAULT NULL,
  `weekstart` tinyint(1) NOT NULL DEFAULT '1',
  `lang` varchar(2) NOT NULL DEFAULT 'en',
  `show_lang` tinyint(1) NOT NULL DEFAULT '0',
  `langdir` varchar(3) NOT NULL DEFAULT 'ltr',
  `eucookie` tinyint(1) NOT NULL DEFAULT '0',
  `offline` tinyint(1) NOT NULL DEFAULT '0',
  `offline_msg` text,
  `offline_d` date DEFAULT NULL,
  `offline_t` time DEFAULT NULL,
  `logo` varchar(100) DEFAULT NULL,
  `logo_footer` varchar(100) DEFAULT NULL,
  `showlogin` tinyint(1) NOT NULL DEFAULT '1',
  `showsearch` tinyint(1) NOT NULL DEFAULT '1',
  `showcrumbs` tinyint(1) NOT NULL DEFAULT '1',
  `bgimg` varchar(60) DEFAULT NULL,
  `repbg` tinyint(1) DEFAULT '0',
  `bgalign` enum('left','right','center') DEFAULT 'left',
  `bgfixed` tinyint(1) DEFAULT '0',
  `bgcolor` varchar(7) DEFAULT NULL,
  `currency` varchar(4) DEFAULT NULL,
  `cur_symbol` varchar(2) DEFAULT NULL,
  `dsep` char(1) NOT NULL DEFAULT ',',
  `tsep` char(1) NOT NULL DEFAULT '.',
  `reg_verify` tinyint(1) NOT NULL DEFAULT '1',
  `auto_verify` tinyint(1) NOT NULL DEFAULT '1',
  `reg_allowed` tinyint(1) NOT NULL DEFAULT '1',
  `notify_admin` tinyint(1) NOT NULL DEFAULT '0',
  `user_limit` varchar(6) DEFAULT NULL,
  `flood` varchar(6) DEFAULT NULL,
  `attempt` varchar(2) DEFAULT NULL,
  `logging` tinyint(1) NOT NULL DEFAULT '0',
  `editor` tinyint(1) NOT NULL DEFAULT '1',
  `metakeys` text,
  `metadesc` text,
  `analytics` text,
  `mailer` enum('PHP','SMTP','SMAIL') DEFAULT NULL,
  `sendmail` varchar(60) DEFAULT NULL,
  `smtp_host` varchar(150) DEFAULT NULL,
  `smtp_user` varchar(50) DEFAULT NULL,
  `smtp_pass` varchar(50) DEFAULT NULL,
  `smtp_port` varchar(3) DEFAULT NULL,
  `is_ssl` tinyint(1) NOT NULL DEFAULT '0',
  `login_page` varchar(100) DEFAULT NULL,
  `register_page` varchar(100) DEFAULT NULL,
  `sitemap_page` varchar(100) DEFAULT NULL,
  `search_page` varchar(100) DEFAULT NULL,
  `activate_page` varchar(100) DEFAULT NULL,
  `account_page` varchar(100) DEFAULT NULL,
  `profile_page` varchar(100) DEFAULT NULL,
  `version` varchar(10) NOT NULL,
  `site_phone` varchar(55) NOT NULL,
  `site_fax` varchar(255) NOT NULL,
  `address` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Tablo döküm verisi `settings`
--

INSERT INTO `settings` (`site_name`, `company`, `site_url`, `site_dir`, `site_email`, `theme`, `theme_var`, `seo`, `perpage`, `backup`, `thumb_w`, `thumb_h`, `img_w`, `img_h`, `avatar_w`, `avatar_h`, `short_date`, `long_date`, `time_format`, `dtz`, `locale`, `weekstart`, `lang`, `show_lang`, `langdir`, `eucookie`, `offline`, `offline_msg`, `offline_d`, `offline_t`, `logo`, `logo_footer`, `showlogin`, `showsearch`, `showcrumbs`, `bgimg`, `repbg`, `bgalign`, `bgfixed`, `bgcolor`, `currency`, `cur_symbol`, `dsep`, `tsep`, `reg_verify`, `auto_verify`, `reg_allowed`, `notify_admin`, `user_limit`, `flood`, `attempt`, `logging`, `editor`, `metakeys`, `metadesc`, `analytics`, `mailer`, `sendmail`, `smtp_host`, `smtp_user`, `smtp_pass`, `smtp_port`, `is_ssl`, `login_page`, `register_page`, `sitemap_page`, `search_page`, `activate_page`, `account_page`, `profile_page`, `version`, `site_phone`, `site_fax`, `address`) VALUES
('POSMAKS NOKTA SATIŞ SİSTEMLERİ', 'POSMAKS', 'http://localhost', 'posmaks-cms', 'eoruc@kodar.com.tr', 'kodar', NULL, 1, 100, '26-Nov-2016_15-29-32.sql', '150', '150', '800', '800', '80', '80', '%d %b %Y', '%B %d, %Y %I:%M %p', '%I:%M %p', 'Europe/Istanbul', 'tr_utf8,Turkish,tr_TR.UTF-8,Turkish_Turkey.1254,WINDOWS-1254', 2, 'tr', 1, 'ltr', 0, 0, '&lt;p&gt;We are currently working on improving our site. Morbi in sem quis dui placerat ornare. Pellentesque odio nisi, euismod in, pharetra a, ultricies in, diam. Sed arcu. Cras consequat.&lt;/p&gt;&lt;p&gt;Praesent dapibus, neque id cursus faucibus, tortor neque egestas augue, eu vulputate magna eros eu erat. Aliquam erat volutpat. Nam dui mi, tincidunt quis, accumsan porttitor, facilisis luctus, metus.&lt;/p&gt;', '0000-00-00', '00:00:00', 'logo-travel.png', 'logo-travel-footer.png', 1, 0, 1, 'kodar-bg.jpg', 0, 'left', 0, '', 'TL', '', ',', '.', 1, 1, 1, 1, '0', '1800', '3', 1, 1, 'metakeys, separated,by coma', 'Your website description goes here', '&lt;script&gt;\r\n  (function(i,s,o,g,r,a,m){i[&#039;GoogleAnalyticsObject&#039;]=r;i[r]=i[r]||function(){\r\n  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),\r\n  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)\r\n  })(window,document,&#039;script&#039;,&#039;https://www.google-analytics.com/analytics.js&#039;,&#039;ga&#039;);\r\n\r\n  ga(&#039;create&#039;, &#039;UA-88419718-1&#039;, &#039;auto&#039;);\r\n  ga(&#039;send&#039;, &#039;pageview&#039;);\r\n\r\n&lt;/script&gt;', 'SMTP', '/usr/sbin/sendmail -t -i', 'in-v3.mailjet.com', '31fe03caa4df1ba6001151033000b9f6', '8fd4c121094e9ca5e36e9a12fe3beca5', '587', 0, 'login', 'registration', 'sitemap', 'search', 'activate', 'dashborad', 'profile', '4.10', '+90 216 352 02 02', '+90 216 352 02 04', 'İçerenköy Mah. Ertaç Sok. Baran İş Merkezi No: 10 Kat: 6 &lt;br&gt; Ataşehir / İSTANBUL');

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `stats`
--

CREATE TABLE `stats` (
  `id` int(10) NOT NULL,
  `day` date NOT NULL,
  `pageviews` int(10) NOT NULL DEFAULT '0',
  `uniquevisitors` int(10) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Tablo döküm verisi `stats`
--

INSERT INTO `stats` (`id`, `day`, `pageviews`, `uniquevisitors`) VALUES
(1, '2016-11-21', 96, 11),
(2, '2016-11-22', 77, 11),
(3, '2016-11-23', 62, 14),
(4, '2016-11-24', 11, 1),
(5, '2016-11-25', 212, 15),
(6, '2016-11-26', 88, 13),
(7, '2016-11-27', 2, 1),
(8, '2016-11-28', 3, 1),
(9, '2016-11-29', 30, 3),
(10, '2016-11-30', 5, 2),
(11, '2016-12-01', 34, 4),
(12, '2016-12-02', 17, 4),
(13, '2016-12-03', 60, 14),
(14, '2016-12-04', 8, 1),
(15, '2016-12-05', 87, 5),
(16, '2016-12-06', 1, 1),
(17, '2016-12-07', 2, 1),
(18, '2016-12-08', 5, 3),
(19, '2016-12-09', 1, 1),
(20, '2016-12-10', 64, 11),
(21, '2016-12-11', 7, 1),
(22, '2016-12-12', 3, 1),
(23, '2016-12-13', 3, 1),
(24, '2016-12-14', 23, 7),
(25, '2016-12-15', 3, 1),
(26, '2016-12-16', 1, 1),
(27, '2016-12-17', 5, 2),
(28, '2016-12-18', 7, 1),
(29, '2016-12-19', 60, 3),
(30, '2016-12-20', 30, 3),
(31, '2016-12-21', 3, 1),
(32, '2016-12-22', 3, 1),
(33, '2016-12-23', 9, 1),
(34, '2016-12-24', 13, 1),
(35, '2016-12-25', 9, 1),
(36, '2016-12-26', 16, 5),
(37, '2016-12-27', 8, 1),
(38, '2016-12-28', 10, 1),
(39, '2016-12-29', 5, 1),
(40, '2016-12-30', 17, 1),
(41, '2016-12-31', 8, 1),
(42, '2017-01-01', 9, 1),
(43, '2017-01-02', 19, 2),
(44, '2017-01-03', 16, 3),
(45, '2017-01-04', 20, 5),
(46, '2017-01-05', 7, 1),
(47, '2017-01-06', 14, 3),
(48, '2017-01-07', 8, 1),
(49, '2017-01-08', 8, 1),
(50, '2017-01-09', 14, 1),
(51, '2017-01-10', 4, 1),
(52, '2017-01-11', 13, 5),
(53, '2017-01-12', 6, 1),
(54, '2017-01-13', 4, 1),
(55, '2017-01-14', 17, 6),
(56, '2017-01-15', 5, 1),
(57, '2017-01-16', 7, 1),
(58, '2017-01-17', 1, 1),
(59, '2017-01-18', 5, 1),
(60, '2017-01-19', 18, 6),
(61, '2017-01-20', 2, 1),
(62, '2017-01-21', 2, 1),
(63, '2017-01-22', 2, 1),
(64, '2017-01-23', 12, 2),
(65, '2017-01-24', 16, 3),
(66, '2017-01-25', 8, 1),
(67, '2017-01-26', 16, 6),
(68, '2017-01-27', 16, 1),
(69, '2017-01-28', 1, 1),
(70, '2017-01-29', 16, 6),
(71, '2017-01-30', 1, 1),
(72, '2017-01-31', 5, 1),
(73, '2017-02-01', 29, 11),
(74, '2017-02-02', 22, 2),
(75, '2017-02-03', 25, 6),
(76, '2017-02-04', 32, 5),
(77, '2017-02-05', 2, 1),
(78, '2017-02-06', 11, 6),
(79, '2017-02-07', 1, 1),
(80, '2017-02-08', 22, 6),
(81, '2017-02-09', 1, 1),
(82, '2017-02-10', 14, 6),
(83, '2017-02-11', 8, 1),
(84, '2017-02-12', 8, 1),
(85, '2017-02-13', 19, 6),
(86, '2017-02-14', 5, 1),
(87, '2017-02-15', 21, 4),
(88, '2017-02-16', 19, 2),
(89, '2017-02-17', 23, 8),
(90, '2017-02-18', 7, 1),
(91, '2017-02-19', 3, 1),
(92, '2017-02-20', 25, 7),
(93, '2017-02-21', 4, 1),
(94, '2017-02-22', 5, 2),
(95, '2017-02-23', 13, 1),
(96, '2017-02-24', 16, 1),
(97, '2017-02-25', 5, 1),
(98, '2017-02-26', 19, 6),
(99, '2017-02-27', 8, 1),
(100, '2017-02-28', 6, 2),
(101, '2017-03-01', 3, 1),
(102, '2017-03-02', 5, 1),
(103, '2017-03-03', 15, 3),
(104, '2017-03-04', 7, 1),
(105, '2017-03-05', 6, 1),
(106, '2017-03-06', 2, 1),
(107, '2017-03-07', 12, 6),
(108, '2017-03-08', 1, 1),
(109, '2017-03-09', 2, 1),
(110, '2017-03-10', 5, 1),
(111, '2017-03-11', 18, 1),
(112, '2017-03-12', 7, 1),
(113, '2017-03-13', 8, 1),
(114, '2017-03-14', 20, 6),
(115, '2017-03-15', 3, 1),
(116, '2017-03-16', 2, 1),
(117, '2017-03-17', 4, 1),
(118, '2017-03-18', 2, 1),
(119, '2017-03-19', 14, 6),
(120, '2017-03-20', 9, 1),
(121, '2017-03-21', 25, 6),
(122, '2017-03-22', 4, 1),
(123, '2017-03-23', 15, 1),
(124, '2017-03-24', 24, 6),
(125, '2017-03-25', 31, 12),
(126, '2017-03-26', 11, 2),
(127, '2017-03-27', 12, 1),
(128, '2017-03-28', 3, 1),
(129, '2017-03-29', 8, 1),
(130, '2017-03-30', 6, 1),
(131, '2017-03-31', 8, 1),
(132, '2017-04-01', 7, 1),
(133, '2017-04-02', 15, 2),
(134, '2017-04-03', 3, 1),
(135, '2017-04-04', 16, 1),
(136, '2017-04-05', 4, 1),
(137, '2017-04-06', 5, 1),
(138, '2017-04-07', 8, 1),
(139, '2017-04-08', 32, 6),
(140, '2017-04-09', 6, 1),
(141, '2017-04-10', 3, 1),
(142, '2017-04-11', 14, 5),
(143, '2017-04-12', 15, 3),
(144, '2017-04-13', 19, 6),
(145, '2017-04-14', 5, 1),
(146, '2017-04-15', 9, 1),
(147, '2017-04-16', 7, 1),
(148, '2017-04-17', 2, 1),
(149, '2017-04-18', 16, 6),
(150, '2017-04-19', 5, 1),
(151, '2017-04-20', 17, 1),
(152, '2017-04-21', 25, 1),
(153, '2017-04-22', 4, 1),
(154, '2017-04-23', 118, 30),
(155, '2017-04-24', 97, 29),
(156, '2017-04-25', 108, 35),
(157, '2017-04-26', 85, 5),
(158, '2017-04-27', 165, 15),
(159, '2017-04-28', 106, 14),
(160, '2017-04-29', 14, 2),
(161, '2017-04-30', 41, 6),
(162, '2017-05-01', 155, 19),
(163, '2017-05-02', 44, 9),
(164, '2017-05-03', 251, 38),
(165, '2017-05-04', 151, 32),
(166, '2017-05-05', 130, 34),
(167, '2017-05-06', 32, 9),
(168, '2017-05-07', 53, 11),
(169, '2017-05-08', 55, 11),
(170, '2017-05-09', 50, 11),
(171, '2017-05-10', 29, 6),
(172, '2017-05-11', 34, 8),
(173, '2017-05-12', 32, 8),
(174, '2017-05-13', 28, 4),
(175, '2017-05-14', 48, 6),
(176, '2017-05-15', 33, 7),
(177, '2017-05-16', 37, 8),
(178, '2017-05-17', 35, 5),
(179, '2017-05-18', 19, 3),
(180, '2017-05-19', 35, 10),
(181, '2017-05-20', 21, 2),
(182, '2017-05-21', 34, 12),
(183, '2017-05-22', 24, 4),
(184, '2017-05-23', 33, 7),
(185, '2017-05-24', 20, 5),
(186, '2017-05-25', 24, 4),
(187, '2017-05-26', 63, 6),
(188, '2017-05-27', 41, 4),
(189, '2017-05-28', 16, 5),
(190, '2017-05-29', 46, 10),
(191, '2017-05-30', 40, 4),
(192, '2017-05-31', 1, 1),
(193, '2017-06-01', 1, 1);

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `mod_website_id` int(11) DEFAULT NULL,
  `fbid` bigint(11) NOT NULL DEFAULT '0',
  `username` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `membership_id` tinyint(3) NOT NULL DEFAULT '0',
  `mem_expire` datetime DEFAULT NULL,
  `trial_used` tinyint(1) NOT NULL DEFAULT '0',
  `memused` tinyint(1) NOT NULL DEFAULT '0',
  `email` varchar(60) NOT NULL,
  `fname` varchar(32) NOT NULL,
  `lname` varchar(32) NOT NULL,
  `token` varchar(40) NOT NULL DEFAULT '0',
  `newsletter` tinyint(1) NOT NULL DEFAULT '0',
  `userlevel` tinyint(1) NOT NULL DEFAULT '1',
  `custom_fields` text,
  `created` datetime DEFAULT NULL,
  `lastlogin` datetime DEFAULT NULL,
  `lastip` varchar(16) DEFAULT '0',
  `avatar` varchar(50) DEFAULT NULL,
  `access` text,
  `notes` tinytext,
  `info` tinytext,
  `fb_link` varchar(100) DEFAULT NULL,
  `tw_link` varchar(100) DEFAULT NULL,
  `gp_link` varchar(100) DEFAULT NULL,
  `active` enum('y','n','t','b') NOT NULL DEFAULT 'n',
  `api_token` varchar(40) DEFAULT NULL COMMENT 'posmaks api_token value to access the api services.',
  `database_name` varchar(255) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Tablo döküm verisi `users`
--

INSERT INTO `users` (`id`, `mod_website_id`, `fbid`, `username`, `password`, `membership_id`, `mem_expire`, `trial_used`, `memused`, `email`, `fname`, `lname`, `token`, `newsletter`, `userlevel`, `custom_fields`, `created`, `lastlogin`, `lastip`, `avatar`, `access`, `notes`, `info`, `fb_link`, `tw_link`, `gp_link`, `active`, `api_token`, `database_name`) VALUES
(1, NULL, 0, 'admin', '601f1889667efaebb33b8c12572835da3f027f78', 0, NULL, 0, 0, 'eoruc@kodar.com.tr', '', '', '0', 0, 9, NULL, '2015-02-27 17:20:51', '2017-05-31 09:27:21', '::1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'y', NULL, NULL),
(2, NULL, 0, 'emrah', '601f1889667efaebb33b8c12572835da3f027f78', 2, '2017-07-02 17:18:09', 0, 1, 'orucemrahis@gmail.com', 'emrah', 'emrah', '0', 1, 1, 'tr::emrah::', '2015-02-27 17:42:01', '2015-02-27 17:42:10', '::1', 'IMG_67B5BE-E3650B-2B2255-D76E40-2AA4B6-3FDA89.jpg', NULL, '', '', NULL, NULL, NULL, 'y', '1234512345123451234512345123451234512345', 'posmaks'),
(3, NULL, 0, 'demodemo', '5863d9e4cbdf522eaa62e0747fceb1c5b249ba13', 2, '2016-06-29 20:15:17', 0, 0, 'demo@demo.com', 'demo', 'demo', '0', 0, 8, 'turkey::demo::', '2016-05-30 20:12:08', '2016-05-30 20:15:24', '::1', NULL, 'Modules,digishop', '', '', NULL, NULL, NULL, 'y', NULL, NULL);

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `user_activity`
--

CREATE TABLE `user_activity` (
  `id` int(11) NOT NULL,
  `uid` int(11) NOT NULL DEFAULT '0',
  `url` varchar(200) DEFAULT NULL,
  `icon` varchar(50) DEFAULT NULL,
  `type` varchar(100) DEFAULT NULL,
  `title` varchar(150) DEFAULT NULL,
  `subject` varchar(100) DEFAULT NULL,
  `message` text,
  `created` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dökümü yapılmış tablolar için indeksler
--

--
-- Tablo için indeksler `custom_fields`
--
ALTER TABLE `custom_fields`
  ADD PRIMARY KEY (`id`);

--
-- Tablo için indeksler `email_templates`
--
ALTER TABLE `email_templates`
  ADD PRIMARY KEY (`id`);

--
-- Tablo için indeksler `gateways`
--
ALTER TABLE `gateways`
  ADD PRIMARY KEY (`id`);

--
-- Tablo için indeksler `language`
--
ALTER TABLE `language`
  ADD PRIMARY KEY (`id`);

--
-- Tablo için indeksler `layout`
--
ALTER TABLE `layout`
  ADD KEY `idx_layout_id` (`page_id`),
  ADD KEY `idx_plugin_id` (`plug_id`);

--
-- Tablo için indeksler `log`
--
ALTER TABLE `log`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`);

--
-- Tablo için indeksler `memberships`
--
ALTER TABLE `memberships`
  ADD PRIMARY KEY (`id`);

--
-- Tablo için indeksler `menus`
--
ALTER TABLE `menus`
  ADD PRIMARY KEY (`id`),
  ADD KEY `content_id` (`active`);

--
-- Tablo için indeksler `modules`
--
ALTER TABLE `modules`
  ADD PRIMARY KEY (`id`);

--
-- Tablo için indeksler `mod_adblock`
--
ALTER TABLE `mod_adblock`
  ADD PRIMARY KEY (`id`);

--
-- Tablo için indeksler `mod_adblock_memberlevels`
--
ALTER TABLE `mod_adblock_memberlevels`
  ADD PRIMARY KEY (`adblock_id`,`memberlevel_id`);

--
-- Tablo için indeksler `mod_career`
--
ALTER TABLE `mod_career`
  ADD PRIMARY KEY (`id`);

--
-- Tablo için indeksler `mod_comments`
--
ALTER TABLE `mod_comments`
  ADD PRIMARY KEY (`id`),
  ADD KEY `parent` (`parent_id`,`id`);

--
-- Tablo için indeksler `mod_digishop`
--
ALTER TABLE `mod_digishop`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_mod_digishop_mod_digishop_categories1_idx` (`cid`);

--
-- Tablo için indeksler `mod_digishop_categories`
--
ALTER TABLE `mod_digishop_categories`
  ADD PRIMARY KEY (`id`);

--
-- Tablo için indeksler `mod_digishop_transactions`
--
ALTER TABLE `mod_digishop_transactions`
  ADD PRIMARY KEY (`id`);

--
-- Tablo için indeksler `mod_digishop_transaction_addresses`
--
ALTER TABLE `mod_digishop_transaction_addresses`
  ADD PRIMARY KEY (`id`);

--
-- Tablo için indeksler `mod_digishop_transaction_statuses`
--
ALTER TABLE `mod_digishop_transaction_statuses`
  ADD PRIMARY KEY (`id`);

--
-- Tablo için indeksler `mod_events`
--
ALTER TABLE `mod_events`
  ADD PRIMARY KEY (`id`);

--
-- Tablo için indeksler `mod_events_data`
--
ALTER TABLE `mod_events_data`
  ADD PRIMARY KEY (`id`);

--
-- Tablo için indeksler `mod_faq`
--
ALTER TABLE `mod_faq`
  ADD PRIMARY KEY (`id`);

--
-- Tablo için indeksler `mod_forum`
--
ALTER TABLE `mod_forum`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `idx_slug` (`slug`);

--
-- Tablo için indeksler `mod_forum_posts`
--
ALTER TABLE `mod_forum_posts`
  ADD PRIMARY KEY (`id`);
ALTER TABLE `mod_forum_posts` ADD FULLTEXT KEY `idx_body` (`body`,`title`);

--
-- Tablo için indeksler `mod_forum_thread`
--
ALTER TABLE `mod_forum_thread`
  ADD PRIMARY KEY (`id`);

--
-- Tablo için indeksler `mod_forum_users`
--
ALTER TABLE `mod_forum_users`
  ADD PRIMARY KEY (`id`);

--
-- Tablo için indeksler `mod_gallery_config`
--
ALTER TABLE `mod_gallery_config`
  ADD PRIMARY KEY (`id`);

--
-- Tablo için indeksler `mod_gallery_images`
--
ALTER TABLE `mod_gallery_images`
  ADD PRIMARY KEY (`id`);

--
-- Tablo için indeksler `mod_gmaps`
--
ALTER TABLE `mod_gmaps`
  ADD PRIMARY KEY (`id`);

--
-- Tablo için indeksler `mod_news`
--
ALTER TABLE `mod_news`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_catid` (`cid`);

--
-- Tablo için indeksler `mod_news_categories`
--
ALTER TABLE `mod_news_categories`
  ADD PRIMARY KEY (`id`),
  ADD KEY `order_num` (`position`),
  ADD KEY `parent_id` (`parent_id`);

--
-- Tablo için indeksler `mod_news_comments`
--
ALTER TABLE `mod_news_comments`
  ADD PRIMARY KEY (`id`),
  ADD KEY `parent` (`parent_id`,`id`);

--
-- Tablo için indeksler `mod_news_related_categories`
--
ALTER TABLE `mod_news_related_categories`
  ADD PRIMARY KEY (`aid`,`cid`);

--
-- Tablo için indeksler `mod_portfolio`
--
ALTER TABLE `mod_portfolio`
  ADD PRIMARY KEY (`id`);

--
-- Tablo için indeksler `mod_portfolio_category`
--
ALTER TABLE `mod_portfolio_category`
  ADD PRIMARY KEY (`id`);

--
-- Tablo için indeksler `mod_projects`
--
ALTER TABLE `mod_projects`
  ADD PRIMARY KEY (`id`);

--
-- Tablo için indeksler `mod_projects_category`
--
ALTER TABLE `mod_projects_category`
  ADD PRIMARY KEY (`id`);

--
-- Tablo için indeksler `mod_solutions`
--
ALTER TABLE `mod_solutions`
  ADD PRIMARY KEY (`id`);

--
-- Tablo için indeksler `mod_solutions_category`
--
ALTER TABLE `mod_solutions_category`
  ADD PRIMARY KEY (`id`);

--
-- Tablo için indeksler `mod_timeline`
--
ALTER TABLE `mod_timeline`
  ADD PRIMARY KEY (`id`);

--
-- Tablo için indeksler `mod_timeline_items`
--
ALTER TABLE `mod_timeline_items`
  ADD PRIMARY KEY (`id`),
  ADD KEY `tid` (`tid`);

--
-- Tablo için indeksler `mod_websites`
--
ALTER TABLE `mod_websites`
  ADD PRIMARY KEY (`id`);

--
-- Tablo için indeksler `notes`
--
ALTER TABLE `notes`
  ADD PRIMARY KEY (`id`);

--
-- Tablo için indeksler `pages`
--
ALTER TABLE `pages`
  ADD PRIMARY KEY (`id`);

--
-- Tablo için indeksler `payments`
--
ALTER TABLE `payments`
  ADD PRIMARY KEY (`id`);

--
-- Tablo için indeksler `plugins`
--
ALTER TABLE `plugins`
  ADD PRIMARY KEY (`id`);

--
-- Tablo için indeksler `plug_content_slider`
--
ALTER TABLE `plug_content_slider`
  ADD PRIMARY KEY (`id`);

--
-- Tablo için indeksler `plug_donate`
--
ALTER TABLE `plug_donate`
  ADD PRIMARY KEY (`id`);

--
-- Tablo için indeksler `plug_elastic`
--
ALTER TABLE `plug_elastic`
  ADD PRIMARY KEY (`id`);

--
-- Tablo için indeksler `plug_newsslider`
--
ALTER TABLE `plug_newsslider`
  ADD PRIMARY KEY (`id`);

--
-- Tablo için indeksler `plug_news_tags`
--
ALTER TABLE `plug_news_tags`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_content_id` (`id`);

--
-- Tablo için indeksler `plug_online`
--
ALTER TABLE `plug_online`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `ip` (`ip`),
  ADD KEY `countrycode` (`countrycode`);

--
-- Tablo için indeksler `plug_poll_options`
--
ALTER TABLE `plug_poll_options`
  ADD PRIMARY KEY (`id`);

--
-- Tablo için indeksler `plug_poll_questions`
--
ALTER TABLE `plug_poll_questions`
  ADD PRIMARY KEY (`id`);

--
-- Tablo için indeksler `plug_poll_votes`
--
ALTER TABLE `plug_poll_votes`
  ADD PRIMARY KEY (`id`);

--
-- Tablo için indeksler `plug_slider`
--
ALTER TABLE `plug_slider`
  ADD PRIMARY KEY (`id`);

--
-- Tablo için indeksler `plug_sliderbrando`
--
ALTER TABLE `plug_sliderbrando`
  ADD PRIMARY KEY (`id`);

--
-- Tablo için indeksler `plug_tabs`
--
ALTER TABLE `plug_tabs`
  ADD PRIMARY KEY (`id`);

--
-- Tablo için indeksler `plug_videoslider`
--
ALTER TABLE `plug_videoslider`
  ADD PRIMARY KEY (`id`);

--
-- Tablo için indeksler `stats`
--
ALTER TABLE `stats`
  ADD PRIMARY KEY (`id`);

--
-- Tablo için indeksler `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_users_mod_websites_idx` (`mod_website_id`);

--
-- Tablo için indeksler `user_activity`
--
ALTER TABLE `user_activity`
  ADD PRIMARY KEY (`id`);

--
-- Dökümü yapılmış tablolar için AUTO_INCREMENT değeri
--

--
-- Tablo için AUTO_INCREMENT değeri `custom_fields`
--
ALTER TABLE `custom_fields`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- Tablo için AUTO_INCREMENT değeri `email_templates`
--
ALTER TABLE `email_templates`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
--
-- Tablo için AUTO_INCREMENT değeri `gateways`
--
ALTER TABLE `gateways`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- Tablo için AUTO_INCREMENT değeri `language`
--
ALTER TABLE `language`
  MODIFY `id` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- Tablo için AUTO_INCREMENT değeri `log`
--
ALTER TABLE `log`
  MODIFY `id` int(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
--
-- Tablo için AUTO_INCREMENT değeri `memberships`
--
ALTER TABLE `memberships`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- Tablo için AUTO_INCREMENT değeri `menus`
--
ALTER TABLE `menus`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=51;
--
-- Tablo için AUTO_INCREMENT değeri `modules`
--
ALTER TABLE `modules`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- Tablo için AUTO_INCREMENT değeri `mod_adblock`
--
ALTER TABLE `mod_adblock`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- Tablo için AUTO_INCREMENT değeri `mod_career`
--
ALTER TABLE `mod_career`
  MODIFY `id` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- Tablo için AUTO_INCREMENT değeri `mod_comments`
--
ALTER TABLE `mod_comments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- Tablo için AUTO_INCREMENT değeri `mod_digishop`
--
ALTER TABLE `mod_digishop`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=43;
--
-- Tablo için AUTO_INCREMENT değeri `mod_digishop_categories`
--
ALTER TABLE `mod_digishop_categories`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;
--
-- Tablo için AUTO_INCREMENT değeri `mod_digishop_transactions`
--
ALTER TABLE `mod_digishop_transactions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;
--
-- Tablo için AUTO_INCREMENT değeri `mod_digishop_transaction_addresses`
--
ALTER TABLE `mod_digishop_transaction_addresses`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
--
-- Tablo için AUTO_INCREMENT değeri `mod_digishop_transaction_statuses`
--
ALTER TABLE `mod_digishop_transaction_statuses`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;
--
-- Tablo için AUTO_INCREMENT değeri `mod_events`
--
ALTER TABLE `mod_events`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- Tablo için AUTO_INCREMENT değeri `mod_events_data`
--
ALTER TABLE `mod_events_data`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1858;
--
-- Tablo için AUTO_INCREMENT değeri `mod_faq`
--
ALTER TABLE `mod_faq`
  MODIFY `id` int(4) NOT NULL AUTO_INCREMENT;
--
-- Tablo için AUTO_INCREMENT değeri `mod_forum`
--
ALTER TABLE `mod_forum`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- Tablo için AUTO_INCREMENT değeri `mod_forum_posts`
--
ALTER TABLE `mod_forum_posts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- Tablo için AUTO_INCREMENT değeri `mod_forum_thread`
--
ALTER TABLE `mod_forum_thread`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- Tablo için AUTO_INCREMENT değeri `mod_forum_users`
--
ALTER TABLE `mod_forum_users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- Tablo için AUTO_INCREMENT değeri `mod_gallery_config`
--
ALTER TABLE `mod_gallery_config`
  MODIFY `id` int(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- Tablo için AUTO_INCREMENT değeri `mod_gallery_images`
--
ALTER TABLE `mod_gallery_images`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=68;
--
-- Tablo için AUTO_INCREMENT değeri `mod_gmaps`
--
ALTER TABLE `mod_gmaps`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- Tablo için AUTO_INCREMENT değeri `mod_news`
--
ALTER TABLE `mod_news`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- Tablo için AUTO_INCREMENT değeri `mod_news_categories`
--
ALTER TABLE `mod_news_categories`
  MODIFY `id` int(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- Tablo için AUTO_INCREMENT değeri `mod_news_comments`
--
ALTER TABLE `mod_news_comments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- Tablo için AUTO_INCREMENT değeri `mod_portfolio`
--
ALTER TABLE `mod_portfolio`
  MODIFY `id` int(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- Tablo için AUTO_INCREMENT değeri `mod_portfolio_category`
--
ALTER TABLE `mod_portfolio_category`
  MODIFY `id` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- Tablo için AUTO_INCREMENT değeri `mod_projects`
--
ALTER TABLE `mod_projects`
  MODIFY `id` int(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- Tablo için AUTO_INCREMENT değeri `mod_projects_category`
--
ALTER TABLE `mod_projects_category`
  MODIFY `id` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- Tablo için AUTO_INCREMENT değeri `mod_solutions`
--
ALTER TABLE `mod_solutions`
  MODIFY `id` int(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- Tablo için AUTO_INCREMENT değeri `mod_solutions_category`
--
ALTER TABLE `mod_solutions_category`
  MODIFY `id` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- Tablo için AUTO_INCREMENT değeri `mod_timeline`
--
ALTER TABLE `mod_timeline`
  MODIFY `id` int(6) NOT NULL AUTO_INCREMENT;
--
-- Tablo için AUTO_INCREMENT değeri `mod_timeline_items`
--
ALTER TABLE `mod_timeline_items`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- Tablo için AUTO_INCREMENT değeri `mod_websites`
--
ALTER TABLE `mod_websites`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
--
-- Tablo için AUTO_INCREMENT değeri `notes`
--
ALTER TABLE `notes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- Tablo için AUTO_INCREMENT değeri `pages`
--
ALTER TABLE `pages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;
--
-- Tablo için AUTO_INCREMENT değeri `payments`
--
ALTER TABLE `payments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;
--
-- Tablo için AUTO_INCREMENT değeri `plugins`
--
ALTER TABLE `plugins`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=63;
--
-- Tablo için AUTO_INCREMENT değeri `plug_content_slider`
--
ALTER TABLE `plug_content_slider`
  MODIFY `id` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- Tablo için AUTO_INCREMENT değeri `plug_donate`
--
ALTER TABLE `plug_donate`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- Tablo için AUTO_INCREMENT değeri `plug_elastic`
--
ALTER TABLE `plug_elastic`
  MODIFY `id` int(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- Tablo için AUTO_INCREMENT değeri `plug_newsslider`
--
ALTER TABLE `plug_newsslider`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- Tablo için AUTO_INCREMENT değeri `plug_news_tags`
--
ALTER TABLE `plug_news_tags`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- Tablo için AUTO_INCREMENT değeri `plug_online`
--
ALTER TABLE `plug_online`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- Tablo için AUTO_INCREMENT değeri `plug_poll_options`
--
ALTER TABLE `plug_poll_options`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- Tablo için AUTO_INCREMENT değeri `plug_poll_questions`
--
ALTER TABLE `plug_poll_questions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- Tablo için AUTO_INCREMENT değeri `plug_poll_votes`
--
ALTER TABLE `plug_poll_votes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=42;
--
-- Tablo için AUTO_INCREMENT değeri `plug_slider`
--
ALTER TABLE `plug_slider`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- Tablo için AUTO_INCREMENT değeri `plug_sliderbrando`
--
ALTER TABLE `plug_sliderbrando`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- Tablo için AUTO_INCREMENT değeri `plug_tabs`
--
ALTER TABLE `plug_tabs`
  MODIFY `id` int(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- Tablo için AUTO_INCREMENT değeri `plug_videoslider`
--
ALTER TABLE `plug_videoslider`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- Tablo için AUTO_INCREMENT değeri `stats`
--
ALTER TABLE `stats`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=194;
--
-- Tablo için AUTO_INCREMENT değeri `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- Tablo için AUTO_INCREMENT değeri `user_activity`
--
ALTER TABLE `user_activity`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

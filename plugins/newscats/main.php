<?php
  /**
   * News Categories
   *
   * @package VeriAsist
   * @author veriasist.com
   * @copyright 2014
   * @version $Id: main.php, v4.00 2014-02-20 10:12:05 gewa Exp $
   */

  if (!defined("_VALID_PHP"))
      die('Direct access to this location is not allowed.');
	  
  require_once (MODPATH . "news/admin_class.php");
  $classname = 'News';
  try {
      if (!class_exists($classname)) {
          throw new exception('Missing news/admin_class.php');
      }
      Registry::set('News', new News());
  }
  catch (exception $e) {
      echo $e->getMessage();
  }
?>
<!-- Start News Categories -->
<div class="veriasist tertiary segment">
  <h4><?php echo Lang::$word->_MOD_AM_CATEGORIES;?></h4>
  <div id="newscats">
    <?php $artcats = Registry::get("News")->getCatList(); Registry::get("News")->getCategories($artcats,0, "acats");?>
  </div>
</div>
<!-- End News Categories /--> 
<script type="text/javascript"> 
// <![CDATA[
$(document).ready(function () {
	$("ul#acats").find('ul.menu-submenu').parent().prepend('<i class=\"down arrow icon\"></i>');
	$('#acats i.icon.down.arrow').click(function () {
		$(this).siblings('#acats ul.menu-submenu').slideToggle();
		$(this).toggleClass('vertically flipped');
		
	});
});
// ]]>
</script>
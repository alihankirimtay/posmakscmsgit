<?php
  /**
   * Digishop Categories
   *
   * @package VeriAsist
   * @author veriasist.com
   * @copyright 2014
   * @version $Id: main.php, v4.00 2014-04-20 16:18:34 gewa Exp $
   */
  
  if (!defined("_VALID_PHP"))
      die('Direct access to this location is not allowed.');
	  
  require_once (MODPATH . "digishop/admin_class.php");
  $classname = 'Digishop';
  try {
      if (!class_exists($classname)) {
          throw new exception('Missing digishop/admin_class.php');
      }
      Registry::set('Digishop', new Digishop());
  }
  catch (exception $e) {
      echo $e->getMessage();
  }
  
  $catrows = Registry::get("Digishop")->getCategoryList(true);
?>
<div class="breadcrumb alt-font no-margin-bottom bg-white" style="position:releative; margin-top:200px; background-color:#e6e9ed !important;">
  <h4><?php echo Lang::$word->_MOD_DS_PRODCATS;?></h4>
  <div class="veriasist divided list">
    <?php foreach($catrows as $crows):?>
    <?php $active = (isset(Registry::get("Content")->_url[2]) and Registry::get("Content")->_url[2] == $crows->slug) ? " active" : null;?>
    <div class="item<?php echo $active;?>">
      <div class="right floated tiny veriasist label"><?php echo $crows->total;?></div>
      <i class="icon right angle"></i> <a class="inverted" href="<?php echo doUrl(false, $crows->slug, "digishop-cat");?>"><?php echo $crows->{'name' . Lang::$lang};?></a></div>
    <?php endforeach;?>
  </div>
</div>
<?php
  /**
   * Article Combobox
   *
   * @package VeriAsist
   * @author veriasist.com
   * @copyright 2014
   * @version $Id: main.php, v4.00 20114-05-10 10:12:05 gewa Exp $
   */
  if (!defined("_VALID_PHP"))
      die('Direct access to this location is not allowed.');

  require_once (MODPATH . "solutions/admin_class.php");
  $classname = 'Solutions';
  try {
	  if (!class_exists($classname)) {
		  throw new exception('Missing solutions/admin_class.php');
	  }
	  Registry::set('Solutions', new Solutions());
  }
  catch (exception $e) {
	  echo $e->getMessage();
  }

  $portadata = Registry::get("Solutions")->renderSolutions(true);
?>

<section class="padding-three work-3col wow fadeIn" >
  <div class="container">

    <div class="row margin-nine no-margin-bottom no-margin-top no-margin-lr">
      <div class="col-md-12 col-sm-12 text-center">
         <span class="title-medium deep-green-text text-uppercase alt-font title-dividers font-weight-600 position-relative"><?php echo Lang::$word->_MOD_SLTNS_ALL_PROJECTS; ?></span>
      </div>
    </div>

    <div class="row">
      <div class="grid-gallery grid-style-solutions overflow-hidden">
        <div class="tab-content">
          <ul class="masonry-items grid">

            <?php foreach ($portadata as $key => $row): ?>
              <!-- solution item -->
              <li>
                <figure>
                  <div class="gallery-img">
                    <a href="<?php echo doUrl(false, $row->slug, "solutions-item"); ?>"><img src="<?php echo SITEURL.'/'.Solutions::imagepath . $row->thumb;?>" alt=""></a>
                  </div>
                  <figcaption>
                    <h3 class="alt-font text-uppercase letter-spacing-2">
                      <p><a href="<?php echo doUrl(false, $row->slug, "solutions-item"); ?>"><?php echo $row->{'title' . Lang::$lang};?></a></p>
                    </h3>
                  </figcaption>
                </figure>
              </li>
              <!-- end solution item -->
            <?php endforeach; ?>

          </ul>
        </div>
      </div>
    </div>

    <div class="row">
      <!-- call to action button -->
      <div class="col-md-12 text-center">
          <a class="btn-success btn btn-medium button letter-spacing-1 inner-link" href="<?php echo SITEURL . '/solutions/' ?>" target="_self"><?php echo Lang::$word->_MOD_SLTNS_ALL; ?></a>
      </div>
      <!-- end call to action button -->
    </div>

  </div>
</section>
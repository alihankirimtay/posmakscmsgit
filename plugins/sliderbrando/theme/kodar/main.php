<?php

  if (!defined("_VALID_PHP"))
  	die('Direct access to this location is not allowed.');
  
  require_once(BASEPATH . "sistemyonetimi/plugins/sliderbrando/admin_class.php");
  Registry::set('Sliderbrando', new Sliderbrando());
  
  $slides = Registry::get("Sliderbrando")->getSlides();
  $conf = Registry::get("Sliderbrando");
  ?>

  <?php if(!$slides):?>

  	<?php echo Filter::msgSingleAlert(Lang::$word->_PLG_SL_NOIMG);?>

  <?php else:?>
  	<!-- slider -->
    <section id="home" class="no-padding">
      <div class="owl-slider-full owl-carousel owl-theme light-pagination square-pagination dark-pagination-without-next-prev-arrow main-slider full-screen position-relative">

<?php foreach ($slides as $srow): ?>
          <div class="item owl-bg-img full-screen" style="background-image:url('<?php echo SITEURL . '/' . Sliderbrando::imgPath . $srow->thumb;?>');">
            <div class="opacity-full bg-dark-gray"></div>
            <div class="container full-screen position-relative">
              <div class="slider-typography text-left">
                    <div class="slider-typography text-left padding-thirty-five no-padding-lr xs-padding-thirty-five2 xs-no-padding-lr text-left">

                          <div class="separator-line-extra-thick no-margin-lr margin-two xs-margin-thirteen xs-no-margin-lr xs-width-20"></div>
                          <p><?php echo cleanOut($srow->{'body'.Lang::$lang});?></p>
                          <?php if($srow->urltype != 'nourl'): ?>
                            <a class="highlight-button-transparent-border btn btn-medium margin-thirteen no-margin-lr no-margin-bottom inner-link" href="<?php echo ($srow->urltype == "ext") ? $srow->url : Url::Page($srow->url); ?>" target="<?php echo (($srow->urltype == "ext") ? '_self' : '_self' ); ?>"><?php echo Lang::$word->_EXPLORE_NOW; ?></a>
                          <?php endif; ?> 
                        </div>

              </div>
            </div>
          </div>
          <?php if($srow->urltype != 'nourl'): ?>
            <?php echo '</a>'; ?>
          <?php endif; ?>
          <!-- end slider item -->
        <?php endforeach; ?>

      </div>
    </section>
    <!-- end slider -->
  <?php endif;?>
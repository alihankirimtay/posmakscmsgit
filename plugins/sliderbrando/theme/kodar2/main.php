<?php
  /**
   * Image Sliderbrando
   *
   * @package VeriAsist
   * @author veriasist.com
   * @copyright 2014
   * @version $Id: main.php, v4.00 2014-04-20 10:12:05 gewa Exp $
   */
  if (!defined("_VALID_PHP"))
  	die('Direct access to this location is not allowed.');
  
  require_once(BASEPATH . "sistemyonetimi/plugins/sliderbrando/admin_class.php");
  Registry::set('Sliderbrando', new Sliderbrando());
  
  $slides = Registry::get("Sliderbrando")->getSlides();
  $conf = Registry::get("Sliderbrando");
  ?>

  <?php if(!$slides):?>

  	<?php echo Filter::msgSingleAlert(Lang::$word->_PLG_SL_NOIMG);?>

  <?php else:?>

    <!-- slider -->
    <section id="home" class="travel-top-slider">
      <div class="owl-slider-full owl-carousel owl-theme light-pagination dark-pagination-without-next-prev-arrow main-slider">
        <?php foreach ($slides as $srow): ?>
          <!-- slider item -->
          <div class="item owl-bg-img">
            <div class="container position-relative">
              <div class="row">
                <div class="col-md-12">
                  <div class="col-md-6 col-sm-6 no-padding travel-slider cover-background" style="background-image:url('<?php echo SITEURL . '/' . Sliderbrando::imgPath . $srow->thumb;?>');"></div>
                  <div class="col-md-6 col-sm-6 no-padding travel-slider cover-background" style="background-image:url('<?php echo SITEURL . '/' . Sliderbrando::imgPath . $srow->thumb_1;?>');">
                    <!-- slider text -->
                    <div class="slider-typography text-left padding-twenty-seven xs-padding-thirteen">
                      <div class="slider-text-middle-main">
                        <div class="slider-text-middle slider-typography-option2 xs-display-block">
                          <span class="white-text font-weight-800 letter-spacing-3 alt-font text-uppercase"><?php echo cleanOut($srow->{'title'.Lang::$lang});?></span><br>
                          <div class="bg-dark-blue separator-line-extra-thick no-margin-lr margin-twenty-two xs-margin-thirteen xs-no-margin-lr xs-width-20"></div>
                          <p class="white-text text-uppercase letter-spacing-2 alt-font width-90 xs-text-small"><?php echo cleanOut($srow->{'body'.Lang::$lang});?></p>
                          <?php if($srow->urltype != 'nourl'): ?>
                            <a class="highlight-button-transparent-border btn btn-medium margin-thirteen no-margin-lr no-margin-bottom inner-link" href="<?php echo ($srow->urltype == "ext") ? $srow->url : Url::Page($srow->url); ?>" target="<?php echo (($srow->urltype == "ext") ? '_blank' : '_self' ); ?>"><?php echo Lang::$word->_EXPLORE_NOW; ?></a>
                          <?php endif; ?> 
                        </div>
                      </div>
                    </div>
                    <!-- end slider text -->
                  </div>
                </div>
              </div>
            </div>
          </div>
          <!-- end slider item -->
        <?php endforeach; ?>
      </div>
    </section>
    <!-- end slider -->
  <?php endif;?>
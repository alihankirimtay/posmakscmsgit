<?php
  /**
   * Digishop Cart
   *
   * @package VeriAsist
   * @author veriasist.com
   * @copyright 2014
   * @version $Id: main.php, v4.00 2014-04-20 16:18:34 gewa Exp $
   */
  
  if (!defined("_VALID_PHP"))
      die('Direct access to this location is not allowed.');
  
  if(!array_search("checkout", Registry::get("Content")->_url)) {  
	  require_once (MODPATH . "digishop/admin_class.php");
	  $classname = 'Digishop';
	  try {
		  if (!class_exists($classname)) {
			  throw new exception('Missing digishop/admin_class.php');
		  }
		  Registry::set('Digishop', new Digishop());
	  }
	  catch (exception $e) {
		  echo $e->getMessage();
	  }
	  
	  $cartdata = Registry::get("Digishop")->getCartContent();
	  $totalrow = Registry::get("Digishop")->getCartTotal();
  }
?>
<?php if(!in_array(doUrlParts("digishop-checkout"), Registry::get("Content")->_url)) /*!array_search("checkout", Registry::get("Content")->_url)) */:?>
<div class="veriasist tertiary segment">
  <h4><i class="icon cart"></i> <?php echo Lang::$word->_MOD_DS_CART;?></h4>
  <div id="cartlist" class="veriasist divided list">
    <?php if(!$cartdata):?>
    <div class="content"> <?php echo Lang::$word->_MOD_DS_NOCART;?> </div>
    <?php else:?>
    <?php foreach($cartdata as $xrow):?>
    <div class="item"> <img class="veriasist image" src="<?php echo SITEURL;?>/thumbmaker.php?src=<?php echo SITEURL.'/'.Digishop::imagepath . $xrow->thumb;?>&amp;h=50&amp;w=50&amp;s=1&amp;a=tl" alt=""/>
      <div class="description">
        <div class="header"><a class="inverted" href="<?php echo doUrl(false, $xrow->slug, "digishop-item");?>"><?php echo truncate($xrow->ptitle,20);?></a></div>
        <div class="price"> <?php echo $xrow->total;?> x <?php echo $core->formatMoney($xrow->price);?> <a data-id="<?php echo $xrow->pid;?>" class="remove push-right veriasist label"><i class="icon danger remove"></i></a></div>
      </div>
    </div>
    <?php endforeach;?>
    <div class="veriasist divider"></div>
    <p><b><?php echo Lang::$word->_MOD_DS_GTOTAL;?></b>: <?php echo $core->formatMoney($totalrow->total);?></p>
    <div class="veriasist divider"></div>
    <a href="<?php echo doUrl(false, false, "digishop-checkout");?>" class="veriasist fluid right labeled basic icon button"> <i class="payment icon"></i> <?php echo Lang::$word->_MOD_DS_CHECKOUT;?> </a>
    <?php endif;?>
  </div>
</div>
<?php endif;?>
<?php
  /**
   * Vertical Menu
   *
   * @package VeriAsist
   * @author veriasist.com
   * @copyright 2014
   * @version $Id: main.php, v4.00 2014-04-20 10:12:05 gewa Exp $
   */
  if (!defined("_VALID_PHP"))
      die('Direct access to this location is not allowed.');
?>
<!-- Accordion Menu -->
<div id="acomenu">
<?php $content->getMenu($mainmenu,0,'side-menu');?>
</div>
<script type="text/javascript">
$(document).ready(function () {
	$("ul#side-menu").find('ul.menu-submenu').parent().prepend('<i class=\"down arrow icon\"></i>');
	$('#side-menu i.icon.down.arrow').click(function () {
		$(this).siblings('#side-menu ul.menu-submenu').slideToggle();
		$(this).toggleClass('vertically flipped');
		
	});
});
</script>
<!-- Accordion Menu /-->
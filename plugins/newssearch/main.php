<?php
  /**
   * News Search
   *
   * @package VeriAsist
   * @author veriasist.com
   * @copyright 2014
   * @version $Id: main.php, v4.00 2014-02-25 16:12:05 gewa Exp $
   */
  if (!defined("_VALID_PHP"))
      die('Direct access to this location is not allowed.');
?>
<!-- Start News Search -->
<form action="<?php echo doUrl(false, false, "news-search");?>" method="post">
  <div class="veriasist fluid icon input">
    <input name="keywords" placeholder="Search..." type="text">
    <i class="search icon"></i> </div>
</form>
<!-- End News Search /--> 
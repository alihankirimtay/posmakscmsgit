<?php
  /**
   * jQuery Poll
   *
   * @package VeriAsist
   * @author veriasist.com
   * @copyright 2014
   * @version $Id: controller.php, v4.00 2014-04-20 10:12:05 gewa Exp $
   */
  define("_VALID_PHP", true);
  require_once("../../init.php");
  
  require_once(PLUGPATH . "poll/admin_class.php");
  Registry::set('Poll',new Poll());
?>
<?php
  if (!isset($_POST['poll']) || !isset($_POST['pollid'])):
      print Registry::get("Poll")->showPollQuestion();
  else:
      if (isset($_COOKIE["VERIASIST_voted" . $_POST['pollid']]) != 'yes')
          Registry::get("Poll")->updatePollResult();

      print Registry::get("Poll")->getPollResults(intval($_POST['pollid']));
  endif
?>
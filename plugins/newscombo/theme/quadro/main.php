<?php
  /**
   * Article Combobox
   *
   * @package VeriAsist
   * @author veriasist.com
   * @copyright 2010
   * @version $Id: main.php, v2.00 2011-09-20 10:12:05 gewa Exp $
   */
  if (!defined("_VALID_PHP"))
      die('Direct access to this location is not allowed.');
  
  
  require_once (MODPATH . "news/admin_class.php");
  $classname = 'News';
  try {
	  if (!class_exists($classname)) {
		  throw new exception('Missing news/admin_class.php');
	  }
	  Registry::set('News', new News(false));
  }
  catch (exception $e) {
	  echo $e->getMessage();
  }
  
  $archive = Registry::get("News")->renderArchive();
  $popular = Registry::get("News")->getPopularList();
  $comments = Registry::get("News")->getCommentsList();
?>
<!-- Start News Combobox -->
<div id="artcombo" class="clearfix">
  <ul class="veriasist tabs">
    <li><a data-tab="#pop"><?php echo Lang::$word->_MOD_AM_POPULAR;?></a></li>
    <li><a data-tab="#arc"><?php echo Lang::$word->_MOD_AM_ARCHIVE;?></a></li>
    <li><a data-tab="#com"><i class="icon chat"></i></a></li>
  </ul>
  <div class="veriasist bottom attached secondary segment">
    <div id="pop" class="veriasist tab content"> 
      <!-- Start Popular Article -->
      <?php if($popular):?>
      <div class="veriasist relaxed divided list">
        <?php foreach ($popular as $poprow):?>
        <div class="item">
          <?php if($poprow->thumb):?>
          <a href="<?php echo SITEURL;?>/modules/news/dataimages/<?php echo $poprow->thumb;?>" title="<?php echo $poprow->imgcap;?>" class="lightbox"><img src="<?php echo SITEURL;?>/thumbmaker.php?src=/<?php echo MODURL;?>news/dataimages/<?php echo $poprow->thumb;?>&amp;h=50&amp;w=50&amp;a=t" alt="" class="veriasist avatar image"></a>
          <?php endif;?>
          <div class="content">
            <div class="header"><a href="<?php echo doUrl(false, $poprow->slug, "news-item");?>" class="inverted"><?php echo truncate($poprow->atitle,25);?></a></div>
            <div class="description"><i class="icon calendar"></i> <?php echo Filter::dodate("short_date",$poprow->created);?></div>
          </div>
        </div>
        <?php endforeach;?>
        <?php unset($poprow);?>
      </div>
    </div>
  
  <?php endif;?>
  <!-- End Popular Article /--> 
  
  <!-- Start News Archive -->
  <?php if($archive):?>
  <div id="arc" class="veriasist tab content">
    <div class="veriasist divided list">
      <?php foreach ($archive as $arow):?>
      <div class="item">
        <div class="right floated veriasist label"><?php echo $arow->total;?></div>
        <i class="icon archive"></i> <a href="<?php echo doUrl(false, false, "news-archive",$arow->year . '-'.$arow->month.'/');?>"> <?php echo News::getShortMonths($arow->month) . " " . $arow->year;?></a></div>
      <?php endforeach;?>
      <?php unset($arow);?>
    </div>
  </div>
  <?php endif;?>
  <!-- End News Archive /--> 
  
  <!-- Start Latest Comments -->
  <?php if($comments):?>
  <div id="com" class="veriasist tab content">
    <div class="veriasist divided list">
      <?php foreach ($comments as $comrow):?>
      <div class="item">
        <div class="right floated veriasist label"><?php echo Filter::dodate("short_date", $comrow->created);?></div>
        <i class="icon chat"></i>
        <div class="content">
          <div class="header"><a href="<?php echo doUrl(false, $comrow->slug, "news-item");?>"><?php echo $comrow->atitle;?></a> </div>
          <div class="description"><?php echo cleanSanitize($comrow->body,50);?></div>
        </div>
      </div>
      <?php endforeach;?>
      <?php unset($comrow);?>
    </div>
  </div>
  <?php endif;?>
  <!-- End Latest Comments /--> 
  </div>
</div>
<!-- End News Combobox /-->
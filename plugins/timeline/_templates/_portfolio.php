<?php
  /**
   * Timeline Plugin News Template
   *
   * @package VeriAsist
   * @author veriasist.com
   * @copyright 2015
   * @version $Id: main.php, v4.00 2015-01-20 10:12:05 gewa Exp $
   */
  if (!defined("_VALID_PHP"))
      die('Direct access to this location is not allowed.');
	  
  require_once(MODPATH . "timeline/admin_class.php");
  Registry::set('Timeline', new Timeline());
  
  $id = intval("###ID###");
  $row = Core::getRowById(Content::plTable, $id);
?>
<div id="timeline"></div>
<script type="text/javascript">
	$(document).ready(function() {
       var timeline_data = [];
		$.ajax({
			type: "get",
			url: SITEURL + "/plugins/timeline/controller.php",
			data: {
				loadData: 1,
				plug_id: '<?php echo $row->plugalias;?>'
			},
			dataType: 'json',
			success: function (json) {
				if (json.message == "success") {
					$(json.entries).each(function (index, data) {
						timeline_data.push({
							type: data.display_mode,
							date: data.timedate,
							dateDisplay: data.edate,
							title: data.title,
							height: data.height,
							content: html_entity_decode(data.content),
							images: data.thumb,
							readmore: data.more
						});
					});
  
					var timeline = new Timeline($('#timeline'), timeline_data);
					timeline.setOptions({
						columnMode:  json.colmode,
						responsive_width: 800,
						max: json.maxitems,
						loadmore: json.showmore,
						readmore_text :'<?php echo Lang::$word->_MOD_TM_READ_MORE;?>',
					    loadmore_text:'<?php echo Lang::$word->_MOD_TM_LOAD_MORE;?>',
					});
					timeline.display();
					doFooter();
				}
			}
		});
	}); 
</script>
<link property="stylesheet" href="<?php echo SITEURL;?>/plugins/timeline/theme/<?php echo Registry::get("Core")->theme . '/' . $row->alt_class?>.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="<?php echo SITEURL;?>/plugins/timeline/script.js"></script>
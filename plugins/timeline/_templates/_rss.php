<?php
  /**
   * Timeline Plugin Rss Template
   *
   * @package VeriAsist
   * @author veriasist.com
   * @copyright 2015
   * @version $Id: main.php, v4.00 2015-01-20 10:12:05 gewa Exp $
   */
  if (!defined("_VALID_PHP"))
      die('Direct access to this location is not allowed.');
	  
  require_once(MODPATH . "timeline/admin_class.php");
  Registry::set('Timeline', new Timeline());
  
  $id = intval("###ID###");
  $row = Core::getRowById(Content::plTable, $id);
?>
<?php if($row):?>
<?php $modid = Core::getRow(Timeline::mTable, "plugin_id", $row->plugalias);?>
<div id="timeline"></div>
<link property="stylesheet" href="<?php echo SITEURL;?>/plugins/timeline/theme/<?php echo Registry::get("Core")->theme . '/' . $row->alt_class?>.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="<?php echo SITEURL;?>/plugins/timeline/script.js"></script>
<script type="text/javascript">
  $(document).ready(function() {
	  var timeline_data = [];
	  var rss_feed = '<?php echo $modid->rssurl;?>';
	  var feed_num = <?php echo $modid->limiter;?>;

	  // Get Rss Feed via Google Feed API
	  $.getJSON('https://ajax.googleapis.com/ajax/services/feed/load?v=1.0&num=' + feed_num + '&q=' + rss_feed + '&callback=?', function(data) {
		  if (data && data.responseData && data.responseData.feed && data.responseData.feed.entries) {
			  $(data.responseData.feed.entries).each(function(index, entry) {
				  var date = entry.publishedDate.split(' ');

				  timeline_data.push({
					  type: 'news_post',
					  date: date[3],
					  dateDisplay: formatDateTime(entry.publishedDate),
					  title: entry.title,
					  content: entry.contentSnippet + '<div align="right"><a href="' + entry.link + '"><?php echo Lang::$word->_MOD_TM_READ_MORE;?> <i class="icon angle right"></i></a></div>'
				  });
			  });

			  var timeline = new Timeline($('#timeline'), timeline_data);
			  timeline.setOptions({
				  animation: true,
				  lightbox: true,
				  separator: 'year',
				  columnMode: '<?php echo $modid->colmode;?>',
				  order: 'desc'
			  });
			  timeline.display();
		  }
	  });
  });
</script>
<?php endif;?>
<?php
  /**
   * Timeline Plugin Facebook Template
   *
   * @package VeriAsist
   * @author veriasist.com
   * @copyright 2015
   * @version $Id: main.php, v4.00 2015-01-20 10:12:05 gewa Exp $
   */
  if (!defined("_VALID_PHP"))
      die('Direct access to this location is not allowed.');
	  
  require_once(MODPATH . "timeline/admin_class.php");
  Registry::set('Timeline', new Timeline());
  
  $id = intval("###ID###");
  $row = Core::getRowById(Content::plTable, $id);
?>
<?php if($row):?>
<?php $modid = Core::getRow(Timeline::mTable, "plugin_id", $row->plugalias);?>
<div id="timeline"></div>
<div id="fb-root"></div>
<script type="text/javascript" src="http://connect.facebook.net/en_US/all.js"></script>
<link property="stylesheet" href="<?php echo SITEURL;?>/plugins/timeline/theme/<?php echo Registry::get("Core")->theme . '/' . $row->alt_class?>.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="<?php echo SITEURL;?>/plugins/timeline/script.js"></script>
<script type="text/javascript">
	$(document).ready(function() {
		var timeline = new Timeline($('#timeline'));
		timeline.setOptions({
			animation:            true,
			lightbox:             true,
			separator:            'year',
			columnMode:           '<?php echo $modid->colmode;?>',
			facebookPageId:       <?php echo $modid->fbpage;?>,
			facebookAccessToken: '<?php echo $modid->fbtoken;?>'
		});
		timeline.display();
	});
</script>
<?php endif;?>
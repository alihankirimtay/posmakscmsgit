<?php
  /**
   * Controller
   *
   * @package VeriAsist
   * @author veriasist.com
   * @copyright 2010
   * @version $Id: calendar.php, v2.00 2011-04-20 10:12:05 gewa Exp $
   */
  define("_VALID_PHP", true);
  require_once ("../../init.php");

  require_once (MODPATH . "timeline/admin_class.php");
  Registry::set('Timeline', new Timeline());
?>
<?php

  if (isset($_GET['loadData'])) {
      if ($row = Core::getRow(Timeline::mTable, "plugin_id", sanitize($_GET['plug_id']))) {

          switch ($row->type) {
              case "news_module":
				  $sql = "SELECT id, thumb, slug, gallery, YEAR(created) as year, MONTH(created) as month, created as timedate,"
				  . "\n title" . Lang::$lang . " as title, body" . Lang::$lang . " as content" 
				  . "\n FROM mod_news" 
				  . "\n WHERE created <= NOW()" 
				  . "\n AND (expire = '0000-00-00 00:00:00' OR expire >= NOW())" 
				  . "\n AND active = 1" 
				  . "\n ORDER BY created DESC LIMIT " . $row->limiter;
                  $dataset = $db->fetch_all($sql);

                  if ($dataset) {
                      $json_response = array();
                      $array = array();
                      foreach ($dataset as $data) {
                          if ($data->gallery) {
							  $sql2 = "SELECT g.*,c.folder"
							  . "\n FROM mod_gallery_images as g" 
							  . "\n LEFT JOIN mod_gallery_config AS c ON c.id = g.gallery_id" 
							  . "\n WHERE g.gallery_id = " . $data->gallery
							  . "\n ORDER BY sorting";
                              $galrow = $db->fetch_all($sql2);
							  
                              if ($galrow) {
								  $images = array();
                                  foreach ($galrow as $grow) {
                                      $images[] = SITEURL . '/modules/gallery/galleries/' . $grow->folder . '/' . $grow->thumb;
									  $array['thumb'] = $images;
                                  }
                              }
                          } else {
                              $array['thumb'] = $data->thumb ? array(SITEURL . '/modules/news/dataimages/' . $data->thumb) : '';
                              $array['height'] = '';
                          }

                          $array['year'] = $data->year;
                          $array['month'] = $data->month;
                          $array['timedate'] = $data->timedate;
                          $array['edate'] = Filter::dodate("short_date", $data->timedate);
                          $array['title'] = $data->title;
                          $array['content'] = cleanSanitize($data->content, 200);
                          $array['more'] = Url::News("item", $data->slug);
                          $array['display_mode'] = 'news_post';

                          array_push($json_response, $array);
                      }

                      $json['message'] = 'success';
					  $json['maxitems'] = ($row->maxitems) ? $row->maxitems : null;
					  $json['showmore'] = $row->showmore;
					  $json['colmode'] = $row->colmode;
                      $json['entries'] = $json_response;
                      print json_encode($json);
                  }
              break;
			  
			  case "event_module":
				  $sql = "SELECT *, YEAR(date_start) as year, MONTH(date_start) as month, CONCAT(date_start,' ',time_start) as timedate,"
				  . "\n title" . Lang::$lang . " as title, body" . Lang::$lang . " as content" 
				  . "\n FROM mod_events" 
				  . "\n WHERE active = 1" 
				  . "\n ORDER BY date_start DESC LIMIT " . $row->limiter;
                  $dataset = $db->fetch_all($sql);

                  if ($dataset) {
                      $json_response = array();
                      $array = array();
                      foreach ($dataset as $data) {
						  
                          $array['year'] = $data->year;
                          $array['month'] = $data->month;
                          $array['timedate'] = $data->timedate;
                          $array['edate'] = Filter::dodate("long_date", $data->timedate);
                          $array['title'] = $data->title;
                          $array['content'] = cleanSanitize($data->content);
                          $array['more'] = null;
                          $array['display_mode'] = 'news_post';

                          array_push($json_response, $array);

                      }

                      $json['message'] = 'success';
					  $json['maxitems'] = ($row->maxitems) ? $row->maxitems : null;
					  $json['showmore'] = $row->showmore;
					  $json['colmode'] = $row->colmode;
                      $json['entries'] = $json_response;
                      print json_encode($json);
                  }  
			  break;
			  
			  case "portfolio_module":
				  $sql = "SELECT *, YEAR(created) as year, MONTH(created) as month, created as timedate,"
				  . "\n title" . Lang::$lang . " as title, body" . Lang::$lang . " as content" 
				  . "\n FROM mod_portfolio" 
				  . "\n ORDER BY created DESC LIMIT " . $row->limiter;
                  $dataset = $db->fetch_all($sql);
				  
                  if ($dataset) {
                      $json_response = array();
                      $array = array();
                      foreach ($dataset as $data) {
						  
                          $array['year'] = $data->year;
                          $array['month'] = $data->month;
                          $array['timedate'] = $data->timedate;
                          $array['edate'] = Filter::dodate("short_date", $data->timedate);
                          $array['title'] = $data->title;
                          $array['content'] = cleanSanitize($data->content);
						  $array['thumb'] = $data->thumb ? array(SITEURL . '/modules/portfolio/dataimages/' . $data->thumb) : '';
                          $array['more'] = Url::Portfolio("item", $data->slug);
                          $array['display_mode'] = 'news_post';

                          array_push($json_response, $array);

                      }

                      $json['message'] = 'success';
					  $json['maxitems'] = ($row->maxitems) ? $row->maxitems : null;
					  $json['showmore'] = $row->showmore;
					  $json['colmode'] = $row->colmode;
                      $json['entries'] = $json_response;
                      print json_encode($json);
                  } 
			  break;
			  
			  case "newsslider_module":
				  $sql = "SELECT *, YEAR(created) as year, MONTH(created) as month, created as timedate,"
				  . "\n title" . Lang::$lang . " as title, body" . Lang::$lang . " as content" 
				  . "\n FROM plug_newsslider" 
				  . "\n WHERE active = 1" 
				  . "\n ORDER BY position LIMIT " . $row->limiter;
                  $dataset = $db->fetch_all($sql);
				  
                  if ($dataset) {
                      $json_response = array();
                      $array = array();
                      foreach ($dataset as $data) {
						  
                          $array['year'] = $data->year;
                          $array['month'] = $data->month;
                          $array['timedate'] = $data->timedate;
                          $array['edate'] = Filter::dodate("long_date", $data->timedate);
                          $array['title'] = $data->title;
                          $array['content'] = cleanOut($data->content);
						  $array['thumb'] = '';
                          $array['more'] = null;
                          $array['display_mode'] = 'news_post';

                          array_push($json_response, $array);

                      }

                      $json['message'] = 'success';
					  $json['maxitems'] = ($row->maxitems) ? $row->maxitems : null;
					  $json['showmore'] = $row->showmore;
					  $json['colmode'] = $row->colmode;
                      $json['entries'] = $json_response;
                      print json_encode($json);
                  } 
			  break;
			  	  
			  case "custom":
				  $sql = "SELECT *, YEAR(created) as year, MONTH(created) as month, created as timedate,"
				  . "\n title" . Lang::$lang . " as title, body" . Lang::$lang . " as content" 
				  . "\n FROM " . Timeline::cTable
				  . "\n WHERE tid = " . $row->id
				  . "\n ORDER BY created DESC LIMIT " . $row->limiter;
                  $dataset = $db->fetch_all($sql);
				  
                  if ($dataset) {
                      $json_response = array();
                      $array = array();
                      foreach ($dataset as $data) {
						  
						  $imagedata = explode(",", $data->images);
						  natcasesort($imagedata);
						  
                          $array['year'] = $data->year;
                          $array['month'] = $data->month;
                          $array['timedate'] = $data->timedate;
                          $array['edate'] = Filter::dodate("long_date", $data->timedate);
                          $array['title'] = $data->title;
                          $array['content'] = cleanOut($data->content);
                          $array['more'] = $data->readmore;
                          $array['display_mode'] = $data->type;
						  
                          if (count($imagedata) > 1) {
							  $images = array();
							  foreach ($imagedata as $grow) {
								  $images[] = UPLOADURL . '/timeline/' . $grow;
								  $array['thumb'] = $images;
							  }
                          } else {
                              $array['thumb'] = $data->images ? array(UPLOADURL . '/timeline/' . $data->images) : '';
                          }
						  if($data->type == "iframe") {
							  $array['url'] = $data->dataurl;
							  $array['height'] = $data->height;
						  }
                          array_push($json_response, $array);

                      }

                      $json['message'] = 'success';
					  $json['maxitems'] = ($row->maxitems) ? $row->maxitems : null;
					  $json['showmore'] = $row->showmore;
					  $json['colmode'] = $row->colmode;
                      $json['entries'] = $json_response;
                      print json_encode($json);
                  } 
			  break;
          }
      }
  }
String.prototype.parseURL = function() {
    return this.replace(/[A-Za-z]+:\/\/[A-Za-z0-9-_]+\.[A-Za-z0-9-_:%&~\?\/.=]+/g, function(url) {
        return url.link(url);
    });
};

String.prototype.trimString = function(length) {
    return this.substr(0, length) + (this.length > length ? '...' : '');
};

function Timeline(element, data, $, undefined) {
    if ($ === undefined) {
        $ = jQuery;
    }

    var SELF = this;
    var BODY = $(document.body);

    this._container = null; // cached DOM
    this._spine = null; // cached DOM
    this._overlay = null; // cached DOM
    this._lightbox = null; // cached DOM

    this._original_data = data ? $.extend(true, [], data) : []; // a copy of the original data
    this._data = []; // main data

    this._responsive = false; // in responsive mode?

    this._options = {
        dateFormat: 'DD MMMM YYYY', // default date format
        first_separator: true, // display the first separator?
        separator: 'year', // year, month, month_year, null
        columnMode: 'dual', // dual, left, right, center
        order: 'desc', // "asc", "desc",
        responsive_width: null, // swith to responsive mode when window width is smaller than the width
        animation: true, // animate on page load?
        lightbox: true, // use lightbox?
        max: null, // maximum number of elements
        loadmore: 0, // load more data. Set to 5 to load 5 additional data each time
        facebookAccessToken: null, // facebook access token  "https://graph.facebook.com/oauth/access_token?client_id={app-id}&client_secret={app-secret}&grant_type=client_credentials"
        facebookPageId: null, // facebook page id
        onSearchSuccess: null, // XHR search success handler
        onSearchError: null, // XHR search error handler.
		readmore_text :'Read More',
		loadmore_text:'Load More',
		month_translation: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December']
    };

    this._years = [];
    this._months = [];

    // settings that user can't change via setOptions()
    //this._readmore_text = 'Read More';
    //this._loadmore_text = 'Load More';
    //this._month_translation = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December']; // month separators
    this._spine_margin = 100;

    this._facebook_content_length = 300;
    this._facebook_description_length = 150;

    // cache timeline elements in an array
    this._elements = [];
    this._separators = [];

    // process queue
    this._iframe_queue = []; // iframe queue

    this._lightbox_loading = false;

    // detect css3 transition support
    this._use_css3 = (function() {
        var style = document.body.style;

        if (typeof style['transition'] == 'string') {
            return true;
        }

        // Tests for vendor specific prop
        var prefix = ['Webkit', 'Moz', 'Khtml', 'O', 'ms'];
        for (var i = 0; i < prefix.length; i++) {
            if (typeof(style[prefix[i] + 'Transition']) == 'string') {
                return true;
            }
        }

        return false;
    })();

    // default timeline element options
    this._default_element_data = {
        type: 'news_post', // news_post   gallery   iframe
        date: '2000-01-01', // news_post   gallery   iframe
        dateDisplay: '',
        dateFormat: null, // news_post   gallery   iframe
        title: null, // news_post   gallery   iframe
        images: [], // news_post   gallery
        content: null, // news_post
        readmore: null, // news_post
        speed: null, // news_post
        height: 300, //             gallery   iframe
        url: null //                       iframe
    };

    this._transitionEnd = function(element, event_handler) {
            var support_transition = false;
            var transition_names = ['transition', 'WebkitTransition', 'MozTransition', 'msTransition'];
            var temp_element = document.createElement('div');

            $(transition_names).each(function(index, transition_name) {
                if (temp_element.style[transition_name] !== undefined) {
                    support_transition = true;
                    return false;
                }
            });

            if (support_transition) {
                element.one('webkitTransitionEnd oTransitionEnd MSTransitionEnd transitionend', event_handler);
            } else {
                setTimeout(event_handler, 0);
            }

            // return element so that we can chain methods
            return element;
        },

        this._prepareData = function() {
            $.each(SELF._original_data, function(index, data) {
                //var m = moment(data.date);
                //data.date = m.format('YYYY-MM-DD HH:mm:ss');
            });

            SELF._sortData(SELF._original_data);

            if (SELF._options.max && SELF._options.max < SELF._original_data.length) {
                SELF._data = SELF._original_data.slice(0, SELF._options.max);
            } else {
                SELF._data = SELF._original_data;
            }
        };

    // sort data in desc or asc order
    this._sortData = function(timeline_data) {
        if (timeline_data) {
            // sort data by date
            timeline_data.sort(function(a, b) {
                if (SELF._options.order === 'desc') {
                    return parseInt(b.date.replace(/[^0-9]/g, ''), 10) - parseInt(a.date.replace(/[^0-9]/g, ''), 10);
                } else {
                    return parseInt(a.date.replace(/[^0-9]/g, ''), 10) - parseInt(b.date.replace(/[^0-9]/g, ''), 10);
                }
            });
        }

        return timeline_data;
    };

    // create a single timeline element
    this._createElement = function(element_data, position) {
        element_data = $.extend({}, SELF._default_element_data, element_data)

        // element DIV
        var element = $('<div>').addClass('timeline_element ' + element_data.type);

        if (position === 'left') {
            element.addClass('timeline_element_left');
        } else if (position === 'right') {
            element.addClass('timeline_element_right');
        }

        if (!SELF._options.animation) {
            element.addClass('animated');
        }

        var element_box = $('<div>').addClass('timeline_element_box').appendTo(element);

        // title DIV
        if (element_data.title) {
            var title = $('<div>').addClass('timeline_title').html('<span class="timeline_title_label">' + element_data.title + '</span><span class="timeline_title_date">' + element_data.dateDisplay + '</span>').appendTo(element_box);
        } else {
            element.addClass('notitle');
        }

        switch (element_data.type) {
            case 'iframe':
                // content DIV
                var content = $('<div>').addClass('content loading').height(element_data.height).appendTo(element_box);

                SELF._iframe_queue.push({
                    element: content,
                    url: element_data.url
                });

                break;
            case 'gallery':
                if (element_data.images.length) {
                    if (element_data.content) {
                        $('<div>').addClass('content').html(element_data.content).appendTo(element_box);
                    }

                    var image_html = '';

                    $(element_data.images).each(function(index, image_src) {
                        image_html += '<img src="' + image_src + '" />';
/*
                        if (SELF._options.lightbox) {
                            image_html += '<div class="img_overlay"><span class="magnifier" data-total="' + element_data.images.length + '" data-order="' + index + '" data-img="' + image_src + '"></span></div>';
                        }
*/
                        image_html += '';
                    });

                    var $container = $('<div>').addClass('veriasist-carousel').appendTo(element_box).html(image_html);
                    $container.owlCarousel({
                        pagination: false,
                        navigation: true,
                        items : 2,
                    })
                }

                break;
            case 'news_post':
                if (!element_data.content && !element_data.readmore) {
                    element.addClass('nocontent');
                }

                var html = '';

                $(element_data.images).each(function(index, image_src) {
                    if (element_data.images.length > 1) {
                        is_multi = '';
                    } else {
                        is_multi = (index === 0 ? ' active' : '') + '" style="display:' + (index === 0 ? 'block' : 'none');
                    }
					is_image = (image_src) ? '<img src="' + image_src + '">' : '';
					
                    html += '<div data-total="' + element_data.images.length + '" data-order="' + index + '" class="img_container' + is_multi + '">' + is_image;

                    if (SELF._options.lightbox && element_data.images.length == 1 && is_image) {
                        html += '<div class="img_overlay"><span class="magnifier" data-total="' + element_data.images.length + '" data-order="' + index + '" data-img="' + image_src + '"></span></div>';
                    }

                    html += '</div>';
					
					//console.log(image_src)
                });

                if (element_data.images.length > 1) {
                    var $container = $('<div>').addClass('veriasist-carousel').html(html).appendTo(element_box);
                    $container.owlCarousel({
                        pagination: true,
                        navigation: false,
                        singleItem: true
                    })
                } else {
                    $('<div>').addClass('body_container').html(html).appendTo(element_box);
                }

                // content DIV
                if (element_data.content) {
                    $('<div>').addClass('content').html(element_data.content).appendTo(element_box);
                }

                if (element_data.readmore) {
                    $('<div>').addClass('readmore').html('<a href="' + element_data.readmore + '">' + SELF._options.readmore_text + '</a>').appendTo(element_box);
                }

                break;
        }

        element.appendTo(SELF._container);

        // cache element in the array
        SELF._elements.push(element);

        return element;
    };

    // create a year separator
    this._createSeparator = function(time) {
        var separator = $('<div>').addClass('date_separator').attr('id', 'timeline_date_separator_' + time).html('<span>' + time + '</span>').appendTo(SELF._container);
        if (!SELF._options.animation) {
            separator.addClass('animated');
        }
        SELF._separators.push(separator);
    };

    // create DOM from JSON
    this._render = function(timeline_data, is_append) {
        var is_odd = true;

        $(timeline_data).each(function(index, data) {
            if (SELF._options.max && SELF._options.max <= index && !is_append) {
                return false;
            }

            if (is_append && index === 0) {
                var last_element = SELF._container.children(':last');
                if (last_element.length && last_element.hasClass('timeline_element_left')) {
                    is_odd = false;
                }
            }

            var year = parseInt(data.date.split('-')[0], 10);
            var month = parseInt(data.date.split('-')[1], 10);

            month = year + '-' + month;

            var start_new = false;

            if ($.inArray(year, SELF._years) === -1 && (SELF._options.separator === 'year' || SELF._options.separator === null)) {
                start_new = true;
                SELF._years.push(year);
            }

            if ($.inArray(month, SELF._months) === -1 && (SELF._options.separator === 'month' || SELF._options.separator === 'month_year')) {
                start_new = true;
                SELF._months.push(month);
            }

            if (start_new) {
                if (SELF._options.separator === 'year') {
                    if (SELF._years.length > 1 || SELF._options.first_separator) {
                        SELF._createSeparator(year);
                    }
                } else if (SELF._options.separator === 'month' || SELF._options.separator === 'month_year') {
                    if (SELF._months.length > 1 || SELF._options.first_separator) {
                        var month_display = SELF._options.month_translation[parseInt(month.split('-')[1], 10) - 1];

                        if (SELF._options.separator === 'month_year') {
                            month_display = month_display + ' ' + year;
                        }

                        SELF._createSeparator(month_display);
                    }
                }

                if (SELF._options.separator) {
                    is_odd = true;
                }
            }

            if (SELF._options.columnMode === 'dual') {
                SELF._createElement(data, is_odd ? 'left' : 'right');
            } else {
                SELF._createElement(data);
            }

            is_odd = is_odd ? false : true;
        });
    };

    // start animation on page load
    this._startAnimation = function(callback) {
        $(window).width(); // force page reflow

        if (SELF._use_css3) {
            SELF._spine.addClass('animated');
        } else {
            SELF._spine.animate({
                bottom: '0%'
            }, 500, function() {
                SELF._spine.addClass('animated');
            });
        }

        if (SELF._options.separator === 'year' || SELF._options.separator === 'month' || SELF._options.separator === 'month_year') {
            setTimeout(function() {
                // separator
                $(SELF._separators).each(function(index, separator) {
                    if (SELF._use_css3) {
                        separator.addClass('animated');
                    } else {
                        separator.children('span').animate({
                            opacity: 1,
                            top: '50%'
                        }, 300, function() {
                            separator.addClass('animated');
                        });
                    }
                });
            }, 500);
        }

        // timeline element
        var count = 0;
        $(SELF._elements).each(function(index, element) {
            if (!element.hasClass('animated')) {
                count++;

                setTimeout(function(count) {
                    if (SELF._use_css3) {
                        element.addClass('animated');
                    } else {
                        element.hide().addClass('animated').fadeIn();
                    }

                    if (index === SELF._elements.length - 1) {
                        setTimeout(callback, 200);
                    }

                }, (SELF._options.separator === 'year' || SELF._options.separator === 'month' || SELF._options.separator === 'month_year' ? 1000 : 500) + count * 100);
            }
        });

        return true;
    };

    this._loadMore = function() {
        if (SELF._loadmore.hasClass('loading')) {
            return;
        }

        SELF._loadmore.addClass('loading');

        setTimeout(function() {
            SELF._loadmore.removeClass('loading');

            var new_data = SELF._original_data.slice(SELF._data.length, SELF._data.length + SELF._options.loadmore);
            SELF.appendData(new_data);

            if (SELF._data.length >= SELF._original_data.length) {
                SELF._loadmore.remove();
            }
        }, 1000);
    };

    // display timeline
    this._display = function() {
        if (SELF._options.lightbox) {
            SELF._overlay = $('.timeline_overlay');
            if (SELF._overlay.length) {
                SELF._lightbox = SELF._overlay.children('.preview');
            } else {
                // lightbox
                SELF._overlay = $('<div>').addClass('timeline_overlay');
                SELF._lightbox = $('<div>').addClass('preview').html('<div class="navigation"><a class="close" href="#"><i class="icon close"></i></a><span class="prev"><i class="icon arrow left"></i></span><span class="next"><i class="icon arrow right"></i></span></div>').appendTo(SELF._overlay);
                SELF._overlay.appendTo(BODY);
            }
        }

        // build container
        SELF._container = $('<div>').addClass('timeline timeline_' + SELF._options.columnMode);

        if (!$.support.opacity) {
            SELF._container.addClass('opacityFilter');
        }

        if (!SELF._use_css3) {
            SELF._container.addClass('noneCSS3');
        }

        if (SELF._options.responsive_width) {
            $(window).resize();
        }

        // spine
        SELF._spine = $('<div>').addClass('spine').appendTo(SELF._container);
        if (!SELF._options.animation) {
            SELF._spine.addClass('animated');
        }

        // render: create DOM
        SELF._render(SELF._data);

        SELF._container.data('loaded', true).appendTo(element);

        // load more
        if (SELF._options.loadmore && SELF._options.max && SELF._original_data.length && SELF._original_data.length > this._options.max) {
            SELF._loadmore = $('<div>').addClass('timeline_loadmore').text(this._options.loadmore_text).appendTo(element);
        }

        if (SELF._options.animation) {
            setTimeout(function() {
                SELF._startAnimation(SELF._processIframeQueue);
            }, 200); // fix animation cannot be fully seen from the very beginning
        } else {
            SELF._processIframeQueue();
        }

        if (SELF._options.responsive_width) {
            $(window).on('resize', SELF._windowResize);
        }

        $(document).on('click', SELF._handleClick);
        $(document).on('keydown', SELF._handleKeyDown);

        return true;
    };

    this._makeResponsive = function(responsive) {
        if (responsive) {
            if (!this._responsive) {
                this._responsive = true;
                SELF._container.removeClass('timeline_left timeline_right timeline_dual');
                SELF._container.addClass('timeline_center');
            }
        } else {
            if (this._responsive) {
                this._responsive = false;
                SELF._container.removeClass('timeline_center');
                SELF._container.addClass('timeline_' + SELF._options.columnMode);
            }
        }
    };

    /*
    |-------------------|
    |     LIGHTBOX      |
    |-------------------|
    */

    // open image in a lighbox
    this._openLightBox = function(magnifier, image_url) {
        if (this._lightbox_loading) {
            return;
        }

        this._lightbox_loading = true;

        magnifier.parent().addClass('loading');

        SELF._lightbox.data('magnifier', magnifier);

        if (magnifier.data('total') === 1) {
            SELF._lightbox.find('.navigation span').hide();
        } else {
            SELF._lightbox.find('.navigation span').show();
        }

        setTimeout(function() {
            SELF._lightbox_loading = false;

            var _addImage = function() {
                magnifier.parent().removeClass('loading');
                SELF._overlay.addClass('open');
                $('<img>').attr('src', image_url).appendTo(SELF._lightbox);

                var lightbox_size = SELF._getLightboxSize(image.width, image.height);

                SELF._lightbox.addClass('loaded').css({
                    width: lightbox_size.width,
                    height: lightbox_size.height
                });
            }

            var image = new Image();
            image.onload = _addImage;
            image.onerror = _addImage;
            image.src = image_url;
        }, 1000);

        return image_url;
    };

    // close lightbox
    this._closeLightBox = function() {
        SELF._transitionEnd(SELF._lightbox, function() {
            SELF._overlay.removeClass('open');
            SELF._lightbox.removeAttr('style').children('img').remove();
        });

        SELF._lightbox.removeClass('loaded');
    };

    // get lightbox size (max: 90% of window size)
    this._getLightboxSize = function(width, height) {
        var max_width = $(window).width() - 60;
        var max_height = $(window).height() - 60;

        var new_width = width;
        var new_height = height;

        if (width > max_width || height > max_height) {
            var ratio = width / height;

            if (width > max_width && height <= max_height) {
                var new_width = max_width;
                var new_height = height / (width / new_width);
            } else if (height > max_height && width <= max_width) {
                var new_height = max_height;
                var new_width = width / (height / new_height);
            } else {
                var new_width = max_width;
                var new_height = height / (width / new_width);

                if (new_height > max_height) {
                    var new_height = max_height;
                    var new_width = width / (height / new_height);
                }
            }
        }

        return {
            width: new_width,
            height: new_height
        };
    };

    // navigate to next/previous image
    this._navLightBox = function(element, direction) {
        var current_magnifier = SELF._lightbox.data('magnifier');

        if (direction === 'next') {
            var next_image = current_magnifier.parents('.img_container:first').next('.img_container');

            if (!next_image.length) {
                next_image = current_magnifier.parents('.img_container:first').parent().children('.img_container:first');
            }
        } else {
            var next_image = current_magnifier.parents('.img_container:first').prev('.img_container');

            if (!next_image.length) {
                next_image = current_magnifier.parents('.img_container:first').parent().children('.img_container:last');
            }
        }

        var next_magnifier = next_image.find('span.magnifier');
        var image_url = next_magnifier.data('img');

        var _updateImage = function() {
            SELF._lightbox.data('magnifier', next_magnifier).addClass('updating');

            SELF._lightbox.children('img').attr('src', image_url)

            var lightbox_size = SELF._getLightboxSize(image.width, image.height);

            SELF._lightbox.css({
                width: lightbox_size.width,
                height: lightbox_size.height
            });

            setTimeout(function() {
                SELF._lightbox.removeClass('updating');
            }, 500);
        }

        var image = new Image();
        image.onload = _updateImage;
        image.src = image_url;
    };

    /*
    |-------------------|
    |      PROCESS      |
    |-------------------|
    */

    this._processIframeQueue = function() {
        $(SELF._iframe_queue).each(function(index, queue) {
            queue.element.removeClass('loading').html('<iframe frameborder="0" src="' + queue.url + '"></iframe>');
			console.log(queue);
        });
		
		
    };

    /*
    |-------------------|
    |      EVENTS       |
    |-------------------|
    */

    this._windowResize = function(e) {
        if ($(window).width() < SELF._options.responsive_width) {
            SELF._makeResponsive(true);
        } else {
            SELF._makeResponsive(false);
        }
    };

    this._handleClick = function(e) {
        var element = $(e.target);

        if (element.hasClass('timeline_overlay')) {
            SELF._closeLightBox();
        } else if (element.hasClass('magnifier')) {
            SELF._openLightBox(element, element.data('img'));
        } else if (element.hasClass('close')) {
            e.preventDefault();
            SELF._closeLightBox();
        } else if (element.hasClass('prev')) {
            SELF._navLightBox(element, 'prev');
        } else if (element.hasClass('next')) {
            SELF._navLightBox(element, 'next');
        } else if (element.hasClass('timeline_loadmore')) {
            SELF._loadMore();
        }

        return true;
    };

    this._handleKeyDown = function(e) {
        switch (parseInt(e.which, 10)) {
            case 27: // ESC
                if (SELF._overlay.hasClass('open')) {
                    SELF._closeLightBox(e);
                }
                break;
            case 37: // LEFT
                if (SELF._lightbox.hasClass('loaded') && SELF._lightbox.find('.navigation span.prev').is(':visible')) {
                    SELF._lightbox.find('.navigation span.prev').click();
                    return false;
                }
                break;
            case 39: // RIGHT
                if (SELF._lightbox.hasClass('loaded') && SELF._lightbox.find('.navigation span.next').is(':visible')) {
                    SELF._lightbox.find('.navigation span.next').click();
                    return false;
                }

                break;
        }
    };

    /*
    |-------------------|
    |     Facebook      |
    |-------------------|
    */

    this._loadFacebook = function(callback) {
        var api_url = '/' + SELF._options.facebookPageId + '/feed';
        var api_option = {
            'access_token': SELF._options.facebookAccessToken
        };

        var facebook_posts = [];

        var picture_in_loading = 0;

        var _done = function() {
            SELF._original_data = facebook_posts;

            if (callback !== undefined) {
                callback();
            }

            if (SELF._options.onSearchSuccess) {
                SELF._options.onSearchSuccess(facebook_posts);
            }
        };

        var _initContent = function(facebook_data) {
            var content = '<div class="facebook_type_' + facebook_data.type + '">' +
                '<div class="facebook_left_column"><img class="facebook_profile" src="https://graph.facebook.com/' + facebook_data.from.id + '/picture?type=square" /></div>' +
                '<div class="facebook_right_column">';

            if (facebook_data.message) {
                content += '<div class="facebook_content">' + facebook_data.message.trimString(SELF._facebook_message_length).parseURL() + '</div>';
            }

            content += '</div>' +
                '<div style="clear:both;"></div>';

            return content;
        };

        var _pushData = function(facebook_data, content) {
            facebook_posts.push({
                type: 'news_post',
                date: facebook_data.updated_time,
				dateDisplay: formatDateTime(facebook_data.updated_time),
                title: facebook_data.from.name,
                content: content
            });
			
        };

        FB.api(api_url, api_option, function(response) {
            if (!response || !response.data || !response.data.length) {
                if (SELF._options.onSearchError) {
                    SELF._options.onSearchError(response);
                }

                // no data
                return;
            }

            // Get Timeline data via Facebook API
            $(response.data).each(function(index, facebook_data) {
                if (facebook_data.from.id) {
                    if (facebook_data.type === 'photo') {
                        picture_in_loading++;
                        FB.api('/' + facebook_data.object_id, api_option, function(response) {
                            var content = _initContent(facebook_data);

                            if (response.source) {
                                content += '<div class="facebook_post">' +
                                    '<a href="' + facebook_data.link + '" style="display:inline;"><img class="facebook_picture" align="left" src="' + response.source + '" /></a>' +
                                    '</div>';
                            }

                            content += '</div>';

                            _pushData(facebook_data, content);

                            picture_in_loading--;

                            if (picture_in_loading === 0) {
                                _done();
                            }
                        });
                    } else if (facebook_data.message) {
                        var content = _initContent(facebook_data);

                        if (facebook_data.picture) {
                            content += '<div class="facebook_post">' +
                                '<a href="' + facebook_data.link + '" style="display:inline;"><img class="facebook_picture" align="left" src="' + facebook_data.picture + '" /></a>' +
                                '<div class="description_container">' +
                                (facebook_data.name ? '<a href="' + facebook_data.link + '">' + facebook_data.name + '</a>' : '') +
                                (facebook_data.caption ? '<div class="facebook_caption">' + facebook_data.caption + '</div>' : '') +
                                (facebook_data.description ? '<div class="facebook_description">' + facebook_data.description.trimString(SELF._facebook_description_length).parseURL() + '</div>' : '') +
                                '</div>' +
                                '</div>';
                        }

                        content += '</div>';

                        _pushData(facebook_data, content);
                    }
                }
            });

            if (picture_in_loading === 0) {
                _done();
            }
        });
    };

    /*
    |-------------------|
    |        API        |
    |-------------------|
    */

    // set options
    this.setOptions = function(opts) {
        SELF._options = $.extend(SELF._options, opts);
        return SELF._options;
    };

    // display
    this.display = function() {
        var _go = function() {
            SELF._prepareData();
            SELF._display();
        };

        if (SELF._original_data && SELF._original_data.length) {
            _go();
        } else if (SELF._options.facebookAccessToken && SELF._options.facebookPageId) {
            if (FB) {
                SELF._loadFacebook(_go);
            }
        }
    };

    // add additional data into an existing timeline
    this.appendData = function(timeline_data) {
        var end_date = parseInt(SELF._data[SELF._data.length - 1].date.replace(/-/g, ''), 10);
        var new_data = [];

        if (SELF._options.order === 'desc') {
            $(timeline_data).each(function(index, data) {
                if (parseInt(data.date.replace(/-/g, ''), 10) <= end_date) {
                    new_data.push(data);
                }
            });
        } else {
            $(timeline_data).each(function(index, data) {
                if (parseInt(data.date.replace(/-/g, ''), 10) >= end_date) {
                    new_data.push(data);
                }
            });
        }

        SELF._data = SELF._data.concat(new_data);

        // render: create DOM
        SELF._render(new_data, true);

        if (SELF._options.animation) {
            SELF._startAnimation(SELF._processIframeQueue);
        } else {
            SELF._processIframeQueue();
        }
    };
}





function formatDateTime(datetype){
    date = new Date(datetype)     
	return date.toDateString();                      
}
function get_html_translation_table(table, quote_style) {
    var entities = {},
        hash_map = {},
        decimal;
    var constMappingTable = {},
        constMappingQuoteStyle = {};
    var useTable = {},
        useQuoteStyle = {};

    // Translate arguments
    constMappingTable[0] = 'HTML_SPECIALCHARS';
    constMappingTable[1] = 'HTML_ENTITIES';
    constMappingQuoteStyle[0] = 'ENT_NOQUOTES';
    constMappingQuoteStyle[2] = 'ENT_COMPAT';
    constMappingQuoteStyle[3] = 'ENT_QUOTES';

    useTable = !isNaN(table) ? constMappingTable[table] : table ? table.toUpperCase() : 'HTML_SPECIALCHARS';
    useQuoteStyle = !isNaN(quote_style) ? constMappingQuoteStyle[quote_style] : quote_style ? quote_style.toUpperCase() : 'ENT_COMPAT';

    if (useTable !== 'HTML_SPECIALCHARS' && useTable !== 'HTML_ENTITIES') {
        throw new Error("Table: " + useTable + ' not supported');
        // return false;
    }

    entities['38'] = '&amp;';
    if (useTable === 'HTML_ENTITIES') {
        entities['160'] = '&nbsp;';
        entities['161'] = '&iexcl;';
        entities['162'] = '&cent;';
        entities['163'] = '&pound;';
        entities['164'] = '&curren;';
        entities['165'] = '&yen;';
        entities['166'] = '&brvbar;';
        entities['167'] = '&sect;';
        entities['168'] = '&uml;';
        entities['169'] = '&copy;';
        entities['170'] = '&ordf;';
        entities['171'] = '&laquo;';
        entities['172'] = '&not;';
        entities['173'] = '&shy;';
        entities['174'] = '&reg;';
        entities['175'] = '&macr;';
        entities['176'] = '&deg;';
        entities['177'] = '&plusmn;';
        entities['178'] = '&sup2;';
        entities['179'] = '&sup3;';
        entities['180'] = '&acute;';
        entities['181'] = '&micro;';
        entities['182'] = '&para;';
        entities['183'] = '&middot;';
        entities['184'] = '&cedil;';
        entities['185'] = '&sup1;';
        entities['186'] = '&ordm;';
        entities['187'] = '&raquo;';
        entities['188'] = '&frac14;';
        entities['189'] = '&frac12;';
        entities['190'] = '&frac34;';
        entities['191'] = '&iquest;';
        entities['192'] = '&Agrave;';
        entities['193'] = '&Aacute;';
        entities['194'] = '&Acirc;';
        entities['195'] = '&Atilde;';
        entities['196'] = '&Auml;';
        entities['197'] = '&Aring;';
        entities['198'] = '&AElig;';
        entities['199'] = '&Ccedil;';
        entities['200'] = '&Egrave;';
        entities['201'] = '&Eacute;';
        entities['202'] = '&Ecirc;';
        entities['203'] = '&Euml;';
        entities['204'] = '&Igrave;';
        entities['205'] = '&Iacute;';
        entities['206'] = '&Icirc;';
        entities['207'] = '&Iuml;';
        entities['208'] = '&ETH;';
        entities['209'] = '&Ntilde;';
        entities['210'] = '&Ograve;';
        entities['211'] = '&Oacute;';
        entities['212'] = '&Ocirc;';
        entities['213'] = '&Otilde;';
        entities['214'] = '&Ouml;';
        entities['215'] = '&times;';
        entities['216'] = '&Oslash;';
        entities['217'] = '&Ugrave;';
        entities['218'] = '&Uacute;';
        entities['219'] = '&Ucirc;';
        entities['220'] = '&Uuml;';
        entities['221'] = '&Yacute;';
        entities['222'] = '&THORN;';
        entities['223'] = '&szlig;';
        entities['224'] = '&agrave;';
        entities['225'] = '&aacute;';
        entities['226'] = '&acirc;';
        entities['227'] = '&atilde;';
        entities['228'] = '&auml;';
        entities['229'] = '&aring;';
        entities['230'] = '&aelig;';
        entities['231'] = '&ccedil;';
        entities['232'] = '&egrave;';
        entities['233'] = '&eacute;';
        entities['234'] = '&ecirc;';
        entities['235'] = '&euml;';
        entities['236'] = '&igrave;';
        entities['237'] = '&iacute;';
        entities['238'] = '&icirc;';
        entities['239'] = '&iuml;';
        entities['240'] = '&eth;';
        entities['241'] = '&ntilde;';
        entities['242'] = '&ograve;';
        entities['243'] = '&oacute;';
        entities['244'] = '&ocirc;';
        entities['245'] = '&otilde;';
        entities['246'] = '&ouml;';
        entities['247'] = '&divide;';
        entities['248'] = '&oslash;';
        entities['249'] = '&ugrave;';
        entities['250'] = '&uacute;';
        entities['251'] = '&ucirc;';
        entities['252'] = '&uuml;';
        entities['253'] = '&yacute;';
        entities['254'] = '&thorn;';
        entities['255'] = '&yuml;';
    }

    if (useQuoteStyle !== 'ENT_NOQUOTES') {
        entities['34'] = '&quot;';
    }
    if (useQuoteStyle === 'ENT_QUOTES') {
        entities['39'] = '&#39;';
    }
    entities['60'] = '&lt;';
    entities['62'] = '&gt;';


    // ascii decimals to real symbols
    for (decimal in entities) {
        if (entities.hasOwnProperty(decimal)) {
            hash_map[String.fromCharCode(decimal)] = entities[decimal];
        }
    }

    return hash_map;
}
function html_entity_decode(string, quote_style) {
    var hash_map = {},
        symbol = '',
        tmp_str = '',
        entity = '';
    tmp_str = string.toString();

    if (false === (hash_map = this.get_html_translation_table('HTML_ENTITIES', quote_style))) {
        return false;
    }
    delete(hash_map['&']);
    hash_map['&'] = '&amp;';

    for (symbol in hash_map) {
        entity = hash_map[symbol];
        tmp_str = tmp_str.split(entity).join(symbol);
    }
    tmp_str = tmp_str.split('&#039;').join("'");

    return tmp_str;
}
<?php
  /**
   * Article Combobox
   *
   * @package VeriAsist
   * @author veriasist.com
   * @copyright 2014
   * @version $Id: main.php, v4.00 20114-05-10 10:12:05 gewa Exp $
   */
  if (!defined("_VALID_PHP"))
      die('Direct access to this location is not allowed.');

  require_once (MODPATH . "news/admin_class.php");
  $classname = 'News';
  try {
	  if (!class_exists($classname)) {
		  throw new exception('Missing news/admin_class.php');
	  }
	  Registry::set('News', new News(false));
  }
  catch (exception $e) {
	  echo $e->getMessage();
  }

  $bmlatest = Registry::get("News")->getLatestPluginArticles();
?>
<!-- Start Latest Article -->
<?php if($bmlatest):?>
<section class="veriasist stacked segment clearfix">
  <div class="veriasist-carousel" 
  data-pagination="true" 
  data-navigation="false" 
  data-slide-speed="200" 
  data-rewind-speed="100" 
  data-transition-style="fade" 
  data-single-item="true"
  >
    <?php foreach ($bmlatest as $bmlrow):?>
    <div class="item">
      <?php if($bmlrow->thumb):?>
      <a href="<?php echo doUrl(false, $bmlrow->slug, "news-item");?>"><img src="<?php echo SITEURL;?>/thumbmaker.php?src=<?php echo SITEURL.'/'.News::imagepath . $bmlrow->thumb;?>&amp;h=150&amp;w=400&amp;s=1&amp;a=tl" alt="" class="veriasist image"></a>
      <?php endif;?>
      <h4><a href="<?php echo doUrl(false, $bmlrow->slug, "news-item");?>"><?php echo $bmlrow->atitle;?></a></h4>
      <div class="description"> <small><i class="icon calendar"></i> <?php echo Filter::dodate("short_date", $bmlrow->created);?><br>
        <i class="icon sitemap"></i> <a href="<?php echo doUrl(false, $bmlrow->catslug, "news-cat");?>" class="inverted"><?php echo $bmlrow->catname;?></a><br>
        <i class="icon chat"></i> <?php echo $bmlrow->totalcomments;?> <?php echo Lang::$word->_MOD_AM_COMMENTS;?></small> </div>
    </div>
    <?php endforeach;?>
    <?php unset($bmlrow);?>
  </div>
</section>
<?php endif;?>
<!-- End Latest Article /-->
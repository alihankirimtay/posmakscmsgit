<?php
  /**
   * Online Users
   *
   * @package VeriAsist
   * @author veriasist.com
   * @copyright 2014
   * @version $Id: main.php, v4.00 2014-05-20 16:18:34 gewa Exp $
   */
  
  if (!defined("_VALID_PHP"))
      die('Direct access to this location is not allowed.');
?>
<div id="online-users" class="online clearfix">
  <div class="items"><img class="preloader" src="<?php echo SITEURL;?>/plugins/online/images/preloader.gif" alt="Loading.." /></div>
  <div class="counter"></div>
  <div class="title">online</div>
  <div class="button"><i class="icon angle up"></i></div>
</div>
<script type="text/javascript"> 
// <![CDATA[
$(document).ready(function () {
    var count = $('#online-users.online .counter');
    var panel = $('#online-users.online .items');
    var timeout;
    count.load(SITEURL + '/plugins/online/controller.php');

    $('#online-users.online').hover(
        function () {
            clearTimeout(timeout);
            timeout = setTimeout(function () {
                panel.trigger('open');
            }, 500);
        },
        function () {
            clearTimeout(timeout);
            timeout = setTimeout(function () {
                panel.trigger('close');
            }, 500);
        }
    );
    var loaded = false;
    panel.on('open', function () {
        panel.slideDown(function () {
            if (!loaded) {
                panel.load(SITEURL + '/plugins/online/geodata.php');
                loaded = true;
            }
        });
    }).on('close', function () {
        panel.slideUp();
    });
});
// ]]>
</script>
<?php
  /**
   * Class Url
   *
   * @package VeriAsist
   * @author veriasist.com
   * @copyright 2014
   * @version $Id: class_url.php, v1.00 2012-03-05 10:12:05 gewa Exp $
   */

  if (!defined("_VALID_PHP"))
      die('Direct access to this location is not allowed.');

  final class Url
  {
      public static $data = array();


      /**
       * Url::__construct()
       * 
       * @return
       */
      public function __construct()
      {
          self::$data = array(
            //Module directories. Only change Keys (Left Side)
            "moddir"    => array(
            "digishop"  => "digishop",
            "news"      => "news",
            "portfolio" => "portfolio",
            "projects"  => "projects",
            "solutions"  => "solutions",
            "booking"   => "booking",
            "psdrive"   => "psdrive",
            "forum"     => "forum",
            "career"     => "career",
				  ),

               //Pages
              "pagedata" => array(
				      "page"     => "page" // <- Change here
				  ),

               //Module. Only change Values (Right Side)
              "module" => array(
                  //Digishop
                  "digishop" => "digishop",
                  "digishop-cat" => "category",
                  "digishop-checkout" => "checkout",

                  //News
                  "news" => "news",
                  "news-cat" => "category",
                  "news-search" => "search",
                  "news-archive" => "archive",
                  "news-author" => "author",
                  "news-tag" => "tag",

                  //Portfolio
                  "portfolio" => "portfolio",
                  "portfolio-cat" => "category",

                  //Projects
                  "projects"     => "projects",
                  "projects-cat" => "category",

                  //Solutions
                  "solutions"     => "solutions",
                  "solutions-cat" => "category",

                  //Booking
                  "booking" => "booking",
				  
                  //Forum
                  "forum" => "forum",
                  "forum-topic" => "topic",
                  "forum-user" => "user",

                  //PS Drive
                  "psdrive" => "psdrive",
                  "psdrive-cat" => "category",
                  "psdrive-share" => "share",
                  "psdrive-tag" => "tag",
                  "booking" => "booking",

                  //Career
                  "career" => "career",
				  )
          );

          return self::$data;

      }

      /**
       * Url::Page()
       * 
       * @return
       */
      public static function Page($slug, $pars = false)
      {
          $segment = self::$data['pagedata'];
		  
		  $url = SITEURL . '/' . $segment['page'] . '/' . $slug . '/' . $pars;
		  
		  return $url;

      }
	  
      /**
       * Url::News()
       * 
       * @return
       */
      public static function News($type, $slug = false, $pars = false)
      {
          $segment = self::$data['module'];
          switch ($type) {
              case 'news':
                  $url = SITEURL . '/' . $segment['news'] . '/' . $pars;
                  break;

              case 'news-cat':
				  $url = SITEURL . '/' . $segment['news'] . '/' . $segment['news-cat'] . '/' . $slug . '/' . $pars;
                  break;

              case 'news-search':
				  $url = SITEURL . '/' . $segment['news'] . '/' . $segment['news-search'] . '/' . $pars;
                  break;

              case 'news-archive':
				  $url = SITEURL . '/' . $segment['news'] . '/' . $segment['news-archive'] . '/' . $pars;
                  break;

              case 'news-author':
				  $url = SITEURL . '/' . $segment['news'] . '/' . $segment['news-author'] . '/' . $slug . '/' . $pars;
                  break;

              case 'news-tags':
				  $url = SITEURL . '/' . $segment['news'] . '/' . $segment['news-tag'] . '/' . urlencode($slug) . '/' . $pars;
                  break;

              default:
				  $url = SITEURL . '/' . $segment['news'] . '/' . $slug . '/' . $pars;
                  break;

          }
		  
		  return $url;

      }
	  
      /**
       * Url::Digishop()
       * 
       * @return
       */
      public static function Digishop($type, $slug = false, $pars = false)
      {
          $segment = self::$data['module'];
          switch ($type) {
              case 'digishop':
                  $url = SITEURL . '/' . $segment['digishop'] . '/' . $pars;
                  break;

              case 'digishop-cat':
				  $url = SITEURL . '/' . $segment['digishop'] . '/' . $segment['digishop-cat'] . '/' . $slug . '/' . $pars;
                  break;

              case 'digishop-checkout':
				  $url = SITEURL . '/' . $segment['digishop'] . '/' . $segment['digishop-checkout'] . '/' . $pars;
                  break;

              default:
				  $url = SITEURL . '/' . $segment['digishop'] . '/' . $slug . '/' . $pars;
                  break;

          }
		  
		  return $url;

      }
	  
      /**
       * Url::Portfolio()
       * 
       * @return
       */
      public static function Portfolio($type, $slug = false, $pars = false)
      {
          $segment = self::$data['module'];
          switch ($type) {
              case 'portfolio':
                  $url = SITEURL . '/' . $segment['portfolio'] . '/' . $pars;
                  break;

              case 'portfolio-cat':
				  $url = SITEURL . '/' . $segment['portfolio'] . '/' . $segment['portfolio-cat'] . '/' . $slug . '/' . $pars;
                  break;

              default:
				  $url = SITEURL . '/' . $segment['portfolio'] . '/' . $slug . '/' . $pars;
                  break;

          }
		  
		  return $url;

      }


      /**
       * Url::Projects()
       * 
       * @return
       */
      public static function Projects($type, $slug = false, $pars = false)
      {
          $segment = self::$data['module'];
          switch ($type) {
              case 'projects':
                  $url = SITEURL . '/' . $segment['projects'] . '/' . $pars;
                  break;

              case 'projects-cat':
          $url = SITEURL . '/' . $segment['projects'] . '/' . $segment['projects-cat'] . '/' . $slug . '/' . $pars;
                  break;

              default:
          $url = SITEURL . '/' . $segment['projects'] . '/' . $slug . '/' . $pars;
                  break;

          }
      
      return $url;

      }

      /**
       * Url::Solutions()
       * 
       * @return
       */
      public static function Solutions($type, $slug = false, $pars = false)
      {
          $segment = self::$data['module'];
          switch ($type) {
              case 'solutions':
                  $url = SITEURL . '/' . $segment['solutions'] . '/' . $pars;
                  break;

              case 'solutions-cat':
          $url = SITEURL . '/' . $segment['solutions'] . '/' . $segment['solutions-cat'] . '/' . $slug . '/' . $pars;
                  break;

              default:
          $url = SITEURL . '/' . $segment['solutions'] . '/' . $slug . '/' . $pars;
                  break;

          }
      
      return $url;

      }
	  
      /**
       * Url::Booking()
       * 
       * @return
       */
      public static function Booking($type, $slug = false, $pars = false)
      {
          $segment = self::$data['module'];
          switch ($type) {
              case 'booking':
                  $url = SITEURL . '/' . $segment['booking'] . '/' . $pars;
                  break;
				  
              default:
				  $url = SITEURL . '/' . $segment['booking'] . '/' . $slug . '/' . $pars;
                  break;

          }
		  
		  return $url;

      }

      /**
       * Url::Career()
       * 
       * @return
       */
      public static function Career($type, $slug = false, $pars = false)
      {
          $segment = self::$data['module'];
          switch ($type) {
              case 'careet':
                  $url = SITEURL . '/' . $segment['careet'] . '/' . $pars;
                  break;
          
              default:
          $url = SITEURL . '/' . $segment['careet'] . '/' . $slug . '/' . $pars;
                  break;

          }
      
      return $url;

      }
	  
	  
      /**
       * Url::Forum()
       * 
       * @return
       */
      public static function Forum($type, $slug = false, $pars = false)
      {
          $segment = self::$data['module'];
          switch ($type) {
              case 'forum':
                  $url = SITEURL . '/' . $segment['forum'] . '/' . $pars;
                  break;

              case 'forum-topic':
				  $url = SITEURL . '/' . $segment['forum'] . '/' . $segment['forum-topic'] . '/' . $slug . '/' . $pars;
                  break;

              case 'forum-user':
				  $url = SITEURL . '/' . $segment['forum'] . '/' . $segment['forum-user'] . '/' . $pars;
                  break;

              default:
				  $url = SITEURL . '/' . $segment['forum'] . '/' . $slug . '/' . $pars;
                  break;

          }
		  
		  return $url;

      }
	  
      /**
       * Url::Psdrive()
       * 
       * @return
       */
      public static function Psdrive($type, $slug = false, $pars = false)
      {
          $segment = self::$data['module'];
          switch ($type) {
              case 'psdrive':
                  $url = SITEURL . '/' . $segment['psdrive'] . '/' . $pars;
                  break;

              case 'psdrive-cat':
				  $url = SITEURL . '/' . $segment['psdrive'] . '/' . $segment['psdrive-cat'] . '/' . $slug . '/' . $pars;
                  break;

              case 'psdrive-search':
				  $url = SITEURL . '/' . $segment['psdrive'] . '/' . $segment['psdrive-search'] . '/' . $pars;
                  break;

              case 'psdrive-share':
				  $url = SITEURL . '/' . $segment['psdrive'] . '/' . $segment['psdrive-share'] . '/' . $pars;
                  break;
				  
              case 'psdrive-tags':
				  $url = SITEURL . '/' . $segment['psdrive'] . '/' . $segment['psdrive-tag'] . '/' . urlencode($slug) . '/' . $pars;
                  break;

              default:
				  $url = SITEURL . '/' . $segment['psdrive'] . '/' . $slug . '/' . $pars;
                  break;

          }
		  
		  return $url;

      }
  }
<?php
  /**
   * Right Sidebar Layout
   *
   * @package VeriAsist
   * @author veriasist.com
   * @copyright 2014
   * @version $Id: right.tpl.php, v4.00 2014-04-20 10:12:05 gewa Exp $
   */
  if (!defined("_VALID_PHP"))
      die('Direct access to this location is not allowed.');
?>
<div class="veriasist-content-full clearfix">
  <?php if($content->getAccess()):?>
  <?php echo Content::getContentPlugins($row->{'body' . Lang::$lang});?>
  <?php if ($page = Content::getAccesPages($row)):?>
  <?php include($page);?>
  <?php endif;?>
  <?php if($row->module_name and $modfile = Content::getModuleTheme($row->module_name)) :?>
  <?php require($modfile); ?>
  <?php endif;?>
  <?php if ($content->jscode) echo cleanOut($content->jscode);?>
  <?php endif;?>
</div>
<?php  if (!defined("_VALID_PHP")) die('Direct access to this location is not allowed.'); ?>
      <!-- footer -->
      <footer class="wow fadeIn cover-background" style="background-image:url('<?php echo THEMEURL;?>/images/image-footer.jpg');">

        <div class="footer-bottom no-padding position-relative">
          <div class="container">
            <!-- footer logo -->
            <div class="row border-top border-transperent-white-light padding-four no-padding-lr xs-padding-twenty-three xs-no-padding-lr">
              <div class="col-md-12 col-sm-12 text-center margin-one no-margin-top no-margin-lr">
                <a class="inner-link" href="<?= SITEURL ?>"><img src="<?= SITEURL ?>/uploads/<?= ($core->logo_footer) ? $core->logo_footer : $core->logo ?>" alt="<?= $core->company ?>"/></a>
              </div>
              <div class="col-md-12 col-sm-12 text-center">
                <span class="text-small text-uppercase letter-spacing-1 light-gray-text alt-font">Copyright &COPY; <?= date('Y') ?> All Rights Reserved. | Powered by: <a href="<?= SITEURL ?>/" target="_blank"><?= $core->site_name ?></a></span>
              </div>
            </div>
            <!-- end footer logo -->
          </div>
        </div>
      </footer>
      <!-- end footer -->
      <!-- scroll to top -->
      <a href="javascript:;" class="scrollToTop"><i class="fa fa-angle-up"></i></a>
      <!-- end scroll to top -->
      
      <!-- javascript libraries -->
      <script src="<?php echo THEMEURL;?>/js/modernizr.js"></script>
      <script src="<?php echo THEMEURL;?>/js/bootstrap.min.js"></script>
      <script src="<?php echo THEMEURL;?>/js/jquery.easing.1.3.js"></script>
      <script src="<?php echo THEMEURL;?>/js/skrollr.min.js"></script>
      <script src="<?php echo THEMEURL;?>/js/smooth-scroll.js"></script>
      <script src="<?php echo THEMEURL;?>/js/jquery.appear.js"></script>
      <!-- menu navigation -->
      <script type="text/javascript" src="<?php echo THEMEURL;?>/js/jquery.nav.js"></script>
      <!-- animation -->
      <script type="text/javascript" src="<?php echo THEMEURL;?>/js/wow.min.js"></script>
      <!-- page scroll -->
      <script type="text/javascript" src="<?php echo THEMEURL;?>/js/page-scroll.js"></script>
      <!-- owl carousel -->
      <script type="text/javascript" src="<?php echo THEMEURL;?>/js/owl.carousel.min.js"></script>
      <!-- counter -->
      <script type="text/javascript" src="<?php echo THEMEURL;?>/js/jquery.countTo.js"></script>
      <!-- parallax -->
      <script type="text/javascript" src="<?php echo THEMEURL;?>/js/jquery.parallax-1.1.3.js"></script>
      <!-- magnific popup -->
      <script type="text/javascript" src="<?php echo THEMEURL;?>/js/jquery.magnific-popup.min.js"></script>
      <!-- portfolio with shorting tab -->
      <script type="text/javascript" src="<?php echo THEMEURL;?>/js/isotope.pkgd.min.js"></script>
      <!-- images loaded -->
      <script type="text/javascript" src="<?php echo THEMEURL;?>/js/imagesloaded.pkgd.min.js"></script>
      <!-- pull menu -->
      <script type="text/javascript" src="<?php echo THEMEURL;?>/js/classie.js"></script>
      <!-- counter  -->
      <script type="text/javascript" src="<?php echo THEMEURL;?>/js/counter.js"></script>
      <!-- fit video  -->
      <script type="text/javascript" src="<?php echo THEMEURL;?>/js/jquery.fitvids.js"></script>
      <!-- setting -->
      <script type="text/javascript" src="<?php echo THEMEURL;?>/js/main.js"></script>

      <script src="<?php echo SITEURL;?>/assets/global.js"></script>
      <script src="<?php echo THEMEURL;?>/master.js"></script>


      <!-- Google Analytics -->
      <?php if($core->analytics):?>
        <?= cleanOut($core->analytics) ?> 
      <?php endif ?>
      <!-- Google Analytics -->
	  
	  

	  
    </body>
</html>
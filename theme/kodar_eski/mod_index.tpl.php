<?php
  /**
   * Module Index
   *
   * @package VeriAsist
   * @author veriasist.com
   * @copyright 2014
   * @version $Id: mod_index.php, v4.00 2014-04-20 10:12:05 gewa Exp $
   */
  
  if (!defined("_VALID_PHP"))
      die('Direct access to this location is not allowed.');
?>
<?php include("header.tpl.php");?>


<div class="container">
  <div class="row subheader">

    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 hidden">
      <div class="page-header title-thick-underline border-color-fast-yellow display-inline-block letter-spacing-2 margin-six no-margin-lr no-margin-top no-margin-bottom">
        <h1>
          <?php echo $content->moduledata->{'title' . Lang::$lang};?>
          <?php if($content->moduledata->{'info' . Lang::$lang}):?>
            <small><?php echo $content->moduledata->{'info' . Lang::$lang};?></small>
          <?php  endif;?>
        </h1>
      </div>
    </div>

    <?php  if($content->modalias and $core->showcrumbs): ?>
      <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 hidden">
        <ol class="breadcrumb text-right">
          <li>
            <a href="<?php  echo SITEURL;?>/"><?php  echo Lang::$word->_HOME;?></a>
          </li>
          <?php echo $content->getBreadcrumbs();?>
        </ol>
      </div>
    <?php  endif;?>
    
  </div>
</div>

<?php if ($widgettop): ?>
<!-- Top Widgets -->
<div id="topwidget">
  <?php include(THEMEDIR . "/top_widget.tpl.php");?>
</div>
<!-- Top Widgets /-->
<?php endif;?>


<?php switch(true): case $widgetleft and $widgetright :?>
<!-- Left and Right Layout -->
<div id="page">
  <div class="veriasist-grid">
    <div class="columns">
      <div class="screen-60 tablet-50 phone-100">
        <?php include(THEMEDIR . "/mod_main.tpl.php");?>
      </div>
      <div class="screen-20 tablet-25 phone-100">
        <?php include(THEMEDIR . "/left_widget.tpl.php");?>
      </div>
      <div class="screen-20 tablet-25 phone-100">
        <?php include(THEMEDIR . "/right_widget.tpl.php");?>
      </div>
    </div>
  </div>
</div>
<!-- Left and Right Layout /-->
<?php break;?>
<?php case $widgetleft :?>
<!-- Left Layout -->
<div id="page">
  <div class="veriasist-grid">
    <div class="columns">
      <div class="screen-30 tablet-40 phone-100">
        <?php include(THEMEDIR . "/left_widget.tpl.php");?>
      </div>
      <div class="screen-70 tablet-60 phone-100">
        <?php include(THEMEDIR . "/mod_main.tpl.php");?>
      </div>
    </div>
  </div>
</div>
<!-- Left Layout /-->
<?php break;?>
<?php case $widgetright :?>
<!-- Right Layout -->
<div id="page">
  <div class="veriasist-grid">
    <div class="columns">
      <div class="screen-70 tablet-60 phone-100">
        <?php include(THEMEDIR . "/mod_main.tpl.php");?>
      </div>
      <div class="screen-30 tablet-40 phone-100">
        <?php include(THEMEDIR . "/right_widget.tpl.php");?>
      </div>
    </div>
  </div>
</div>
<!-- Right Layout /-->
<?php break;?>


<?php default:?>
<!-- Full Layout -->
    <?php include(THEMEDIR . "/mod_main.tpl.php");?>
<!-- Full Layout /-->
<?php break;?>


<?php endswitch;?>
<?php if ($widgetbottom): ?>
<!-- Bottom Widgets -->
<div id="botwidget">
  <div class="veriasist-grid">
    <?php include(THEMEDIR . "/bottom_widget.tpl.php");?>
  </div>
</div>
<!-- Bottom Widgets /-->
<?php endif;?>
<?php include("footer.tpl.php");?>
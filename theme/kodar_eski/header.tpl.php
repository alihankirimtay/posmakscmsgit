<?php

  
  if (!defined("_VALID_PHP"))
      die('Direct access to this location is not allowed.');
?>
<!doctype html>
<html lang="<?php echo str_replace('_', '', Lang::$lang); ?>">
<head>
<?php echo $content->getMeta(); ?>
<!-- favicon -->
<link rel="shortcut icon" href="<?=SITEURL?>/assets/favicon.png">
<link rel="apple-touch-icon" href="<?=SITEURL?>/assets/apple-touch-icon-57x57.png">
<link rel="apple-touch-icon" sizes="72x72" href="<?=SITEURL?>/assets/apple-touch-icon-72x72.png">
<link rel="apple-touch-icon" sizes="114x114" href="<?=SITEURL?>/assets/apple-touch-icon-114x114.png">
<!-- favicon -->
<script type="text/javascript">
var SITEURL = "<?php echo SITEURL; ?>";
</script>
<?php $cssS = array( 'css/animate.css', 'css/bootstrap.min.css', 'css/et-line-icons.css', 'css/font-awesome.min.css', 'css/owl.transitions.css', 'css/owl.carousel.css', 'css/magnific-popup.css' ); ?>
<?php foreach ($cssS as $css): ?>
<link rel="stylesheet" href="<?php echo THEMEURL;?>/<?php echo $css; ?>">
<?php endforeach ?>
<?php Content::getThemeStyle();?>
<link rel="stylesheet" href="<?php echo THEMEURL;?>/css/responsive.css">
<link rel="stylesheet" href="<?php echo THEMEURL;?>/css/kodar.css">
<script src="<?php echo THEMEURL;?>/js/jquery.min.js"></script>


<?php $content->getPluginAssets(@$assets); //@ işareti 404 sayfası için eklendi. kaldırmayın.?>
<?php $content->getModuleAssets();?>
<?php if($core->eucookie):?>
<script type="text/javascript" src="<?php echo SITEURL;?>/assets/eu_cookies.js"></script>
<script type="text/javascript"> 
$(document).ready(function () {
    $("body").acceptCookies({
        position: 'top',
        notice: '<?php echo Lang::$word->_EU_NOTICE;?>',
        accept: '<?php echo Lang::$word->_EU_ACCEPT;?>',
        decline: '<?php echo Lang::$word->_EU_DECLINE;?>',
        decline_t: '<?php echo Lang::$word->_EU_DECLINE_T;?>',
        whatc: '<?php echo Lang::$word->_EU_W_COOKIES;?>'
    })
});
</script>
<?php endif;?>
</head>
<body class="animated" <?php if($core->bgimg): ?>style="background:url('<?php echo SITEURL . '/theme/' . $core->theme . '/images/' . $core->bgimg; ?>') top #F2F2F2 no-repeat;" <?php endif; ?>>

  <!-- kodar navigasyon -->


  <div class="navbea-top">
  <div class="container navigation-menu">
  
  <div class="header-pos">
  <p class="daktilo">Akıllı Restaurant Otomasyon Sistemi</p>
  </div>
  
		  <a class="item" href="#modal-demo_request" class="mpf-popup-form btn-success btn btn-medium button btn-round" data-toggle="modal"><li class="login-navbea btn btn-medium button letter-spacing-1 inner-link" style="height: 40px;"><?php echo Lang::$word->DEMO_REQ_TITLE; ?></li></a>
		  
		  </div>
		  
		  
  </div>



<div class="modal fade in" id="modal-demo_request">

  <div class="modal-dialog">
  <div class="col-md-12 col-sm-12 col-xs-12 no-padding bg-white">
  
  	<div class="col-md-6 col-sm-6 col-xs-12 margin-four no-margin-lr no-margin-top" style="padding:0px !important;">
	<img src="'.SITEURL.'/uploads/restourant-otomasyon-sistemi-demo.jpg" />
	<a class="item" href="#modal-demo_request" data-toggle="modal"><li class="login-navbea btn btn-medium button letter-spacing-1 inner-link" style="height: 100px; width: 100%; padding-top:33px; font-size:20px;">2 ADIMDA KURULUM REHBERİ</li></a>
	</div>
    <div class="modal-content">
	
	<div class="col-md-6 col-sm-6 col-xs-12 margin-four no-margin-lr no-margin-top">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title text-center">DEMO BAŞVURUSU İÇİN LÜTFEN AŞAĞIDAKİ FORMU DOLDURUN</h4>
      </div>
      <div class="modal-body">

        <form role="form" id="form-demo_request">
          
          <div class="form-group">
            <label for="">Erişim Adresi (<span style="color:#b9121b;">sizinadresiniz</span>.posmaks.com)</label>
            <input name="folder" type="text" class="form-control" placeholder="Erişim Adresi" required>
          </div>

          <div class="form-group">
            <label for="">Adınız Soyadınız</label>
            <input name="name" type="text" class="form-control" placeholder="Adınız Soyadınız" required>
          </div>

          <div class="form-group">
            <label for="">Şirket İsmi</label>
            <input name="company" type="text" class="form-control" placeholder="Şirket İsmi">
          </div>

          <div class="form-group">
            <label for="">Telefon Numarası</label>
            <input name="phone" type="text" class="form-control" placeholder="Telefon Numarası">
          </div>

          <div class="form-group">
            <label for="">E-posta Adresi</label>
            <input name="email" type="mail" class="form-control" placeholder="E-posta Adresi">
          </div>
                    
        </form>
      </div>
      <div class="modal-footer">
        <div id="msgholder_demo_req" class="padding-one text-left"></div>
        <button type="button" class="btn btn-default"  style="padding: 24px 34px; font-size:20px;" data-dismiss="modal">VAZGEÇ</button>
        <button type="button" class="btn btn-primary" style="padding: 24px 34px; background-color: #389b1f; font-size:20px; margin-right: 0px;" id="btn-demo_request">BAŞLA</button>
      </div>
	  
	  </div></div>
    </div>
  </div>
</div>



  <div class="navikodar">
    
  <nav class="navbar no-margin-bottom no-border navbar-height alt-font">
  
  
  
    <div class="container navigation-menu">

      <div class="row">

        <div class="col-lg-3 col-md-3">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand inner-link" href="<?php echo SITEURL;?>/"><?php echo ($core->logo) ? '<img src="'.SITEURL.'/uploads/'.$core->logo.'" alt="'.$core->company.'" />': $core->company;?></a>
        </div>
		
		
        <div class="col-lg-6 col-md-6 col-sm-9 collapse navbar-collapse kodarcrativenav" id="bs-example-navbar-collapse-1">
          <ul class="nav navbar-nav">
            <?php $mainmenu = $content->getMenuList(); $content->getMenu1($mainmenu);?>
          </ul>
		  
        </div>

        
        
		
		
		
		
		
		
       <!--  <?php if($core->show_lang):?>
          <div class="col-lg-3 col-md-2 pull-right header-right text-right sm-display-none" id="langmenu">
              <?php foreach($core->langList() as $lang):?>
                  <?php $active_lang_class =  (Core::$language != $lang->flag) ? ' highlight-button-gray-border' : null; ?>
                  <a class="btn-small-white btn btn-very-small no-margin inner-link <?php echo $active_lang_class; ?>" href="javascript:;" data-lang="<?php echo $lang->flag;?>" data-text="<?php echo $lang->name;?>" ><?php echo $lang->name;?></a>
              <?php endforeach;?>
          </div>
        <?php endif;?> -->

		
        
      </div>
    </div>
  </nav></div>

  <!-- end navigation -->
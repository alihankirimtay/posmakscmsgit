<?php
  /**
   * Index
   *
   * @package VeriAsist
   * @author veriasist.com
   * @copyright 2014
   * @version $Id: index.php, v4.00 2014-04-20 10:12:05 gewa Exp $
   */
  if (!defined("_VALID_PHP"))
      die('Direct access to this location is not allowed.');
?>
<?php include("header.tpl.php");?>

<?php if(!$row->home_page):?>


  <!-- page title -->
  <section id="page-title" class="page-title parallax2 parallax-fix wow fadeIn">
    <?php if (preg_match('/^.*\.(mp4)$/', $row->custom_bg)): ?>
      <div class="video-wrapper position-top z-index-0">
        <video autoplay="" muted="" loop="" class="html-video">
          <source type="video/mp4" src="<?php echo SITEURL.'/uploads/images/pages/video.mp4';?>">
        </video>
      </div>
      <?php else: ?>
        <img class="parallax-background-img" src="<?php echo ($row->custom_bg) 
          ? SITEURL.'/uploads/'.$row->custom_bg 
          : SITEURL.'/uploads/images/subheaders/'.(str_replace('theme/', '', CTHEME).'/bg.jpg'); ?>" alt=""
        />
      <?php endif; ?>
      <div class="opacity-full bg-deep-blue3"></div>
      <div class="container position-relative">
        <div class="row">
          <div class="col-md-12 col-sm-12 text-center">
            <h2 class="alt-font white-text font-weight-600 xs-title-extra-large"><?php echo $row->{'title' . Lang::$lang};?></h2>
            <?php if($row->{'caption' . Lang::$lang}):?>
              <span class="alt-font title-small xs-text-large white-text text-uppercase margin-one no-margin-bottom no-margin-lr display-block"><?php echo $row->{'caption' . Lang::$lang};?></span>
            <?php endif;?>
          </div>
        </div>
      </div>
    </section>
    <!-- end page title -->


  <div class="breadcrumb alt-font no-margin-bottom bg-white">
    <div class="container">
      <div class="col-sm-9">
        <ul>
          <li><a href="<?php  echo SITEURL;?>/"><?php  echo Lang::$word->_HOME;?></a></li>
          <li><?php echo $content->getBreadcrumbs();?></li>
        </ul> 
      </div>
      <div class="col-sm-3">
        <!-- Livesearch Start -->
        <?php if($core->showsearch):?>
          <div class="widget margin-one no-margin-lr no-margin-top xs-margin-twelve xs-no-margin-lr xs-no-margin-top">
            <form action="<?php echo Url::Page($core->search_page);?>" method="POST" id="livesearch" name="search-form">
              <i class="fa fa-search close-search search-button"></i>
              <input type="text" class="alt-font news-search-btn" id="searchfield" name="keywords" placeholder="<?php echo Lang::$word->_SEARCH;?>..." autocomplete="off">
            </form>
          </div>
        <?php endif;?>
        <!-- Livesearch Start END --> 
      </div>
    </div>
  </div>
<?php endif; ?>




<?php if ($widgettop): ?>
<!-- Top Widgets -->
<!-- <div class="container"> -->
  <!-- <div class="row"> -->
    <!-- <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"> -->
      <?php include(THEMEDIR . "/top_widget.tpl.php");?>
    <!-- </div> -->
  <!-- </div> -->
<!-- </div> -->
<!-- Top Widgets /-->
<?php endif;?>
<?php switch(true): case $widgetleft and $widgetright :?>
<!-- Left and Right Layout -->
<div class="container">
  <div class="row">
    <div class="col-lg-8 col-md-6 col-sm-12 col-xs-12">
      <?php include(THEMEDIR . "/main.tpl.php");?>
    </div>
    <div class="col-lg-2 col-md-3 col-sm-12 col-xs-12">
      <?php include(THEMEDIR . "/left_widget.tpl.php");?>
    </div>
    <div class="col-lg-2 col-md-3 col-sm-12 col-xs-12">
      <?php include(THEMEDIR . "/right_widget.tpl.php");?>
    </div>
  </div>
</div>  
<!-- Left and Right Layout /-->
<?php break;?>
<?php case $widgetleft :?>
<!-- Left Layout -->
<div class="container">
  <div class="row">
    <div class="col-lg-4 col-md-5 col-sm-12 col-xs-12">
      <?php include(THEMEDIR . "/left_widget.tpl.php");?>
    </div>
    <div class="col-lg-8 col-md-7 col-sm-12 col-xs-12">
      <?php include(THEMEDIR . "/main.tpl.php");?>
    </div>
  </div>
</div> 
<!-- Left Layout /-->
<?php break;?>
<?php case $widgetright :?>
<!-- Right Layout -->
<div class="container">
  <div class="row">
    <div class="col-lg-8 col-md-7 col-sm-12 col-xs-12">
      <?php include(THEMEDIR . "/main.tpl.php");?>
    </div>
    <div class="col-lg-4 col-md-5 col-sm-12 col-xs-12">
      <?php include(THEMEDIR . "/right_widget.tpl.php");?>
    </div>
  </div>
</div> 
<!-- Right Layout /-->
<?php break;?>
<?php default:?>
<!-- Full Layout -->
<!-- <div class="container"> -->
  <!-- <div class="row"> -->
    <!-- <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12"> -->
      <?php include(THEMEDIR . "/full.tpl.php");?>
    <!-- </div> -->
  <!-- </div> -->
<!-- </div>  -->
<!-- Full Layout /-->
<?php break;?>
<?php endswitch;?>
<?php if ($widgetbottom): ?>
<!-- Bottom Widgets -->
<!-- <div class="container"> -->
  <!-- <div class="row"> -->
    <!-- <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12"> -->
     <?php include(THEMEDIR . "/bottom_widget.tpl.php");?>
    <!-- </div> -->
 <!-- </div> -->
<!-- </div>  -->
<!-- Bottom Widgets /-->
<?php endif;?>

<?php include("footer.tpl.php");?>
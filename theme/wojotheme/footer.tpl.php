<?php
  /**
   * Footer
   *
   * @package VeriAsist
   * @author veriasist.com
   * @copyright 2014
   * @version $Id: footer.php, v4.00 4011-04-20 10:12:05 gewa Exp $
   */
  if (!defined("_VALID_PHP"))
      die('Direct access to this location is not allowed.');
?>
<!-- Footer -->
<footer>
  <div class="veriasist-grid">
    <div class="columns">
      <div class="screen-50 phone-100 push-center">
        <div class="ficons"> <a href="<?php echo SITEURL;?>/" data-content="<?php echo Lang::$word->_HOME;?>"><i class="circular white large inverted icon home link"></i></a> <a href="http://validator.w3.org/check/referer" target="_blank" data-content="Our website is valid HTML5"><i class="circular white large inverted icon html5 link"></i></a> <a href="<?php echo SITEURL;?>/rss.php" data-content="Rss"><i class="circular white large inverted icon rss link"></i></a> <a href="<?php echo Url::Page($core->sitemap_page);?>" data-content="<?php echo Lang::$word->_SM_SITE_MAP;?>"><i class="circular white large inverted icon map link"></i></a> </div>
        <div class="copyright">
          <p>Copyright &copy;<?php echo date('Y').' <a href="'.SITEURL.'/" class="inverted">'.$core->site_name.'</a>';?> All Rights Reserved.</p>
          <p>Powered by: VeriAsist! v <?php echo $core->version;?></p>
        </div>
        <div class="logo"><a href="<?php echo SITEURL;?>"><?php echo ($core->logo) ? '<img src="'.SITEURL.'/uploads/'.$core->logo.'" alt="'.$core->company.'" />': $core->company;?></a></div>
      </div>
    </div>
  </div>
</footer>
<!-- Footer /-->
<script src="<?php echo THEMEURL;?>/wow.min.js"></script> 
<script type="text/javascript"> 
  var wow = new WOW({
	  boxClass: 'wow', // animated element css class (default is wow)
	  animateClass: 'animated', // animation css class (default is animated)
	  offset: 0, // distance to the element when triggering the animation (default is 0)
	  mobile: false // trigger animations on mobile devices (true is default)
  });
  wow.init();
</script> 
<?php if($core->analytics):?>
<!-- Google Analytics --> 
<?php echo cleanOut($core->analytics);?> 
<!-- Google Analytics /-->
<?php endif;?>
</body></html>
<?php
  /**
   * 404 Template
   *
   * @package VeriAsist
   * @author veriasist.com
   * @copyright 2014
   * @version $Id: 404.tpl.php, v4.00 2014-04-20 10:12:05 gewa Exp $
   */
  if (!defined("_VALID_PHP"))
      die('Direct access to this location is not allowed.');
  // pr(get_required_files());
?>
<?php require_once ("header.tpl.php");?>

<section class="no-padding work-3col wow fadeIn ">
  <div class="container">

    <div class="row margin-thirty-five">
      <div class="col-md-12 col-sm-12 text-center">
        <h2 class="title-large alt-font text-uppercase letter-spacing-2 white-text display-block margin-one no-margin-top"><?php echo Lang::$word->_ER_404;?></h2>
        <h2 class="title-small alt-font text-uppercase letter-spacing-2 white-text display-block margin-one no-margin-top"><?php echo Lang::$word->_ER_404_1;?></h2>
        <h2 class="title-small alt-font text-uppercase letter-spacing-2 white-text display-block margin-one no-margin-top"><?php echo Lang::$word->_ER_404_2;?></h2>
        <div class="no-margin-top"><a href="<?php echo SITEURL;?>"><?php echo ($core->logo) ? '<img src="'.SITEURL.'/uploads/'.$core->logo.'" alt="'.$core->company.'" />': $core->company;?></a></div>
      </div>
    </div>

  </div>
</section>

<?php require_once ("footer.tpl.php");?>
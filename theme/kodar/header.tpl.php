<?php if (!defined("_VALID_PHP")) die('Direct access to this location is not allowed.'); ?>

<!doctype html>
<html lang="<?php echo str_replace('_', '', Lang::$lang); ?>">

<head>
<?php 
//Google Adword Dönüşüm Kodu
  $google_adwords_code = "AW-1040661918"
 ?>
  <!-- Global site tag (gtag.js) - Google Ads: 1040661918 -->
  <script async src="https://www.googletagmanager.com/gtag/js?id=<?=$google_adwords_code?>"></script> 
  <script> 
    window.dataLayer = window.dataLayer || []; function gtag(){dataLayer.push(arguments);} gtag('js', new Date()); gtag('config', '<?= $google_adwords_code ?>'); 
  </script> 

  <!-- Meta Tags -->
  <?php echo $content->getMeta(); ?>

  <!-- Favicon -->
  <link rel="shortcut icon" href="<?=SITEURL?>/assets/favicon.png">
  <link rel="apple-touch-icon" href="<?=SITEURL?>/assets/apple-touch-icon-57x57.png">
  <link rel="apple-touch-icon" sizes="72x72" href="<?=SITEURL?>/assets/apple-touch-icon-72x72.png">
  <link rel="apple-touch-icon" sizes="114x114" href="<?=SITEURL?>/assets/apple-touch-icon-114x114.png">

  <!-- Base URL JS -->
  <script type="text/javascript">
    var SITEURL = "<?php echo SITEURL; ?>";
    var login = false;
    var google_adwords_code = "<?=$google_adwords_code?>";
  </script>

  <!-- All Style Files Array -->
  <?php 
  $css = [
    'css/theme-styles.css',
    'css/blocks.css',
    'css/count.css',
    'css/swiper.min.css',
    'css/widgets.css',
    'css/style.css',
  ];
  ?>

  <!-- Fonts -->
  
  <link href="https://fonts.googleapis.com/css?family=Nunito:300,400,700,900" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Baloo+Paaji" rel="stylesheet">



  <!-- Foreach Style Files -->
  <?php foreach($css as $c): ?>
    <link rel="stylesheet" href="<?php echo THEMEURL;?>/<?php echo $c; ?>">
  <?php endforeach; ?>

  <!-- Jquery -->

  <script src="<?php echo THEMEURL;?>/js/jquery-3.2.0.min.js"></script>
  <script src="https://www.google.com/recaptcha/api.js?hl=tr" async defer></script>

  <?php $content->getPluginAssets(@$assets); //@ işareti 404 sayfası için eklendi. kaldırmayın.?>
  <?php $content->getModuleAssets();?>

  <?php if($core->eucookie):?>

    <script type="text/javascript" src="<?php echo SITEURL;?>/assets/eu_cookies.js"></script>

    <script type="text/javascript"> 
      $(document).ready(function () {
        $("body").acceptCookies({
          position: 'top',
          notice: '<?php echo Lang::$word->_EU_NOTICE;?>',
          accept: '<?php echo Lang::$word->_EU_ACCEPT;?>',
          decline: '<?php echo Lang::$word->_EU_DECLINE;?>',
          decline_t: '<?php echo Lang::$word->_EU_DECLINE_T;?>',
          whatc: '<?php echo Lang::$word->_EU_W_COOKIES;?>'
        })
      });
    </script>

  <?php endif;?>

</head>

<body class="crumina-grid">

  <header class="header header--menu-rounded animated headroom--not-bottom slideDown" id="site-header">

    <!-- Header Top Lines -->
    <div class="header-lines-decoration">
      <span class="bg-secondary-color"></span>
      <span class="bg-blue"></span>
      <span class="bg-blue-light"></span>
      <span class="bg-orange-light"></span>
      <span class="bg-red"></span>
      <span class="bg-green"></span>
      <span class="bg-secondary-color"></span>
    </div>


    <!-- Header Container -->
    <div class="container">

      <a href="#" id="top-bar-js" class="top-bar-link">
        <svg class="utouch-icon utouch-icon-arrow-top">
          <use xlink:href="#utouch-icon-arrow-top"></use>
        </svg>
      </a>

      <div class="header-content-wrapper">

        <!-- Logo -->
        <div class="site-logo">
          <a href="<?= SITEURL; ?>" class="full-block"></a>
          <img src="<?php echo THEMEURL;?>/img/logo.png" alt="posmaks">

        </div>

        <!-- Nav -->
        <nav id="primary-menu" class="primary-menu">

          <!-- menu-icon-wrapper -->

          <a href='javascript:void(0)' id="menu-icon-trigger" class="menu-icon-trigger showhide">
            <span class="mob-menu--title">Menu</span>
            <span id="menu-icon-wrapper" class="menu-icon-wrapper">
              <svg width="1000px" height="1000px">
                <path id="pathD" d="M 300 400 L 700 400 C 900 400 900 750 600 850 A 400 400 0 0 1 200 200 L 800 800"></path>
                <path id="pathE" d="M 300 500 L 700 500"></path>
                <path id="pathF" d="M 700 600 L 300 600 C 100 600 100 200 400 150 A 400 380 0 1 1 200 800 L 800 200"></path>
              </svg>
            </span>
          </a>

          <ul class="primary-menu-menu">

            <!-- Menu Items -->
            <?php $mainmenu = $content->getMenuList(); $content->getMenu1($mainmenu);?>

            <a class="js-login-popup btn btn-small btn-border btn--with-shadow c-primary" style="margin-bottom: 20px; margin-left: 0;">
              GİRİŞ YAP
            </a>                  

            <a href="<?= SITEURL ?>/index.php#ucretsiz-dene-form" class="js-try-popup btn btn-small btn--with-shadow btn--green" style="margin-bottom: 20px; margin-left: 0;">
              ÜCRETSİZ DENE
            </a>          

          </ul>

        </nav>


      </div>

    </div>


  </header>

  <div class="window-popup message-popup" style="background-color: rgba(0,0,0,0.7);"> 
      
      <div class="send-message-popup"> 
        <svg class="col-lg-11 js-popup-close popup-close-button"><use xlink:href="#utouch-icon-cancel-1"></use></svg> 
        <h5>Biz Sizi Arayalım</h5> 
        <form id="mail-contact-form" class="contact-form" method="post" action="sendmail.php"> 
          <div class="with-icon"> 
            <input name="name" placeholder="İsim" type="text" required="required"> 
            <svg class="utouch-icon utouch-icon-user"><use xlink:href="#utouch-icon-user"></use></svg> 
          </div> 
          <div class="with-icon"> 
            <input name="email" placeholder="Email" type="text" required="required"> 
            <svg class="utouch-icon utouch-icon-message-closed-envelope-1"><use xlink:href="#utouch-icon-message-closed-envelope-1"></use></svg> 
          </div> 
          <div class="with-icon"> 
            <input class="with-icon" name="phone" placeholder="Telefon" type="tel" required="required"> 
            <svg class="utouch-icon utouch-icon-telephone-keypad-with-ten-keys"><use xlink:href="#utouch-icon-telephone-keypad-with-ten-keys"></use></svg> 
          </div> 
          <div class="with-icon"> 
            <input class="with-icon" name="company" placeholder="İşletme Adı" type="text" required="required"> 
            <svg class="utouch-icon utouch-icon-icon-1"><use xlink:href="#utouch-icon-icon-1"></use></svg> 
          </div> 
          <button id="sendMail" class="btn btn--green btn--with-shadow full-width"> 
          Gönder 
          </button> 
          <div id="formMessage"> 
          </div> 
        </form> 
      </div> 
  </div> 
  <div class="window-popup login-popup" style="background-color: rgba(0,0,0,0.7);"> 
    <div class="send-message-popup modal-mobil"> 
      <svg class="col-lg-11 js-popup-close popup-close-button"><use xlink:href="#utouch-icon-cancel-1"></use></svg> 
      <h5>Giriş Yap</h5> 
      <form id="login-contact-form" method="post"> 
        <div class="with-icon"> 
          <input name="folder" placeholder="Erişim Adresiniz" type="text"> 
          <svg class="utouch-icon utouch-icon-users"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#utouch-icon-home-icon-silhouette"></use></svg> 
          <div class="pormaks-w-form"> 
            <p class="btn btn-small btn--with-shadow btn--green" style="margin-top: 4px;"> 
                .posmaks.com 
            </p> 
          </div> 
        </div> 
        <div class="with-icon"> 
          <input name="name" placeholder="Kullanıcı Adınız" type="text"> 
          <svg class="utouch-icon utouch-icon-user"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#utouch-icon-user"></use></svg> 
        </div> 
        <div class="with-icon"> 
          <input name="password" placeholder="Şifreniz" type="password"> 
          <svg class="utouch-icon utouch-icon-telephone-keypad-with-ten-keys"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#utouch-icon-telephone-keypad-with-ten-keys"></use></svg> 
        </div> 
        <div class="g-recaptcha" data-sitekey="6Ldko2AUAAAAAA1ZRkGN19Ig57MkW3OSnwfYXbhE"></div>
        <div id="error-captcha"></div>
        <div id="error-login"> 
        </div> 
        <button class="btn btn--green btn--with-shadow full-width login-button mt-10" id="login-button"> 
        OTURUM AÇ 
        </button> 
      </form> 
    </div> 
  </div> 
  <div class="window-popup try-popup" style="background-color: rgba(0,0,0,0.7);"> 
    <div class="send-message-popup modal-mobil"> 
      <svg class="col-lg-11 js-popup-close popup-close-button"><use xlink:href="#utouch-icon-cancel-1"></use></svg> 
      <h5>Hemen Ücretsiz Dene</h5> 
      <form class="contact-form form-demo-request" method="post"> 
        <div class="with-icon"> 
          <input name="folder" placeholder="Erişim Adresiniz" type="text"> 
          <svg class="utouch-icon utouch-icon-users"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#utouch-icon-home-icon-silhouette"></use></svg> 
          <div class="pormaks-w-form"> 
            <p class="btn btn-small btn--with-shadow btn--green"> 
                .posmaks.com 
            </p> 
          </div> 
        </div> 
        <div class="with-icon"> 
          <input name="name" placeholder="Adınız Soyadınız" type="text"> 
          <svg class="utouch-icon utouch-icon-user"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#utouch-icon-user"></use></svg> 
        </div> 
        <div class="with-icon"> 
          <input name="company" placeholder="İşletme Adı" pwfprops="," type="text"> 
          <svg class="utouch-icon #utouch-icon-telephone-keypad-with-ten-keys"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#utouch-icon-team"></use></svg> 
        </div> 
        <div class="with-icon"> 
          <input name="phone" placeholder="Telefon Numaranız" pwfprops="," type="text"> 
          <svg class="utouch-icon #utouch-icon-telephone-keypad-with-ten-keys"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#utouch-icon-telephone-keypad-with-ten-keys"></use></svg> 
        </div> 
        <div class="with-icon"> 
          <input name="email" placeholder="E-Posta Adresiniz" pwfprops="," type="email"> 
          <svg class="utouch-icon utouch-icon-message-closed-envelope-1"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#utouch-icon-message-closed-envelope-1"></use></svg> 
        </div> 
        <div class="g-recaptcha" data-sitekey="6Ldko2AUAAAAAA1ZRkGN19Ig57MkW3OSnwfYXbhE" data-error-callback="error-callback"></div>
        <div id="error-captcha"></div>
        <button class="btn btn--primary btn--with-shadow full-width btn-demo-request mt-10" button-type="modal"> 
        Hemen Başla 
        </button> 
      </form> 
      <div id="modal_request_message" class="padding-one text-left">
			</div>
    </div> 
  </div> 


  <div class="header-spacer"></div>
  <div class="content-wrapper">

</body>
  $(document).ready(function () {

      /* == Master Form == */
      $("button[name=dosubmit]").on('click', function () {

          posturl = $(this).data('url')

          function showResponse(json) {
              if (json.status == "success") {
                  $(".veriasist.form").removeClass("loading").slideUp();
                  $("#msgholder").html(json.message);
              } else {
                  $(".veriasist.form").removeClass("loading");
                  $("#msgholder").html(json.message);
              }
          }

          function showLoader() {
              $(".veriasist.form").addClass("loading");
          }
          var options = {
              target: "#msgholder",
              beforeSubmit: showLoader,
              success: showResponse,
              type: "post",
              url: SITEURL + posturl,
              dataType: 'json'
          };

          $('#veriasist_form').ajaxForm(options).submit();
      });
  });

  $(".btn-demo-request").on("click", function(){
      
      var btn = $(this),
      btn_text = btn.text(),
      form = btn.closest('form'),
      form_data = form.serializeArray(),
      type = btn.attr("button-type");
      
      form_data.push({
        "name":"processDemoRequest",
        "value":1
      });

      btn.html("Yollanıyor...").attr("disabled", true);

      $.post(SITEURL + "/ajax/user.php", form_data,
        function(json) {
        
        if (json.type === "success") {
          
          form.html(`<h4>${json.message}</h4>`).fadeTo(300, 1);
          $("#msgholder_demo_req").html("");
          $('#modal_request_message').html("");          
          gtag('event', 'conversion', { 'send_to': `${google_adwords_code}/-3t0CJvO3IsBEJ77nPAD`});

        } else {
  
          if (type === "page") {
            $("#msgholder_demo_req").html(json.message);
          } else if (type === "modal") {
            $('#modal_request_message').html(json.message);
          }

        }

        btn.attr("disabled", false).html(btn_text);

      }, "json");
  });

function ahmet(url) {
  var callback = function () { if (typeof (url) != 'undefined') { window.location = url; } };
  gtag('event', 'conversion', { 'send_to': 'AW-1040661918/-3t0CJvO3IsBEJ77nPAD', 'event_callback': callback }); return false;
}

  $("body").on('click', '.login-button', function(e) {
    e.preventDefault();

    var 
    btn = $(this),
    form = btn.closest('form'),
    form_data = form.serializeArray();

    form_data.push({
      "name":"processLoginRequest",
      "value":1
    });

    var url = $('input[name="folder"]',form).val(),
    name = $('input[name="name"]',form).val(),
    password = $('input[name="password"]',form).val(),
    link = `http://${url}.posmaks.com/login`;

    $.post(SITEURL + "/ajax/login.php", form_data,
    function(json) {

      if (json === "error") {
        $('#error-captcha').html('<p style="text-align: center; font-weight: bold;">DOĞRULAMA BOŞ BIRAKILAMAZ</p>');        
      } else {
        $('#error-captcha').html('');        
        
      }

      if(json === "success") {

        var form = $('<form class="hidden" action="'+link+'" method="post"></form>');
        form.append(`<input type="hidden" name="name" value="${name}"/>`);
        form.append(`<input type="hidden" name="password" value="${password}"/>`);

        $('body').append(form);

        form.submit();

      } else {
        $('#error-login').html('<p style="text-align: center; font-weight: bold;">GİRİŞ BİLGİLERİ HATALI</p>');
      }

    });

  });

  $("body").on('click', '#sendMail', function (e) { 
    e.preventDefault();

    var 
    form = $('#mail-contact-form'),
    form_data = form.serializeArray();

    $.post(SITEURL + "/ajax/sendmail.php", form_data,
      function (json) {

      if (json.status === "success") {
        $('#formMessage').html(json.message);

        $('#mail-contact-form input').val("");
        $('#formMessage').html();
      } else {
        $('#formMessage').html(json.message);        
      }
      
    }, "json");

  });


  
  




<?php
  /**
   * Right Sidebar Layout
   *
   * @package VeriAsist
   * @author veriasist.com
   * @copyright 2014
   * @version $Id: right.tpl.php, v4.00 2014-04-20 10:12:05 gewa Exp $
   */
  if (!defined("_VALID_PHP"))
      die('Direct access to this location is not allowed.');
?>
    <?php

      if($content->getAccess()){

        // NOT: Sadece İletişim sayfası için burada değil de contact_form.tpl.php sayfasında basılıyor.
        if(! $row->contact_form) {
          // echo Content::getContentPlugins($row->{'body' . Lang::$lang});
        }

        if ($page = Content::getAccesPages($row)) {
          include($page);
        }

        if($row->module_name and $modfile = Content::getModuleTheme($row->module_name)) {
          require($modfile);
        }

        if ($content->jscode) {
          echo cleanOut($content->jscode);
        }

      }
    ?>
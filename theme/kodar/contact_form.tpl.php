<?php
  /**
   * Contact Form
   *
   * @package VeriAsist
   * @author veriasist.com
   * @copyright 2014
   * @version $Id: contact_form.php, v4.00 2014-04-20 10:12:05 gewa Exp $
   */
  if (!defined("_VALID_PHP"))
      die('Direct access to this location is not allowed.');
    // pr(get_required_files() );
?>
<!-- contact section -->
<section id="contact" class="wow fadeIn no-padding-top" style="padding: 10px 0;">
  <div class="container-fluid xs-text-center bg-white">
    <div class="container">
    <div class="row">
      <!-- text -->
      <div class="col-md-5 col-sm-12 margin-nine no-margin-bottom no-margin-lr">
        <div class="sm-margin-nine sm-no-margin-tb sm-no-margin-left sm-display-inline-table xs-margin-nine xs-no-margin-lr xs-no-margin-top xs-width-100">
          <span class="text-uppercase alt-font black-text "><?php echo Lang::$word->_CF_OFFICE;?></span>
          <p class="text-medium"><?php echo cleanOut($core->address); ?></p>
        </div>
        <div class="margin-thirteen no-margin-lr sm-margin-nine sm-no-margin-tb sm-no-margin-left sm-display-inline-table xs-margin-nine xs-no-margin-lr xs-no-margin-top xs-width-100">
          <span class="text-uppercase alt-font black-text"><?php echo Lang::$word->_CG_SITEPHONE;?></span>
          <p class="text-medium"><?php echo $core->site_phone; ?></p>
        </div>
        <div class="margin-thirteen no-margin-lr sm-margin-nine sm-no-margin-tb sm-no-margin-left sm-display-inline-table xs-margin-nine xs-no-margin-lr xs-no-margin-top xs-width-100">
          <span class="text-uppercase alt-font black-text"><?php echo Lang::$word->_CF_EMAIL;?></span>
          <p class="text-medium"><a href="mailto:<?php echo $core->site_email; ?>" class="gray-text"><?php echo $core->site_email; ?></a></p>
        </div>
        <div class="margin-six no-margin-lr sm-margin-nine sm-no-margin-tb sm-no-margin-left sm-display-inline-table xs-margin-nine xs-no-margin-lr xs-no-margin-top xs-width-100">
          <span class="text-uppercase alt-font black-text"><?php echo Lang::$word->_CG_SITEFAX;?></span>
          <p class="text-medium"><?php echo $core->site_fax; ?></p>
        </div>
      </div>
      <!-- end text -->
      <div class="col-md-7 col-sm-12 margin-nine no-margin-bottom no-margin-lr">
        <form id="veriasist_form" action="javascript:void(0)" method="post">
          <div id="success" class="no-margin-lr"></div>


          <input type="text" name="name" placeholder="<?php echo Lang::$word->_CF_NAME;?>" value="<?php if ($user->logged_in) echo trim($user->name);?>" class="big-input alt-font">
          <input type="text" name="email" placeholder="<?php echo Lang::$word->_CF_EMAIL;?>" value="<?php if ($user->logged_in) echo $user->email;?>" class="big-input alt-font">
          <input type="text" name="phone" placeholder="<?php echo Lang::$word->_CF_PHONE;?>" class="big-input alt-font">

          <div class="select-style big-select alt-font">
            <select name="subject">
              <option value=""><?php echo Lang::$word->_CF_SUBJECT_1;?></option>
              <option value="<?php echo Lang::$word->_CF_SUBJECT_2;?>"><?php echo Lang::$word->_CF_SUBJECT_2;?></option>
              <option value="<?php echo Lang::$word->_CF_SUBJECT_3;?>"><?php echo Lang::$word->_CF_SUBJECT_3;?></option>
              <option value="<?php echo Lang::$word->_CF_SUBJECT_4;?>"><?php echo Lang::$word->_CF_SUBJECT_4;?></option>
              <option value="<?php echo Lang::$word->_CF_SUBJECT_5;?>"><?php echo Lang::$word->_CF_SUBJECT_5;?></option> --> -->
<!--               <option value="<?php echo Lang::$word->_CF_SUBJECT_6;?>"><?php echo Lang::$word->_CF_SUBJECT_6;?></option>
              <option value="<?php echo Lang::$word->_CF_SUBJECT_7;?>"><?php echo Lang::$word->_CF_SUBJECT_7;?></option> --> -->
            </select>
          </div>

          <textarea name="message" placeholder="<?php echo Lang::$word->_CF_MSG;?>" class="big-input alt-font"></textarea>
          
          <div>
            <input type="text" name="code" placeholder="<?php echo Lang::$word->_CF_TOTAL;?>" class="big-input alt-font">
            <img src="<?php echo SITEURL;?>/captcha.php" alt="" class="captcha-img" style="    top: 25.6em;" />
          </div>

          <button data-url="/ajax/sendmail.php" type="button" name="dosubmit" class="highlight-button-dark btn btn-medium"><?php echo Lang::$word->_CF_SEND;?></button>

          <div id="msgholder" class=" padding-one"></div>

          <input name="processContact" type="hidden" value="1">
        </form>
      </div>
    </div>
    </div>
  </div>
</section>
<!-- end contact section -->

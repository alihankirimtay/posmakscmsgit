<?php
  /**
   * Index
   *
   * @package VeriAsist
   * @author veriasist.com
   * @copyright 2014
   * @version $Id: index.php, v4.00 2014-04-20 10:12:05 gewa Exp $
   */
  if (!defined("_VALID_PHP"))
      die('Direct access to this location is not allowed.');
?>
<?php include("header.tpl.php");?>

<?php if(!$row->home_page):?>

<?php endif; ?>

<!-- Widgets -->

<!-- TOP -->
<?php if ($widgettop): ?>
  <?php include(THEMEDIR . "/top_widget.tpl.php");?>
<?php endif;?>


<?php include("footer.tpl.php");?>
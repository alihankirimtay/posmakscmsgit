<?php
  /**
   * Bottom Widget Layout
   *
   * @package VeriAsist
   * @author veriasist.com
   * @copyright 2014
   * @version $Id: bottom_widget.tpl.php, v4.00 2014-04-20 10:12:05 gewa Exp $
   */
  if (!defined("_VALID_PHP"))
      die('Direct access to this location is not allowed.');
?>
<?php if($totalbot):?>
<section class="botwidget clearfix">
  <div class="columns<?php if($totalbot > 1):?> small-vertical-gutters<?php endif;?>">
    <?php foreach ($widgetbottom as $brow): ?>
    <div class="screen-<?php echo $brow->space;?>0 phone-100">
      <?php if($totalbot > 1):?>
      <div class="veriasist-content">
        <?php endif;?>
        <div class="botwidget-wrap<?php if($brow->alt_class !="") echo ' '.$brow->alt_class;?>">
          <?php if ($brow->show_title == 1):?>
          <h3 class="veriasist header"><span><?php echo $brow->{'title' . Lang::$lang};?></span></h3>
          <?php endif;?>
          <?php if ($brow->{'body' . Lang::$lang}) echo "<div class=\"widget-body\">".cleanOut($brow->{'body' . Lang::$lang})."</div>";?>
          <?php if ($brow->jscode) echo cleanOut($brow->jscode);?>
          <?php if ($brow->system == 1):?>
          <?php $widgetfile = Content::getPluginTheme($brow->plugalias);?>
          <?php require($widgetfile);?>
          <?php endif;?>
        </div>
      </div>
      <?php if($totalbot > 1):?>
    </div>
    <?php endif;?>
    <?php endforeach; ?>
    <?php unset($brow);?>
  </div>
</section>
<?php endif;?>
  $(document).ready(function () {
      $("select").chosen({
          disable_search_threshold: 10,
          width: "100%"
      });

      $('.veriasist.dropdown').dropdown();
      $('body [data-content]').popover({
          trigger: 'hover',
          placement: 'auto'
      });

      $("table.sortable").tablesort();

      $('.filefield').filestyle({
          buttonText: 'Choose file...'
      });

	  $(".chosen-results, .scrollbox").enscroll({
		  showOnHover: true,
		  addPaddingToPane: false,
		  verticalTrackClass: 'scrolltrack',
		  verticalHandleClass: 'scrollhandle'
	  });
	  
      /* == Lightbox == */
      $('.lightbox').swipebox();

      /* == Carousel == */
      $(".veriasist-carousel").owlCarousel();

      /* == Smooth Scroll == */
      $(".scroller").click(function (e) {
          e.preventDefault();
          var defaultAnchorOffset = 0;
          var a = $(this).attr('data-scroller');
          var anchorOffset = $('#' + a).attr('data-scroller-offset');
          if (!anchorOffset)
              anchorOffset = defaultAnchorOffset;
          $('html,body').animate({
              scrollTop: $('#' + a).offset().top - anchorOffset
          }, 500);
      });

      /* == Date/Time Picker == */
      $('body [data-datepicker]').pickadate({});
      $('body [data-timepicker]').pickatime({
          formatSubmit: 'HH:i:00'
      });

      /* == Close Message == */
      $('body').on('click', '.message i.close.icon', function () {
          var $msgbox = $(this).closest('.message')
          $msgbox.slideUp(500, function () {
              $(this).remove()
          });
      });

      $('body').on('click', '.toggle', function () {
          $('.toggle.active').not(this).removeClass('active');
          $(this).toggleClass("active");
      });

      /* == Language Switcher == */
      $('#langmenu').on('click', 'a', function () {
          var target = $(this).attr('href');
          $.cookie("LANG_VERIASIST", $(this).data('lang'), {
              expires: 120,
              path: '/'
          });
          $('body').fadeOut(1000, function () {
              window.location.href = SITEURL + "/" + target;
          });
          return false
      });

      /* == Navigation Menu == */
      $('#menu').smartmenus({
          subIndicatorsText: "<i class=\"down angle icon\"></i>",
          subMenusMaxWidth: "auto",
          subMenusMinWidth: "14em"
      });

      $('#menu li a[href="#"]').click(function (e) {
          e.preventDefault();
      });

      $('#menu > li[data-cols]').each(function () {
          var $ul = $(this);
          $ul.has('ul').children('ul').addClass($ul.data('cols'));
      });

      /* == Scroll To Top == */
      $.scrollUp();

      /* == Tabs == */
	  $(".wtabs .veriasist.tab.content").hide();
	  $(".veriasist.tabs").find('a:first').addClass("active").show();
	  $('.wtabs').each(function(){
		  $(this).find('.veriasist.tab.content:first').show();
	  });
      $(".veriasist.tabs a").on('click', function () {
		  id = $(this).closest(".wtabs").attr("id");
          $("#" + id + " .veriasist.tabs a").removeClass("active");
          $(this).addClass("active");
		  $("#" + id + " .veriasist.tab.content").hide();
          var activeTab = $(this).data("tab");
          $(activeTab).show();
      });

      /* == Accordion == */
      $('.accordion .header').toggleClass('inactive');
      $('.accordion .header').first().toggleClass('active').toggleClass('inactive');
      $('.accordion .content').first().slideDown().toggleClass('open');
      $('.accordion .header').click(function () {
          if ($(this).is('.inactive')) {
              $('.accordion .active').toggleClass('active').toggleClass('inactive').next().slideToggle().toggleClass('open');
              $(this).toggleClass('active').toggleClass('inactive');
              $(this).next().slideToggle().toggleClass('open');
          } else {
              $(this).toggleClass('active').toggleClass('inactive');
              $(this).next().slideToggle().toggleClass('open');
          }
      });

      /* == Live Search == */
      $("#searchfield").on('keyup', function () {
          var srch_string = $(this).val();
          var data_string = 'liveSearch=' + srch_string;
          if (srch_string.length > 4) {
              $.ajax({
                  type: "post",
                  url: SITEURL + "/ajax/livesearch.php",
                  data: data_string,
                  beforeSend: function () {

                  },
                  success: function (res) {
                      $('#suggestions').html(res).show();
                      $("input").blur(function () {
                          $('#suggestions').fadeOut();
                      });
                  }
              });
          }
          return false;
      });
      $("#livesearch").on('click', 'i.search', function () {
          $("#livesearch").submit()

      });

      /* == Animate Progress Bars == */
      function progress(percent, element) {
          var progressBarWidth = percent * element.width() / 100;
          $(element).find('div').animate({
              width: progressBarWidth
          }, 500);
      }
      animateProgress();

      function animateProgress() {
          $('.progress').each(function () {
              var bar = $(this);
              var size = $(this).data('percent');
              progress(size, bar);
          });
      }
      $(window).resize(function () {
          animateProgress();
      });

      /* == Master Form == */
      $('body').on('click', 'button[name=dosubmit]', function () {
          posturl = $(this).data('url')

          function showResponse(json) {
              if (json.status == "success") {
                  $(".veriasist.form").removeClass("loading").slideUp();
                  $("#msgholder").html(json.message);
              } else {
                  $(".veriasist.form").removeClass("loading");
                  $("#msgholder").html(json.message);
              }
          }

          function showLoader() {
              $(".veriasist.form").addClass("loading");
          }
          var options = {
              target: "#msgholder",
              beforeSubmit: showLoader,
              success: showResponse,
              type: "post",
              url: SITEURL + posturl,
              dataType: 'json'
          };

          $('#veriasist_form').ajaxForm(options).submit();
      });

      /* == Sticky Footer == */
      doFooter();
      $(window).scroll(doFooter);
      $(window).resize(doFooter);
      $(window).load(doFooter);

      $.browser = {};
      $.browser.version = 0;
      if (navigator.userAgent.match(/MSIE ([0-9]+)\./)) {
          $.browser.version = RegExp.$1;
          if ($.browser.version < 10) {
              new Messi("<p class=\"veriasist red segment\" style=\"width:300px\">It appears that you are using a <em>very</em> old version of MS Internet Explorer (MSIE) v." + $.browser.version + ".<br />If you seriously want to continue to use MSIE, at least <a href=\"http://www.microsoft.com/windows/internet-explorer/\">upgrade</a></p>", {
                  title: "Old Browser Detected",
                  modal: true,
                  closeButton: true
              });
          }
      }
  });
  $(window).on('resize', function () {
      $(".slrange").ionRangeSlider('update');
  });

  function doFooter() {
      var footer = $("footer");
      if ((($(document.body).height() + footer.outerHeight()) < $(window).height() && footer.css("position") == "fixed") || ($(document.body).height() < $(window).height() && footer.css("position") != "fixed")) {
          footer.css({
              position: "fixed",
              bottom: 0,
              width: "100%"
          });
      } else {
          footer.css({
              position: "static"
          });
      }
  }
  
  jQuery(function ($) {
      $(document).ready(function () {
          var contentButton = [];
          var contentTop = [];
          var content = [];
          var lastScrollTop = 0;
          var scrollDir = '';
          var itemClass = '';
          var itemHover = '';
          var menuSize = null;
          var stickyHeight = 0;
          var stickyMarginB = 0;
          var currentMarginT = 0;
          var topMargin = 0;
          $(window).scroll(function (event) {
              var st = $(this).scrollTop();
              if (st > lastScrollTop) {
                  scrollDir = 'down';
              } else {
                  scrollDir = 'up';
              }
              lastScrollTop = st;
          });
          $.fn.stickUp = function (options) {
			  if ($(window).width() > 768) {
				 $(this).addClass('stuckMenu');
			  } else {
				  $(this).removeClass('stuckMenu');
			  }
              var objn = 0;
              if (options != null) {
                  for (var o in options.parts) {
                      if (options.parts.hasOwnProperty(o)) {
                          content[objn] = options.parts[objn];
                          objn++;

                      }
                  }
                  if (objn == 0) {
                      console.log('error:needs arguments');
                  }
                  itemClass = options.itemClass;
                  itemHover = options.itemHover;
                  if (options.topMargin != null) {
                      if (options.topMargin == 'auto') {
                          topMargin = parseInt($('.stuckMenu').css('margin-top'));
                      } else {
                          if (isNaN(options.topMargin) && options.topMargin.search("px") > 0) {
                              topMargin = parseInt(options.topMargin.replace("px", ""));
                          } else if (!isNaN(parseInt(options.topMargin))) {
                              topMargin = parseInt(options.topMargin);
                          } else {
                              console.log("incorrect argument, ignored.");
                              topMargin = 0;
                          }
                      }
                  } else {
                      topMargin = 0;
                  }
                  menuSize = $('.' + itemClass).size();
              }
              stickyHeight = parseInt($(this).height());
              stickyMarginB = parseInt($(this).css('margin-bottom'));
              currentMarginT = parseInt($(this).next().closest('div').css('margin-top'));
              vartop = parseInt($(this).offset().top);
          };
          $(document).on('scroll', function () {
              varscroll = parseInt($(document).scrollTop());
              if (menuSize != null) {
                  for (var i = 0; i < menuSize; i++) {
                      contentTop[i] = $('#' + content[i] + '').offset().top;

                      function bottomView(i) {
                          contentView = $('#' + content[i] + '').height() * .4;
                          testView = contentTop[i] - contentView;
                          if (varscroll > testView) {
                              $('.' + itemClass).removeClass(itemHover);
                              $('.' + itemClass + ':eq(' + i + ')').addClass(itemHover);
                          } else if (varscroll < 50) {
                              $('.' + itemClass).removeClass(itemHover);
                              $('.' + itemClass + ':eq(0)').addClass(itemHover);
                          }
                      }
                      if (scrollDir == 'down' && varscroll > contentTop[i] - 50 && varscroll < contentTop[i] + 50) {
                          $('.' + itemClass).removeClass(itemHover);
                          $('.' + itemClass + ':eq(' + i + ')').addClass(itemHover);
                      }
                      if (scrollDir == 'up') {
                          bottomView(i);
                      }
                  }
              }
              if (vartop < varscroll + topMargin) {
                  $('.stuckMenu').addClass('isStuck');
                  $('.stuckMenu').next().closest('div').css({
                      'margin-top': stickyHeight + stickyMarginB + currentMarginT + 'px'
                  }, 10);
                  $('.stuckMenu').css("position", "fixed");
                  $('.isStuck').css({
                      top: '0px'
                  }, 10, function () {});
              };
              if (varscroll + topMargin < vartop) {
                  $('.stuckMenu').removeClass('isStuck');
                  $('.stuckMenu').next().closest('div').css({
                      'margin-top': currentMarginT + 'px'
                  }, 10);
                  $('.stuckMenu').css("position", "relative");
              };
          });
      });
  });

	  
  $(function ($) {
      $(document).ready(function () {
		  $('#menu-wrap').stickUp();
      });
  });
<?php
  /**
   * Content
   *
   * @package VeriAsist
   * @author veriasist.com
   * @copyright 2014
   * @version $Id: content.php, v4.00 2014-04-20 10:12:05 gewa Exp $
   */
  define("_VALID_PHP", true);
  require_once ("init.php");

  if ($content->_url[0] == Url::$data['pagedata']['page']):
      $row = $content->renderPage();

      $plgresult = $content->getPluginLayoutFront();
      $widgettop = Content::countPlace($plgresult, "top");
      $widgetbottom = Content::countPlace($plgresult, "bottom");
      $widgetleft = Content::countPlace($plgresult, "left");
      $widgetright = Content::countPlace($plgresult, "right");
      $assets = Content::countPlace($plgresult, false, false);

      $totalleft = count($widgetleft);
      $totalright = count($widgetright);
      $totaltop = count($widgettop);
      $totalbot = count($widgetbottom);

      if ($row):
          $core->getVisitors(); // visitor counter
          require_once (THEMEDIR . "/index.tpl.php");
      else:
          redirect_to(SITEURL . "/404.php");
      endif;

  else:
	  if (isset(Url::$data['moddir'][$content->_url[0]]) and file_exists('modules/' . Url::$data['moddir'][$content->_url[0]] . '/main.php')):
		  $plgresult = $content->getPluginLayoutFront();
		  $widgettop = Content::countPlace($plgresult, "top");
		  $widgetbottom = Content::countPlace($plgresult, "bottom");
		  $widgetleft = Content::countPlace($plgresult, "left");
		  $widgetright = Content::countPlace($plgresult, "right");
		  $assets = Content::countPlace($plgresult, false, false);

		  $totalleft = count($widgetleft);
		  $totalright = count($widgetright);
		  $totaltop = count($widgettop);
		  $totalbot = count($widgetbottom);

		  require_once (THEMEDIR . "/mod_index.tpl.php");
	  else:
		  redirect_to(SITEURL . "/404.php");
	  endif;
  endif;
?>
<?php
  /**
   * Cron Job Listing Expiry Zero Days Notice
   *
   * @package Veriasist!
   * @author veriasist.com
   * @copyright 2011
   * @version $Id: cron_job0days.php,v 1.00 2010-08-10 21:12:05 gewa Exp $
   */
  define("_VALID_PHP", true);
  require_once("../init.php");


    $sql = "SELECT W.*, U.database_name
    FROM mod_websites as W 
    INNER JOIN users as U ON W.id = U.mod_website_id 
    WHERE W.deleted BETWEEN DATE_SUB(NOW(), INTERVAL 45 DAY) AND DATE_SUB(NOW(), INTERVAL 30 DAY) AND W.deleted is not null ";

    $users = Registry::get("Database")->fetch_all($sql);
    
    if ($users) {

      $DB_HOST = DB_SERVER;
      $DB_USER = DB_USER;
      $DB_PASS = DB_PASS;

      foreach ($users as $user) {
        $dir = BASEPATH . "sistemyonetimi/modules/websites/backups/".$user->folder.".sql";

        if (!file_exists($dir)) {
          exec("mysqldump -u {$DB_USER} --password='{$DB_PASS}' {$user->database_name} > {$dir} 2>&1", $output1);
        }

        exec("mysqladmin -u {$DB_USER} --password='{$DB_PASS}' drop {$user->database_name} -f 2>&1", $output4);

        if ($user->folder && is_dir(BASEPATH . "deleted-".$user->folder)) { 
          delete_directory(BASEPATH . "deleted-".$user->folder); 
        } 

      }

    }

    


  
  
?>
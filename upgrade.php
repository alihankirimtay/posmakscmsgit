<?php
  define("_VALID_PHP", true);
  
  $BASEPATH = str_replace("upgrade.php", "", realpath(__FILE__));
  define("BASEPATH", $BASEPATH);
  
  $configFile = BASEPATH . "lib/config.ini.php";
  if (file_exists($configFile)) {
      require_once($configFile);
  } else {
      exit("Configuration file is missing. Upgrade can not continue");
  }
  
  require_once (BASEPATH . "lib/class_db.php");

  require_once (BASEPATH . "lib/class_registry.php");
  Registry::set('Database', new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE));
  $db = Registry::get("Database");
  $db->connect();
  
  $ver = $db->first("SELECT version FROM settings");
  
  function redirect_to($location)
  {
      if (!headers_sent()) {
          header('Location: ' . $location);
          exit;
      } else
          echo '<script type="text/javascript">';
      echo 'window.location.href="' . $location . '";';
      echo '</script>';
      echo '<noscript>';
      echo '<meta http-equiv="refresh" content="0;url=' . $location . '" />';
      echo '</noscript>';
  }
  
  if (isset($_POST['submit'])) {
	  $db->query("ALTER TABLE `email_templates` ADD COLUMN `typeid` varchar(50) COLLATE utf8_general_ci NULL after `type`;");
	  
	  $db->query("ALTER TABLE `mod_faq` 
				CHANGE `position` `position` smallint(3) NOT NULL DEFAULT 0 after `answer_en` , COMMENT='contains f.a.q. data' COLLATE ='utf8_unicode_ci';");
	
	  $db->query("ALTER TABLE `pages` 
				ADD COLUMN `profile` tinyint(1) NOT NULL DEFAULT 0 after `sitemap`;");
	
	  $db->query("ALTER TABLE `settings` 
				ADD COLUMN `profile_page` varchar(100)  COLLATE utf8_general_ci NULL after `account_page`;");
				
	  $db->query("CREATE TABLE `user_activity`(
		  `id` int(11) NOT NULL  auto_increment , 
		  `uid` int(11) NOT NULL  DEFAULT 0 , 
		  `url` varchar(200) COLLATE utf8_general_ci NULL  , 
		  `icon` varchar(50) COLLATE utf8_general_ci NULL  , 
		  `type` varchar(100) COLLATE utf8_general_ci NULL  , 
		  `title` varchar(150) COLLATE utf8_general_ci NULL  , 
		  `subject` varchar(100) COLLATE utf8_general_ci NULL  , 
		  `message` text COLLATE utf8_general_ci NULL  , 
		  `created` datetime NULL  DEFAULT '0000-00-00 00:00:00' , 
		  PRIMARY KEY (`id`) 
	  ) ENGINE=MyISAM DEFAULT CHARSET='utf8' COLLATE='utf8_general_ci';");

	  $db->query("ALTER TABLE `users`   
				ADD COLUMN `fb_link` varchar(100)  COLLATE utf8_general_ci NULL after `info` , 
				ADD COLUMN `tw_link` varchar(100)  COLLATE utf8_general_ci NULL after `fb_link` , 
				ADD COLUMN `gp_link` varchar(100)  COLLATE utf8_general_ci NULL after `tw_link`;");

	  $db->query("INSERT INTO `pages` (`title_en`, `slug`, `caption_en`, `profile`, `created`) VALUES   
				('Profile', 'profile', 'Public User Profile', 1, '2014-11-13 17:27:25');");
				
	  $setdata['profile_page'] = 'profile';
	  $setdata['version'] = '4.10';
	  $db->update("settings", $setdata);
	  
      redirect_to("upgrade.php?update=done");
  }
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Veriasist Upgrade</title>
<style type="text/css">
@import url(https://fonts.googleapis.com/css?family=Raleway:400,100,300,600,700);
body {
  font-family: Raleway, Arial, Helvetica, sans-serif;
  font-size: 14px;
  line-height: 1.3em;
  color: #FFF;
  background-color: #222;
  font-weight: 300;
  margin: 0;
  padding: 0
}
#wrap {
  width: 800px;
  margin-top: 150px;
  margin-right: auto;
  margin-left: auto;
  background-color: #208ed3;
  box-shadow: 2px 2px 2px 2px rgba(0,0,0,0.1);
  border: 2px solid #111;
  border-radius: 3px
}
header {
  background-color: #145983;
  font-size: 26px;
  font-weight: 200;
  padding: 35px
}
.line {
  height: 2px;
  background: linear-gradient(to right, rgba(255,255,255,0) 0%, rgba(255,255,255,1) 47%, rgba(255,255,255,0) 100%)
}
.line2 {
  position: absolute;
  left: 200px;
  height: 360px;
  width: 2px;
  background: linear-gradient(to bottom, rgba(255,255,255,0) 0%, rgba(255,255,255,1) 47%, rgba(255,255,255,0) 100%);
  display: block
}
#content {
  position: relative;
  padding: 45px 20px
}
#content .left {
  float: left;
  width: 200px;
  height: 400px;
  background-image: url(assets/installer.png);
  background-repeat: no-repeat;
  background-position: 10px center
}
#content .right {
  margin-left: 200px
}
h4 {
  font-size: 18px;
  font-weight: 300;
  margin: 0 0 40px;
  padding: 0
}
p.info {
  background-color: #383838;
  border-radius: 3px;
  box-shadow: 1px 1px 1px 1px rgba(0,0,0,0.1);
  padding: 10px
}
p.info span {
  display: block;
  float: left;
  padding: 10px;
  background: rgba(255,255,255,0.1);
  margin-left: -10px;
  margin-top: -10px;
  border-radius: 3px 0 0 3px;
  margin-right: 5px;
  border-right: 1px solid rgba(255,255,255,0.05)
}
footer {
  background-color: #383838;
  padding: 20px
}
form {
  display: inline-block;
  float: right;
  margin: 0;
  padding: 0
}
.button {
  border: 2px solid #222;
  font-family: Raleway, Arial, Helvetica, sans-serif;
  font-size: 14px;
  color: #FFF;
  background-color: #208ED3;
  text-align: center;
  cursor: pointer;
  font-weight: 600;
  -webkit-transition: all .35s ease;
  -moz-transition: all .35s ease;
  -o-transition: all .35s ease;
  transition: all .35s ease;
  outline: none;
  margin: 0;
  padding: 5px 20px
}
.button:hover {
  background-color: #222;
  -webkit-transition: all .55s ease;
  -moz-transition: all .55s ease;
  -o-transition: all .35s ease;
  transition: all .55s ease;
  outline: none
}
.clear {
  font-size: 0;
  line-height: 0;
  clear: both;
  height: 0
}
.clearfix:after {
  content: ".";
  display: block;
  height: 0;
  clear: both;
  visibility: hidden;
}
a {
  text-decoration: none;
  float: right
}
</style>
</head>
<body>
<div id="wrap">
  <header>Welcome to Veriasist Upgrade Wizard</header>
  <div class="line"></div>
  <div id="content">
    <div class="left">
      <div class="line2"></div>
    </div>
    <div class="right">
      <h4>Veriasist Upgrade v.4.10</h4>
      <?php if(isset($_GET['update']) && $_GET['update'] == "done"):?>
      <p class="info"><span>Success!</span>Installation Completed. Please delete upgrade.php</p>
      <?php else:?>
      <?php if($ver->version != 4.06):?>
      <p class="info"><span>Warning!</span>You need at least Veriasist v4.06 in order to continue.</p>
      <?php else:?>
      <p class="info"><span>Warning!</span>Please make sure you have performed database backup!!!</p>
      <p style="margin-top:60px">When ready click Install button.</p>
      <p><span>Please be patient, and<strong> DO NOT</strong> Refresh your browser.<br>
        This process might take a while</span>.</p>
      <?php endif;?>
      <?php endif;?>
    </div>
  </div>
  <div class="clear"></div>
  <footer class="clearfix"> <small>current <b>veriasist v.<?php echo $ver->version;?></b></small>
    <?php if(isset($_GET['update']) && $_GET['update'] == "done"):?>
    <a href="sistemyonetimi/index.php" class="button">Back to admin panel</a>
    <?php else:?>
    <form method="post" name="upgrade_form">
      <?php if($ver->version == 4.06):?>
      <input name="submit" type="submit" class="button" value="Upgrade Veriasist" id="submit" />
      <?php endif;?>
    </form>
    <?php endif;?>
  </footer>
</div>
</body>
</html>
<?php

define('_VALID_PHP', true);
define('DS', DIRECTORY_SEPARATOR);

$BASEPATH = str_replace('api' . DS . 'index.php', null, realpath(__FILE__));
define('BASEPATH', $BASEPATH);

require_once BASEPATH . 'lib/config.ini.php';
require_once BASEPATH . 'lib/class_db.php';
require_once BASEPATH . 'lib/class_registry.php';
Registry::set('Database', new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE));
$db = Registry::get('Database');
$db->connect();

//Start Url Class
require_once BASEPATH . "lib/class_url.php";
Registry::set('Url', new Url());

//Include Functions
require_once BASEPATH . "lib/functions.php";
require_once BASEPATH . "lib/fn_seo.php";

//Start Core Class
require_once BASEPATH . "lib/class_core.php";
Registry::set('Core', new Core());
$core = Registry::get("Core");

if (isset($_SERVER['HTTPS'])) {
    $protocol = ($_SERVER['HTTPS'] && $_SERVER['HTTPS'] != 'off') ? 'https' : 'http';
} else {
    $protocol = 'http';
}

$dir = (Registry::get('Core')->site_dir) ? '/' . Registry::get('Core')->site_dir : '';
$url = preg_replace('#/+#', '/', $_SERVER['HTTP_HOST'] . $dir);
$site_url = $protocol . '://' . $url;
define('SITEURL', $site_url);

define('UPLOADS', BASEPATH . 'uploads/');
define('UPLOADURL', SITEURL . '/uploads/');

new Api($db, $core);

class Api
{
    private $fn;
    private $db;
    private $Core;

    private $user;

    const lenght_device_id = 5;
    const lenght_api_token = 40;

    public function __construct($db = null, $core = null)
    {
        $this->db = $db;
        $this->Core = $core;

        if (!$this->_callFunction()) {
            $this->api_messageError('Invalid Request.');
        }

        $this->{$this->fn}();
    }

    private function Auth(array $config = array())
    {
        $api_token = $this->request('get', 'api_token', 'Api Token', 'required|trim|exact_length::' . self::lenght_api_token);

        $this->user = getValues(
            'id, username, email, fname, lname, membership_id, mem_expire, api_token, avatar,
            userlevel, active, (SELECT title_en FROM memberships WHERE id = users.membership_id LIMIT 1) as membership,
            billing_address, iyzipay_credit_card, database_name',
            'users', "api_token = '{$api_token}'");

        if (!$this->user) {
            $this->api_messageError([
                'message' => 'Invalid Api Token.',
            ]);
        }

        switch ($this->user->active) {
            case 'b':$this->api_messageError(['event' => 'login', 'Your account has been banned.']);
            break;
            case 'n':$this->api_messageError(['event' => 'login', 'Your account it\'s not activated.']);
            break;
            case 't':$this->api_messageError(['event' => 'login', 'You need to verify your email address.']);
            break;
        }

        $this->user->membership = htmlspecialchars_decode($this->user->membership);
        $this->user->userlevel = (int) $this->user->userlevel;

        $this->user->billing_address = (Object) array_merge([
            'type' => 'firm',
            'company' => null,
            'tax_office' => null,
            'tax_number' => null,
            'name' => null,
            'identity_number' => null,
            'address' => null,
            'zip_code' => null,
            'district' => null,
            'city' => null,
        ], (Array) json_decode($this->user->billing_address));

        $this->user->iyzipay_credit_card = (Object) array_merge([
            'cardUserKey' => null,
            'cardToken' => null,
            'binNumber' => null,
        ], (Array) json_decode($this->user->iyzipay_credit_card));
    }

    public function memberships()
    {
        $this->Auth();

        $where_membership = null;

        $memberships = $this->db->fetch_all("SELECT
            id, title_en AS title, description_en AS description, private,
            price, '' AS price_formated, '' AS currency, '' AS currency_symbol, days AS `time`, period
            FROM memberships
            WHERE trial = 0 AND active = 1 $where_membership ORDER BY price");

        if ($memberships) {
            $json['count'] = count($memberships);

            $paypal = getValues('*', 'gateways', 'active = 1 AND name = "paypal"') or $this->api_messageError('No payment method found!');
            $paypal_url = ($paypal->live ? 'www.paypal.com' : 'www.sandbox.paypal.com') . '/cgi-bin/webscr';
            $paypal_currency_code = $paypal->extra2 ? $paypal->extra2 : $this->Core->currency;

            foreach ($memberships as $membership) {

                switch ($membership->period) {
                    case 'D':$membership->period = 'Day(s)';
                    break;
                    case 'W':$membership->period = 'Week(s)';
                    break;
                    case 'M':$membership->period = 'Month(s)';
                    break;
                    case 'Y':$membership->period = 'Year(s)';
                    break;
                }

                $membership->price_formated = $this->Core->api_formatMoney($membership->price);
                $membership->currency = $this->Core->currency;
                $membership->currency_symbol = $this->Core->cur_symbol;
                $membership->payment_methods = (object) [
                    'paypal' => (object) [
                        'url' => $paypal_url,
                        'cmd' => '_cart',
                        'upload' => 1,
                        'business' => $paypal->extra,
                        'notify_url' => SITEURL . '/modules/digishop/gateways/paypal/ipn.php',
                        'currency_code' => $paypal_currency_code,
                        'custom' => $this->user->id . '_' . 'SessionIdNotRequired',
                        'no_note' => 0,
                        'rm' => 2,
                        'item_name_1' => cleanOut($membership->title),
                        'item_number_1' => $membership->id,
                        'quantity_1' => 1,
                        'amount_1' => $membership->price,
                    ],
                ];

                $json['items'][] = $membership;
            }

            $this->api_messageOk($json);
        }

        $this->api_messageError('No Membership found!');
    }

    public function membershipsPayments()
    {
        $this->Auth();

        $payments = $this->db->fetch_all("SELECT *,
          (SELECT title_tr FROM memberships WHERE id = membership_id LIMIT 1) As membership_title
          FROM payments
          WHERE user_id = {$this->user->id}");

        if ($payments) {

            $json['count'] = count($payments);

            foreach ($payments as $payment) {

                $payment->status_text = $payment->status ? 'Completed' : 'Pending';
                $payment->price_formated = $this->Core->api_formatMoney($payment->rate_amount);

                $json['items'][] = $payment;
            }

            $this->api_messageOk($json);
        }

        $this->api_messageError('İşlem geçmişi bulunamadı.');
    }

    public function membershipsGetSettings()
    {
        $this->Auth();

        $this->api_messageOk([
            'count' => 2,
            'items' => [
                'billing_address' => $this->user->billing_address,
                'iyzipay_credit_card' => [
                    'binNumber' => $this->user->iyzipay_credit_card->binNumber,
                ],
            ],
        ]);
    }

    public function membershipsUpdateSettings()
    {
        $this->Auth();

        $bill_type = $this->request('post', 'bill_type', 'Type', 'required|trim|in_list::firm,person');
        if ($bill_type == 'firm') {
            $bill_company = $this->request('post', 'bill_company', 'Ünvan', 'required|trim');
            $bill_tax_office = $this->request('post', 'bill_tax_office', 'Vergi Dairesi', 'required|trim');
            $bill_tax_number = $this->request('post', 'bill_tax_number', 'Vergi No', 'required|trim');

            $bill_name = $this->user->billing_address->name;
            $bill_identity_number = $this->user->billing_address->identity_number;
        } else {
            $bill_name = $this->request('post', 'bill_name', 'İsim Soyisim', 'required|trim');
            $bill_identity_number = $this->request('post', 'bill_identity_number', 'T.C. Kimlik No', 'required|trim|exact_length::11');

            $bill_company = $this->user->billing_address->company;
            $bill_tax_office = $this->user->billing_address->tax_office;
            $bill_tax_number = $this->user->billing_address->tax_number;
        }
        $bill_address = $this->request('post', 'bill_address', 'Adres', 'required|trim');
        $bill_zip_code = $this->request('post', 'bill_zip_code', 'Posta Kodu', 'required|trim');
        $bill_district = $this->request('post', 'bill_district', 'İlçe', 'required|trim');
        $bill_city = $this->request('post', 'bill_city', 'İl', 'required|trim');

        if (!empty($_POST['card_holdername']) || !empty($_POST['card_number']) || !empty($_POST['card_month']) || !empty($_POST['card_year'])) {
            $card_holdername = $this->request('post', 'card_holdername', 'Kart Üzerindeki İsim', 'required|trim');
            $card_number = $this->request('post', 'card_number', 'Kredi Kartı Numarası', 'required|trim');
            $card_month = $this->request('post', 'card_month', 'Ay', 'required|trim|in_list::' . implode(',', range(1, 12)));
            $card_year = $this->request('post', 'card_year', 'Yıl', 'required|trim|in_list::' . implode(',', range(date('Y'), date('Y', strtotime('+ 10 year')))));

            $card = $this->createIyzipayCreditCart([
                'card_holdername' => $card_holdername,
                'card_number' => $card_number,
                'card_month' => $card_month,
                'card_year' => $card_year,
            ]);

            $data['iyzipay_credit_card'] = json_encode($card);

            $this->deleteIyzipayCreditCard();
        }

        $billing_address = [
            'type' => $bill_type,
            'company' => $bill_company,
            'tax_office' => $bill_tax_office,
            'tax_number' => $bill_tax_number,
            'name' => $bill_name,
            'identity_number' => $bill_identity_number,
            'address' => $bill_address,
            'zip_code' => $bill_zip_code,
            'district' => $bill_district,
            'city' => $bill_city,
        ];

        $data['billing_address'] = json_encode($billing_address);

        $this->db->update('users', $data, 'id=' . $this->user->id);

        $this->api_messageOk([
            'count' => 1,
            'message' => 'Abonelik ayarlarınız başarıyla güncellendi.',
            'items' => [
                'billing_address' => $billing_address,
            ],
        ]);
    }

    public function membershipsSubscription()
    {
        $this->Auth();

        $membership_id = $this->request('post', 'membership_id', 'Memebership', 'required|trim|is_natural_no_zero');
        $redirect_url = $this->request('post', 'redirect_url', 'Redirect', 'required|trim');
        $txn_id = 'IYZPY_' . $this->user->id . '_' . time();

        if (!$membership = getValues('*, title_en As title', 'memberships', "active = 1 AND id = '{$membership_id}' LIMIT 1")) {
            $this->api_messageError('Invalid Memebership.');
        }

        if (!$gateway = getValues('*', 'gateways', 'active = 1 AND name = "iyzipay" LIMIT 1')) {
            $this->api_messageError('Invalid Gateway.');
        }

        $createPaymentRequest = $this->_subscription([
            'membership' => $membership,
            'gateway' => $gateway,
            'redirect_url' => $redirect_url,
            'conversation_id' => $txn_id,
        ]);

        if ($createPaymentRequest->getStatus() == 'success') {

            $this->db->insert('payments', [
                'txn_id' => $txn_id,
                'membership_id' => $membership->id,
                'user_id' => $this->user->id,
                'rate_amount' => (float) $membership->price,
                'ip' => $_SERVER['REMOTE_ADDR'],
                'created' => 'NOW()',
                'pp' => 'IyziPay',
                'currency' => $gateway->extra2,
                'status' => 0,
            ]);

            $json['items'] = [
                'htmlContent' => $createPaymentRequest->getHtmlContent(),
            ];

            $this->api_messageOk($json);
        }

        $this->api_messageError($createPaymentRequest->getErrorMessage());
    }

    private function createIyzipayCreditCart($data)
    {
        $this->IyziPayConfigs();

        # create request class
        $request = new \Iyzipay\Request\CreateCardRequest();
        $request->setLocale(\Iyzipay\Model\Locale::TR);
        $request->setEmail($this->user->email);

        $cardInformation = new \Iyzipay\Model\CardInformation();
        $cardInformation->setCardAlias($this->user->id);
        $cardInformation->setCardHolderName($data['card_holdername']);
        $cardInformation->setCardNumber($data['card_number']);
        $cardInformation->setExpireMonth($data['card_month']);
        $cardInformation->setExpireYear($data['card_year']);
        $request->setCard($cardInformation);

        # make request
        $card = \Iyzipay\Model\Card::create(
            $request,
            Config::options($this->iyzipay->extra, $this->iyzipay->extra3, $this->iyzipay->live)
        );

        if ($card->getStatus() != 'success') {
            $this->api_messageError($card->getErrorMessage());
        }

        return [
            'cardUserKey' => $card->getCardUserKey(),
            'cardToken' => $card->getCardToken(),
            'binNumber' => $card->getBinNumber(),
        ];
    }

    private function deleteIyzipayCreditCard()
    {
        if ($this->user->iyzipay_credit_card->cardUserKey) {

            # create request class
            $request = new \Iyzipay\Request\DeleteCardRequest();
            $request->setLocale(\Iyzipay\Model\Locale::TR);
            $request->setCardToken($this->user->iyzipay_credit_card->cardToken);
            $request->setCardUserKey($this->user->iyzipay_credit_card->cardUserKey);

            # make request
            $card = \Iyzipay\Model\Card::delete(
                $request,
                Config::options($this->iyzipay->extra, $this->iyzipay->extra3, $this->iyzipay->live)
            );

            return (boolean) ($card->getStatus() == 'success');
        }
    }

    public function membershipsCheckOut()
    {
        $this->Auth();

        $membership_id = $this->request('post', 'membership_id', 'Memebership', 'required|trim|is_natural_no_zero');
        $redirect = $this->request('post', 'redirect', 'Redirect', 'required|trim');

        if (!$membership = getValues('*', 'memberships', "active = 1 AND id = '{$membership_id}' LIMIT 1")) {
            $this->api_messageError('Invalid Memebership.');
        }

        if (!$gateway = getValues('*', 'gateways', 'active = 1 AND name = "iyzipay" LIMIT 1')) {
            $this->api_messageError('Invalid Gateway.');
        }

        $checkoutFormInitialize = $this->_checkOutForm([
            'membership' => $membership,
            'gateway' => $gateway,
            'redirect' => $redirect,
        ]);

        if ($checkoutFormInitialize->getStatus() == 'success') {

            $json['items'] = [
                'checkoutFormContent' => $checkoutFormInitialize->getCheckoutFormContent(),
            ];

            $this->api_messageOk($json);
        }

        $this->api_messageError($checkoutFormInitialize->getErrorMessage());
    }

    public function membershipsDiscount()
    {
        $this->Auth();

        $code = $this->request('post', 'code', 'İndirim Kodu', 'required|trim');

        $discount = $this->db->first("SELECT id, price FROM mod_discounts WHERE code = BINARY('{$code}') AND status = 1");

        if (!$discount) {
            $this->api_messageError('Geçersiz İndirim Kodu');
        }

        $this->api_messageOk([
            'items' => $discount,
        ]);
    }

    private function _subscription($data)
    {
        $membership = $data['membership'];
        $gateway = $data['gateway'];

        $this->IyziPayConfigs();

        # create request class
        $request = new \Iyzipay\Request\CreatePaymentRequest();
        $request->setLocale(\Iyzipay\Model\Locale::TR);
        $request->setConversationId($data['conversation_id']);
        $request->setPrice($membership->price);
        $request->setPaidPrice($membership->price);
        $request->setCurrency($gateway->extra2);
        $request->setInstallment(1);
        $request->setPaymentChannel(\Iyzipay\Model\PaymentChannel::WEB);
        $request->setPaymentGroup(\Iyzipay\Model\PaymentGroup::PRODUCT);
        $request->setCallbackUrl(SITEURL . '/gateways/iyzipay/ipn.php?' . http_build_query([
            't' => time(),
            'redirect' => $data['redirect_url'],
        ]));

        $paymentCard = new \Iyzipay\Model\PaymentCard();
        $paymentCard->setCardUserKey($this->user->iyzipay_credit_card->cardUserKey);
        $paymentCard->setCardToken($this->user->iyzipay_credit_card->cardToken);
        $request->setPaymentCard($paymentCard);

        $buyer = new \Iyzipay\Model\Buyer();
        $buyer->setId($this->user->id);
        $buyer->setName($this->user->fname);
        $buyer->setSurname($this->user->lname);
        $buyer->setEmail($this->user->email);
        $buyer->setIdentityNumber($this->user->billing_address->identity_number);
        $buyer->setRegistrationAddress($this->Core->address);
        $buyer->setIp(sanitize($_SERVER['REMOTE_ADDR']));
        $buyer->setCity($this->user->billing_address->city);
        $buyer->setCountry('Turkey');
        $buyer->setZipCode($this->user->billing_address->zip_code);
        $request->setBuyer($buyer);

        $shippingAddress = new \Iyzipay\Model\Address();
        $shippingAddress->setContactName($this->user->billing_address->name);
        $shippingAddress->setCity($this->user->billing_address->city);
        $shippingAddress->setCountry('Turkey');
        $shippingAddress->setAddress($this->user->billing_address->address);
        $shippingAddress->setZipCode($this->user->billing_address->zip_code);
        $request->setShippingAddress($shippingAddress);

        $billingAddress = new \Iyzipay\Model\Address();
        $billingAddress->setContactName($this->user->billing_address->name);
        $billingAddress->setCity($this->user->billing_address->city);
        $billingAddress->setCountry('Turkey');
        $billingAddress->setAddress($this->user->billing_address->address);
        $billingAddress->setZipCode($this->user->billing_address->zip_code);
        $request->setBillingAddress($billingAddress);

        $basketItems = array();
        $firstBasketItem = new \Iyzipay\Model\BasketItem();
        $firstBasketItem->setId($membership->id);
        $firstBasketItem->setName($membership->title);
        $firstBasketItem->setCategory1('Memberships');
        $firstBasketItem->setItemType(\Iyzipay\Model\BasketItemType::VIRTUAL);
        $firstBasketItem->setPrice($membership->price);
        $basketItems[0] = $firstBasketItem;
        $request->setBasketItems($basketItems);

        return \Iyzipay\Model\ThreedsInitialize::create($request, Config::options($gateway->extra, $gateway->extra3, $gateway->live));

        # make request
        return \Iyzipay\Model\Payment::create(
            $request,
            Config::options($gateway->extra, $gateway->extra3, $gateway->live)
        );
    }

    private function _checkOutForm($data)
    {
        $membership = $data['membership'];
        $gateway = $data['gateway'];
        $redirect = $data['redirect'];

        require_once BASEPATH . 'gateways/iyzipay/config.php';

        # create request class
        $request = new \Iyzipay\Request\CreateCheckoutFormInitializeRequest();
        $request->setLocale(\Iyzipay\Model\Locale::TR);
        $request->setConversationId($this->user->id);
        $request->setPrice($membership->price);
        $request->setPaidPrice($membership->price);
        $request->setCurrency($gateway->extra2);
        $request->setPaymentGroup(\Iyzipay\Model\PaymentGroup::PRODUCT);
        $request->setCallbackUrl(SITEURL . '/gateways/iyzipay/ipn.php?' . http_build_query([
            'conversationid' => $this->user->id,
            't' => time(),
            'redirect' => $redirect,
        ]));
        $request->setEnabledInstallments(array(2, 3, 6, 9));

        $buyer = new \Iyzipay\Model\Buyer();
        $buyer->setId($this->user->id);
        $buyer->setName($this->user->fname);
        $buyer->setSurname($this->user->lname);
        $buyer->setEmail($this->user->email);
        $buyer->setIdentityNumber('00000000000');
        $buyer->setRegistrationAddress($this->Core->address);
        $buyer->setIp(sanitize($_SERVER['REMOTE_ADDR']));
        $buyer->setCity('Istanbul');
        $buyer->setCountry('Turkey');
        $request->setBuyer($buyer);

        $shippingAddress = new \Iyzipay\Model\Address();
        $shippingAddress->setContactName($this->user->fname . ' ' . $this->user->lname);
        $shippingAddress->setCity('Istanbul');
        $shippingAddress->setCountry('Turkey');
        $shippingAddress->setAddress($this->Core->address);
        $request->setShippingAddress($shippingAddress);

        $billingAddress = new \Iyzipay\Model\Address();
        $billingAddress->setContactName($this->user->fname . ' ' . $this->user->lname);
        $billingAddress->setCity('Istanbul');
        $billingAddress->setCountry('Turkey');
        $billingAddress->setAddress($this->Core->address);
        $request->setBillingAddress($billingAddress);

        $basketItems = array();
        $firstBasketItem = new \Iyzipay\Model\BasketItem();
        $firstBasketItem->setId($membership->id);
        $firstBasketItem->setName($membership->title_tr);
        $firstBasketItem->setCategory1('Memberships');
        $firstBasketItem->setItemType(\Iyzipay\Model\BasketItemType::VIRTUAL);
        $firstBasketItem->setPrice($membership->price);
        $basketItems[0] = $firstBasketItem;
        $request->setBasketItems($basketItems);

        # make request
        return \Iyzipay\Model\CheckoutFormInitialize::create(
            $request,
            Config::options($gateway->extra, $gateway->extra3, $gateway->live)
        );
    }

    private function IyziPayConfigs()
    {
        if (!$this->iyzipay = getValues('*', 'gateways', 'active = 1 AND name = "iyzipay" LIMIT 1')) {
            $this->api_messageError('Invalid Gateway.');
        }

        require_once BASEPATH . 'gateways/iyzipay/config.php';
    }

    public function mailSettings()
    {
        $this->Auth();

        $smtp = [
            'smtp_host' => $this->Core->smtp_host,
            'smtp_user' => $this->Core->smtp_user,
            'smtp_pass' => $this->Core->smtp_pass,
            'smtp_port' => $this->Core->smtp_port,
            'smtp_email' => $this->Core->site_email,
        ];

        $this->api_messageOk([
            'items' => $smtp,
        ]);

    }

    public function updatesGetArchives()
    {
        $this->Auth();

        $version = (int) $this->request('post', 'version', 'Version', 'required|trim');
        $branch_name = (string) $this->request('post', 'branch', 'Branch', 'required|trim');

        $updates = $this->db->fetch_all("
            SELECT * 
            FROM mod_selfupdate_archives 
            WHERE branch_name = '{$branch_name} AND id > '{$version}'
        ");


        $archives = [];
        foreach ($updates as $update) {
            if (file_exists(BASEPATH . $update->path)) {
                $archives[] = SITEURL . '/' . $update->path;
            }
        }

        $this->api_messageOk([
            'items' => $archives
        ]);
    }




    // lib/class_user.php
    private function calculateDays($membership_id)
    {
        $now = date('Y-m-d H:i:s');
        $row = $this->db->first("SELECT days, period FROM memberships WHERE id = " . (int) $membership_id);
        if ($row) {
            switch ($row->period) {
                case "D":
                $diff = $row->days;
                break;
                case "W":
                $diff = $row->days * 7;
                break;
                case "M":
                $diff = $row->days * 30;
                break;
                case "Y":
                $diff = $row->days * 365;
                break;
            }
            $expire = date("Y-m-d H:i:s", strtotime($now . +$diff . " days"));
        } else {
            $expire = "0000-00-00 00:00:00";
        }
        return $expire;
    }

    private function _callFunction()
    {
        if (empty($_GET)) {
            return false;
        }

        $this->fn = key($_GET);

        if ($this->fn == '__construct') {
            return false;
        }

        if (!method_exists(__CLASS__, $this->fn)) {
            return false;
        }

        // Call if function is public
        $reflection = new ReflectionMethod(__CLASS__, $this->fn);
        if (!$reflection->isPublic()) {
            return false;
        }

        return true;
    }

    /*



    @regex = "/ regex_string /"
     */
    private function request($method = 'post|get', $key, $input_name, $validation = 'required', $message = null)
    {
        $REQUIRED = false;
        $_METHOD = ($method == 'post') ? $_POST : $_GET;
        $str = @$_METHOD[$key];
        $rules = explode('|', $validation);

        // trim
        if (in_array('trim', $rules)) {
            $str = trim($str);
        }

        // required
        if (in_array('required', $rules)) {

            $result = false;
            if (!isset($_METHOD[$key])) {
                $result = false;
            } elseif ($str != null) {
                $result = true;
            }

            if (!$result) {
                $this->api_messageError(
                    $message !== null
                    ? $message
                    : sprintf('%s field is required.', $input_name)
                );
            }

            $REQUIRED = true;
        }

        // int
        if (in_array('int', $rules)) {
            $str = (int) $str;
        }

        // min_length
        if ($match = preg_grep('/^min_length::[0-9]+$/', $rules)) {

            $length = explode('::', reset($match));
            $length = (int) @$length[1];
            $str_len = strlen($str);

            if (!(!$REQUIRED && !$str_len) && $str_len < $length) {
                $this->api_messageError(
                    $message !== null
                    ? $message
                    : sprintf('The %s field must be at least %u characters in length.', $input_name, $length)
                );
            }
        }

        // max_length
        if ($match = preg_grep('/^max_length::[0-9]+$/', $rules)) {

            $length = explode('::', reset($match));
            $length = (int) @$length[1];
            $str_len = strlen($str);

            if ((!$REQUIRED && !$str_len) && $str_len > $length) {
                $this->api_messageError(
                    $message !== null
                    ? $message
                    : sprintf('The %s field cannot exceed %u characters in length.', $input_name, $length)
                );
            }
        }

        // exact_length
        if ($match = preg_grep('/^exact_length::[0-9]+$/', $rules)) {

            $length = explode('::', reset($match));
            $length = (int) @$length[1];
            $str_len = strlen($str);

            if (!(!$REQUIRED && !$str_len) && $str_len != $length) {
                $this->api_messageError(
                    $message !== null
                    ? $message
                    : sprintf('The %s field must be exactly %u characters in length.', $input_name, $length)
                );
            }
        }

        // regex
        if ($match = preg_grep('/^regex::(.?)+$/', $rules)) {

            $regex = explode('::', reset($match));
            $regex = @$regex[1];
            $str_len = strlen($str);

            if (!(!$REQUIRED && !$str_len) && !preg_match("/$regex/", $str)) {
                $this->api_messageError(
                    $message !== null
                    ? $message
                    : sprintf('The %s field is not in the correct format. %s', $input_name, $regex)
                );
            }
        }

        // alpha_dash
        if (preg_grep('/^alpha_dash$/', $rules)) {

            $str_len = strlen($str);

            if (!(!$REQUIRED && !$str_len) && !preg_match('/^[a-z0-9_-]+$/i', $str)) {
                $this->api_messageError(
                    $message !== null
                    ? $message
                    : sprintf('The %s field may only contain alpha-numeric characters, underscores, and dashes.', $input_name)
                );
            }
        }

        // valid_email
        if (preg_grep('/^valid_email$/', $rules)) {

            $result = false;
            $str_len = strlen($str);

            if (function_exists('filter_var')) {
                if (filter_var($str, FILTER_VALIDATE_EMAIL)) {
                    $result = true;
                }
            } elseif (preg_match('/^[a-zA-Z0-9._+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/', $str)) {
                $result = true;
            }

            if (!(!$REQUIRED && !$str_len) && !$result) {
                $this->api_messageError(
                    $message !== null
                    ? $message
                    : sprintf('The %s field must contain a valid email address.', $input_name)
                );
            }
        }

        // is_unique
        if ($match = preg_grep('/^is_unique::(.?)+$/', $rules)) {

            $str_len = strlen($str);
            $is_unique = explode('::', reset($match));
            $is_unique = @$is_unique[1];
            sscanf($is_unique, '%[^.].%[^.]', $table, $field);

            if (!(!$REQUIRED && !$str_len) && getValues('*', $table, " `$field` = '$str' ")) {
                $this->api_messageError(
                    $message !== null
                    ? $message
                    : sprintf('The %s field must contain a unique value.', $input_name)
                );
            }
        }

        // is_unique_except_me
        if ($match = preg_grep('/^is_unique_except_me::(.?)+$/', $rules)) {

            $str_len = strlen($str);
            $is_unique = explode('::', reset($match));
            $is_unique = @$is_unique[1];
            sscanf($is_unique, '%[^.].%[^.].%[^.]', $table, $field, $id);

            if (!(!$REQUIRED && !$str_len) && getValues('*', $table, " `$field` = '$str' AND `id` != $id ")) {
                $this->api_messageError(
                    $message !== null
                    ? $message
                    : sprintf('The %s field must contain a unique value.', $input_name)
                );
            }
        }

        // is_natural
        if (preg_grep('/^is_natural$/', $rules)) {

            $str_len = strlen($str);

            if (!(!$REQUIRED && !$str_len) && !ctype_digit((string) $str)) {
                $this->api_messageError(
                    $message !== null
                    ? $message
                    : sprintf('The %s field must only contain digits.', $input_name)
                );
            }
        }

        // is_natural_no_zero
        if (preg_grep('/^is_natural_no_zero$/', $rules)) {

            $str_len = strlen($str);

            if (!(!$REQUIRED && !$str_len) && (!ctype_digit((string) $str) || $str == 0)) {
                $this->api_messageError(
                    $message !== null
                    ? $message
                    : sprintf('The %s field must only contain digits and must be greater than zero.', $input_name)
                );
            }
        }

        // in_list
        if ($match = preg_grep('/^in_list::(.?)+$/', $rules)) {

            $str_len = strlen($str);
            $list = explode('::', reset($match));
            $list = @$list[1];

            if (!(!$REQUIRED && !$str_len) && !in_array($str, explode(',', $list), true)) {
                $this->api_messageError(
                    $message !== null
                    ? $message
                    : sprintf('The %s field must be one of: %s', $input_name, $list)
                );
            }
        }

        // valid_base64
        if (preg_grep('/^valid_base64$/', $rules)) {

            $str_len = strlen($str);

            if (!(!$REQUIRED && !$str_len) && !(base64_encode(base64_decode($str)) === $str)) {
                $this->api_messageError(
                    $message !== null
                    ? $message
                    : sprintf('The %s field must contain a valid base64.', $input_name)
                );
            }
        }

        // prep_url
        if (preg_grep('/^prep_url$/', $rules)) {

            if ($str === 'http://' or $str === '') {
                $str = '';
            }

            if (strpos($str, 'http://') !== 0 && strpos($str, 'https://') !== 0) {
                $str = 'http://' . $str;
            }
        }

        // valid_url
        if (preg_grep('/^valid_url$/', $rules)) {

            $result = false;
            $str_len = strlen($str);

            if (strpos($str, 'http://') !== 0 && strpos($str, 'https://') !== 0) {
                $str = 'http://' . $str;
            }

            if (preg_match("/\b(?:(?:https?|ftp):\/\/|www\.)[-a-z0-9+&@#\/%?=~_|!:,.;]*[-a-z0-9+&@#\/%=~_|]/i", $str)) {
                $result = true;
            }

            if (!(!$REQUIRED && !$str_len) && !$result) {
                $this->api_messageError(
                    $message !== null
                    ? $message
                    : sprintf('The %s field must contain a valid URL.', $input_name)
                );
            }
        }

        return $str;
    }

    private function api_messageError($str = null)
    {
        $json = ['status' => 'ERROR', 'event' => null, 'message' => (is_array($str) ? null : $str), 'count' => 0, 'items' => []];
        if (is_array($str)) {
            $json = array_merge($json, $str);
        }
        exit(json_encode($json));
    }
    private function api_messageOk($str = null)
    {
        $json = ['status' => 'SUCCESS', 'event' => null, 'message' => (is_array($str) ? null : $str), 'count' => 0, 'items' => []];
        if (is_array($str)) {
            $json = array_merge($json, $str);
        }
        exit(json_encode($json));
    }
    private function noImageFound($dir, $image, $noimage = null)
    {
        $noimage = SITEURL . ($noimage ? '/' . ltrim($noimage, '/') : '/uploads/noimage.jpg');

        if (empty($dir) || empty($image)) {
            return $noimage;
        }

        $dir = trim($dir, '/') . '/';
        $image = ltrim($image, '/');
        if (file_exists(BASEPATH . $dir . $image)) {
            return SITEURL . '/' . $dir . $image;
        }

        return $noimage;
    }
    private function fileExists($dir, $file, $if_failed_return = false)
    {
        $dir = trim($dir, '/') . '/';
        $file = ltrim($file, '/');
        if (file_exists(BASEPATH . $dir . $file)) {
            return SITEURL . '/' . $dir . $file;
        }

        return $if_failed_return;
    }
    private function audioExists($dir, $file, $suffix_session = true)
    {
        $dir = trim($dir, '/') . '/';
        $file = ltrim($file, '/');
        if (file_exists(BASEPATH . $dir . $file)) {
            $suffix_session = $suffix_session ? "&device_id=${_GET['device_id']}&device_session=${_GET['device_session']}" : null;
            return SITEURL . '/api/?audio&url=' . $dir . $file . $suffix_session;
        }

        return false;
    }
}

-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Anamakine: localhost
-- Üretim Zamanı: 05 Nis 2018, 15:22:18
-- Sunucu sürümü: 10.1.28-MariaDB
-- PHP Sürümü: 5.6.32

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Veritabanı: `posmaks_cms`
--

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `mirrormx_customer_chat_data`
--

CREATE TABLE `mirrormx_customer_chat_data` (
  `id` bigint(20) NOT NULL,
  `type` varchar(255) NOT NULL,
  `key` varchar(255) NOT NULL,
  `value` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `mirrormx_customer_chat_message`
--

CREATE TABLE `mirrormx_customer_chat_message` (
  `id` bigint(20) NOT NULL,
  `from_id` bigint(20) NOT NULL,
  `to_id` bigint(20) NOT NULL,
  `body` text NOT NULL,
  `datetime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `talk_id` bigint(20) NOT NULL,
  `is_new` char(1) NOT NULL DEFAULT 'y',
  `from_user_info` text NOT NULL,
  `to_user_info` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Tablo döküm verisi `mirrormx_customer_chat_message`
--

INSERT INTO `mirrormx_customer_chat_message` (`id`, `from_id`, `to_id`, `body`, `datetime`, `talk_id`, `is_new`, `from_user_info`, `to_user_info`) VALUES
(1, 2, 1, 'sa', '2018-04-05 12:06:38', 1, 'n', '{\"id\":2,\"name\":\"ahmet-1522933593\",\"mail\":\"ahmet@mail.com\",\"image\":\"\\/posmakscms\\/livechat\\/upload\\/default-avatars\\/a.png\",\"info\":{\"ip\":\"::1\",\"referer\":\"http:\\/\\/localhost\\/posmakscms\\/\"},\"roles\":[\"GUEST\"],\"last_activity\":\"2018-04-05 15:06:38\"}', 'all'),
(2, 1, 2, 'as', '2018-04-05 12:06:44', 1, 'n', '{\"id\":1,\"name\":\"asa\",\"mail\":\"ahmet@mail.com\",\"roles\":[\"OPERATOR\"],\"last_activity\":\"2018-04-05 15:06:44\",\"7\":\"2018-04-05 15:06:44\"}', '{\"id\":2,\"name\":\"ahmet-1522933593\",\"mail\":\"ahmet@mail.com\",\"image\":\"\\/posmakscms\\/livechat\\/upload\\/default-avatars\\/a.png\",\"info\":{\"ip\":\"::1\",\"referer\":\"http:\\/\\/localhost\\/posmakscms\\/\"},\"roles\":[\"GUEST\"],\"last_activity\":\"2018-04-05 15:06:43\"}'),
(3, 2, -1, 'naptın', '2018-04-05 12:06:51', 1, 'y', '{\"id\":2,\"name\":\"ahmet-1522933593\",\"mail\":\"ahmet@mail.com\",\"image\":\"\\/posmakscms\\/livechat\\/upload\\/default-avatars\\/a.png\",\"info\":{\"ip\":\"::1\",\"referer\":\"http:\\/\\/localhost\\/posmakscms\\/\"},\"roles\":[\"GUEST\"],\"last_activity\":\"2018-04-05 15:06:48\"}', 'all'),
(4, 1, 3, 'orda mısın', '2018-04-05 12:15:43', 2, 'n', '{\"id\":1,\"name\":\"asa\",\"mail\":\"ahmet@mail.com\",\"roles\":[\"OPERATOR\"],\"last_activity\":\"2018-04-05 15:15:38\",\"7\":\"2018-04-05 15:15:38\"}', '{\"id\":3,\"name\":\"asa-1522934115\",\"mail\":\"asa@mail.com\",\"image\":\"\\/posmakscms\\/livechat\\/upload\\/default-avatars\\/a.png\",\"info\":{\"ip\":\"::1\",\"referer\":\"http:\\/\\/localhost\\/posmakscms\\/\"},\"roles\":[\"GUEST\"],\"last_activity\":\"2018-04-05 15:15:40\"}'),
(5, 3, -1, '[ user has closed the chat ]', '2018-04-05 12:15:53', 2, 'y', '{\"id\":3,\"name\":\"asa-1522934115\",\"mail\":\"asa@mail.com\",\"image\":\"\\/posmakscms\\/livechat\\/upload\\/default-avatars\\/a.png\",\"info\":{\"ip\":\"::1\",\"referer\":\"http:\\/\\/localhost\\/posmakscms\\/\"},\"roles\":[\"GUEST\"],\"last_activity\":\"2018-04-05 15:15:50\"}', 'all');

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `mirrormx_customer_chat_user`
--

CREATE TABLE `mirrormx_customer_chat_user` (
  `id` bigint(20) NOT NULL,
  `name` char(32) NOT NULL,
  `mail` char(64) NOT NULL,
  `password` char(255) NOT NULL,
  `image` char(255) DEFAULT NULL,
  `info` text,
  `roles` char(128) DEFAULT NULL,
  `last_activity` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Tablo döküm verisi `mirrormx_customer_chat_user`
--

INSERT INTO `mirrormx_customer_chat_user` (`id`, `name`, `mail`, `password`, `image`, `info`, `roles`, `last_activity`) VALUES
(1, 'asa', 'ahmet@mail.com', '040bd08a4290267535cd247b8ba2eca129d9fe9f', NULL, NULL, 'OPERATOR', '2018-04-05 12:16:03'),
(2, 'ahmet-1522933593', 'ahmet@mail.com', 'x', '/posmakscms/livechat/upload/default-avatars/a.png', '{\"ip\":\"::1\",\"referer\":\"http:\\/\\/localhost\\/posmakscms\\/\"}', 'GUEST', '2018-04-05 12:22:14'),
(3, 'asa-1522934115', 'asa@mail.com', 'x', '/posmakscms/livechat/upload/default-avatars/a.png', '{\"ip\":\"::1\",\"referer\":\"http:\\/\\/localhost\\/posmakscms\\/\"}', 'GUEST', '2018-04-05 12:15:50');

--
-- Dökümü yapılmış tablolar için indeksler
--

--
-- Tablo için indeksler `mirrormx_customer_chat_data`
--
ALTER TABLE `mirrormx_customer_chat_data`
  ADD PRIMARY KEY (`id`),
  ADD KEY `type` (`type`),
  ADD KEY `key` (`key`);

--
-- Tablo için indeksler `mirrormx_customer_chat_message`
--
ALTER TABLE `mirrormx_customer_chat_message`
  ADD PRIMARY KEY (`id`);

--
-- Tablo için indeksler `mirrormx_customer_chat_user`
--
ALTER TABLE `mirrormx_customer_chat_user`
  ADD PRIMARY KEY (`id`);

--
-- Dökümü yapılmış tablolar için AUTO_INCREMENT değeri
--

--
-- Tablo için AUTO_INCREMENT değeri `mirrormx_customer_chat_data`
--
ALTER TABLE `mirrormx_customer_chat_data`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- Tablo için AUTO_INCREMENT değeri `mirrormx_customer_chat_message`
--
ALTER TABLE `mirrormx_customer_chat_message`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- Tablo için AUTO_INCREMENT değeri `mirrormx_customer_chat_user`
--
ALTER TABLE `mirrormx_customer_chat_user`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

<?php

return array (
  'dbHost' => '127.0.0.1',
  'dbPort' => '3306',
  'dbUser' => 'root',
  'dbPassword' => '',
  'dbName' => 'posmaks_cms',
  'superUser' => 'admin',
  'superPass' => '123123',
  'services' => 
  array (
    'mailer' => 
    array (
      'smtp' => '',
      'smtpSecure' => 'ssl',
      'smtpHost' => '',
      'smtpPort' => '465',
      'smtpUser' => '',
      'smtpPass' => '',
    ),
  ),
  'appSettings' => 
  array (
    'contactMail' => 'admin@posmakscms.com',
  ),
);

?>
<?php
  /**
   * 404
   *
   * @package Veriasist
   * @author veriasist.com
   * @copyright 2014
   * @version $Id: 404.php, v4.00 2014-01-10 10:12:05 gewa Exp $
   */
  define("_VALID_PHP", true);
  require_once("init.php");
?>

<?php require_once (THEMEDIR . "/404.tpl.php");?>

<?php
  /**
   * Send Mail
   *
   * @package VeriAsist
   * @author veriasist.com
   * @copyright 2014
   * @version $Id: sendmail.php, v4.00 2014-04-20 10:12:05 gewa Exp $
   */
  define("_VALID_PHP", true);
  require_once("../../init.php");
?>
<?php
  $post = (!empty($_POST)) ? true : false;



  if ($post && isset($_POST['processRequestDemo'])) {
      Filter::checkPost("name", Lang::$word->_CF_NAME_R);
      Filter::checkPost("email", Lang::$word->_CF_EMAIL_R);
      Filter::checkPost("phone", Lang::$word->_CF_GENDER_R);
      Filter::checkPost("company_title", Lang::$word->_CF_GENDER_R);
      
      if (!preg_match("/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,4})$/", $_POST['email']))
          Filter::$msgs['email'] = Lang::$word->_CF_EMAIL_ERR;
      
	  Filter::checkPost("code", Lang::$word->_CF_TOTAL_R);
      
	  if ($_SESSION['captchacode'] != $_POST['code'])
          Filter::$msgs['code'] = Lang::$word->_CF_TOTAL_ERR;

      if (!isset($_POST['projects']) && !isset($_POST['solutions'])) {
      	Filter::$msgs['p_s'] = Lang::$word->_SELECT_ON_PROJECT_OR_SOLUTION;
      }
      		

      
      if (empty(Filter::$msgs)) {
          
          $projects = NULL;
          if (isset($_POST['projects[]'])) {
          	foreach ($_POST['projects[]'] as $project) {
          		$projects .= '<li>'.sanitize($project).'</li>';
          	}
          }

          $solutions = NULL;
          if (isset($_POST['solutions[]'])) {
          	foreach ($_POST['solutions[]'] as $solution) {
          		$solutions .= '<li>'.sanitize($solution).'</li>';
          	}
          }

          $list = '<ul>'.$projects.'<li>--------</li>'.$solutions.'</ul>';
          
          $sender_email = sanitize($_POST['email']);
          $name = sanitize($_POST['name']);
		  $phone = sanitize($_POST['phone']);
		  $company_title = sanitize($_POST['company_title']);
		  $ip = sanitize($_SERVER['REMOTE_ADDR']);

		  require_once(BASEPATH . "lib/class_mailer.php");
		  $mailer = Mailer::sendMail();	
					  		  
		  $row = Registry::get("Core")->getRowById(Content::eTable, 17);
		  
		  $body = str_replace(array('[LIST]', '[SENDER]', '[NAME]', '[PHONE]', '[COMPANY]', '[IP]'), 
		  array($list, $sender_email, $name, $phone, $company_title, $ip), $row->{'body'.Lang::$lang});

		  $msg = Swift_Message::newInstance()
					->setSubject('Sunum Talebi - ' . $name)
					->setTo(array($core->site_email => $core->site_name))
					->setFrom(array($core->site_email => $core->site_name))
					->setBody(cleanOut($body), 'text/html');


		  if ($mailer->send($msg)) {
			  $json['status'] = 'success';
			  $json['message'] = Filter::msgOk(Lang::$word->_CF_OK, false);
			  Security::writeLog(Lang::$word->_USER . ' ' . $user->username . ' ' . Lang::$word->_LG_CONTACT_SENT, "", "no", "contact");
			  print json_encode($json);
		  } else {
			  $json['message'] = Filter::msgAlert(Lang::$word->_CF_ERROR, false);
			  print json_encode($json);
			  Security::writeLog(Lang::$word->_CF_ERROR, "", "yes", "contact");
		  }

      } else {
		  $json['message'] = Filter::msgStatus();
		  print json_encode($json);
	  }
  }
?>
<?php
  /**
   * Portfolio Main
   *
   * @package VeriAsist
   * @author veriasist.com
   * @copyright 2014
   * @version $Id: main.php, v4.00 2014-04-20 16:18:34 gewa Exp $
   */
  
  if (!defined("_VALID_PHP"))
      die('Direct access to this location is not allowed.');
	  
  require_once (MODPATH . "portfolio/admin_class.php");
  $classname = 'Portfolio';
  try {
	  if (!class_exists($classname)) {
		  throw new exception('Missing digishop/admin_class.php');
	  }
	  Registry::set('Portfolio', new Portfolio());
  }
  catch (exception $e) {
	  echo $e->getMessage();
  }
  
  $length = count($content->_url);
?>
<?php switch($length): case 3: ?>
<?php $catrow = Registry::get("Portfolio")->renderCategory($content->moduledata->mod->id);?>
<?php if(!$catrow):?>
<?php echo Filter::msgSingleAlert(Lang::$word->_MOD_PF_NOPROJECTS);?>
<?php else:?>
<h1 class="veriasist double header"><span><?php echo Lang::$word->_MOD_PF_ALL_PROJECTS;?> / <?php echo $content->moduledata->mod->{'title' . Lang::$lang};?></span></h1>
<?php $catsrow = Registry::get("Portfolio")->getCategories();?>
<?php if($catsrow):?>
<div class="veriasist basic buttons"> <a href="<?php echo doUrl(false, false, "portfolio");?>" class="veriasist black button"><span><?php echo Lang::$word->_MOD_PF_ALL;?></span></a>
  <?php foreach($catsrow as $fcrow):?>
  <a class="veriasist <?php echo ($content->_url[2] == $fcrow->slug)? "active" : "black";?> button" href="<?php echo doUrl(false, $fcrow->slug, "portfolio-cat");?>" data-category="<?php echo $fcrow->id;?>"><?php echo $fcrow->{'title'.Lang::$lang};?></a>
  <?php endforeach;?>
</div>
<?php endif;?>
<div class="veriasist section divider"></div>
<div id="foliowrap" class="clearfix">
  <?php foreach($catrow as $row):?>
  <div class="item">
    <section>
      <div class="image-overlay"> <img src="<?php echo SITEURL.'/'.Portfolio::imagepath . $row->thumb;?>" alt="">
        <div class="overlay-fade"> <a href="<?php echo doUrl(false, $row->slug, "portfolio-item");?>"><i class="icon-overlay icon url"></i></a> </div>
      </div>
      <h4><?php echo $row->{'title' . Lang::$lang};?> </h4>
      <p><?php echo cleanSanitize($row->{'short_desc' . Lang::$lang});?></p>
    </section>
  </div>
  <?php endforeach;?>
</div>
<?php endif;?>
<div id="pagination" class="content-center"><?php echo $pager->display_pages();?></div>
<script type="text/javascript">
// <![CDATA[
$(document).ready(function () {
	$('#foliowrap').waitForImages(function () {
		$('#foliowrap').Grid({
			inner: 20,
			outer: 0,
			cols: Math.round(1200 / <?php echo Registry::get("Portfolio")->cols;?>)
		});
	});
});
// ]]>
</script>
<?php break;?>
<?php case 2: ?>
<?php $row = $content->moduledata->mod;?>
<h1 class="veriasist double header"><span><?php echo Lang::$word->_MOD_PF_V_ITEM . $row->{'title' . Lang::$lang};?></span></h1>
<div id="folio-single">
  <div class="columns gutters">
    <div class="screen-50 tablet-50 phone-100">
      <?php if($row->gallery):?>
      <?php include(MODPATHF . "/portfolio/gallery.tpl.php");?>
      <?php else:?>
      <div class="pitem">
        <div class="image-overlay"> <img src="<?php echo SITEURL.'/'.Portfolio::imagepath . $row->thumb;?>" alt="">
          <div class="overlay-fade"> <a title="<?php echo $row->{'title' . Lang::$lang};?>" href="<?php echo SITEURL.'/'.Portfolio::imagepath . $row->thumb;?>" class="lightbox"><i class="icon-overlay icon url"></i></a> </div>
        </div>
      </div>
      <?php endif;?>
    </div>
    <div class="screen-50 tablet-50 phone-100"> <?php echo cleanOut($row->{'short_desc'.Lang::$lang});?>
      <div class="veriasist section divider"></div>
      <h4 class="veriasist header"> <?php echo Lang::$word->_MOD_PF_KEY;?></h4>
      <div class="veriasist divided list">
        <div class="item"> <i class="globe icon"></i>
          <div class="content">
            <div class="header"><?php echo Lang::$word->_MOD_PF_RWEB;?></div>
            <div class="description"><?php echo $row->www;?></div>
          </div>
        </div>
        <div class="item"> <i class="calendar icon"></i>
          <div class="content">
            <div class="header"><?php echo Lang::$word->_MOD_PF_RDATE;?></div>
            <div class="description"><?php echo Filter::doDate("short_date", $row->created);?></div>
          </div>
        </div>
        <div class="item"> <i class="user icon"></i>
          <div class="content">
            <div class="header"><?php echo Lang::$word->_MOD_PF_RCLIENT;?></div>
            <div class="description"><?php echo $row->client;?></div>
          </div>
        </div>
        <div class="item"> <i class="building icon"></i>
          <div class="content">
            <div class="header"><?php echo Lang::$word->_MOD_PF_RLOC;?></div>
            <div class="description"><?php echo $row->location;?></div>
          </div>
        </div>
      </div>
      <a href="<?php echo doUrl(false, false, "portfolio");?>" class="veriasist basic labeled icon button"><i class="left arrow icon"></i><?php echo Lang::$word->_MOD_PF_BACK;?></a> </div>
  </div>
  <ul class="veriasist tabs">
    <li><a data-tab="#tab1"><?php echo Lang::$word->_MOD_PF_PSTUDY;?></a></li>
    <li><a data-tab="#tab2"><?php echo Lang::$word->_MOD_PF_PAPPROACH;?></a></li>
    <li><a data-tab="#tab3"><?php echo Lang::$word->_MOD_PF_RESULTS;?></a></li>
  </ul>
  <div class="veriasist bottom attached secondary segment">
    <div id="tab1" class="veriasist tab content"> <?php echo cleanOut($row->{'detail'.Lang::$lang});?> </div>
    <div id="tab2" class="veriasist tab content"> <?php echo cleanOut($row->{'body'.Lang::$lang});?> </div>
    <div id="tab3" class="veriasist tab content"> <?php echo cleanOut($row->{'result'.Lang::$lang});?> </div>
  </div>
</div>
<?php break;?>
<?php default: ?>
<?php $portadata = Registry::get("Portfolio")->renderPortfolio();?>
<?php $catrow = Registry::get("Portfolio")->getCategories();?>
<h1 class="veriasist double header"><span><?php echo Lang::$word->_MOD_PF_ALL_PROJECTS;?></span></h1>
<?php if($catrow):?>
<div class="veriasist basic buttons"> <a class="veriasist active button"><span><?php echo Lang::$word->_MOD_PF_ALL;?></span></a>
  <?php foreach($catrow as $fcrow):?>
  <a class="veriasist black button" href="<?php echo doUrl(false, $fcrow->slug, "portfolio-cat");?>" data-category="<?php echo $fcrow->id;?>"><?php echo $fcrow->{'title'.Lang::$lang};?></a>
  <?php endforeach;?>
</div>
<?php endif;?>
<div class="veriasist section divider"></div>
<div id="foliowrap" class="clearfix">
  <?php if(!$portadata):?>
  <?php echo Filter::msgSingleAlert(Lang::$word->_MOD_PF_NOPROJECTS);?>
  <?php else:?>
  <?php foreach($portadata as $row):?>
  <div class="item">
    <section>
      <div class="image-overlay"> <img src="<?php echo SITEURL.'/'.Portfolio::imagepath . $row->thumb;?>" alt="">
        <div class="overlay-fade"> <a href="<?php echo doUrl(false, $row->slug, "portfolio-item");?>"><i class="icon-overlay icon url"></i></a> </div>
      </div>
      <h4><?php echo $row->{'title' . Lang::$lang};?> </h4>
      <p><?php echo cleanSanitize($row->{'short_desc' . Lang::$lang});?></p>
    </section>
  </div>
  <?php endforeach;?>
</div>
<div id="pagination" class="content-center"><?php echo $pager->display_pages();?></div>
<?php endif;?>
<script type="text/javascript">
// <![CDATA[
$(document).ready(function () {
	$('#foliowrap').waitForImages(function () {
		$('#foliowrap').Grid({
			inner: 20,
			outer: 0,
			cols: Math.round(1200 / <?php echo Registry::get("Portfolio")->cols;?>)
		});
	});
});
// ]]>
</script>
<?php break;?>
<?php endswitch;?>
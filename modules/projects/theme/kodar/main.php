<?php
  /**
   * Projects Main
   *
   * @package VeriAsist
   * @author veriasist.com
   * @copyright 2014
   * @version $Id: main.php, v4.00 2014-04-20 16:18:34 gewa Exp $
   */
  
  if (!defined("_VALID_PHP"))
      die('Direct access to this location is not allowed.');
    
  require_once (MODPATH . "projects/admin_class.php");
  $classname = 'Projects';
  try {
    if (!class_exists($classname)) {
      throw new exception('Missing projects/admin_class.php');
    }
    Registry::set('Projects', new Projects());
  }
  catch (exception $e) {
    echo $e->getMessage();
  }

  $length = count($content->_url);

?>
<?php switch($length): case 3: ?>
<?php //$catrow = Registry::get("Projects")->renderCategory($content->moduledata->mod->id);?>
<?php $portadata = Registry::get("Projects")->renderProjects();?>
<?php $catrow = Registry::get("Projects")->getCategories();?>




<section class="no-padding work-3col wow fadeIn ">
  <div class="container">

    <div class="row margin-nine no-margin-top no-margin-lr">
      <div class="col-md-12 col-sm-12 text-center">
        <h2 class="title-large alt-font text-uppercase letter-spacing-2 white-text display-block font-weight-600 margin-one no-margin-top"><?php echo $content->moduledata->{'title' . Lang::$lang}; ?></h2>
        <div class="no-margin-top"><img src="<?php echo THEMEURL; ?>/images/separator3.png" alt=""></div>
      </div>
    </div>

    <div class="row xs-text-center">
      <div class="col-lg-12 col-md-12 text-center display-inline md-text-center md-margin-five md-no-margin-lr md-no-margin-bottom ">
        <!-- filter navigation -->
        <ul class="portfolio-filter nav nav-tabs no-border portfolio-filter-tab alt-font text-uppercase letter-spacing-1 margin-seven no-margin-lr no-margin-bottom xs-margin-one-half xs-no-margin-bottom xs-no-margin-lr">
          <?php if($catrow):?>
            <li class="nav xs-display-inline active">
              <a href="#" data-filter="*" class="xs-display-inline"><?php echo Lang::$word->_ALL; ?></a>
            </li>
            <?php foreach($catrow as $key => $fcrow):?>
              <li class="nav xs-display-inline">
                <a href="#" data-filter=".<?= $fcrow->slug ?>" class="xs-display-inline"><?php echo $fcrow->{'title'.Lang::$lang};?></a>
              </li>
            <?php endforeach;?>
          <?php endif; ?>
        </ul>
        <!-- end filter navigation -->
      </div>
    </div>

    <div class="row">
      <div class="grid-gallery grid-style-projects overflow-hidden">
        <div class="tab-content">
          <ul class="masonry-items grid">

            <?php foreach ($portadata as $key => $row): ?>
              <!-- project item -->
              <li class="html <?= $row->category_slug ?>">
                <figure>
                  <div class="gallery-img">
                    <a href="<?php echo doUrl(false, $row->slug, "projects-item"); ?>"><img src="<?php echo SITEURL.'/'.Projects::imagepath . $row->thumb;?>" alt=""></a>
                  </div>
                  <figcaption>
                    <h3 class="alt-font text-uppercase letter-spacing-2">
                      <p><a href="<?php echo doUrl(false, $row->slug, "projects-item"); ?>"><?php echo $row->{'title' . Lang::$lang};?></a></p>
                    </h3>
                  </figcaption>
                </figure>
              </li>
              <!-- end project item -->
            <?php endforeach; ?>

          </ul>
        </div>
      </div>
    </div>

  </div>
</section>




<?php if (0): ?>
  

<?php if(!$catrow):?>
<?php echo Filter::msgSingleAlert(Lang::$word->_MOD_WRKS_NOPROJECTS);?>
<?php else:?>
<h1 class="veriasist double header"><span><?php echo Lang::$word->_MOD_WRKS_ALL_PROJECTS;?> / <?php echo $content->moduledata->mod->{'title' . Lang::$lang};?></span></h1>
<?php $catsrow = Registry::get("Projects")->getCategories();?>
<?php if($catsrow):?>
<div class="veriasist basic buttons"> <a href="<?php echo doUrl(false, false, "projects");?>" class="veriasist black button"><span><?php echo Lang::$word->_MOD_WRKS_ALL;?></span></a>
  <?php foreach($catsrow as $fcrow):?>
  <a class="veriasist <?php echo ($content->_url[2] == $fcrow->slug)? "active" : "black";?> button" href="<?php echo doUrl(false, $fcrow->slug, "projects-cat");?>" data-category="<?php echo $fcrow->id;?>"><?php echo $fcrow->{'title'.Lang::$lang};?></a>
  <?php endforeach;?>
</div>
<?php endif;?>
<div class="veriasist section divider"></div>
<div id="foliowrap" class="clearfix">
  <?php foreach($catrow as $row):?>
  <div class="item">
    <section>
      <div class="image-overlay"> <img src="<?php echo SITEURL.'/'.Projects::imagepath . $row->thumb;?>" alt="">
        <div class="overlay-fade"> <a href="<?php echo doUrl(false, $row->slug, "projects-item");?>"><i class="icon-overlay icon url"></i></a> </div>
      </div>
      <h4><?php echo $row->{'title' . Lang::$lang};?> </h4>
      <p><?php echo cleanSanitize($row->{'short_desc' . Lang::$lang});?></p>
    </section>
  </div>
  <?php endforeach;?>
</div>
<?php endif;?>
<div id="pagination" class="content-center"><?php echo $pager->display_pages();?></div>
<script type="text/javascript">
// <![CDATA[
$(document).ready(function () {
  $('#foliowrap').waitForImages(function () {
    $('#foliowrap').Grid({
      inner: 20,
      outer: 0,
      cols: Math.round(1200 / <?php echo Registry::get("Projects")->cols;?>)
    });
  });
});
// ]]>
</script>
<?php endif; ?>



<?php break;?>
<?php case 2: ?>
<?php $row = $content->moduledata->mod;?>
<?php $pagination = ($row) ? Registry::get("Projects")->getPrevNext() : FALSE;?>



<!-- FORM -->
<a href="#mpf-popup-form" class="mpf-popup-form btn-success btn btn-medium button btn-round" data-toggle="modal"><?php echo Lang::$word->DEMO_REQ_TITLE; ?></a>
<!-- FORM -->

<!-- PREV-NEXT -->
<?php if (@$pagination->prev): ?>
  <?php $tmp = @explode('{|}', $pagination->prev); ?>
  <a href="<?php echo doUrl(false, @$tmp[ 1 ], "projects-item"); ?>" class="mfp-arrow mfp-arrow-left mfp-prevent-close"> <div class="mfp-arrow-left-popup"><?php echo @$tmp[ 0 ]; ?></div></a>
<?php endif; ?>
<?php if (@$pagination->next): ?>
  <?php $tmp1 = @explode('{|}', $pagination->next); ?>
  <a href="<?php echo doUrl(false, @$tmp1[ 1 ], "projects-item"); ?>" class="mfp-arrow mfp-arrow-right mfp-prevent-close"> <div class="mfp-arrow-right-popup"><?php echo @$tmp1[ 0 ]; ?></div></a>
<?php endif; ?>
<!-- PREV-NEXT -->


<?php if(@$row->layout == 'l'): ?>
<!-- LAYOUT LEFT -->
<div class="popup-main bg-white">
  <section class="no-padding">
    <div class="container-fluid">
      <div class="row">
        
        <!-- slider item-->
        <div class="col-lg-6 col-md-12 col-sm-12 no-padding">
          <?php if($row->gallery):?>
            <?php include(MODPATHF . "/projects/gallery.tpl.php");?>  
          <?php else: ?>
            <div class="col-lg-12 col-md-12 col-sm-12 cover-background" style="background-image:url('<?php echo SITEURL.'/'.Projects::imagepath . $row->thumb;?>');">
              <div class="opacity-full bg-dark-gray"></div>
              <div class="full-screen ajax-popup-title position-relative">
                <div class="slider-typography text-center">
                  <div class="slider-text-middle-main">
                    <div class="slider-text-middle">
                      <span class="alt-font font-weight-600 white-text title-extra-large-3 letter-spacing-2 text-uppercase display-block"><?php echo $row->{'title' . Lang::$lang};?></span>                     
                      <div class="bg-fast-yellow separator-line-thick-long margin-seven margin-lr-auto no-margin-bottom"></div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          <?php endif; ?>
        </div>
        <!--end slider item --> 


        <!-- popup text -->
        <div class="col-lg-6 col-md-12 col-sm-12 padding-eighteen no-padding-bottom">
          <div class="padding-four no-padding-lr no-padding-top border-bottom">
            <span class="alt-font deep-blue-text font-weight-600 title-extra-large letter-spacing-2 text-uppercase display-block"><?php echo $row->{'title' . Lang::$lang};?></span>
            <p class="text-extra-large margin-seven no-margin-bottom no-margin-lr display-inline-block alt-font black-text no-padding-bottom cursor-default"><?php echo cleanSanitize($row->{'short_desc' . Lang::$lang});?></p>

			
          </div>
          <div class="padding-four no-padding-lr pull-left ajax-popup-content">
            
            <?php if ($row->{'detail' . Lang::$lang}): ?>
			  
			<div class="cursor-default">

				<div class="clearfix"><?php echo cleanOut($row->{'detail'.Lang::$lang});?></div>
			  
            </div>

            <?php endif; ?>
<br>
			
            <?php if ($row->{'body' . Lang::$lang}): ?>
			
			
			<div class="cursor-default">

				<div class="clearfix"><?php echo cleanOut($row->{'body'.Lang::$lang});?></div>
			  
            </div>
			  
            <?php endif; ?>
            
            <?php if ($row->{'result' . Lang::$lang}): ?>

			<div class="cursor-default">

				<div class="clearfix"><?php echo cleanOut($row->{'result'.Lang::$lang});?></div>
			  
            </div>
			  
            <?php endif; ?>

            <?php if ($row->www): ?>
              <p class="text-medium text-center">
                <a class="btn highlight-button-black-border btn-medium margin-three no-margin-bottom no-margin-lr" href="<?php echo $row->www; ?>" target="_blank"><?php echo ($row->web_text ? $row->web_text : $row->www); ?></a>
              </p>
            <?php endif; ?>
            
          </div>
        </div>
        <!-- end popup text -->
      </div>
    </div>
  </section>
</div>
<!-- LAYOUT LEFT -->


<?php elseif(@$row->layout == 't'): ?>
<!-- LAYOUT TOP -->
<div class="popup-main bg-white">
  <?php if($row->gallery):?>
    <!-- slider item-->
      <?php include(MODPATHF . "/projects/gallery.tpl.php");?>
    <!--end slider item -->
  <?php else: ?>
    <div id="t-no-gallery" class="col-lg-12 col-md-12 col-sm-12 cover-background" style="background-image:url('<?php echo SITEURL.'/'.Projects::imagepath . $row->thumb;?>');">
      <div class="opacity-full bg-dark-gray"></div>
      <div class="full-screen ajax-popup-title">
        <div class="slider-typography text-center">
          <div class="slider-text-middle-main">
            <div class="slider-text-middle">
              <span class="alt-font font-weight-600 white-text title-extra-large-3 letter-spacing-2 text-uppercase display-block"><?php echo $row->{'title' . Lang::$lang};?></span>                     
            </div>
          </div>
        </div>
      </div>
    </div>
  <?php endif; ?>

  <?php if ($row->{'short_desc' . Lang::$lang}): ?>        
    <!-- popup text -->
<div class="col-lg-12 col-md-12 col-sm-12 no-padding-bottom">
      <div class="container">
        <div class="row">
          <div class="col-md-12 col-sm-10 text-center center-col cursor-default padding-four">
            <span class="alt-font font-weight-600 deep-blue-text title-extra-large letter-spacing-2 text-uppercase display-block xs-title-medium"><?php echo $row->{'title' . Lang::$lang};?></span>
          </div>
        </div>
      </div>
</div>			
			
<div class="container">
<div class="margin-ten no-margin-lr xs-no-margin">
    <div class="row">
        <div class="col-md-12 col-sm-3 xs-margin-five xs-no-margin-lr xs-no-margin-top">
            <p class="text-extra-large margin-four no-margin-lr no-margin-top display-inline-block alt-font black-text no-padding-bottom cursor-default"><?php echo cleanSanitize($row->{'short_desc' . Lang::$lang});?></p>
		</div>
    <!-- end popup text -->
  <?php endif; ?>

  <?php if ($row->{'body' . Lang::$lang}): ?>

      
        <div>
          <!-- popup body -->
          

              <div class="col-md-6 col-sm-3 xs-margin-five xs-no-margin-lr xs-no-margin-top cursor-default">

			    <div class="clearfix"><?php echo cleanOut($row->{'detail'.Lang::$lang});?></div>
			  
            </div>
          <!-- end popup body -->
        </div>

  <?php endif; ?>

  <?php if ($row->{'detail' . Lang::$lang}): ?>

        <div>
          <!-- popup details -->
          
            <div class="col-md-6 col-sm-3 xs-margin-five xs-no-margin-lr xs-no-margin-top cursor-default">
			  <div class="clearfix"><?php echo cleanOut($row->{'body'.Lang::$lang});?></div>
			  
            </div>
          
          <!-- end popup details -->
        </div>
      </div>

  <?php endif; ?>

  <?php if ($row->www): ?>

      <div class="container">
        <div class="row">
          <!-- call to action button -->
          <div class="col-md-12 text-center">
            <span class="alt-font display-block text-uppercase padding-two cursor-default"><?php echo cleanOut($row->{'result'.Lang::$lang});?></span>
            <?php if ($row->www): ?>
              <a class="btn highlight-button-black-border btn-medium no-margin-bottom no-margin-lr" href="<?php echo $row->www; ?>" target="_blank"><?php echo ($row->web_text ? $row->web_text : $row->www); ?></a>
            <?php endif; ?>
          </div>
          <!-- end call to action button -->
        </div>
      </div>

  <?php endif ?>

</div></div>
<!-- LAYOUT TOP -->
<?php endif; ?>


<!-- Form Modal -->
<div class="modal fade" id="mpf-popup-form">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title"><?php echo Lang::$word->DEMO_REQ_TITLE_FORM;?></h4>
      </div>
      <div class="modal-body">
        
        <form id="veriasist_form" class="veriasist form" action="javascript:void(0)" method="post">
          <div id="success" class="no-margin-lr"></div>

          <input type="text" name="name" placeholder="<?php echo Lang::$word->_CF_NAME;?>" value="<?php if ($user->logged_in) echo trim($user->name);?>" class="big-input alt-font">
          <input type="text" name="email" placeholder="<?php echo Lang::$word->_CF_EMAIL;?>" value="<?php if ($user->logged_in) echo $user->email;?>" class="big-input alt-font">
          <input type="text" name="phone" placeholder="<?php echo Lang::$word->_CF_PHONE;?>" class="big-input alt-font">
          <input type="text" name="company_title" placeholder="Şirket / Ünvan<?php //echo Lang::$word->_COMPANY;?>" class="big-input alt-font">
          
          <div>
            <input type="text" name="code" placeholder="<?php echo Lang::$word->_CF_TOTAL;?>" class="big-input alt-font">
            <img src="<?php echo SITEURL;?>/captcha.php" alt="" class="captcha-img" style="top: 19.9em;" />
          </div>

          <div class="col-sm-5">
            <h4><?php echo Lang::$word->_MOD_WRKS_ALL_PROJECTS; ?></h4>
            <?php foreach (Registry::get("Projects")->renderProjects() as $p): ?>
              <div class="row big-input cbox-input">
                <div class="col-sm-2" style="padding-left: 0; margin-right: -10px;">
                  <input type="checkbox" name="projects" value="<?php echo $p->{'title' . Lang::$lang}; ?>" id="p_<?=$p->id?>">
                </div>
                <div class="col-sm-10" style="padding-left: 0;" for="p_<?=$p->id?>">
                  <label for="p_<?=$p->id?>"><?php echo $p->{'title' . Lang::$lang}; ?></label>
                </div>
              </div>
            <?php endforeach; ?>
          </div>        
          
          <div class="row">
            <div class="col-sm-12">
              <div id="msgholder" class=" padding-one"></div>
            </div>
          </div>

          <input name="processRequestDemo" type="hidden" value="1">
        </form>

        <div class="row">
          <div class="col-sm-12 text-right padding-four">
            <a href="#home" aria-controls="home" role="tab" data-toggle="tab" class="highlight-button-dark btn btn-medium"><?php echo Lang::$word->_CANCEL; ?></a>
            <button data-url="/modules/projects/sendmail.php" type="button" name="dosubmit" class="highlight-button-dark btn btn-medium"><?php echo Lang::$word->_CF_SEND;?></button>
          </div>
        </div>

      </div>
    </div>
  </div>
</div>







<?php if (0): ?>
<h1 class="veriasist double header"><span><?php echo Lang::$word->_MOD_WRKS_V_ITEM . $row->{'title' . Lang::$lang};?></span></h1>
<div id="folio-single">
  <div class="columns gutters">
    <div class="screen-50 tablet-50 phone-100">
      <?php if($row->gallery):?>
      <?php include(MODPATHF . "/projects/gallery.tpl.php");?>
      <?php else:?>
      <div class="pitem">
        <div class="image-overlay"> <img src="<?php echo SITEURL.'/'.Projects::imagepath . $row->thumb;?>" alt="">
          <div class="overlay-fade"> <a title="<?php echo $row->{'title' . Lang::$lang};?>" href="<?php echo SITEURL.'/'.Projects::imagepath . $row->thumb;?>" class="lightbox"><i class="icon-overlay icon url"></i></a> </div>
        </div>
      </div>
      <?php endif;?>
    </div>
    <div class="screen-50 tablet-50 phone-100"> <?php echo cleanOut($row->{'short_desc'.Lang::$lang});?>
      <div class="veriasist section divider"></div>
      <h4 class="veriasist header"> <?php echo Lang::$word->_MOD_WRKS_KEY;?></h4>
      <div class="veriasist divided list">
        <div class="item"> <i class="globe icon"></i>
          <div class="content">
            <div class="header"><?php echo Lang::$word->_MOD_WRKS_RWEB;?></div>
            <div class="description"><?php echo $row->www;?></div>
          </div>
        </div>
        <div class="item"> <i class="calendar icon"></i>
          <div class="content">
            <div class="header"><?php echo Lang::$word->_MOD_WRKS_RDATE;?></div>
            <div class="description"><?php echo Filter::doDate("short_date", $row->created);?></div>
          </div>
        </div>
        <div class="item"> <i class="user icon"></i>
          <div class="content">
            <div class="header"><?php echo Lang::$word->_MOD_WRKS_RCLIENT;?></div>
            <div class="description"><?php echo $row->client;?></div>
          </div>
        </div>
        <div class="item"> <i class="building icon"></i>
          <div class="content">
            <div class="header"><?php echo Lang::$word->_MOD_WRKS_RLOC;?></div>
            <div class="description"><?php echo $row->location;?></div>
          </div>
        </div>
      </div>
      <a href="<?php echo doUrl(false, false, "projects");?>" class="veriasist basic labeled icon button"><i class="left arrow icon"></i><?php echo Lang::$word->_MOD_WRKS_BACK;?></a> </div>
  </div>
  <ul class="veriasist tabs">
    <li><a data-tab="#tab1"><?php echo Lang::$word->_MOD_WRKS_PSTUDY;?></a></li>
    <li><a data-tab="#tab2"><?php echo Lang::$word->_MOD_WRKS_PAPPROACH;?></a></li>
    <li><a data-tab="#tab3"><?php echo Lang::$word->_MOD_WRKS_RESULTS;?></a></li>
  </ul>
  <div class="veriasist bottom attached secondary segment">
    <div id="tab1" class="veriasist tab content"> <?php echo cleanOut($row->{'detail'.Lang::$lang});?> </div>
    <div id="tab2" class="veriasist tab content"> <?php echo cleanOut($row->{'body'.Lang::$lang});?> </div>
    <div id="tab3" class="veriasist tab content"> <?php echo cleanOut($row->{'result'.Lang::$lang});?> </div>
  </div>
</div>
<?php endif ?>



<?php break;?>




<?php default: ?>
<?php $portadata = Registry::get("Projects")->renderProjects(); ?>

<section class="no-padding work-3col wow fadeIn ">
  <div class="container">

    <div class="row margin-nine no-margin-top no-margin-lr">
      <div class="col-md-12 col-sm-12 text-center">
        <h2 class="title-large alt-font text-uppercase letter-spacing-2 white-text display-block font-weight-600 margin-one no-margin-top"><?php echo $content->moduledata->{'title' . Lang::$lang}; ?></h2>
        <div class="no-margin-top"><img src="<?php echo THEMEURL; ?>/images/separator3.png" alt=""></div>
      </div>
    </div>

    <div class="row">
      <div class="grid-gallery grid-style-projects overflow-hidden">
        <div class="tab-content">
          <ul class="masonry-items grid">

            <?php foreach ($portadata as $key => $row): ?>

              <li>
                <figure>
                  <div class="gallery-img">
                    <a href="<?php echo doUrl(false, $row->slug, "projects-item"); ?>"><img src="<?php echo SITEURL.'/'.Projects::imagepath . $row->thumb;?>" alt=""></a>
                  </div>
                  <figcaption>
                    <h3 class="alt-font text-uppercase letter-spacing-2">
                      <p><a href="<?php echo doUrl(false, $row->slug, "projects-item"); ?>"><?php echo $row->{'title' . Lang::$lang};?></a></p>
                    </h3>
                  </figcaption>
                </figure>
              </li>

            <?php endforeach; ?>

          </ul>
        </div>
      </div>
    </div>

  </div>
</section>

<?php break;?>
<?php endswitch;?>
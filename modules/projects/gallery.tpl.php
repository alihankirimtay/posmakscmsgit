<?php
  /**
   * Portfolio Gallery
   *
   * @package VeriAsist
   * @author veriasist.com
   * @copyright 2014
   * @version $Id: gallery.tpl.php, v4.00 2014-04-20 16:18:34 gewa Exp $
   */
  
  if (!defined("_VALID_PHP"))
      die('Direct access to this location is not allowed.');
    
  require_once(MODPATH . "gallery/admin_class.php");
  Registry::set('Gallery', new Gallery($row->gallery));
  
  $galrow = Registry::get("Gallery")->getGalleryImages($row->gallery); 
?>
<?php if(!$galrow):?>
  <?php Filter::msgSingleAlert(Lang::$word->_MOD_GA_NOIMG);?>
<?php else:?>

<div class="owl-slider-full owl-carousel owl-theme light-pagination square-pagination dark-pagination-without-next-prev-arrow main-slider">

  <?php foreach($galrow as $i => $grow):?>
    <?php if ($grow->type == 'i'): ?>
      <div class="item padding-thirteen no-padding-lr sm-padding-twenty-nine sm-no-padding-lr xs-padding-seven xs-no-padding-lr half-project-bg owl-bg-img" style="background-image:url('<?php echo SITEURL.'/'.Gallery::galpath.Registry::get("Gallery")->folder.'/'.$grow->thumb;?>');">
        <div class="opacity-full bg-dark-gray"></div>
        <div class="container">
          <div class="row">
            <div class="col-md-<?=(@$row->layout == 'l' ? 10 : 12)?> text-center z-index-1 margin-twenty-five no-margin-lr no-margin-bottom xs-margin-nineteen xs-no-margin-bottom xs-no-margin-lr">
              <div class="slider-typographi-text">
                <span class="slider-subtitle alt-font font-weight-100 white-text text-medium letter-spacing-2 text-uppercase"><?php echo $grow->{'title' . Lang::$lang};?></span>
                <span class="alt-font font-weight-600 black-text title-extra-large letter-spacing-2 text-uppercase display-block xs-title-medium"><?php echo $grow->{'description'.Lang::$lang};?></span>
                <div class="bg-deep-red separator-line-thick-long margin-seven margin-lr-auto xs-margin-eleven xs-margin-lr-auto"></div>
              </div>
            </div>
          </div>
        </div>
      </div>
    <?php else: ?>
      <section style="visibility: visible; animation-name: fadeIn; background: url('<?=SITEURL?>/modules/projects/preload.jpg') 50% 0px repeat scroll transparent;" class="video-hover page-title parallax2 parallax-fix fadeIn">
        <div class="video-wrapper position-top z-index-0">
          <!-- html5 video -->
          <video loop="loop" controls class="html-video">
            <source src="<?php echo SITEURL.'/'.Gallery::galpath.Registry::get("Gallery")->folder.'/'.$grow->thumb;?>" type="video/mp4">
          </video>
          <!-- end html5 video -->
        </div>
        <div class="opacity-full-dark bg-deep-blue3"></div>
        <div class="container position-relative">
          <div class="row">
            <div class="col-md-<?=(@$row->layout == 'l' ? 10 : 12)?> col-sm-<?=(@$row->layout == 'l' ? 10 : 12)?> text-center">
              <div class="slider-typographi-text">
                <span class="slider-video-controls display-block">
                  <a href="javascript:void(0);" class="volume"></a>
                  <a href="javascript:void(0);" class="play"></a>
                  <a href="javascript:void(0);" class="expand"></a>
                </span>
                <span class="slider-subtitle alt-font font-weight-100 white-text text-medium letter-spacing-2 text-uppercase"><?php echo $grow->{'title' . Lang::$lang};?></span>
                <span class="alt-font font-weight-600 white-text title-extra-large letter-spacing-2 text-uppercase display-block xs-title-medium"><?php echo $grow->{'description'.Lang::$lang};?></span>
                <?php if (strlen($grow->{'title' . Lang::$lang}) > 2 && strlen($grow->{'description' . Lang::$lang}) > 2): ?>
                  <div class="bg-deep-red separator-line-thick-long margin-seven margin-lr-auto xs-margin-eleven xs-margin-lr-auto"></div>
                <?php endif; ?>
              </div>
            </div>
          </div>
        </div>
      </section>
    <?php endif; ?>
    
  <?php endforeach; ?>
</div>

  <script type="text/javascript">
  $(function() {

    // HOVER
    $(".video-hover")
      .mouseenter(function() {
        $(this).find("a.volume, a.expand, a.play").css("opacity", 1);
      })
      .mouseleave(function() {
        $(this).find("a.volume, a.expand, a.play").css("opacity", 0);
      });

    // PLAY
    $("body").on('click', ".slider-video-controls a.play", function() {
      el = $(this);

      if (el.hasClass("pause")) {
        el.removeClass("pause");
        el.closest("section").find("video").get(0).pause();
      } else {
        el.addClass("pause");
        el.closest("section").find("video").get(0).play();
      }
    });

    // VOLUME
    $("body").on('click', ".slider-video-controls a.volume", function() {
      el = $(this);

      if (el.hasClass("mute")) {
        el.removeClass("mute");
        el.closest("section").find("video").prop({'muted':false, 'volume':1});
      } else {
        el.addClass("mute");
        el.closest("section").find("video").prop({'muted':true, 'volume':0});
      }
    });

    // FULLSCREEN
    $("body").on('click', ".slider-video-controls a.expand", function() {

      el     = $(this),
      video  = el.closest("section").find("video");

      toggleFullScreen( video.get(0) );

      function toggleFullScreen(elem) {
        if (elem.requestFullscreen) {
          elem.requestFullscreen();
        } else if (elem.msRequestFullscreen) {
          elem.msRequestFullscreen();
        } else if (elem.mozRequestFullScreen) {
          elem.mozRequestFullScreen();
        } else if (elem.webkitRequestFullscreen) {
          elem.webkitRequestFullscreen();
        }
      }

    });
  });
  </script>
<?php endif;?>
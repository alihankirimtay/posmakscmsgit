<?php
  /**
   * Calendar
   *
   * @package VeriAsist
   * @author veriasist.com
   * @copyright 2014
   * @version $Id: calendar.php, v4.00 2014-04-20 10:12:05 gewa Exp $
   */
  define("_VALID_PHP", true);
  
  require_once("../../init.php");

  require_once(MODPATH . "events/admin_class.php");
  Registry::set('eventManager', new eventManager());
?>
<?php
  /* == Get Calendar Month == */
  if (isset($_POST['getcal'])):
      Registry::get("eventManager")->renderCalendar();
  endif;
?>

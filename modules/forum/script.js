(function ($) {
    $.Forum = function (settings) {
        var config = {
            url: SITEURL + "/modules/forum/controller.php",
            followYes: "You are following this topic",
            followNo: "Follow Topic",
            updateConv: "Update Conversation",
            thread: 0,
            id: 0

        };

        if (settings) {
            $.extend(config, settings);
        }

        $("#forumsearch").on('click', '.veriasist.button', function () {
            var str = $("#forumsearch input[name=search]").val();
            $.ajax({
                type: "post",
                url: config.url,
                data: {
                    'dosearch': 1,
                    'string': str,
                    'thread_id': config.thread,
                    'forum_id': config.id,
                },
                dataType: 'json',
                success: function (json) {
                    if (json.type == "success") {
                        $("#forumsearch").removeClass("error");
                        $("#forum").html(json.message).fadeIn();
                        $("#pagination").hide();
                    } else if (json.type == "emptystr") {
                        $("#forumsearch").addClass("error");
                    } else {
                        $("#forumsearch").removeClass("error");
                        $(".srch-message").html(json.message);
                    }
                }
            });
        });

        $("#forumsearch").on('click', ".veriasist.button i.icon", function () {
            window.location.reload();
        });

        $('body').on('click', 'button[name=doForum]', function () {
            function showResponse(json) {
                if (json.status == "success") {
                    $(".veriasist.form").removeClass("loading").slideUp();
                    $("#forum").prepend(json.message);
                } else {
                    $(".veriasist.form").removeClass("loading");
                    $("#msgholder").html(json.message);
                }
            }

            function showLoader() {
                $(".veriasist.form").addClass("loading");
            }
            var options = {
                target: "#msgholder",
                beforeSubmit: showLoader,
                success: showResponse,
                type: "post",
                url: config.url,
                dataType: 'json'
            };

            $('#admin_form').ajaxForm(options).submit();
        });

        $('body').on('click', 'button[name=doPost]', function () {
            function showResponse(json) {
                if (json.status == "updated") {
                    $post = $('div.section[data-id = ' + json.pid + ']');
                    $post.find(".header").html(json.title);
                    $post.find("article").html(json.body);
                    $(".veriasist.form").removeClass("loading").slideUp();
                    $("#msgholder").html(json.message);
                } else if (json.status == "success") {
                    $("#forum").append(json.info);
                    $(".veriasist.form").removeClass("loading").slideUp();
                    $("#msgholder").html(json.message);
                    $.post(config.url, {
                        notify: 1,
                        thread_id: json.thread_id,
                        forum_id: json.forum_id,
                        msg: json.msg
                    });
                } else {
                    $(".veriasist.form").removeClass("loading");
                    $("#msgholder").html(json.message);
                }

            }

            function showLoader() {
                $(".veriasist.form").addClass("loading");
            }
            var options = {
                target: "#msgholder",
                beforeSubmit: showLoader,
                success: showResponse,
                type: "post",
                url: config.url,
                dataType: 'json'
            };

            $('#admin_form').ajaxForm(options).submit();
        });

        $('body').on('click', 'a.dofollow', function () {
            var id = $(this).data('id');
            main = $(this).closest('.section');
            parent = $(this).parent('.item');
            $self = $(this);
            $.ajax({
                type: "post",
                url: SITEURL + "/modules/forum/controller.php",
                data: {
                    newFollow: 1,
                    id: id
                },
                dataType: 'json',
                success: function (json) {
                    if (json.type == "success") {
                        if (json.message == "remove") {
                            parent.html("<i class=\"icon unhide\"></i><a data-id=\"" + id + "\" class=\"dofollow\">" + config.followNo + "</a>");
                        } else {
                            parent.html("<i class=\"icon hide\"></i><a data-id=\"" + id + "\" class=\"dofollow\">" + config.followYes + "</a>");
                        }
                        $(main).effect("highlight", {}, 3000);
                    }
                }
            });
        });

        $("#newreply, #newtopic").on('click', function () {
            $('#admin_form').slideDown();
        });

        $('#forum').on('click', 'a[data-action = "delete"]', function () {
            id = $(this).data('id');
            parent = $(this).closest('.section')
            $.post(config.url, {
                deletePost: 1,
                id: id
            }, function (data) {
                parent.slideUp();
            });
        })

        $('body').on('click', 'a.tdelete', function () {
            id = $(this).data('id');
            parent = $(this).closest('.section')
            $.post(config.url, {
                deleteThread: 1,
                id: id
            }, function (data) {
                parent.slideUp();
            });
        })

        $('#forum').on('click', 'a[data-action = "like"]', function () {
            $parent = $(this).closest('div.section');
            $el = $parent.find('footer b.like')
            id = $(this).closest('div.section').data("id");
            total = $(this).data("total");
            $.post(config.url, {
                doLike: 1,
                id: id,
                total: total
            }, function (data) {
                $($el).html(data);
            });
            $(this).attr('data-action', null);
            $(this).addClass('negative');
        });

        $('#forum').on('click', 'a[data-action = "quote"], a[data-action = "reply"], a[data-action = "edit"]', function () {
            $parent = $(this).closest('div.section');
            id = $(this).closest('div.section').data("id");
            $el = $parent.find('article').html()
            $title = $parent.find('.header').html()

            if ($(this).data('action') == "quote") {
                $('.forumpost').redactor('set', '<blockquote>' + $el + '</blockquote><p>&nbsp;</p>');
                $('#admin_form input[name=title]').val("re:" + $title).prop('readonly', true);
                $('<input>').prop({
                    type: 'hidden',
                    name: 'is_reply',
                    value: 1
                }).appendTo('#admin_form');
            } else if ($(this).data('action') == "edit") {
                $('.forumpost').redactor('set', $el);
                $('#admin_form input[name=title]').val($title);
                $('<input>').prop({
                    type: 'hidden',
                    name: 'adminEdit',
                    value: '1'
                }).appendTo('#admin_form');
                $('<input>').prop({
                    type: 'hidden',
                    name: 'post_id',
                    value: id
                }).appendTo('#admin_form');
                $('#admin_form button[name=doPost]').html(config.updateConv);
            } else {
                $('#admin_form input[name=title]').val("re:" + $title).prop('readonly', true);
                $('.forumpost').redactor('set', '');
                $('<input>').prop({
                    type: 'hidden',
                    name: 'is_reply',
                    value: 1
                }).appendTo('#admin_form');
            }

            $('#admin_form').slideDown();
        });

        $('.forumpost').redactor({
            observeLinks: true,
            toolbarFixed: true,
            wym: true,
            minHeight: 200,
            maxHeight: 500,
            buttons: ['formatting', 'bold', 'italic', 'unorderedlist', 'orderedlist', 'outdent', 'indent', 'video']
        });

    };
})(jQuery);
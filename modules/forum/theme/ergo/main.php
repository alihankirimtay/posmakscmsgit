<?php
  /**
   * Forum Main
   *
   * @package VeriAsist
   * @author veriasist.com
   * @copyright 2014
   * @version $Id: main.php, v4.00 2014-03-20 16:18:34 gewa Exp $
   */
  
  if (!defined("_VALID_PHP"))
      die('Direct access to this location is not allowed.');
	  
  require_once (MODPATH . "forum/admin_class.php");
  $classname = 'Forum';
  try {
	  if (!class_exists($classname)) {
		  throw new exception('Missing forum/admin_class.php');
	  }
	  Registry::set('Forum', new Forum());
  }
  catch (exception $e) {
	  echo $e->getMessage();
  }

  $length = count($content->_url);
?>
<?php switch($length): case 3: ?>
<?php if(in_array(doUrlParts("forum-user"), $content->_url)):?>
<?php $single = $content->moduledata->mod;?>
<?php $userdata = Registry::get("Forum")->getUserInfo($single->id);?>
<div id="forum">
  <div class="veriasist header">
    <?php if($user->is_Admin()):?>
    <a class="veriasist danger button doban push-right" data-id="<?php echo $single->id;?>"><?php echo Lang::$word->_MOD_FB_BANUSER;?></a>
    <?php endif;?>
    <img src="<?php echo UPLOADURL;?>avatars/<?php echo ($single->avatar) ? $single->avatar : "blank.png";?>" alt="" class="veriasist image avatar">
    <div class="content"> <?php echo Lang::$word->_MOD_FB_FORUMUSER . $single->username;?>
      <div class="sub header"><?php echo Lang::$word->_MOD_FB_TOTALPOSTS;?> <b><?php echo count($userdata);?></b><br>
        <?php echo Lang::$word->_UR_DATE_REGGED;?>: <?php echo Filter::dodate("long_date", $single->created);?></div>
    </div>
  </div>
  <div class="veriasist divider"></div>
  <?php if($userdata):?>
  <div class="veriasist divided list">
    <?php foreach($userdata as $row):?>
    <div class="item">
      <div class="right floated"><?php echo timesince($row->cdate);?></div>
      <div class="content">
        <div class="header"><a href="<?php echo doUrl(false, $row->slug, "forum-topic");?>"><?php echo $row->title;?></a></div>
      </div>
    </div>
    <?php endforeach;?>
    <?php unset($row);?>
  </div>
</div>
<?php if($user->is_Admin()):?>
<script type="text/javascript"> 
  $(document).ready(function () {
      $("a.doban").on('click', function () {
          var id = $(this).data('id')
          var parent = $(this).closest('#forum');
          $.ajax({
              type: 'post',
              url: SITEURL + "/modules/forum/controller.php",
              data: {
                  banUser: 1,
                  id: id
              },
              success: function (msg) {
                  parent.slideUp();
              }
          });
          return false;
      });
  });
</script>
<?php endif;?>
<?php endif;?>
<?php else:?>
<?php $row = $content->moduledata->mod;?>
<?php $threadrows = Registry::get("Forum")->renderThreads($row->id);?>
<div class="veriasist header">
  <?php if($user->logged_in or ($row->access == "Public" and $row->publicpost)):?>
  <a class="veriasist positive button push-right scroller" data-scroller="newreply" id="newtopic"><i class="icon add"></i><?php echo Lang::$word->_MOD_FB_NEWTHREAD;?></a>
  <?php endif;?>
  <?php if($row->icon):?>
  <i class="<?php echo $row->icon;?>"></i>
  <?php endif;?>
  <div class="content"> <?php echo Lang::$word->_MOD_FB_VIEW_THREADS . $row->title;?>
    <div class="sub header"><?php echo $row->description;?></div>
  </div>
</div>
<div class="veriasist divider"></div>
<?php if($row->access == "Registered" and !$user->logged_in):?>
<?php Filter::msgSingleError(Lang::$word->_UA_ACC_ERR1);?>
<?php elseif(!Forum::getMembershipAccess($row->membership_id)):?>
<?php echo Filter::msgSingleAlert(Lang::$word->_UA_ACC_ERR2 . Forum::listMemberships($row->membership_id));?>
<?php else:?>
<?php $forumrow = Registry::get("Forum")->renderForums();?>
<div class="veriasist small buttons">
  <?php foreach($forumrow as $tag):?>
  <?php $active = ($tag->slug == $content->_url[2]) ? ' active' : null;?>
  <a class="veriasist button<?php echo $active;?>" href="<?php echo doUrl(false, $tag->slug, "forum-topic");?>"><?php echo $tag->title;?></a>
  <?php endforeach;?>
  <?php unset($tag);?>
</div>
<div class="veriasist divider"></div>
<div id="forum">
  <?php if($threadrows):?>
  <?php foreach($threadrows as $trow):?>
  <?php $username = ($trow->user_id) ? $trow->username : Lang::$word->_MOD_FB_GUEST;?>
  <?php if($trow->user_id):?>
  <?php $uurl = '<a href="' . doUrl(false, $username, "forum-user") . '">' . $username . '</a>';?>
  <?php else:?>
  <?php $uurl = $username;?>
  <?php endif;?>
  <div class="section">
    <article>
      <div class="veriasist push-right"><?php echo Lang::$word->_MOD_FB_LPOST;?> <?php echo timesince($trow->cdate);?> <?php echo Lang::$word->_MOD_FB_BY . ': ' . $uurl;?></div>
      <div class="content">
        <div class="veriasist header"><a href="<?php echo doUrl(false, $trow->slug, "forum-item");?>"><?php echo $trow->title;?></a></div>
      </div>
    </article>
    <section>
      <div class="veriasist divided horizontal list">
        <div class="item"><i class="icon chat"></i><?php echo Lang::$word->_MOD_FB_TOTALREP;?> <b><?php echo $trow->totalposts;?></b></div>
        <?php if($user->logged_in):?>
        <?php if($trow->follow):?>
        <div class="item"><i class="icon hide"></i><a data-id="<?php echo $trow->id;?>" class="dofollow"><?php echo Lang::$word->_MOD_FB_FOLLOW_Y;?></a></div>
        <?php else:?>
        <div class="item"><i class="icon unhide"></i><a data-id="<?php echo $trow->id;?>" class="dofollow"><?php echo Lang::$word->_MOD_FB_FOLLOW;?></a></div>
        <?php endif;?>
        <?php endif;?>
      </div>
      <?php if($user->is_Admin()):?>
      <a class="veriasist bottom right attached negative label tdelete" data-id="<?php echo $trow->id;?>" data-content="<?php echo Lang::$word->_DELETE;?>"><i class="icon remove link"></i></a>
      <?php endif;?>
    </section>
  </div>
  <?php endforeach;?>
  <?php unset($trow);?>
  <?php endif;?>
  <?php endif;?>
</div>
<?php if($user->logged_in or ($row->access == "Public" and $row->publicpost)):?>
<div class="veriasist secondary form segment">
  <div id="newreply" class="veriasist header"><?php echo Lang::$word->_MOD_FB_NEWREPLY;?></div>
  <form id="admin_form" name="admin_form" method="post" style="display:none">
    <div class="field">
      <label><?php echo Lang::$word->_MOD_FB_NEWTHREAD_TITLE;?></label>
      <label class="input"><i class="icon-append icon asterisk"></i>
        <input type="text" placeholder="<?php echo Lang::$word->_MOD_FB_NEWTHREAD_TITLE;?>" name="title">
      </label>
    </div>
    <div class="field">
      <label><?php echo Lang::$word->_MOD_FB_NEWTHREAD_BODY;?></label>
      <textarea class="forumpost" placeholder="<?php echo Lang::$word->_MOD_FB_NEWTHREAD_BODY;?>" name="body"></textarea>
    </div>
    <div class="veriasist double fitted divider"></div>
    <button type="button" name="doForum" class="veriasist positive button"><?php echo Lang::$word->_MOD_FB_START;?></button>
    <input name="processForum" type="hidden" value="1">
    <input name="id" type="hidden" value="<?php echo $row->id;?>">
    <input name="dothread" type="hidden" value="1">
  </form>
</div>
<div id="msgholder"></div>
<?php endif;?>
<script type="text/javascript" src="<?php echo ADMINURL;?>/assets/js/editor.js"></script> 
<script type="text/javascript">
// <![CDATA[
$(document).ready(function () {
    $.Forum({
            url: SITEURL + "/modules/forum/controller.php",
            followYes: "<?php echo Lang::$word->_MOD_FB_FOLLOW_Y;?>",
            followNo: "<?php echo Lang::$word->_MOD_FB_FOLLOW;?>"
    });
});
// ]]>
</script>
<?php endif;?>
<?php break;?>
<?php case 2: ?>
<?php $row = $content->moduledata->mod;?>
<?php $postrows = Registry::get("Forum")->renderPosts($row->tid);?>
<div class="veriasist header">
  <?php if($user->logged_in or ($row->access == "Public" and $row->publicpost)):?>
  <a class="veriasist positive button push-right scroller" data-scroller="newreply" id="newtopic"><i class="icon add"></i><?php echo Lang::$word->_MOD_FB_NEWPOST;?></a>
  <?php endif;?>
  <?php if($row->icon):?>
  <i class="<?php echo $row->icon;?>"></i>
  <?php endif;?>
  <div class="content"> <?php echo Lang::$word->_MOD_FB_VIEW_POSTS . $row->ftitle;?>
    <div class="sub header"><?php echo $row->description;?></div>
  </div>
</div>
<div class="veriasist divider"></div>
<?php if($row->access == "Registered" and !$user->logged_in):?>
<?php Filter::msgSingleError(Lang::$word->_UA_ACC_ERR1);?>
<?php elseif(!Forum::getMembershipAccess($row->membership_id)):?>
<?php echo Filter::msgSingleAlert(Lang::$word->_UA_ACC_ERR2 . Forum::listMemberships($row->membership_id));?>
<?php else:?>
<div id="forumsearch" class="veriasist fluid action input small-bottom-space">
  <input name="search" placeholder="<?php echo Lang::$word->_MOD_FB_SEARCH;?>" type="text">
  <div class="veriasist right labeled icon button"><i data-content="<?php echo Lang::$word->_MOD_FB_RESET;?>" class="home icon"></i> <?php echo Lang::$word->_MOD_FB_SEARCH;?></div>
</div>
<div class="srch-message"></div>
<div id="forum">
  <?php if($postrows):?>
  <?php foreach($postrows as $trow):?>
  <?php $username = ($trow->user_id) ? $trow->username : Lang::$word->_MOD_FB_GUEST;?>
  <?php $avatar = ($trow->avatar) ? '<img src="' . UPLOADURL . 'avatars/' . $trow->avatar . '" alt="' . $username . '" class="veriasist avatar image">' : '<img src="' . UPLOADURL . 'avatars/blank.png" alt="' . $username . '" class="veriasist avatar image">';?>
  <?php if($trow->user_id):?>
  <?php $uurl = '<a href="' . doUrl(false, $username, "forum-user") . '">' . $username . '</a>';?>
  <?php $avurl = '<a href="' . doUrl(false, $username, "forum-user") . '">' . $avatar . '</a>';?>
  <?php else:?>
  <?php $uurl = $username;?>
  <?php $avurl = '<a>' . $avatar . '</a>';?>
  <?php endif;?>
  <div class="section<?php if($trow->is_reply):?> reply<?php endif;?>" data-id="<?php echo $trow->id;?>">
    <section>
      <div class="veriasist divided list">
        <div class="item">
          <div class="right floated">
            <?php if($user->logged_in or ($row->access == "Public" and $row->publicpost)):?>
            <a data-scroller="newreply" data-action="quote" data-content="<?php echo Lang::$word->_MOD_FB_QUOTE;?>" class="veriasist mini basic icon button scroller"><i class="icon quote left"></i></a> <a data-scroller="newreply" data-action="reply" data-content="<?php echo Lang::$word->_MOD_FB_REPLYTO;?>" class="veriasist mini basic icon button scroller"><i class="icon comment"></i></a> <a data-action="like" data-total="<?php echo $trow->like;?>" data-content="<?php echo Lang::$word->_MOD_FB_LIKE;?>" class="veriasist mini basic icon button like"><i class="icon heart"></i></a>
            <?php endif;?>
            <?php if($user->is_Admin()):?>
            <a data-scroller="newreply" class="veriasist mini basic icon button scroller" data-action="edit" data-id="<?php echo $trow->id;?>" data-content="<?php echo Lang::$word->_EDIT;?>"><i class="icon pencil"></i></a> <a class="veriasist mini basic icon button" data-action="delete" data-id="<?php echo $trow->id;?>" data-content="<?php echo Lang::$word->_DELETE;?>"><i class="icon remove"></i></a>
            <?php endif;?>
          </div>
          <?php echo $avurl;?>
          <div class="content">
            <div class="header"><?php echo $trow->title;?></div>
          </div>
        </div>
      </div>
    </section>
    <article> <?php echo cleanOut($trow->body);?> </article>
    <footer>
      <div class="veriasist divided horizontal list">
        <div class="item"><i class="icon time"></i>
          <?php if($trow->is_reply == $trow->id):?>
          <?php echo Lang::$word->_MOD_FB_REPLIED . ': ' .timesince($trow->cdate) . Lang::$word->_MOD_FB_BY . ': ' . $uurl;?>
          <?php else:?>
          <?php echo Lang::$word->_MOD_FB_CREATED . ': ' .timesince($trow->cdate) . Lang::$word->_MOD_FB_BY . ': ' . $uurl;?>
          <?php endif;?>
        </div>
        <div class="item"><i class="icon danger heart"></i><?php echo Lang::$word->_MOD_FB_LIKE . ': <b class="like">' . $trow->like . '</b>';?></div>
      </div>
    </footer>
  </div>
  <?php endforeach;?>
  <?php unset($trow);?>
  <?php endif;?>
</div>
<div id="pagination" class="content-center"><?php echo $pager->display_pages();?></div>
<?php if($user->logged_in or ($row->access == "Public" and $row->publicpost)):?>
<div class="veriasist secondary form segment">
  <div id="newreply" class="veriasist header"><?php echo Lang::$word->_MOD_FB_NEWREPLY;?></div>
  <form id="admin_form" name="admin_form" method="post" style="display:none">
    <div class="field">
      <label><?php echo Lang::$word->_MOD_FB_NEWTHREAD_TITLE;?></label>
      <label class="input"><i class="icon-append icon asterisk"></i>
        <input type="text" placeholder="<?php echo Lang::$word->_MOD_FB_NEWTHREAD_TITLE;?>" name="title">
      </label>
    </div>
    <div class="field">
      <label><?php echo Lang::$word->_MOD_FB_NEWTHREAD_BODY;?></label>
      <textarea class="forumpost" placeholder="<?php echo Lang::$word->_MOD_FB_NEWTHREAD_BODY;?>" name="body"></textarea>
    </div>
    <div class="veriasist double fitted divider"></div>
    <button type="button" name="doPost" class="veriasist positive button"><?php echo Lang::$word->_MOD_FB_START;?></button>
    <input name="processForum" type="hidden" value="1">
    <input name="id" type="hidden" value="<?php echo $row->id;?>">
    <input name="tid" type="hidden" value="<?php echo $row->tid;?>">
    <input name="dopost" type="hidden" value="1">
  </form>
</div>
<div id="msgholder"></div>
<?php endif;?>
<script type="text/javascript" src="<?php echo ADMINURL;?>/assets/js/editor.js"></script> 
<script type="text/javascript">
// <![CDATA[
$(document).ready(function () {
    $.Forum({
            url: SITEURL + "/modules/forum/controller.php",
			thread: <?php echo $row->tid;?>,
			id: <?php echo $row->id;?>,
			updateConv: "<?php echo Lang::$word->_MOD_FB_PUPDATE;?>"
    });
});
// ]]>
</script>
<?php endif;?>
<?php break;?>
<?php default: ?>
<?php $forumrow = Registry::get("Forum")->renderForums();?>
<div id="forum">
  <h1 class="veriasist double header"><span><?php echo Lang::$word->_MOD_FB_VIEW_ALL;?></span></h1>
  <?php if(!$forumrow):?>
  <?php echo Filter::msgSingleInfo(Lang::$word->_MOD_FB_NOFORUMS);?>
  <?php else:?>
  <?php foreach($forumrow as $row):?>
  <div class="section">
    <section>
      <?php $username = ($row->last_entry_user_id) ? $row->username : Lang::$word->_MOD_FB_GUEST;?>
      <?php if($row->last_entry_user_id):?>
      <?php $uurl = '<a href="' . doUrl(false, $username, "forum-user") . '">' . $username . '</a>';?>
      <?php else:?>
      <?php $uurl = $username;?>
      <?php endif;?>
      <div class="veriasist header">
        <?php if($row->icon):?>
        <i class="<?php echo $row->icon;?>"></i>
        <?php endif;?>
        <div class="content"> <a href="<?php echo doUrl(false, $row->slug, "forum-topic");?>"><?php echo $row->title;?></a>
          <div class="sub header"><?php echo $row->description;?></div>
        </div>
      </div>
    </section>
    <article>
      <div class="veriasist divided horizontal list">
        <div class="item"><i class="icon <?php echo ($row->access == "Public" ? "unlock" : ($row->access == "Membership" ? "certificate" : "lock"));?>"></i><?php echo Lang::$word->_MOD_FB_ACCESS;?> - <?php echo $row->access;?></div>
        <div class="item"><i class="icon bullseye"></i><?php echo Lang::$word->_MOD_FB_TOTALPOSTS;?> <b><?php echo $row->totalposts;?></b></div>
        <?php if($row->totalposts):?>
        <div class="item"><i class="icon time"></i><?php echo Lang::$word->_MOD_FB_LPOST;?> <?php echo timesince($row->last_entry);?> <?php echo Lang::$word->_MOD_FB_BY . ': ' . $uurl;?> </div>
        <?php endif;?>
      </div>
    </article>
  </div>
  <?php endforeach;?>
  <?php unset($row);?>
  <?php endif;?>
</div>
<?php break;?>
<?php endswitch;?>

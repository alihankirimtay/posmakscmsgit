<?php
  /**
   * Forum Controller
   *
   * @package VeriAsist
   * @author veriasist.com
   * @copyright 2014
   * @version $Id: controller.php, v4.00 2014-03-20 10:12:05 gewa Exp $
   */
  define("_VALID_PHP", true);
  require_once("../../init.php");

  require_once (MODPATH . "forum/admin_class.php");

  Registry::set('Forum', new Forum());
  
?>
<?php
  /* == System Error == */
  if (Registry::get("Forum")->hash !== hash('sha256', Registry::get("Forum")->secret)):
      exit(Filter::ooops());
  endif;

  /* == Proccess Conversations == */
  if (isset($_POST['processForum'])):
      $row = $db->first("SELECT access, publicpost FROM " . Forum::mTable . " WHERE id='" . Filter::$id . "' AND active = 1");
      if ($row and $user->logged_in or ($row->access == "Public" and $row->publicpost)):
          if (isset($_POST['dothread']) and intval($_POST['dothread']) == 1):
              Registry::get("Forum")->processNewThread();
          else:
              Registry::get("Forum")->processNewPost();
          endif;
      endif;
  endif;

  /* == Notify Users == */
  if (isset($_POST['notify'])):
      if (isset($_POST['thread_id']) and isset($_POST['forum_id']) and isset($_POST['msg'])):
          $thread_id = intval($_POST['thread_id']);
          $forum_id = intval($_POST['forum_id']);
          $body = $_POST['msg'];
          Registry::get("Forum")->sendEmail($thread_id, $forum_id, $body);
      endif;
  endif;

  /* == Proccess Search == */
  if (isset($_POST['dosearch'])):
      if (isset($_POST['string']) and strlen($_POST['string']) > 4):
          $string = sanitize($_POST['string']);
          $thread_id = intval($_POST['thread_id']);
		  $forum_id = intval($_POST['forum_id']);
          Registry::get("Forum")->doSearch($string, $thread_id, $forum_id);
      else:
          $json['type'] = 'emptystr';
          print json_encode($json);
      endif;
  endif;

      /* == Proccess Like == */
  if (isset($_POST['doLike']) and Filter::$id ):
      $data['like'] = "INC(1)";
      $db->update(Forum::pTable, $data, "id = " . Filter::$id);
      print intval($_POST['total'] + 1);
  endif;

      /* == Proccess Follow == */
  if (isset($_POST['newFollow']) and $user->logged_in):
	  Registry::get("Forum")->processFollow();
  endif;

  if ($user->is_Admin()):
      /* == Delete Post == */
      if (isset($_POST['deletePost'])):
          $db->delete(Forum::pTable, "id=" . Filter::$id);

          $data = array('totalposts' => "INC(-1)");
          $db->update(Forum::tTable, $data, "id = " . Filter::$id);
      endif;

      /* == Delete Topic == */
      if (isset($_POST['deleteThread'])):
          $db->delete(Forum::tTable, "id=" . Filter::$id);
		  $db->delete(Forum::pTable, "thread_id=" . Filter::$id);
		  $db->delete(Forum::uTable, "thread_id=" . Filter::$id);
      endif;
	  
      /* == Edit Topic Title == */
	  /*
      if (isset($_POST['editThread'])):
          $id = intval($_POST['editThread']);
          if (!empty($_POST['title'])):
              $data = array(
                  'title' => sanitize($_POST['title']),
                  'slug' => paranoia($_POST['title'])
                  );
              $db->update("mod_forum_thread", $data, "id = '" . $id . "'");
          endif;
      endif;
*/
      /* == Ban User == */
      if (isset($_POST['banUser']) and $user->userlevel == 9):
          $data['active'] = 'b';
          $db->update(Users::uTable, $data, "id=" . Filter::$id);
		  $db->delete(Forum::uTable, "user_id=" . Filter::$id);
      endif;
  endif;
?>
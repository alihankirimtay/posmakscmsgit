<?php
  /**
   * Controller
   *
   * @package VeriAsist
   * @author veriasist.com
   * @copyright 2012
   * @version $Id: controller.php, v1.00 2012-12-24 12:12:12 gewa Exp $
   */
  define("_VALID_PHP", true);
  
  require_once("../../init.php");

  require_once(MODPATH . "adblock/admin_class.php");
  Registry::set('AdBlock', new AdBlock());
?>
<?php 
  /* Proccess AdClick */
  if (isset($_GET['adC'])):
      $fname = substr($_GET['f'],42);
  	  Filter::$id= (isset($_GET['adC'])) ? $_GET['adC'] : 0; 
  	  if($fname != md5(sha1(Filter::$id))) die('err');
  	  Registry::get("AdBlock")->incrementClicksNumber();
  endif;
?>
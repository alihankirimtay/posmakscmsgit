<?php
  /**
   * Controller
   *
   * @package VeriAsist
   * @author veriasist.com
   * @copyright 2012
   * @version $Id: controller.php, v1.00 2012-12-24 12:12:12 gewa Exp $
   */
  define("_VALID_PHP", true);
  
  require_once("../../init.php");

  require_once(MODPATH . "selfupdate/admin_class.php");
  Registry::set('SelfUpdate', new SelfUpdate());
?>
<?php 
  /* Proccess AdClick */
  if (isset($_GET['create_archive'])):
  	  Registry::get("SelfUpdate")->createArchive();
  endif;
    if (isset($_GET['send_archives'])):
      Registry::get("SelfUpdate")->triggerAllUsers();
  endif;
?>
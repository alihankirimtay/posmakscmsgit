(function ($) {
    $.DigiShop = function (settings) {
        var config = {
            url: "controller.php",
            cat: 0,
        };

        if (settings) {
            $.extend(config, settings);
        }

        $('#lswitch a').on('click', function () {
            $container = $('#digishop');
            var mode = $(this).data('type')
            $('#lswitch a.active').not(this).removeClass('active');
            $(this).toggleClass("active");

            type = (mode == "list") ? "list-view.php" : "grid-view.php";
            $container.addClass('loader');

            $.get(SITEURL + "/modules/digishop/" + type, {
                cid: config.cat
            }, function (data) {
                setTimeout(function () {
                    $container.html(data).fadeIn();
                    $container.removeClass('loader');
                    $.cookie("DIGIVIEW_VERIASIST", mode, {
                        expires: 120,
                        path: '/'
                    });

                    if (mode == "grid") {
                        $('#digiwrap.digigrid').waitForImages(function () {
                            $('#digiwrap.digigrid').elasticColumns('refresh');
                            $('#digiwrap.digigrid').Grid('displayItems')
                        });
                    }

                }, 500);
            });
        });

        $('#digishop').on('click', 'a.like', function () {
            $el = $(this).children('small')
            id = $(this).data("id");
            total = $(this).data("total");
            $.post(SITEURL + config.url, {
                doLike: 1,
                id: id,
                total: total
            }, function (data) {
                $($el).html(data);
            });
            $(this).removeClass('like');
        });

        $('#digishop').on('click', 'a.add', function () {
            $btn = $(this);
            $btn.addClass("loading");
            id = $(this).data("id");
            $.ajax({
                type: "post",
                dataType: "json",
                url: SITEURL + config.url,
                data: {
                    addtocart: 1,
                    id: id
                },
                success: function (json) {
                    setTimeout(function () {
                        $btn.removeClass("loading");
                        if (json.status == "success") {
                            loadCartSmall();
                        } else {
                            $btn.text(json.message)
                        }

                    }, 800);
                }
            });
        });

        $('body').on('click', '#cartlist a.remove', function () {
            $item = $(this).closest('.item');
            $item.slideUp();
            id = $(this).data('id');
            $.ajax({
                type: "post",
                url: SITEURL + config.url,
                data: {
                    removeitem: 1,
                    id: id
                },
                success: function (msg) {
                    setTimeout(function () {
                        loadCartSmall();
                    }, 800);
                }
            });
        });

		$("#doOffline").on('click', function () {
			$btn = $(this).parent();
			$btn.addClass("loading");
			$.ajax({
				type: 'POST',
				url: SITEURL + "/modules/digishop/controller.php",
				data: {
					doOffline: 1
				},
				success: function (msg) {
					$btn.removeClass("loading");
					$("#show-result").html(msg);
					$("#cko").slideUp();
				}
			});
			return false;
		});
		
        function loadCartSmall() {
            $.ajax({
                type: "POST",
                url: SITEURL + config.url,
                dataType: 'json',
                data: 'loadcart=1',
                cache: false,
                success: function (json) {
                    $("#cartlist").html(json.message)
                }
            });
        }
    };
})(jQuery);
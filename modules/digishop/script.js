(function ($) {
    // $.DigiShop = function (settings) {
        var config = {
            url: "/modules/digishop/controller.php",
            cat: 0,
        };

        // if (settings) {
        //     $.extend(config, settings);
        // }

        $('#lswitch a').on('click', function () {
            $container = $('#digishop');
            var mode = $(this).data('type')
            $('#lswitch a.active').not(this).removeClass('active');
            $(this).toggleClass("active");

            type = (mode == "list") ? "list-view.php" : "grid-view.php";
            $container.addClass('loader');

            $.get(SITEURL + "/modules/digishop/" + type, {
                cid: config.cat
            }, function (data) {
                setTimeout(function () {
                    $container.html(data).fadeIn();
                    $container.removeClass('loader');
                    $.cookie("DIGIVIEW_VERIASIST", mode, {
                        expires: 120,
                        path: '/'
                    });

                    if (mode == "grid") {
                        $('#digiwrap.digigrid').waitForImages(function () {
                            $('#digiwrap.digigrid').elasticColumns('refresh');
                            $('#digiwrap.digigrid').Grid('displayItems')
                        });
                    }

                }, 500);
            });
        });

        $('#digishop').on('click', 'a.like', function () {
            $el = $(this).children('small')
            id = $(this).data("id");
            total = $(this).data("total");
            $.post(SITEURL + config.url, {
                doLike: 1,
                id: id,
                total: total
            }, function (data) {
                $($el).html(data);
            });
            $(this).removeClass('like');
        });
        
        
        $('#country , #country1 ').change(function(){
        	var country_id = $(this).val();
        	var cityName = $(this).data('target');
        		$.ajax({
        			url: SITEURL + config.url + '?cities='+country_id,
        			success:function(result){
        				$('#'+cityName).removeAttr('disabled','disabled');
        				$('#'+cityName).html(result);
        			}
        		});
        });

        $("body").on('click', ".addtocart", function () {
            $id = $(this).data("id");
            $qnt = $("#"  + $(this).data("quantity")).val();

            loader('.addtocart[data-id="'+$id+'"]', 'add', 'section');
            
            $.ajax({
                type: "post",
                dataType: "json",
                url: SITEURL + config.url,
                data: {
                    addtocart: $qnt,
                    id: $id
                },
                success: function (json) {
                    setTimeout(function () {
                        loader('.addtocart[data-id="'+$id+'"]', 'remove', 'section');
                        if (json.status == "success") {
                            $("#blink").effect( "shake" );
                            loadCartSmall();
                            $("#cartModal").modal("toggle");
                        } else {
                            $(this).text(json.message)
                        }

                    }, 800);
                }
            });
        });

        $("body").on('click', ".removeitem", function () {
            $id = $(this).data("id");

            loader('#cartlist', 'add');

            $.ajax({
                type: "post",
                url: SITEURL + config.url,
                data: {
                    removeitem: 1,
                    id: $id
                },
                success: function (msg) {
                    setTimeout(function () {
                        loader('#cartlist', 'remove');
                        loadCartSmall();
                    }, 800);
                }
            });
        });

        $("#doOffline").on('click', function () {
            loader("#doOffline", "add", '.panel');

            $.ajax({
                type: 'POST',
                url: SITEURL + "/modules/digishop/controller.php",
                data: {
                    doOffline: 1
                },
                success: function (msg) {
                    loader("#doOffline", "remove", ".panel");
                    $("#show-result").html(msg);
                    $("#cko").slideUp();
                }
            });
            return false;
        });

        $("#invoiceAddresses").change(function(){

            $id = $(this).val();
            $form = $("#veriasist_form");

            if($id == 0) {

                $($form).find("select[name=country]").attr("disabled", false).trigger("chosen:updated");
                $($form).find("input[name=addressName]").val("").attr("disabled", false);
                $($form).find("textarea[name=address]").val("").attr("disabled", false);
                $($form).find("select[name=country]").val("0").trigger("chosen:updated");
                $($form).find("input[name=state_province]").val("").attr("disabled", false);
                $($form).find("input[name=zip_post]").val("").attr("disabled", false);
                $($form).find("select[name=city]").html("<option>Seçiniz</option>");

                return false;
            }
            $.ajax({
                type: "POST",
                url: SITEURL + config.url,
                dataType: 'json',
                data: {getAddress:'', id:$id},
                cache: false,
                success: function (json) {
                    json = JSON.parse(JSON.stringify(json));
                    $($form).find("input[name=addressName]").val(json.name);
                    $($form).find("textarea[name=address]").val(json.address);
                    $($form).find("select[name=country]").val(json.country).trigger("chosen:updated");
                    $($form).find("input[name=state_province]").val(json.state_province);
                    $($form).find("input[name=zip_post]").val(json.zip_post);
                    $($form).find("select[name=city]").html(json.city);

                    $($form).find("input[name=addressName], \
                        textarea[name=address], \
                        select[name=country], \
                        input[name=state_province], \
                        input[name=zip_post], \
                        input[name=city] \
                        ").attr("disabled", true);
                }
            });
        });
        $("#invoiceAddresses1").change(function(){

            $id = $(this).val();
            $form = $("#veriasist_form");

            if($id == 0) {       

                $($form).find("select[name=country1]").attr("disabled", false).trigger("chosen:updated");
                $($form).find("input[name=addressName1]").val("").attr("disabled", false);
                $($form).find("textarea[name=address1]").val("").attr("disabled", false);
                $($form).find("select[name=country1]").val("0").trigger("chosen:updated");
                $($form).find("input[name=state_province1]").val("").attr("disabled", false);
                $($form).find("input[name=zip_post1]").val("").attr("disabled", false);
               	$($form).find("select[name=city1]").html("<option>Seçiniz</option>");
                
                return false;
            }
            $.ajax({
                type: "POST",
                url: SITEURL + config.url,
                dataType: 'json',
                data: {getAddress:'', id:$id},
                cache: false,
                success: function (json) {
                    json = JSON.parse(JSON.stringify(json));
                    $($form).find("input[name=addressName1]").val(json.name);
                    $($form).find("textarea[name=address1]").val(json.address);
                    $($form).find("select[name=country1]").val(json.country).trigger("chosen:updated");
                    $($form).find("input[name=state_province1]").val(json.state_province);
                    $($form).find("input[name=zip_post1]").val(json.zip_post);
                    $($form).find("select[name=city1]").html(json.city);

                    $($form).find("input[name=addressName1], \
                        textarea[name=address1], \
                        select[name=country1], \
                        input[name=state_province1], \
                        input[name=zip_post1], \
                        input[name=city1] \
                        ").attr("disabled", true);
                }
            });
        });

        $("#sameaddress").change(function() {
            if( $(this).is(':checked') )
                $("#secondAddress").slideUp();
            else
                $("#secondAddress").slideDown();
        });

        $("#paymentNext2").on("click", function(){
             window.location = SITEURL + '/digishop/checkout/?shipping';
        });


        $('#paymentNext3').on('click', function () {
            loader("#veriasist_form", "add", ".panel");
            id = $(this).data("id");
            $.ajax({
                type: "post",
                dataType: "json",
                url: SITEURL + config.url,
                data: $("#veriasist_form").serialize(),
                success: function (json) {
                    setTimeout(function () {
                        loader("#veriasist_form", "remove", ".panel");
                        if(json.type == "success") {
                            $("#msgholder").html('');
                            window.location = SITEURL + '/digishop/checkout/?payment';
                        }
                        else {
                            $("#msgholder").html(json.message);
                        }
                    }, 800);
                }
            });
        });
        
        function loadCartSmall() {
            $.ajax({
                type: "POST",
                url: SITEURL + config.url,
                dataType: 'json',
                data: 'loadcart=1',
                cache: false,
                success: function (json) {
                    $("#cartlist, #cartModal").find("tbody").html(json.message);
                }
            });
        }


        function loader(element, action, clsst) {
            if(typeof clsst === 'undefined' || clsst == '')
              clsst = '';

          if(action == 'add') {

              if(clsst)
                $(element).closest(clsst).addClass("loader");
            else
                $(element).addClass("loader");
            }
            
            else if(action == 'remove') {

                if(clsst)
                  $(element).closest(clsst).removeClass("loader");
              else
                  $(element).removeClass("loader");
            }
          }



          $("body").on("click", ".spinner-control-button", function(){
            $target = $(this).data('target');
            $id = $(this).data('id');

            $.ajax({
                type: "POST",
                url: SITEURL + config.url,
                data: "addDelItem=1&id="+$id+"&target="+$target,
                cache: false,
                success: function(json){
                    loadCartSmall();
                }
            });
            
          });

          $("body").on("keyup", ".spinner-control-input", function(){
            $value = $(this).data('value');
            $id = $(this).data('id');
            $quantity = $(this).val();

            $.ajax({
                type: "POST",
                url: SITEURL + config.url,
                data: "addDelItemINPUT=1&id="+$id+"&value="+$value+"&quantity="+$quantity,
                cache: false,
                success: function(json){
                    setTimeout(function () {
                        loadCartSmall();
                    }, 1500);
                }
            });
          });


    // };
})(jQuery);
<?php
  /**
   * Digishop Controller
   *
   * @package Veri Asist
   * @author veriasist.com
   * @copyright 2014
   * @version $Id: controller.php, v4.00 2014-04-20 16:18:34 gewa Exp $
   */

  define("_VALID_PHP", true);
  require_once ("../../init.php");
  
  require_once (MODPATH . "digishop/admin_class.php");
  Registry::set('Digishop', new Digishop());
?>
<?php
  /* == get invoice address == */
  if (isset($_POST['getAddress'], $_POST['id']))
  {
    Registry::get("Digishop")->getUserAddresses( intval($_POST['id']) );
  }

  if (isset($_POST['paymentNext3']))
  { 
    Registry::get("Digishop")->paymentAddresses();
  }

?>
<?php
	if(isset($_GET['cities'])){
		$cities = Registry::get("Digishop")->getCities($_GET['cities']);
		
		foreach ($cities as $key => $value) {
			
			echo '<option value='.$value->city_id.'>'.$value->name.'</option>';
		}
	}
?>
<?php
  /* == Process Lke == */
  if (isset($_POST['doLike']) and Filter::$id and Registry::get("Digishop")->like):
      $data['likes'] = "INC(1)";
      $db->update(Digishop::mTable, $data, "id = " . Filter::$id);
      print intval($_POST['total'] + 1);

  endif;
?>
<?php
  /* == Add to Cart == */
  if (isset($_POST['addtocart'])):
      if ($row = $db->first("SELECT id, membership_id, price FROM " . Digishop::mTable . " WHERE id = " . Filter::$id . " AND active = 1")):
          if ($row->membership_id == 0):
              $data = array(
                  'user_id' => sanitize($user->sesid),
                  'pid' => $row->id,
                  'price' => floatval($row->price),
                  'uid' => $user->uid);

              $quantity = intval($_POST['addtocart']) < 1 ? 1 : intval($_POST['addtocart']);

              for ($i=1; $i <= $quantity; $i++) { 
                $db->insert(Digishop::xTable, $data);
              }

			  $json['status'] = 'success';
			  print json_encode($json);
          endif;
		  
      else:
		  $json['status'] = 'error';
		  $json['message'] = Lang::$word->_MOD_DS_ILLEGAL;
		  print json_encode($json);
      endif;
  endif;
  
  /* == Load Cart == */
  if (isset($_POST['loadcart'])):

    $html = Registry::get("Digishop")->loadcart();

	  $json['status'] = 'success';
	  $json['message'] = $html;
	  print json_encode($json);
	  
  endif;


  /* == Remove Item == */
  if (isset($_POST['removeitem'])):
    if( $db->delete(Digishop::xTable, "user_id = '".$db->escape($user->sesid)."' AND pid = " . Filter::$id) )
      echo "success";
  endif;


  /* == Add or Delete at cart by buttons == */
  if(isset($_POST['addDelItem'])):
  if($_POST['target'] == 1) { // ADD

    if( $db->query('INSERT INTO '.Digishop::xTable.' (user_id, pid, price, uid, adr1, adr2)
      SELECT user_id, pid, price, uid, adr1, adr2 FROM '.Digishop::xTable.' WHERE user_id ="'.$db->escape($user->sesid).'" AND pid='.Filter::$id.' LIMIT 1') )
      echo "success";
    else echo "error";
  }
  elseif($_POST['target'] == -1) {
    if( $db->delete(Digishop::xTable, "user_id = '".$db->escape($user->sesid)."' AND pid = " . Filter::$id . ' LIMIT 1 ') )
      echo "success";
    else echo "error";
  }
  endif;


    /* == Add or Delete at cart bay input == */
  if (isset($_POST['addDelItemINPUT'])):

    $quantity = intval($_POST['quantity']) > 0 ? intval($_POST['quantity']) : false;
    $item = $db->first('SELECT * FROM '.Digishop::xTable.' WHERE user_id = "'.$db->escape($user->sesid).'" AND pid = '.Filter::$id.' LIMIT 1');
    if($item && $quantity)
    {
      $db->delete(Digishop::xTable, "user_id = '".$db->escape($user->sesid)."' AND pid = " . Filter::$id);

      $data = array(
        'user_id' => $item->user_id,
        'pid' => $item->pid,
        'price' => $item->price,
        'uid' => $item->uid,
        'adr1' => $item->adr1,
        'adr2' => $item->adr2,
        );

      for ($i=1; $i <= $quantity; $i++) { 
         $db->insert(Digishop::xTable, $data);
      }
    }
  endif;


  
  /* == Offline Payment == */
  if (isset($_POST['doOffline'])):
      $cartrow = Registry::get("Digishop")->getCartContent();
      $totalrow = Registry::get("Digishop")->getCartTotal();
      $max_ = Registry::get('Digishop')->getLatestTransactionId();
      $max_id = ( isset($max_->transaction_id) && $max_->transaction_id ) ? intval($max_->transaction_id + 1) : 1;
      $gross = $totalrow->total;

      if ($cartrow):
          $txn_id = 'BLG_' . $max_id . '_' . time();
          foreach ($cartrow as $k => $crow) :
              $data = array(
                  'transaction_id' => $max_id,
                  'txn_id'         => $txn_id,
                  'pid'            => $crow->pid,
                  'uid'            => intval($user->uid),
                  'downloads'      => 0,
                  'file_date'      => time(),
                  'token'          => sha1(time() . $k . $crow->pid),
                  'ip'             => sanitize($_SERVER['REMOTE_ADDR']),
                  'created'        => "NOW()",
                  'payer_email'    => sanitize($user->email),
                  'payer_status'   => "verified",
                  'item_qty'       => $crow->total,
                  'price'          => $crow->total * $crow->price,
                  'unit_price'     => $crow->price,
                  'mc_fee'         => 0.00,
                  'currency'       => sanitize($core->currency),
                  'pp'             => "Offline",
                  'address1'       =>$crow->adr1,
                  'address2'       =>($crow->adr2 ? $crow->adr2 : $crow->adr1),
                  'status'         => 0,
                  'active'         => 0);

            $db->insert(Digishop::trTable, $data);

              $address1 = $crow->adr1;
              $address2 = $crow->adr2;
          endforeach;
          unset($crow);

          print Filter::msgSingleInfo(Lang::$word->_MOD_DS_MSG8);


          // <--- Adres geçmişi tutuluyor ---> //
          $address[1] = $db->first('SELECT 
            CONCAT( 
            _address," ",
            _zip_post, " ",
            _state_province, " ",
            (SELECT name FROM _cities WHERE city_id=mod_invoices_users._city LIMIT 1), " " ,
            (SELECT name FROM _countries WHERE country_id=mod_invoices_users._country LIMIT 1) ) AS address
            FROM mod_invoices_users WHERE id = '.$address1.' AND user_id = '.intval($user->uid).' AND _active = 1 LIMIT 1');
          $address[2] = $db->first('SELECT 
            CONCAT( 
            _address," ",
            _zip_post, " ",
            _state_province, " ",
            (SELECT name FROM _cities WHERE city_id=mod_invoices_users._city LIMIT 1), " " ,
            (SELECT name FROM _countries WHERE country_id=mod_invoices_users._country LIMIT 1) ) AS address
            FROM mod_invoices_users WHERE id = '.$address2.' AND user_id = '.intval($user->uid).' AND _active = 1 LIMIT 1');
          if(!$address[1]) {
            $json['status'] = 'error';
            $json['message'] = print_r($cartrow);
            print json_encode($json);
            exit;
          }
          $data1 = array(
            'transaction_id'  => $max_id,
            'address1'        => $address[1]->address,
            'address2'        => ($address[2] && isset($address[2]->address) ? $address[2]->address : $address[1]->address)
          );
		  
          $db->insert('mod_digishop_transaction_addresses', $data1);
          // <--- Adres geçmişi tutuluyor ---> //
          
          
          $db->insert('mod_digishop_transaction_statuses', ['transaction_id'=>$max_id, 'created'=>"NOW()"]);

          /* == Create Invoice == */
          if (Core::checkTable("mod_invoices")):
              $idata = array(
                  'user_id' => $user->uid,
                  'transaction_id' => $max_id,
                  'ppid' => 'offline',
                  'pp' => 'Offline',
                  'currency' => $core->currency,
                  'cur_symbol' => $core->cur_symbol,
                  'total' => floatval($gross),
                  'paid' => 0.00,
                  'token' => sha1(time() . $max_id),
                  'created' => "NOW()");

              $last_id = $db->insert("mod_invoices", $idata);

              $pdata = array(
                  'title' => Lang::$word->_MOD_DS_OFFTITLE . ' - ' . $user->name,
                  'user_id' => $user->uid,
                  'invoice_id' => $last_id,
                  'description' => Lang::$word->_MOD_DS_OFFTITLE,
                  'price' => floatval($gross),
                  'qty' => 1);

              $db->insert("mod_invoices_products", $pdata);
          endif;

          /* == Notify User == */
          require_once (BASEPATH . "lib/class_mailer.php");

          $sql = "SELECT t.*, p.id as pid, p.title" . Lang::$lang . " as ptitle, p.price, COUNT(t.pid) as total" 
		  . "\n FROM " . Digishop::trTable . " as t" 
		  . "\n LEFT JOIN " . Digishop::mTable . " as p ON p.id = t.pid" 
		  . "\n WHERE t.uid = '" . intval($user->uid) . "' AND txn_id = '" . sanitize($txn_id) . "'" 
		  . "\n GROUP BY t.pid ORDER BY t.id DESC";
		  
          $transrow = $db->fetch_all($sql);

          $html = '
			<table width="100%" border="0" cellpadding="4" cellspacing="2">
			  <thead>
				<tr>
				  <td width="20"><strong>#</strong></td>
				  <td><strong>' . Lang::$word->_MOD_DS_NAME . '</strong></td>
				  <td><strong>' . Lang::$word->_MOD_DS_PRODPRICE . '</strong></td>
				  <td><strong>' . Lang::$word->_MOD_DS_QTY . '</strong></td>
				  <td><strong>' . Lang::$word->_MOD_DS_TOTPRICE . '</strong></td>
				</tr>
			</thead>';
          $i = 0;
          foreach ($transrow as $ccrow) {
              $i++;
              $html .= '
				  <tr>
					<td style="border-bottom-width:1px; border-bottom-color:#bbb; border-bottom-style:dashed">' . $i . '.</td>
					<td style="border-bottom-width:1px; border-bottom-color:#bbb; border-bottom-style:dashed">' . sanitize($ccrow->ptitle, 30) . '</td>
					<td style="border-bottom-width:1px; border-bottom-color:#bbb; border-bottom-style:dashed">' . $core->formatMoney($ccrow->price) . '</td>
					<td align="center" style="border-bottom-width:1px; border-bottom-color:#bbb; border-bottom-style:dashed">' . $ccrow->item_qty . '</td>
					<td align="right" style="border-bottom-width:1px; border-bottom-color:#bbb; border-bottom-style:dashed">' . $core->formatMoney($ccrow->item_qty * $ccrow->price) . '</td>
				</tr>';
          }
          unset($ccrow);
          $html .= '
			<tr>
				<td colspan="4" align="right" valign="top"><strong style="color:#F00">' . Lang::$word->_MOD_DS_GTOTAL . ':</strong></td>
				<td align="right" valign="top"><strong style="color:#F00">' . $core->formatMoney($gross) . '</strong></td>
			  </tr>
			</tr>';
          if (Core::checkTable("mod_invoices")) {
              // $ilink = SITEURL . '/modules/invoice/view_invoice.php?token=' . $idata['token'];
              $ilink = SITEURL . '/panel/orders';
              $html .= '
				<tr>
				  <td colspan="5" align="right" valign="top">&nbsp;</td>
				</tr>
				<tr>
				  <td colspan="5">' . Lang::$word->_MOD_DS_INV_INFO . '<br />
				  <a href="' . $ilink . '">' . Lang::$word->_MOD_DS_INV_VIEW . '</a></td>
				</tr>';
          }
          $html .= '
		  </table>';

          $template = Core::getRowById(Content::eTable, 15);
          $info = getValueById("extra3", Membership::gTable, 3);

          $body3 = str_replace(array(
              '[NAME]',
              '[ITEMS]',
              '[INFO]',
              '[SITENAME]',
              '[URL]'), array(
              $user->username,
              $html,
              $info,
              $core->site_name,
              SITEURL), $template->{'body' . Lang::$lang}
			  );
			  
          $newbody2 = cleanOut($body3);
          $mailer2 = Mailer::sendMail();
          $message2 = Swift_Message::newInstance()
					->setSubject($template->{'subject' . Lang::$lang})
					->setTo(array($user->email => $user->username))
					->setFrom(array($core->site_email => ucfirst($core->site_name)))
					->setBody($newbody2, 'text/html');

          $mailer2->send($message2);

          /* == Notify Administrator == */
          $row2 = Core::getRowById(Content::eTable, 5);

          $body = str_replace(array(
              '[USERNAME]',
              '[ITEMNAME]',
              '[PRICE]',
              '[STATUS]',
              '[PP]',
              '[IP]'), array(
              $user->username,
              "Offline Digishop Order",
              $core->formatMoney($gross),
              "Pending",
              "Offline",
              $_SERVER['REMOTE_ADDR']), $row2->{'body' . Lang::$lang});

          $newbody = cleanOut($body);

          $mailer3 = Mailer::sendMail();
          $message = Swift_Message::newInstance()
					->setSubject($row2->{'subject' . Lang::$lang})
					->setTo(array($core->site_email => $core->site_name))
					->setFrom(array($core->site_email => $core->site_name))
					->setBody($newbody, 'text/html');

          $mailer3->send($message);

          $db->delete(Digishop::xTable, "user_id='" . $user->sesid . "'");

      endif;
  endif;
?>

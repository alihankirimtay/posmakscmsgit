<?php
  /**
   * Digishop Grid View
   *
   * @package Veri Asist
   * @author veriasist.com
   * @copyright 2014
   * @version $Id: grid-view.php, v4.00 2014-04-20 16:18:34 gewa Exp $
   */
  
  define("_VALID_PHP", true);
  require_once ("../../init.php");
  
  require_once (MODPATH . "digishop/admin_class.php");
  Registry::set('Digishop', new Digishop());
  
  $cid = (isset($_GET['cid'])) ? intval($_GET['cid']) : null;
  $slug = (isset($_GET['slug'])) ? sanitize($_GET['slug']) : null;
  $digirow = ($cid == 0 or empty($cid)) ? Registry::get("Digishop")->renderProducts() : Registry::get("Digishop")->renderProductByCategory($cid, $slug);
?>
<div id="digiwrap" class="clearfix digigrid">
  <?php foreach($digirow as $row):?>
  <div class="item">
    <section>
      <div class="image-overlay"> <img src="<?php echo SITEURL.'/'.Digishop::imagepath . $row->thumb;?>" alt="">
        <div class="overlay-fade"> <a href="<?php echo doUrl(false, $row->slug, "digishop-item");?>"><i class="icon-overlay icon url"></i></a> </div>
      </div>
    </section>
    <article>
      <h4><a class="inverted" href="<?php echo doUrl(false, $row->slug, "digishop-item");?>"><?php echo truncate($row->{'title' . Lang::$lang},20);?></a></h4>
      <p><span><?php echo $core->formatMoney($row->price);?></span></p>
      <?php if($row->membership_id <> 0):?>
      <?php Registry::get("Digishop")->getPriceMembershipData($row->id, $row->token, $row->price, $row->membership_id)?>
      <?php else:?>
      <?php Registry::get("Digishop")->getPriceData($row->id, $row->token, $row->price);?>
      <?php endif;?>
    </article>
  </div>
  <?php endforeach;?>
</div>
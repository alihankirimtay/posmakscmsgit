<?php
  /**
   * Digishop Grid View
   *
   * @package Veri Asist
   * @author veriasist.com
   * @copyright 2014
   * @version $Id: grid-view.php, v4.00 2014-04-20 16:18:34 gewa Exp $
   */
  
  define("_VALID_PHP", true);
  require_once ("../../init.php");
  
  require_once (MODPATH . "digishop/admin_class.php");
  Registry::set('Digishop', new Digishop());
  
  $cid = (isset($_GET['cid'])) ? intval($_GET['cid']) : null;
  $slug = (isset($_GET['slug'])) ? sanitize($_GET['slug']) : null;
  $digirow = ($cid == 0 or empty($cid)) ? Registry::get("Digishop")->renderProducts() : Registry::get("Digishop")->renderProductByCategory($cid, $slug);
?>
<div id="digiwrap" class="clearfix digilist">
  <div class="columns">
    <?php foreach($digirow as $row):?>
    <div class="veriasist tabular segment">
      <div class="screen-25 tablet-40 phone-100">
        <aside>
          <div class="image-overlay"><img src="<?php echo SITEURL.'/'.Digishop::imagepath . $row->thumb;?>" alt="">
            <div class="overlay-fade"> <a href="<?php echo doUrl(false, $row->slug, "digishop-item");?>"><i class="icon-overlay icon url"></i></a> </div>
          </div>
        </aside>
      </div>
      <div class="screen-75 tablet-60 phone-100">
        <section>
          <div class="veriasist huge top right attached label"><?php echo $core->formatMoney($row->price);?></div>
          <h4><a class="inverted" href="<?php echo doUrl(false, $row->slug, "digishop-item");?>"><?php echo $row->{'title' . Lang::$lang};?></a></h4>
          <p><?php echo cleanSanitize($row->{'body' . Lang::$lang},400);?></p>
          <div class="veriasist divider"></div>
          <?php if($row->membership_id <> 0):?>
          <?php Registry::get("Digishop")->getPriceMembershipData($row->id, $row->token, $row->price, $row->membership_id)?>
          <?php else:?>
          <?php Registry::get("Digishop")->getPriceData($row->id, $row->token, $row->price);?>
          <?php endif;?>
		  <?php if(Registry::get("Digishop")->like):?>
          <a data-id="<?php echo $row->id;?>" data-total="<?php echo $row->likes;?>" class="like push-right veriasist circular icon danger button"><i class="icon heart"></i> <small><?php echo $row->likes;?></small></a> 
          <?php endif;?>
             </section>
      </div>
    </div>
    <?php endforeach;?>
  </div>
</div>
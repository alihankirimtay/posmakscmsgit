<?php
  /**
   * Offline Form
   *
   * @package Veri Asist
   * @author veriasist.com
   * @copyright 2014
   * @version $Id: form.tpl.php, v4.00 2014-04-20 10:12:05 gewa Exp $
   */
  if (!defined("_VALID_PHP"))
      die('Direct access to this location is not allowed.');
?>
<div class="btn btn-default">
  <input id="doOffline" class="offline" type="image" src="<?php echo SITEURL.'/modules/digishop/gateways/offline/offline_big.png';?>" name="submit" title="Offline Payment" alt="Offline Payment">
</div>
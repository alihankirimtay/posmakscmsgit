<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
</head>  
<body>
<?php
require_once 'lib/PayuLiveUpdate.class.php';

//$liveUpdate = new PayuLu('PAYUDEMO','P5@F8*3!m0+?^9s3&u8(');
$liveUpdate = new PayuLu('OPU_TEST','SECRET_KEY');

//Show all errors
$liveUpdate->setDebug(PayuLu::DEBUG_ALL);

//pname,pcode,pinfo,price,priceType,quantity,tax

$pname = "Product nameĞŞÇÖıİ";
$pcode = "Product code";
$pinfo = "Product info";
$price = "89.90";
$priceType = "GROSS";
$quantity = "1";
$tax = "67";
$product = new PayuProduct($pname,$pcode,$pinfo,$price,$priceType,$quantity,$tax);

$liveUpdate->setOrderRef("6112457");
$liveUpdate->addProduct($product);


//Billing adresi hash stringe dahil olmaz. Dolayısıyla hash değerinin
//hesabına bir etkisi yoktur.
//Fakat billing bilgisinin gönderilmesi zorunludur.
//Bundan dolayı billing için first name, last name ve email gönderilmelidir

$billing = new PayuAddress();
$billing->setFirstName('Mehmet Emin');
$billing->setLastName('Coşkun');
$billing->setEmail('mehmet.coskun@payu.com.tr');
$billing->setCity("Kağıthane"); //Ilce/Semt
$billing->setState("Istanbul"); //Sehir
$billing->setCountryCode("TR");
$liveUpdate->setBillingAddress($billing);

$liveUpdate->setPaymentCurrency("TRY");
$liveUpdate->setInstalments("2,3,7,10,12");
$liveUpdate->setOrderShipping("");
$liveUpdate->setBackRef("");

$liveUpdate->setButtonName('Go to Payment Page');

$t = $liveUpdate->renderPaymentForm();

?>
</body></html>

<?php

/**
 * PayPal IPN
 *
 * @package Veri Asist
 * @author veriasist.com
 * @copyright 2014
 * @version $Id: ipn.php, 2014-04-10 21:12:05 gewa Exp $
 */
define("_VALID_PHP", true);
define("_PIPN", true);


ini_set('log_errors', true);
ini_set('error_log', dirname(__FILE__) . '/ipn_errors.log');

// $p = 'POST{'.implode(', ', $_POST).'}';
// file_put_contents(dirname(__FILE__) . '/ipn_errors.log', $p);
if (isset($_POST['payment_status'])) {
    require_once ("../../../../init.php");

    require_once (BASEPATH . "lib/class_pp.php");
    $listener = new IpnListener();

    require_once (MODPATH . "digishop/admin_class.php");
    Registry::set('Digishop', new Digishop());
    $digishop = Registry::get("Digishop");

    $pp = Core::getRow(Membership::gTable, "name", "paypal");

    $listener->use_live = $pp->live;
    $listener->use_ssl = true;
    $listener->use_curl = false;

    try {
        $listener->requirePostMethod();
        $ppver = $listener->processIpn();
    } catch (exception $e) {
        error_log($e->getMessage(), 3, "pp_errorlog.log");
        exit(0);
    }

    $payment_status = $_POST['payment_status'];
    $receiver_email = $_POST['business'];
    $payer_email = $_POST['payer_email'];
    $payer_status = $_POST['payer_status'];
    $mc_currency = $_POST['mc_currency'];
	
	

    list($user_id, $sesid) = explode('_', $_POST['custom']);
    $mc_gross = $_POST['mc_gross'];
    $txn_id = $_POST['txn_id'];
	
	

    $getxn_id = Digishop::verifyTxnId($txn_id);

    $cartrow = $digishop->getCartContent($sesid);
    if (!$cartrow)
        die('no item in cart');
    $totalrow = $digishop->getCartTotal($sesid);

    $max_ = Registry::get('Digishop')->getLatestTransactionId();
    $max_id = ( isset($max_->transaction_id) && $max_->transaction_id ) ? intval($max_->transaction_id + 1) : 1;

    $gross = $totalrow->total;

    $v1 = compareFloatNumbers($mc_gross, $gross, "=");

    if ($ppver) {
        if ($_POST['payment_status'] == 'Completed') {
            if ($receiver_email == $pp->extra && $v1 == true && $getxn_id == true) {
                if ($cartrow) {
                    foreach ($cartrow as $k => $crow) {
                        $data = array(
                            'transaction_id' => $max_id,
                            'txn_id' => sanitize($txn_id),
                            'pid' => $crow->pid,
                            'uid' => intval($user_id),
                            'downloads' => 0,
                            'file_date' => time(),
                            'token' => sha1(time() . $k),
                            'ip' => sanitize($_SERVER['REMOTE_ADDR']),
                            'created' => "NOW()",
                            'payer_email' => sanitize($payer_email),
                            'payer_status' => sanitize($payer_status),
                            'item_qty' => $crow->total,
                            'price' => $crow->total * $crow->price,
                            'unit_price' => $crow->price,
                            'mc_fee' => 0.00,
                            'currency' => sanitize($mc_currency),
                            'pp' => "PayPal",
                            'address1'=>$crow->adr1,
                            'address2'=>($crow->adr2 ? $crow->adr2 : $crow->adr1),
                            'status' => 1,
                            'active' => 1);

                        $db->insert(Digishop::trTable, $data);
						
					
                    }

                    $address1 = $crow->adr1;
                    $address2 = $crow->adr2;
                    unset($crow);

                    // <--- Adres geçmişi tutuluyor ---> //
                    $address[1] = $db->first('SELECT 
                      CONCAT( 
                        _address," ",
                        _zip_post, " ",
                        _state_province, " ",
                        (SELECT name FROM _cities WHERE city_id=mod_invoices_users._city LIMIT 1), " " ,
                        (SELECT name FROM _countries WHERE country_id=mod_invoices_users._country LIMIT 1) ) AS address
                    FROM mod_invoices_users WHERE id = '.$address1.' AND user_id = '.intval($user_id).' AND _active = 1 LIMIT 1');
                    
                    $address[2] = $db->first('SELECT 
                      CONCAT( 
                        _address," ",
                        _zip_post, " ",
                        _state_province, " ",
                        (SELECT name FROM _cities WHERE city_id=mod_invoices_users._city LIMIT 1), " " ,
                        (SELECT name FROM _countries WHERE country_id=mod_invoices_users._country LIMIT 1) ) AS address
                    FROM mod_invoices_users WHERE id = '.$address2.' AND user_id = '.intval($user_id).' AND _active = 1 LIMIT 1');
                  
				     //if(!$address[1]) {
                     //  $json['status'] = 'error';
                     //  $json['message'] = 'Address Error!';
                     //  print json_encode($json);
                     //}
                    
					
					
                    $data1 = array(
                      'transaction_id'  => $max_id,
                      'address1'        => $address[1]->address,
                      'address2'        => ($address[2] && isset($address[2]->address) ? $address[2]->address : $address[1]->address)
                      );
                    $db->insert('mod_digishop_transaction_addresses', $data1);
                    // <--- Adres geçmişi tutuluyor ---> //


                    $db->insert('mod_digishop_transaction_statuses', ['transaction_id'=>$max_id, 'created'=>"NOW()"]);
                    $db->insert('mod_digishop_transaction_statuses', ['transaction_id'=>$max_id, 'created'=>"NOW()", 'status'=>1]);

                  /* == Create Invoice == */
                  if (Core::checkTable("mod_invoices")):
                      $idata = array(
                          'user_id' => $user_id,
                          'transaction_id' => $max_id,
                          'ppid' => 'offline',
                          'pp' => 'Offline',
                          'currency' => $core->currency,
                          'cur_symbol' => $core->cur_symbol,
                          'total' => floatval($gross),
                          'paid' => floatval($gross),
                          'token' => sha1(time()),
                          'created' => "NOW()");

                      $last_id = $db->insert("mod_invoices", $idata);

                      $pdata = array(
                          'title' => Lang::$word->_MOD_DS_OFFTITLE . ' - ' . $user->name,
                          'user_id' => $user_id,
                          'invoice_id' => $last_id,
                          'description' => Lang::$word->_MOD_DS_OFFTITLE,
                          'price' => floatval($gross),
                          'qty' => 1);

                      $db->insert("mod_invoices_products", $pdata);
                  endif;
                }

                /* == Notify User == */
                require_once (BASEPATH . "lib/class_mailer.php");

                $usr = Core::getRowById(Users::uTable, $user_id);

                $sql = "SELECT t.*, p.id as pid, p.title" . Lang::$lang . " as ptitle, p.price, COUNT(t.pid) as total"
                        . "\n FROM " . Digishop::trTable . " as t"
                        . "\n LEFT JOIN " . Digishop::mTable . " as p ON p.id = t.pid"
                        . "\n WHERE t.uid = '" . intval($user_id) . "' AND txn_id = '" . sanitize($txn_id) . "'"
                        . "\n GROUP BY t.pid ORDER BY t.id DESC";
                $transrow = $db->fetch_all($sql);

                $html = '
          <table width="100%" border="0" cellpadding="4" cellspacing="2">
            <thead>
            <tr>
              <td width="20"><strong>#</strong></td>
              <td><strong>' . Lang::$word->_MOD_DS_NAME . '</strong></td>
              <td><strong>' . Lang::$word->_MOD_DS_PRODPRICE . '</strong></td>
              <td><strong>' . Lang::$word->_MOD_DS_QTY . '</strong></td>
              <td><strong>' . Lang::$word->_MOD_DS_TOTPRICE . '</strong></td>
            </tr>
          </thead>';

                $i = 0;
                foreach ($transrow as $ccrow) {
                    $i++;
                    $html .= '
              <tr>
              <td style="border-bottom-width:1px; border-bottom-color:#bbb; border-bottom-style:dashed">' . $i . '.</td>
              <td style="border-bottom-width:1px; border-bottom-color:#bbb; border-bottom-style:dashed">' . sanitize($ccrow->ptitle, 30) . '</td>
              <td style="border-bottom-width:1px; border-bottom-color:#bbb; border-bottom-style:dashed">' . $core->formatMoney($ccrow->price) . '</td>
              <td align="center" style="border-bottom-width:1px; border-bottom-color:#bbb; border-bottom-style:dashed">' . $ccrow->item_qty . '</td>
              <td align="right" style="border-bottom-width:1px; border-bottom-color:#bbb; border-bottom-style:dashed">' . $core->formatMoney($ccrow->item_qty * $ccrow->price) . '</td>
            </tr>';
                }
                unset($ccrow);
                $html .= '
          <tr>
            <td colspan="4" align="right" valign="top"><strong style="color:#F00">' . Lang::$word->_MOD_DS_GTOTAL . ':</strong></td>
            <td align="right" valign="top"><strong style="color:#F00">' . $core->formatMoney($gross) . '</strong></td>
            </tr>
          </tr>';

                $body3 = str_replace(array(
                    '[NAME]',
                    '[ITEMS]',
                    '[DASH]',
                    '[SITE_NAME]',
                    '[URL]'), array(
                    $usr->username,
                    $html,
                    doUrl(false, Registry::get("Core")->account_page, "page"),
                    $core->site_name,
                    SITEURL), $digishop->template);
                $newbody2 = cleanOut($body3);

                $mailer2 = Mailer::sendMail();
                $message2 = Swift_Message::newInstance()
                        ->setSubject(Lang::$word->_MOD_DS_R_SUBJECT . $core->site_name)
                        ->setTo(array($usr->email => $usr->fname . ' ' . $usr->fname))
                        ->setFrom(array($core->site_email => $core->site_name))
                        ->setBody($newbody2, 'text/html');

                $mailer2->send($message2);

                $db->delete(Digishop::xTable, "user_id='" . $sesid . "'");

            }
        } else {
            /* == Failed Transaction= = */
            require_once (BASEPATH . "lib/class_mailer.php");
            $row = Core::getRowById(Content::eTable, 6);
            $user = Core::getRowById(Users::uTable, $user_id);

            $body = str_replace(array(
                '[USERNAME]',
                '[ITEM]',
                '[STATUS]',
                '[TOTAL]',
                '[PP]',
                '[IP]'), array(
                $usr->username,
                "Item(s) from DigiShop",
                "Failed",
                $core->formatMoney($gross),
                "PayPal",
                $_SERVER['REMOTE_ADDR']), $row->{'body' . Lang::$lang});

            $newbody = cleanOut($body);

            $mailer = Mailer::sendMail();
            $message = Swift_Message::newInstance()
                    ->setSubject($row->{'subject' . Lang::$lang})
                    ->setTo(array($core->site_email => $core->site_name))
                    ->setFrom(array($core->site_email => $core->site_name))
                    ->setBody($newbody, 'text/html');

            $mailer->send($message);
        }
    }
}
?>
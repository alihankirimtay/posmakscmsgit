<?php
  /**
   * Paypal Form
   *
   * @package Digital Downloads Pro
   * @author veriasist.com
   * @copyright 2014
   * @version $Id: form.tpl.php, v4.00 2014-04-20 10:12:05 gewa Exp $
   */
  if (!defined("_VALID_PHP"))
      die('Direct access to this location is not allowed.');
?>
<div class="btn btn-default">
  <?php $url = ($grows->live) ? 'www.paypal.com' : 'www.sandbox.paypal.com';?>
  <form action="https://<?php echo $url;?>/cgi-bin/webscr" method="post" id="pp_form" name="pp_form">
    <input type="image" src="<?php echo SITEURL.'/modules/digishop/gateways/paypal/paypal_big.png';?>" name="submit" title="Pay With Paypal" alt="Pay With Paypal" onclick="document.pp_form.submit();"/>
    <input type="hidden" name="cmd" value="_cart"/>
    <input type="hidden" name="upload" value="1" />
    <input type="hidden" name="business" value="<?php echo $grows->extra;?>" />
    <input type="hidden" name="notify_url" value="<?php echo SITEURL;?>/modules/digishop/gateways/paypal/ipn.php" />
    <!-- <input type="hidden" name="return" value="<?php echo doUrl(false, $core->account_page, "page");?>" /> -->
    <!-- <input type="hidden" name="return" value="<?php echo SITEURL;?>/modules/digishop/gateways/paypal/ipn.php" /> -->
    <input type="hidden" name="return" value="<?php echo SITEURL;?>/digishop/checkout/?result" />
    <input type="hidden" name="currency_code" value="<?php echo ($grows->extra2) ? $grows->extra2 : $core->currency;?>" />
    <input type="hidden" name="custom" value="<?php echo $user->uid.'_'.$user->sesid;?>" />
    <input type="hidden" name="no_note" value="0" />
    <input type="hidden" name="rm" value="2" />
    <?php $x = 0;?>
    <?php foreach ($cartrow as $crow):?>
    <?php $x++; ?>
    <input type="hidden" name="item_name_<?php echo $x;?>" value="<?php echo cleanOut($crow->ptitle);?>" />
    <input type="hidden" name="item_number_<?php echo $x;?>" value="<?php echo $crow->pid;?>" />
    <input type="hidden" name="quantity_<?php echo $x;?>" value="<?php echo $crow->total;?>" />
    <input type="hidden" name="amount_<?php echo $x;?>" value="<?php echo $crow->price;?>" />
    <?php endforeach;?>
    <?php unset($crow);?>
  </form>
</div>

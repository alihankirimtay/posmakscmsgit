<?php
  /**
   * MoneyBookers IPN
   *
   * @package Veri Asist
   * @author veriasist.com
   * @copyright 2010
   * @version $Id: ipn.php,<?php echo  2010-10-10 21:12:05 gewa Exp $
   */
  define("_VALID_PHP", true);

  require_once ("../../../../init.php");

  require_once (MODPATH . "digishop/admin_class.php");
  Registry::set('Digishop', new Digishop());
  $digishop = Registry::get("Digishop");

  /* only for debuggin purpose. Create logfile.txt and chmot to 0777
  * ob_start();
  * echo '<pre>';
  * print_r($_POST);
  * echo '</pre>';
  * $logInfo = ob_get_contents();
  * ob_end_clean();
  * 
  * $file = fopen('logfile.txt', 'a');
  * fwrite($file, $logInfo);
  * fclose($file);
  */

  /* Check for mandatory fields */
  $r_fields = array('status', 'md5sig', 'merchant_id', 'pay_to_email', 'mb_amount', 'mb_transaction_id', 'currency', 'amount', 'transaction_id', 'pay_from_email', 'mb_currency');
  //$mdata = $db->first("SELECT demo, extra FROM gateways WHERE name = 'moneybookers'");
  $mdata = Core::getRow(Membership::gTable, "name", "skrill");

  foreach ($r_fields as $f)
      if (!isset($_POST[$f]))
          die();

  /* Check for MD5 signature */
  $md5 = strtoupper(md5($_POST['merchant_id'] . $_POST['transaction_id'] . strtoupper(md5($mdata['extra3'])) . $_POST['mb_amount'] . $_POST['mb_currency'] . $_POST['status']));
  if ($md5 != $_POST['md5sig'])
      die();

  if (intval($_POST['status']) == 2) {

      list($user_id, $sesid) = explode("_", $_POST['custom']);

      $cartrow = $digishop->getCartContent($sesid);
      $totalrow = $digishop->getCartTotal($sesid);
      $gross = $totalrow->total;

      $payer_email = $_POST['pay_from_email'];
      $receiver_email = $_POST['pay_to_email'];
      $mb_currency = $_POST['mb_currency'];
      $mb_gross = $_POST['amount'];
      $txn_id = $_POST['transaction_id'];

	  $v1 = compareFloatNumbers($mb_gross, $gross, "=");
      $getxn_id = Digishop::verifyTxnId($txn_id);

      if ($receiver_email == $mdata->extra && $v1 == true && $getxn_id == true) {
          if ($cartrow) {
              foreach ($cartrow as $k => $crow) {
				  $data = array(
					  'txn_id' => sanitize($txn_id),
					  'pid' => $crow->pid,
					  'uid' => intval($user->uid),
					  'downloads' => 0,
					  'file_date' => time(),
					  'token' => sha1(time() . $k),
					  'ip' => sanitize($_SERVER['REMOTE_ADDR']),
					  'created' => "NOW()",
					  'payer_email' => sanitize($payer_email),
					  'payer_status' => 'verified', 
					  'item_qty' => $crow->total,
					  'price' => $crow->total * $crow->price,
					  'unit_price' => $crow->price,
					  'mc_fee' => 0.00,
					  'currency' => sanitize($mb_currency),
					  'pp' => "Skrill",
					  'status' => 0,
					  'active' => 0);

                  $db->insert(Digishop::trTable, $data);
              }

              unset($crow);
          }

          /* == Notify User == */
		  require_once (BASEPATH . "lib/class_mailer.php");
		  $usr = Core::getRowById(Users::uTable, $user_id);

		  $sql = "SELECT t.*, p.id as pid, p.title" . Lang::$lang . " as ptitle, p.price, COUNT(t.pid) as total" 
		  . "\n FROM " . Digishop::trTable . " as t" 
		  . "\n LEFT JOIN " . Digishop::mTable . " as p ON p.id = t.pid" 
		  . "\n WHERE t.uid = '" . intval($user_id) . "' AND txn_id = '" . sanitize($txn_id) . "'" 
		  . "\n GROUP BY t.pid ORDER BY t.id DESC";
		  $transrow = $db->fetch_all($sql);
				  
		  $html = '
			<table width="100%" border="0" cellpadding="4" cellspacing="2">
			  <thead>
				<tr>
				  <td width="20"><strong>#</strong></td>
				  <td><strong>' . Lang::$word->_MOD_DS_NAME . '</strong></td>
				  <td><strong>' . Lang::$word->_MOD_DS_PRODPRICE . '</strong></td>
				  <td><strong>' . Lang::$word->_MOD_DS_QTY . '</strong></td>
				  <td><strong>' . Lang::$word->_MOD_DS_TOTPRICE . '</strong></td>
				</tr>
			</thead>';
          $i = 0;
		  foreach ($transrow as $ccrow) {
			  $i++;
			  $html .= '
				  <tr>
					<td style="border-bottom-width:1px; border-bottom-color:#bbb; border-bottom-style:dashed">' . $i . '.</td>
					<td style="border-bottom-width:1px; border-bottom-color:#bbb; border-bottom-style:dashed">' . sanitize($ccrow->ptitle, 30) . '</td>
					<td style="border-bottom-width:1px; border-bottom-color:#bbb; border-bottom-style:dashed">' . $core->formatMoney($ccrow->price) . '</td>
					<td align="center" style="border-bottom-width:1px; border-bottom-color:#bbb; border-bottom-style:dashed">' . $ccrow->item_qty . '</td>
					<td align="right" style="border-bottom-width:1px; border-bottom-color:#bbb; border-bottom-style:dashed">' . $core->formatMoney($ccrow->item_qty * $ccrow->price) . '</td>
				</tr>';
		  }
          unset($ccrow);
		  $html .= '
			<tr>
				<td colspan="4" align="right" valign="top"><strong style="color:#F00">' . Lang::$word->_MOD_DS_GTOTAL . ':</strong></td>
				<td align="right" valign="top"><strong style="color:#F00">' . $core->formatMoney($gross) . '</strong></td>
			  </tr>
			</tr>';

		  $body3 = str_replace(array(
			  '[NAME]',
			  '[ITEMS]',
			  '[DASH]',
			  '[SITE_NAME]',
			  '[URL]'), array(
			  $usr->username,
			  $html,
			  doUrl(false, Registry::get("Core")->account_page, "page"),
			  $core->site_name,
			  SITEURL), $digishop->template);
		  $newbody2 = cleanOut($body3);

		  $mailer2 = Mailer::sendMail();
		  $message2 = Swift_Message::newInstance()
					->setSubject(Lang::$word->_MOD_DS_R_SUBJECT . $core->site_name)
					->setTo(array($usr->email => $usr->name))
					->setFrom(array($core->site_email => $core->site_name))
					->setBody($newbody2, 'text/html');

		  $mailer2->send($message2);
		  $db->delete(Digishop::xTable, "user_id='" . $sesid . "'");
      }
  } else {
      /* == Failed Transaction= = */
	  require_once (BASEPATH . "lib/class_mailer.php");
	  $row = Core::getRowById(Content::eTable, 6);
	  $user = Core::getRowById(Users::uTable, $user_id);

	  $body = str_replace(array(
		  '[USERNAME]',
		  '[ITEM]',
		  '[STATUS]',
		  '[TOTAL]',
		  '[PP]',
		  '[IP]'), array(
		  $usr->username,
		  "Item(s) from DigiShop",
		  "Failed",
		  $core->formatMoney($gross),
		  "PayPal",
		  $_SERVER['REMOTE_ADDR']), $row->{'body' . Lang::$lang});

	  $newbody = cleanOut($body);

	  $mailer = Mailer::sendMail();
	  $message = Swift_Message::newInstance()
				->setSubject($row->{'subject' . Lang::$lang})
				->setTo(array($core->site_email => $core->site_name))
				->setFrom(array($core->site_email => $core->site_name))
				->setBody($newbody, 'text/html');

	  $mailer->send($message);
  }
?>
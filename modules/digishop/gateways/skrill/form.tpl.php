<?php
  /**
   * Moneybookers Form
   *
   * @package Veri Asist
   * @author veriasist.com
   * @copyright 2014
   * @version $Id: form.tpl.php, v4.00 2014-04-20 10:12:05 gewa Exp $
   */
  if (!defined("_VALID_PHP"))
      die('Direct access to this location is not allowed.');
?>
<div class="veriasist button">
  <form action="https://www.moneybookers.com/app/payment.pl" method="post" id="mb_form" name="mb_form">
    <input type="image" src="<?php echo SITEURL.'/modules/digishop/gateways/skrill/skrill_big.png';?>" name="submit" title="Pay With Skrill" alt="Pay With Skrill" onclick="document.mb_form.submit();"/>
    <input type="hidden" name="pay_to_email" value="<?php echo $grows->extra;?>" />
    <input type="hidden" name="recipient_description" value="<?php echo $core->company;?>" />
    <input type="hidden" name="transaction_id" value="<?php echo rand(10,99)?>A<?php echo rand(10,999)?>Z<?php echo rand(10,9999)?>" />
    <input type="hidden" name="language" value="EN" />
    <input type="hidden" name="logo_url" value="<?php echo ($core->logo) ? SITEURL.'/uploads/'.$core->logo : $core->company;?>" />
    <input type="hidden" name="return_url" value="<?php echo doUrl(false, $core->account_page, "page");?>" />
    <input type="hidden" name="cancel_url" value="<?php echo SITEURL;?>" />
    <input type="hidden" name="status_url" value="<?php echo SITEURL;?>/modules/digishop/gateways/skrill/ipn.php" />
    <input type="hidden" name="merchant_fields" value="session_id, item, custom" />
    <input type="hidden" name="item" value="Purchase from <?php echo $core->site_name; ?>" />
    <input type="hidden" name="session_id" value="<?php echo md5(time())?>" />
    <input type="hidden" name="custom" value="<?php echo $user->uid.'_'.$user->sesid;?>" />
    <input type="hidden" name="amount" value="<?php echo $totalrow->total;?>" />
    <input type="hidden" name="currency" value="<?php echo ($grows->extra2) ? $grows->extra2 : $core->currency;?>" />
    <?php $x = 0;?>
    <?php foreach ($cartrow as $crow):?>
    <?php $x++; ?>
    <input type="hidden" name="detail<?php echo $x;?>_text" value="<?php echo $crow->total.' x '.cleanOut($crow->ptitle);?>" />
    <input type="hidden" name="detail<?php echo $x;?>_description" value="Product Name:">
    <?php endforeach;?>
    <?php unset($crow);?>
  </form>
</div>
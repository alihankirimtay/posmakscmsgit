<?php
  /**
   * Digishop Main
   *
   * @package Veri Asist
   * @author veriasist.com
   * @copyright 2014
   * @version $Id: main.php, v4.00 2014-04-20 16:18:34 gewa Exp $
   */
  
  if (!defined("_VALID_PHP"))
      die('Direct access to this location is not allowed.');
	  
  require_once (MODPATH . "digishop/admin_class.php");
  $classname = 'Digishop';
  try {
	  if (!class_exists($classname)) {
		  throw new exception('Missing digishop/admin_class.php');
	  }
	  Registry::set('Digishop', new Digishop());
  }
  catch (exception $e) {
	  echo $e->getMessage();
  }

  $length = count($content->_url);
?>
<?php switch($length): case 3: ?>
<!-- Category Starts -->
<?php $digirow = Registry::get("Digishop")->renderProductByCategory($content->moduledata->mod->id, $content->moduledata->mod->slug)?>
<div class="clearfix bottom-space">
  <div id="lswitch" class="right floated veriasist icon buttons"><a data-type="list" class="veriasist<?php echo Registry::get("Digishop")->layoutSwitchList();?> button"><i class="list layout icon"></i></a><a data-type="grid" class="veriasist<?php echo Registry::get("Digishop")->layoutSwitchGrid();?> button"><i class="grid layout icon"></i></a></div>
  <div class="veriasist header"><?php echo Lang::$word->_MOD_DS_SUBTITLE9 . $content->moduledata->mod->{'name' . Lang::$lang};?></div>
</div>
<?php if(!$digirow):?>
<?php echo Filter::msgSingleAlert(Lang::$word->_MOD_DS_NOPRODUCTS);?>
<?php else:?>
<div id="digishop">
  <?php if(isset($_COOKIE['DIGIVIEW_VERIASIST']) && $_COOKIE['DIGIVIEW_VERIASIST'] == 'list' or Registry::get("Digishop")->layout == 0):?>
  <div id="digiwrap" class="clearfix digilist">
    <div class="columns">
      <?php foreach($digirow as $row):?>
      <div class="veriasist tabular segment">
        <div class="screen-25 tablet-40 phone-100">
          <aside>
            <div class="image-overlay"><img src="<?php echo SITEURL.'/'.Digishop::imagepath . $row->thumb;?>" alt="">
              <div class="overlay-fade"> <a href="<?php echo doUrl(false, $row->slug, "digishop-item");?>"><i class="icon-overlay icon url"></i></a> </div>
            </div>
          </aside>
        </div>
        <div class="screen-75 tablet-60 phone-100">
          <section>
            <div class="veriasist huge top right attached label"><?php echo $core->formatMoney($row->price);?></div>
            <h4><a class="inverted" href="<?php echo doUrl(false, $row->slug, "digishop-item");?>"><?php echo $row->{'title' . Lang::$lang};?></a></h4>
            <p><?php echo cleanSanitize($row->{'body' . Lang::$lang},400);?></p>
            <div class="veriasist divider"></div>
            <?php if($row->membership_id <> 0):?>
            <?php Registry::get("Digishop")->getPriceMembershipData($row->id, $row->token, $row->price, $row->membership_id)?>
            <?php else:?>
            <?php Registry::get("Digishop")->getPriceData($row->id, $row->token, $row->price);?>
            <?php endif;?>
            <?php if(Registry::get("Digishop")->like):?>
            <a data-id="<?php echo $row->id;?>" data-total="<?php echo $row->likes;?>" class="like push-right veriasist circular icon danger button"><i class="icon heart"></i> <small><?php echo $row->likes;?></small></a>
            <?php endif;?>
          </section>
        </div>
      </div>
      <?php endforeach;?>
    </div>
  </div>
  <?php else:?>
  <div id="digiwrap" class="clearfix digigrid">
    <?php foreach($digirow as $row):?>
    <div class="item">
      <section>
        <div class="image-overlay"> <img src="<?php echo SITEURL.'/'.Digishop::imagepath . $row->thumb;?>" alt="">
          <div class="overlay-fade"> <a href="<?php echo doUrl(false, $row->slug, "digishop-item");?>"><i class="icon-overlay icon url"></i></a> </div>
        </div>
      </section>
      <article>
        <h4><a class="inverted" href="<?php echo doUrl(false, $row->slug, "digishop-item");?>"><?php echo truncate($row->{'title' . Lang::$lang},20);?></a></h4>
        <p><span><?php echo $core->formatMoney($row->price);?></span></p>
        <?php if($row->membership_id <> 0):?>
        <?php Registry::get("Digishop")->getPriceMembershipData($row->id, $row->token, $row->price, $row->membership_id)?>
        <?php else:?>
        <?php Registry::get("Digishop")->getPriceData($row->id, $row->token, $row->price);?>
        <?php endif;?>
      </article>
    </div>
    <?php endforeach;?>
  </div>
  <?php endif;?>
</div>
<div id="pagination" class="content-center"><?php echo $pager->display_pages();?></div>
<?php endif;?>
<!-- Category Ends /-->
<?php break;?>


<?php case 2: ?>
<?php
$step = isset($_GET['step']) && is_numeric($_GET['step']) ? $_GET['step'] : 1;

// <!-- Checkout Starts -->
if(in_array(doUrlParts("digishop-checkout"), $content->_url)):

$cartrow = Registry::get("Digishop")->getCartContent();
$totalrow = Registry::get("Digishop")->getCartTotal();
$gaterows = $member->getGateways(true);


if($step == 1):
?>


  <ul class="nav nav-pills nav-justified">
    <li class="active"><a href="javascript:;">1. Cart summary</a></li>
    <li class="disabled"><a href="javascript:;">2. Shipping</a></li>
    <li class="disabled"><a href="javascript:;">3. Checkout & Payment</a></li>
  </ul>


  <div class="tab-content">
    <div class="tab-pane fade in active">
      <?php if(!$cartrow): // Sepet boş mu??>
        <?php Filter::msgSingleAlert(Lang::$word->_MOD_DS_CEMPTY);?>
      <?php else:?>
          <!-- <p class="veriasist notice message"><?php echo Lang::$word->_MOD_DS_CHECKOUT_M;?></p> -->
          <div class="veriasist fitted divider"></div>
          <div id="cko" class="veriasist fitted segment">
            <table class="veriasist table">
              <thead>
                <tr>
                  <th><i class="icon photo"></i></th>
                  <th><?php echo Lang::$word->_MOD_DS_NAME;?></th>
                  <th><?php echo Lang::$word->_MOD_DS_PRODPRICE;?></th>
                  <th><?php echo Lang::$word->_MOD_DS_QTY;?></th>
                  <th><?php echo Lang::$word->_MOD_DS_TOTPRICE;?></th>
                </tr>
              </thead>
              <tbody>
                <?php $i = 0;?>
                <?php foreach ($cartrow as $row): ?>
                  <?php $i++;;?>
                  <tr>
                    <td><a href="<?php echo doUrl(false, $row->slug, "digishop-item");?>"><img class="veriasist image" src="<?php echo SITEURL;?>/thumbmaker.php?src=<?php echo SITEURL.'/'.Digishop::imagepath . $row->thumb;?>&amp;h=50&amp;w=50&amp;s=1&amp;a=tl" alt=""/></a></td>
                    <td><a href="<?php echo doUrl(false, $row->slug, "digishop-item");?>"><?php echo $row->ptitle;?></a></td>
                    <td><?php echo $core->formatMoney($row->price);?></td>
                    <td><?php echo $row->total;?></td>
                    <td><?php echo $core->formatMoney($row->total * $row->price);?></td>
                  </tr>
                <?php endforeach;?>
                <?php unset($ccrow);?>
                <tr class="warning">
                  <td class="hide-phone">&nbsp;</td>
                  <td class="hide-phone">&nbsp;</td>
                  <td class="hide-phone">&nbsp;</td>
                  <td><strong><?php echo Lang::$word->_MOD_DS_GTOTAL;?>:</strong></td>
                  <td><strong><?php echo $core->formatMoney($totalrow->total);?></strong></td>
                </tr>
              </tbody>
            </table>
            <div class="content-right">
              <button type="button" id="paymentNext2" class="veriasist positive button">Next</button>
            </div>
          </div>

      <?php endif;?>
    </div>
  </div>
<?php elseif($step == 2):
$addresses = Registry::get("Digishop")->getUserAddresses();
$countries = Registry::get("Digishop")->getContries();
?>

  <ul class="nav nav-pills nav-justified">
    <li class="disabled"><a href="javascript:;">1. Cart summary</a></li>
    <li class="active"><a href="javascript:;">2. Shipping</a></li>
    <li class="disabled"><a href="javascript:;">3. Checkout & Payment</a></li>
  </ul>

  <div class="tab-pane fade in active">
    <div class="veriasist fitted divider"></div>
    <?php if(!$cartrow): ?>
      <?php Filter::msgSingleAlert(Lang::$word->_MOD_DS_CEMPTY);?>
    <?php elseif(!$user->logged_in): // giriş yapmış mı?
      if (isset($_POST['doLogin']))
      {
        $result = $user->login($_POST['username'], $_POST['password']);
        if ($result) redirect_to($_SERVER['REQUEST_URI']);
      }
    ?>
      <p class="veriasist notice message"><?php echo Lang::$word->_MOD_DS_CLOGIN_TO;?></p>

      <div id="signin" class="veriasist tab content">
        <div class="veriasist bottom attached secondary segment">
          <p><i class="information icon"></i> <?php echo Lang::$word->_UA_SUBTITLE2;?></p>
          <form method="post" id="login_form" name="login_form" class="veriasist form">
            <div class="columns">
              <div class="screen-30">
                <div class="field">
                  <label><?php echo Lang::$word->_UA_TITLE2;?> <i class="small icon asterisk"></i></label>
                </div>
              </div>
              <div class="screen-70">
                <div class="field">
                  <input name="username" placeholder="<?php echo Lang::$word->_UA_TITLE2;?>" type="text">
                </div>
              </div>
              <div class="screen-30">
                <div class="field">
                  <label><?php echo Lang::$word->_PASSWORD;?> <i class="small icon asterisk"></i></label>
                </div>
              </div>
              <div class="screen-70">
                <div class="field">
                  <input name="password" placeholder="<?php echo Lang::$word->_PASSWORD;?>" type="password">
                </div>
              </div>
              <div class="content-right">
                <button name="submit" type="submit" class="veriasist button"><?php echo Lang::$word->_UA_LOGINNOW;?></button>
              </div>
            </div>
            <input name="doLogin" type="hidden" value="1">
          </form>
          <?php print Filter::$showMsg;?> </div>
        </div>

    <?php else:?>
    <div class="veriasist form secondary segment">
      <form id="veriasist_form" name="veriasist_form" method="post">


          <div class="field">
          <label><?php echo Lang::$word->_CF_SELADDR;?></label>
            <select name="invAdress" id="invoiceAddresses">
              <option value="0">Add New Address</option>
            <?php foreach ($addresses as $address):?>
              <option value="<?php echo $address->id; ?>"><?php echo $address->name; ?></option>
            <?php endforeach; ?>
            </select>
          </div>


        <div class="two fields">
          <div class="field">
          <label>Country:</label>
            <select name="country" id="country">
              <option value="0">Seçiniz</option>
            <?php foreach ($countries as $contry):?>
              <option value="<?php echo $contry->id; ?>"><?php echo $contry->name; ?></option>
            <?php endforeach; ?>
            </select>
          </div>
          <div class="field">
            <label>City</label>
            <label class="input"><i class="icon-append icon asterisk"></i>
              <input type="text" placeholder="" name="city">
            </label>
          </div>
        </div>

        <div class="two fields">
          <div class="field">
            <label>State/Province/Region</label>
            <label class="input"><i class="icon-append icon asterisk"></i>
              <input type="text" placeholder="" name="state_province">
            </label>
          </div>
          <div class="field">
            <label>Zip/Postal Code</label>
            <label class="input"><i class="icon-append icon asterisk"></i>
              <input type="text" placeholder="" name="zip_post">
            </label>
          </div>
        </div>

        <div class="field">
          <label>Address</label>
          <label class="textarea"><i class="icon-append icon asterisk"></i>
            <textarea placeholder="" name="address"></textarea>
          </label>
        </div>


        <div class="field">
          <label>Same Billing Address?</label>
          <div class="inline-group">
            <label class="checkbox">
              <input name="sameaddress" id="sameaddress" type="checkbox" value="1" checked="checked">
              <i></i>Yes</label>
            </div>
          </div>

          <div id="secondAddress" style="display: none;">
          <!-- Second Address -->
            <div class="field">
              <label><?php echo Lang::$word->_CF_SELADDR;?></label>
              <select name="invAdress1" id="invoiceAddresses1">
                <option value="0">Add New Address</option>
                <?php foreach ($addresses as $address):?>
                  <option value="<?php echo $address->id; ?>"><?php echo $address->name; ?></option>
                <?php endforeach; ?>
              </select>
            </div>


            <div class="two fields">
              <div class="field">
                <label>Country:</label>
                <select name="country1" id="country1">
                  <option value="0">Seçiniz</option>
                  <?php foreach ($countries as $contry):?>
                    <option value="<?php echo $contry->id; ?>"><?php echo $contry->name; ?></option>
                  <?php endforeach; ?>
                </select>
              </div>
              <div class="field">
                <label>City</label>
                <label class="input"><i class="icon-append icon asterisk"></i>
                  <input type="text" placeholder="" name="city1">
                </label>
              </div>
            </div>

            <div class="two fields">
              <div class="field">
                <label>State/Province/Region</label>
                <label class="input"><i class="icon-append icon asterisk"></i>
                  <input type="text" placeholder="" name="state_province1">
                </label>
              </div>
              <div class="field">
                <label>Zip/Postal Code</label>
                <label class="input"><i class="icon-append icon asterisk"></i>
                  <input type="text" placeholder="" name="zip_post1">
                </label>
              </div>
            </div>

            <div class="field">
              <label>Address</label>
              <label class="textarea"><i class="icon-append icon asterisk"></i>
                <textarea placeholder="" name="address1"></textarea>
              </label>
            </div>
            <!-- Second Address END-->
          </div>

          <div class="field">
            <label>Note:(Optionlal)</label>
            <label class="textarea">
              <textarea placeholder="" name="note"></textarea>
            </label>
          </div>

          <input type="hidden" name="paymentNext3" value="1">
        <!-- <button  data-url="/ajax/sendmail.php"    type="button" name="dosubmit" class="veriasist button"><?php echo Lang::$word->_CF_SEND;?></button>-->
        <div class="content-right">
          <button type="button" id="paymentNext3" class="veriasist positive button">Next</button>
        </div>
      </form>
      <br>
      <div id="msgholder"></div>
    </div>
    <?php endif; ?>
  </div>


<?php elseif($step == 3): ?>
  <?php if(!$cartrow): ?>
      <?php redirect_to( SITEURL . '/digishop/checkout/' ); ?>
  <?php else: ?>

    <?php foreach ($cartrow as $cart)
    { /// adres seçilmemişse adres seçim ekranına git.
      if($cart->adr1 == 0 || $cart->adr2 == 0)
        redirect_to( SITEURL . '/digishop/checkout/?step=2' );
    }
    ?>
  <ul class="nav nav-pills nav-justified">
    <li class="disabled"><a href="javascript:;">1. Cart summary</a></li>
    <li class="disabled"><a href="javascript:;">2. Shipping</a></li>
    <li class="active"><a href="javascript:;">3. Checkout & Payment</a></li>
  </ul>


  <div class="tab-pane fade in active">
    <h3 class="veriasist double header"><span><?php echo Lang::$word->_MOD_DS_SELPAY;?></span></h3>
    <div id="show-result" class="content-center">
      <div class="veriasist buttons">
        <?php
        if ($cartrow and $gaterows):
          $content = '';
        foreach ($gaterows as $grows):
          $form_url = MODPATHF . "digishop/gateways/" . $grows->dir . "/form.tpl.php";
        ob_start();
        include ($form_url);
        $content .= ob_get_contents();
        ob_end_clean();
        endforeach;
        print $content;
        endif;
        ?>
      </div>
    </div>
  </div>
  <?php endif; ?>
<?php endif; ?>













   <!--  <?php if(!$cartrow):?>
        <?php Filter::msgSingleAlert(Lang::$word->_MOD_DS_CEMPTY);?>
    <?php else:?>
        <?php if(!$user->logged_in):?>
        <p class="veriasist notice message"><?php echo Lang::$word->_MOD_DS_CLOGIN_TO;?></p>
        <?php else:?>
        <p class="veriasist notice message"><?php echo Lang::$word->_MOD_DS_CHECKOUT_M;?></p>
        <div id="cko" class="veriasist fitted segment">
          <table class="veriasist table">
            <thead>
              <tr>
                <th><i class="icon photo"></i></th>
                <th><?php echo Lang::$word->_MOD_DS_NAME;?></th>
                <th><?php echo Lang::$word->_MOD_DS_PRODPRICE;?></th>
                <th><?php echo Lang::$word->_MOD_DS_QTY;?></th>
                <th><?php echo Lang::$word->_MOD_DS_TOTPRICE;?></th>
              </tr>
            </thead>
            <tbody>
              <?php $i = 0;?>
              <?php foreach ($cartrow as $row): ?>
              <?php $i++;;?>
              <tr>
                <td><a href="<?php echo doUrl(false, $row->slug, "digishop-item");?>"><img class="veriasist image" src="<?php echo SITEURL;?>/thumbmaker.php?src=<?php echo SITEURL.'/'.Digishop::imagepath . $row->thumb;?>&amp;h=50&amp;w=50&amp;s=1&amp;a=tl" alt=""/></a></td>
                <td><a href="<?php echo doUrl(false, $row->slug, "digishop-item");?>"><?php echo $row->ptitle;?></a></td>
                <td><?php echo $core->formatMoney($row->price);?></td>
                <td><?php echo $row->total;?></td>
                <td><?php echo $core->formatMoney($row->total * $row->price);?></td>
              </tr>
              <?php endforeach;?>
              <?php unset($ccrow);?>
              <tr class="warning">
                <td class="hide-phone">&nbsp;</td>
                <td class="hide-phone">&nbsp;</td>
                <td class="hide-phone">&nbsp;</td>
                <td><strong><?php echo Lang::$word->_MOD_DS_GTOTAL;?>:</strong></td>
                <td><strong><?php echo $core->formatMoney($totalrow->total);?></strong></td>
              </tr>
            </tbody>
          </table>
        </div>
        <h1 class="veriasist double header"><span><?php echo Lang::$word->_MOD_DS_SELPAY;?></span></h1>
        <div id="show-result" class="content-center">
          <div class="veriasist buttons">
        <?php
          if ($cartrow and $gaterows):
              $content = '';
              foreach ($gaterows as $grows):
                  $form_url = MODPATHF . "digishop/gateways/" . $grows->dir . "/form.tpl.php";
                  ob_start();
                  include ($form_url);
                  $content .= ob_get_contents();
                  ob_end_clean();
              endforeach;
              print $content;
          endif;
        ?>
          </div>
        </div>
        <?php endif;?>
    <?php endif;?> -->
<!-- Checkout Ends /-->


<?php else:?>
<!-- Single Item Starts -->
<?php $row = $content->moduledata->mod;?>
<div id="digishop">
  <div class="columns gutters">
    <div class="screen-40 tablet-30 phone-100">
      <?php if($row->gallery):?>
      <?php include(MODPATHF . "/digishop/gallery.tpl.php");?>
      <?php else:?>
      <div class="image-overlay"> <img class="veriasist image" src="<?php echo SITEURL.'/'.Digishop::imagepath . $row->thumb;?>" alt="">
        <div class="overlay-hslide"> <a title="<?php echo $row->{'title' . Lang::$lang};?>" href="<?php echo SITEURL.'/'.Digishop::imagepath . $row->thumb;?>" class="lightbox"><i class="icon-overlay icon search"></i></a> </div>
      </div>
      <?php endif;?>
    </div>
    <div class="screen-60 tablet-70 phone-100">
      <?php if(Registry::get("Digishop")->like):?>
      <a data-id="<?php echo $row->id;?>" data-total="<?php echo $row->likes;?>" class="like push-right veriasist circular icon danger button"><i class="icon heart"></i> <small><?php echo $row->likes;?></small></a>
      <?php endif;?>
      <h3><?php echo $row->{'title' . Lang::$lang};?></h3>
      <div class="veriasist huge info label"><?php echo $core->formatMoney($row->price);?></div>
      <div class="veriasist divider"></div>
      <?php echo cleanOut($row->{'body' . Lang::$lang});?> 
      <div class="veriasist divider"></div>
      <?php if($row->membership_id <> 0):?>
      <?php Registry::get("Digishop")->getPriceMembershipData($row->id, $row->token, $row->price, $row->membership_id)?>
      <?php else:?>
      <?php Registry::get("Digishop")->getPriceData($row->id, $row->token, $row->price);?>
      <?php endif;?>
      </div>
  </div>
</div>
<!-- Single Item Ends /-->
<?php endif;?>







<?php break;?>
<?php default: ?>
<!-- Landing Page -->
<?php $digirow = Registry::get("Digishop")->renderProducts();?>
<div class="clearfix bottom-space">
  <div id="lswitch" class="right floated veriasist icon buttons"><a data-type="list" class="veriasist<?php echo Registry::get("Digishop")->layoutSwitchList();?> button"><i class="list layout icon"></i></a><a data-type="grid" class="veriasist<?php echo Registry::get("Digishop")->layoutSwitchGrid();?> button"><i class="grid layout icon"></i></a></div>
  <div class="veriasist header"><?php echo Lang::$word->_MOD_DS_SUBTITLE1;?></div>
</div>
<?php if(!$digirow):?>
<?php echo Filter::msgSingleAlert(Lang::$word->_MOD_DS_NOPRODUCTS);?>
<?php else:?>
<div id="digishop">
  <?php if(isset($_COOKIE['DIGIVIEW_VERIASIST']) && $_COOKIE['DIGIVIEW_VERIASIST'] == 'list' or Registry::get("Digishop")->layout == 0):?>
  <div id="digiwrap" class="clearfix digilist">
    <div class="columns">
      <?php foreach($digirow as $row):?>
      <div class="veriasist tabular segment">
        <div class="screen-25 tablet-40 phone-100">
          <aside>
            <div class="image-overlay"><img src="<?php echo SITEURL.'/'.Digishop::imagepath . $row->thumb;?>" alt="">
              <div class="overlay-fade"> <a href="<?php echo doUrl(false, $row->slug, "digishop-item");?>"><i class="icon-overlay icon url"></i></a> </div>
            </div>
          </aside>
        </div>
        <div class="screen-75 tablet-60 phone-100">
          <section>
            <div class="veriasist huge top right attached label"><?php echo $core->formatMoney($row->price);?></div>
            <h4><a class="inverted" href="<?php echo doUrl(false, $row->slug, "digishop-item");?>"><?php echo $row->{'title' . Lang::$lang};?></a></h4>
            <p><?php echo cleanSanitize($row->{'body' . Lang::$lang},400);?></p>
            <div class="veriasist divider"></div>
            <?php if($row->membership_id <> 0):?>
            <?php Registry::get("Digishop")->getPriceMembershipData($row->id, $row->token, $row->price, $row->membership_id)?>
            <?php else:?>
            <?php Registry::get("Digishop")->getPriceData($row->id, $row->token, $row->price);?>
            <?php endif;?>
            <?php if(Registry::get("Digishop")->like):?>
            <a data-id="<?php echo $row->id;?>" data-total="<?php echo $row->likes;?>" class="like push-right veriasist circular icon danger button"><i class="icon heart"></i> <small><?php echo $row->likes;?></small></a>
            <?php endif;?>
          </section>
        </div>
      </div>
      <?php endforeach;?>
    </div>
  </div>
  <?php else:?>
  <div id="digiwrap" class="clearfix digigrid">
    <?php foreach($digirow as $row):?>
    <div class="item">
      <section>
        <div class="image-overlay"> <img src="<?php echo SITEURL.'/'.Digishop::imagepath . $row->thumb;?>" alt="">
          <div class="overlay-fade"> <a href="<?php echo doUrl(false, $row->slug, "digishop-item");?>"><i class="icon-overlay icon url"></i></a> </div>
        </div>
      </section>
      <article>
        <h4><a class="inverted" href="<?php echo doUrl(false, $row->slug, "digishop-item");?>"><?php echo truncate($row->{'title' . Lang::$lang},20);?></a></h4>
        <p><span><?php echo $core->formatMoney($row->price);?></span></p>
        <?php if($row->membership_id <> 0):?>
        <?php Registry::get("Digishop")->getPriceMembershipData($row->id, $row->token, $row->price, $row->membership_id)?>
        <?php else:?>
        <?php Registry::get("Digishop")->getPriceData($row->id, $row->token, $row->price);?>
        <?php endif;?>
      </article>
    </div>
    <?php endforeach;?>
  </div>
  <?php endif;?>
</div>
<div id="pagination" class="content-center"><?php echo $pager->display_pages();?></div>
<?php endif;?>
<!-- Landing Page /-->
<?php break;?>
<?php endswitch;?>
<?php if(isset($_GET['msg'])):?>
<?php Digishop::downloadErrors();?>
<?php endif;?>
<script src="<?php echo MODURL;?>digishop/digishop.js"></script> 
<script type="text/javascript">
// <![CDATA[
$(document).ready(function () {
<?php if(isset($_GET['msg'])):?>
	var text = $("#showerror").html();
	new Messi(text, {
		title: "Error",
		modal: true
	});
<?php endif;?>
	$('#digiwrap.digigrid').waitForImages(function () {
		$('#digiwrap.digigrid').Grid({
			inner: 0,
			outer: 0,
			cols: <?php echo round(1200 / Registry::get("Digishop")->cols);?>
		});
	});
		
    $.DigiShop({
            url: "/modules/digishop/controller.php",
            cat: <?php echo ($length == 3) ? $content->moduledata->mod->id : 0;?>
    });
});
// ]]>
</script> 
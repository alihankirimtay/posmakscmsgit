<?php
  /**
   * Account View
   *
   * @package Veri Asist
   * @author veriasist.com
   * @copyright 2014
   * @version $Id: account.tpl.php, v4.00 2014-04-20 16:18:34 gewa Exp $
   */
  
  if (!defined("_VALID_PHP"))
      die('Direct access to this location is not allowed.');
	  
  require_once (MODPATH . "digishop/admin_class.php");

  Registry::set('Digishop', new Digishop());
  $digitems = Registry::get("Digishop")->getUserTransactions()
?>
<div class="veriasist secondary segment">
  <h3><?php echo Lang::$word->_MOD_DS_SUBTITLE10;?></h3>
  <p><i class="information icon"></i> <?php echo Lang::$word->_MOD_DS_INFO9;?></p>
  <table class="veriasist sortable table">
    <thead>
      <tr>
        <th><i class="icon photo"></i></th>
        <th data-sort="string"><?php echo Lang::$word->_MOD_DS_NAME;?></th>
        <th data-sort="int"><?php echo Lang::$word->_TR_AMOUNT;?></th>
        <th data-sort="int"><?php echo Lang::$word->_TR_PAYDATE;?></th>
        <th data-sort="string"><?php echo Lang::$word->_TR_PROCESSOR;?></th>
        <th class="disabled"><?php echo Lang::$word->_DOWNLOAD;?></th>
      </tr>
    </thead>
    <tbody>
      <?php if(!$digitems):?>
      <tr>
        <td colspan="6"><?php echo Filter::msgSingleAlert(Lang::$word->_MOD_DS_NOTRANS);?></td>
      </tr>
      <?php else:?>
      <?php foreach ($digitems as $drow):?>
      <tr>
        <td><a href="<?php echo doUrl(false, $drow->slug, "digishop-item");?>"><img class="veriasist image" src="<?php echo SITEURL;?>/thumbmaker.php?src=<?php echo SITEURL.'/'.Digishop::imagepath . $drow->thumb;?>&amp;h=50&amp;w=50&amp;s=1&amp;a=tl" alt=""/></a></td>
        <td><a href="<?php echo doUrl(false, $drow->slug, "digishop-item");?>"><?php echo $drow->ptitle;?></a></td>
        <td data-sort-value="<?php echo $drow->price;?>"><?php echo $core->formatMoney($drow->price);?></td>
        <td data-sort-value="<?php echo strtotime($drow->created);?>"><?php echo Filter::dodate("long_date", $drow->created);?></td>
        <td><span class="veriasist positive label"><?php echo $drow->pp;?></span></td>
        <td><a href="<?php echo MODURL;?>digishop/download.php?fileid=<?php echo $drow->token;?>"><i class="rounded inverted download disk icon"></i></a></td>
      </tr>
      <?php endforeach;?>
      <?php unset($drow);?>
      <?php endif;?>
    </tbody>
  </table>
</div>
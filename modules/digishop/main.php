<?php
  /**
   * Digishop Main
   *
   * @package Veri Asist
   * @author veriasist.com
   * @copyright 2014
   * @version $Id: main.php, v4.00 2014-04-20 16:18:34 gewa Exp $
   */
  
  if (!defined("_VALID_PHP"))
      die('Direct access to this location is not allowed.');
	  
  require_once (MODPATH . "digishop/admin_class.php");
  $classname = 'Digishop';
  try {
	  if (!class_exists($classname)) {
		  throw new exception('Missing digishop/admin_class.php');
	  }
	  Registry::set('Digishop', new Digishop());
  }
  catch (exception $e) {
	  echo $e->getMessage();
  }

  $length = count($content->_url);
?>




<?php switch($length): case 3: ?>
<!-- Category Starts -->



<?php $digirow = Registry::get("Digishop")->renderProductByCategory($content->moduledata->mod->id, $content->moduledata->mod->slug)?>




<section id="theme-panel" class="panel-close">
  <a href="javascript:;" class="panel-btn" data-toggle="collapse" data-target="#cartlist" aria-expanded="false"><i class="fa fa-shopping-cart " style="color:#3EA8F4;" id="blink"></i></a>
</section>

<div id="cartlist" class="panel-collapse collapse" aria-expanded="false">
  <div class="panel-body">

     <div class="container">
      <div class="row">
        <div class="table-responsive">
          <table class="table table-hover table-condensed no-margin">

           <thead>
              <tr>
                <th><?php echo Lang::$word->_MOD_DS_NAME; ?></th>
                <th class="text-center"><?php echo Lang::$word->_MOD_DS_QNTTY; ?></th>
                <th class="text-center"><?php echo Lang::$word->_MOD_DS_PRICE; ?></th>
                <th class="text-center"><?php echo Lang::$word->_MOD_DS_TOTPRICE; ?></th>
                <th> </th>
              </tr>
            </thead>
            <tbody>
              <?php echo Registry::get("Digishop")->loadcart(); ?>
            </tbody>
          </table>
        </div>
      </div>
    </div>

  </div>
</div>


<div class="clearfix bottom-space">
  <div id="lswitch" class="right floated veriasist icon buttons"><a data-type="list" class="veriasist<?php echo Registry::get("Digishop")->layoutSwitchList();?> button"><i class="list layout icon"></i></a><a data-type="grid" class="veriasist<?php echo Registry::get("Digishop")->layoutSwitchGrid();?> button"><i class="grid layout icon"></i></a></div>
  
  <div class="container">
  <h1 style="text-align:center; padding:30px;"><?php echo Lang::$word->_MOD_DS_SUBTITLE9 . $content->moduledata->mod->{'name' . Lang::$lang};?></h1>
  </div>
</div>
<?php if(!$digirow):?>
<?php echo Filter::msgSingleAlert(Lang::$word->_MOD_DS_NOPRODUCTS);?>
<?php else:?>
<div id="digishop">
  <?php if(isset($_COOKIE['DIGIVIEW_VERIASIST']) && $_COOKIE['DIGIVIEW_VERIASIST'] == 'list' or Registry::get("Digishop")->layout == 0):?>
  <div id="digiwrap" class="clearfix digilist">
    <div class="columns">
      <?php foreach($digirow as $row):?>
      <div class="veriasist tabular segment">
        <div class="screen-25 tablet-40 phone-100">
          <aside>
            <div class="image-overlay"><img src="<?php echo SITEURL.'/'.Digishop::imagepath . $row->thumb;?>" alt="">
              <div class="overlay-fade"> <a href="<?php echo doUrl(false, $row->slug, "digishop-item");?>"><i class="icon-overlay icon url"></i></a> </div>
            </div>
          </aside>
        </div>
        <div class="screen-75 tablet-60 phone-100">
          <section>
            <div class="veriasist huge top right attached label"><?php echo $core->formatMoney($row->price);?></div>
            <h4><a class="inverted" href="<?php echo doUrl(false, $row->slug, "digishop-item");?>"><?php echo $row->{'title' . Lang::$lang};?></a></h4>
            <p><?php echo cleanSanitize($row->{'body' . Lang::$lang},400);?></p>
            <div class="veriasist divider"></div>
            <?php if($row->membership_id <> 0):?>
            <?php Registry::get("Digishop")->getPriceMembershipData($row->id, $row->token, $row->price, $row->membership_id)?>
            <?php else:?>
            <?php Registry::get("Digishop")->getPriceData($row->id, $row->token, $row->price);?>
            <?php endif;?>
            <?php if(Registry::get("Digishop")->like):?>
            <a data-id="<?php echo $row->id;?>" data-total="<?php echo $row->likes;?>" class="like push-right veriasist circular icon danger button"><i class="icon heart"></i> <small><?php echo $row->likes;?></small></a>
            <?php endif;?>
          </section>
        </div>
      </div>
      <?php endforeach;?>
    </div>
  </div>
  <?php else:?>
  <div id="digiwrap" class="clearfix digigrid">
    <?php foreach($digirow as $row):?>
<section class="item-bg" style="background-image: url(<?php echo SITEURL.'/'.Digishop::imagepath . $row->thumbbg;?>);" id="about"> 
  <div class="container">
    <div class="row">

      <div class="col-md-9">
        <h2 style="visibility: visible;" class="content-title white wow fadeInUp animated"><?php echo $row->{'title' . Lang::$lang};?></h2>          
        <?php echo str_replace('<p>', '<p style="visibility: visible;" class="grey wow fadeInUp animated">', htmlspecialchars_decode($row->{'body' . Lang::$lang}));?>
        
        <hr>

        <div class="col-md-12">
          <div class="col-md-4">
            <div class="input-group">
              <span class="input-group-btn">
                <button type="button" class="btn btn-default" data-value="-1" data-target="#spinner<?php echo $key; ?>" data-toggle="spinner">
                  <span class="glyphicon glyphicon-minus"></span>
                </button>
              </span>

              <input type="text" data-ride="spinner" id="spinner<?php echo $key; ?>" class="form-control input-number" value="1" data-min="1" data-max="100">

              <span class="input-group-btn">
                <button type="button" class="btn btn-default" data-value="1" data-target="#spinner<?php echo $key; ?>" data-toggle="spinner" data-on="mousehold">
                  <span class="glyphicon glyphicon-plus"></span>
                </button>
              </span>
            </div>
          </div>
          

          <div class="col-md-8">
            <button class="btn btn-success addtocart" data-id="<?php echo $row->id; ?>" data-quantity="spinner<?php echo $key; ?>"><?php echo $core->formatMoney($row->price) . ' - ' . Lang::$word->_MOD_DS_ADD_TO;?></button>
          </div>
        </div>

      </div>

      <div class="col-md-3 fadeInUp animated">
        <img  src="<?php echo SITEURL.'/'.Digishop::imagepath . $row->thumb;?>" style="max-width: 250px" alt="">
      </div>

    </div>
  </div>
</section>
    <?php endforeach;?>
  </div>
  <?php endif;?>
</div>
<div id="pagination" class="content-center"><?php echo $pager->display_pages();?></div>
<?php endif;?>
<!-- Category Ends /-->
<?php break;?>





<?php case 2:

$digirow = Registry::get("Digishop")->renderProducts();
$cartrow = Registry::get("Digishop")->getCartContent();
$totalrow = Registry::get("Digishop")->getCartTotal();
?>

<!-- Checkout Starts -->
<?php if(in_array(doUrlParts("digishop-checkout"), $content->_url)) ?>
<!-- Step1 -->
<?php if(!$cartrow): ?>
    <?php redirect_to(SITEURL . '/digishop/');?>
<?php elseif(isset($_GET['cartsummary'])):?>

    <?php echo Registry::get('Digishop')->firstStep(); ?>

<?php elseif(isset($_GET['shipping'])): ?>

  <?php if(!$user->logged_in):?>
    <?php
      if (isset($_POST['doLogin'])) {
        $result = $user->login($_POST['username'], $_POST['password']);
        if ($result) redirect_to($_SERVER['REQUEST_URI']);
      } ?>
      <?php echo Registry::get('Digishop')->secondStep(true); ?>
      <?php require_once (BASEPATH.'theme/kodar/login.tpl.php'); ?>
  <?php else: ?>
    <?php echo Registry::get('Digishop')->secondStep(); ?>
  <?php endif; ?>

<?php elseif(isset($_GET['payment'])):?>
  <?php if(!$user->logged_in):?>
    <?php redirect_to(SITEURL . '/digishop/');?>
  <?php else: ?>

    <?php $gaterows = $member->getGateways(true); ?>
    <?php //echo Registry::get('Digishop')->thirdStep(); ?>
    

    <ul class="nav nav-pills nav-justified">
      <li class="disabled"><a href="javascript:;">1. <?php echo Lang::$word->_MOD_CART_SUM;?></a></li>
      <li class="disabled"><a href="javascript:;">2. <?php echo Lang::$word->_MOD_CART_SHPP;?></a></li>
      <li class="active"><a href="javascript:;">3. <?php echo Lang::$word->_MOD_CHCK_PAY;?></a></li>
    </ul>


    <div class="container minheight500"> 
      <div style="margin-top:50px">
        <div class="panel panel-info">
          <div class="panel-body text-center" >

            <div class="row">

              <div class="col-md-12">
                <div class="table-responsive">
                  <table class="table table-hover table-condensed no-margin">
                    <thead>
                      <tr>
                        <th><?php echo Lang::$word->_MOD_DS_NAME; ?></th>
                        <th class="text-center"><?php echo Lang::$word->_MOD_DS_QNTTY; ?></th>
                        <th class="text-center"><?php echo Lang::$word->_MOD_DS_PRICE; ?></th>
                        <th class="text-center"><?php echo Lang::$word->_MOD_DS_TOTPRICE; ?></th>
                        <th> </th>
                      </tr>
                    </thead>
                    <tbody>
                      <?php echo Registry::get('Digishop')->loadcartsummary(); ?>
                    </tbody>
                  </table>
                </div>
              </div>

              <div class="col-md-12">
                <header><h4><?php echo Lang::$word->_MOD_DS_SELPAY;?></h4></header>
                <div id="show-result"> </div>
                <div class="row" id="cko">

                  <?php if ($cartrow and $gaterows): ?>
                    <?php $content1 = ''; ?>
                    <?php foreach ($gaterows as $grows): ?>

                      <?php $form_url = MODPATHF . "digishop/gateways/" . $grows->dir . "/form.tpl.php"; ?>
                      <?php ob_start(); ?>

                        <?php include ($form_url); ?>
                        
                      <?php $content1 .= ob_get_contents(); ?>
                      <?php ob_end_clean(); ?>


                    <?php endforeach; ?>
                    <?php print $content1; ?>
                  <?php endif; ?>

                </div>
              </div>
            </div>

          </div>
        </div>
      </div>
    </div>



  <?php endif; ?>
<?php endif; ?>
<!-- Steps END -->
<!-- Checkout Ends /-->


<!-- Single Item Starts -->
<?php $row = $content->moduledata->mod;?>
<div id="digishop">
  <div class="columns gutters">
    <div class="screen-40 tablet-30 phone-100">
      <?php if($row->gallery):?>
      <?php include(MODPATHF . "/digishop/gallery.tpl.php");?>
      <?php else:?>
      <div class="image-overlay"> <img class="veriasist image" src="<?php echo SITEURL.'/'.Digishop::imagepath . $row->thumb;?>" alt="">
        <div class="overlay-hslide"> <a title="<?php echo $row->{'title' . Lang::$lang};?>" href="<?php echo SITEURL.'/'.Digishop::imagepath . $row->thumb;?>" class="lightbox"><i class="icon-overlay icon search"></i></a> </div>
      </div>
      <?php endif;?>
    </div>
    <div class="screen-60 tablet-70 phone-100">
      <?php if(Registry::get("Digishop")->like):?>
      <a data-id="<?php echo $row->id;?>" data-total="<?php echo $row->likes;?>" class="like push-right veriasist circular icon danger button"><i class="icon heart"></i> <small><?php echo $row->likes;?></small></a>
      <?php endif;?>
      <h3><?php echo $row->{'title' . Lang::$lang};?></h3>
      <div class="veriasist huge info label"><?php echo $core->formatMoney($row->price);?></div>
      <div class="veriasist divider"></div>
      <?php echo cleanOut($row->{'body' . Lang::$lang});?> 
      <div class="veriasist divider"></div>
      <?php if($row->membership_id <> 0):?>
      <?php Registry::get("Digishop")->getPriceMembershipData($row->id, $row->token, $row->price, $row->membership_id)?>
      <?php else:?>
      <?php Registry::get("Digishop")->getPriceData($row->id, $row->token, $row->price);?>
      <?php endif;?>
      </div>
  </div>
</div>
<!-- Single Item Ends /-->

<?php break;?>
<?php default: ?>
<!-- Landing Page -->





<?php
$digirow = Registry::get("Digishop")->renderProducts();
$cartrow = Registry::get("Digishop")->getCartContent();
$totalrow = Registry::get("Digishop")->getCartTotal();

if(!$digirow):
  echo Filter::msgSingleAlert(Lang::$word->_MOD_DS_NOPRODUCTS);
else: ?>
<?php $cartModal = 1; ?>

<section id="theme-panel" class="panel-close">
  <a href="javascript:;" class="panel-btn" data-toggle="collapse" data-target="#cartlist" aria-expanded="false"><i class="fa fa-shopping-cart " style="color:#3EA8F4;" id="blink"></i></a>
</section>

<div id="cartlist" class="panel-collapse collapse" aria-expanded="false">
  <div class="panel-body">

     <div class="container">
      <div class="row">
        <div class="table-responsive">
          <table class="table table-hover table-condensed no-margin">

           <thead>
              <tr>
                <th><?php echo Lang::$word->_MOD_DS_NAME; ?></th>
                <th class="text-center"><?php echo Lang::$word->_MOD_DS_QNTTY; ?></th>
                <th class="text-center"><?php echo Lang::$word->_MOD_DS_PRICE; ?></th>
                <th class="text-center"><?php echo Lang::$word->_MOD_DS_TOTPRICE; ?></th>
                <th> </th>
              </tr>
            </thead>
            <tbody>
              <?php echo Registry::get("Digishop")->loadcart(); ?>
            </tbody>
          </table>
        </div>
      </div>
    </div>

  </div>
</div>


<?php  foreach ($digirow as $key => $row): ?>

<section class="item-bg" style="background-image: url(<?php echo SITEURL.'/'.Digishop::imagepath . $row->thumbbg;?>);" id="about"> 
  <div class="container">
    <div class="row">

      <div class="col-md-9">
        <h2 style="visibility: visible;" class="content-title white wow fadeInUp animated"><?php echo $row->{'title' . Lang::$lang};?></h2>          
        <?php echo str_replace('<p>', '<p style="visibility: visible;" class="grey wow fadeInUp animated">', htmlspecialchars_decode($row->{'body' . Lang::$lang}));?>
        
<div class="col-md-12" style="padding:0px;">
<div class="col-md-5" style="padding:0px;">
		  <h1 class="content-title fiyat"><?php echo $core->formatMoney($row->price)?></h1>
		  </div>	
<div class="col-md-7" style="padding:0px;">
		   <button class="satin-al addtocart" data-id="<?php echo $row->id; ?>" data-quantity="spinner<?php echo $key; ?>"><?php echo Lang::$word->_MOD_DS_ADD_TO;?></button>
</div>	  
</div>	
	   
        <div class="col-md-12">
          <div class="col-md-4">
            <div class="input-group" style="display:none;">
              <span class="input-group-btn">
                <button type="button" class="btn btn-default" data-value="-1" data-target="#spinner<?php echo $key; ?>" data-toggle="spinner">
                  <span class="glyphicon glyphicon-minus"></span>
                </button>
              </span>

              <input type="text" data-ride="spinner" id="spinner<?php echo $key; ?>" class="form-control input-number" value="1" data-min="1" data-max="100">

              <span class="input-group-btn">
                <button type="button" class="btn btn-default" data-value="1" data-target="#spinner<?php echo $key; ?>" data-toggle="spinner" data-on="mousehold">
                  <span class="glyphicon glyphicon-plus"></span>
                </button>
              </span>
            </div>
          </div>
          


          <div class="col-md-8">
           
          </div>

        </div>
				  <div style="margin-top: 150px;">
		   <h2><a class="detay-bilgi" href="<?php echo doUrl(false, $row->slug, "digishop-item");?>"><?php echo truncate($row->{'title' . Lang::$lang},20);?> Hakkında daha fazla bilgi</a></h2>
		  </div>

      </div>

      <div class="col-md-3 fadeInUp animated">
        <img  src="<?php echo SITEURL.'/'.Digishop::imagepath . $row->thumb;?>" style="max-width: 450px" alt="">
      </div>

    </div>
  </div>
</section>


<?php endforeach;
endif; ?>


<?php break;?>
<?php endswitch;?>



<?php if(isset($cartModal)): ?>
  <div class="modal fade" id="cartModal" tabindex="-1" role="dialog" aria-labelledby="cartModal" aria-hidden="true" style="margin-top: 120px;">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
          <h4 class="modal-title">SEPETİNİZ</h4>
        </div>
        <div class="modal-body well">
          <div style="padding-right: 15px;
padding-left: 15px;
margin-right: auto;
margin-left: auto;">
            <div class="row">
              <div class="table-responsive">
                <table class="table table-hover table-condensed no-margin">

                 <thead>
                  <tr>
                    <th><?php echo Lang::$word->_MOD_DS_NAME; ?></th>
                    <th class="text-center"><?php echo Lang::$word->_MOD_DS_QNTTY; ?></th>
                    <th class="text-center"><?php echo Lang::$word->_MOD_DS_PRICE; ?></th>
                    <th class="text-center"><?php echo Lang::$word->_MOD_DS_TOTPRICE; ?></th>
                    <th> </th>
                  </tr>
                </thead>

                <tbody>
                  <?php echo Registry::get("Digishop")->loadcart(); ?>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<?php endif; ?>


<!-- Start Back To Top -->
<a id="back-to-top"><i class="icon ion-chevron-up"></i></a>
<!-- End Back To Top -->
<!-- </div> -->

<script src="<?php echo THEMEURL;?>/plugins/jquery.min.js"></script>
<script src="<?php echo SITEURL;?>/assets/jquery-ui.js"></script>




<script src="<?php echo THEMEURL;?>/plugins/bootstrap.min.js"></script>
<script src="<?php echo THEMEURL;?>/plugins/bootstrap-spinner.js"></script>
<script src="<?php echo THEMEURL;?>/plugins/waypoints.min.js"></script>
<script src="<?php echo THEMEURL;?>/plugins/parallax.min.js"></script>
<script src="<?php echo THEMEURL;?>/plugins/easign1.3.min.js"></script>
<script src="<?php echo THEMEURL;?>/plugins/moderniz.min.js"></script>
<script src="<?php echo SITEURL;?>/assets/global.js"></script>
<script src="<?= THEMEURL; ?>/plugins/redactor.min.js"></script>
<script src="<?php echo THEMEURL;?>/plugins/cubeportfolio.min.js"></script>
<script src="<?php echo THEMEURL;?>/plugins/owlcarousel.min.js"></script>
<script src="<?php echo THEMEURL;?>/plugins/tweetie.min.js"></script>
<script src="http://maps.googleapis.com/maps/api/js?sensor=false"></script>
<script src="<?php echo THEMEURL;?>/plugins/gmap3.min.js"></script>
<script src="<?php echo THEMEURL;?>/plugins/wow.min.js"></script>
<script src="<?php echo THEMEURL;?>/plugins/counterup.min.js"></script> 
<script src="<?php echo THEMEURL;?>/scripts.js"></script>
<script src="<?php echo THEMEURL;?>/master.js"></script>

<?php $content->getPluginAssets($assets);?>
<?php $content->getModuleAssets();?>
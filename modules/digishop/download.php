<?php
  /**
   * Download
   *
   * @package Veri Asist
   * @author veriasist.com
   * @copyright 2014
   * @version $Id: download.php, v4.00 2014-01-10 10:12:05 gewa Exp $
   */
  define("_VALID_PHP", true);

  require_once ("../../init.php");
  require_once (MODPATH . "digishop/admin_class.php");

  Registry::set('Digishop', new Digishop());
  $digishop = Registry::get("Digishop");

  define('BASE_DIR', $digishop->filedir);

  $allowed_ext = array(
      'zip' => 'application/zip',
      'rar' => 'application/x-rar-compressed',
      'pdf' => 'application/pdf',
      'doc' => 'application/msword',
      'xls' => 'application/vnd.ms-excel',
      'ppt' => 'application/vnd.ms-powerpoint',
      'exe' => 'application/octet-stream',
      'gif' => 'image/gif',
      'png' => 'image/png',
      'jpg' => 'image/jpeg',
      'jpeg' => 'image/jpeg',
      'mp3' => 'audio/mpeg',
      'wav' => 'audio/x-wav',
      'mpeg' => 'video/mpeg',
      'mpg' => 'video/mpeg',
      'mpe' => 'video/mpeg',
      'mov' => 'video/quicktime',
      'avi' => 'video/x-msvideo');

  set_time_limit(0);

  if (ini_get('zlib.output_compression')) {
      ini_set('zlib.output_compression', 'Off');
  }
?>
<?php
  // Free Downloads
  if (isset($_GET['free'])) {
      $row = $digishop->getFreeDownload($_GET['free']);

      if ($row) {
          $fext = strtolower(substr(strrchr($row->filename, "."), 1));

          if (!file_exists(BASE_DIR . $row->filename) || !is_file(BASE_DIR . $row->filename)) {
              redirect_to(doUrl(false, false, "digishop", "?msg=1"));
              exit;
          }

          if (!array_key_exists($fext, $allowed_ext)) {
              redirect_to(doUrl(false, false, "digishop", "?msg=2"));
              exit;
          }

          if ($digishop->allow_free == '1' && !$user->logged_in) {
              redirect_to(doUrl(false, false, "digishop", "?msg=3"));
              exit;
          }

          downloadFile(BASE_DIR . $row->filename, $row->filename);

      } else {
          redirect_to(doUrl(false, false, "digishop"));
      }

      // Membership Downloads
  } elseif (isset($_GET['member']) && $user->logged_in) {
      $row = $digishop->getMembershipDownload($_GET['member']);

      if ($row) {
          $m_arr = explode(",", $row->membership_id);
          reset($m_arr);

          $fext = strtolower(substr(strrchr($row->filename, "."), 1));

          if (!file_exists(BASE_DIR . $row->filename) || !is_file(BASE_DIR . $row->filename)) {
              redirect_to(doUrl(false, false, "digishop", "?msg=1"));
              exit;
          }

          if (!array_key_exists($fext, $allowed_ext)) {
              redirect_to(doUrl(false, false, "digishop", "?msg=2"));
              exit;
          }

          if ($user->validateMembership() and in_array($user->membership_id, $m_arr)) {
              downloadFile(BASE_DIR . $row->filename, $row->filename);
          } else {
              redirect_to(doUrl(false, false, "digishop", "?msg=4"));
              exit;
          }

      } else {
          redirect_to(doUrl(false, false, "digishop"));
      }

      // Paid Downloads
  } elseif (isset($_GET['fileid'])) {
      $row = $digishop->getPaidDownload($_GET['fileid']);
	  $fext = strtolower(substr(strrchr($row->filename, "."), 1));

      if ($row) {
          if (!file_exists(BASE_DIR . $row->filename) || !is_file(BASE_DIR . $row->filename)) {
              redirect_to(doUrl(false, false, "digishop", "?msg=1"));
              exit;
          }

          if (!array_key_exists($fext, $allowed_ext)) {
              redirect_to(doUrl(false, false, "digishop", "?msg=2"));
              exit;
          }

          downloadFile(BASE_DIR . $row->filename, $row->filename);

      } else {
          redirect_to(doUrl(false, false, "digishop"));
      }

  } else {
      redirect_to(doUrl(false, false, "digishop", "?msg=5"));
      exit;
  }
?>
<?php
  /**
   * CAREER Manager
   *
   * @package VeriAsist
   * @author veriasist.com
   * @copyright 2014
   * @version $Id: main.php, v4.00 2014-05-31 10:12:05 gewa Exp $
   */
  if (!defined("_VALID_PHP"))
      die('Direct access to this location is not allowed.');
	  
  require_once(MODPATH . "career/admin_class.php");
  Registry::set('CareerManager', new CareerManager());
  
  $careerrow = Registry::get("CareerManager")->getCareer();
?>
<!-- Start CAREER Manager -->

<?php if (!$careerrow): ?>
  <section class="wow fadeIn animated bg-white">
    <div class="container position-relative">
      <div class="row margin-nine no-margin-top no-margin-lr">
        <div class="col-md-12 col-sm-12 text-center">
          <h2 class="title-small alt-font text-uppercase letter-spacing-2 display-block font-weight-600 margin-one no-margin-top">
            <?php echo Filter::msgSingleAlert(Lang::$word->_MOD_CAREER_NOT_FOUND);?>
          </h2>
        </div>
      </div>
    </div>
  </section>
<?php else: ?>

<section id="news" class="wow fadeIn animated bg-white no-padding-top" style="visibility: visible; animation-name: fadeIn;">

  <div class="container xs-text-center">
    <!-- text -->
    <div class="row">
      <div class="col-md-8 col-sm-12">
        <span class="text-extra-large margin-six no-margin-lr no-margin-top display-inline-block alt-font black-text"><?php echo cleanSanitize($row->{'body' . Lang::$lang}); ?></span>
      </div>
    </div>
    <!-- end text -->
  </div>

  <div class="container-fluid no-margin-lr no-margin-bottom xs-margin-eleven xs-no-margin-lr xs-no-margin-bottom">
    <div class="row">
      <div class="col-md-12 col-sm-12 news-post news-post-style1 no-padding">

        <?php foreach ($careerrow as $crrow):?>
          <!-- news item -->
          <article>
            <!-- news image -->
            <div class="post-thumbnail" style="width: 100%;">
              <img src="<?php echo SITEURL.'/'.CareerManager::imagepath . $crrow->thumb;?>" alt="" style="width: 100%;">
              <div class="opacity-full bg-dark-gray"></div>
            </div>
            <!-- end news image -->
            <div class="container post-details">
              <div class="row margin-thirteen no-margin-lr xs-margin-twenty-three xs-no-margin-lr">
                <div class="col-md-1 col-sm-2 xs-display-none">
                  <span class="alt-font title-large news-post-number bg-fast-yellow"><i class="fa fa-arrow-right"></i></span>
                </div>
                <div class="col-md-10 col-sm-10 md-margin-three md-no-margin-tb md-no-margin-right sm-no-margin">
                  <h5 class="alt-font margin-two no-margin-lr no-margin-bottom"><a href="#career<?php echo $crrow->id; ?>" data-desc="<?php echo $crrow->{'answer' . Lang::$lang};?>" class="toggle-career"><?php echo $crrow->{'question' . Lang::$lang};?></a></h5>
                  <span class="text-small alt-font"><?php echo truncate(cleanSanitize($crrow->{'answer' . Lang::$lang}), 130, '...');?></span>
                </div>
              </div>
            </div>
          </article>
          <!-- end news item -->
        <?php endforeach; ?>
        
      </div>
    </div>
  </div>
</section>

<script type="text/javascript">
  $(function() {

    var TITLE_FRM = "<?php echo Lang::$word->_MOD_CAREER_TITLE_FRM; ?>";


    $(".toggle-career").on("click", function(e){

      $this = $(this);
      modal = $("#modal-career");

      modal.find(".modal-title").html($this.html());
      modal.find("input[name=title]").val($this.html());
      modal.find(".career-body").html($this.data("desc"));

      $('a[href="#home"]').click();

      modal.modal("toggle");

      e.preventDefault();
    });


    $(".cf-cv").on("click", function(){
      $("input[name=cv]").click();
    });

    $("input[name=cv]").on("input change", function(){
      $(".cf-cv").val($(this)[0].files[0].name);
    });

  });
</script>

<div class="modal fade" id="modal-career">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title"></h4>
      </div>
      <div class="modal-body">

        <div class="tab-content">
          <div role="tabpanel" class="tab-pane active" id="home">
            <div class="career-body">#</div>

            <div class="row">
              <div class="col-sm-12 text-right">
                <a href="#tab" aria-controls="tab" role="tab" data-toggle="tab" class="highlight-button-dark btn btn-medium button inner-link xs-margin-eleven xs-no-margin-lr xs-no-margin-bottom"><?php echo Lang::$word->_CF_APPEAL; ?></a>
              </div>
            </div>
          </div>
          <div role="tabpanel" class="tab-pane" id="tab">
              
            <form id="veriasist_form" class="veriasist form" action="javascript:void(0)" method="post">
              <div id="success" class="no-margin-lr"></div>

              <input type="text" name="name" placeholder="<?php echo Lang::$word->_CF_NAME;?>" value="<?php if ($user->logged_in) echo trim($user->name);?>" class="big-input alt-font">
              <input type="text" name="email" placeholder="<?php echo Lang::$word->_CF_EMAIL;?>" value="<?php if ($user->logged_in) echo $user->email;?>" class="big-input alt-font">
              <input type="text" name="phone" placeholder="<?php echo Lang::$word->_CF_PHONE;?>" class="big-input alt-font">

              <div class="select-style big-select alt-font">
                <select name="gender">
                  <option value=""><?php echo Lang::$word->_CF_GENDER;?></option>
                  <option value="<?php echo Lang::$word->_CF_GENDER_F;?>"><?php echo Lang::$word->_CF_GENDER_F;?></option>
                  <option value="<?php echo Lang::$word->_CF_GENDER_M;?>"><?php echo Lang::$word->_CF_GENDER_M;?></option>
                </select>
              </div>

              <input type="text" placeholder="<?php echo Lang::$word->_CF_CV_FILE;?>" class="cf-cv big-input alt-font cursor-pointer" readonly>

              <div>
                <input type="text" name="code" placeholder="<?php echo Lang::$word->_CF_TOTAL;?>" class="big-input alt-font">
                <img src="<?php echo SITEURL;?>/captcha.php" alt="" class="captcha-img" style="top: 24.6em;" />
              </div>

              <div id="msgholder" class=" padding-one"></div>

              <input name="processCareer" type="hidden" value="1">
              <input name="title" type="hidden" value="#">
              <input name="cv" type="file" class="hidden">
            </form>

            <div class="row">
              <div class="col-sm-12 text-right">
                <a href="#home" aria-controls="home" role="tab" data-toggle="tab" class="highlight-button-dark btn btn-medium"><?php echo Lang::$word->_CANCEL; ?></a>
                <button data-url="/modules/career/sendmail.php" type="button" name="dosubmit" class="highlight-button-dark btn btn-medium"><?php echo Lang::$word->_CF_SEND;?></button>
              </div>
            </div>
          </div>
        </div>

      </div>
    </div>
  </div>
</div>
<?php endif; ?>




<?php if (0): ?>
<div id="career">
  <?php if($careerrow):?>
  <div class="clearfix"><a class="expand_all veriasist basic button" data-hint="<?php echo Lang::$word->_MOD_CAREER_EXP;?>"><?php echo Lang::$word->_MOD_CAREER_EXP;?></a></div>
  <?php foreach ($careerrow as $crrow):?>
  <section class="clearfix">
    <h4 class="question"><i class="icon help"></i><?php echo $crrow->{'question' . Lang::$lang};?></h4>
    <div class="answer clearfix"><?php echo cleanOut($crrow->{'answer' . Lang::$lang});?></div>
  </section>
  <?php endforeach;?>
  <?php unset($crrow);?>
</div>
<script type="text/javascript">
// <![CDATA[
$(document).ready(function () {
    $('h4.question').on('click', function () {
        var parent = $(this).parent();
		var active = $(this).parent();
        var answer = parent.find('.answer');

        if (answer.is(':visible')) {
            answer.slideUp(100, function () {
                answer.slideUp();
				active.removeClass('active')
            });
        } else {
            answer.fadeIn(300, function () {
                answer.slideDown();
				active.addClass('active')
            });
        }
    });

    $("a.expand_all").on("click", function () {
        if (!$('.answer').is(':visible')) {
            $(this).text('<?php echo Lang::$word->_MOD_CAREER_COL;?>');
            $('.answer').slideDown(150);
			$('h4.question').addClass('active');
        } else {
            $(this).text('<?php echo Lang::$word->_MOD_CAREER_EXP;?>');
            $('.answer').slideUp(150);
			$('h4.question').removeClass('active');
        }
    });
});
// ]]>
</script>
<?php endif ?>




<?php endif;?>
<!-- End CAREER Manager /-->
<?php
  /**
   * Articles Tag Search
   *
   * @package VeriAsist
   * @author veriasist.com
   * @copyright 2014
   * @version $Id: tag_search.tpl.php, v4.00 2014-02-20 16:18:34 gewa Exp $
   */
  
  if (!defined("_VALID_PHP"))
      die('Direct access to this location is not allowed.');
	  
  $tagname = sanitize($content->_url[2]);
?>
<?php $latestrow = Registry::get("News")->renderTagList();?>
<?php if (!$tagname || strlen($tagname = trim($tagname)) == 0 || strlen($tagname) < 3):?>
  <section class="wow fadeIn animated no-padding-top">
    <div class="container position-relative">
      <div class="row margin-nine no-margin-top no-margin-lr">
        <div class="col-md-12 col-sm-12 text-center">
          <h2 class="title-small alt-font text-uppercase letter-spacing-2 white-text display-block font-weight-600 margin-one no-margin-top">
            <?php Filter::msgAlert(Lang::$word->_MOD_AMT_NOTAG);?>
          </h2>
        </div>
      </div>
    </div>
  </section>
<?php elseif(!$latestrow):?>
  <section class="wow fadeIn animated no-padding-top">
    <div class="container position-relative">
      <div class="row margin-nine no-margin-top no-margin-lr">
        <div class="col-md-12 col-sm-12 text-center">
          <h2 class="title-small alt-font text-uppercase letter-spacing-2 white-text display-block font-weight-600 margin-one no-margin-top">
            <?php Filter::msgAlert(Lang::$word->_SR_SEARCH_EMPTY . '<span class="veriasist black label">'.$tagname.'</span>' . Lang::$word->_SR_SEARCH_EMPTY1);?>
          </h2>
        </div>
      </div>
    </div>
  </section>
<?php else:?>

  <section class="wow fadeIn animated no-padding-top">
    <div class="container position-relative">
      <!-- section title -->
      <div class="row margin-nine no-margin-top no-margin-lr">
        <div class="col-md-12 col-sm-12 text-center">
        <h2 class="title-large alt-font text-uppercase letter-spacing-2 white-text display-block font-weight-600 margin-one no-margin-top"><?php echo Lang::$word->_NEWS;?> / #<?php echo $tagname; ?> </h2>
          <div class="margin-four no-margin-top"><img src="<?=THEMEURL?>/images/separator3.png" alt=""/></div>
        </div>
      </div>
      <!-- end section title -->
      <div class="row news-post-style2">

        <?php foreach ($latestrow as $i => $row): ?>
          <!-- news item -->
          <article class="col-md-6 col-sm-6 col-xs-12 margin-four no-margin-lr no-margin-top">
            <div class="col-md-12 col-sm-12 col-xs-12 no-padding bg-white">
              <!-- news image -->
              <div class="col-md-6 col-sm-6 col-xs-6 no-padding post-thumbnail overflow-hidden">
                <a href="<?php echo doUrl(false, $row->slug, "news-item");?>">
                  <img src="<?php echo SITEURL;?>/thumbmaker.php?src=<?php echo SITEURL.'/'.News::imagepath . $row->thumb;?>&amp;h=350&amp;w=600&amp;s=1&amp;a=tl" alt=""/>
                </a>
              </div>
              <!-- end news image -->
              <div class="col-md-6 col-sm-6 col-xs-6 no-padding post-details-arrow">
                <div class="post-details">
                  <span class="text-small text-uppercase letter-spacing-2 alt-font light-gray-text"><?php echo Filter::dodate("short_date", $row->created);?></span>
                  <span class="text-extra-large text-uppercase display-block alt-font margin-twelve no-margin-lr sm-text-medium xs-text-medium">
                    <a href="<?php echo doUrl(false, $row->slug, "news-item");?>"><?php echo truncate($row->atitle,40);?></a>
                  </span>
                  <div class="separator-line bg-deep-red margin-seventeen no-margin-bottom margin-lr-auto"></div>
                </div>
              </div>
            </div>
          </article>
          <!-- end news item -->
        <?php endforeach; ?>
        <?php unset($row);?>

      </div>
    </div>
  </section>




 <?php if (0): ?>
                <h1 class="veriasist double header"><span><?php echo '<small class="veriasist label">' . count($latestrow) . '</small> ' .Lang::$word->_MOD_AMT_RESFOR . $tagname;?></span></h1>
                <div id="articles" class="relative layout-left">
                  <?php foreach ($latestrow as $row):?>
                  <article class="clearfix">
                    <?php if($row->show_created):?>
                    <aside>
                      <div><span class="day"><?php echo $row->day;?></span><span class="year"><?php echo News::getShortMonths($row->month) . ' ' . $row->year;?></span></div>
                    </aside>
                    <?php endif;?>
                    <section>
                      <div class="header clearfix">
                        <div class="title">
                          <h3><a href="<?php echo doUrl(false, $row->slug, "news-item");?>" class="inverted"><?php echo $row->atitle;?></a></h3>
                        </div>
                        <div class="veriasist small horizontal divided list">
                          <?php if($row->show_author):?>
                          <div class="item"> <i class="icon user"></i> <a href="<?php echo doUrl(false, $row->username, "news-author");?>" class="inverted"><?php echo $row->username;?></a> </div>
                          <?php endif;?>
                          <div class="item"> <i class="icon sitemap"></i> <a href="<?php echo doUrl(false, $row->catslug, "news-cat");?>" class="inverted"><?php echo $row->catname;?></a> </div>
                          <div class="item"> <i class="icon chat"></i> <?php echo $row->totalcomments;?> <?php echo Lang::$word->_MOD_AM_COMMENTS;?></div>
                        </div>
                      </div>
                      <?php if($row->thumb):?>
                      <figure class="small-top-space small-bottom-space">
                        <div class="image-overlay veriasist left floated image"> <img src="<?php echo SITEURL;?>/thumbmaker.php?src=<?php echo SITEURL.'/'.News::imagepath . $row->thumb;?>&amp;h=150&amp;w=250&amp;s=1&amp;a=tl" alt="">
                          <div class="overlay-fade"> <a title="<?php echo $row->{'caption'.Lang::$lang};?>" href="<?php echo doUrl(false, $row->slug, "news-item");?>"><i class="icon-overlay icon url"></i></a></div>
                        </div>
                      </figure>
                      <?php endif;?>
                      <?php $desc = cleanSanitize($row->{'short_desc'.Lang::$lang});?>
                      <div class="description"><?php echo $desc;?> <a href="<?php echo doUrl(false, $row->slug, "news-item");?>"><?php echo Lang::$word->_MOD_AM_MORE;?><i class="icon right angle"></i></a></div>
                    </section>
                  </article>
                  <?php endforeach;?>
                  <?php unset($row);?>
                </div>
<?php endif ?> 



<?php endif;?>
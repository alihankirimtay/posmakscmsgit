<?php
  /**
   * News Main
   *
   * @package VeriAsist
   * @author veriasist.com
   * @copyright 2014
   * @version $Id: main.php, v2.00 2014-04-10 16:18:34 gewa Exp $
   */
  
  if (!defined("_VALID_PHP"))
      die('Direct access to this location is not allowed.');
	  
  require_once (MODPATH . "news/admin_class.php");
  $classname = 'News';
  try {
	  if (!class_exists($classname)) {
		  throw new exception('Missing news/admin_class.php');
	  }
	  Registry::set('News', new News(false));
  }
  catch (exception $e) {
	  echo $e->getMessage();
  }

  $length = count($content->_url);

?>
<?php switch($length): case 3: ?>
  <?php if(in_array(doUrlParts("news-tags"), $content->_url)):?>
	<?php include_once("layout_tags.tpl.php");?>
  <?php elseif(in_array(doUrlParts("news-archive"), $content->_url)):?>
	<?php include_once("layout_archive.tpl.php");?>
  <?php elseif(in_array(doUrlParts("news-author"), $content->_url)):?>
	<?php include_once("layout_author.tpl.php");?>
  <?php else:?>
	<?php $single = $content->moduledata->mod;?>
    <?php $latestrow = Registry::get("News")->renderCategory($single->id, $single->perpage, $single->slug);?>

        
    <?php switch($single->layout): case 4: ?>
    <?php include_once("layout_twocol.tpl.php");?>
    <?php break;?>
    <?php case 3: ?>
    <?php include_once("layout_top.tpl.php");?>
    <?php break;?>
    <?php case 2: ?>
    <?php include_once("layout_right.tpl.php");?>
    <?php break;?>
    <?php default: ?>
    <?php include_once("layout_left.tpl.php");?>
    <?php break;?>
    <?php endswitch;?>
  <?php endif;?>

<?php break;?>
<?php case 2: ?>
  
  <?php if(in_array(doUrlParts("news-search"), $content->_url)):?>
	<?php include_once("layout_search.tpl.php");?>
  <?php else:?>
	<?php $row = $content->moduledata->mod; ?>
  <?php News::doHits($row->id);?>
  <?php $artcats = Registry::get("News")->getCatList();?>
  <?php $populars = Registry::get("News")->getPopularList(); ?>
  <?php $newstags = Registry::get("News")->getArticleTags(); ?>

  <!-- page title -->
  <div>
    <div class="container">
      <article class="hentry post post-standard has-post-thumbnail  post-standard-details">

          <div class="post-thumb">
            <img style="width: 50%; position: relative; left: 0; right: 0; margin:auto;" src="<?php echo SITEURL.'/'.News::imagepath . $row->thumb;?>" alt="post">
          </div>

          <div class="post__content">

            

            

            <div class="post__date">

              <time class="published" datetime="2017-03-20 12:00:00">
                <a href="#" class="number"><?= date("d" ,strtotime($row->created)); ?></a>
                <span class="month"><?= date("m" ,strtotime($row->created)); ?> <?= date("Y" ,strtotime($row->created)); ?></span>
              </time>

            </div>

            <div class="post__content-info">

              <h3 class="post__title entry-title"><?= $row->atitle; ?></h3>

              <div class="post-additional-info">
                <span class="category">
                  Kategori
                  <a href="<?= doUrl(false, $row->catslug, "news-cat");?>"><?= $row->catname?></a>
                </span>
               <!--  <span class="post__comments">
                  <a href="#">6 <span>Comments</span></a>
                </span> -->
              </div>

            </div>

           <?= cleanOut($row->body_tr); ?>

           <?php 
            $tags = explode(',', $row->tags);
           ?>
            <div class="post-details-shared">
              <ul class="tags-inline">
                <li>Etiketler:</li>

                <?php foreach($tags as $t): ?>
                <li>
                  <a href="#">
                    <?= $t?>
                  </a>
                  ,
                </li>
               <?php endforeach; ?>
              </ul>
          </div>

        </article>
    </div>
  </div>
 
  <!-- end page title -->
  
  <?php //include_once("layout_single_left.tpl.php");?>

	<?php // switch($row->layout): case 4: ?>
    <?php // include_once("layout_single_bottom.tpl.php");?>
    <?php // break;?>
    <?php // case 3: ?>
    <?php // include_once("layout_single_top.tpl.php");?>
    <?php // break;?>
    <?php // case 2: ?>
    <?php // include_once("layout_single_right.tpl.php");?>
    <?php // break;?>
    <?php // default: ?>
    <?php // include_once("layout_single_left.tpl.php");?>
    <?php // break;?>
    <?php //endswitch;?>
  <?php endif;?>

<?php break;?>
<?php default: ?>
  
  <?php $latestrow = Registry::get("News")->getLatestArticles();?>

  <?php switch(Registry::get("News")->flayout): case 4: ?>
  <?php include_once("layout_twocol.tpl.php");?>
  <?php break;?>
  <?php case 3: ?>
  <?php include_once("layout_top.tpl.php");?>
  <?php break;?>
  <?php case 2: ?>
  <?php include_once("layout_right.tpl.php");?>
  <?php break;?>
  <?php default: ?>
  <?php include_once("layout_left.tpl.php");?>
  <?php break;?>
  <?php endswitch;?>

<?php break;?>
<?php endswitch;?>

<?php
  /**
   * News Layout Top
   *
   * @package VeriAsist
   * @author veriasist.com
   * @copyright 2014
   * @version $Id: layout_top.tpl.php, v4.00 2014-04-20 16:18:34 gewa Exp $
   */
  
  if (!defined("_VALID_PHP"))
      die('Direct access to this location is not allowed.');
?>
<?php if(!$latestrow):?>
<?php echo Filter::msgSingleAlert(Lang::$word->_MOD_AM_NO_ART_CATS);?>
<?php else:?>
<?php if($length == 3):?>
<h3><a href="<?php echo SITEURL;?>/news_rss.php?cid=<?php echo $single->id;?>" class="push-right"><i class="icon rss"></i></a>
  <?php if($single->icon):?>
  <i class="<?php echo $single->icon;?>"></i>
  <?php endif;?>
  <?php echo Lang::$word->_MOD_AM_BLCAT.' / '.$single->{'name'.Lang::$lang};?></h3>
<?php if($single->{'description'.Lang::$lang}):?>
<p><?php echo cleanSanitize($single->{'description'.Lang::$lang});?></p>
<div class="veriasist divider"></div>
<?php endif;?>
<?php endif;?>
<div id="articles" class="relative layout-top">
  <?php foreach ($latestrow as $row):?>
  <article class="clearfix">
    <?php if($row->show_created):?>
    <aside>
      <div><span class="day"><?php echo $row->day;?></span><span class="year"><?php echo News::getShortMonths($row->month) . ' ' . $row->year;?></span></div>
    </aside>
    <?php endif;?>
    <section>
      <div class="header clearfix">
        <div class="title">
          <h3><a href="<?php echo doUrl(false, $row->slug, "news-item");?>" class="inverted"><?php echo $row->atitle;?></a></h3>
        </div>
        <div class="veriasist small horizontal divided list">
          <?php if($row->show_author):?>
          <div class="item"> <i class="icon user"></i> <a href="<?php echo doUrl(false, $row->username, "news-author");?>" class="inverted"><?php echo $row->username;?></a> </div>
          <?php endif;?>
          <div class="item"> <i class="icon sitemap"></i> <a href="<?php echo doUrl(false, $row->catslug, "news-cat");?>" class="inverted"><?php echo $row->catname;?></a> </div>
          <div class="item"> <i class="icon chat"></i> <?php echo $row->totalcomments;?> <?php echo Lang::$word->_MOD_AM_COMMENTS;?></div>
        </div>
      </div>
      <?php if($row->thumb):?>
      <figure class="small-top-space small-bottom-space">
        <div class="image-overlay veriasist center floated image"> <img src="<?php echo SITEURL.'/'.News::imagepath . $row->thumb;?>" alt="">
          <div class="overlay-fade"> <a title="<?php echo $row->{'caption'.Lang::$lang};?>" href="<?php echo doUrl(false, $row->slug, "news-item");?>"><i class="icon-overlay icon url"></i></a></div>
        </div>
      </figure>
      <?php endif;?>
      <?php $desc = cleanSanitize($row->{'short_desc'.Lang::$lang});?>
      <div class="description"><?php echo $desc;?> <a href="<?php echo doUrl(false, $row->slug, "news-item");?>"><?php echo Lang::$word->_MOD_AM_MORE;?><i class="icon right angle"></i></a></div>
    </section>
  </article>
  <?php endforeach;?>
  <?php unset($row);?>
  <div id="pagination" class="content-center"><?php echo $pager->display_pages();?></div>
</div>
<?php endif;?>
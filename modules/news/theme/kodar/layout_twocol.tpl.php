<?php
  /**
   * News Layout Two Columns
   *
   * @package VeriAsist
   * @author veriasist.com
   * @copyright 2014
   * @version $Id: layout_twocol.tpl.php, v4.00 2014-04-20 16:18:34 gewa Exp $
   */

  if (!defined("_VALID_PHP"))
      die('Direct access to this location is not allowed.');
?>
<?php if(!$latestrow):?>
  <section class="wow fadeIn animated no-padding-top">
    <div class="container position-relative">
      <div class="row margin-nine no-margin-top no-margin-lr">
        <div class="col-md-12 col-sm-12 text-center">
        <h2 class="title-small alt-font text-uppercase letter-spacing-2 white-text display-block font-weight-600 margin-one no-margin-top">
            <?php echo Filter::msgSingleAlert(Lang::$word->_MOD_AM_NO_ART_CATS);?>
          </h2>
        </div>
      </div>
    </div>
  </section>
<?php else:?>


  <div class="container">
    <div class="row">

      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <main class="main">

          <?php foreach ($latestrow as $i => $row): ?>
          <article class="hentry post post-standard has-post-thumbnail ">

            <div class="post-thumb">
              <img style="width: 50%; position: relative; right: 0;left: 0;margin:auto;" src="<?php echo SITEURL.'/'.News::imagepath . $row->thumb;?>" alt="post">
              <a href="<?php echo SITEURL.'/'.News::imagepath . $row->thumb;?>" class="link-image js-zoom-image">
                <svg class="utouch-icon utouch-icon-zoom-increasing-button-outline"><use xlink:href="#utouch-icon-zoom-increasing-button-outline"></use></svg>
              </a>
              <a href="<?= doUrl(false, $row->slug, "news-item"); ?>" class="link-post">
                <svg class="utouch-icon utouch-icon-link-chain"><use xlink:href="#utouch-icon-link-chain"></use></svg>
              </a>
              <div class="overlay-standard overlay--blue-dark"></div>
            </div>

            <div class="post__content">

              <a href="#" class="social__item main">
                <svg class="utouch-icon utouch-icon-1496680146-share"><use xlink:href="#utouch-icon-1496680146-share"></use></svg>
              </a>

              <div class="share-product">

                <ul class="socials">
                  <li>
                    <a href="https://www.facebook.com/sharer.php?u=<?= doUrl(false, $row->slug, "news-item"); ?>" target="_blank" class="social__item facebook">
                      <svg class="utouch-icon" viewBox="0 0 16 16" xmlns="http://www.w3.org/2000/svg" fill-rule="evenodd" clip-rule="evenodd" stroke-linejoin="round" stroke-miterlimit="1.414"><path d="M15.117 0H.883C.395 0 0 .395 0 .883v14.234c0 .488.395.883.883.883h7.663V9.804H6.46V7.39h2.086V5.607c0-2.066 1.262-3.19 3.106-3.19.883 0 1.642.064 1.863.094v2.16h-1.28c-1 0-1.195.48-1.195 1.18v1.54h2.39l-.31 2.42h-2.08V16h4.077c.488 0 .883-.395.883-.883V.883C16 .395 15.605 0 15.117 0" fill-rule="nonzero"></path></svg>
                    </a>
                  </li>

                  <li>
                    <a href="https://twitter.com/home?status=<?php echo doUrl(false, $row->slug, "news-item");?>" target="_blank" class="social__item twitter">
                      <svg class="utouch-icon" viewBox="0 0 16 16" xmlns="http://www.w3.org/2000/svg" fill-rule="evenodd" clip-rule="evenodd" stroke-linejoin="round" stroke-miterlimit="1.414"><path d="M16 3.038c-.59.26-1.22.437-1.885.517.677-.407 1.198-1.05 1.443-1.816-.634.37-1.337.64-2.085.79-.598-.64-1.45-1.04-2.396-1.04-1.812 0-3.282 1.47-3.282 3.28 0 .26.03.51.085.75-2.728-.13-5.147-1.44-6.766-3.42C.83 2.58.67 3.14.67 3.75c0 1.14.58 2.143 1.46 2.732-.538-.017-1.045-.165-1.487-.41v.04c0 1.59 1.13 2.918 2.633 3.22-.276.074-.566.114-.865.114-.21 0-.41-.02-.61-.058.42 1.304 1.63 2.253 3.07 2.28-1.12.88-2.54 1.404-4.07 1.404-.26 0-.52-.015-.78-.045 1.46.93 3.18 1.474 5.04 1.474 6.04 0 9.34-5 9.34-9.33 0-.14 0-.28-.01-.42.64-.46 1.2-1.04 1.64-1.7z" fill-rule="nonzero"></path></svg>
                    </a>
                  </li>

                  <li>
                    <a href="https://plus.google.com/share?url=<?php echo doUrl(false, $row->slug, "news-item");?>" target="_blank" class="social__item googlePlus">
                      <svg class="utouch-icon" viewBox="0 0 16 16" xmlns="http://www.w3.org/2000/svg" fill-rule="evenodd" clip-rule="evenodd" stroke-linejoin="round" stroke-miterlimit="1.414"><path d="M8.16 6.857V9.6h4.537c-.183 1.177-1.37 3.45-4.537 3.45-2.73 0-4.96-2.26-4.96-5.05s2.23-5.05 4.96-5.05c1.554 0 2.594.66 3.19 1.233l2.17-2.092C12.126.79 10.32 0 8.16 0c-4.423 0-8 3.577-8 8s3.577 8 8 8c4.617 0 7.68-3.246 7.68-7.817 0-.526-.057-.926-.126-1.326H8.16z"></path></svg>
                    </a>
                  </li>
                </ul>

              </div>

              <div class="post__date">

                <time class="puchoose-item bg-orange align-centerlished" datetime="<?= Filter::dodate("short_date", $row->created);?>">
                  <a href="#" class="number"><?= $row->day; ?></a>
                  <span class="month"><?= $row->month; ?> <?= $row->year; ?></span>
                  <!-- <span class="day">Monday</span> -->
                </time>

              </div>

              <div class="post__content-info">

                <a href="<?= doUrl(false, $row->slug, "news-item");?>" class="h5 post__title entry-title"><?php echo truncate($row->atitle,40);?></a>

                <p class="post__text"><?= $row->short_desc_tr; ?>
                </p>

                <div class="post-additional-info">
      
                  <span class="category">
                    Kategori
                    <a href="<?= doUrl(false, $row->catslug, "news-cat");?>"><?= $row->catname; ?></a>
                  </span>

                  <!-- <span class="post__comments">
                    <a href="#">0 <span>Comments</span></a>
                  </span> -->



                  <a href="<?= doUrl(false, $row->slug, "news-item");?>" class="btn-next">
                    <svg class="utouch-icon icon-hover utouch-icon-arrow-right-1"><use xlink:href="#utouch-icon-arrow-right-1"></use></svg>
                    <svg class="utouch-icon utouch-icon-arrow-right1"><use xlink:href="#utouch-icon-arrow-right1"></use></svg>
                  </a>

                </div>

              </div>
            </div>

          </article>
        <?php endforeach; ?>

        </main>
      </div>
    </div>
  </div>

<?php //echo $pager->display_pages();?>

<?php endif;?>
<?php if(!News::getMembershipAccess($row->membership_id)):?>
<?php echo Filter::msgSingleAlert(Lang::$word->_UA_ACC_ERR2 . News::listMemberships($row->membership_id));?>
<?php else:?>



  
  
  <div class="breadcrumb alt-font no-margin-bottom bg-white">
    <div class="container">
      <div class="col-sm-9">
        <!-- <ul>
          <li><a href="<?php  echo SITEURL;?>/"><?php  echo Lang::$word->_HOME;?></a></li>
          <?php echo $content->getBreadcrumbs();?>
        </ul>  -->
      </div>
      <div class="col-sm-3">
        <!-- Livesearch Start -->
        <?php if($core->showsearch):?>
          <div class="widget margin-twenty no-margin-lr no-margin-top xs-margin-twelve xs-no-margin-lr xs-no-margin-top">
            <form action="<?php echo Url::Page($core->search_page);?>" method="POST" id="livesearch" name="search-form">
              <i class="fa fa-search close-search search-button"></i>
              <input type="text" class="alt-font news-search-btn" id="searchfield" name="keywords" placeholder="<?php echo Lang::$word->_SEARCH;?>..." autocomplete="off">
            </form>
          </div>
        <?php endif;?>
        <!-- Livesearch Start END --> 
      </div>
    </div>
  </div>
  


  <section class="wow border-bottom bg-white">
    <div class="container">
      <div class="row">
        <div class="col-md-8 col-sm-8 news-listing">
          <article>
            
            <!-- post gallery -->
            <?php if($row->gallery):?>
              <div class="col-md-12 post-details no-padding margin-seven no-margin-lr no-margin-top">
                <?php include_once("gallery.tpl.php");?>
              </div>
            <?php endif;?>
            <!-- end post gallery -->
          
            
            <div class="no-margin-lr">
              <!-- text -->
              <?php echo cleanOut($row->{'body'.Lang::$lang}); ?>
              <!-- end text -->
              
              <!-- post tags -->
              <?php if($row->tags):?>
                <?php $tags = explode(',', $row->tags);?>
                <div class="margin-twelve no-margin-lr">
                  <h5 class="alt-font text-extra-large dark-gray-text"><?php echo Lang::$word->_TAGS; ?></h5>
                  <?php foreach($tags as $tag):?>
                    <a href="<?php echo doUrl(false, $tag, "news-tags");?>" class="text-small text-uppercase alt-font"><?php echo $tag;?></a>
                  <?php endforeach;?>
                </div>                
              <?php endif;?>
              <!-- end post tags -->
              
              <!-- author -->
              <?php if($row->show_author):?>
                <div class="text-center bg-gray">
                  <div class="news-comment text-left clearfix no-margin padding-ten">
                    <!-- author image -->
                    <a href="<?php echo doUrl(false, $row->username, "news-author");?>" class="comment-avtar no-margin-top"><i class="fa fa-user"></i></a>
                    <!-- end author image -->
                    <!-- author text -->
                    <div class="comment-text overflow-hidden position-relative">
                      <h5 class="alt-font text-large dark-gray-text"><a href="<?php echo doUrl(false, $row->username, "news-author");?>"><?php echo $row->username;?></a></h5>
                    </div>
                    <!-- end author text -->
                  </div>
                </div>
              <?php endif;?>
              <!-- end author -->

            </div>


            <!-- social media icon -->
            <div class="text-center">
            <?php $social_u = urlencode(doUrl(false, $row->slug, "news-item")); ?>
              <a class="" href="http://www.facebook.com/sharer.php?u=<?= $social_u ?>" target="_blank"><i class="fa fa-facebook dark-gray-text title-medium"></i></a>
              <a class="margin-ten no-margin-top no-margin-bottom no-margin-right" href="https://twitter.com/share?url=<?= $social_u ?>&text=<?= $row->atitle ?>&via=<?= Registry::get('Core')->site_url ?>&hashtags=<?= Registry::get('Core')->site_name ?>" target="_blank"><i class="fa fa-twitter dark-gray-text title-medium"></i></a>
              <a class="margin-ten no-margin-top no-margin-bottom no-margin-right" href="https://plus.google.com/share?url=<?= $social_u ?>" target="_blank"><i class="fa fa-google-plus dark-gray-text title-medium"></i></a>
              <a class="margin-ten no-margin-top no-margin-bottom no-margin-right" href="http://www.linkedin.com/shareArticle?url=<?= $social_u ?>&title=<?= $row->atitle ?>" target="_blank"><i class="fa fa-linkedin dark-gray-text title-medium"></i></a>
            </div>
            <!-- end social media icon -->


            <div class="bg-fast-yellow separator-line-thick-full no-margin-lr md-margin-eleven md-no-margin-lr"></div>
          </article>

        </div>


        <!-- sidebar -->
        <div class="col-md-3 col-md-offset-1 col-sm-4">

          <!-- widget  -->
          <div class="widget">
            <span class="alt-font text-uppercase dark-gray-text font-weight-600 text-large"><?php echo Lang::$word->_MOD_AM_CATEGORIES;?></span>
            <div class="bg-fast-yellow separator-line-thick no-margin-lr md-margin-eleven md-no-margin-lr"></div>
            <div class="widget-body">
              <ul class="category-list">
              <?php foreach ($artcats as $cat): ?>
                <li><a href="<?php echo doUrl(false, $cat['slug'], "news-cat"); ?>"><?php echo $cat[ 'name' . Lang::$lang ] ?></a></li>
              <?php endforeach ?>
              </ul>
            </div>
          </div>
          <!-- end widget  -->

          <!-- widget  -->
          <div class="widget">
            <span class="alt-font text-uppercase dark-gray-text font-weight-600 text-large"><?php echo Lang::$word->_MOD_AM_POPULARS;?></span>
            <div class="bg-fast-yellow separator-line-thick no-margin-lr md-margin-eleven md-no-margin-lr"></div>
            <div class="widget-body">
              <ul class="widget-posts">
                <?php foreach ($populars as $popular): ?>
                  <li><a href="<?php echo doUrl(false, $popular->slug, "news-item"); ?>"><?php echo $popular->atitle; ?></a><?php echo Filter::dodate("short_date", $popular->created);?></li>
                <?php endforeach ?>
              </ul>
            </div>
          </div>
          <!-- end widget  -->

          <!-- widget  -->
          <div class="widget">
            <span class="alt-font text-uppercase dark-gray-text font-weight-600 text-large"><?php echo Lang::$word->_MOD_AM_TAGS;?></span>
            <div class="bg-fast-yellow separator-line-thick no-margin-lr md-margin-eleven md-no-margin-lr"></div>
            <div class="widget-body tags">
              <?php foreach ($newstags as $tag): ?>
                <a href="<?php echo doUrl(false, $tag->tags, "news-tags");?>"><?php echo $tag->tags; ?></a>
              <?php endforeach ?>
            </div>
          </div>
          <!-- end widget  -->

        </div>
        <!-- end sidebar -->
      </div>
    </div>
  </section>


 <?php if (0): ?>
                    <h3><?php echo $row->atitle;?></h3>
                    <div class="veriasist divider"></div>
                    <div class="veriasist horizontal divided list">
                      <?php if($row->show_author):?>
                      <div class="item"> <i class="icon user"></i> <a href="<?php echo doUrl(false, $row->username, "news-author");?>" class="inverted"><?php echo $row->username;?></a> </div>
                      <?php endif;?>
                      <div class="item"> <i class="icon sitemap"></i> <a href="<?php echo doUrl(false, $row->catslug, "news-cat");?>" class="inverted"><?php echo $row->catname;?></a> </div>
                      <div class="item"> <i class="icon chat"></i> <?php echo $row->totalcomments;?> <?php echo Lang::$word->_MOD_AM_COMMENTS;?></div>
                      <div class="item"> <i class="icon bullseye"></i> <?php echo $row->hits;?> </div>
                      <?php if($row->show_created):?>
                      <div class="item"> <i class="icon calendar"></i> <?php echo Filter::dodate("short_date",$row->created);?> </div>
                      <?php endif;?>
                    </div>
                    <div class="veriasist divider"></div>
                    <article id="article" class="clearfix">
                      <figure class="small-top-space small-bottom-space">
                        <?php if($row->gallery):?>
                        <?php include_once("gallery.tpl.php");?>
                        <?php else:?>
                        <?php if($row->thumb):?>
                        <div class="image-overlay veriasist left floated image"> <img src="<?php echo SITEURL;?>/thumbmaker.php?src=<?php echo SITEURL.'/'.News::imagepath . $row->thumb;?>&amp;h=200&amp;w=300&amp;s=1&amp;a=tl" alt="">
                          <div class="overlay-fade"> <a title="<?php echo $row->{'caption'.Lang::$lang};?>" href="<?php echo SITEURL.'/'.News::imagepath . $row->thumb;?>" class="lightbox"><i class="icon-overlay icon url"></i></a></div>
                        </div>
                        <?php endif;?>
                        <?php endif;?>
                      </figure>
                      <div class="clearfix"><?php echo cleanOut($row->{'body'.Lang::$lang});?></div>
                      <div class="veriasist divider"></div>
                      <div class="veriasist horizontal divided list">
                        <?php if($row->filename):?>
                        <div class="item"> <i class="icon download disk"></i> <a href="<?php echo MODURL;?>news/datafiles/<?php echo $row->filename;?>" class="inverted"><?php echo Lang::$word->_MOD_AM_FILE_ATTD;?></a> </div>
                        <?php endif;?>
                        <?php if($row->show_ratings):?>
                        <div class="item">
                          <?php News::renderRating($row->id, $row->rating, $row->rate_number, "small");?>
                        </div>
                        <?php endif;?>
                        <div class="item"> <i class="icon calendar"></i> <?php echo Lang::$word->_MOD_AM_MODIFIED;?>: <?php echo ($row->modified == 0) ? '-/-' : dodate($core->long_date,$row->modified);?> </div>
                      </div>
                      <div class="veriasist divider"></div>
                      <?php $related = Registry::get("News")->getRelatedArticles($row->id, $row->atitle);?>
                      <?php if($related):?>
                      <b><?php echo Lang::$word->_MOD_AM_RELATED;?></b>:
                      <?php foreach($related as $relrow):?>
                      <a href="<?php echo doUrl(false, $relrow->slug, "news-item");?>" class="veriasist label"><?php echo $relrow->atitle;?></a>
                      <?php endforeach;?>
                      <div class="veriasist divider"></div>
                      <?php endif;?>
                      <?php if($row->show_sharing or $row->show_like):?>
                      <div class="content"> <a target="_blank" data-content="<?php echo Lang::$word->_MOD_AM_SHARE;?> Facebook" href="https://www.facebook.com/sharer/sharer.php?u=<?php echo doUrl(false, $row->slug, "news-item");?>" class="veriasist icon facebook button"><i class="icon facebook"></i></a> <a data-content="<?php echo Lang::$word->_MOD_AM_SHARE;?> Twitter" href="https://twitter.com/home?status=<?php echo doUrl(false, $row->slug, "news-item");?>" class="veriasist icon twitter button"><i class="icon twitter"></i></a> <a target="_blank" data-content="<?php echo Lang::$word->_MOD_AM_SHARE;?> Google +" href="https://plus.google.com/share?url=<?php echo doUrl(false, $row->slug, "news-item");?>" class="veriasist icon google plus button"><i class="icon google plus"></i></a> <a target="_blank" data-content="<?php echo Lang::$word->_MOD_AM_SHARE;?> Pinterest" href="https://pinterest.com/pin/create/button/?url=&amp;media=<?php echo doUrl(false, $row->slug, "news-item");?>" class="veriasist icon pinterest button"><i class="icon pinterest"></i></a>
                        <?php if($row->show_like):?>
                        <a data-id="<?php echo $row->id;?>" data-total="<?php echo $row->like_up;?>" class="veriasist like danger labeled icon button push-right"> <i data-content="<?php echo Lang::$word->_MOD_AM_LIKE;?>" class="heart icon"></i> <span><?php echo $row->like_up;?></span> </a>
                        <?php endif;?>
                      </div>
                      <div class="veriasist divider"></div>
                      <?php endif;?>
                      <?php if($row->tags):?>
                      <?php $tags = explode(",", $row->tags);?>
                      <?php foreach($tags as $tag):?>
                      <a href="<?php echo doUrl(false, $tag, "news-tags");?>" class="veriasist tag label"><i class="icon tag"></i> <?php echo $tag;?></a>
                      <?php endforeach;?>
                      <div class="veriasist divider"></div>
                      <?php endif;?>
                    </article>
                    <?php include_once("comments.tpl.php");?>
                    
 <?php endif ?>

<script src="<?php echo MODURL;?>news/common.js"></script>
<?php endif;?>
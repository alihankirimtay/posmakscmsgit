<?php
  /**
   * Articles Layout Author
   *
   * @package VeriAsist
   * @author veriasist.com
   * @copyright 2014
   * @version $Id: layout_author.tpl.php, v4.00 2014-03-24 15:14:44 gewa Exp $
   */
  
  if (!defined("_VALID_PHP"))
      die('Direct access to this location is not allowed.');
?>
<?php $latestrow = Registry::get("News")->renderAuthorArticles();?>
<?php $bio = getValues("info, avatar, username", Users::uTable, "username = '" . $content->_url[2] . "'");?>
<?php if(!$latestrow):?>
  <section class="wow fadeIn animated no-padding-top">
    <div class="container position-relative">
      <div class="row margin-nine no-margin-top no-margin-lr">
        <div class="col-md-12 col-sm-12 text-center">
          <h2 class="title-small alt-font text-uppercase letter-spacing-2 white-text display-block font-weight-600 margin-one no-margin-top">
            <?php echo Filter::msgSingleAlert(Lang::$word->_MOD_AM_NOAUTHOR);?>
          </h2>
        </div>
      </div>
    </div>
  </section>
<?php else:?>


  <section class="wow fadeIn animated no-padding-top">
    <div class="container position-relative">
      <!-- section title -->
      <div class="row margin-nine no-margin-top no-margin-lr">
        <div class="col-md-12 col-sm-12 text-center">
          <h2 class="title-large alt-font text-uppercase letter-spacing-2 white-text display-block font-weight-600 margin-one no-margin-top"><?php echo Lang::$word->_NEWS;?> / <?php echo $content->_url[2]; ?> </h2>
          <div class="margin-four no-margin-top"><img src="<?=THEMEURL?>/images/separator3.png" alt=""/></div>
        </div>
      </div>
      <!-- end section title -->
      <div class="row news-post-style2">

        <?php foreach ($latestrow as $i => $row): ?>
          <!-- news item -->
          <article class="col-md-6 col-sm-6 col-xs-12 margin-four no-margin-lr no-margin-top">
            <div class="col-md-12 col-sm-12 col-xs-12 no-padding bg-white">
              <!-- news image -->
              <div class="col-md-6 col-sm-6 col-xs-6 no-padding post-thumbnail overflow-hidden">
                <a href="<?php echo doUrl(false, $row->slug, "news-item");?>">
                  <img src="<?php echo SITEURL;?>/thumbmaker.php?src=<?php echo SITEURL.'/'.News::imagepath . $row->thumb;?>&amp;h=350&amp;w=600&amp;s=1&amp;a=tl" alt=""/>
                </a>
              </div>
              <!-- end news image -->
              <div class="col-md-6 col-sm-6 col-xs-6 no-padding post-details-arrow">
                <div class="post-details">
                  <span class="text-small text-uppercase letter-spacing-2 alt-font light-gray-text"><?php echo Filter::dodate("short_date", $row->created);?></span>
                  <span class="text-extra-large text-uppercase display-block alt-font margin-twelve no-margin-lr sm-text-medium xs-text-medium">
                    <a href="<?php echo doUrl(false, $row->slug, "news-item");?>"><?php echo truncate($row->atitle,40);?></a>
                  </span>
                  <div class="separator-line bg-deep-red margin-seventeen no-margin-bottom margin-lr-auto"></div>
                </div>
              </div>
            </div>
          </article>
          <!-- end news item -->
        <?php endforeach; ?>
        <?php unset($row);?>

      </div>
    </div>
  </section>




<?php if (0): ?>
                <h1 class="veriasist double header"><span><?php echo '<small class="veriasist label">' . $pager->items_total . '</small> ' .Lang::$word->_MOD_AM_ARTFOUND;?></span></h1>
                <?php if($bio):?>
                <div class="veriasist secondary segment">
                  <?php if($bio->avatar):?>
                  <img src="<?php echo UPLOADURL;?>avatars/<?php echo $bio->avatar;?>" alt="<?php echo $bio->username;?>" class="veriasist left floated avatar image">
                  <?php else:?>
                  <img src="<?php echo UPLOADURL;?>avatars/blank.png" alt="<?php echo $bio->username;?>" class="veriasist left floated avatar image">
                  <?php endif;?>
                  <?php echo $bio->info;?></div>
                <div class="veriasist small space divider"></div>
                <?php endif;?>
                <div id="author-data" class="relative">
                  <?php foreach ($latestrow as $row):?>
                  <div class="item">
                    <h5><a href="<?php echo doUrl(false, $row->slug, "news-item");?>" class="inverted"><?php echo $row->atitle;?></a></h5>
                    <div class="veriasist small divider"></div>
                    <div class="veriasist small horizontal list">
                      <div class="item"> <i class="icon sitemap"></i> <a href="<?php echo doUrl(false, $row->catslug, "news-cat");?>" class="inverted"><?php echo $row->catname;?></a> </div>
                      <div class="item"> <i class="icon chat"></i> <?php echo $row->totalcomments;?> <?php echo Lang::$word->_MOD_AM_COMMENTS;?></div>
                    </div>
                    <div class="veriasist small divider"></div>
                    <?php $desc = cleanSanitize($row->{'short_desc'.Lang::$lang});?>
                    <div class="vspace"><?php echo truncate($desc,250);?></div>
                  </div>
                  <?php endforeach;?>
                </div>
                <?php unset($row);?>
                <div id="pagination" class="content-center"><?php echo $pager->display_pages();?></div>
                <script type="text/javascript">
                // <![CDATA[
                $(document).ready(function () {
                  $('#author-data').waitForImages(function () {
                    $('#author-data').Grid({
                      inner: 14,
                      outer: 0,
                      cols: Math.round(1200 / 3)
                    });
                  });
                });
                // ]]>
                </script>
<?php endif ?>

<?php endif;?>
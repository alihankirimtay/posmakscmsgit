<?php
  /**
   * Breadcrumbs
   *
   * @package VeriAsist
   * @author veriasist.com
   * @copyright 2014
   * @version $Id: crumbs.php, v4.00 2014-04-20 10:12:05 gewa Exp $
   */
  if (!defined("_VALID_PHP"))
      die('Direct access to this location is not allowed.');

  require_once("admin_class.php");
?>
<?php
  $length = count($this->_url);
  switch ($length) {
      case 3:
          $nav = '<a href="' . doUrl(false, false, "portfolio") . '" class="section">' . $this->moduledata->{'title' . Lang::$lang} . '</a>';
          $nav .= '<span class="divider"></span>';
          $nav .= '<div class="active section">' . $this->moduledata->mod->{'title' . Lang::$lang} . '</div>';
          break;
		  
      case 2:
          $nav = '<a href="' . doUrl(false, false, "portfolio") . '" class="section">' . $this->moduledata->{'title' . Lang::$lang} . '</a>';
          $nav .= '<span class="divider"></span>';
          $nav .= '<div class="active section">' . $this->moduledata->mod->{'title' . Lang::$lang} . '</div>';
          break;

      default:
          $nav = '<div class="active section">' . $this->moduledata->{'title' . Lang::$lang} . '</div>';
          break;
  }
?>
<?php
  /**
   * Language Data Add
   *
   * @package VeriAsist
   * @author veriasist.com
   * @copyright 2014
   * @version $Id: lang-add.php, v4.00 2014-04-20 10:12:05 gewa Exp $
   */
  if (!defined("_VALID_PHP"))
      die('Direct access to this location is not allowed.');
?>
<?php
  self::$db->query("LOCK TABLES mod_projects WRITE");
  self::$db->query("ALTER TABLE mod_projects ADD COLUMN title_$flag_id VARCHAR(100) NOT NULL AFTER title_en");
  self::$db->query("ALTER TABLE mod_projects ADD COLUMN short_desc_$flag_id TEXT AFTER short_desc_en");
  self::$db->query("ALTER TABLE mod_projects ADD COLUMN detail_$flag_id TEXT AFTER detail_en");
  self::$db->query("ALTER TABLE mod_projects ADD COLUMN body_$flag_id TEXT AFTER body_en");
  self::$db->query("ALTER TABLE mod_projects ADD COLUMN result_$flag_id TEXT AFTER result_en");
  self::$db->query("ALTER TABLE mod_projects ADD COLUMN metakey_$flag_id VARCHAR(200) NOT NULL AFTER metakey_en");
  self::$db->query("ALTER TABLE mod_projects ADD COLUMN metadesc_$flag_id TEXT AFTER metadesc_en");
  self::$db->query("UNLOCK TABLES");

  if ($mod_projects = self::$db->fetch_all("SELECT * FROM mod_projects")) {
      foreach ($mod_projects as $row) {
          $data['title_' . $flag_id] = $row->title_en;
          $data['short_desc_' . $flag_id] = $row->short_desc_en;
          $data['detail_' . $flag_id] = $row->detail_en;
          $data['body_' . $flag_id] = $row->body_en;
          $data['metakey_' . $flag_id] = $row->metakey_en;
          $data['metadesc_' . $flag_id] = $row->metadesc_en;
          self::$db->update("mod_projects", $data, "id = " . $row->id);
      }
      unset($data, $row);
  }

  self::$db->query("LOCK TABLES mod_projects_category WRITE");
  self::$db->query("ALTER TABLE mod_projects_category ADD COLUMN title_$flag_id VARCHAR(100) NOT NULL AFTER title_en");
  self::$db->query("ALTER TABLE mod_projects_category ADD COLUMN metakey_$flag_id VARCHAR(100) NOT NULL AFTER metakey_en");
  self::$db->query("ALTER TABLE mod_projects_category ADD COLUMN metadesc_$flag_id TINYTEXT AFTER metadesc_en");
  self::$db->query("UNLOCK TABLES");

  if ($mod_projects_category = self::$db->fetch_all("SELECT * FROM mod_projects_category")) {
      foreach ($mod_projects_category as $row) {
          $data = array(
		  'title_' . $flag_id => $row->title_en,
		  'metakey_' . $flag_id => $row->metakey_en,
		  'metadesc_' . $flag_id => $row->metadesc_en
		  );
          self::$db->update("mod_projects_category", $data, "id = " . $row->id);
      }
      unset($data, $row);
  }
?>
<?php
  /**
   * Projects
   *
   * @package VeriAsist
   * @author veriasist.com
   * @copyright 2014
   * @version $Id: admin.php, v4.00 2014-04-20 10:12:05 gewa Exp $
   */
  if (!defined("_VALID_PHP"))
      die('Direct access to this location is not allowed.');
  
  if(!$user->getAcl("projects")): print $core->msgAlert(_CG_ONLYADMIN, false); return; endif;
  
  Registry::set('Projects', new Projects());
?>
<?php switch(Filter::$maction): case "edit": ?>
<?php $row = Core::getRowById(Projects::mTable, Filter::$id);?>
<?php $catrow = Registry::get("Projects")->getCategories();?>
<div class="veriasist icon heading message blue"> <a class="helper veriasist top right info corner label" data-help="projects"><i class="icon help"></i></a> <i class="puzzle piece icon"></i>
  <div class="content">
    <div class="header"> <?php echo Lang::$word->_MOD_WRKS_TITLE4;?> </div>
    <div class="veriasist breadcrumb"><i class="icon home"></i> <a href="index.php" class="section"><?php echo Lang::$word->_N_DASH;?></a>
      <div class="divider"> / </div>
      <a href="index.php?do=modules" class="section"><?php echo Lang::$word->_N_MODS;?></a>
      <div class="divider"> / </div>
      <a href="index.php?do=modules&amp;action=config&amp;modname=projects" class="section"><?php echo $content->getModuleName(Filter::$modname);?></a>
      <div class="divider"> / </div>
      <div class="active section"><?php echo Lang::$word->_MOD_WRKS_TITLE1;?></div>
    </div>
  </div>
</div>
<div class="veriasist-large-content">
  <div class="veriasist message"><?php echo Core::langIcon();?><?php echo Lang::$word->_MOD_WRKS_INFO1. Lang::$word->_REQ1 . '<i class="icon asterisk"></i>' . Lang::$word->_REQ2;?></div>
  <div class="veriasist form segment">
    <div class="veriasist header"><?php echo Lang::$word->_MOD_WRKS_SUBTITLE1 . $row->{'title'.Lang::$lang};?></div>
    <div class="veriasist double fitted divider"></div>
    <form id="veriasist_form" name="veriasist_form" method="post">
      <div class="two fields">
        <div class="field">
          <label><?php echo Lang::$word->_MOD_WRKS_NAME;?></label>
          <label class="input"><i class="icon-append icon asterisk"></i>
            <input type="text" value="<?php echo $row->{'title'.Lang::$lang};?>" name="title<?php echo Lang::$lang;?>">
          </label>
        </div>
        <div class="field">
          <label><?php echo Lang::$word->_MOD_WRKS_SLUG;?></label>
          <label class="input">
            <input type="text" value="<?php echo $row->slug;?>" name="slug">
          </label>
        </div>
      </div>
      <div class="two fields">
        <div class="field">
          <label><?php echo Lang::$word->_MOD_WRKS_CATEGORY;?></label>
          <select name="cid">
            <option value=""><?php echo Lang::$word->_MOD_WRKS_CATEGORY_SEL;?></option>
            <?php if($catrow):?>
            <?php foreach($catrow as $crow):?>
            <?php $sel = ($crow->id == $row->cid) ? ' selected="selected"' : '' ;?>
            <option value="<?php echo $crow->id;?>"<?php echo $sel;?>><?php echo $crow->{'title'.LANG::$lang};?></option>
            <?php endforeach;?>
            <?php unset($crow);?>
            <?php endif;?>
          </select>
        </div>
        <div class="field">
          <?php $module_data = $row->gallery;?>
          <?php include(BASEPATH . "sistemyonetimi/modules/gallery/config.php");?>
        </div>
      </div>
      <div class="two fields">
        <div class="field">
          <label><?php echo Lang::$word->_MOD_WRKS_PIMAGE;?></label>
          <label class="input">
            <input type="file" name="thumb" class="filefield">
          </label>
        </div>
        <div class="field">
          <div class="two fields">
            <div class="field">
              <label><?php echo Lang::$word->_MOD_WRKS_PIMAGEPP;?></label>
              <div class="veriasist small image"> <a class="lightbox" href="<?php echo SITEURL . '/' . Projects::imagepath . $row->thumb;?>"><img src="<?php echo SITEURL . '/' . Projects::imagepath . $row->thumb;?>" alt="<?php echo $row->thumb;?>"></a> </div>
            </div>
            <div class="field">
              <label><?php echo Lang::$word->_MOD_AM_LAYOUT;?></label>
              <div class="inline-group">
                <label class="radio">
                <input name="layout" type="radio" value="t" <?php getChecked($row->layout, 't'); ?>>
                  <i></i><img src="modules/projects/images/top.png" alt="" data-content="<?php echo Lang::$word->_MOD_AM_LAYOUT3;?>">
                </label>
                <label class="radio">
                  <input name="layout" type="radio" value="l" <?php getChecked($row->layout, 'l'); ?>>
                  <i></i><img src="modules/projects/images/left.png" alt="" data-content="<?php echo Lang::$word->_MOD_AM_LAYOUT1;?>">
                </label>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="two fields">
        <div class="field">
          <label><?php echo Lang::$word->_MOD_WRKS_RWEB;?></label>
          <label class="input">
            <input type="text" value="<?php echo $row->www;?>" name="www">
          </label>
        </div>
        <div class="field">
          <label><?php echo Lang::$word->_MOD_WRKS_WEB_TEXT;?></label>
          <label class="input">
            <input type="text" value="<?php echo $row->web_text;?>" name="web_text">
          </label>
        </div>
      </div>
      <div class="two fields" style="display: none;">
        <div class="field">
          <label><?php echo Lang::$word->_MOD_WRKS_RLOC;?></label>
          <label class="input">
            <input type="text" value="<?php echo $row->location;?>" name="location">
          </label>
        </div>
        <div class="field">
          <label><?php echo Lang::$word->_MOD_WRKS_RCLIENT;?></label>
          <label class="input">
            <input type="text" value="<?php echo $row->client;?>" name="client">
          </label>
        </div>
      </div>
      <div class="field">
        <label><?php echo Lang::$word->_MOD_WRKS_SHORTDESC;?></label>
        <textarea name="short_desc<?php echo LANG::$lang;?>"><?php echo $row->{'short_desc'.Lang::$lang};?></textarea>
      </div>
      <div class="field">
        <label><?php echo Lang::$word->_MOD_WRKS_PSTUDY;?></label>
        <textarea class="plugpost" name="detail<?php echo Lang::$lang;?>"><?php echo Filter::out_url($row->{'detail'.Lang::$lang});?></textarea>
      </div>
      <div class="field">
        <label><?php echo Lang::$word->_MOD_WRKS_PAPPROACH;?></label>
        <textarea class="plugpost" name="body<?php echo LANG::$lang;?>"><?php echo Filter::out_url($row->{'body'.Lang::$lang});?></textarea>
      </div>
      <div class="field">
        <label><?php echo Lang::$word->_MOD_WRKS_RESULTS;?></label>
        <textarea class="plugpost" name="result<?php echo Lang::$lang;?>"><?php echo Filter::out_url($row->{'result'.Lang::$lang});?></textarea>
      </div>
      <div class="two fields">
        <div class="field">
          <label><?php echo Lang::$word->_METAKEYS;?></label>
          <textarea name="metakey<?php echo Lang::$lang;?>"><?php echo $row->{'metakey'.Lang::$lang};?></textarea>
        </div>
        <div class="field">
          <label><?php echo Lang::$word->_METADESC;?></label>
          <textarea name="metadesc<?php echo Lang::$lang;?>"><?php echo $row->{'metadesc'.Lang::$lang};?></textarea>
        </div>
      </div>
      <div class="field">
        <div class="field">
          <label>Modül Ana Ekranında Göster</label>
          <div class="inline-group">
            <label class="radio">
              <input name="selected" type="radio" value="1" <?php getChecked($row->selected, 1); ?> >
              <i></i>Evet</label>
            <label class="radio">
              <input name="selected" type="radio" value="0" <?php getChecked($row->selected, 0); ?> >
              <i></i>Hayır</label>
          </div>
        </div>
      </div>
      <div class="veriasist double fitted divider"></div>
      <button type="button" name="dosubmit" class="veriasist positive button"><?php echo Lang::$word->_MOD_WRKS_UPDATE;?></button>
      <a href="index.php?do=modules&amp;action=config&amp;modname=projects" class="veriasist basic button"><?php echo Lang::$word->_CANCEL;?></a>
      <input name="id" type="hidden" value="<?php echo Filter::$id;?>">
      <input name="processFolio" type="hidden" value="1">
    </form>
  </div>
  <div id="msgholder"></div>
</div>
<?php break;?>
<?php case"add": ?>
<?php $catrow = Registry::get("Projects")->getCategories();?>
<div class="veriasist icon heading message blue"> <a class="helper veriasist top right info corner label" data-help="projects"><i class="icon help"></i></a> <i class="puzzle piece icon"></i>
  <div class="content">
    <div class="header"> <?php echo Lang::$word->_MOD_WRKS_TITLE4;?> </div>
    <div class="veriasist breadcrumb"><i class="icon home"></i> <a href="index.php" class="section"><?php echo Lang::$word->_N_DASH;?></a>
      <div class="divider"> / </div>
      <a href="index.php?do=modules" class="section"><?php echo Lang::$word->_N_MODS;?></a>
      <div class="divider"> / </div>
      <a href="index.php?do=modules&amp;action=config&amp;modname=projects" class="section"><?php echo $content->getModuleName(Filter::$modname);?></a>
      <div class="divider"> / </div>
      <div class="active section"><?php echo Lang::$word->_MOD_WRKS_TITLE2;?></div>
    </div>
  </div>
</div>
<div class="veriasist-large-content">
  <div class="veriasist message"><?php echo Core::langIcon();?><?php echo Lang::$word->_MOD_WRKS_INFO2. Lang::$word->_REQ1 . '<i class="icon asterisk"></i>' . Lang::$word->_REQ2;?></div>
  <div class="veriasist form segment">
    <div class="veriasist header"><?php echo Lang::$word->_MOD_WRKS_SUBTITLE2;?></div>
    <div class="veriasist double fitted divider"></div>
    <form id="veriasist_form" name="veriasist_form" method="post">
      <div class="two fields">
        <div class="field">
          <label><?php echo Lang::$word->_MOD_WRKS_NAME;?></label>
          <label class="input"><i class="icon-append icon asterisk"></i>
            <input type="text" placeholder="<?php echo Lang::$word->_MOD_WRKS_NAME;?>" name="title<?php echo Lang::$lang;?>">
          </label>
        </div>
        <div class="field">
          <label><?php echo Lang::$word->_MOD_WRKS_SLUG;?></label>
          <label class="input">
            <input type="text" placeholder="<?php echo Lang::$word->_MOD_WRKS_SLUG;?>" name="slug">
          </label>
        </div>
      </div>
      <div class="two fields">
        <div class="field">
          <label><?php echo Lang::$word->_MOD_WRKS_CATEGORY;?></label>
          <select name="cid">
            <option value=""><?php echo Lang::$word->_MOD_WRKS_CATEGORY_SEL;?></option>
            <?php if($catrow):?>
            <?php foreach($catrow as $crow):?>
            <option value="<?php echo $crow->id;?>"><?php echo $crow->{'title'.LANG::$lang};?></option>
            <?php endforeach;?>
            <?php unset($crow);?>
            <?php endif;?>
          </select>
        </div>
        <div class="field">
          <?php $module_data = 0;?>
          <?php include(BASEPATH . "sistemyonetimi/modules/gallery/config.php");?>
        </div>
      </div>
      <div class="two fields">
        <div class="field">
          <label><?php echo Lang::$word->_MOD_WRKS_PIMAGE;?></label>
          <label class="input">
            <input type="file" name="thumb" class="filefield">
          </label>
        </div>
        <div class="field">
          <label><?php echo Lang::$word->_MOD_AM_LAYOUT;?></label>
          <div class="inline-group">
            <label class="radio">
              <input name="layout" type="radio" value="t" checked="checked">
              <i></i><img src="modules/projects/images/top.png" alt="" data-content="<?php echo Lang::$word->_MOD_AM_LAYOUT3;?>">
            </label>
            <label class="radio">
              <input name="layout" type="radio" value="l">
              <i></i><img src="modules/projects/images/left.png" alt="" data-content="<?php echo Lang::$word->_MOD_AM_LAYOUT1;?>">
            </label>
          </div>
        </div>
      </div>
      <div class="two fields">
        <div class="field">
          <label><?php echo Lang::$word->_MOD_WRKS_RWEB;?></label>
          <label class="input">
            <input type="text" placeholder="<?php echo Lang::$word->_MOD_WRKS_RWEB;?>" name="www" value="http://">
          </label>
        </div>
        <div class="field">
          <label><?php echo Lang::$word->_MOD_WRKS_WEB_TEXT;?></label>
          <label class="input">
            <input type="text" value="" name="web_text" readonly="readonly">
          </label>
        </div>
      </div>
      <div class="two fields" style="display: none;">
        <div class="field">
          <label><?php echo Lang::$word->_MOD_WRKS_RLOC;?></label>
          <label class="input">
            <input type="text" placeholder="<?php echo Lang::$word->_MOD_WRKS_RLOC;?>" name="location">
          </label>
        </div>
        <div class="field">
          <label><?php echo Lang::$word->_MOD_WRKS_RCLIENT;?></label>
          <label class="input">
            <input type="text" placeholder="<?php echo Lang::$word->_MOD_WRKS_RCLIENT;?>" name="client">
          </label>
        </div>
      </div>
      <div class="field">
        <label><?php echo Lang::$word->_MOD_WRKS_SHORTDESC;?></label>
        <label class="textarea"><i class="icon-append icon asterisk"></i>
          <textarea placeholder="<?php echo Lang::$word->_MOD_WRKS_SHORTDESC;?>" name="short_desc<?php echo LANG::$lang;?>"></textarea>
        </label>
      </div>
      <div class="field">
        <label><?php echo Lang::$word->_MOD_WRKS_PSTUDY;?></label>
        <textarea placeholder="<?php echo Lang::$word->_MOD_WRKS_PSTUDY;?>" class="plugpost" name="detail<?php echo Lang::$lang;?>"></textarea>
      </div>
      <div class="field">
        <label><?php echo Lang::$word->_MOD_WRKS_PAPPROACH;?></label>
        <textarea placeholder="<?php echo Lang::$word->_MOD_WRKS_PAPPROACH;?>" class="plugpost" name="body<?php echo LANG::$lang;?>"></textarea>
      </div>
      <div class="field">
        <label><?php echo Lang::$word->_MOD_WRKS_RESULTS;?></label>
        <textarea placeholder="<?php echo Lang::$word->_MOD_WRKS_RESULTS;?>" class="plugpost" name="result<?php echo Lang::$lang;?>"></textarea>
      </div>
      <div class="two fields">
        <div class="field">
          <label><?php echo Lang::$word->_METAKEYS;?></label>
          <textarea placeholder="<?php echo Lang::$word->_METAKEYS;?>" name="metakey<?php echo Lang::$lang;?>"></textarea>
        </div>
        <div class="field">
          <label><?php echo Lang::$word->_METADESC;?></label>
          <textarea placeholder="<?php echo Lang::$word->_METADESC;?>" name="metadesc<?php echo Lang::$lang;?>"></textarea>
        </div>
      </div>
      <div class="two fields">
        <div class="field">
          <label>Modül Ana Ekranında Göster</label>
          <div class="inline-group">
            <label class="radio">
              <input name="selected" type="radio" value="1" checked="checked">
              <i></i>Evet</label>
            <label class="radio">
              <input name="selected" type="radio" value="0">
              <i></i>Hayır</label>
          </div>
        </div>
      </div>
      <div class="veriasist double fitted divider"></div>
      <button type="button" name="dosubmit" class="veriasist positive button"><?php echo Lang::$word->_MOD_WRKS_ADD;?></button>
      <a href="index.php?do=modules&amp;action=config&amp;modname=projects" class="veriasist basic button"><?php echo Lang::$word->_CANCEL;?></a>
      <input name="processFolio" type="hidden" value="1">
    </form>
  </div>
  <div id="msgholder"></div>
</div>
<?php break;?>
<?php case"config": ?>
<?php $row = Registry::get("Projects");?>
<div class="veriasist icon heading message blue"> <a class="helper veriasist top right info corner label" data-help="pfconfig"><i class="icon help"></i></a> <i class="puzzle piece icon"></i>
  <div class="content">
    <div class="header"> <?php echo Lang::$word->_MOD_WRKS_TITLE4;?> </div>
    <div class="veriasist breadcrumb"><i class="icon home"></i> <a href="index.php" class="section"><?php echo Lang::$word->_N_DASH;?></a>
      <div class="divider"> / </div>
      <a href="index.php?do=modules" class="section"><?php echo Lang::$word->_N_MODS;?></a>
      <div class="divider"> / </div>
      <a href="index.php?do=modules&amp;action=config&amp;modname=projects" class="section"><?php echo $content->getModuleName(Filter::$modname);?></a>
      <div class="divider"> / </div>
      <div class="active section"><?php echo Lang::$word->_MOD_WRKS_TITLE3;?></div>
    </div>
  </div>
</div>
<div class="veriasist-large-content">
  <div class="veriasist message"><?php echo Core::langIcon();?><?php echo Lang::$word->_MOD_WRKS_INFO3. Lang::$word->_REQ1 . '<i class="icon asterisk"></i>' . Lang::$word->_REQ2;?></div>
  <div class="veriasist form segment">
    <div class="veriasist header"><?php echo Lang::$word->_MOD_WRKS_SUBTITLE8;?></div>
    <div class="veriasist double fitted divider"></div>
    <form id="veriasist_form" name="veriasist_form" method="post">
      <div class="three fields">
        <div class="field">
          <label><?php echo Lang::$word->_MOD_WRKS_COLS;?></label>
          <label class="input">
            <input type="text" class="slrange" value="<?php echo $row->cols;?>" name="cols">
          </label>
        </div>
        <div class="field">
          <label><?php echo Lang::$word->_MOD_WRKS_ITEMPP;?></label>
          <label class="input">
            <input type="text" class="slrange" value="<?php echo $row->ipp;?>" name="ipp">
          </label>
        </div>
        <div class="field">
          <label><?php echo Lang::$word->_MOD_WRKS_ITEMPPF;?></label>
          <label class="input">
            <input type="text" class="slrange" value="<?php echo $row->fpp;?>" name="fpp">
          </label>
        </div>
      </div>
      <div class="veriasist double fitted divider"></div>
      <button type="button" name="dosubmit" class="veriasist positive button"><?php echo Lang::$word->_MOD_WRKS_UPDATEC;?></button>
      <a href="index.php?do=modules&amp;action=config&amp;modname=projects" class="veriasist basic button"><?php echo Lang::$word->_CANCEL;?></a>
      <input name="processConfig" type="hidden" value="1">
    </form>
  </div>
  <div id="msgholder"></div>
</div>
<script type="text/javascript"> 
// <![CDATA[
$(document).ready(function () {
    $("input[name=cols]").ionRangeSlider({
		min: 2,
		max: 6,
        step: 1,
		postfix: " col",
        type: 'single',
        hasGrid: true
    });
	
    $("input[name=ipp], input[name=fpp]").ionRangeSlider({
		min: 5,
		max: 20,
        step: 1,
		postfix: " itm",
        type: 'single',
        hasGrid: true
    });
});
// ]]>
</script>
<?php break;?>
<?php case"category": ?>
<?php $catrow = Registry::get("Projects")->getCategories();?>
<div class="veriasist icon heading message blue"> <i class="puzzle piece icon"></i>
  <div class="content">
    <div class="header"> <?php echo Lang::$word->_MOD_WRKS_TITLE4;?> </div>
    <div class="veriasist breadcrumb"><i class="icon home"></i> <a href="index.php" class="section"><?php echo Lang::$word->_N_DASH;?></a>
      <div class="divider"> / </div>
      <a href="index.php?do=modules" class="section"><?php echo Lang::$word->_N_MODS;?></a>
      <div class="divider"> / </div>
      <a href="index.php?do=modules&amp;action=config&amp;modname=projects" class="section"><?php echo $content->getModuleName(Filter::$modname);?></a>
      <div class="divider"> / </div>
      <div class="active section"><?php echo Lang::$word->_MOD_WRKS_TITLE5;?></div>
    </div>
  </div>
</div>
<div class="veriasist-large-content">
  <div class="veriasist message"><?php echo Core::langIcon();?><?php echo Lang::$word->_MOD_WRKS_INFO5;?></div>
  <div class="veriasist segment"> <a class="veriasist icon warning button push-right" href="<?php echo Core::url("modules", "catadd");?>"><i class="icon add"></i> <?php echo Lang::$word->_MOD_WRKS_CADD;?></a>
    <div class="veriasist header"><?php echo Lang::$word->_MOD_WRKS_SUBTITLE5;?></div>
    <div class="veriasist fitted divider"></div>
    <table class="veriasist sortable table">
      <thead>
        <tr>
          <th class="disabled"></th>
          <th class="left"><?php echo Lang::$word->_MOD_WRKS_CATNAME;?></th>
          <th><?php echo Lang::$word->_MOD_WRKS_POS;?></th>
          <th class="disabled"><?php echo Lang::$word->_ACTIONS;?></th>
        </tr>
      </thead>
      <tbody>
        <?php if(!$catrow):?>
        <tr>
          <td colspan="4"><?php echo Filter::msgSingleAlert(Lang::$word->_MOD_WRKS_NOCATS);?></td>
        </tr>
        <?php else:?>
        <?php foreach ($catrow as $row):?>
        <tr id="node-<?php echo $row->id;?>">
          <td class="id-handle"><i class="icon reorder"></i></td>
          <td data-sort="string"><?php echo $row->{'title'.Lang::$lang};?></td>
          <td data-sort="int"><span class="veriasist black label"><?php echo $row->position;?></span></td>
          <td><a href="<?php echo Core::url("modules", "catedit", $row->id);?>"><i class="rounded inverted success icon pencil link"></i></a> <a class="delete" data-title="<?php echo Lang::$word->_DELETE.' '.Lang::$word->_MOD_WRKS_CATEGORY;?>" data-option="deleteCategory" data-id="<?php echo $row->id;?>" data-name="<?php echo $row->{'title'.Lang::$lang};?>"><i class="rounded danger inverted remove icon link"></i></a></td>
        </tr>
        <?php endforeach;?>
        <?php unset($row);?>
        <?php endif;?>
      </tbody>
    </table>
  </div>
</div>
<script type="text/javascript"> 
// <![CDATA[
$(document).ready(function () {
    $(".veriasist.table tbody").sortable({
        helper: 'clone',
        handle: '.id-handle',
        placeholder: 'placeholder',
        opacity: .6,
        update: function (event, ui) {
            serialized = $(".veriasist.table tbody").sortable('serialize');
            $.ajax({
                type: "POST",
                url: "modules/projects/controller.php?sortcats",
                data: serialized,
                success: function (msg) {}
            });
        }
    });
});
// ]]>
</script>
<?php break;?>
<?php case"catedit": ?>
<?php $row = Core::getRowById(Projects::cTable, Filter::$id);?>
<div class="veriasist icon heading message blue"> <i class="puzzle piece icon"></i>
  <div class="content">
    <div class="header"> <?php echo Lang::$word->_MOD_WRKS_TITLE4;?> </div>
    <div class="veriasist breadcrumb"><i class="icon home"></i> <a href="index.php" class="section"><?php echo Lang::$word->_N_DASH;?></a>
      <div class="divider"> / </div>
      <a href="index.php?do=modules" class="section"><?php echo Lang::$word->_N_MODS;?></a>
      <div class="divider"> / </div>
      <a href="index.php?do=modules&amp;action=config&amp;modname=projects" class="section"><?php echo $content->getModuleName(Filter::$modname);?></a>
      <div class="divider"> / </div>
      <a href="<?php echo Core::url("modules", "category");?>" class="section"><?php echo Lang::$word->_MOD_WRKS_TITLE5;?></a>
      <div class="divider"> / </div>
      <div class="active section"><?php echo Lang::$word->_MOD_WRKS_TITLE6;?></div>
    </div>
  </div>
</div>
<div class="veriasist-large-content">
  <div class="veriasist message"><?php echo Core::langIcon();?><?php echo Lang::$word->_MOD_WRKS_INFO6. Lang::$word->_REQ1 . '<i class="icon asterisk"></i>' . Lang::$word->_REQ2;?></div>
  <div class="veriasist form segment">
    <div class="veriasist header"><?php echo Lang::$word->_MOD_WRKS_SUBTITLE6 . $row->{'title'.Lang::$lang};?></div>
    <div class="veriasist double fitted divider"></div>
    <form id="veriasist_form" name="veriasist_form" method="post">
      <div class="two fields">
        <div class="field">
          <label><?php echo Lang::$word->_MOD_WRKS_CATNAME;?></label>
          <label class="input"><i class="icon-append icon asterisk"></i>
            <input type="text" value="<?php echo $row->{'title'.Lang::$lang};?>" name="title<?php echo Lang::$lang;?>">
          </label>
        </div>
        <div class="field">
          <label><?php echo Lang::$word->_MOD_WRKS_CATSLUG;?></label>
          <input type="text" value="<?php echo $row->slug;?>" name="slug">
        </div>
      </div>
      <div class="two fields">
        <div class="field">
          <label><?php echo Lang::$word->_METAKEYS;?></label>
          <textarea name="metakey<?php echo Lang::$lang;?>"><?php echo $row->{'metakey'.Lang::$lang};?></textarea>
        </div>
        <div class="field">
          <label><?php echo Lang::$word->_METADESC;?></label>
          <textarea name="metadesc<?php echo Lang::$lang;?>"><?php echo $row->{'metadesc'.Lang::$lang};?></textarea>
        </div>
      </div>
      <div class="veriasist double fitted divider"></div>
      <button type="button" name="dosubmit" class="veriasist positive button"><?php echo Lang::$word->_MOD_WRKS_CAUPDATE;?></button>
      <a href="<?php echo Core::url("modules", "category");?>" class="veriasist basic button"><?php echo Lang::$word->_CANCEL;?></a>
      <input name="id" type="hidden" value="<?php echo Filter::$id;?>">
      <input name="processCategory" type="hidden" value="1">
    </form>
  </div>
  <div id="msgholder"></div>
</div>
<?php break;?>
<?php case"catadd": ?>
<div class="veriasist icon heading message blue"> <i class="puzzle piece icon"></i>
  <div class="content">
    <div class="header"> <?php echo Lang::$word->_MOD_WRKS_TITLE4;?> </div>
    <div class="veriasist breadcrumb"><i class="icon home"></i> <a href="index.php" class="section"><?php echo Lang::$word->_N_DASH;?></a>
      <div class="divider"> / </div>
      <a href="index.php?do=modules" class="section"><?php echo Lang::$word->_N_MODS;?></a>
      <div class="divider"> / </div>
      <a href="index.php?do=modules&amp;action=config&amp;modname=projects" class="section"><?php echo $content->getModuleName(Filter::$modname);?></a>
      <div class="divider"> / </div>
      <a href="<?php echo Core::url("modules", "category");?>" class="section"><?php echo Lang::$word->_MOD_WRKS_TITLE5;?></a>
      <div class="divider"> / </div>
      <div class="active section"><?php echo Lang::$word->_MOD_WRKS_TITLE7;?></div>
    </div>
  </div>
</div>
<div class="veriasist-large-content">
  <div class="veriasist message"><?php echo Core::langIcon();?><?php echo Lang::$word->_MOD_WRKS_INFO7. Lang::$word->_REQ1 . '<i class="icon asterisk"></i>' . Lang::$word->_REQ2;?></div>
  <div class="veriasist form segment">
    <div class="veriasist header"><?php echo Lang::$word->_MOD_WRKS_SUBTITLE7;?></div>
    <div class="veriasist double fitted divider"></div>
    <form id="veriasist_form" name="veriasist_form" method="post">
      <div class="two fields">
        <div class="field">
          <label><?php echo Lang::$word->_MOD_WRKS_CATNAME;?></label>
          <label class="input"><i class="icon-append icon asterisk"></i>
            <input type="text" placeholder="<?php echo Lang::$word->_MOD_WRKS_CATNAME;?>" name="title<?php echo Lang::$lang;?>">
          </label>
        </div>
        <div class="field">
          <label><?php echo Lang::$word->_MOD_WRKS_CATSLUG;?></label>
          <input type="text" placeholder="<?php echo Lang::$word->_MOD_WRKS_CATSLUG;?>" name="slug">
        </div>
      </div>
      <div class="two fields">
        <div class="field">
          <label><?php echo Lang::$word->_METAKEYS;?></label>
          <textarea placeholder="<?php echo Lang::$word->_METAKEYS;?>" name="metakey<?php echo Lang::$lang;?>"></textarea>
        </div>
        <div class="field">
          <label><?php echo Lang::$word->_METADESC;?></label>
          <textarea placeholder="<?php echo Lang::$word->_METADESC;?>" name="metadesc<?php echo Lang::$lang;?>"></textarea>
        </div>
      </div>
      <div class="veriasist double fitted divider"></div>
      <button type="button" name="dosubmit" class="veriasist positive button"><?php echo Lang::$word->_MOD_WRKS_CADD;?></button>
      <a href="<?php echo Core::url("modules", "category");?>" class="veriasist basic button"><?php echo Lang::$word->_CANCEL;?></a>
      <input name="processCategory" type="hidden" value="1">
    </form>
  </div>
  <div id="msgholder"></div>
</div>
<?php break;?>
<?php default: ?>
<?php $portrow = Registry::get("Projects")->getProjects();?>
<?php $catrow = Registry::get("Projects")->getCategories();?>
<div class="veriasist icon heading message blue"> <i class="puzzle piece icon"></i>
  <div class="content">
    <div class="header"> <?php echo Lang::$word->_MOD_WRKS_TITLE4;?> </div>
    <div class="veriasist breadcrumb"><i class="icon home"></i> <a href="index.php" class="section"><?php echo Lang::$word->_N_DASH;?></a>
      <div class="divider"> / </div>
      <a href="index.php?do=modules" class="section"><?php echo Lang::$word->_N_MODS;?></a>
      <div class="divider"> / </div>
      <div class="active section"><?php echo $content->getModuleName(Filter::$modname);?></div>
    </div>
  </div>
</div>
<div class="veriasist-large-content">
  <div class="veriasist message"><?php echo Core::langIcon();?><?php echo Lang::$word->_MOD_WRKS_INFO4;?></div>
  <div class="veriasist segment">
    <div class="push-right">
      <div class="veriasist right pointing dropdown icon info button"> <i class="settings icon"></i>
        <div class="menu"> <a class="item" href="<?php echo Core::url("modules", "add");?>"><i class="icon add"></i><?php echo Lang::$word->_MOD_WRKS_SUBTITLE4;?></a> <a class="item" href="<?php echo Core::url("modules", "category");?>"><i class="icon add"></i><?php echo Lang::$word->_MOD_WRKS_CATEGORIES;?></a> <a class="item" href="<?php echo Core::url("modules", "config");?>"><i class="icon setting"></i><?php echo Lang::$word->_MOD_WRKS_CONFIGURE;?></a> </div>
      </div>
    </div>
    <div class="veriasist header"><?php echo Lang::$word->_MOD_WRKS_SUBTITLE3;?></div>
    <div class="veriasist fitted divider"></div>
    <table class="veriasist sortable table">
      <thead>
        <tr>
          <th class="disabled"><i class="icon photo"></i></th>
          <th data-sort="string"><?php echo Lang::$word->_MOD_WRKS_NAME;?></th>
          <th data-sort="string"><?php echo Lang::$word->_MOD_WRKS_CATEGORY;?></th>
          <th data-sort="string">Modül Ekranında Görünür</th>
          <th class="disabled"><?php echo Lang::$word->_ACTIONS;?></th>
        </tr>
      </thead>
      <tbody>
        <?php if(!$portrow):?>
        <tr>
          <td colspan="4"><?php echo Filter::msgSingleAlert(Lang::$word->_MOD_WRKS_NOPROJECTS);?></td>
        </tr>
        <?php else:?>
        <?php foreach ($portrow as $row):?>
        <tr>
          <td><a class="lightbox" href="<?php echo SITEURL . '/' . Projects::imagepath . $row->thumb;?>" title="<?php echo $row->{'title'.LANG::$lang};?>"> <img src="<?php echo SITEURL;?>/thumbmaker.php?src=<?php echo SITEURL . '/' . Projects::imagepath . $row->thumb;?>&amp;w=120&amp;h=60" alt="" class="veriasist image"></a></td>
          <td><?php echo $row->{'title'.LANG::$lang};?></td>
          <td><a class="item" href="<?php echo Core::url("modules", "catedit", $row->cid);?>"><?php echo $row->catname;?></a></td>
          <td><?php echo isActive($row->selected, TRUE); ?></td>
          <td><a href="<?php echo Core::url("modules", "edit", $row->id);?>"><i class="rounded inverted success icon pencil link"></i></a> <a class="delete" data-title="<?php echo Lang::$word->_DELETE.' '.Lang::$word->_MOD_WRKS_PROJECT;?>" data-option="deleteProject" data-id="<?php echo $row->id;?>" data-name="<?php echo $row->{'title' . Lang::$lang};?>"><i class="rounded danger inverted remove icon link"></i></a></td>
        </tr>
        <?php endforeach;?>
        <?php unset($row);?>
        <?php endif;?>
      </tbody>
    </table>
  </div>
</div>
<?php break;?>
<?php endswitch;?>
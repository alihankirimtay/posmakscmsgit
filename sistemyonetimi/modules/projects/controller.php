<?php
  /**
   * Controller
   *
   * @package VeriAsist
   * @author veriasist.com
   * @copyright 2014
   * @version $Id: controller.php, v4.00 2014-04-20 10:12:05 gewa Exp $
   */
  define("_VALID_PHP", true);
  
  require_once("../../init.php");
  if (!$user->is_Admin())
      redirect_to("../../login.php");
  
  require_once("admin_class.php");
  Registry::set('Projects', new Projects());
?>
<?php
  /* == Update Configuration == */
  if (isset($_POST['processConfig'])):
      Registry::get("Projects")->processConfig();
  endif;

  /* == Proccess Project == */
  if (isset($_POST['processFolio'])):
      Registry::get("Projects")->processFolio();
  endif;

  /* == Delete Project == */
  if (isset($_POST['delete']) and $_POST['delete'] == "deleteProject"):
      $title = sanitize($_POST['title']);
      if ($thumb = getValueById("thumb", Projects::mTable, Filter::$id)):
          unlink(BASEPATH . Projects::imagepath . $thumb);
      endif;
      $result = $db->delete(Projects::mTable, "id=" . Filter::$id);

      if ($result):
          $json['type'] = 'success';
          $json['title'] = Lang::$word->_SUCCESS;
          $json['message'] = Lang::$word->_MOD_PF_PROJECT . ' /' . $title . '/ ' . Lang::$word->_DELETED;
          Security::writeLog(Lang::$word->_MOD_PF_PROJECT . ' /' . urldecode($title) . '/ ' . Lang::$word->_DELETED, "", "no", "module");
      else:
          $json['type'] = 'warning';
          $json['title'] = Lang::$word->_ALERT;
          $json['message'] = Lang::$word->_SYSTEM_PROCCESS;
      endif;
      print json_encode($json);
  endif;

  /* == Proccess Category == */
  if (isset($_POST['processCategory'])):
      Registry::get("Projects")->processCategory();
  endif;

  /* == Update Category Order == */
  if (isset($_GET['sortcats'])):
      Projects::updateOrder();
  endif;

  /* == Delete Category == */
  if (isset($_POST['delete']) and $_POST['delete'] == "deleteCategory"):
      $title = sanitize($_POST['title']);
      $result = $db->delete(Projects::cTable, "id=" . Filter::$id);

      if ($result):
          $json['type'] = 'success';
          $json['title'] = Lang::$word->_SUCCESS;
          $json['message'] = Lang::$word->_MOD_PF_CATEGORY . ' /' . $title . '/ ' . Lang::$word->_DELETED;
          Security::writeLog(Lang::$word->_MOD_PF_CATEGORY . ' /' . urldecode($title) . '/ ' . Lang::$word->_DELETED, "", "no", "module");
      else:
          $json['type'] = 'warning';
          $json['title'] = Lang::$word->_ALERT;
          $json['message'] = Lang::$word->_SYSTEM_PROCCESS;
      endif;
      print json_encode($json);
  endif;
?>

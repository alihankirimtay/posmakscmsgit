<?php
  /**
   * Comments Class
   *
   * @package VeriAsist
   * @author veriasist.com
   * @copyright 2014
   * @version $Id: class_admin.php, v4.00 2014-04-20 10:12:05 gewa Exp $
   */
  
  if (!defined("_VALID_PHP"))
      die('Direct access to this location is not allowed.');
  
  class Discounts
  {
	  const mTable = "mod_discounts";

	  private static $db;


      /**
       * Comments::__construct()
       * 
       * @return
       */
      function __construct()
      {
		  $this->getConfig();

		  self::$db = Registry::get("Database");
      }

	  
	  /**
	   * Comments::getConfig()
	   * 
	   * @return
	   */
	  private function getConfig()
	  {
	
		  // $row = INI::read(MODPATH . 'comments/config.ini');
	
		  // $this->username_req = $row->com_config->username_req;
		  // $this->email_req = $row->com_config->show_captcha;
		  // $this->show_captcha = $row->com_config->show_captcha;
		  // $this->show_www = $row->com_config->show_www;
		  // $this->show_username = $row->com_config->show_username;
		  // $this->show_email = $row->com_config->show_email;
		  // $this->auto_approve = $row->com_config->auto_approve;
		  // $this->public_access = $row->com_config->public_access;
		  // $this->notify_new = $row->com_config->notify_new;
		  // $this->sorting = $row->com_config->sorting;
		  // $this->blacklist_words = $row->com_config->blacklist_words;
		  // $this->char_limit = $row->com_config->char_limit;
		  // $this->perpage = $row->com_config->perpage;
		  // $this->dateformat = $row->com_config->dateformat;
	
		  // return ($row) ? $row : 0;
	  }

	  /**
	   * Comments::updateConfig()
	   * 
	   * @return
	   */
	  public function processConfig()
	  {
		  
		  Filter::checkPost('title_tr', Lang::$word->_MOD_DC_FORM_NAME);
		  Filter::checkPost('code', Lang::$word->_MOD_DC_FORM_CODE);
		  Filter::checkPost('price', Lang::$word->_MOD_DC_FORM_PRICE);
		  Filter::checkPost('status', Lang::$word->_MOD_DC_FORM_STATUS);
		  

		  if (empty(Filter::$msgs)) {

		  	$data = [
		  		'title'.Lang::$lang => sanitize($_POST['title'.Lang::$lang]),
		  		'code'	 			=> sanitize($_POST['code']),
		  		'price' 			=> floatval($_POST['price']),
		  		'status' 			=> sanitize($_POST['status'])
		  	];
		  	
		  	(Filter::$id) ? self::$db->update(self::mTable,$data,"id=".Filter::$id) :
		  	$lastid = self::$db->insert(self::mTable , $data);

		  	$message = (Filter::$id) ? Lang::$word->_MOD_DS_PRODUPDATED : Lang::$word->_MOD_DS_PRODADDED;

		  	if(self::$db->affected()){
		  		Security::writeLog($message, "", "no", "module");
				$json['type'] = 'success';
				$json['message'] = Filter::msgOk($message, false);
		  	} else {
		  		$json['type'] = 'info';
		  		$json['message'] = Filter::msgAlert(Lang::$word->_SYSTEM_PROCCESS, false);
		  	}
		  	print json_encode($json);
			   
		  } else {


			  $json['message'] = Filter::msgStatus();
			  print json_encode($json);
		  }
	  }
	  	  
	  /**
	   * Comments::getDiscounts()
	   * 
	   * @return
	   */
	  public function getDiscounts()
	  {			
		  $counter = countEntries(self::mTable);	
		  $pager = Paginator::instance();
		  $pager->items_total = $counter;
		  $pager->default_ipp = Registry::get("Core")->perpage;
		  $pager->paginate();

		  if(isset(Filter::$id)):


		  	$sql = " SELECT d.id, d.title". Lang::$lang ." , d.code , d.price , d.status 
		  	FROM ". self::mTable ." as d WHERE id=". Filter::$id;
		  	$row = Registry::get("Database")->fetch_all($sql);


		  	return ($row) ? $row[0] : 0;
		  else: 
		  	$sql = " SELECT d.id, d.title". Lang::$lang ." , d.code , d.price , d.status 
		  	FROM ". self::mTable ." as d". $pager->limit;
		  	$row = Registry::get("Database")->fetch_all($sql);

		  	return ($row) ? $row : 0;
		  endif;
	  }

	  /**
	   * Comments::getDateFormat()
	   * 
	   * @param bool $selected
	   * @return
	   */
	  public static function getDateFormat($selected = false)
	  {
		  $format = (strtoupper(substr(PHP_OS, 0, 3)) == 'WIN') ? "%#d" : "%e";
		  $arr = array(
			  '%m-%d-%Y' => strftime('%m-%d-%Y') . ' (MM-DD-YYYY)',
			  $format . '-%m-%Y' => strftime($format . '-%m-%Y') . ' (D-MM-YYYY)',
			  '%m-' . $format . '-%y' => strftime('%m-' . $format . '-%y') . ' (MM-D-YY)',
			  $format . '-%m-%y' => strftime($format . '-%m-%y') . ' (D-MMM-YY)',
			  '%d %b %Y' => strftime('%d %b %Y'),
			  '%B %d, %Y %I:%M %p' => strftime('%B %d, %Y %I:%M %p'),
			  '%d %B %Y %I:%M %p' => strftime('%d %B %Y %I:%M %p'),
			  '%B %d, %Y' => strftime('%B %d, %Y'),
			  '%d %B, %Y' => strftime('%d %B, %Y'),
			  '%A %d %B %Y' => strftime('%A %d %B %Y'),
			  '%A %d %B %Y %H:%M' => strftime('%A %d %B %Y %H:%M'),
			  '%a %d, %B' => strftime('%a %d, %B'));
	
		  $html = '';
		  foreach ($arr as $key => $val) {
			  if ($key == $selected) {
				  $html .= "<option selected=\"selected\" value=\"" . $key . "\">" . $val . "</option>\n";
			  } else
				  $html .= "<option value=\"" . $key . "\">" . $val . "</option>\n";
		  }
		  unset($val);
		  return $html;
	  }

	  /**
	   * Comments::censored()
	   *
	   * @param mixed $string
	   * @return
	   */
	  public function censored($string)
	  {
		  $array = explode(",", $this->blacklist_words);
		  reset($array);
	
		  foreach ($array as $row) {
			  $string = preg_replace("`$row`", "***", $string);
		  }
		  unset($row);
		  return $string;
	  }
  
	  /**
	   * Comments::keepTags()
	   *
	   * @param mixed $string
	   * @param mixed $allowtags
	   * @param mixed $allowattributes
	   * @return
	   */
	  public static function keepTags($string, $allowtags = null, $allowattributes = null)
	  {
		  $string = strip_tags($string, $allowtags);
		  if (!is_null($allowattributes)) {
			  if (!is_array($allowattributes))
				  $allowattributes = explode(",", $allowattributes);
			  if (is_array($allowattributes))
				  $allowattributes = implode(")(?<!", $allowattributes);
			  if (strlen($allowattributes) > 0)
				  $allowattributes = "(?<!" . $allowattributes . ")";
			  $string = preg_replace_callback("/<[^>]*>/i", create_function('$matches', 'return preg_replace("/ [^ =]*' . $allowattributes . '=(\"[^\"]*\"|\'[^\']*\')/i", "", $matches[0]);'), $string);
		  }
		  return $string;
	  }
  }
?>
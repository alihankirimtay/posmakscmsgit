<?php
  /**
   * Admin
   *
   * @package VeriAsist
   * @author veriasist.com
   * @copyright 2014
   * @version $Id: admin.php, v4.00 2014-04-20 10:12:05 gewa Exp $
   */
  if (!defined("_VALID_PHP"))
      die('Direct access to this location is not allowed.');

  if(!$user->getAcl("discounts")): print Filter::msgAlert(Lang::$word->_CG_ONLYADMIN); return; endif;

  Registry::set('Discounts', new Discounts());
?>

<?php switch (Filter::$maction): case 'add': ?>

<div class="veriasist icon heading message blue"> 
  
  <a class="helper veriasist top right info corner label" data-help="comments">
    <i class="icon help"></i>
  </a> 
  <i class="puzzle piece icon"></i>
  
  <div class="content">
    
    <div class="header"> <?php echo Lang::$word->_MOD_CM_TITLE3;?> </div>
    <div class="veriasist breadcrumb">
      <i class="icon home"></i> 
      <a href="index.php" class="section">
        <?php echo Lang::$word->_N_DASH;?>    
      </a>
      <div class="divider"> / </div>
      <a href="index.php?do=modules" class="section"><?php echo Lang::$word->_N_MODS;?></a>
      <div class="divider"> / </div>
      <a href="index.php?do=modules&amp;action=config&amp;modname=discounts" class="section"><?php echo $content->getModuleName(Filter::$modname);?></a>
      <div class="divider"> / </div>
      <div class="active section"><?php echo Lang::$word->_MOD_DC_TITLE1;?></div>
    </div>
  </div>
</div>

<div class="veriasist-large-content">

  <div class="veriasist message">
      <?php echo Core::langIcon();?><?php echo Lang::$word->_MOD_DC_INFO1. Lang::$word->_REQ1 . '<i class="icon asterisk"></i>' . Lang::$word->_REQ2;?>

      <div class="veriasist form segment">
        
        <div class="veriasist header"><?php echo Lang::$word->_MOD_DC_SUBTITLE1;?></div>

        <div class="veriasist double fitted divider"></div>

        <form id="veriasist_form" name="veriasist_form" method="post">

           <div class="two fields">
              <div class="field">
                <label><?php echo Lang::$word->_MOD_DC_FORM_NAME;?></label>
                <label class="input"><i class="icon-append icon asterisk"></i>
                  <input type="text" value="" name="title<?= Lang::$lang?>">
                </label>
              </div>

              <div class="field">
                <label><?php echo Lang::$word->_MOD_DC_FORM_CODE;?></label>
                <label class="input"><i class="icon-append icon asterisk"></i>
                  <input type="text" value="" name="code">
                </label>
              </div>
           </div>

           <div class="two fields">
            <div class="field">
              <label><?php echo Lang::$word->_MOD_DC_FORM_PRICE;?></label>
              <label class="input"><i class="icon-append icon asterisk"></i>
                <label class="input">
                  <i class="icon-prepend icon dollar"></i>
                  <i class="icon-append icon asterisk"></i>
                  <input type="text" value="" name="price">
                </label>
              </label>
            </div>
             <div class="field">
                <label><?php echo Lang::$word->_MOD_DC_FORM_STATUS;?></label>
                <div class="inline-group">
                  <label class="radio">
                    <input name="status" type="radio" value="1">
                    <i></i><?php echo Lang::$word->_ACTIVE;?></label>
                  <label class="radio">
                    <input name="status" type="radio" value="0">
                    <i></i><?php echo Lang::$word->_PASIVE;?></label>
                </div>
              </div>
           </div>

            <div class="veriasist double fitted divider"></div>

            <button type="button" name="dosubmit" class="veriasist positive button"><?php echo Lang::$word->_MOD_DC_ADD_B;?></button>
            <a href="index.php?do=modules&amp;action=config&amp;modname=discounts" class="veriasist basic button"><?php echo Lang::$word->_CANCEL;?></a>
            <input name="processConfig" type="hidden" value="1">
          </form>

      </div>
      
  </div>
  <div id="msgholder"></div>
</div>




<?php break; ?>

<?php case 'edit': ?>

   <?php $discountRow = Registry::get("Discounts")->getDiscounts();?>

  <div class="veriasist icon heading message blue"> 
  
  <a class="helper veriasist top right info corner label" data-help="comments">
    <i class="icon help"></i>
  </a> 
  <i class="puzzle piece icon"></i>
  
  <div class="content">
    
    <div class="header"> <?php echo Lang::$word->_MOD_CM_TITLE3;?> </div>
    <div class="veriasist breadcrumb">
      <i class="icon home"></i> 
      <a href="index.php" class="section">
        <?php echo Lang::$word->_N_DASH;?>    
      </a>
      <div class="divider"> / </div>
      <a href="index.php?do=modules" class="section"><?php echo Lang::$word->_N_MODS;?></a>
      <div class="divider"> / </div>
      <a href="index.php?do=modules&amp;action=config&amp;modname=discounts" class="section"><?php echo $content->getModuleName(Filter::$modname);?></a>
      <div class="divider"> / </div>
      <div class="active section"><?php echo Lang::$word->_MOD_DC_TITLE1;?></div>
    </div>
  </div>
</div>

<div class="veriasist-large-content">

  <div class="veriasist message">
      <?php echo Core::langIcon();?><?php echo Lang::$word->_MOD_DC_INFO1. Lang::$word->_REQ1 . '<i class="icon asterisk"></i>' . Lang::$word->_REQ2;?>

      <div class="veriasist form segment">
        
        <div class="veriasist header"><?php echo Lang::$word->_MOD_DC_SUBTITLE1;?></div>

        <div class="veriasist double fitted divider"></div>

        <form id="veriasist_form" name="veriasist_form" method="post">

           <div class="two fields">
              <div class="field">
                <label><?php echo Lang::$word->_MOD_DC_FORM_NAME;?></label>
                <label class="input"><i class="icon-append icon asterisk"></i>
                  <input type="text" value="<?= $discountRow->{'title'.Lang::$lang}?>" name="title<?= Lang::$lang?>">
                </label>
              </div>

              <div class="field">
                <label><?php echo Lang::$word->_MOD_DC_FORM_CODE;?></label>
                <label class="input"><i class="icon-append icon asterisk"></i>
                  <input type="text" value="<?= $discountRow->code; ?>" name="code">
                </label>
              </div>
           </div>

           <div class="two fields">
            <div class="field">
              <label><?php echo Lang::$word->_MOD_DC_FORM_PRICE;?></label>
              <label class="input"><i class="icon-append icon asterisk"></i>
                <label class="input">
                  <i class="icon-prepend icon dollar"></i>
                  <i class="icon-append icon asterisk"></i>
                  <input type="text" value="<?= $discountRow->price ?>" name="price">
                </label>
              </label>
            </div>
            <input name="id" type="hidden" value="<?php echo Filter::$id;?>">
             <div class="field">
                <label><?php echo Lang::$word->_MOD_DC_FORM_STATUS;?></label>
                <div class="inline-group">
                  <label class="radio">
                    <input name="status" type="radio" value="1" <?php echo getChecked($discountRow->status, 1);?>>
                    <i></i><?php echo Lang::$word->_ACTIVE;?></label>
                  <label class="radio">
                    <input name="status" type="radio" value="0" <?php echo getChecked($discountRow->status, 0);?>>
                    <i></i><?php echo Lang::$word->_PASIVE;?></label>
                </div>
              </div>
           </div>

            <div class="veriasist double fitted divider"></div>

            <button type="button" name="dosubmit" class="veriasist positive button"><?php echo Lang::$word->_MOD_DC_ADD_B;?></button>
            <a href="index.php?do=modules&amp;action=config&amp;modname=discounts" class="veriasist basic button"><?php echo Lang::$word->_CANCEL;?></a>
            <input name="processConfig" type="hidden" value="1">
          </form>

      </div>
      
  </div>
  <div id="msgholder"></div>
</div>
  
<?php  break; ?>
  
<?php default: ?>
  <?php $discountRow = Registry::get("Discounts")->getDiscounts();?>

  <div class="veriasist icon heading message blue"> <i class="puzzle piece icon"></i>
  <div class="content">
    <div class="header"> <?php echo Lang::$word->_MOD_DC_TITLE3;?> </div>
    <div class="veriasist breadcrumb"><i class="icon home"></i> <a href="index.php" class="section"><?php echo Lang::$word->_N_DASH;?></a>
      <div class="divider"> / </div>
      <a href="index.php?do=modules" class="section"><?php echo Lang::$word->_N_MODS;?></a>
      <div class="divider"> / </div>
      <div class="active section"><?php echo $content->getModuleName(Filter::$modname);?></div>
    </div>
  </div>
</div>

<div class="veriasist-large-content"> 
  <div class="veriasist message">
    <?php echo Core::langIcon();?><?php echo Lang::$word->_MOD_DC_INFO3;?>
  </div>

  <div class="veriasist segment"> 
    
    <a class="veriasist icon positive button push-right" href="<?php echo Core::url("modules", "add");?>">
      <i class="icon add"></i> 
      <?php echo Lang::$word->_MOD_DC_ADD;?>
    </a>
    
    <div class="veriasist header">
      <?php echo Lang::$word->_MOD_DC_SUBTITLE3;?>  
    </div>


    <div class="veriasist fitted divider"></div>
    
    <form method="post" id="admin_form" name="admin_form">
      
      <table class="veriasist sortable table">
        <thead>
          <tr>
            
            <th data-sort="string"><?php echo Lang::$word->_MOD_DC_UNAME;?></th>
            <th data-sort="string"><?php echo Lang::$word->_MOD_DC_CODE;?></th>
            <th data-sort="int"><?php echo Lang::$word->_MOD_DC_USING;?></th>
            <th data-sort="string"><?php echo Lang::$word->_MOD_DC_STATUS;?></th>
            <th class="disabled"><?php echo Lang::$word->_ACTIONS;?></th>
          </tr>
        </thead>
        <?php if($discountRow):?>
        
        <?php endif;?>
        <tbody>
          <?php if(!$discountRow):?>
          <tr>
            <td colspan="6"><?php echo Filter::msgSingleAlert(Lang::$word->_MOD_DC_NONCOMMENTS);?></td>
          </tr>
          <?php else:?>
          <?php foreach ($discountRow as $row):?>
          <tr>
            <td><?php echo $row->{'title'.Lang::$lang };?></td>
            <td><?php echo $row->code;?></td>
            <td><?php echo $row->price ?></td>
            <td><?php echo isActive($row->status); ?></td>
            <td>
              <a href="index.php?do=modules&action=config&modname=discounts&maction=edit&id=<?= $row->id; ?>">
                <i class="rounded success inverted pencil icon link"></i>
              </a>
              <a class="delete" data-option="deleteDiscounts" data-id="<?= $row->id; ?>" data-name="<?= $row->{'title'.Lang::$lang};?>"><i class="rounded danger inverted trash icon link"></i></a>
            </td>
          </tr>
          <?php endforeach;?>
          <?php unset($row);?>
          <?php endif;?>
        </tbody>
      </table>
    </form>
  </div>

</div>
  
<?php break; endswitch; ?>
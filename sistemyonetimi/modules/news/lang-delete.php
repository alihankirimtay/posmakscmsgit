<?php
  /**
   * Language Data Delete
   *
   * @package VeriAsist
   * @author veriasist.com
   * @copyright 2014
   * @version $Id: lang-delete.php, v4.00 2014-04-20 10:12:05 gewa Exp $
   */
  if (!defined("_VALID_PHP"))
      die('Direct access to this location is not allowed.');
?>
<?php
  self::$db->query("LOCK TABLES mod_news WRITE");
  self::$db->query("ALTER TABLE mod_news DROP COLUMN title_" . $flag_id);
  self::$db->query("ALTER TABLE mod_news DROP COLUMN short_desc_" . $flag_id);
  self::$db->query("ALTER TABLE mod_news DROP COLUMN body_" . $flag_id);
  self::$db->query("ALTER TABLE mod_news DROP COLUMN caption_" . $flag_id);
  self::$db->query("ALTER TABLE mod_news DROP COLUMN tags_" . $flag_id);
  self::$db->query("ALTER TABLE mod_news DROP COLUMN metakey_" . $flag_id);
  self::$db->query("ALTER TABLE mod_news DROP COLUMN metadesc_" . $flag_id);
  self::$db->query("UNLOCK TABLES");

  self::$db->query("LOCK TABLES mod_news_categories WRITE");
  self::$db->query("ALTER TABLE mod_news_categories DROP COLUMN name_" . $flag_id);
  self::$db->query("ALTER TABLE mod_news_categories DROP COLUMN description_" . $flag_id);
  self::$db->query("ALTER TABLE mod_news_categories DROP COLUMN metakey_" . $flag_id);
  self::$db->query("ALTER TABLE mod_news_categories DROP COLUMN metadesc_" . $flag_id);
  self::$db->query("UNLOCK TABLES");

  self::$db->query("LOCK TABLES plug_news_tags WRITE");
  self::$db->query("ALTER TABLE plug_news_tags DROP COLUMN tagname_" . $flag_id);
  self::$db->query("UNLOCK TABLES");
?>
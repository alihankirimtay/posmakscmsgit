<?php
  /**
   * Crumbs
   *
   * @package Veri Asist
   * @author veriasist.com
   * @copyright 2014
   * @version $Id: crumbs.php, v4.00 2014-03-20 10:12:05 gewa Exp $
   */
  if (!defined("_VALID_PHP"))
      die('Direct access to this location is not allowed.');

  require_once("admin_class.php");
  
  $length = count($this->_url);
?>
<?php
  switch ($length) {
      case 3:
          $nav = '<a href="' . doUrl(false, false, "digishop") . '" class="section">' . $this->moduledata->{'title' . Lang::$lang} . '</a>';
          $nav .= '<span class="divider"></span>';
		  $nav .= '<div class="active section"> ' . Lang::$word->_CATEGORY . ' </div>';
		  $nav .= '<span class="divider"></span>';
          $nav .= '<div class="active section"> ' . $this->moduledata->mod->{'name' . Lang::$lang} . ' </div>';
          break;
		  
      case 2:
	      if(in_array(doUrlParts("digishop-checkout"), $this->_url)) {
			  $nav = '<a href="' . doUrl(false, false, "digishop") . '" class="section">' . $this->moduledata->{'title' . Lang::$lang} . '</a>';
			  $nav .= '<span class="divider"></span> ';
			  $nav .= '<div class="active section">' . Lang::$word->_MOD_DS_CHECKOUT . '</div>';
		  } else {
			  $nav = '<a href="' . doUrl(false, false, "digishop") . '" class="section">' . $this->moduledata->{'title' . Lang::$lang} . '</a>';
			  $nav .= '<span class="divider"></span>';
			  $nav .= '<div class="active section">' . $this->moduledata->mod->{'title' . Lang::$lang} . '</div>';
		  }
          break;

      default:
          $nav = '<div class="active section">' . $this->moduledata->{'title' . Lang::$lang} . '</div>';
          break;
  }
?>
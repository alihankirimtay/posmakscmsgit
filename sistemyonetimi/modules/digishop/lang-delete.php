<?php
  /**
   * Language Data Delete
   *
   * @package Veri Asist
   * @author veriasist.com
   * @copyright 2014
   * @version $Id: lang-delete.php, v4.00 2014-04-20 10:12:05 gewa Exp $
   */
  if (!defined("_VALID_PHP"))
      die('Direct access to this location is not allowed.');
?>
<?php
  self::$db->query("LOCK TABLES mod_digishop WRITE");
  self::$db->query("ALTER TABLE mod_digishop DROP COLUMN title_" . $flag_id);
  self::$db->query("ALTER TABLE mod_digishop DROP COLUMN body_" . $flag_id);
  self::$db->query("ALTER TABLE mod_digishop DROP COLUMN metakey_" . $flag_id);
  self::$db->query("ALTER TABLE mod_digishop DROP COLUMN metadesc_" . $flag_id);
  self::$db->query("UNLOCK TABLES");

  self::$db->query('LOCK TABLES mod_digishop_categories WRITE');
  self::$db->query("ALTER TABLE mod_digishop_categories DROP COLUMN name_" . $flag_id);
  self::$db->query("ALTER TABLE mod_digishop_categories DROP COLUMN metakey_" . $flag_id);
  self::$db->query("ALTER TABLE mod_digishop_categories DROP COLUMN metadesc_" . $flag_id);
  self::$db->query("UNLOCK TABLES");
?>
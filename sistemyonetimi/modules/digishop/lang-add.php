<?php
  /**
   * Language Data Add
   *
   * @package Veri Asist
   * @author veriasist.com
   * @copyright 2014
   * @version $Id: lang-add.php, v4.00 2014-04-20 10:12:05 gewa Exp $
   */
  if (!defined("_VALID_PHP"))
      die('Direct access to this location is not allowed.');
?>
<?php
  self::$db->query("LOCK TABLES mod_digishop WRITE");
  self::$db->query("ALTER TABLE mod_digishop ADD COLUMN title_$flag_id VARCHAR(100) NOT NULL AFTER title_en");
  self::$db->query("ALTER TABLE mod_digishop ADD COLUMN body_$flag_id TEXT AFTER body_en");
  self::$db->query("ALTER TABLE mod_digishop ADD COLUMN metakey_$flag_id VARCHAR(200) NOT NULL AFTER metakey_en");
  self::$db->query("ALTER TABLE mod_digishop ADD COLUMN metadesc_$flag_id TEXT AFTER metadesc_en");
  self::$db->query("UNLOCK TABLES");

  if ($mod_digishop = self::$db->fetch_all("SELECT * FROM mod_digishop")) {
      foreach ($mod_digishop as $row) {
          $data['title_' . $flag_id] = $row->title_en;
          $data['body_' . $flag_id] = $row->body_en;
          $data['metakey_' . $flag_id] = $row->metakey_en;
          $data['metadesc_' . $flag_id] = $row->metadesc_en;
          self::$db->update("mod_digishop", $data, "id = " . $row->id);
      }
      unset($data, $row);
  }

  self::$db->query("LOCK TABLES mod_digishop_categories WRITE");
  self::$db->query("ALTER TABLE mod_digishop_categories ADD COLUMN name_$flag_id VARCHAR(100) NOT NULL AFTER name_en");
  self::$db->query("ALTER TABLE mod_digishop_categories ADD COLUMN metakey_$flag_id VARCHAR(200) NOT NULL AFTER metakey_en");
  self::$db->query("ALTER TABLE mod_digishop_categories ADD COLUMN metadesc_$flag_id TEXT AFTER metadesc_en");
  self::$db->query("UNLOCK TABLES");

  if ($mod_digishop_categories = self::$db->fetch_all("SELECT * FROM mod_digishop_categories")) {
      foreach ($mod_digishop_categories as $row) {
          $data = array(
              'name_' . $flag_id => $row->name_en,
              'metakey_' . $flag_id => $row->metakey_en,
              'metadesc_' . $flag_id => $row->metadesc_en);

          self::$db->update("mod_digishop_categories", $data, "id = " . $row->id);
      }
      unset($data, $row);
  }
?>
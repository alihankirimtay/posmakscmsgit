<?php
  /**
   * DigiShop
   *
   * @package Veri Asist
   * @author veriasist.com
   * @copyright 2014
   * @version $Id: admin.php, v4.00 2014-04-25 10:12:05 gewa Exp $
   */
  if (!defined("_VALID_PHP"))
      die('Direct access to this location is not allowed.');
  
  if(!$user->getAcl("digishop")): print Filter::msgAlert(Lang::$word->_CG_ONLYADMIN); return; endif;
  Registry::set('Digishop', new Digishop());
?>
<?php switch(Filter::$maction): case "config": ?>
<?php $row = Registry::get("Digishop");?>
<div class="veriasist icon heading message blue"> <a class="helper veriasist top right info corner label" data-help="dsconfig"><i class="icon help"></i></a> <i class="puzzle piece icon"></i>
  <div class="content">
    <div class="header"> <?php echo Lang::$word->_MOD_DS_TITLE1;?> </div>
    <div class="veriasist breadcrumb"><i class="icon home"></i> <a href="index.php" class="section"><?php echo Lang::$word->_N_DASH;?></a>
      <div class="divider"> / </div>
      <a href="index.php?do=modules" class="section"><?php echo Lang::$word->_N_MODS;?></a>
      <div class="divider"> / </div>
      <a href="index.php?do=modules&amp;action=config&amp;modname=digishop" class="section"><?php echo $content->getModuleName(Filter::$modname);?></a>
      <div class="divider"> / </div>
      <div class="active section"><?php echo Lang::$word->_MOD_DS_TITLE4;?></div>
    </div>
  </div>
</div>
<div class="veriasist-large-content">
  <div class="veriasist message"><?php echo Core::langIcon();?><?php echo Lang::$word->_MOD_DS_INFO4. Lang::$word->_REQ1 . '<i class="icon asterisk"></i>' . Lang::$word->_REQ2;?></div>
  <div class="veriasist form segment">
    <div class="veriasist header"><?php echo Lang::$word->_MOD_DS_SUBTITLE4;?></div>
    <div class="veriasist double fitted divider"></div>
    <form id="veriasist_form" name="veriasist_form" method="post">
      <div class="two fields">
        <div class="field">
          <label><?php echo Lang::$word->_MOD_DS_FILEDIR;?></label>
          <label class="input"><i class="icon-append icon asterisk"></i>
            <input type="text" value="<?php echo $row->filedir;?>" name="filedir">
          </label>
        </div>
        <div class="field">
          <div class="three fields">
            <div class="field">
              <label><?php echo Lang::$word->_MOD_DS_IS_FREE;?></label>
              <div class="inline-group">
                <label class="radio">
                  <input name="allow_free" type="radio" value="1" <?php echo getChecked($row->allow_free, 1);?>>
                  <i></i><?php echo Lang::$word->_YES;?></label>
                <label class="radio">
                  <input name="allow_free" type="radio" value="0" <?php echo getChecked($row->allow_free, 0);?>>
                  <i></i><?php echo Lang::$word->_NO;?></label>
              </div>
            </div>
            <div class="field">
              <label><?php echo Lang::$word->_MOD_DS_LAYOUT;?></label>
              <div class="inline-group">
                <label class="radio">
                  <input name="layout" type="radio" value="1" <?php echo getChecked($row->layout, 1);?>>
                  <i></i><?php echo Lang::$word->_MOD_DS_GRID_W;?></label>
                <label class="radio">
                  <input name="layout" type="radio" value="0" <?php echo getChecked($row->layout, 0);?>>
                  <i></i><?php echo Lang::$word->_MOD_DS_LIST_W;?></label>
              </div>
            </div>
            <div class="field">
              <label><?php echo Lang::$word->_MOD_DS_LIKE;?></label>
              <div class="inline-group">
                <label class="radio">
                  <input name="like" type="radio" value="1" <?php echo getChecked($row->like, 1);?>>
                  <i></i><?php echo Lang::$word->_YES;?></label>
                <label class="radio">
                  <input name="like" type="radio" value="0" <?php echo getChecked($row->like, 0);?>>
                  <i></i><?php echo Lang::$word->_NO;?></label>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="three fields">
        <div class="field">
          <label><?php echo Lang::$word->_MOD_DS_COLS;?></label>
          <label class="input">
            <input type="text" class="slrange" value="<?php echo $row->cols;?>" name="cols">
          </label>
        </div>
        <div class="field">
          <label><?php echo Lang::$word->_MOD_DS_ITEMPP;?></label>
          <label class="input">
            <input type="text" class="slrange" value="<?php echo $row->ipp;?>" name="ipp">
          </label>
        </div>
        <div class="field">
          <label><?php echo Lang::$word->_MOD_DS_LATEST;?></label>
          <label class="input">
            <input type="text" class="slrange" value="<?php echo $row->fpp;?>" name="fpp">
          </label>
        </div>
      </div>
      <div class="field">
        <label><?php echo Lang::$word->_MOD_DS_TEMPLATE;?></label>
        <textarea class="plugpost" name="template"><?php echo $row->template;?></textarea>
      </div>
      <div class="veriasist double fitted divider"></div>
      <button type="button" name="dosubmit" class="veriasist positive button"><?php echo Lang::$word->_MOD_DS_CFGUPDATE;?></button>
      <a href="index.php?do=modules&amp;action=config&amp;modname=digishop" class="veriasist basic button"><?php echo Lang::$word->_CANCEL;?></a>
      <input name="processConfig" type="hidden" value="1">
    </form>
  </div>
  <div id="msgholder"></div>
</div>
<script type="text/javascript"> 
// <![CDATA[
$(document).ready(function () {
    $("input[name=cols]").ionRangeSlider({
		min: 2,
		max: 6,
        step: 1,
		postfix: " col",
        type: 'single',
        hasGrid: true
    });
    $("input[name=ipp], input[name=fpp]").ionRangeSlider({
		min: 5,
		max: 20,
        step: 1,
		postfix: " itm",
        type: 'single',
        hasGrid: true
    });
});
// ]]>
</script>
<?php break;?>
<?php case"category": ?>
<?php $catrow = Registry::get("Digishop")->getCategoryList();?>
<div class="veriasist icon heading message blue"> <i class="puzzle piece icon"></i>
  <div class="content">
    <div class="header"> <?php echo Lang::$word->_MOD_DS_TITLE1;?> </div>
    <div class="veriasist breadcrumb"><i class="icon home"></i> <a href="index.php" class="section"><?php echo Lang::$word->_N_DASH;?></a>
      <div class="divider"> / </div>
      <a href="index.php?do=modules" class="section"><?php echo Lang::$word->_N_MODS;?></a>
      <div class="divider"> / </div>
      <a href="index.php?do=modules&amp;action=config&amp;modname=digishop" class="section"><?php echo $content->getModuleName(Filter::$modname);?></a>
      <div class="divider"> / </div>
      <div class="active section"><?php echo Lang::$word->_MOD_DS_TITLE5;?></div>
    </div>
  </div>
</div>
<div class="veriasist-large-content">
  <div class="veriasist message"><?php echo Core::langIcon();?><?php echo Lang::$word->_MOD_DS_INFO5;?></div>
  <div class="veriasist segment"> <a class="veriasist icon warning button push-right" href="<?php echo Core::url("modules", "catadd");?>"><i class="icon add"></i> <?php echo Lang::$word->_MOD_DS_ADDC;?></a>
    <div class="veriasist header"><?php echo Lang::$word->_MOD_DS_SUBTITLE5;?></div>
    <div class="veriasist fitted divider"></div>
    <table class="veriasist sortable table">
      <thead>
        <tr>
          <th class="disabled"></th>
          <th data-sort="string"><?php echo Lang::$word->_MOD_DS_CATNAME;?></th>
          <th data-sort="int"><?php echo Lang::$word->_MOD_DS_ITEMS;?></th>
          <th data-sort="int"><?php echo Lang::$word->_MOD_DS_POSITION;?></th>
          <th class="disabled"><?php echo Lang::$word->_ACTIONS;?></th>
        </tr>
      </thead>
      <tbody>
        <?php if(!$catrow):?>
        <tr>
          <td colspan="5"><?php echo Filter::msgSingleAlert(Lang::$word->_MOD_DS_NOCATS);?></td>
        </tr>
        <?php else:?>
        <?php foreach ($catrow as $row):?>
        <tr id="node-<?php echo $row->id;?>">
          <td class="id-handle"><i class="icon reorder"></i></td>
          <td><?php echo $row->{'name'.Lang::$lang};?></td>
          <td><span class="veriasist info label"><?php echo $row->total;?></span></td>
          <td><span class="veriasist black label"><?php echo $row->sorting;?></span></td>
          <td><a href="<?php echo Core::url("modules", "catedit", $row->id);?>"><i class="rounded inverted success icon pencil link"></i></a> <a class="delete" data-title="<?php echo Lang::$word->_DELETE.' '.Lang::$word->_MOD_DS_CATEGORY;?>" data-option="deleteCategory" data-id="<?php echo $row->id;?>" data-name="<?php echo $row->{'name'.Lang::$lang};?>"><i class="rounded danger inverted remove icon link"></i></a></td>
        </tr>
        <?php endforeach;?>
        <?php unset($row);?>
        <?php endif;?>
      </tbody>
    </table>
  </div>
</div>
<script type="text/javascript"> 
// <![CDATA[
$(document).ready(function () {
    $(".veriasist.table tbody").sortable({
        helper: 'clone',
        handle: '.id-handle',
        placeholder: 'placeholder',
        opacity: .6,
        update: function (event, ui) {
            serialized = $(".veriasist.table tbody").sortable('serialize');
            $.ajax({
                type: "POST",
                url: "modules/digishop/controller.php?sortcats",
                data: serialized,
                success: function (msg) {}
            });
        }
    });
});
// ]]>
</script>
<?php break;?>
<?php case"catedit": ?>
<?php $row = Core::getRowById(Digishop::ctTable, Filter::$id);?>
<div class="veriasist icon heading message blue"> <i class="puzzle piece icon"></i>
  <div class="content">
    <div class="header"> <?php echo Lang::$word->_MOD_DS_TITLE1;?> </div>
    <div class="veriasist breadcrumb"><i class="icon home"></i> <a href="index.php" class="section"><?php echo Lang::$word->_N_DASH;?></a>
      <div class="divider"> / </div>
      <a href="index.php?do=modules" class="section"><?php echo Lang::$word->_N_MODS;?></a>
      <div class="divider"> / </div>
      <a href="index.php?do=modules&amp;action=config&amp;modname=digishop" class="section"><?php echo $content->getModuleName(Filter::$modname);?></a>
      <div class="divider"> / </div>
      <a href="<?php echo Core::url("modules", "category");?>" class="section"><?php echo Lang::$word->_MOD_DS_TITLE5;?></a>
      <div class="divider"> / </div>
      <div class="active section"><?php echo Lang::$word->_MOD_DS_TITLE6;?></div>
    </div>
  </div>
</div>
<div class="veriasist-large-content">
  <div class="veriasist message"><?php echo Core::langIcon();?><?php echo Lang::$word->_MOD_DS_INFO6. Lang::$word->_REQ1 . '<i class="icon asterisk"></i>' . Lang::$word->_REQ2;?></div>
  <div class="veriasist form segment">
    <div class="veriasist header"><?php echo Lang::$word->_MOD_DS_SUBTITLE6 . $row->{'name'.Lang::$lang};?></div>
    <div class="veriasist double fitted divider"></div>
    <form id="veriasist_form" name="veriasist_form" method="post">
      <div class="two fields">
        <div class="field">
          <label><?php echo Lang::$word->_MOD_DS_CATNAME;?></label>
          <label class="input"><i class="icon-append icon asterisk"></i>
            <input type="text" value="<?php echo $row->{'name'.Lang::$lang};?>" name="name<?php echo Lang::$lang;?>">
          </label>
        </div>
        <div class="field">
          <label><?php echo Lang::$word->_MOD_DS_CATSLUG;?></label>
          <input type="text" value="<?php echo $row->slug;?>" name="slug">
        </div>
      </div>
      <div class="two fields">
        <div class="field">
          <label><?php echo Lang::$word->_METAKEYS;?></label>
          <textarea name="metakey<?php echo Lang::$lang;?>"><?php echo $row->{'metakey'.Lang::$lang};?></textarea>
        </div>
        <div class="field">
          <label><?php echo Lang::$word->_METADESC;?></label>
          <textarea name="metadesc<?php echo Lang::$lang;?>"><?php echo $row->{'metadesc'.Lang::$lang};?></textarea>
        </div>
      </div>
      <div class="veriasist double fitted divider"></div>
      <button type="button" name="dosubmit" class="veriasist positive button"><?php echo Lang::$word->_MOD_DS_UDTC;?></button>
      <a href="<?php echo Core::url("modules", "category");?>" class="veriasist basic button"><?php echo Lang::$word->_CANCEL;?></a>
      <input name="id" type="hidden" value="<?php echo Filter::$id;?>">
      <input name="processCategory" type="hidden" value="1">
    </form>
  </div>
  <div id="msgholder"></div>
</div>
<?php break;?>
<?php case"catadd": ?>
<div class="veriasist icon heading message blue"> <i class="puzzle piece icon"></i>
  <div class="content">
    <div class="header"> <?php echo Lang::$word->_MOD_DS_TITLE1;?> </div>
    <div class="veriasist breadcrumb"><i class="icon home"></i> <a href="index.php" class="section"><?php echo Lang::$word->_N_DASH;?></a>
      <div class="divider"> / </div>
      <a href="index.php?do=modules" class="section"><?php echo Lang::$word->_N_MODS;?></a>
      <div class="divider"> / </div>
      <a href="index.php?do=modules&amp;action=config&amp;modname=digishop" class="section"><?php echo $content->getModuleName(Filter::$modname);?></a>
      <div class="divider"> / </div>
      <a href="<?php echo Core::url("modules", "category");?>" class="section"><?php echo Lang::$word->_MOD_DS_TITLE5;?></a>
      <div class="divider"> / </div>
      <div class="active section"><?php echo Lang::$word->_MOD_DS_TITLE7;?></div>
    </div>
  </div>
</div>
<div class="veriasist-large-content">
  <div class="veriasist message"><?php echo Core::langIcon();?><?php echo Lang::$word->_MOD_DS_INFO7. Lang::$word->_REQ1 . '<i class="icon asterisk"></i>' . Lang::$word->_REQ2;?></div>
  <div class="veriasist form segment">
    <div class="veriasist header"><?php echo Lang::$word->_MOD_DS_SUBTITLE7;?></div>
    <div class="veriasist double fitted divider"></div>
    <form id="veriasist_form" name="veriasist_form" method="post">
      <div class="two fields">
        <div class="field">
          <label><?php echo Lang::$word->_MOD_DS_CATNAME;?></label>
          <label class="input"><i class="icon-append icon asterisk"></i>
            <input type="text" placeholder="<?php echo Lang::$word->_MOD_DS_CATNAME;?>" name="name<?php echo Lang::$lang;?>">
          </label>
        </div>
        <div class="field">
          <label><?php echo Lang::$word->_MOD_DS_CATSLUG;?></label>
          <input type="text" placeholder="<?php echo Lang::$word->_MOD_DS_CATSLUG;?>" name="slug">
        </div>
      </div>
      <div class="two fields">
        <div class="field">
          <label><?php echo Lang::$word->_METAKEYS;?></label>
          <textarea placeholder="<?php echo Lang::$word->_METAKEYS;?>" name="metakey<?php echo Lang::$lang;?>"></textarea>
        </div>
        <div class="field">
          <label><?php echo Lang::$word->_METADESC;?></label>
          <textarea placeholder="<?php echo Lang::$word->_METADESC;?>" name="metadesc<?php echo Lang::$lang;?>"></textarea>
        </div>
      </div>
      <div class="veriasist double fitted divider"></div>
      <button type="button" name="dosubmit" class="veriasist positive button"><?php echo Lang::$word->_MOD_DS_ADDC;?></button>
      <a href="<?php echo Core::url("modules", "category");?>" class="veriasist basic button"><?php echo Lang::$word->_CANCEL;?></a>
      <input name="processCategory" type="hidden" value="1">
    </form>
  </div>
  <div id="msgholder"></div>
</div>
<?php break;?>
<?php case"transactions": ?>
<?php $transrow = Registry::get("Digishop")->getPayments();?>
<div class="veriasist icon heading message blue"> <i class="puzzle piece icon"></i>
  <div class="content">
    <div class="header"> <?php echo Lang::$word->_MOD_DS_TITLE1;?> </div>
    <div class="veriasist breadcrumb"><i class="icon home"></i> <a href="index.php" class="section"><?php echo Lang::$word->_N_DASH;?></a>
      <div class="divider"> / </div>
      <a href="index.php?do=modules" class="section"><?php echo Lang::$word->_N_MODS;?></a>
      <div class="divider"> / </div>
      <a href="index.php?do=modules&amp;action=config&amp;modname=digishop" class="section"><?php echo $content->getModuleName(Filter::$modname);?></a>
      <div class="divider"> / </div>
      <div class="active section"><?php echo Lang::$word->_MOD_DS_TITLE8;?></div>
    </div>
  </div>
</div>
<div class="veriasist-large-content">
  <div class="veriasist message"><?php echo Core::langIcon();?><?php echo Lang::$word->_MOD_DS_INFO8;?></div>
  <div class="veriasist segment"> <a href="modules/digishop/controller.php?exportTransactions" class="veriasist info button push-right"><i class="icon table"></i><?php echo Lang::$word->_MOD_DS_EXPORT;?></a>
    <div class="veriasist header"><?php echo Lang::$word->_MOD_DS_SUBTITLE8;?></div>
    <div class="veriasist fitted divider"></div>
    <div class="veriasist small form basic segment">
      <form method="post" id="veriasist_form" name="veriasist_form">
        <div class="three fields">
          <div class="field">
            <div class="veriasist input"> <i class="icon-prepend icon calendar"></i>
              <input name="fromdate" type="text" data-datepicker="true" placeholder="<?php echo Lang::$word->_UR_SHOW_FROM;?>" id="fromdate" />
            </div>
          </div>
          <div class="field">
            <div class="veriasist action input"> <i class="icon-prepend icon calendar"></i>
              <input name="enddate" type="text" data-datepicker="true" placeholder="<?php echo Lang::$word->_UR_SHOW_TO;?>" id="enddate" />
              <a id="doDates" class="veriasist icon button"><?php echo Lang::$word->_SR_SEARCH_GO;?></a> </div>
          </div>
          <div class="field">
            <div class="two fields">
              <div class="field"> <?php echo $pager->items_per_page();?> </div>
              <div class="field"> <?php echo $pager->jump_menu();?> </div>
            </div>
          </div>
        </div>
      </form>
      <div class="veriasist fitted divider"></div>
    </div>
    <table class="veriasist sortable table">
      <thead>
        <tr>
          <th>#</th>
          <th data-sort="string"><?php echo Lang::$word->_MOD_DS_NAME;?></th>
          <th data-sort="string"><?php echo Lang::$word->_USERNAME;?></th>
          <th data-sort="int"><?php echo Lang::$word->_TR_AMOUNT;?></th>
          <th data-sort="int"><?php echo Lang::$word->_TR_PAYDATE;?></th>
          <th data-sort="string"><?php echo Lang::$word->_TR_PROCESSOR;?></th>
          <th class="disabled"><?php echo Lang::$word->_ACTIONS;?></th>
        </tr>
      </thead>
      <tbody>
        <?php if(!$transrow):?>
        <tr>
          <td colspan="7"><?php echo Filter::msgSingleAlert(Lang::$word->_MOD_DS_NOTRANS);?></td>
        </tr>
        <?php else:?>
        <?php foreach ($transrow as $row):?>
        <tr>
          <td><?php echo $row->id;?>.</td>
          <td><a href="<?php echo Core::url("modules", "edit", $row->did);?>"><?php echo $row->{'title'.Lang::$lang};?></a></td>
          <td><a href="index.php?do=users&amp;action=edit&amp;userid=<?php echo $row->uid;?>"><?php echo $row->username;?></a></td>
          <td data-sort-value="<?php echo $row->price;?>"><?php echo $core->formatMoney($row->price);?></td>
          <td data-sort-value="<?php echo strtotime($row->created);?>"><?php echo Filter::dodate("long_date", $row->created);?></td>
          <td><span class="veriasist positive label"><?php echo $row->pp;?></span></td>
          <td><?php if($row->status):?>
            <i data-content="<?php echo Lang::$word->_COMPLETED;?>" class="rounded inverted icon check"></i>
            <?php else:?>
            <a class="resend" data-id="<?php echo $row->id;?>"><i data-content="<?php echo Lang::$word->_PENDING;?>" class="rounded inverted icon time link"></i></a>
            <?php endif;?>
            <a class="delete" data-title="<?php echo Lang::$word->_DELETE.' '.Lang::$word->_MOD_DS_TRANSREC;?>" data-option="deleteTransaction" data-id="<?php echo $row->id;?>" data-name="<?php echo $row->{'title' . Lang::$lang};?>"><i class="rounded danger inverted remove icon link"></i></a></td>
        </tr>
        <?php endforeach;?>
        <?php unset($row);?>
        <?php endif;?>
      </tbody>
    </table>
  </div>
  <div class="veriasist-grid">
    <div class="two columns horizontal-gutters">
      <div class="row"> <span class="veriasist label"><?php echo Lang::$word->_PAG_TOTAL.': '.$pager->items_total;?> / <?php echo Lang::$word->_PAG_CURPAGE.': '.$pager->current_page.' '.Lang::$word->_PAG_OF.' '.$pager->num_pages;?></span> </div>
      <div class="row">
        <div id="pagination"><?php echo $pager->display_pages();?></div>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
// <![CDATA[
$(document).ready(function () {
    $('body').on('click', 'a.resend', function () {
		$(".veriasist.segment:first").addClass("loading")
        var id = $(this).data('id');
        var $child = $(this).children('i');
        var $parent = $(this).closest('tr');
        $.ajax({
            type: 'post',
            dataType: 'json',
            url: "modules/digishop/controller.php",
            data: {
                activateTransaction: 1,
                id: id
            },
            success: function (json) {
				$(".veriasist.segment:first").removeClass("loading")
                if (json.type = "success") {
                    $child.toggleClass("time check");
                    $parent.addClass("positive");
                }

                $.sticky(decodeURIComponent(json.message), {
                    type: json.type,
                    title: json.title
                });
            }
        });
    });
});
// ]]>
</script>
<?php break;?>
<?php case"edit": ?>
<?php $row = Core::getRowById(Digishop::mTable, Filter::$id);?>
<?php $catrow = Registry::get("Digishop")->getCategories();?>
<div class="veriasist icon heading message blue"> <a class="helper veriasist top right info corner label" data-help="digishop"><i class="icon help"></i></a> <i class="puzzle piece icon"></i>
  <div class="content">
    <div class="header"> <?php echo Lang::$word->_MOD_DS_TITLE1;?> </div>
    <div class="veriasist breadcrumb"><i class="icon home"></i> <a href="index.php" class="section"><?php echo Lang::$word->_N_DASH;?></a>
      <div class="divider"> / </div>
      <a href="index.php?do=modules" class="section"><?php echo Lang::$word->_N_MODS;?></a>
      <div class="divider"> / </div>
      <a href="index.php?do=modules&amp;action=config&amp;modname=digishop" class="section"><?php echo $content->getModuleName(Filter::$modname);?></a>
      <div class="divider"> / </div>
      <div class="active section"><?php echo Lang::$word->_MOD_DS_TITLE2;?></div>
    </div>
  </div>
</div>
<div class="veriasist-large-content">
  <div class="veriasist message"><?php echo Core::langIcon();?><?php echo Lang::$word->_MOD_DS_INFO2. Lang::$word->_REQ1 . '<i class="icon asterisk"></i>' . Lang::$word->_REQ2;?></div>
  <div class="veriasist form segment">
    <div class="veriasist header"><?php echo Lang::$word->_MOD_DS_SUBTITLE2 . $row->{'title'.Lang::$lang};?></div>
    <div class="veriasist double fitted divider"></div>
    <form id="veriasist_form" name="veriasist_form" method="post">
      <div class="two fields">
        <div class="field">
          <label><?php echo Lang::$word->_MOD_DS_NAME;?></label>
          <label class="input"><i class="icon-append icon asterisk"></i>
            <input type="text" value="<?php echo $row->{'title'.Lang::$lang};?>" name="title<?php echo Lang::$lang;?>">
          </label>
        </div>
        <div class="field">
          <label><?php echo Lang::$word->_MOD_DS_SLUG;?></label>
          <label class="input">
            <input type="text" value="<?php echo $row->slug;?>" name="slug">
          </label>
        </div>
      </div>
      <div class="two fields">
        <div class="field">
          <label><?php echo Lang::$word->_MOD_DS_CATEGORY;?></label>
          <select name="cid">
            <option value=""><?php echo Lang::$word->_MOD_DS_CATEGORY_SEL;?></option>
            <?php if($catrow):?>
            <?php foreach($catrow as $crow):?>
            <?php $sel = ($crow->id == $row->cid) ? ' selected="selected"' : '' ;?>
            <option value="<?php echo $crow->id;?>"<?php echo $sel;?>><?php echo $crow->{'name'.Lang::$lang};?></option>
            <?php endforeach;?>
            <?php unset($crow);?>
            <?php endif;?>
          </select>
        </div>
        <div class="field">
          <label><?php echo Lang::$word->_MOD_DS_PRODPRICE;?></label>
          <label class="input"><i class="icon-prepend icon dollar"></i><i class="icon-append icon asterisk"></i>
            <input type="text" value="<?php echo $row->price;?>" name="price">
          </label>
        </div>
      </div>
      <div class="two fields">
        <div class="field">
          <label><?php echo Lang::$word->_MOD_DS_MEMBERSHIP;?></label>
          <?php echo $member->getMembershipList($row->membership_id);?> </div>
        <div class="field">
          <?php $module_data = $row->gallery;?>
          <?php include(BASEPATH . "sistemyonetimi/modules/gallery/config.php");?>
        </div>
      </div>
      <div class="two fields">
        <div class="field">
          <label><?php echo Lang::$word->_MOD_DS_PRODIMG;?></label>
          <label class="input">
            <input type="file" name="thumb" class="filefield">
          </label>
        </div>
        <div class="field">
          <label><?php echo Lang::$word->_MOD_DS_PRODIMG;?></label>
          <div class="veriasist small image"> <a class="lightbox" href="<?php echo SITEURL . '/' . Digishop::imagepath . $row->thumb;?>"><img src="<?php echo SITEURL . '/' . Digishop::imagepath . $row->thumb;?>" alt="<?php echo $row->thumb;?>"></a> </div>
        </div>
      </div>

      <!-- backgroun image-->
      <div class="two fields">
        <div class="field">
          <label><?php echo Lang::$word->_MOD_DS_PRODIMG;?> (Arkaplan)</label>
          <label class="input">
            <input type="file" name="thumbbg" class="filefield">
          </label>
        </div>
        <div class="field">
          <label><?php echo Lang::$word->_MOD_DS_PRODIMG;?> (Arkaplan)</label>
          <div class="veriasist small image"> <a class="lightbox" href="<?php echo SITEURL . '/' . Digishop::imagepath . $row->thumbbg;?>"><img src="<?php echo SITEURL . '/' . Digishop::imagepath . $row->thumbbg;?>" alt="<?php echo $row->thumbbg;?>"></a> </div>
        </div>
      </div>

      <div class="two fields">
        <div class="field">
          <label><?php echo Lang::$word->_MOD_DS_PRODFILE;?></label>
          <label class="input"> </label>
          <input type="file" name="filename" class="filefield">
        </div>
        <div class="field"></div>
        <label><?php echo Lang::$word->_MOD_DS_PUBLISHED;?></label>
        <div class="inline-group">
          <label class="radio">
            <input name="active" type="radio" value="1" <?php echo getChecked($row->active, 1);?>>
            <i></i><?php echo Lang::$word->_YES;?></label>
          <label class="radio">
            <input name="active" type="radio" value="0" <?php echo getChecked($row->active, 0);?>>
            <i></i><?php echo Lang::$word->_NO;?></label>
        </div>
      </div>
      <div class="veriasist double fitted divider"></div>
      <div class="field">
        <label><?php echo Lang::$word->_MOD_DS_PRODDESC;?></label>
        <textarea class="plugpost" name="body<?php echo LANG::$lang;?>"><?php echo Filter::out_url($row->{'body'.Lang::$lang});?></textarea>
      </div>
      <div class="two fields">
        <div class="field">
          <label><?php echo Lang::$word->_METAKEYS;?></label>
          <textarea name="metakey<?php echo Lang::$lang;?>"><?php echo $row->{'metakey'.Lang::$lang};?></textarea>
        </div>
        <div class="field">
          <label><?php echo Lang::$word->_METADESC;?></label>
          <textarea name="metadesc<?php echo Lang::$lang;?>"><?php echo $row->{'metadesc'.Lang::$lang};?></textarea>
        </div>
      </div>
      <div class="veriasist double fitted divider"></div>
      <button type="button" name="dosubmit" class="veriasist positive button"><?php echo Lang::$word->_MOD_DS_UDT;?></button>
      <a href="index.php?do=modules&amp;action=config&amp;modname=digishop" class="veriasist basic button"><?php echo Lang::$word->_CANCEL;?></a>
      <input name="id" type="hidden" value="<?php echo Filter::$id;?>">
      <input name="processProduct" type="hidden" value="1">
    </form>
  </div>
  <div id="msgholder"></div>
</div>
<?php break;?>
<?php case"add": ?>
<?php $catrow = Registry::get("Digishop")->getCategories();?>
<div class="veriasist icon heading message blue"> <a class="helper veriasist top right info corner label" data-help="digishop"><i class="icon help"></i></a> <i class="puzzle piece icon"></i>
  <div class="content">
    <div class="header"> <?php echo Lang::$word->_MOD_DS_TITLE1;?> </div>
    <div class="veriasist breadcrumb"><i class="icon home"></i> <a href="index.php" class="section"><?php echo Lang::$word->_N_DASH;?></a>
      <div class="divider"> / </div>
      <a href="index.php?do=modules" class="section"><?php echo Lang::$word->_N_MODS;?></a>
      <div class="divider"> / </div>
      <a href="index.php?do=modules&amp;action=config&amp;modname=digishop" class="section"><?php echo $content->getModuleName(Filter::$modname);?></a>
      <div class="divider"> / </div>
      <div class="active section"><?php echo Lang::$word->_MOD_DS_TITLE3;?></div>
    </div>
  </div>
</div>
<div class="veriasist-large-content">
  <div class="veriasist message"><?php echo Core::langIcon();?><?php echo Lang::$word->_MOD_DS_INFO3. Lang::$word->_REQ1 . '<i class="icon asterisk"></i>' . Lang::$word->_REQ2;?></div>
  <div class="veriasist form segment">
    <div class="veriasist header"><?php echo Lang::$word->_MOD_DS_SUBTITLE3;?></div>
    <div class="veriasist double fitted divider"></div>
    <form id="veriasist_form" name="veriasist_form" method="post">
      <div class="two fields">
        <div class="field">
          <label><?php echo Lang::$word->_MOD_DS_NAME;?></label>
          <label class="input"><i class="icon-append icon asterisk"></i>
            <input type="text" placeholder="<?php echo Lang::$word->_MOD_DS_NAME;?>" name="title<?php echo Lang::$lang;?>">
          </label>
        </div>
        <div class="field">
          <label><?php echo Lang::$word->_MOD_DS_SLUG;?></label>
          <label class="input">
            <input type="text" placeholder="<?php echo Lang::$word->_MOD_DS_SLUG;?>" name="slug">
          </label>
        </div>
      </div>
      <div class="two fields">
        <div class="field">
          <label><?php echo Lang::$word->_MOD_DS_CATEGORY;?></label>
          <select name="cid">
            <option value=""><?php echo Lang::$word->_MOD_DS_CATEGORY_SEL;?></option>
            <?php if($catrow):?>
            <?php foreach($catrow as $crow):?>
            <option value="<?php echo $crow->id;?>"><?php echo $crow->{'name'.Lang::$lang};?></option>
            <?php endforeach;?>
            <?php unset($crow);?>
            <?php endif;?>
          </select>
        </div>
        <div class="field">
          <label><?php echo Lang::$word->_MOD_DS_PRODPRICE;?></label>
          <label class="input"><i class="icon-prepend icon dollar"></i><i class="icon-append icon asterisk"></i>
            <input type="text" placeholder="<?php echo Lang::$word->_MOD_DS_PRODPRICE;?>" name="price">
          </label>
        </div>
      </div>
      <div class="two fields">
        <div class="field">
          <label><?php echo Lang::$word->_MOD_DS_MEMBERSHIP;?></label>
          <?php echo $member->getMembershipList();?> </div>
        <div class="field">
          <?php $module_data = 0;?>
          <?php include(BASEPATH . "sistemyonetimi/modules/gallery/config.php");?>
        </div>
      </div>
      <div class="two fields">
        <div class="field">
          <label><?php echo Lang::$word->_MOD_DS_PRODIMG;?></label>
          <label class="input">
            <input type="file" name="thumb" class="filefield">
          </label>
        </div>
        <div class="field">
          <label><?php echo Lang::$word->_MOD_DS_PRODFILE;?></label>
          <label class="input">
            <input type="file" name="filename" class="filefield">
          </label>
        </div>
      </div>

      <!-- backgroun image-->
        <div class="field">
          <label><?php echo Lang::$word->_MOD_DS_PRODIMG;?> (Arkaplan)</label>
          <label class="input">
            <input type="file" name="thumbbg" class="filefield">
          </label>
        </div>



      <div class="veriasist double fitted divider"></div>
      <div class="field">
        <label><?php echo Lang::$word->_MOD_DS_PRODDESC;?></label>
        <textarea class="plugpost" placeholder="<?php echo Lang::$word->_MOD_DS_PRODDESC;?>" name="body<?php echo LANG::$lang;?>"></textarea>
      </div>
      <div class="two fields">
        <div class="field">
          <label><?php echo Lang::$word->_METAKEYS;?></label>
          <textarea placeholder="<?php echo Lang::$word->_METAKEYS;?>" name="metakey<?php echo Lang::$lang;?>"></textarea>
        </div>
        <div class="field">
          <label><?php echo Lang::$word->_METADESC;?></label>
          <textarea placeholder="<?php echo Lang::$word->_METADESC;?>" name="metadesc<?php echo Lang::$lang;?>"></textarea>
        </div>
      </div>
      <div class="field">
        <label><?php echo Lang::$word->_MOD_DS_PUBLISHED;?></label>
        <div class="inline-group">
          <label class="radio">
            <input name="active" type="radio" value="1" checked="checked">
            <i></i><?php echo Lang::$word->_YES;?></label>
          <label class="radio">
            <input name="active" type="radio" value="0">
            <i></i><?php echo Lang::$word->_NO;?></label>
        </div>
      </div>
      <div class="veriasist double fitted divider"></div>
      <button type="button" name="dosubmit" class="veriasist positive button"><?php echo Lang::$word->_MOD_DS_ADD;?></button>
      <a href="index.php?do=modules&amp;action=config&amp;modname=digishop" class="veriasist basic button"><?php echo Lang::$word->_CANCEL;?></a>
      <input name="processProduct" type="hidden" value="1">
    </form>
  </div>
  <div id="msgholder"></div>
</div>
<?php break;?>
<?php default: ?>
<?php $prodrow = Registry::get("Digishop")->getProducts();?>
<?php $catrow = Registry::get("Digishop")->getCategories();?>
<div class="veriasist icon heading message blue"> <i class="puzzle piece icon"></i>
  <div class="content">
    <div class="header"> <?php echo Lang::$word->_MOD_DS_TITLE1;?> </div>
    <div class="veriasist breadcrumb"><i class="icon home"></i> <a href="index.php" class="section"><?php echo Lang::$word->_N_DASH;?></a>
      <div class="divider"> / </div>
      <a href="index.php?do=modules" class="section"><?php echo Lang::$word->_N_MODS;?></a>
      <div class="divider"> / </div>
      <div class="active section"><?php echo $content->getModuleName(Filter::$modname);?></div>
    </div>
  </div>
</div>
<div class="veriasist-large-content">
  <div class="veriasist message"><?php echo Core::langIcon();?><?php echo Lang::$word->_MOD_DS_INFO1;?></div>
  <div class="veriasist segment">
    <div class="push-right">
      <div class="veriasist right pointing dropdown icon info button"> <i class="settings icon"></i>
        <div class="menu"> <a class="item" href="<?php echo Core::url("modules", "add");?>"><i class="icon add"></i><?php echo Lang::$word->_MOD_DS_ADDNEW;?></a> <a class="item" href="<?php echo Core::url("modules", "category");?>"><i class="icon add"></i><?php echo Lang::$word->_MOD_DS_MANAGE_CAT;?></a> <a class="item" href="<?php echo Core::url("modules", "transactions");?>"><i class="icon payment"></i><?php echo Lang::$word->_MOD_DS_VIEWTRANS;?></a> <a class="item" href="<?php echo Core::url("modules", "config");?>"><i class="icon setting"></i><?php echo Lang::$word->_MOD_DS_MAN_CONFIG;?></a> </div>
      </div>
    </div>
    <div class="veriasist header"><?php echo Lang::$word->_MOD_DS_SUBTITLE1;?></div>
    <div class="veriasist fitted divider"></div>
    <div class="veriasist small form basic segment">
      <form method="post" id="veriasist_form" name="veriasist_form">
        <div class="four fields">
          <div class="field">
            <div class="veriasist input"> <i class="icon-prepend icon calendar"></i>
              <input name="fromdate" type="text" data-datepicker="true" placeholder="<?php echo Lang::$word->_UR_SHOW_FROM;?>" id="fromdate" />
            </div>
          </div>
          <div class="field">
            <div class="veriasist action input"> <i class="icon-prepend icon calendar"></i>
              <input name="enddate" type="text" data-datepicker="true" placeholder="<?php echo Lang::$word->_UR_SHOW_TO;?>" id="enddate" />
              <a id="doDates" class="veriasist icon button"><?php echo Lang::$word->_SR_SEARCH_GO;?></a> </div>
          </div>
          <div class="field">
            <select name="cid" onchange="if(this.value!='NA') window.location='index.php?do=modules&amp;action=config&amp;modname=digishop&amp;id='+this[this.selectedIndex].value; else window.location='index.php?do=modules&amp;action=config&amp;modname=digishop';" id="cat_id">
              <option value="NA"><?php echo Lang::$word->_MOD_DS_FILTER_RESET;?></option>
              <?php if($catrow):?>
              <?php foreach($catrow as $crow):?>
              <?php $sel = ($crow->id == Filter::$id) ? ' selected="selected"' : '' ;?>
              <option value="<?php echo $crow->id;?>"<?php echo $sel;?>><?php echo $crow->{'name'.Lang::$lang};?></option>
              <?php endforeach;?>
              <?php unset($crow);?>
              <?php endif;?>
            </select>
          </div>
          <div class="field">
            <div class="two fields">
              <div class="field"> <?php echo $pager->items_per_page();?> </div>
              <div class="field"> <?php echo $pager->jump_menu();?> </div>
            </div>
          </div>
        </div>
      </form>
      <div class="veriasist fitted divider"></div>
    </div>
    <table class="veriasist sortable table">
      <thead>
        <tr>
          <th class="disabled"><i class="icon photo"></i></th>
          <th data-sort="string"><?php echo Lang::$word->_MOD_DS_NAME;?></th>
          <th data-sort="string"><?php echo Lang::$word->_MOD_DS_CATEGORY;?></th>
          <th data-sort="int"><?php echo Lang::$word->_MOD_DS_SALES;?></th>
          <th data-sort="int"><?php echo Lang::$word->_MOD_DS_PRICE;?></th>
          <th data-sort="int"><?php echo Lang::$word->_CREATED;?></th>
          <th class="disabled"><?php echo Lang::$word->_ACTIONS;?></th>
        </tr>
      </thead>
      <tbody>
        <?php if(!$prodrow):?>
        <tr>
          <td colspan="7"><?php echo Filter::msgSingleAlert(Lang::$word->_MOD_DS_NOPRODUCTS);?></td>
        </tr>
        <?php else:?>
        <?php foreach ($prodrow as $row):?>
        <tr>
          <td><a class="lightbox" href="<?php echo SITEURL . '/' . Digishop::imagepath . $row->thumb;?>" title="<?php echo $row->{'title'.Lang::$lang};?>"> <img src="<?php echo SITEURL;?>/thumbmaker.php?src=<?php echo SITEURL . '/' . Digishop::imagepath . $row->thumb;?>&amp;w=120&amp;h=60" alt="" class="veriasist image"></a></td>
          <td><?php echo $row->{'title'.Lang::$lang};?></td>
          <td><a href="<?php echo Core::url("modules", "catedit", $row->cid);?>"><?php echo $row->catname;?></a></td>
          <td><span class="veriasist black label"><?php echo $row->sales;?></span></td>
          <td data-sort-value="<?php echo $row->price;?>"><?php echo $core->formatMoney($row->price);?></td>
          <td data-sort-value="<?php echo strtotime($row->created);?>"><?php echo Filter::dodate("short_date", $row->created);?></td>
          <td><a href="<?php echo Core::url("modules", "edit", $row->id);?>"><i class="rounded inverted success icon pencil link"></i></a> <a class="delete" data-title="<?php echo Lang::$word->_DELETE.' '.Lang::$word->_MOD_DS_PRODUCT;?>" data-option="deleteProduct" data-id="<?php echo $row->id;?>" data-name="<?php echo $row->{'title' . Lang::$lang};?>"><i class="rounded danger inverted remove icon link"></i></a></td>
        </tr>
        <?php endforeach;?>
        <?php unset($row);?>
        <?php endif;?>
      </tbody>
    </table>
  </div>
  <div class="veriasist-grid">
    <div class="two columns horizontal-gutters">
      <div class="row"> <span class="veriasist label"><?php echo Lang::$word->_PAG_TOTAL.': '.$pager->items_total;?> / <?php echo Lang::$word->_PAG_CURPAGE.': '.$pager->current_page.' '.Lang::$word->_PAG_OF.' '.$pager->num_pages;?></span> </div>
      <div class="row">
        <div id="pagination"><?php echo $pager->display_pages();?></div>
      </div>
    </div>
  </div>
</div>
<?php break;?>
<?php endswitch;?>
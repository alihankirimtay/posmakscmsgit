<?php
  /**
   * Language Data Add
   *
   * @package VeriAsist
   * @author veriasist.com
   * @copyright 2014
   * @version $Id: lang-add.php, v4.00 2014-04-20 10:12:05 gewa Exp $
   */
  if (!defined("_VALID_PHP"))
      die('Direct access to this location is not allowed.');
?>
<?php
  self::$db->query("LOCK TABLES mod_timeline_items WRITE");
  self::$db->query("ALTER TABLE mod_timeline_items ADD COLUMN title_$flag_id VARCHAR(100) NOT NULL AFTER title_en");
  self::$db->query("ALTER TABLE mod_timeline_items ADD COLUMN body_$flag_id TEXT AFTER body_en");
  self::$db->query("UNLOCK TABLES");

  if ($mod_timeline_items = self::$db->fetch_all("SELECT * FROM mod_timeline_items")) {
      foreach ($mod_timeline_items as $row) {
          $data['title_' . $flag_id] = $row->title_en;
          $data['body_' . $flag_id] = $row->body_en;
          self::$db->update("mod_timeline_items", $data, "id = " . $row->id);
      }
      unset($data, $row);
  }
?>
<?php
  /**
   * Timeline Class
   *
   * @package VeriAsist
   * @author veriasist.com
   * @copyright 2015
   * @version $Id: class_admin.php, v4.00 2015-01-24 19:18:02 gewa Exp $
   */

  if (!defined("_VALID_PHP"))
      die('Direct access to this location is not allowed.');

  class Timeline
  {

      const mTable = "mod_timeline";
	  const cTable = "mod_timeline_items";
	  const pluginspath = "plugins/";
	  const pltmpath = "plugins/timeline/_templates/";
	  
	  private static $db;


      /**
       * Timeline::__construct()
       * 
       * @return
       */
      function __construct()
      {
		  self::$db = Registry::get("Database");
      }

      /**
       * Timeline::getTimelines()
       * 
       * @return
       */
      public function getTimelines()
      {

          $sql = "SELECT * FROM " . self::mTable . " ORDER BY name";
          $row = self::$db->fetch_all($sql);

          return ($row) ? $row : 0;
      }
	  

      /**
       * Timeline::updatePlugin()
       * 
       * @return
       */
      public function updatePlugin()
      {

          Filter::checkPost('name', Lang::$word->_MOD_TM_NAME);
		  
		  switch($_POST['type']) {
			  case "rss":
			      Filter::checkPost('rssurl', Lang::$word->_MOD_TM_RSS);
			  break;
			  
			  case "facebook":
			      Filter::checkPost('fbpage', Lang::$word->_MOD_TM_FBPID);
				  Filter::checkPost('fbtoken', Lang::$word->_MOD_TM_FBTOKEN);
			  break;
		  }
          if (empty(Filter::$msgs)) {
			  $data = array(
				  'name' => sanitize($_POST['name']), 
				  'limiter' => intval($_POST['limiter']),
				  'showmore' => intval($_POST['showmore']),
				  'maxitems' => intval($_POST['maxitems']),
				  'colmode' => sanitize($_POST['colmode']),
				  'rssurl' => isset($_POST['rssurl']) ? sanitize($_POST['rssurl']) : "NULL",
				  'fbpage' => isset($_POST['fbpage']) ? sanitize($_POST['fbpage']) : 0,
				  'fbtoken' => isset($_POST['fbtoken']) ? sanitize($_POST['fbtoken']) : "NULL",
			  );
              
			  $pdata['alt_class'] = sanitize($_POST['alt_class']);
			  self::$db->update(Content::plTable, $pdata, "plugalias = '" . sanitize($_POST['plugin_id']) . "'");
			  
			  $result = self::$db->update(self::mTable, $data, "id=" . Filter::$id);
			  $message = Lang::$word->_MOD_TM_UPDATED;

			  if ($result) {
				  Security::writeLog($message, "", "no", "module");
				  $json['type'] = 'success';
				  $json['message'] = Filter::msgOk($message, false);
			  } else {
				  $json['type'] = 'success';
				  $json['message'] = Filter::msgAlert(Lang::$word->_SYSTEM_PROCCESS, false);
			  }
			  print json_encode($json);

		  } else {
			  $json['message'] = Filter::msgStatus();
			  print json_encode($json);
		  }
      }
	  
      /**
       * Timeline::addPlugin()
       * 
       * @return
       */
      public function addPlugin()
      {

          Filter::checkPost('name', Lang::$word->_MOD_TM_NAME);
		  
		  switch($_POST['type']) {
			  case "rss":
			      Filter::checkPost('rssurl', Lang::$word->_MOD_TM_RSS);
			  break;
			  
			  case "facebook":
			      Filter::checkPost('fbpage', Lang::$word->_MOD_TM_FBPID);
				  Filter::checkPost('fbtoken', Lang::$word->_MOD_TM_FBTOKEN);
			  break;
		  }
          if (empty(Filter::$msgs)) {
			  $data = array(
				  'name' => sanitize($_POST['name']), 
				  'type' => sanitize($_POST['type']),
				  'plugin_id' => "timeline/" . randName(1),
				  'limiter' => intval($_POST['limiter']),
				  'showmore' => intval($_POST['showmore']),
				  'maxitems' => intval($_POST['maxitems']),
				  'colmode' => sanitize($_POST['colmode']),
				  'rssurl' => isset($_POST['rssurl']) ? sanitize($_POST['rssurl']) : "NULL",
				  'fbpage' => isset($_POST['fbpage']) ? sanitize($_POST['fbpage']) : 0,
				  'fbtoken' => isset($_POST['fbtoken']) ? sanitize($_POST['fbtoken']) : "NULL",
			  );
			  
			  $pdata = array(
				  'title' . Lang::$lang => sanitize($_POST['name']),
				  'show_title' => 0,
				  'alt_class' => sanitize($_POST['alt_class']),
				  'system' => 0,
				  'cplugin' => 1,
				  'plugalias' => $data['plugin_id'],
				  'hasconfig' => 0,
				  'active' => 1,
				  'created' => "NOW()"
			  ); 
			  
              $last_id = self::$db->insert(Content::plTable, $pdata);
			  
			  mkdir(BASEPATH . self::pluginspath . $data['plugin_id']);
			  $plugin_file_main = BASEPATH . self::pluginspath . $data['plugin_id'] . '/main.php';
			  
			  switch($_POST['type']) {
				  case "news_module":
					  $plugin_file = BASEPATH . self::pltmpath . '_news.php';
					  file_put_contents($plugin_file_main, str_replace('###ID###', $last_id, file_get_contents($plugin_file)));
				  break;
				  
				  case "event_module":
					  $plugin_file = BASEPATH . self::pltmpath . '_event.php';
					  file_put_contents($plugin_file_main, str_replace('###ID###', $last_id, file_get_contents($plugin_file)));
				  break;
				  
				  case "rss":
					  $plugin_file = BASEPATH . self::pltmpath . '_rss.php';
					  file_put_contents($plugin_file_main, str_replace('###ID###', $last_id, file_get_contents($plugin_file)));
				  break;
				  
				  case "portfolio_module":
					  $plugin_file = BASEPATH . self::pltmpath . '_portfolio.php';
					  file_put_contents($plugin_file_main, str_replace('###ID###', $last_id, file_get_contents($plugin_file)));
				  break;
				  
				  case "facebook":
					  $plugin_file = BASEPATH . self::pltmpath . '_facebook.php';
					  file_put_contents($plugin_file_main, str_replace('###ID###', $last_id, file_get_contents($plugin_file)));
				  break;
				  
				  case "newsslider_module":
					  $plugin_file = BASEPATH . self::pltmpath . '_news.php';
					  file_put_contents($plugin_file_main, str_replace('###ID###', $last_id, file_get_contents($plugin_file)));
				  break;
				  
				  case "custom":
					  $plugin_file = BASEPATH . self::pltmpath . '_custom.php';
					  file_put_contents($plugin_file_main, str_replace('###ID###', $last_id, file_get_contents($plugin_file)));
				  break;
			  }

			  $result = self::$db->insert(self::mTable, $data);
			  $message = Lang::$word->_MOD_TM_ADDED;
			  
			  if ($result) {
				  Security::writeLog($message, "", "no", "module");
				  $json['type'] = 'success';
				  $json['message'] = Filter::msgOk($message, false);
			  } else {
				  $json['type'] = 'success';
				  $json['message'] = Filter::msgAlert(Lang::$word->_SYSTEM_PROCCESS, false);
			  }
			  print json_encode($json);

		  } else {
			  $json['message'] = Filter::msgStatus();
			  print json_encode($json);
		  }
      }

      /**
       * Timeline::getItemsById()
       * 
       * @return
       */
      public function getItemsById()
      {

		  $sql = "
			  SELECT id, tid,
				CASE
				  WHEN type = 'news_post' 
				  THEN 'News Type' 
				  WHEN type = 'iframe' 
				  THEN 'Iframe' 
				  WHEN type = 'gallery' 
				  THEN 'Gallery' 
				  ELSE 'Unknown' 
				END type, title" . Lang::$lang . ", created
			  FROM " . self::cTable . " 
			  WHERE tid = " . Filter::$id . " ORDER BY created DESC";
			  
          $row = self::$db->fetch_all($sql);

          return ($row) ? $row : 0;
      }

      /**
       * Timeline::processItem()
       * 
       * @return
       */
      public function processItem()
      {

		  Filter::checkPost('title' . Lang::$lang, Lang::$word->_MOD_TM_INAME);
		  
		  switch($_POST['type']) {
			  case "iframe":
			      Filter::checkPost('dataurl', Lang::$word->_MOD_TM_IFRAME);
			  break;
			  
			  case "gallery":
			      Filter::checkPost('images', Lang::$word->_MOD_TM_IIMG);
			  break;
		  }
          if (empty(Filter::$msgs)) {
			  $data = array(
				  'title' . Lang::$lang => sanitize($_POST['title' . Lang::$lang]),
				  'type' => sanitize($_POST['type']),
				  'tid' => intval($_POST['tid']),
				  'images' => isset($_POST['images']) ? sanitize($_POST['images']) : "NULL",
				  'body' . Lang::$lang => isset($_POST['body' . Lang::$lang]) ? str_replace("&lt;p&gt;&lt;/p&gt;", "", Filter::in_url($_POST['body' . Lang::$lang])) : "NULL",
				  'dataurl' => isset($_POST['dataurl']) ? sanitize($_POST['dataurl']) : "NULL",
				  'height' => isset($_POST['height']) ? intval($_POST['height']) : 300,
				  'readmore' => sanitize($_POST['readmore']),
				  'created' => sanitize($_POST['dcreated_submit']) . ' ' . sanitize($_POST['tcreated_submit'])
			  );

			  (Filter::$id) ? self::$db->update(self::cTable, $data, "id=" . Filter::$id) : $lastid = self::$db->insert(self::cTable, $data);
			  $message = (Filter::$id) ? Lang::$word->_MOD_TM_ITMUPDATED : Lang::$word->_MOD_TM_ITMADDED;

			  if (self::$db->affected()) {
				  Security::writeLog($message, "", "no", "module");
				  $json['type'] = 'success';
				  $json['message'] = Filter::msgOk($message, false);
			  } else {
				  $json['type'] = 'success';
				  $json['message'] = Filter::msgAlert(Lang::$word->_SYSTEM_PROCCESS, false);
			  }
			  print json_encode($json);

		  } else {
			  $json['message'] = Filter::msgStatus();
			  print json_encode($json);
		  }
      }
	  
      /**
       * Timeline::getImgList()
       * 
       * @return
       */ 	  
	  public static function getImgList($data, $class)
	  {
	
		  $imagedata = explode(",", $data->images);
		  $images = '';
		  if ($data->images) {
			  foreach ($imagedata as $row) {
				  $images .= '<a href="' . UPLOADURL . 'timeline/' . $row . '" class="lightbox"><img src="' . UPLOADURL . 'timeline/' . $row . '" class="' . $class . '" alt=""></a>';
			  }
		  }
		  return $images;
	  }
	  
      /**
       * Timeline::getImagePicker()
       * 
       * @param mixed $dir
       * @return
       */
	  public static function getImagePicker($dir)
	  {
		  $filedata = Filemanager::scanFiles($dir, array("jpg","png","gif"));
	
		  $html = '';
		  if ($filedata) {
			  natsort($filedata);
			  $html = '<div id="imgGrid" style="max-width:480px">';
			  $html .= '<ul class="clearfix">';
			  foreach ($filedata as $row):
				  $row = Filemanager::fixpath($row);
				  $upl = Filemanager::fixpath(UPLOADS);
				  $data = str_replace($upl, "", $row);
				  $img = '<img src="' . UPLOADURL . $data . '">';
                  
				  $html .= '<li><label class="checkbox">';
				  $html .= '<input name="imgs[]" type="checkbox" value="' . str_replace("timeline/", "", $data) . '">';
				  $html .= '<i></i></label><a data-path="' . $data . '">' . $img . '</a></li>';
	
			  endforeach;
			  $html .= '</ul>';
			  $html .= '</div>';
		  }
	
		  print $html;
	
	  }
	  
      /**
       * Timeline::themeList()
       * 
       * @return
       */ 	  
	  public static function themeList($plugin_id = false)
	  {
	
	      if($plugin_id) {
			  $plg = getValue("alt_class", Content::plTable, "plugalias = '$plugin_id'");
		  }
		  $arr = array(
			  'default_theme' => "Default Theme",
			  'dark_theme' => "Dark Theme",
			  'basic_theme' => "Basic Theme",
			  'light_theme' => "Light Theme",
			  );
	
		  $html = '';
		  foreach ($arr as $key => $val) {
			  $html .= "<option value=\"$key\"";
			  $html .= ($plugin_id and $plg == $key) ? ' selected="selected"' : '';
			  $html .= ">$val</option>\n";
		  }
	
		  unset($val);
		  return $html;
	  }

      /**
       * Timeline::columnList()
       * 
       * @return
       */ 	  
	  public static function columnList($selected = false)
	  {
		  $arr = array(
			  'dual' => "Dual Column",
			  'left' => "Left Column",
			  'right' => "Right Column",
			  'center' => "Center Column",
			  );
	
		  $html = '';
		  foreach ($arr as $key => $val) {
			  $html .= "<option value=\"$key\"";
			  $html .= ($selected == $key) ? ' selected="selected"' : '';
			  $html .= ">$val</option>\n";
		  }
	
		  unset($val);
		  return $html;
	  }
	  
	  
      /**
       * Timeline::typeList()
       * 
       * @return
       */ 	  
	  public static function typeList()
	  {
	
		  $barray = array();
		  if (Core::checkTable("mod_news")) {
			  $barray = array('news_module' => "News Module");
		  }
		  $parray = array();
		  if (Core::checkTable("mod_portfolio")) {
			  $parray = array('portfolio_module' => "Portfolio Module");
		  }
	
		  $arr = array(
			  'event_module' => "Event Module",
			  'rss' => "Rss Feed",
			  'newsslider_module' => "Newsslider Module",
			  'facebook' => "Facebook Page",
			  'custom' => "Custom Timeline",
			  );
	
		  $html = '';
		  $array = array_merge($arr, $barray, $parray);
		  foreach ($array as $key => $val) {
			  $html .= "<option value=\"$key\">$val</option>\n";
		  }
	
		  unset($val);
		  return $html;
	  }
	  
  }
?>
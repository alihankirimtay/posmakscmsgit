<?php
  /**
   * Timeline
   *
   * @package VeriAsist
   * @author veriasist.com
   * @copyright 2015
   * @version $Id: class_admin.php, v4.00 2015-01-25 12:12:12 gewa Exp $
   */
  if (!defined("_VALID_PHP"))
      die('Direct access to this location is not allowed.');
  
  if(!$user->getAcl("timeline")): print Filter::msgAlert(Lang::$word->_CG_ONLYADMIN); return; endif;
  
  Registry::set('Timeline', new Timeline());
?>
<?php switch(Filter::$maction): case "manage": ?>
<?php $single = Core::getRowById(Timeline::mTable, Filter::$id);?>
<?php $datarow = Registry::get("Timeline")->getItemsById();?>
<div class="veriasist icon heading message blue"> <i class="puzzle piece icon"></i>
  <div class="content">
    <div class="header"> <?php echo Lang::$word->_MOD_TM_TITLE4;?> </div>
    <div class="veriasist breadcrumb"><i class="icon home"></i> <a href="index.php" class="section"><?php echo Lang::$word->_N_DASH;?></a>
      <div class="divider"> / </div>
      <a href="index.php?do=modules" class="section"><?php echo Lang::$word->_N_MODS;?></a>
      <div class="divider"> / </div>
      <a href="index.php?do=modules&amp;action=config&amp;modname=timeline" class="section"><?php echo $content->getModuleName(Filter::$modname);?></a>
      <div class="divider"> / </div>
      <div class="active section"><?php echo Lang::$word->_MOD_TM_TITLE5;?></div>
    </div>
  </div>
</div>
<div class="veriasist-large-content">
<div class="veriasist message"><?php echo Core::langIcon();?><?php echo Lang::$word->_MOD_TM_INFO5;?></div>
<div class="veriasist segment"> <a class="veriasist icon warning button push-right" href="index.php?do=modules&amp;action=config&amp;modname=timeline&amp;maction=tiadd&amp;tid=<?php echo Filter::$id;?>"><i class="icon add"></i> <?php echo Lang::$word->_MOD_TM_ADDBL;?></a>
  <div class="veriasist header"><?php echo Lang::$word->_MOD_TM_SUBTITLE4 . $single->name;?></div>
  <div class="veriasist fitted divider"></div>
  <table class="veriasist sortable table">
    <thead>
      <tr>
        <th data-sort="int">#</th>
        <th data-sort="string"><?php echo Lang::$word->_MOD_TM_INAME;?></th>
        <th data-sort="string"><?php echo Lang::$word->_MOD_TM_ITYPE;?></th>
        <th data-sort="int"><?php echo Lang::$word->_CREATED;?></th>
        <th class="disabled"><?php echo Lang::$word->_ACTIONS;?></th>
      </tr>
    </thead>
    <tbody>
      <?php if(!$datarow):?>
      <tr>
        <td colspan="5"><?php Filter::msgSingleInfo(Lang::$word->_MOD_TM_NOPLUGS);?></td>
      </tr>
      <?php else:?>
      <?php foreach ($datarow as $row):?>
      <tr>
        <td><?php echo $row->id;?>.</td>
        <td><?php echo $row->{'title' . Lang::$lang};?></td>
        <td><?php echo $row->type;?></td>
        <td data-sort-value="<?php echo strtotime($row->created);?>"><?php echo Filter::dodate("short_date", $row->created);?></td>
        <td><a href="<?php echo Core::url("modules", "tiedit", $row->id);?>"><i class="rounded inverted success icon pencil link"></i></a> <a class="delete" data-title="<?php echo Lang::$word->_DELETE.' '.Lang::$word->_MOD_TM_TMLITM;?>" data-option="deleteTmitem" data-id="<?php echo $row->id;?>" data-name="<?php echo $row->{'title' . Lang::$lang};?>"><i class="rounded danger inverted remove icon link"></i></a></td>
      </tr>
      <?php endforeach;?>
      <?php unset($row);?>
      <?php endif;?>
    </tbody>
  </table>
</div>
<?php break;?>
<?php case"tiedit": ?>
<?php $row = Core::getRowById(Timeline::cTable, Filter::$id);?>
<div class="veriasist icon heading message blue"> <a class="helper veriasist top right info corner label" data-help="tline"><i class="icon help"></i></a> <i class="puzzle piece icon"></i>
  <div class="content">
    <div class="header"> <?php echo Lang::$word->_MOD_TM_TITLE4;?> </div>
    <div class="veriasist breadcrumb"><i class="icon home"></i> <a href="index.php" class="section"><?php echo Lang::$word->_N_DASH;?></a>
      <div class="divider"> / </div>
      <a href="index.php?do=modules" class="section"><?php echo Lang::$word->_N_MODS;?></a>
      <div class="divider"> / </div>
      <a href="index.php?do=modules&amp;action=config&amp;modname=timeline" class="section"><?php echo $content->getModuleName(Filter::$modname);?></a>
      <div class="divider"> / </div>
      <a href="<?php echo Core::url("modules", "manage", $row->tid);?>" class="section"><?php echo Lang::$word->_MOD_TM_TITLE5;?></a>
      <div class="divider"> / </div>
      <div class="active section"><?php echo Lang::$word->_MOD_TM_TITLE6;?></div>
    </div>
  </div>
</div>
<div class="veriasist-large-content">
  <div class="veriasist message"><?php echo Core::langIcon();?><?php echo Lang::$word->_MOD_TM_INFO6. Lang::$word->_REQ1 . '<i class="icon asterisk"></i>' . Lang::$word->_REQ2;?></div>
  <div class="veriasist form segment">
    <div class="veriasist header"><?php echo Lang::$word->_MOD_TM_SUBTITLE5 . $row->{'title' . Lang::$lang};?></div>
    <div class="veriasist double fitted divider"></div>
    <form id="veriasist_form" name="veriasist_form" method="post">
      <div class="two fields">
        <div class="field">
          <label><?php echo Lang::$word->_MOD_TM_INAME;?></label>
          <label class="input"><i class="icon-append icon asterisk"></i>
            <input name="title<?php echo Lang::$lang;?>" type="text" value="<?php echo $row->{'title' . Lang::$lang};?>">
          </label>
        </div>
        <div class="field">
          <div class="two fields">
            <div class="field">
              <label><?php echo Lang::$word->_CREATED;?></label>
              <label class="input"><i class="icon-prepend icon calendar"></i>
                <input name="dcreated" data-datepicker="true" data-value="<?php echo $row->created;?>" type="text" value="<?php echo $row->created;?>">
              </label>
            </div>
            <div class="field">
              <label><?php echo Lang::$word->_CREATED;?></label>
              <label class="input"><i class="icon-prepend icon calendar"></i>
                <input name="tcreated" data-timepicker="true" data-value="<?php echo $row->created;?>" type="text" value="<?php echo $row->created;?>">
              </label>
            </div>
          </div>
        </div>
      </div>
      <?php if($row->type == "iframe" ):?>
      <div class="two fields">
        <div class="field">
          <label><?php echo Lang::$word->_MOD_TM_IFRAME;?></label>
          <textarea name="dataurl"><?php echo $row->dataurl;?></textarea>
        </div>
        <div class="field">
          <label><?php echo Lang::$word->_MOD_TM_IFRAMEH;?></label>
          <label class="input"><i class="icon-append icon asterisk"></i>
            <input name="height" type="text" value="<?php echo $row->height;?>">
          </label>
        </div>
      </div>
      <?php else:?>
      <div class="field">
        <label><?php echo Lang::$word->_MOD_TM_IIMG;?></label>
        <div id="oldImgs"><?php echo Timeline::getImgList($row, "veriasist normal image");?></div>
        <div class="veriasist divider"></div>
        <input name="images" type="hidden" id="doimg" value="<?php echo $row->images;?>">
        <a id="doimgs" class="veriasist basic button"><?php echo Lang::$word->_MOD_TM_SIMG;?></a> </div>
      <div class="veriasist divider"></div>
      <div class="field">
        <label><?php echo Lang::$word->_MOD_TM_BODY;?></label>
        <textarea id="plugpost" class="plugpost" name="body<?php echo Lang::$lang;?>"><?php echo Filter::out_url($row->{'body'.Lang::$lang});?></textarea>
      </div>
      <?php endif;?>
      <div class="field">
        <label><?php echo Lang::$word->_MOD_TM_READ_MOREU;?></label>
        <label class="input">
          <input name="readmore" type="text" value="<?php echo $row->readmore;?>">
        </label>
      </div>
      <div class="veriasist double fitted divider"></div>
      <button type="button" name="dosubmit" class="veriasist positive button"><?php echo Lang::$word->_MOD_TM_ITMUPDATE;?></button>
      <a href="<?php echo Core::url("modules", "manage", $row->tid);?>" class="veriasist basic button"><?php echo Lang::$word->_CANCEL;?></a>
      <input name="id" type="hidden" value="<?php echo Filter::$id;?>">
      <input name="type" type="hidden" value="<?php echo $row->type;?>">
      <input name="tid" type="hidden" value="<?php echo $row->tid;?>">
      <input name="processItem" type="hidden" value="1">
    </form>
  </div>
  <div id="msgholder"></div>
</div>
<script type="text/javascript"> 
// <![CDATA[
$(document).ready(function() {
    $('body').on('click', '.addimgs', function() {
        var output = $.map($(':checkbox[name=imgs\\[\\]]:checked'), function(n, i) {
            return n.value;
        }).join(',');

        if (output) {
            var myArray = output.split(',');
            result = '';
            for (var i = 0; i < myArray.length; i++) {
                result += '<a href="<?php echo UPLOADURL;?>timeline/' + myArray[i] + '" class="lightbox"><img src="<?php echo UPLOADURL;?>timeline/' + myArray[i] + '" class="veriasist normal image" alt=""></a>';
            }
            $('#oldImgs').html(result).fadeIn("slow");

            $('#oldImgs').waitForImages(function() {
                $('#oldImgs').gridalicious({
                    width: 150,
                    gutter: 10,
                    selector: 'a'
                });
            });
            $("#doimg").val(output);
        }
    });

    $('#oldImgs').waitForImages(function() {
        $('#oldImgs').gridalicious({
            width: 150,
            gutter: 10,
            selector: 'a'
        });
    });

    $('body').on('click', '#doimgs', function() {
        Messi.load('modules/timeline/controller.php', {
            pickImages: 1
        }, {
            title: "<?php echo Lang::$word->_MOD_TM_SIMG;?>",
            buttons: [{
                id: 0,
                label: '<?php echo Lang::$word->_MOD_TM_IMGADD;?>',
                class: 'positive addimgs',
                val: 'Y'
            }],
        });
    });
	
   $('body').on('click', '#imgGrid li', function() {
       var $checkbox = $(this).find(':checkbox');
       $checkbox.prop('checked', !$checkbox[0].checked);
    });  
});
// ]]>
</script>
<?php break;?>
<?php case"tiadd": ?>
<?php $row = Core::getRowById(Timeline::mTable, get('tid'));?>
<div class="veriasist icon heading message blue"> <a class="helper veriasist top right info corner label" data-help="tline"><i class="icon help"></i></a> <i class="puzzle piece icon"></i>
  <div class="content">
    <div class="header"> <?php echo Lang::$word->_MOD_TM_TITLE4;?> </div>
    <div class="veriasist breadcrumb"><i class="icon home"></i> <a href="index.php" class="section"><?php echo Lang::$word->_N_DASH;?></a>
      <div class="divider"> / </div>
      <a href="index.php?do=modules" class="section"><?php echo Lang::$word->_N_MODS;?></a>
      <div class="divider"> / </div>
      <a href="index.php?do=modules&amp;action=config&amp;modname=timeline" class="section"><?php echo $content->getModuleName(Filter::$modname);?></a>
      <div class="divider"> / </div>
      <a href="<?php echo Core::url("modules", "manage", $row->id);?>" class="section"><?php echo Lang::$word->_MOD_TM_TITLE5;?></a>
      <div class="divider"> / </div>
      <div class="active section"><?php echo Lang::$word->_MOD_TM_TITLE7;?></div>
    </div>
  </div>
</div>
<div class="veriasist-large-content">
  <div class="veriasist message"><?php echo Core::langIcon();?><?php echo Lang::$word->_MOD_TM_INFO7. Lang::$word->_REQ1 . '<i class="icon asterisk"></i>' . Lang::$word->_REQ2;?></div>
  <div class="veriasist form segment">
    <div class="veriasist header"><?php echo Lang::$word->_MOD_TM_SUBTITLE6;?></div>
    <div class="veriasist double fitted divider"></div>
    <form id="veriasist_form" name="veriasist_form" method="post">
      <div class="two fields">
        <div class="field">
          <label><?php echo Lang::$word->_MOD_TM_INAME;?></label>
          <label class="input"><i class="icon-append icon asterisk"></i>
            <input name="title<?php echo Lang::$lang;?>" type="text" placeholder="<?php echo Lang::$word->_MOD_TM_INAME;?>">
          </label>
        </div>
        <div class="field">
          <div class="two fields">
            <div class="field">
              <label><?php echo Lang::$word->_CREATED;?></label>
              <label class="input"><i class="icon-prepend icon calendar"></i>
                <input name="dcreated" data-datepicker="true" data-value="<?php echo date('Y-m-d');?>" type="text" value="<?php echo date('Y-m-d');?>">
              </label>
            </div>
            <div class="field">
              <label><?php echo Lang::$word->_CREATED;?></label>
              <label class="input"><i class="icon-prepend icon calendar"></i>
                <input name="tcreated" data-timepicker="true" data-value="<?php echo date('H:i:s');?>" type="text" value="<?php echo date('H:i:s');?>">
              </label>
            </div>
          </div>
        </div>
      </div>
      <div class="two fields">
        <div class="field">
          <label><?php echo Lang::$word->_MOD_TM_ITYPE;?></label>
          <select name="type" id="tmtype">
            <option value="news_post">News Type</option>
            <option value="iframe">iFrame</option>
            <option value="gallery">Gallery</option>
          </select>
        </div>
        <div class="field"></div>
      </div>
      <div class="two fields" id="iframe" style="display:none">
        <div class="field">
          <label><?php echo Lang::$word->_MOD_TM_IFRAME;?></label>
          <textarea name="dataurl"></textarea>
        </div>
        <div class="field">
          <label><?php echo Lang::$word->_MOD_TM_IFRAMEH;?></label>
          <label class="input"><i class="icon-append icon asterisk"></i>
            <input name="height" type="text" value="300">
          </label>
        </div>
      </div>
      <div class="field" id="imgfield">
        <label><?php echo Lang::$word->_MOD_TM_IIMG;?></label>
        <div id="oldImgs"></div>
        <div class="veriasist divider"></div>
        <input name="images" type="hidden" id="doimg" value="">
        <a id="doimgs" class="veriasist basic button"><?php echo Lang::$word->_MOD_TM_SIMG;?></a> </div>
      <div class="veriasist divider"></div>
      <div class="field" id="bodyfield">
        <label><?php echo Lang::$word->_MOD_TM_BODY;?></label>
        <textarea id="plugpost" class="plugpost" name="body<?php echo Lang::$lang;?>"></textarea>
      </div>
      <div class="field">
        <label><?php echo Lang::$word->_MOD_TM_READ_MOREU;?></label>
        <label class="input">
          <input name="readmore" type="text" placeholder="<?php echo Lang::$word->_MOD_TM_READ_MOREU;?>">
        </label>
      </div>
      <div class="veriasist double fitted divider"></div>
      <button type="button" name="dosubmit" class="veriasist positive button"><?php echo Lang::$word->_MOD_TM_ITMADD;?></button>
      <a href="<?php echo Core::url("modules", "manage", $row->id);?>" class="veriasist basic button"><?php echo Lang::$word->_CANCEL;?></a>
      <input name="tid" type="hidden" value="<?php echo $row->id;?>">
      <input name="processItem" type="hidden" value="1">
    </form>
  </div>
  <div id="msgholder"></div>
</div>
<script type="text/javascript"> 
// <![CDATA[
$(document).ready(function() {
    $('#tmtype').change(function() {
        var selected = $("#tmtype option:selected").val();
        if (selected == "iframe") {
            $('#iframe').show()
            $('#imgfield, #bodyfield').hide()
        } else if (selected == "gallery") {
            $('#iframe, #bodyfield').hide()
			$('#imgfield').show()
        } else {
            $('#iframe').hide()
			$('#bodyfield').show()
        }
    });
    $('body').on('click', '.addimgs', function() {
        var output = $.map($(':checkbox[name=imgs\\[\\]]:checked'), function(n, i) {
            return n.value;
        }).join(',');

        if (output) {
            var myArray = output.split(',');
            result = '';
            for (var i = 0; i < myArray.length; i++) {
                result += '<a href="<?php echo UPLOADURL;?>timeline/' + myArray[i] + '" class="lightbox"><img src="<?php echo UPLOADURL;?>timeline/' + myArray[i] + '" class="veriasist normal image" alt=""></a>';
            }
            $('#oldImgs').html(result).fadeIn("slow");

            $('#oldImgs').waitForImages(function() {
                $('#oldImgs').gridalicious({
                    width: 150,
                    gutter: 10,
                    selector: 'a'
                });
            });
            $("#doimg").val(output);
        }
    });

    $('#oldImgs').waitForImages(function() {
        $('#oldImgs').gridalicious({
            width: 150,
            gutter: 10,
            selector: 'a'
        });
    });

    $('body').on('click', '#doimgs', function() {
        Messi.load('modules/timeline/controller.php', {
            pickImages: 1
        }, {
            title: "<?php echo Lang::$word->_MOD_TM_SIMG;?>",
            buttons: [{
                id: 0,
                label: '<?php echo Lang::$word->_MOD_TM_IMGADD;?>',
                class: 'positive addimgs',
                val: 'Y'
            }],
        });
    });
	
   $('body').on('click', '#imgGrid li', function() {
       var $checkbox = $(this).find(':checkbox');
       $checkbox.prop('checked', !$checkbox[0].checked);
    });  
});
// ]]>
</script>
<?php break;?>
<?php case"edit": ?>
<?php $row = Core::getRowById(Timeline::mTable, Filter::$id);?>
<div class="veriasist icon heading message blue"> <a class="helper veriasist top right info corner label" data-help="tmconfig"><i class="icon help"></i></a> <i class="puzzle piece icon"></i>
  <div class="content">
    <div class="header"> <?php echo Lang::$word->_MOD_TM_TITLE4;?> </div>
    <div class="veriasist breadcrumb"><i class="icon home"></i> <a href="index.php" class="section"><?php echo Lang::$word->_N_DASH;?></a>
      <div class="divider"> / </div>
      <a href="index.php?do=modules" class="section"><?php echo Lang::$word->_N_MODS;?></a>
      <div class="divider"> / </div>
      <a href="index.php?do=modules&amp;action=config&amp;modname=timeline" class="section"><?php echo $content->getModuleName(Filter::$modname);?></a>
      <div class="divider"> / </div>
      <div class="active section"><?php echo Lang::$word->_MOD_TM_TITLE1;?></div>
    </div>
  </div>
</div>
<div class="veriasist-large-content">
  <div class="veriasist message"><?php echo Core::langIcon();?><?php echo Lang::$word->_MOD_TM_INFO1. Lang::$word->_REQ1 . '<i class="icon asterisk"></i>' . Lang::$word->_REQ2;?></div>
  <div class="veriasist form segment">
    <div class="veriasist header"><?php echo Lang::$word->_MOD_TM_SUBTITLE1 . $row->name;?></div>
    <div class="veriasist double fitted divider"></div>
    <form id="veriasist_form" name="veriasist_form" method="post">
      <div class="two fields">
        <div class="field">
          <label><?php echo Lang::$word->_MOD_TM_NAME;?></label>
          <label class="input"><i class="icon-append icon asterisk"></i>
            <input type="text" value="<?php echo $row->name;?>" name="name">
          </label>
        </div>
        <div class="field">
          <label><?php echo Lang::$word->_MOD_TM_TMTYPE;?></label>
          <label class="input">
            <input name="type" type="text" disabled value="<?php echo $row->type;?>" readonly>
          </label>
        </div>
      </div>
      <div class="three fields">
        <div class="field">
          <label><?php echo Lang::$word->_MOD_TM_LIMITER;?></label>
          <label class="input">
            <input type="text" class="slrange" value="<?php echo $row->limiter;?>" name="limiter">
          </label>
        </div>
        <div class="field">
          <label><?php echo Lang::$word->_MOD_TM_MAX;?></label>
          <label class="input">
            <input type="text" class="slrange" value="<?php echo $row->maxitems;?>" name="maxitems">
          </label>
        </div>
        <div class="field">
          <label><?php echo Lang::$word->_MOD_TM_LMODE;?></label>
          <label class="input">
            <input type="text" class="slrange" value="<?php echo $row->showmore;?>" name="showmore">
          </label>
        </div>
      </div>
      <div class="two fields">
        <div class="field">
          <label><?php echo Lang::$word->_MOD_TM_DTHEME;?></label>
          <select name="alt_class">
            <?php echo Timeline::themeList($row->plugin_id);?>
          </select>
        </div>
        <div class="field">
          <label><?php echo Lang::$word->_MOD_TM_LMODE;?></label>
          <select name="colmode">
            <?php echo Timeline::columnList($row->colmode);?>
          </select>
        </div>
      </div>
      <?php if($row->type == "facebook"):?>
      <div class="two fields">
        <div class="field">
          <label><?php echo Lang::$word->_MOD_TM_FBPID;?></label>
          <label class="input"><i class="icon-append icon asterisk"></i>
            <input type="text" value="<?php echo $row->fbpage;?>" name="fbpage">
          </label>
        </div>
        <div class="field">
          <label><?php echo Lang::$word->_MOD_TM_FBTOKEN;?></label>
          <label class="input"><i class="icon-append icon asterisk"></i>
            <input type="text" value="<?php echo $row->fbtoken;?>" name="fbtoken">
          </label>
        </div>
      </div>
      <?php endif;?>
      <?php if($row->type == "rss"):?>
      <div class="two fields">
        <div class="field">
          <label><?php echo Lang::$word->_MOD_TM_RSS;?></label>
          <label class="input"><i class="icon-append icon asterisk"></i>
            <input type="text" value="<?php echo $row->rssurl;?>" name="rssurl">
          </label>
        </div>
        <div class="field"> </div>
      </div>
      <?php endif;?>
      <div class="veriasist double fitted divider"></div>
      <button type="button" name="dosubmit" class="veriasist positive button"><?php echo Lang::$word->_MOD_TM_EDIT;?></button>
      <a href="index.php?do=modules&amp;action=config&amp;modname=timeline" class="veriasist basic button"><?php echo Lang::$word->_CANCEL;?></a>
      <input name="id" type="hidden" value="<?php echo Filter::$id;?>">
      <input name="updateTimeline" type="hidden" value="1">
      <input name="plugin_id" type="hidden" value="<?php echo $row->plugin_id;?>">
      <input name="type" type="hidden" value="<?php echo $row->type;?>">
    </form>
  </div>
  <div id="msgholder"></div>
</div>
<script type="text/javascript"> 
// <![CDATA[
$(document).ready(function () {
    $("input[name=limiter]").ionRangeSlider({
		min: 5,
		max: 50,
        step: 1,
		postfix: " itm",
        type: 'single',
        hasGrid: true
    });
    $("input[name=showmore], input[name=maxitems]").ionRangeSlider({
		min: 0,
		max: 20,
        step: 1,
		postfix: " itm",
        type: 'single',
        hasGrid: true
    });
});
// ]]>
</script>
<?php break;?>
<?php case"add": ?>
<div class="veriasist icon heading message blue"> <a class="helper veriasist top right info corner label" data-help="tmconfig"><i class="icon help"></i></a> <a class="helper veriasist top right info corner label" data-help="gmaps"><i class="icon help"></i></a> <i class="puzzle piece icon"></i>
  <div class="content">
    <div class="header"> <?php echo Lang::$word->_MOD_TM_TITLE4;?> </div>
    <div class="veriasist breadcrumb"><i class="icon home"></i> <a href="index.php" class="section"><?php echo Lang::$word->_N_DASH;?></a>
      <div class="divider"> / </div>
      <a href="index.php?do=modules" class="section"><?php echo Lang::$word->_N_MODS;?></a>
      <div class="divider"> / </div>
      <a href="index.php?do=modules&amp;action=config&amp;modname=timeline" class="section"><?php echo $content->getModuleName(Filter::$modname);?></a>
      <div class="divider"> / </div>
      <div class="active section"><?php echo Lang::$word->_MOD_TM_TITLE2;?></div>
    </div>
  </div>
</div>
<div class="veriasist-large-content">
  <div class="veriasist message"><?php echo Core::langIcon();?><?php echo Lang::$word->_MOD_TM_INFO2. Lang::$word->_REQ1 . '<i class="icon asterisk"></i>' . Lang::$word->_REQ2;?></div>
  <div class="veriasist form segment">
    <div class="veriasist header"><?php echo Lang::$word->_MOD_TM_SUBTITLE2;?></div>
    <div class="veriasist double fitted divider"></div>
    <form id="veriasist_form" name="veriasist_form" method="post">
      <div class="two fields">
        <div class="field">
          <label><?php echo Lang::$word->_MOD_TM_NAME;?></label>
          <label class="input"><i class="icon-append icon asterisk"></i>
            <input type="text" placeholder="<?php echo Lang::$word->_MOD_TM_NAME;?>" name="name">
          </label>
        </div>
        <div class="field">
          <label><?php echo Lang::$word->_MOD_TM_TMTYPE;?></label>
          <select name="type" id="tmtype">
            <?php echo Timeline::typeList();?>
          </select>
        </div>
      </div>
      <div class="three fields">
        <div class="field">
          <label><?php echo Lang::$word->_MOD_TM_LIMITER;?></label>
          <label class="input">
            <input type="text" class="slrange" name="limiter">
          </label>
        </div>
        <div class="field">
          <label><?php echo Lang::$word->_MOD_TM_MAX;?></label>
          <label class="input">
            <input type="text" class="slrange" name="maxitems">
          </label>
        </div>
        <div class="field">
          <label><?php echo Lang::$word->_MOD_TM_LMODE;?></label>
          <label class="input">
            <input type="text" class="slrange" name="showmore">
          </label>
        </div>
      </div>
      <div class="two fields">
        <div class="field">
          <label><?php echo Lang::$word->_MOD_TM_DTHEME;?></label>
          <select name="alt_class">
            <?php echo Timeline::themeList();?>
          </select>
        </div>
        <div class="field">
          <label><?php echo Lang::$word->_MOD_TM_LMODE;?></label>
          <select name="colmode">
            <?php echo Timeline::columnList();?>
          </select>
        </div>
      </div>
      <div id="fbconf" class="two fields" style="display:none">
        <div class="field">
          <label><?php echo Lang::$word->_MOD_TM_FBPID;?></label>
          <label class="input"><i class="icon-append icon asterisk"></i>
            <input type="text" placeholder="<?php echo Lang::$word->_MOD_TM_FBPID;?>" name="fbpage">
          </label>
        </div>
        <div class="field">
          <label><?php echo Lang::$word->_MOD_TM_FBTOKEN;?></label>
          <label class="input"><i class="icon-append icon asterisk"></i>
            <input type="text" placeholder="<?php echo Lang::$word->_MOD_TM_FBTOKEN;?>" name="fbtoken">
          </label>
        </div>
      </div>
      <div id="rssconf" class="two fields" style="display:none">
        <div class="field">
          <label><?php echo Lang::$word->_MOD_TM_RSS;?></label>
          <label class="input"><i class="icon-append icon asterisk"></i>
            <input type="text" placeholder="<?php echo Lang::$word->_MOD_TM_RSS;?>" name="rssurl">
          </label>
        </div>
        <div class="field"> </div>
      </div>
      <div class="veriasist double fitted divider"></div>
      <button type="button" name="dosubmit" class="veriasist positive button"><?php echo Lang::$word->_MOD_TM_ADD;?></button>
      <a href="index.php?do=modules&amp;action=config&amp;modname=timeline" class="veriasist basic button"><?php echo Lang::$word->_CANCEL;?></a>
      <input name="addTimeline" type="hidden" value="1">
    </form>
  </div>
  <div id="msgholder"></div>
</div>
<script type="text/javascript"> 
// <![CDATA[
$(document).ready(function() {
    $("input[name=limiter]").ionRangeSlider({
        min: 5,
        max: 50,
        step: 2,
        postfix: " itm",
        type: 'single',
        hasGrid: true
    });
    $("input[name=showmore], input[name=maxitems]").ionRangeSlider({
        min: 0,
        max: 20,
        step: 1,
        postfix: " itm",
        type: 'single',
        hasGrid: true
    });
    $('#tmtype').change(function() {
        var selected = $("#tmtype option:selected").val();
        if (selected == "facebook") {
            $('#fbconf').show()
            $('#rssconf').hide()
        } else if (selected == "rss") {
            $('#fbconf').hide()
            $('#rssconf').show()
        } else {
            $('#fbconf, #rssconf').hide()
        }
    });
});
// ]]>
</script>
<?php break;?>
<?php default: ?>
<?php $tmrow = Registry::get("Timeline")->getTimelines();?>
<div class="veriasist icon heading message blue"> <i class="puzzle piece icon"></i>
  <div class="content">
    <div class="header"> <?php echo Lang::$word->_MOD_TM_TITLE4;?> </div>
    <div class="veriasist breadcrumb"><i class="icon home"></i> <a href="index.php" class="section"><?php echo Lang::$word->_N_DASH;?></a>
      <div class="divider"> / </div>
      <a href="index.php?do=modules" class="section"><?php echo Lang::$word->_N_MODS;?></a>
      <div class="divider"> / </div>
      <div class="active section"><?php echo $content->getModuleName(Filter::$modname);?></div>
    </div>
  </div>
</div>
<div class="veriasist-large-content">
  <div class="veriasist message"><?php echo Core::langIcon();?><?php echo Lang::$word->_MOD_TM_INFO4;?></div>
  <div class="veriasist segment"> <a class="veriasist icon warning button push-right" href="<?php echo Core::url("modules", "add");?>"><i class="icon add"></i> <?php echo Lang::$word->_MOD_TM_ADD;?></a>
    <div class="veriasist header"><?php echo Lang::$word->_MOD_TM_SUBTITLE3;?></div>
    <div class="veriasist fitted divider"></div>
    <table class="veriasist sortable table">
      <thead>
        <tr>
          <th data-sort="int">#</th>
          <th data-sort="string"><?php echo Lang::$word->_MOD_TM_NAME;?></th>
          <th class="disabled"><?php echo Lang::$word->_ACTIONS;?></th>
        </tr>
      </thead>
      <tbody>
        <?php if(!$tmrow):?>
        <tr>
          <td colspan="3"><?php Filter::msgSingleInfo(Lang::$word->_MOD_TM_NOPLUGS);?></td>
        </tr>
        <?php else:?>
        <?php foreach ($tmrow as $row):?>
        <tr>
          <td><?php echo $row->id;?>.</td>
          <td><?php echo $row->name;?></td>
          <td><?php if($row->type == "custom"):?>
            <a href="<?php echo Core::url("modules", "manage", $row->id);?>"><i class="rounded inverted info icon sliders link"></i></a>
            <?php endif;?>
            <a href="<?php echo Core::url("modules", "edit", $row->id);?>"><i class="rounded inverted success icon pencil link"></i></a> <a class="delete" data-title="<?php echo Lang::$word->_DELETE.' '.Lang::$word->_MOD_TM_TMLINE;?>" data-option="deleteTmplugin" data-id="<?php echo $row->id;?>" data-name="<?php echo $row->name;?>"><i class="rounded danger inverted remove icon link"></i></a></td>
        </tr>
        <?php endforeach;?>
        <?php unset($row);?>
        <?php endif;?>
      </tbody>
    </table>
  </div>
</div>
<?php break;?>
<?php endswitch;?>
<?php
  /**
   * Controller
   *
   * @package VeriAsist
   * @author veriasist.com
   * @copyright 2015
   * @version $Id: controller.php, v4.00 2015-01-24 12:12:12 gewa Exp $
   */
  define("_VALID_PHP", true);

  require_once ("../../init.php");
  if (!$user->is_Admin())
      redirect_to("../../login.php");

  require_once("admin_class.php");
  Registry::set('Timeline', new Timeline());
?>
<?php
  /* == Update Plugin == */
  if (isset($_POST['updateTimeline'])):
      Registry::get("Timeline")->updatePlugin();
  endif;

  /* == Add Plugin == */
  if (isset($_POST['addTimeline'])):
      Registry::get("Timeline")->addPlugin();
  endif;
  
  /* == Delete Plugin  == */
  if (isset($_POST['delete']) and $_POST['delete'] == "deleteTmplugin"):
      $title = sanitize($_POST['title']);
      $name = getValuesById("plugin_id, type", Timeline::mTable, Filter::$id);

      if ($name):
          $plugin_file_current = BASEPATH . Timeline::pluginspath . $name->plugin_id . '/main.php';
		  if(is_file($plugin_file_current)):
              unlink($plugin_file_current);
              rmdir(str_replace('/main.php', '', $plugin_file_current));
		  endif;
              $db->delete(Content::plTable, "plugalias='" . $name->plugin_id  . "'");
      endif;
	  
      if($name->type == "custom") :
		  $db->delete(Timeline::cTable, "tid=" . Filter::$id);
	  endif;
	  
      $result = $db->delete(Timeline::mTable, "id=" . Filter::$id);
      if ($result):
          $json['type'] = 'success';
          $json['title'] = Lang::$word->_SUCCESS;
          $json['message'] = Lang::$word->_MOD_TM_TMLINE . ' /' . $title . '/ ' . Lang::$word->_DELETED;
          Security::writeLog(Lang::$word->_MOD_TM_TMLINE . ' /' . urldecode($title) . '/ ' . Lang::$word->_DELETED, "", "no", "module");
      else:
          $json['type'] = 'warning';
          $json['title'] = Lang::$word->_ALERT;
          $json['message'] = Lang::$word->_SYSTEM_PROCCESS;
      endif;
      print json_encode($json);
  endif;

  /* == Delete Item  == */
  if (isset($_POST['delete']) and $_POST['delete'] == "deleteTmitem"):
      $title = sanitize($_POST['title']);
	  
      $result = $db->delete(Timeline::cTable, "id=" . Filter::$id);
      if ($result):
          $json['type'] = 'success';
          $json['title'] = Lang::$word->_SUCCESS;
          $json['message'] = Lang::$word->_MOD_TM_ITEM . ' /' . $title . '/ ' . Lang::$word->_DELETED;
          Security::writeLog(Lang::$word->_MOD_TM_ITEM . ' /' . urldecode($title) . '/ ' . Lang::$word->_DELETED, "", "no", "module");
      else:
          $json['type'] = 'warning';
          $json['title'] = Lang::$word->_ALERT;
          $json['message'] = Lang::$word->_SYSTEM_PROCCESS;
      endif;
      print json_encode($json);
  endif;

  /* == Process Item == */
  if (isset($_POST['processItem'])):
      Registry::get("Timeline")->processItem();
  endif;
  
  /* == Image Picker == */
  if (isset($_POST['pickImages'])):
      require_once (BASEPATH . "lib/class_fm.php");
      Registry::set('FileManager', new FileManager());
	  
      Timeline::getImagePicker(UPLOADS . "timeline/");
  endif;
  
?>
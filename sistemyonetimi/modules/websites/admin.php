<?php
  /**
   * Web Sites
   *
   * @package VeriAsist
   * @author veriasist.com
   * @copyright 2014
   * @version $Id: class_admin.php, v4.00 2014-12-24 12:12:12 gewa Exp $
   */
  if (!defined("_VALID_PHP"))
      die('Direct access to this location is not allowed.');
  
  if(!$user->getAcl("websites")): print Filter::msgAlert(Lang::$word->_CG_ONLYADMIN); return; endif;
  
  Registry::set('WebSites', new WebSites());
  $sql = "SELECT mem_expire,mod_website_id FROM users WHERE active=1";
  $users = Registry::get("Database")->fetch_all($sql);

  function dateCalc($date1, $date2) {

    $dateCurrent = new DateTime($date1);
    $dateExpire = new DateTime($date2);
    $interval = $dateCurrent->diff($dateExpire);

    $date = "$interval->days gün";
    return $date;
  }
?>
<?php switch(Filter::$maction): case "edit": ?>
<?php $row = Core::getRowById(WebSites::mTable, Filter::$id);?>
<?php $row = (object) array_merge((array) $row, (array) ['customer' => json_decode($row->customer)]); ?>
<div class="veriasist icon heading message blue"> <a class="helper veriasist top right info corner label" data-help="websites"><i class="icon help"></i></a> <a class="helper veriasist top right info corner label" data-help="websites"><i class="icon help"></i></a> <i class="puzzle piece icon"></i>
  <div class="content">
    <div class="header"> <?php echo Lang::$word->_MOD_GA_TITLE4;?> </div>
    <div class="veriasist breadcrumb"><i class="icon home"></i> <a href="index.php" class="section"><?php echo Lang::$word->_N_DASH;?></a>
      <div class="divider"> / </div>
      <a href="index.php?do=modules" class="section"><?php echo Lang::$word->_N_MODS;?></a>
      <div class="divider"> / </div>
      <a href="index.php?do=modules&amp;action=config&amp;modname=websites" class="section"><?php echo $content->getModuleName(Filter::$modname);?></a>
      <div class="divider"> / </div>
      <div class="active section"><?php echo Lang::$word->_MOD_WS_TITLE1;?></div>
    </div>
  </div>
</div>
<div class="veriasist-large-content">
  <div class="veriasist message"><?php echo Core::langIcon();?><?php echo Lang::$word->_MOD_WS_INFO1. Lang::$word->_REQ1 . '<i class="icon asterisk"></i>' . Lang::$word->_REQ2;?></div>
  <div class="veriasist form segment">
    <div class="veriasist header"><?php echo Lang::$word->_MOD_WS_SUBTITLE1 . $row->folder;?></div>
    <div class="veriasist double fitted divider"></div>
    <form id="veriasist_form" name="veriasist_form" method="post">
      
      <div class="field">
        <label><?php echo Lang::$word->_MOD_WS_NAME;?></label>
        <div class="veriasist action input"><i class="icon-append icon asterisk"></i>
          <input type="text" value="<?php echo $row->folder;?>" name="folder">
          <div class="veriasist icon button" style="width: 20%;">.<?= substr(SITEURL, strpos(SITEURL, '://') + 3) ?></div>
        </div>
      </div>
      <div class="field">
      <label><?php echo Lang::$word->_MS_DESC2;?></label>
        <textarea name="desc" placeholder="<?php echo Lang::$word->_MS_DESC2;?>"><?php echo $row->desc;?></textarea>
      </div>
      
      <div class="veriasist double fitted divider"></div>
      <div class="veriasist header"><?php echo Lang::$word->_MOD_WS_CUST_TITLE1;?></div>
      <div class="two fields">
        <div class="field">
          <label><?php echo Lang::$word->_MOD_WS_CUST_NAME;?></label>
          <label class="input"><i class="icon-append icon asterisk"></i>
            <input type="text" value="<?php echo $row->customer->name;?>" name="customer_name">
          </label>
        </div>
        <div class="field">
          <label><?php echo Lang::$word->_MOD_WS_CUST_COMPANY;?></label>
          <label class="input">
            <input type="text" value="<?php echo $row->customer->company;?>" name="customer_company">
          </label>
        </div>
      </div>
      <div class="two fields">
        <div class="field">
          <label><?php echo Lang::$word->_MOD_WS_CUST_PHONE;?></label>
          <label class="input">
            <input type="text" value="<?php echo $row->customer->phone;?>" name="customer_phone">
          </label>
        </div>
        <div class="field">
          <label><?php echo Lang::$word->_MOD_WS_CUST_EMAIL;?></label>
          <label class="input">
            <input type="text" value="<?php echo $row->customer->email;?>" name="customer_email">
          </label>
        </div>
      </div>

      <div class="two fields">
        <?php if ($row->status != -1): ?>
          <div class="field">
            <label><?php echo Lang::$word->_MOD_WS_PUB;?></label>
            <div class="inline-group">
              <label class="radio">
                <input name="status" type="radio" value="1" <?php getChecked($row->status, 1); ?>>
                <i></i><?php echo Lang::$word->_YES;?></label>
              <label class="radio">
                <input name="status" type="radio" value="0" <?php getChecked($row->status, 0); ?>>
                <i></i><?php echo Lang::$word->_NO;?></label>
            </div>
          </div>
        <?php endif; ?>
      </div>

      <div class="veriasist double fitted divider"></div>
      <button type="button" name="dosubmit" class="veriasist positive button"><?php echo Lang::$word->_MOD_WS_EDIT;?></button>
      <a href="index.php?do=modules&amp;action=config&amp;modname=websites" class="veriasist basic button"><?php echo Lang::$word->_CANCEL;?></a>
      <input name="id" type="hidden" value="<?php echo Filter::$id;?>">
      <input name="processWebSite" type="hidden" value="1">
    </form>
  </div>
  <div id="msgholder"></div>
</div>
<?php break;?>
<?php case"add": ?>
<div class="veriasist icon heading message blue"> <a class="helper veriasist top right info corner label" data-help="websites"><i class="icon help"></i></a> <a class="helper veriasist top right info corner label" data-help="websites"><i class="icon help"></i></a> <i class="puzzle piece icon"></i>
  <div class="content">
    <div class="header"> <?php echo Lang::$word->_MOD_GA_TITLE4;?> </div>
    <div class="veriasist breadcrumb"><i class="icon home"></i> <a href="index.php" class="section"><?php echo Lang::$word->_N_DASH;?></a>
      <div class="divider"> / </div>
      <a href="index.php?do=modules" class="section"><?php echo Lang::$word->_N_MODS;?></a>
      <div class="divider"> / </div>
      <a href="index.php?do=modules&amp;action=config&amp;modname=websites" class="section"><?php echo $content->getModuleName(Filter::$modname);?></a>
      <div class="divider"> / </div>
      <div class="active section"><?php echo Lang::$word->_MOD_WS_TITLE2;?></div>
    </div>
  </div>
</div>
<div class="veriasist-large-content">
  <div class="veriasist message"><?php echo Core::langIcon();?><?php echo Lang::$word->_MOD_WS_INFO2. Lang::$word->_REQ1 . '<i class="icon asterisk"></i>' . Lang::$word->_REQ2;?></div>
  <div class="veriasist form segment">
    <div class="veriasist header"><?php echo Lang::$word->_MOD_WS_SUBTITLE2;?></div>
    <div class="veriasist double fitted divider"></div>
    <form id="veriasist_form" name="veriasist_form" method="post">
      
      <div class="field">
        <label><?php echo Lang::$word->_MOD_WS_NAME;?></label>
        <div class="veriasist action input"><i class="icon-append icon asterisk"></i>
          <input type="text" placeholder="<?php echo Lang::$word->_MOD_WS_NAME;?>" name="folder">
          <div class="veriasist icon button" style="width: 20%;">.<?= substr(SITEURL, strpos(SITEURL, '://') + 3) ?></div>
        </div>
      </div>
      <div class="field">
      <label><?php echo Lang::$word->_MS_DESC2;?></label>
        <textarea name="desc" placeholder="<?php echo Lang::$word->_MS_DESC2;?>"></textarea>
      </div>
      
      <div class="veriasist double fitted divider"></div>
      <div class="veriasist header"><?php echo Lang::$word->_MOD_WS_CUST_TITLE1;?></div>
      <div class="two fields">
        <div class="field">
          <label><?php echo Lang::$word->_MOD_WS_CUST_NAME;?></label>
          <label class="input"><i class="icon-append icon asterisk"></i>
            <input type="text" placeholder="<?php echo Lang::$word->_MOD_WS_CUST_NAME;?>" name="customer_name">
          </label>
        </div>
        <div class="field">
          <label><?php echo Lang::$word->_MOD_WS_CUST_COMPANY;?></label>
          <label class="input">
            <input type="text" placeholder="<?php echo Lang::$word->_MOD_WS_CUST_COMPANY;?>" name="customer_company">
          </label>
        </div>
      </div>
      <div class="two fields">
        <div class="field">
          <label><?php echo Lang::$word->_MOD_WS_CUST_PHONE;?></label>
          <label class="input">
            <input type="text" placeholder="<?php echo Lang::$word->_MOD_WS_CUST_PHONE;?>" name="customer_phone">
          </label>
        </div>
        <div class="field">
          <label><?php echo Lang::$word->_MOD_WS_CUST_EMAIL;?></label>
          <label class="input">
            <input type="text" placeholder="<?php echo Lang::$word->_MOD_WS_CUST_EMAIL;?>" name="customer_email">
          </label>
        </div>
      </div>

      <div class="veriasist double fitted divider"></div>
      <button type="button" name="dosubmit" class="veriasist positive button"><?php echo Lang::$word->_MOD_WS_ADD;?></button>
      <a href="index.php?do=modules&amp;action=config&amp;modname=websites" class="veriasist basic button"><?php echo Lang::$word->_CANCEL;?></a>
      <input name="lat" id="lat" type="hidden">
      <input name="lng" id="lng" type="hidden">
      <input name="processWebSite" type="hidden" value="1">
    </form>
  </div>
  <div id="msgholder"></div>
</div>
<?php break;?>
<?php default: ?>

<?php
  function web_site_status($status)
  {
    switch ($status) {
      case -1:
        return '<span class="veriasist warning label">' . Lang::$word->_USER_P . '</span>';
      break;
      case 0:
        return '<span class="veriasist negative label">' . Lang::$word->_USER_I . '</span>';
      break;
      case 1:
        return '<span class="veriasist positive label">' . Lang::$word->_USER_A . '</span>';
      break;
    }
  }
?>
<?php
  $db_sizes = Registry::get("WebSites")->getDbSize();
?>
<?php $gmrow = Registry::get("WebSites")->getWebSites();?>
<div class="veriasist icon heading message blue"> <i class="puzzle piece icon"></i>
  <div class="content">
    <div class="header"> <?php echo Lang::$word->_MOD_WS_TITLE4;?> </div>
    <div class="veriasist breadcrumb"><i class="icon home"></i> <a href="index.php" class="section"><?php echo Lang::$word->_N_DASH;?></a>
      <div class="divider"> / </div>
      <a href="index.php?do=modules" class="section"><?php echo Lang::$word->_N_MODS;?></a>
      <div class="divider"> / </div>
      <div class="active section"><?php echo $content->getModuleName(Filter::$modname);?></div>
    </div>
  </div>
</div>
<div class="veriasist-large-content">
  <div class="veriasist message"><?php echo Core::langIcon();?><?php echo Lang::$word->_MOD_WS_INFO4;?></div>
  <div class="veriasist segment"> <a class="veriasist icon warning button push-right" href="<?php echo Core::url("modules", "add");?>"><i class="icon add"></i> <?php echo Lang::$word->_MOD_WS_ADD;?></a>
    <div class="veriasist header"><?php echo Lang::$word->_MOD_WS_SUBTITLE3;?></div>
    <div class="veriasist fitted divider"></div>
    <table class="veriasist sortable table">
      <thead>
        <tr>
          <th data-sort="int">#</th>
          <th data-sort="string"><?php echo Lang::$word->_MOD_WS_NAME;?></th>
          <th data-sort="string"><?php echo Lang::$word->_MOD_WS_STATUS;?></th>
          <th data-sort="int"><?php echo Lang::$word->_MOD_WS_APPEAL_DATE;?></th>
          <th data-sort="int"><?php echo Lang::$word->_MOD_WS_ACTIVE_STARTUP_DATE;?></th>
          <th data-sort="int"><?php echo Lang::$word->_MOD_WS_END_DATE;?></th>
          <th data-sort="string"><?php echo Lang::$word->_MOD_WS_END_DAY;?></th>
          <th data-sort="string">Son Giriş Tarihi</th>
          <th data-sort="string">Database Size</th>
          <th data-sort="string">Folder Size</th>
          <th class="disabled"><?php echo Lang::$word->_ACTIONS;?></th>
        </tr>
      </thead>
      <tbody>
        <?php if(!$gmrow):?>
        <tr>
          <td colspan="6"><?php Filter::msgSingleInfo(Lang::$word->_MOD_WS_NOGMAPS);?></td>
        </tr>
        <?php else:?>
        <?php foreach ($gmrow as $row):?>
        <?php $date = "Oluşturulmadı"; ?>
        <tr>
          <td><?php echo $row->id;?></td>
          <td><a href="http://<?= $row->folder.'.'.substr(SITEURL, strpos(SITEURL, '://') + 3) ?>" target="_blank"><?php echo $row->folder;?></a></td>
          <td class="ws_status"><?= web_site_status($row->status); ?></td>
          <td data-sort-value="<?php echo strtotime($row->created);?>"><?php echo Filter::dodate("short_date", $row->created);?></td>
          <td data-sort-value="<?= strtotime($row->activated); ?>"><?php echo Filter::dodate("short_date", $row->activated);?></td>
          <td data-sort-value="<?= strtotime($row->user_mem_expire); ?>"><?= $row->user_mem_expire ? Filter::dodate("short_date", $row->user_mem_expire) : null ?></td>  
          <td><?= $row->user_mem_expire ? dateCalc($row->activated, $row->user_mem_expire) : null ?></td>
          <td data-sort-value="<?= strtotime($row->user_last_login); ?>"><?= $row->user_last_login ? Filter::dodate("short_date", $row->user_last_login) : null ?></td>
          <td>
          <?php foreach ($db_sizes as $db_size):?>
            <?php if($db_size->name == "posmaks_demo".$row->id): ?>
              <?= $db_size->size."mb"?>
            <?php endif; ?>
          <?php endforeach;?>
          </td>
          <td><?= getFileSize(BASEPATH . $row->folder ."/assets/uploads") ?></td>
          <td>
            <a href="<?php echo Core::url("modules", "edit", $row->id);?>"><i class="rounded inverted success icon pencil link"></i></a> 
            <a class="delete" data-title="<?php echo Lang::$word->_DELETE.' '.Lang::$word->_MOD_WS_GMAP;?>" data-option="deleteWebSite" data-id="<?php echo $row->id;?>" data-name="<?php echo $row->folder;?>"><i class="rounded danger inverted remove icon link"></i></a>
            <?php if ($row->status == -1): ?>
              <a class="activate" data-title="<?php echo Lang::$word->_MOD_WS_ACTIVATE?>" data-option="activateWebSite" data-id="<?php echo $row->id;?>" data-name="<?php echo $row->folder;?>"><i class="rounded inverted info icon checkmark link" data-content="<?php echo Lang::$word->_MOD_WS_ACTIVATE;?>"></i></a>
            <?php endif; ?>
            <span class="ws_processing"></span>
          </td>
        </tr>
        <?php endforeach;?>
        <?php unset($row);?>
        <?php endif;?>
      </tbody>
    </table>
  </div>
</div>
<script type="text/javascript">
  /* == Activate Website == */
  $('body').on('click', 'a.activate', function () {
    var element = $(this);
    var id = element.data('id');
    var name = element.data('name');
    var title = element.data('title');
    var option = element.data('option');
    var extra = element.data('extra');
    var parent = element.closest("tr");
    var td = element.closest("td");
    new Messi("<div class=\"messi-warning\"><i class=\"massive icon warn warning sign\"></i></p><p><strong>" + name  + "</strong>, sitesi oluşturulup yayınlansın mı?</p></div>", {
      title: title,
      titleClass: '',
      modal: true,
      closeButton: true,
      buttons: [{
        id: 0,
        label: 'Onayla',
        class: 'positive',
        val: 'Y'
      }],
      callback: function (val) {
        $.ajax({
          type: 'post',
          url: 'modules/websites/controller.php',
          dataType: 'json',
          data: {
            id: id,
            event: option,
            extra: extra ? extra : null,
            title: encodeURIComponent(name)
          },
          beforeSend: function () {
            parent.animate({
              'backgroundColor': 'rgba(85, 220, 57, 0.26)'
            }, 400);
            td.find(".ws_processing").html("<?= Lang::$word->_MOD_WS_PROCESSING ?>");
          },
          success: function (json) {
            if (json.type === "success") {
              td.find(".ws_processing").html(json.message);
              element.remove();
              parent.find(".ws_status").html('<span class="veriasist positive label"><?= Lang::$word->_USER_A ?></span>');
            } else{
              td.find(".ws_processing").html(json.message);
            }
            $.sticky(decodeURIComponent(json.message), {
              type: json.type,
              title: json.title
            });
          }

        });
      }
    });
  });
</script>
<?php break;?>
<?php endswitch;?>
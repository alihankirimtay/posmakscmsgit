<?php

  /**
   * Web Sites Class
   *
   * @package VeriAsist
   * @author veriasist.com
   * @copyright 2012
   * @version $Id: class_admin.php, v1.00 2012-12-24 12:12:12 gewa Exp $
   */

  if (!defined("_VALID_PHP"))
      die('Direct access to this location is not allowed.');

  class WebSites
  {

      const mTable = "mod_websites";
      const uTable = "users"; 
      const pluginspath = "plugins/websites/";


      /**
       * WebSites::__construct()
       * 
       * @return
       */
      function __construct()
      {
      }


      /**
       * WebSites::getWebSites()
       * 
       * @return
       */
      public function getWebSites($where = null)
      {

          $sql = "SELECT *, 
          (SELECT users.mem_expire FROM users WHERE users.mod_website_id = mod_websites.id) as user_mem_expire,  
          (SELECT users.lastlogin FROM users WHERE users.mod_website_id = mod_websites.id) as user_last_login  
          FROM " . self::mTable . " WHERE deleted IS NULL {$where} ORDER BY status ASC";
          $row = Registry::get("Database")->fetch_all($sql);

          return ($row) ? $row : 0;
      }


      /**
       * WebSites::processWebSite()
       * 
       * @return
       */
      public function processWebSite()
      {

        Filter::checkPost('folder', Lang::$word->_MOD_WS_NAME);
        Filter::checkPost('customer_name', Lang::$word->_MOD_WS_CUST_NAME);

        $_POST['folder'] = strtolower($_POST['folder']);
        if (!preg_match('/^[a-z0-9_-]+$/i', $_POST['folder'])) {
          Filter::$msgs['folder'] = Lang::$word->_MOD_WS_NAME2;
        }

        if (empty(Filter::$msgs)) {

          $data = [
            'folder'   => sanitize(post('folder')),
            'desc'     => sanitize(post('desc')),
            'customer' => json_encode([
              'name'    => sanitize(post('customer_name')),
              'company' => sanitize(post('customer_company')),
              'phone'   => sanitize(post('customer_phone')),
              'email'   => sanitize(post('customer_email')),
            ]),
          ];

          $mode = (Filter::$id) ? 'update' : 'insert';

          if ($mode == 'insert') {

            $data['created'] = "NOW()";
            $data['status']  = '-1';

            // Aynı subdomain/klasör var mı?
            if (is_dir(BASEPATH . $data['folder']) || getValue('folder', self::mTable, "deleted IS NULL AND folder = '{$data['folder']}'")) {
              Filter::$msgs['folder'] = Lang::$word->_MOD_WS_NAME_EXISTS;
            }

          } elseif ($mode == 'update') {

            $website_status = getValueById('status', self::mTable, Filter::$id);

            $data['modified'] = "NOW()";
            if ($website_status != -1) {
              $data['status'] = post('status');
            }

            // Aynı subdomain/klasör var mı?
            if (is_dir(BASEPATH . $data['folder']) || getValue('folder', self::mTable, "deleted IS NULL AND folder = '{$data['folder']}' AND id != ".Filter::$id)) {
              Filter::$msgs['folder'] = Lang::$word->_MOD_WS_NAME_EXISTS;
            }

          }

          if (Filter::$msgs) {
            $json['message'] = Filter::msgStatus();
            exit(json_encode($json));
          }


          ($mode == 'update') 
            ? Registry::get("Database")->update(self::mTable, $data, "id=" . Filter::$id) 
            : $last_id = Registry::get("Database")->insert(self::mTable, $data);
          $message = ($mode == 'update') ? Lang::$word->_MOD_WS_GUPDATED : Lang::$word->_MOD_WS_GADDED;



          if (Registry::get("Database")->affected()) {
            $json['type'] = 'success';
            $json['message'] = Filter::msgOk($message, false);
            Security::writeLog($message, "", "no", "module");
          } else {
            $json['type'] = 'info';
            $json['message'] = Filter::msgAlert(Lang::$word->_SYSTEM_PROCCESS, false);
          }
          print json_encode($json);

        } else {
          $json['message'] = Filter::msgStatus();
          print json_encode($json);
        }
      }



      public function activateWebSite()
      {
        if (!$website = getValues("*", self::mTable, 'deleted IS NULL AND status = -1 AND id = '.Filter::$id)) {
          $this->jsonResult('error', Lang::$word->_MOD_WS_NOT_FND);
        }
        $website->customer = json_decode($website->customer);



        //Find Membership Trial Days
        $trial_days = "SELECT * FROM memberships WHERE trial = 1 AND active = 1 ORDER BY id DESC";
        $trial = Registry::get("Database")->fetch_all($trial_days);

        if(!$trial[0]->days)
            $this->jsonResult('error', Lang::$word->_MOD_WS_NOT_TRIAL);
        
        $tomorrow_timestamp = strtotime("+".$trial[0]->days. "days");
        $trial_date = date("Y-m-d", $tomorrow_timestamp);
              


        $api_token = sha1(json_encode($website));
        $password =  mt_rand(100000, 999999);
        $password_token = sha1($password . "+u+i06i4am+ei6e6ea35am3mpaX.");


        // Aynı isimde klasör var mı?
        if (empty($website->folder) || is_dir(BASEPATH . $website->folder)) {
          $this->jsonResult('error', BASEPATH . $website->folder . ' ' . Lang::$word->_MOD_WS_INV_DIR);
        }

        // DATABASE ---------------------------------------------------
        // Database yoksa oluştur?
        $DB_NAME = 'posmaks_demo'.$website->id;
        $DB_USER = DB_USER;
        $DB_PASS = DB_PASS;
         
        exec("mysqladmin -u {$DB_USER} --password='{$DB_PASS}' create {$DB_NAME} 2>&1", $output);
        
        // Bağlantı kur.
        $mysqli = @new mysqli(DB_SERVER, DB_USER, DB_PASS, $DB_NAME);

        if ($mysqli->connect_error) {

          $this->jsonResult('error', $DB_NAME . ', ' .Lang::$word->_MOD_WS_DB_ERR1);
        }

        // Veritabanı tablolarını boşalt
        $mysqli->query('SET foreign_key_checks = 0');
        if ($result = $mysqli->query("SHOW TABLES"))
          while($row = $result->fetch_array(MYSQLI_NUM)) {
            $mysqli->query('DROP TABLE IF EXISTS '.$row[0]);
          }
        $mysqli->query('SET foreign_key_checks = 1');
        
        // Demo uygulama dosyalarını taşı.
        $this->copyDir(BASEPATH . 'sistemyonetimi/DemoApp', BASEPATH . $website->folder);
        // Database Config dosyasını oluştur
        $db_config = BASEPATH . $website->folder . '/application/config/database.php';
        file_put_contents($db_config, require(MODPATH . 'websites/database.template.php'));

        if (!file_exists($db_config) || !is_dir(BASEPATH . $website->folder)) {
          $this->jsonResult('error', Lang::$word->_MOD_WS_COPY_ERR1);
        }


        // Run Database Migration
        exec('php ' . BASEPATH . $website->folder . '/index.php migrate latest 2>&1', $exec_output);
        $exec_output = implode(' ', (Array) $exec_output);
        if (!preg_match('/\[Success\]/', $exec_output)) {
          $this->jsonResult('error', 'Migrate Error: ' . $exec_output);
        }

        $mysqli->query("UPDATE users SET email = '{$website->customer->email}', password = '{$password_token}', pin = '{$password}' WHERE id = 1");
        $mysqli->query("UPDATE settings SET email = '{$website->customer->email}', api_token = '{$api_token}', date_expire = '{$trial_date} 00:00:00' WHERE id = 1");
        $mysqli->close();
        // END - DATABASE ---------------------------------------------------


        Registry::get("Database")->update(self::mTable, [
          'modified' => "NOW()",
          'status' => 1
        ], "id=" . Filter::$id);

        if (isset($_POST['event']) and $_POST['event'] == "activateWebSite") {
          Registry::get("Database")->update(self::mTable, [
            'activated' => "NOW()",
          ], "id=" . Filter::$id);
        
        }
            

        list($fname, $lname) = array_pad(explode(' ', $website->customer->name, 2), 2, null);
        $lname = $lname ? $lname : $fname;



        Registry::get("Database")->insert('users', [
          'mod_website_id' => Filter::$id,
          'username' => $website->folder,
          'email' => $website->customer->email,
          'fname' => $fname,
          'lname' => $lname,
          'mem_expire' => $trial_date,
          'database_name' => $DB_NAME,
          'api_token' => $api_token,
          'userlevel' => 1,
          'active' => 'y',
          'created' => "NOW()",
        ]);
          
        require_once(BASEPATH . "lib/class_mailer.php");
        $mailer = Mailer::sendMail();

        $row = Registry::get("Core")->getRowById(Content::eTable, 19);

        $body = str_replace(array('[NAME]', '[SITE_NAME]', '[URL]', '[MAIL]', '[PASSWORD]'), 
        array($website->customer->name, $website->folder, SITEURL, $website->customer->email, $password), $row->{'body'.Lang::$lang});
        $user_msg = Swift_Message::newInstance()
              ->setSubject('Posmaks Hesap Erişim Bilgileri')
              ->setTo($website->customer->email)
              ->setFrom([Registry::get("Core")->site_email => "Posmaks"])
              ->setBody(cleanOut($body), 'text/html');
        
        $mailer->send($user_msg);
        

        $this->jsonResult('success', $website->folder. ', ' . Lang::$word->_MOD_WS_ACTIVATED . ' ('.number_format(microtime(true) - $_SERVER['REQUEST_TIME_FLOAT'], 3).' sn)');
      
      }

      public function getDbSize()
      {

        $sql = 'SELECT table_schema name,  
          ROUND(SUM(data_length + index_length) / 1024 / 1024, 2) size 
          FROM information_schema.tables 
          WHERE table_schema LIKE "posmaks%"
          GROUP BY table_schema';
        
      
      $users_size = Registry::get("Database")->fetch_all($sql);
      return $users_size;
        
      }


      protected function copyDir($src, $dst) { 
        $dir = opendir($src); 
        @mkdir($dst); 
        while(false !== ( $file = readdir($dir)) ) { 
          if (( $file != '.' ) && ( $file != '..' )) { 
            if ( is_dir($src . '/' . $file) ) { 
              $this->copyDir($src . '/' . $file,$dst . '/' . $file); 
            } 
            else { 
              copy($src . '/' . $file,$dst . '/' . $file); 
            } 
          } 
        } 
        closedir($dir); 
      }

      protected function jsonResult($type, $message = null)
      {
        if ($type == 'success') {

          $json['type'] = 'success';
          $json['title'] = Lang::$word->_SUCCESS;
          $json['message'] = $message;
        } else {
          $json['type'] = 'error';
          $json['title'] = Lang::$word->_ALERT;
          $json['message'] = isset($message) ? $message : Lang::$word->_SYSTEM_PROCCESS;
        }

        exit(json_encode($json));
      }
  }
?>
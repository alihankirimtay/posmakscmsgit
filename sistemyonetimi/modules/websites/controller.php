<?php
  /**
   * Controller
   *
   * @package VeriAsist
   * @author veriasist.com
   * @copyright 2014
   * @version $Id: controller.php, v4.00 2014-12-24 12:12:12 gewa Exp $
   */
  define("_VALID_PHP", true);

  require_once ("../../init.php");
  if (!$user->is_Admin())
      redirect_to("../../login.php");

  require_once("admin_class.php");
  Registry::set('WebSites', new WebSites());
?>
<?php
  /* == Proccess WebSite == */
  if (isset($_POST['processWebSite'])):
      Registry::get("WebSites")->processWebSite();
  endif;

  /* == Delete WebSite  == */
  if (isset($_POST['delete']) and $_POST['delete'] == "deleteWebSite"):
      $result = false;
      $title = sanitize($_POST['title']);
      $website = getValuesById("*", WebSites::mTable, Filter::$id);
      if ($website) {

        $db->update(WebSites::mTable, [
          'deleted' => date('Y-m-d H:i:s')
        ], "id=" . Filter::$id);

        $db->update(WebSites::uTable, [ 
          'active' => "n" 
        ], "mod_website_id=" . Filter::$id); 

        rename(BASEPATH .$website->folder, BASEPATH .'deleted-'. $website->folder);

        $result = true;
      }

      if ($result):
          $json['type'] = 'success';
          $json['title'] = Lang::$word->_SUCCESS;
          $json['message'] = Lang::$word->_MOD_WS_GMAP . ' /' . $title . '/ ' . Lang::$word->_DELETED;
          Security::writeLog(Lang::$word->_MOD_WS_GMAP . ' /' . urldecode($title) . '/ ' . Lang::$word->_DELETED, "", "no", "module");
      else:
          $json['type'] = 'warning';
          $json['title'] = Lang::$word->_ALERT;
          $json['message'] = Lang::$word->_SYSTEM_PROCCESS;
      endif;
      print json_encode($json);
  endif;



    /* == Activate WebSite  == */
  if (isset($_POST['event']) and $_POST['event'] == "activateWebSite"):
    
    Registry::get("WebSites")->activateWebSite();
  endif;
?>
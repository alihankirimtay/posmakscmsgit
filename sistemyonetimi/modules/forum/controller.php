<?php
  /**
   * Controller
   *
   * @package VeriAsist
   * @author veriasist.com
   * @copyright 2014
   * @version $Id: controller.php, v4.00 2014-03-20 10:12:05 gewa Exp $
   */
  define("_VALID_PHP", true);
  
  require_once ("../../init.php");
  
  if (!$user->is_Admin())
      redirect_to("../../login.php");

  require_once ("admin_class.php");
  Registry::set('Forum', new Forum());
?>
<?php
  /* == Proccess Forum == */
  if (isset($_POST['processForum'])): 
	  Registry::get("Forum")->processForum();
  endif;

  /* == Proccess Config == */
  if (isset($_POST['processConfig'])): 
	 Registry::get("Forum")->processConfig();
  endif;

  /* == Update Forum Order == */
  if (isset($_GET['sortforum'])):
      Forum::updateOrder();
  endif;
  
  /* == Delete forum == */
  if (isset($_POST['delete']) and $_POST['delete'] == "deleteForum"):
      $title = sanitize($_POST['title']);
	  
      $result = $db->delete(Forum::mTable, "id=" . Filter::$id);
	  $db->delete(Forum::pTable, "forum_id=" . Filter::$id);
	  $db->delete(Forum::tTable, "forum_id=" . Filter::$id);

      if ($result):
          $json['type'] = 'success';
          $json['title'] = Lang::$word->_SUCCESS;
          $json['message'] = Lang::$word->_MOD_FB_FORUM . ' /' . $title . '/ ' . Lang::$word->_DELETED;
          Security::writeLog(Lang::$word->_MOD_FB_FORUM . ' /' . urldecode($title) . '/ ' . Lang::$word->_DELETED, "", "no", "module");
      else:
          $json['type'] = 'warning';
          $json['title'] = Lang::$word->_ALERT;
          $json['message'] = Lang::$word->_SYSTEM_PROCCESS;
      endif;
       print json_encode($json);
  endif;
?>
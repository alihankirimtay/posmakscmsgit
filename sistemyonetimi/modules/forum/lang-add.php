<?php
  /**
   * Language Data Add
   *
   * @package VeriAsist
   * @author veriasist.com
   * @copyright 2014
   * @version $Id: lang-add.php, v4.00 2014-03-20 10:12:05 gewa Exp $
   */
  if (!defined("_VALID_PHP"))
      die('Direct access to this location is not allowed.');
?>
<?php
  self::$db->query("LOCK TABLES mod_forum WRITE");
  self::$db->query("ALTER TABLE mod_forum ADD COLUMN metakey_$flag_id VARCHAR(200) NOT NULL AFTER metakey_en");
  self::$db->query("ALTER TABLE mod_forum ADD COLUMN metadesc_$flag_id TEXT AFTER metadesc_en");
  self::$db->query("UNLOCK TABLES");

  if ($mod_forum = self::$db->fetch_all("SELECT * FROM mod_forum")) {
      foreach ($mod_forum as $row) {
          $data['metakey_' . $flag_id] = $row->metakey_en;
          $data['metadesc_' . $flag_id] = $row->metadesc_en;
          self::$db->update("mod_forum", $data, "id = " . $row->id);
      }
      unset($data, $row);
  }
?>
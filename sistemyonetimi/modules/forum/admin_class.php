<?php
  /**
   * Forum Class
   *
   * @package VeriAsist
   * @author veriasist.com
   * @copyright 2014
   * @version $Id: class_admin.php, v4.00 2014-05-01 14:12:48 gewa Exp $
   */
  
  if (!defined("_VALID_PHP"))
      die('Direct access to this location is not allowed.');
  
  class Forum
  {
	  
	  const mTable = "mod_forum";
	  const pTable = "mod_forum_posts";
	  const tTable = "mod_forum_thread";
	  const uTable = "mod_forum_users";
	  
	  public $icons = "modules/forum/icons/";
	  
	  private static $db;

      /**
       * Forum::__construct()
       * 
       * @return
       */
      function __construct($forum = false, $topic = false, $user = false)
      {
		  self::$db = Registry::get("Database");
		  $this->getConfig();
          ($forum) ? $this->renderSingleThread() : null;
		  ($topic) ? $this->renderSingleForum() : null;
		  ($user) ? $this->renderSingleUser() : null;
      }


	  /**
	   * Forum::getConfig()
	   * 
	   * @return
	   */
	  private function getConfig()
	  {

          $row = INI::read(MODPATH . 'forum/config.ini');

          $this->hash = $row->fb_config->hash;
          $this->secret = $row->fb_config->secret;
          $this->sorting = $row->fb_config->sorting;
		  $this->ipp = $row->fb_config->ipp;
		  $this->template = base64_decode($row->fb_config->template);

          return ($row) ? $row : 0;
	  }

	  /**
	   * Forum::processForum()
	   * 
	   * @return
	   */
	  public function processConfig()
	  {
		  
		  Filter::checkPost('ipp', Lang::$word->_MOD_FB_IPP);
		  Filter::checkPost('template', Lang::$word->_MOD_FB_TEMPLATE);
		  Filter::checkPost('secret', Lang::$word->_MOD_FB_HASH);

		  if (empty(Filter::$msgs)) {
			  $data = array('fb_config' => array(
					  'ipp' => intval($_POST['ipp']),
					  'sorting' => sanitize($_POST['sorting']),
					  'hash' => hash('sha256', $_POST['secret']),
					  'secret' => '"' . sanitize($_POST['secret']) . '"',
					  'template' => '"' . base64_encode($_POST['template']) . '"')
					  );
					  
			  if (INI::write(MODPATH . 'forum/config.ini', $data)) {
				  $json['type'] = 'success';
				  $json['message'] = Filter::msgOk(Lang::$word->_MOD_FB_CUPDATED, false);
				  Security::writeLog(Lang::$word->_MOD_FB_CUPDATED, "", "no", "module");
			  } else {
				  $json['type'] = 'info';
				  $json['message'] = Filter::msgAlert(Lang::$word->_PROCCESS_C_ERR . '{sistemyonetimi/modules/digishop/config.ini}', false);
			  }
			  print json_encode($json);
	
		  } else {
			  $json['message'] = Filter::msgStatus();
			  print json_encode($json);
		  }
	  }
	  
	  /**
	   * Forum::getForums()
	   * 
	   * @return
	   */
	  public function getForums()
	  {
		  
		  $sql = "SELECT *,(SELECT COUNT(forum_id) FROM " . self::pTable . " WHERE forum_id = " . self::mTable . ".id) as totalposts"
		  . "\n FROM " . self::mTable
		  . "\n ORDER BY ordering";
          $row = self::$db->fetch_all($sql);
          
		  return ($row) ? $row : 0;
	  }	 	

	  /**
	   * Forum::processForum()
	   * 
	   * @return
	   */
	  public function processForum()
	  {
		  
		  Filter::checkPost('title', Lang::$word->_MOD_FB_NAME);

		  if ($_POST['access'] == "Membership" && !isset($_POST['membership_id']))
			  Filter::$msgs['access'] = Lang::$word->_PG_MEMBERSHIP;
			  
		  if (empty(Filter::$msgs)) {
			  $data = array(
				  'title' => sanitize($_POST['title']), 
				  'slug' => (empty($_POST['slug'])) ? doSeo($_POST['title']) : doSeo($_POST['slug']),
				  'description' => sanitize($_POST['description']), 
				  'icon' => isset($_POST['icon']) ? sanitize($_POST['icon']) : 'NULL', 
				  'access' => sanitize($_POST['access']),
				  'publicpost' => intval($_POST['publicpost']),
				  'metakey' . Lang::$lang => sanitize($_POST['metakey' . Lang::$lang]),
				  'metadesc' . Lang::$lang => sanitize($_POST['metadesc' . Lang::$lang]),
				  'active' => intval($_POST['active']),
			  );

			  if (empty($_POST['metakey' . Lang::$lang]) or empty($_POST['metadesc' . Lang::$lang])) {
				  include (BASEPATH . 'lib/class_meta.php');
				  parseMeta::instance($_POST['description']);
				  if (empty($_POST['metakey' . Lang::$lang])) {
					  $data['metakey' . Lang::$lang] = parseMeta::get_keywords();
				  }
				  if (empty($_POST['metadesc'])) {
					  $data['metadesc' . Lang::$lang] = parseMeta::metaText($_POST['description']);
				  }
			  }
			  
			  if (isset($_POST['membership_id'])) {
				  $data['membership_id'] = Core::_implodeFields($_POST['membership_id']);
			  } else
				  $data['membership_id'] = 0;
			  
			  if (!Filter::$id) {
				  $data['created'] = "NOW()";
			  }

			  if (Filter::$id) {
				  $data['modified'] = "NOW()";
			  }
			  
			  (Filter::$id) ? self::$db->update(self::mTable, $data, "id=" . Filter::$id) : $lastid = self::$db->insert(self::mTable, $data);
			  $message = (Filter::$id) ? Lang::$word->_MOD_FB_UPDATED : Lang::$word->_MOD_FB_ADDED;
	
			  if (self::$db->affected()) {
				  Security::writeLog($message, "", "no", "module");
				  $json['type'] = 'success';
				  $json['message'] = Filter::msgOk($message, false);
			  } else {
				  $json['type'] = 'success';
				  $json['message'] = Filter::msgAlert(Lang::$word->_SYSTEM_PROCCESS, false);
			  }
			  print json_encode($json);
			  
		  } else {
			  $json['message'] = Filter::msgStatus();
			  print json_encode($json);
		  }
	  }
	  
	  /**
	   * Forum::renderForums()
	   * 
	   * @return
	   */
	  public function renderForums()
	  {

		  $sql = "SELECT f.*, u.username, UNIX_TIMESTAMP(f.last_entry_date) as last_entry,"
		  . "\n (SELECT COUNT(" . self::pTable . ".forum_id) FROM " . self::pTable . " WHERE " . self::pTable . ".forum_id = f.id) as totalposts"
		  . "\n FROM " . self::mTable . " as f" 
		  . "\n LEFT JOIN ".Users::uTable." as u ON f.last_entry_user_id = u.id" 
		  . "\n WHERE f.active = 1"
		  . "\n ORDER BY ordering";
          $row = self::$db->fetch_all($sql);
          
		  return ($row) ? $row : 0;
	  }	
	   
	  /**
	   * Forum::renderSingleForum()
	   * 
	   * @return
	   */
	  public function renderSingleForum()
	  {
		  
		  $sql = "SELECT * FROM " . self::mTable
		  . "\n WHERE active = 1"
		  . "\n AND slug = '" . Registry::get("Content")->_url[2] . "'";
          $row = self::$db->first($sql);
          
		  return ($row) ? Registry::get("Content")->moduledata->mod = $row : 0;
	  }	 

	  /**
	   * Forum::getSingleThread()
	   * 
	   * @return
	   */
	  public function renderSingleThread()
	  {
	
		  $sql = "SELECT f.*, t.id as tid, t.title as ftitle" 
		  . "\n FROM " . self::mTable . " as f" 
		  . "\n LEFT JOIN " . self::tTable . " as t ON t.forum_id = f.id" 
		  . "\n WHERE f.active = 1" . "\n AND t.slug = '" . Registry::get("Content")->_url[1] . "'";
		  $row = self::$db->first($sql);
	
		  return ($row) ? Registry::get("Content")->moduledata->mod = $row : 0;
	  }

	  /**
	   * Forum::renderSingleUser()
	   * 
	   * @return
	   */
	  public function renderSingleUser()
	  {
		  
		  $sql = "SELECT id, avatar, username, CONCAT(fname,' ',lname) as name, created"
		  . "\n FROM " . Users::uTable . " WHERE username = '" . Registry::get("Content")->_url[2] . "'"
		  . "\n AND active = 'y'";
          $row = self::$db->first($sql);
          
		  return ($row) ? Registry::get("Content")->moduledata->mod = $row : 0;
	  }
	  /**
	   * Forum::renderThreads()
	   * 
	   * @return
	   */
	  public function renderThreads($forumid)
	  {
		  
		$sql = "SELECT t.*, u.username, u.avatar, u.id as uid, UNIX_TIMESTAMP(t.created) as cdate," 
		. "\n (SELECT COUNT(" . self::uTable . ".id)" 
		. "\n FROM " . self::uTable . " WHERE " . self::uTable . ".user_id = " . Registry::get("Users")->uid . " AND " . self::uTable . ".thread_id = t.id) as follow" 
		. "\n FROM " . self::tTable . " as t" 
		. "\n LEFT JOIN " . Users::uTable . " as u ON t.user_id = u.id" 
		. "\n WHERE forum_id = " . (int)$forumid
		. "\n ORDER BY t.created " . $this->sorting;
		
		$row = self::$db->fetch_all($sql);
          
		  return ($row) ? $row : 0;
	  }	

	  /**
	   * Forum::processNewThread()
	   * 
	   * @return
	   */
	  public function processNewThread()
	  {
	
		  Filter::checkPost('title', Lang::$word->_MOD_FB_NEWTHREAD_TITLE);

		  if (self::titleExists(sanitize($_POST['title']))) {
			  Filter::$msgs['title'] = Lang::$word->_MOD_AM_NAME_R;
		  }
		  
		  $text = cleanOut($_POST['body']);
		  if (strlen($text) < 10)
			  Filter::$msgs['body'] = Lang::$word->_MOD_FB_MSG_R;
	
		   if (empty(Filter::$msgs)) {
			  $string = $this->keepTags($text, '<strong><ol><ul><li><u><em><i><b><br><p><a><img><blockquote><pre><code><div>', 'href, src, alt, iframe');

			  $data = array(
				  'forum_id' => Filter::$id,
				  'user_id' => Registry::get("Users")->uid,
				  'title' => sanitize($_POST['title']),
				  'slug' => doSeo($_POST['title']),
				  'created' => "NOW()"
				  );

			  $last_id = self::$db->insert(self::tTable, $data);
			  $data2 = array(
				  'forum_id' => Filter::$id,
				  'user_id' => Registry::get("Users")->uid,
				  'title' => sanitize($_POST['title']),
				  'body' => htmlspecialchars($string, ENT_QUOTES, 'UTF-8'),
				  'thread_id' => $last_id,
				  'is_reply' => 0,
				  'created' => $data['created'],
				  'last_user_id' => Registry::get("Users")->uid,
				  );
			  self::$db->insert(self::pTable, $data2);
			  	  
			  $data3 = array('last_entry_date' => $data['created'], 'last_entry_user_id' => $data['user_id']);
			  self::$db->update(self::mTable, $data3, "id = " . $data['forum_id']);
				  
			  $username = (Registry::get("Users")->logged_in) ? Registry::get("Users")->username : Lang::$word->_MOD_FB_GUEST;
			  $html = '
			  <div class="item active">
				<article>
				  <div class="veriasist push-right">' . Lang::$word->_MOD_FB_LPOST . ' ' . timesince(strtotime(date('c'))) . ' ' . Lang::$word->_MOD_FB_BY . ': ' . $username . '</div>
				  <div class="content">
					<div class="veriasist header"><a href="' . doUrl(false, $data["slug"], "forum-item") . '">' . $data['title'] . '</a></div>
				  </div>
				</article>
				<section>
				  <div class="veriasist divided horizontal list">
					<div class="item"><i class="icon chat"></i>' . Lang::$word->_MOD_FB_TOTALREP . ' <b>0</b></div>
				  </div>';
				  
				  if(Registry::get("Users")->is_Admin()) {
					  $html .= '<a class="veriasist bottom right attached negative label tdelete" data-id="' . $last_id . '" data-content="' . Lang::$word->_DELETE . '"><i class="icon remove link"></i></a>';
				  }
				  $html .='
				</section>
			  </div>';

			  if (self::$db->affected()) {
				  $json['status'] = 'success';
				  $json['message'] = $html;
			  } else {
				  $json['type'] = 'success';
				  $json['message'] = Filter::msgAlert(Lang::$word->_SYSTEM_PROCCESS, false);
			  }
			  print json_encode($json);
	
		  } else {
			  $json['message'] = Filter::msgStatus();
			  print json_encode($json);
		  }
	  }
	  
	  
	  /**
	   * Forum::renderPosts()
	   * 
	   * @return
	   */
	  public function renderPosts($thread_id)
	  {
	
		  $q = "SELECT COUNT(id) FROM " . self::pTable . " WHERE thread_id = " . (int)$thread_id . " LIMIT 1";
		  $record = self::$db->query($q);
		  $total = self::$db->fetchrow($record);
		  $counter = $total[0];
	
		  $pager = Paginator::instance();
		  $pager->items_total = $counter;
		  $pager->default_ipp = $this->ipp;
		  $pager->path = doUrl(false, Registry::get("Content")->_url[1], "forum-item", "?");
		  $pager->paginate();
	
		  $sql = "SELECT p.*, u.username, u.avatar, u.id as uid, UNIX_TIMESTAMP(p.created) as cdate" 
		  . "\n FROM " . self::pTable . " as p" 
		  . "\n LEFT JOIN users as u ON p.user_id = u.id" 
		  . "\n WHERE thread_id = " . (int)$thread_id 
		  . "\n ORDER BY p.created " . $this->sorting . $pager->limit;
		  $row = self::$db->fetch_all($sql);
	
		  return ($row) ? $row : 0;
	  }	

	  /**
	   * Forum::processNewPost()
	   * 
	   * @return
	   */
	  public function processNewPost()
	  {
	
		  Filter::checkPost('title', Lang::$word->_MOD_FB_NEWPOST_TITLE);
	
		  $text = cleanOut($_POST['body']);
		  if (strlen($text) < 10)
			  Filter::$msgs['body'] = Lang::$word->_MOD_FB_MSG_R;
	
		  if (empty(Filter::$msgs)) {
			  $string = $this->keepTags($text, '<strong><ol><ul><li><u><em><i><b><br><p><a><img><blockquote><pre><code><div>', 'href,src,alt,iframe');
	
			  if (isset($_POST['adminEdit']) and Registry::get("Users")->is_Admin()) {
				  $data = array(
					  'title' => sanitize($_POST['title']),
					  'body' => htmlspecialchars($string, ENT_QUOTES, 'UTF-8'),
					  );
				  self::$db->update(self::pTable, $data, "id = " . intval($_POST['post_id']));
	
				  $json['status'] = 'updated';
				  $json['title'] = $data['title'];
				  $json['body'] = cleanOut($data['body']);
				  $json['pid'] = intval($_POST['post_id']);
				  $json['message'] = Filter::msgSingleOk(Lang::$word->_MOD_FB_PUPDATE_P, false);
				  print json_encode($json);
	
			  } else {
				  $data = array(
					  'forum_id' => Filter::$id,
					  'user_id' => Registry::get("Users")->uid,
					  'thread_id' => intval($_POST['tid']),
					  'title' => sanitize($_POST['title']),
					  'body' => htmlspecialchars($string, ENT_QUOTES, 'UTF-8'),
					  'is_reply' => isset($_POST['is_reply']) ? intval($_POST['is_reply']) : 0,
					  'created' => "NOW()",
					  'last_user_id' => Registry::get("Users")->uid,
					  );
	
				  $last_id = self::$db->insert(self::pTable, $data);
	
				  $data3 = array('last_entry_date' => $data['created'], 'last_entry_user_id' => $data['user_id']);
				  self::$db->update(self::mTable, $data3, "id = " . $data['forum_id']);
	
				  $data4 = array('totalposts' => "INC(1)");
				  self::$db->update(self::tTable, $data4, "id = " . $data['thread_id']);
	
				  $avatar = (Registry::get("Users")->logged_in) ? getValueById("avatar", Users::uTable, Registry::get("Users")->uid) : null;
				  $username = (Registry::get("Users")->logged_in) ? Registry::get("Users")->username : Lang::$word->_MOD_FB_GUEST;
				  $pic = ($avatar) ? '<img src="' . UPLOADURL . 'avatars/' . $avatar . '" alt="' . $username . '" class="veriasist avatar image">' : '<img src="' . UPLOADURL . 'avatars/blank.png" alt="' . $username . '" class="veriasist avatar image">';
	
				  if (Registry::get("Users")->logged_in) {
					  $uurl = '<a href="' . doUrl(false, $username, "forum-user") . '">' . $username . '</a>';
					  $avurl = '<a href="' . doUrl(false, $username, "forum-user") . '">' . $avatar . '</a>';
				  } else {
					  $uurl = $username;
					  $avurl = '<a>' . $avatar . '</a>';
				  }
	
				  //$is_reply = ($data['is_reply']) ? ' class="reply"' : null;
				  
				  $html = '
				  <div class="section data-id="' . $last_id . '">
				  <section>
					<div class="veriasist divided list">
					  <div class="item">
						<div class="right floated"><a data-scroller="newreply" data-action="quote" data-content="' . Lang::$word->_MOD_FB_QUOTE . '" class="veriasist mini basic icon button scroller"><i class="icon quote left"></i></a> <a data-scroller="newreply" data-action="reply" data-content="' . Lang::$word->_MOD_FB_REPLYTO . '" class="veriasist mini basic icon button scroller"><i class="icon comment"></i></a> <a data-action="like" data-total="1" data-content="' . Lang::$word->_MOD_FB_LIKE . '" class="veriasist mini basic icon button like"><i class="icon heart"></i></a>';
						  if (Registry::get("Users")->is_Admin()) {
						  $html .= ' <a data-scroller="newreply" class="veriasist mini basic icon button scroller" data-action="edit" data-id="' . $last_id . '" data-content="' . Lang::$word->_EDIT . '"><i class="icon pencil"></i></a> <a class="veriasist mini basic icon button" data-action="delete" data-id="' . $last_id . '" data-content="' . Lang::$word->_DELETE . '"><i class="icon remove"></i></a>';
						  }
						  $html .= ' </div>
						' . $avurl . '
						<div class="content">
						  <div class="header">' . $data['title'] . '</div>
						</div>
					  </div>
					</div>
				  </section>
				  <article> ' . cleanOut($data['body']) . ' </article>
				  <footer>
					<div class="veriasist divided horizontal list">
					  <div class="item"><i class="icon time"></i>';
						if ($data['is_reply']) {
						$html .= Lang::$word->_MOD_FB_REPLIED . ': ' . timesince(time()) . Lang::$word->_MOD_FB_BY . ': ' . $uurl;
						} else {
						$html .= Lang::$word->_MOD_FB_CREATED . ': ' . timesince(time()) . Lang::$word->_MOD_FB_BY . ': ' . $uurl;
						}
						$html .= ' </div>
					  <div class="item"><i class="icon danger heart"></i>' . Lang::$word->_MOD_FB_LIKE . ': <b class="like">0</b></div>
					</div>
				  </footer>
				  </div>';
	
				  $json['status'] = 'success';
				  $json['message'] = Filter::msgSingleOk(Lang::$word->_MOD_FB_PPOST_A, false);
				  $json['thread_id'] = $data['thread_id'];
				  $json['forum_id'] = $data['forum_id'];
				  $json['msg'] = $data['body'];
				  $json['info'] = $html;
				  print json_encode($json);
			  }
	
		  } else {
			  $json['message'] = Filter::msgStatus();
			  print json_encode($json);
		  }
	  }

	  /**
	   * Forum::sendEmail()
	   * 
	   * @param int $thread_id
	   * @param int $forum_id
	   * @param string $msg
	   * @return
	   */
	  public function sendEmail($thread_id, $forum_id, $msg)
	  {
	
		  $sql = "SELECT u.*, CONCAT(u.fname,' ',lname) as name" 
		  . "\n FROM " . Users::uTable . " as u" 
		  . "\n LEFT JOIN " . self::uTable . " as fu ON fu.user_id = u.id" 
		  . "\n WHERE fu.thread_id = " . $thread_id;
	
		  $datarow = self::$db->fetch_all($sql);
	
		  if ($datarow) {
			  $forum = self::$db->first("SELECT title, slug FROM " . self::mTable . " WHERE id=" . $forum_id);
			  $forumurl = doUrl(false, $forum->slug, "forum-item");
			  $furl = '<a href="' . $forumurl . '">' . $forum->title . '</a>';
	
			  $thread = self::$db->first("SELECT title, slug FROM " . self::tTable . " WHERE id=" . $thread_id);
			  $threadurl = doUrl(false, $thread->slug, "forum-topic");
			  $turl = '<a href="' . $threadurl . '">' . $thread->title . '</a>';
	
			  require_once (BASEPATH . "lib/class_mailer.php");
			  $mailer = Mailer::sendMail();
			  $mailer->registerPlugin(new Swift_Plugins_AntiFloodPlugin(100, 30));
	
			  $replacements = array();
			  foreach ($datarow as $cols) {
				  $replacements[$cols->email] = array(
					  '[NAME]' => $cols->name,
					  '[FORUMNAME]' => $furl,
					  '[THREADNAME]' => $turl,
					  '[MESSAGE]' => cleanOut($msg),
					  '[SITE_NAME]' => Registry::get("Core")->site_name,
					  '[URL]' => SITEURL);
			  }
			  $decorator = new Swift_Plugins_DecoratorPlugin($replacements);
			  $mailer->registerPlugin($decorator);
	
			  $message = Swift_Message::newInstance()
						->setSubject(Lang::$word->_MOD_FB_FOLLOW_S)
						->setFrom(array(Registry::get("Core")->site_email => Registry::get("Core")->site_name))
						->setBody(cleanOut($this->template), 'text/html');
	
			  foreach ($datarow as $row) {
				  $message->setTo($row->email, $row->name);
				  $mailer->send($message);
			  }
			  unset($row);
	
		  }
	
	  }
	  
	  /**
	   * Forum::processFollow()
	   * 
	   * @return
	   */
	  public function processFollow()
	  {
	
		  $data = array('thread_id' => Filter::$id, 'user_id' => Registry::get("Users")->uid);
		  
		  if (self::$db->first("SELECT * FROM " . self::uTable . " WHERE user_id = " . Registry::get("Users")->uid . " AND thread_id = " . Filter::$id)) {
			  self::$db->delete(self::uTable, "user_id=" . Registry::get("Users")->uid . " AND thread_id=" . Filter::$id);
			  $json['type'] = 'success';
			  $json['message'] = 'remove';
		  } else {
			  self::$db->insert(self::uTable, $data);
			  $json['type'] = 'success';
			  $json['message'] = 'add';
		  }
	
		  print json_encode($json);
	
	  }

	  /**
	   * Forum::getUserInfo()
	   * 
	   * @return
	   */
	  public function getUserInfo($id)
	  {
		  
		  $sql = "SELECT p.*, f.title as ftitle, f.slug, UNIX_TIMESTAMP(p.created) as cdate"
		  . "\n FROM " . self::pTable . " as p" 
		  . "\n LEFT JOIN " . self::mTable . " as f ON f.id = p.forum_id" 
		  . "\n WHERE p.user_id = " . $id
		  . "\n ORDER BY f.id, p.thread_id, p.created";
          $row = self::$db->fetch_all($sql);
          
		  return ($row) ? $row : 0;
	  }	

	  /**
	   * Forum::doSearch()
	   * 
	   * @param string $string
	   * @return
	   */
	  public function doSearch($string, $thread_id, $forum_id)
	  {
	
	      $string = self::$db->escape($string);
		  
		  $sql = "SELECT p.*,MATCH(p.title, p.body) AGAINST('$string') AS score,  u.username, u.avatar, u.id as uid" 
		  . "\n FROM " . self::pTable . " as p" 
		  . "\n LEFT JOIN users as u ON p.user_id = u.id" 
		  . "\n WHERE thread_id = " . (int)$thread_id
		  . "\n AND MATCH (p.title, p.body) AGAINST('$string)' IN BOOLEAN MODE)"
		  . "\n ORDER BY score " . $this->sorting . " LIMIT 20";
		  $results = self::$db->fetch_all($sql);
		  
		  $sql2 = "SELECT * FROM " . self::mTable
		  . "\n WHERE id = " . $forum_id
		  . "\n AND active = 1";
          $single = self::$db->first($sql2);
	      $html = '';
		  if ($results) {
			  foreach ($results as $row) {

                  $username = ($row->user_id) ? $row->username : Lang::$word->_MOD_FB_GUEST;
				  $avatar = ($row->avatar) ? '<img src="' . UPLOADURL . 'avatars/' . $row->avatar . '" alt="' . $username . '" class="veriasist avatar image">' : '<img src="' . UPLOADURL . 'avatars/blank.png" alt="' . $username . '" class="veriasist avatar image">';
	
				  if ($row->user_id) {
					  $uurl = '<a href="' . doUrl(false, $username, "forum-user") . '">' . $username . '</a>';
					  $avurl = '<a href="' . doUrl(false, $username, "forum-user") . '">' . $avatar . '</a>';
				  } else {
					  $uurl = $username;
					  $avurl = '<a>' . $avatar . '</a>';
				  }
				  
				  $html .= '
				  <div class="section data-id="' . $row->id . '">
				  <section>
					<div class="veriasist divided list">
					  <div class="item">
						<div class="right floated">';
						if(Registry::get("Users")->logged_in or ($single->access == "Public" and $single->publicpost)) {
							$html .= '
							<a data-scroller="newreply" data-action="quote" data-content="' . Lang::$word->_MOD_FB_QUOTE . '" class="veriasist mini basic icon button scroller"><i class="icon quote left"></i></a> <a data-scroller="newreply" data-action="reply" data-content="' . Lang::$word->_MOD_FB_REPLYTO . '" class="veriasist mini basic icon button scroller"><i class="icon comment"></i></a> <a data-action="like" data-total="' . $row->like . '" data-content="' . Lang::$word->_MOD_FB_LIKE . '" class="veriasist mini basic icon button like"><i class="icon heart"></i></a>';
						}
						  if (Registry::get("Users")->is_Admin()) {
						  $html .= ' <a data-scroller="newreply" class="veriasist mini basic icon button scroller" data-action="edit" data-id="' . $row->id . '" data-content="' . Lang::$word->_EDIT . '"><i class="icon pencil"></i></a> <a class="veriasist mini basic icon button" data-action="delete" data-id="' . $row->id . '" data-content="' . Lang::$word->_DELETE . '"><i class="icon remove"></i></a>';
						  }
						  $html .= ' </div>
						' . $avurl . '
						<div class="content">
						  <div class="header">' . $row->title . '</div>
						</div>
					  </div>
					</div>
				  </section>
				  <article> ' . cleanOut($row->body) . ' </article>
				  <footer>
					<div class="veriasist divided horizontal list">
					  <div class="item"><i class="icon time"></i>';
						if ($row->is_reply) {
						$html .= Lang::$word->_MOD_FB_REPLIED . ': ' . timesince(time()) . Lang::$word->_MOD_FB_BY . ': ' . $uurl;
						} else {
						$html .= Lang::$word->_MOD_FB_CREATED . ': ' . timesince(time()) . Lang::$word->_MOD_FB_BY . ': ' . $uurl;
						}
						$html .= ' </div>
					  <div class="item"><i class="icon danger heart"></i>' . Lang::$word->_MOD_FB_LIKE . ': <b class="like">' . $row->like . '</b></div>
					</div>
				  </footer>
				  </div>';
			  }
			  unset($row);
	
			  $json['type'] = 'success';
			  $json['message'] = $html;
		  } else {
			  $json['type'] = 'nores';
			  $json['message'] = Filter::msgAlert(str_replace("[KEYWORD]", $string, Lang::$word->_MOD_FB_SEARCH_NORES), false);
		  }
	
		  print json_encode($json);
	  }
	  
	  
      /**
       * Forum::getAccessList()
       * 
       * @param bool $sel
       * @return
       */
      public static function getAccessList($sel)
	  {
		  switch($sel) {
			  case "Public":
			  print Lang::$word->_PUBLIC;
			  break;
			  
			  case "Registered":
			  print Lang::$word->_REGISTERED;
			  break;
			  
			  case "Membership":
			  print Lang::$word->_MEMBERSHIP;
			  break;
		  }
		  
      }

      /**
       * Forum::getMembershipAccess()
       * 
       * @param mixed $memid
       * @return
       */
	  public static function getMembershipAccess($memid)
	  {
	
		  $m_arr = explode(",", $memid);
		  reset($m_arr);
		  if ($memid > 0) {
			  if (Registry::get("Users")->logged_in and Registry::get("Users")->validateMembership() and in_array(Registry::get("Users")->membership_id, $m_arr)) {
				  return true;
			  } else
				  return false;
		  } else
			  return true;
	  }

      /**
       * Forum::listMemberships()
       * 
       * @param mixed $memid
       * @return
       */
	  public static function listMemberships($memid)
	  {
	
		  $data = self::$db->fetch_all("SELECT title" . Lang::$lang . " as mtitle FROM " . Membership::mTable . " WHERE id IN(" . $memid . ")");
		  if ($data) {
			  $display = ' ' . Lang::$word->_MOD_AM_MEMBREQ;
			  $display .= '<ul class="veriasist numbered list">';
			  foreach ($data as $row) {
				  $display .= '<li>' . $row->mtitle . '</li>';
			  }
			  $display .= '</ul>';
			  return $display;
		  }
	
	  }
	   
	  /**
	   * Forum::updateOrder()
	   * 
	   * @return
	   */
	  public static function updateOrder()
	  {
		  foreach ($_POST['node'] as $k => $v) {
			  $p = $k + 1;
			  $data['ordering'] = intval($p);
			  self::$db->update(self::mTable, $data, "id='" . (int)$v . "'");
		  }
		  
	  }

	  /**
	   * Forum::titleExists()
	   * 
	   * @param mixed $title
	   * @return
	   */
	  private static function titleExists($title)
	  {
		  
		  $sql = self::$db->query("SELECT title"
		  . "\n FROM " .self::tTable
		  . "\n WHERE title = '" . sanitize($title) . "'" 
		  . "\n LIMIT 1");
		  
		  if (self::$db->numrows($sql) == 1) {
			  return true;
		  } else
			  return false;
	  }	
	  
	  /**
	   * Forum::keepTags()
	   *
	   * @param mixed $str
	   * @param mixed $tags
	   * @return
	   */
	  public static function keepTags($string, $allowtags = null, $allowattributes = null)
	  {
		  $string = strip_tags($string, $allowtags);
		  if (!is_null($allowattributes)) {
			  if (!is_array($allowattributes))
				  $allowattributes = explode(",", $allowattributes);
			  if (is_array($allowattributes))
				  $allowattributes = implode(")(?<!", $allowattributes);
			  if (strlen($allowattributes) > 0)
				  $allowattributes = "(?<!" . $allowattributes . ")";
			  $string = preg_replace_callback("/<[^>]*>/i", create_function('$matches', 'return preg_replace("/ [^ =]*' . $allowattributes . '=(\"[^\"]*\"|\'[^\']*\')/i", "", $matches[0]);'), $string);
		  }
		  return $string;
	  }  	
  }
?>
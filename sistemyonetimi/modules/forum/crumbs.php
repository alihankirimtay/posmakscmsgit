<?php
  /**
   * Crumbs
   *
   * @package VeriAsist
   * @author veriasist.com
   * @copyright 2014
   * @version $Id: crumbs.php, v4.00 2014-03-20 10:12:05 gewa Exp $
   */
  if (!defined("_VALID_PHP"))
      die('Direct access to this location is not allowed.');

  require_once ("admin_class.php");
?>
<?php
  $length = count($this->_url);
  switch ($length) {
      case 3:
          if (in_array(doUrlParts("forum-user"), $this->_url)) {
              $nav = '<a href="' . doUrl(false, false, "forum") . '" class="section">' . $this->moduledata->{'title' . Lang::$lang} . '</a>';
              $nav .= '<span class="divider"></span> ';
              $nav .= '<div class="section active">' . Lang::$word->_USER . '</div>';
              $nav .= '<span class="divider"></span> ';
              $nav .= '<div class="active section">' . $this->_url[2] . '</div>';
          } else {
              $nav = '<a href="' . doUrl(false, false, "forum") . '" class="section">' . $this->moduledata->{'title' . Lang::$lang} . '</a>';
              $nav .= '<span class="divider"></span>';
              $nav .= '<div class="active section"> ' . $this->moduledata->mod->title . ' </div>';
          }
          break;

      case 2:
          $nav = '<a href="' . doUrl(false, false, "forum") . '" class="section">' . $this->moduledata->{'title' . Lang::$lang} . '</a>';
          $nav .= '<span class="divider"></span>';
          $nav .= '<a class="section" href="' . doUrl(false, $this->moduledata->mod->slug, "forum-topic") . '">' . $this->moduledata->mod->title . '</a>';
          $nav .= '<span class="divider"></span>';
          $nav .= '<div class="active section"> ' . $this->moduledata->mod->ftitle . ' </div>';
          break;

      default:
          $nav = '<div class="active section">' . $this->moduledata->{'title' . Lang::$lang} . '</div>';
          break;
  }
?>
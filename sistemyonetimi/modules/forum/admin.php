<?php
  /**
   * Forum
   *
   * @package VeriAsist
   * @author veriasist.com
   * @copyright 2010
   * @version $Id: admin.php, v2.00 2011-04-20 10:12:05 gewa Exp $
   */
  if (!defined("_VALID_PHP"))
      die('Direct access to this location is not allowed.');
  
  if(!$user->getAcl("forum")): print Filter::msgAlert(Lang::$word->_CG_ONLYADMIN); return; endif;
  Registry::set('Forum', new Forum());
?>
<?php switch(Filter::$maction): case "edit": ?>
<?php $row = Core::getRowById(Forum::mTable, Filter::$id);?>
<div class="veriasist icon heading message blue"> <a class="helper veriasist top right info corner label" data-help="forum"><i class="icon help"></i></a> <i class="puzzle piece icon"></i>
  <div class="content">
    <div class="header"> <?php echo Lang::$word->_MOD_FB_TITLE4;?> </div>
    <div class="veriasist breadcrumb"><i class="icon home"></i> <a href="index.php" class="section"><?php echo Lang::$word->_N_DASH;?></a>
      <div class="divider"> / </div>
      <a href="index.php?do=modules" class="section"><?php echo Lang::$word->_N_MODS;?></a>
      <div class="divider"> / </div>
      <a href="index.php?do=modules&amp;action=config&amp;modname=forum" class="section"><?php echo $content->getModuleName(Filter::$modname);?></a>
      <div class="divider"> / </div>
      <div class="active section"><?php echo Lang::$word->_MOD_FB_TITLE1;?></div>
    </div>
  </div>
</div>
<div class="veriasist-large-content">
  <div class="veriasist message"><?php echo Core::langIcon();?><?php echo Lang::$word->_MOD_FB_INFO1. Lang::$word->_REQ1 . '<i class="icon asterisk"></i>' . Lang::$word->_REQ2;?></div>
  <div class="veriasist form segment">
    <div class="veriasist header"><?php echo Lang::$word->_MOD_FB_SUBTITLE1 . $row->title;?></div>
    <div class="veriasist double fitted divider"></div>
    <form id="veriasist_form" name="veriasist_form" method="post">
      <div class="two fields">
        <div class="field">
          <label><?php echo Lang::$word->_MOD_FB_NAME;?></label>
          <label class="input"><i class="icon-append icon asterisk"></i>
            <input type="text" value="<?php echo $row->title;?>" name="title">
          </label>
        </div>
        <div class="field">
          <label><?php echo Lang::$word->_MOD_FB_SLUG;?></label>
          <input type="text" value="<?php echo $row->slug;?>" name="slug">
        </div>
      </div>
      <div class="two fields">
        <div class="field">
          <label><?php echo Lang::$word->_MOD_FB_ACCESS;?></label>
          <select name="access" id="access_id" data-id="<?php echo $row->id;?>">
            <?php echo $member->getAccessList($row->access);?>
          </select>
        </div>
        <div class="field" id="memrow">
          <label><?php echo Lang::$word->_PG_MEM_LEVEL;?></label>
          <div id="membership">
            <?php if($row->membership_id == 0):?>
            <input disabled="disabled" type="text" value="<?php echo Lang::$word->_PG_NOMEM_REQ;?>" name="na">
            <?php else:?>
            <?php echo $member->getMembershipList($row->membership_id);?>
            <?php endif;?>
          </div>
        </div>
      </div>
      <div class="two fields">
        <div class="field">
          <label><?php echo Lang::$word->_MOD_FB_PPOST;?></label>
          <div class="inline-group">
            <label class="radio">
              <input name="publicpost" type="radio" value="1" <?php echo getChecked($row->publicpost, 1);?>>
              <i></i><?php echo Lang::$word->_YES;?></label>
            <label class="radio">
              <input name="publicpost" type="radio" value="0" <?php echo getChecked($row->publicpost, 0);?>>
              <i></i><?php echo Lang::$word->_NO;?></label>
          </div>
        </div>
        <div class="field">
          <label><?php echo Lang::$word->_PUBLISHED;?></label>
          <div class="inline-group">
            <label class="radio">
              <input name="active" type="radio" value="1" <?php echo getChecked($row->active, 1);?>>
              <i></i><?php echo Lang::$word->_YES;?></label>
            <label class="radio">
              <input name="active" type="radio" value="0" <?php echo getChecked($row->active, 0);?>>
              <i></i><?php echo Lang::$word->_NO;?></label>
          </div>
        </div>
      </div>
      <div class="veriasist divider"></div>
      <div class="field">
        <label><?php echo Lang::$word->_MOD_FB_ICON;?></label>
        <div class="scrollbox">
          <div id="scroll-icons"> <?php print Core::iconList($row->icon);?> </div>
          <input name="icon" type="hidden" value="<?php echo $row->icon;?>">
        </div>
      </div>
      <div class="field">
        <label><?php echo Lang::$word->_MOD_FB_SHORTDESC;?></label>
        <textarea name="description"><?php echo $row->description;?></textarea>
      </div>
      <div class="two fields">
        <div class="field">
          <label><?php echo Lang::$word->_METAKEYS;?></label>
          <textarea name="metakey<?php echo Lang::$lang;?>"><?php echo $row->{'metakey'.Lang::$lang};?></textarea>
        </div>
        <div class="field">
          <label><?php echo Lang::$word->_METADESC;?></label>
          <textarea name="metadesc<?php echo Lang::$lang;?>"><?php echo $row->{'metadesc'.Lang::$lang};?></textarea>
        </div>
      </div>
      <div class="veriasist double fitted divider"></div>
      <button type="button" name="dosubmit" class="veriasist positive button"><?php echo Lang::$word->_MOD_FB_UPDATE;?></button>
      <a href="index.php?do=modules&amp;action=config&amp;modname=forum" class="veriasist basic button"><?php echo Lang::$word->_CANCEL;?></a>
      <input name="processForum" type="hidden" value="1">
      <input name="id" type="hidden" value="<?php echo Filter::$id;?>">
    </form>
  </div>
  <div id="msgholder"></div>
</div>
<script type="text/javascript"> 
// <![CDATA[
$(document).ready(function() {
    $('#access_id').change(function () {
        var option = $(this).val();
        var result = 'mid=' + $(this).data('id');;
        result += '&membershiplist=' + option;
        $.ajax({
            type: "post",
            url: "controller.php",
            data: result,
            cache: false,
            success: function (res) {
                (option == "Membership") ? $('#memrow').show() : $('#memrow').hide();
                $('#membership').html(res);
                $("select").chosen({
                    disable_search_threshold: 10,
                    width: "100%"
                });
            }
        });
    });
});
// ]]>
</script>
<?php break;?>
<?php case"add": ?>
<div class="veriasist icon heading message blue"> <a class="helper veriasist top right info corner label" data-help="forum"><i class="icon help"></i></a> <i class="puzzle piece icon"></i>
  <div class="content">
    <div class="header"> <?php echo Lang::$word->_MOD_FB_TITLE4;?> </div>
    <div class="veriasist breadcrumb"><i class="icon home"></i> <a href="index.php" class="section"><?php echo Lang::$word->_N_DASH;?></a>
      <div class="divider"> / </div>
      <a href="index.php?do=modules" class="section"><?php echo Lang::$word->_N_MODS;?></a>
      <div class="divider"> / </div>
      <a href="index.php?do=modules&amp;action=config&amp;modname=forum" class="section"><?php echo $content->getModuleName(Filter::$modname);?></a>
      <div class="divider"> / </div>
      <div class="active section"><?php echo Lang::$word->_MOD_FB_TITLE2;?></div>
    </div>
  </div>
</div>
<div class="veriasist-large-content">
  <div class="veriasist message"><?php echo Core::langIcon();?><?php echo Lang::$word->_MOD_FB_INFO2. Lang::$word->_REQ1 . '<i class="icon asterisk"></i>' . Lang::$word->_REQ2;?></div>
  <div class="veriasist form segment">
    <div class="veriasist header"><?php echo Lang::$word->_MOD_FB_SUBTITLE2;?></div>
    <div class="veriasist double fitted divider"></div>
    <form id="veriasist_form" name="veriasist_form" method="post">
      <div class="two fields">
        <div class="field">
          <label><?php echo Lang::$word->_MOD_FB_NAME;?></label>
          <label class="input"><i class="icon-append icon asterisk"></i>
            <input type="text" placeholder="<?php echo Lang::$word->_MOD_FB_NAME;?>" name="title">
          </label>
        </div>
        <div class="field">
          <label><?php echo Lang::$word->_MOD_FB_SLUG;?></label>
          <label class="input">
            <input type="text" placeholder="<?php echo Lang::$word->_MOD_FB_SLUG;?>" name="slug">
          </label>
        </div>
      </div>
      <div class="two fields">
        <div class="field">
          <label><?php echo Lang::$word->_MOD_FB_ACCESS;?></label>
          <select name="access" id="access_id" data-id="-1">
            <?php echo $member->getAccessList();?>
          </select>
        </div>
        <div class="field" id="memrow">
          <label><?php echo Lang::$word->_PG_MEM_LEVEL;?></label>
          <div id="membership">
            <input type="text" disabled="disabled" value="<?php echo Lang::$word->_PG_NOMEM_REQ;?>" name="na">
          </div>
        </div>
      </div>
      <div class="two fields">
        <div class="field">
          <label><?php echo Lang::$word->_MOD_FB_PPOST;?></label>
          <div class="inline-group">
            <label class="radio">
              <input name="publicpost" type="radio" value="1" checked="checked">
              <i></i><?php echo Lang::$word->_YES;?></label>
            <label class="radio">
              <input name="publicpost" type="radio" value="0">
              <i></i><?php echo Lang::$word->_NO;?></label>
          </div>
        </div>
        <div class="field">
          <label><?php echo Lang::$word->_PUBLISHED;?></label>
          <div class="inline-group">
            <label class="radio">
              <input name="active" type="radio" value="1" checked="checked">
              <i></i><?php echo Lang::$word->_YES;?></label>
            <label class="radio">
              <input name="active" type="radio" value="0">
              <i></i><?php echo Lang::$word->_NO;?></label>
          </div>
        </div>
      </div>
      <div class="veriasist divider"></div>
      <div class="field">
        <label><?php echo Lang::$word->_MOD_FB_ICON;?></label>
        <div class="scrollbox">
          <div id="scroll-icons"> <?php print Core::iconList();?> </div>
          <input name="icon" type="hidden" value="">
        </div>
      </div>
      <div class="field">
        <label><?php echo Lang::$word->_MOD_FB_SHORTDESC;?></label>
        <textarea placeholder="<?php echo Lang::$word->_MOD_FB_SHORTDESC;?>" name="description"></textarea>
      </div>
      <div class="two fields">
        <div class="field">
          <label><?php echo Lang::$word->_METAKEYS;?></label>
          <textarea placeholder="<?php echo Lang::$word->_METAKEYS;?>" name="metakey<?php echo Lang::$lang;?>"></textarea>
        </div>
        <div class="field">
          <label><?php echo Lang::$word->_METADESC;?></label>
          <textarea placeholder="<?php echo Lang::$word->_METADESC;?>" name="metadesc<?php echo Lang::$lang;?>"></textarea>
        </div>
      </div>
      <div class="veriasist double fitted divider"></div>
      <button type="button" name="dosubmit" class="veriasist positive button"><?php echo Lang::$word->_MOD_FB_ADD;?></button>
      <a href="index.php?do=modules&amp;action=config&amp;modname=forum" class="veriasist basic button"><?php echo Lang::$word->_CANCEL;?></a>
      <input name="processForum" type="hidden" value="1">
    </form>
  </div>
  <div id="msgholder"></div>
</div>
<script type="text/javascript"> 
// <![CDATA[
$(document).ready(function() {
    $('#access_id').change(function () {
        var option = $(this).val();
        var result = 'mid=' + $(this).data('id');;
        result += '&membershiplist=' + option;
        $.ajax({
            type: "post",
            url: "controller.php",
            data: result,
            cache: false,
            success: function (res) {
                (option == "Membership") ? $('#memrow').show() : $('#memrow').hide();
                $('#membership').html(res);
                $("select").chosen({
                    disable_search_threshold: 10,
                    width: "100%"
                });
            }
        });
    });
});
// ]]>
</script>
<?php break;?>
<?php case"config": ?>
<?php $row = Registry::get("Forum");?>
<div class="veriasist icon heading message blue"> <a class="helper veriasist top right info corner label" data-help="fbconfig"><i class="icon help"></i></a> <i class="puzzle piece icon"></i>
  <div class="content">
    <div class="header"> <?php echo Lang::$word->_MOD_FB_TITLE4;?> </div>
    <div class="veriasist breadcrumb"><i class="icon home"></i> <a href="index.php" class="section"><?php echo Lang::$word->_N_DASH;?></a>
      <div class="divider"> / </div>
      <a href="index.php?do=modules" class="section"><?php echo Lang::$word->_N_MODS;?></a>
      <div class="divider"> / </div>
      <a href="index.php?do=modules&amp;action=config&amp;modname=forum" class="section"><?php echo $content->getModuleName(Filter::$modname);?></a>
      <div class="divider"> / </div>
      <div class="active section"><?php echo Lang::$word->_MOD_FB_TITLE3;?></div>
    </div>
  </div>
</div>
<div class="veriasist-large-content">
  <div class="veriasist message"><?php echo Core::langIcon();?><?php echo Lang::$word->_MOD_FB_INFO3. Lang::$word->_REQ1 . '<i class="icon asterisk"></i>' . Lang::$word->_REQ2;?></div>
  <div class="veriasist form segment">
    <div class="veriasist header"><?php echo Lang::$word->_MOD_FB_SUBTITLE3;?></div>
    <div class="veriasist double fitted divider"></div>
    <form id="veriasist_form" name="veriasist_form" method="post">
      <div class="three fields">
        <div class="field">
          <label><?php echo Lang::$word->_MOD_FB_IPP;?></label>
          <input type="text" class="slrange" value="<?php echo $row->ipp;?>" name="ipp">
        </div>
        <div class="field">
          <label><?php echo Lang::$word->_MOD_FB_HASH;?></label>
          <input type="text" value="<?php echo $row->secret;?>" name="secret">
        </div>
        <div class="field">
          <label><?php echo Lang::$word->_MOD_FB_SORT;?></label>
          <div class="inline-group">
            <label class="radio">
              <input name="sorting" type="radio" value="ASC" <?php echo getChecked($row->sorting, "ASC");?>>
              <i></i><?php echo Lang::$word->_MOD_FB_SORT_D;?></label>
            <label class="radio">
              <input name="sorting" type="radio" value="DESC" <?php echo getChecked($row->sorting, "DESC");?>>
              <i></i><?php echo Lang::$word->_MOD_FB_SORT_A;?></label>
          </div>
        </div>
      </div>
      <div class="field">
        <label><?php echo Lang::$word->_MOD_FB_TEMPLATE;?></label>
        <textarea class="plugpost" name="template"><?php echo Filter::out_url($row->template);?></textarea>
      </div>
      <div class="veriasist double fitted divider"></div>
      <button type="button" name="dosubmit" class="veriasist positive button"><?php echo Lang::$word->_MOD_FB_CUPDATE;?></button>
      <a href="index.php?do=modules&amp;action=config&amp;modname=forum" class="veriasist basic button"><?php echo Lang::$word->_CANCEL;?></a>
      <input name="processConfig" type="hidden" value="1">
    </form>
  </div>
  <div id="msgholder"></div>
</div>
<script type="text/javascript"> 
// <![CDATA[
$(document).ready(function () {
    $("input[name=ipp]").ionRangeSlider({
		min: 6,
		max: 20,
        step: 2,
		postfix: " ipp",
        type: 'single',
        hasGrid: true
    });
});
// ]]>
</script>
<?php break;?>
<?php default: ?>
<?php $forumrow = Registry::get("Forum")->getForums();?>
<div class="veriasist icon heading message blue"> <i class="puzzle piece icon"></i>
  <div class="content">
    <div class="header"> <?php echo Lang::$word->_MOD_FB_TITLE4;?> </div>
    <div class="veriasist breadcrumb"><i class="icon home"></i> <a href="index.php" class="section"><?php echo Lang::$word->_N_DASH;?></a>
      <div class="divider"> / </div>
      <a href="index.php?do=modules" class="section"><?php echo Lang::$word->_N_MODS;?></a>
      <div class="divider"> / </div>
      <div class="active section"><?php echo $content->getModuleName(Filter::$modname);?></div>
    </div>
  </div>
</div>
<div class="veriasist-large-content">
  <div class="veriasist message"><?php echo Core::langIcon();?><?php echo Lang::$word->_MOD_FB_INFO4;?></div>
  <div class="veriasist segment">
    <div class="push-right">
      <div class="veriasist right pointing dropdown icon info button"> <i class="settings icon"></i>
        <div class="menu"> <a class="item" href="<?php echo Core::url("modules", "add");?>"><i class="icon add"></i><?php echo Lang::$word->_MOD_FB_ADDNEW;?></a> <a class="item" href="<?php echo Core::url("modules", "config");?>"><i class="icon setting"></i><?php echo Lang::$word->_MOD_FB_CONFIG;?></a> </div>
      </div>
    </div>
    <div class="veriasist header"><?php echo Lang::$word->_MOD_FB_SUBTITLE4;?></div>
    <div class="veriasist fitted divider"></div>
    <table class="veriasist sortable table">
      <thead>
        <tr>
          <th class="disabled"></th>
          <th data-sort="string"><?php echo Lang::$word->_MOD_FB_NAME;?></th>
          <th data-sort="string"><?php echo Lang::$word->_MOD_FB_ACCESS;?></th>
          <th data-sort="int"><?php echo Lang::$word->_CREATED;?></th>
          <th data-sort="int"><?php echo Lang::$word->_MOD_FB_TOTALPOSTS;?></th>
          <th class="disabled"><?php echo Lang::$word->_ACTIONS;?></th>
        </tr>
      </thead>
      <tbody>
        <?php if(!$forumrow):?>
        <tr>
          <td colspan="6"><?php echo Filter::msgSingleAlert(Lang::$word->_MOD_FB_NOFORUM);?></td>
        </tr>
        <?php else:?>
        <?php foreach ($forumrow as $row):?>
        <tr id="node-<?php echo $row->id;?>">
          <td class="id-handle"><i class="icon reorder"></i></td>
          <td><?php echo $row->title;?></td>
          <td><?php Forum::getAccessList($row->access)?></td>
          <td data-sort-value="<?php echo strtotime($row->created);?>"><?php echo Filter::dodate("long_date", $row->created);?></td>
          <td><span class="veriasist black label"><?php echo $row->totalposts;?></span></td>
          <td><a href="<?php echo Core::url("modules", "edit", $row->id);?>"><i class="rounded inverted success icon pencil link"></i></a> <a class="delete" data-title="<?php echo Lang::$word->_DELETE.' '.Lang::$word->_MOD_FB_FORUM;?>" data-option="deleteForum" data-id="<?php echo $row->id;?>" data-name="<?php echo $row->title;?>"><i class="rounded danger inverted remove icon link"></i></a></td>
        </tr>
        <?php endforeach;?>
        <?php unset($row);?>
        <?php endif;?>
      </tbody>
    </table>
  </div>
</div>
<script type="text/javascript"> 
// <![CDATA[
$(document).ready(function () {
    $(".veriasist.table tbody").sortable({
        helper: 'clone',
        handle: '.id-handle',
        placeholder: 'placeholder',
        opacity: .6,
        update: function (event, ui) {
            serialized = $(".veriasist.table tbody").sortable('serialize');
            $.ajax({
                type: "POST",
                url: "modules/forum/controller.php?sortforum",
                data: serialized,
                success: function (msg) {}
            });
        }
    });
});
// ]]>
</script>
<?php break;?>
<?php endswitch;?>
<?php
  /**
   * Web Sites
   *
   * @package VeriAsist
   * @author veriasist.com
   * @copyright 2014
   * @version $Id: class_admin.php, v4.00 2014-12-24 12:12:12 gewa Exp $
   */
  if (!defined("_VALID_PHP"))
    die('Direct access to this location is not allowed.');
  
  if(!$user->getAcl("selfupdate")): print Filter::msgAlert(Lang::$word->_CG_ONLYADMIN); return; endif;
  
  Registry::set('SelfUpdate', new SelfUpdate());


  function dateCalc($date1, $date2) {

    $dateCurrent = new DateTime($date1);
    $dateExpire = new DateTime($date2);
    $interval = $dateCurrent->diff($dateExpire);

    $date = "$interval->days gün";
    return $date;
  }
  ?>

  <?php switch(Filter::$maction): case "edit": ?>
    <?php $row = Registry::get("Database")->first("SELECT * FROM mod_selfupdate_settings");?>
      <div class="veriasist icon heading message blue"> <a class="helper veriasist top right info corner label" data-help="selfupdate"><i class="icon help"></i></a> <i class="puzzle piece icon"></i>
        <div class="content">
          <div class="header"> <?php echo Lang::$word->_MOD_SU_TITLE2;?> </div>
          <div class="veriasist breadcrumb"><i class="icon home"></i> <a href="index.php" class="section"><?php echo Lang::$word->_N_DASH;?></a>
            <div class="divider"> / </div>
            <a href="index.php?do=modules" class="section"><?php echo Lang::$word->_N_MODS;?></a>
            <div class="divider"> / </div>
            <a href="index.php?do=modules&amp;action=config&amp;modname=selfupdate" class="section"><?php echo $content->getModuleName(Filter::$modname);?></a>
            <div class="divider"> / </div>
            <div class="active section"><?php echo Lang::$word->_MOD_SU_TITLE2;?></div>
          </div>
        </div>
      </div>
      <div class="veriasist-large-content">
        <div class="veriasist message"><?php echo Core::langIcon();?><?php echo Lang::$word->_MOD_SU_INFO2. Lang::$word->_REQ1 . '<i class="icon asterisk"></i>' . Lang::$word->_REQ2;?></div>
       <div class="veriasist form segment">
        <div class="veriasist header"><?php echo Lang::$word->_MOD_SU_SUBTITLE2;?></div>
        <div class="veriasist double fitted divider"></div>
        <form id="veriasist_form" name="veriasist_form" method="post">
         
            <div class="field">
              <label><?php echo Lang::$word->_MOD_SU_NAME;?></label>
              <label class="input"><i class="icon-append icon asterisk"></i>
                <input type="text" placeholder="<?php echo $row->account; ?>" name="account">
              </label>
            </div>
            <div class="field">
              <label><?php echo Lang::$word->_MOD_SU_REPO;?></label>
              <label class="input"><i class="icon-append icon asterisk"></i>
                <input type="text" placeholder="<?php echo $row->repo_slug?>" name="repo_slug">
              </label>
            </div>

            <div class="field">
              <label><?php echo Lang::$word->_MOD_SU_PASS;?></label>
              <label class="input"><i class="icon-append icon asterisk"></i>
                <input type="password" name="password" placeholder="<?php echo Lang::$word->_MOD_SU_PASS;?>" >
              </label>
            </div>

             <div class="field">
              <div class="two fields">
                <div class="field">
                  <label><?php echo Lang::$word->_MOD_SU_bMAIN;?></label>
                  <label class="input"><i class="icon-append icon asterisk"></i>
                    <input type="text"  name="branch_main" placeholder="<?= $row->branch_main?>" >
                  </label>
                </div>
                <div class="field">
                  <label><?php echo Lang::$word->_MOD_SU_bTEST;?></label>
                  <label class="input"><i class="icon-append icon asterisk"></i>
                    <input type="text" name="branch_test" placeholder="<?= $row->branch_test?>">
                  </label>
                </div>
              </div>
            </div>

            <!-- oAuth Yapıldığında aktif edilecek
            <div class="field radio">
              <div class="inline-group">
                <input name="banner_type" type="radio" value="0" checked="checked">
                  <i></i><?php echo Lang::$word->_MOD_SU_OAUTH;?>
              </div>
            </div>

            <div class="field">
              <div class="two fields">
                <div class="field">
                  <label><?php echo Lang::$word->_MOD_SU_cKEY;?></label>
                  <label class="input"><i class="icon-append icon asterisk"></i>
                    <input type="text" name="client_key" placeholder="<?= $row->client_key?>">
                  </label>
                </div>
                <div class="field">
                  <label><?php echo Lang::$word->_MOD_SU_cSECRET;?></label>
                  <label class="input"><i class="icon-append icon asterisk"></i>
                    <input type="text" name="client_secret" placeholder="<?= $row->client_secret?>">
                  </label>
                </div>
              </div>
            </div>
            -->
          <div class="veriasist double fitted divider"></div>
          <button type="button" name="dosubmit" class="veriasist positive button"><?php echo Lang::$word->_SAVE;?></button>
          <a href="index.php?do=modules&amp;action=config&amp;modname=selfupdate" class="veriasist basic button"><?php echo Lang::$word->_CANCEL;?></a>
          <input name="processSelfUpdate" type="hidden" value="1">
        </form>
      </div>
      <div id="msgholder"></div>
    </div>
    <?php break; ?>
    <?php case "updateAll": ?>
      <div class="veriasist icon heading message blue"> <a class="helper veriasist top right info corner label" data-help="websites"><i class="icon help"></i></a> <a class="helper veriasist top right info corner label" data-help="websites"><i class="icon help"></i></a> <i class="puzzle piece icon"></i>
        <div class="content">
          <div class="header"> <?php echo Lang::$word->_MOD_SU_TITLE1;?> </div>
          <div class="veriasist breadcrumb"><i class="icon home"></i> <a href="index.php" class="section"><?php echo Lang::$word->_N_DASH;?></a>
            <div class="divider"> / </div>
            <a href="index.php?do=modules" class="section"><?php echo Lang::$word->_N_MODS;?></a>
            <div class="divider"> / </div>
            <a href="index.php?do=modules&amp;action=config&amp;modname=selfupdate" class="section"><?php echo $content->getModuleName(Filter::$modname);?></a>
            <div class="divider"> / </div>
            <div class="active section"><?php echo Lang::$word->_MOD_SU_TITLE1;?></div>
          </div>
        </div>
      </div>
    <?php break; ?>
    <?php default: ?>
      <div class="veriasist icon heading message blue"> <a class="helper veriasist top right info corner label" data-help="websites"><i class="icon help"></i></a> <a class="helper veriasist top right info corner label" data-help="websites"><i class="icon help"></i></a> <i class="puzzle piece icon"></i>
        <div class="content">
          <div class="header"> <?php echo Lang::$word->_MOD_SU_TITLE1;?> </div>
          <div class="veriasist breadcrumb"><i class="icon home"></i> <a href="index.php" class="section"><?php echo Lang::$word->_N_DASH;?></a>
            <div class="divider"> / </div>
            <a href="index.php?do=modules" class="section"><?php echo Lang::$word->_N_MODS;?></a>
            <div class="divider"> / </div>
            <a href="index.php?do=modules&amp;action=config&amp;modname=selfupdate" class="section"><?php echo $content->getModuleName(Filter::$modname);?></a>
            <div class="divider"> / </div>
            <div class="active section"><?php echo Lang::$word->_MOD_SU_TITLE1;?></div>
          </div>
        </div>
      </div>
      <?php $gmrow = Registry::get("SelfUpdate")->getUpdates();?>
      <div class="veriasist message"><?php echo Core::langIcon();?><?php echo Lang::$word->_MOD_SU_INFO1?></div>
      <div class="veriasist segment">
        <table class="veriasist sortable table">
          <div class="veriasist header"><?php echo Lang::$word->_MOD_SU_CUST_TITLE1;?> 
          <a class="veriasist icon warning button push-right" href="index.php?do=modules&amp;action=config&amp;modname=selfupdate&amp;maction=updateAll"><i class="icon add"></i> <?php echo Lang::$word->_MOD_SU_ADD;?></a>
          <a class="veriasist icon succes button push-right" href="index.php?do=modules&amp;action=config&amp;modname=selfupdate&amp;maction=edit" style="margin-right:10px;"><i class="icon setting"></i><?php echo Lang::$word->_MOD_SU_ACCOUNT;?></a>
        </div>

        <thead>
          <tr>
            <th data-sort="int">#</th>
            <th data-sort="string"><?php echo Lang::$word->_MOD_SU_COMMIT;?></th>
            <th data-sort="string"><?php echo Lang::$word->_MOD_SU_PATH;?></th>
            <th data-sort="string"><?php echo Lang::$word->_MOD_SU_APPEAL_DATE;?></th>
            <th class="disabled"><?php echo Lang::$word->_ACTIONS;?></th>
            
          </tr>
        </thead>
        <tbody>
         <?php if(!$gmrow):?>
          <tr>
            <td colspan="6"><?php Filter::msgSingleInfo(Lang::$word->_MOD_SU_NOGMAPS);?></td>
          </tr>
          <?php else:?>
            <?php foreach ($gmrow as $row):?>
              <?php $date = "Oluşturulmadı"; ?>
              <tr>
                <td><?php echo $row->id;?></td>
                <td><?php echo $row->commit;?></td>
                <td><a href="<?= SITEURL."/".$row->path?>" target="_blank"><?php echo $row->path;?></a></td>
                <td data-sort-value="<?php echo strtotime($row->created);?>"><?php echo Filter::dodate("short_date", $row->created);?></td>
               <td><a class="delete" data-option="deleteArchive" data-id="<?php echo $row->id;?>" data-name="<?php echo $row->commit;?>"><i class="rounded danger inverted remove icon link"></i></a></td>

              <?php endforeach; ?>
            <?php endif; ?>
          </tbody>
        </table>
      </div>
      <div class="veriasist-grid">
      <div class="two columns horizontal-gutters">
        <div class="row"> <span class="veriasist label"><?php echo Lang::$word->_PAG_TOTAL.': '.$pager->items_total;?> / <?php echo Lang::$word->_PAG_CURPAGE.': '.$pager->current_page.' '.Lang::$word->_PAG_OF.' '.$pager->num_pages;?></span> </div>
        <div class="row">
          <div id="pagination"><?php echo $pager->display_pages();?></div>
        </div>
      </div>
    </div>
    <?php break; ?>
  <?php endswitch; ?>


<?php

  /**
   * Web Sites Class
   *
   * @package VeriAsist
   * @author veriasist.com
   * @copyright 2012
   * @version $Id: class_admin.php, v1.00 2012-12-24 12:12:12 gewa Exp $
   */
  
  if (!defined("_VALID_PHP"))
    die('Direct access to this location is not allowed.');


  
  require_once'C://xampp/htdocs/posmakscmsgit/lib/bitbucket_api/autoload.php'; // Dinamikleştirilmeli
  class SelfUpdate
  {


    const mTable = "mod_selfupdate_archives";
    const setTable = "mod_selfupdate_settings";
    const sTable = "settings"; 
    const uTable = "users";
    const wTable = "mod_websites";
    const pluginspath = "plugins/selfupdate/";

    private static $db;


      /**
       * WebSites::__construct()
       * 
       * @return
       */
      function __construct()
      {
        self::$db = Registry::get("Database");
      }


      /**
       * WebSites::getWebSites()
       * 
       * @return
       */
      public function getUpdates()
      {           
        $sql = "SELECT * FROM ".self::mTable. " WHERE deleted IS NULL ORDER BY id ASC";
        return Registry::get("Database")->fetch_all($sql);
      }

      public function getLastUpdate($branch)
      {
        $query = "SELECT commit FROM ".self::mTable." archive LEFT JOIN ".self::sTable." sets ON (archive.id=sets.version) WHERE branch_name = '".$branch."' AND deleted IS NULL ORDER BY id DESC LIMIT 1";
       
        return (Registry::get("Database")->first($query)->commit);
      }

      public function processSelfUpdate($post)
      {
        if ($post) {
         $settings = Registry::get("Database")->first("SELECT * FROM ".self::setTable);
         if ($settings) {

          //oAuth Eklendikten sonra düzenlenecek
           Registry::get("Database")->query('UPDATE '.self::setTable." 
            SET account = '{$post['account']}',
            repo_slug = '{$post['repo_slug']}',
            account_pass = '{$post['password']}',
            branch_main = '{$post['branch_main']}',
            branch_test = '{$post['branch_test']}'
            ");
           $message = Lang::$word->_MOD_SU_PUPDATED;
         } else {

             Registry::get("Database")->insert(self::mTable, [
              'account' => $post['account'],
              'repo_slug' => $post['repo_slug'],
              'account_pass' => $post['password'],
              'branch_main' => $post['branch_main'],
              'branch_test' => $post['branch_test']
            ]);
             $message = Lang::$word->_MOD_SU_PADDED;
         }
          if (self::$db->affected()) {
            Security::writeLog($message, "", "no", "module");
            $json['type'] = 'success';
            $json['message'] = Filter::msgOk($message, false);
          } else {
            $json['type'] = 'success';
            $json['message'] = Filter::msgAlert(Lang::$word->_SYSTEM_PROCCESS, false);
          }
        print json_encode($json);
       }
      }

      public function setAuthSettings()
      {
        $sql = " SELECT * FROM ".self::setTable;
        $row = Registry::get("Database")->first($sql);

        return [
          'oauth' => $row->oauth, //this is connection type 1=oAuth 0=Basic
          'oauth_params' => [
            'client_id' => $row->client_key,
            'client_secret' => $row->client_secret,
          ],
          'account' => $row->account,
          'repo_slug' => $row->repo_slug,
          'account_pass' => $row->account_pass,
          'branches' => [
            'branch_main' => $row->branch_main,
            'branch_test' => $row->branch_test,
          ],
        ];
      }

      public function deleteRecord($post = null)
      {
        Registry::get("Database")->query('UPDATE '.self::mTable." SET deleted = NOW() WHERE id ='{$post['id']}'");
        return true;
        
      }

      public function createArchive()
      {

        ini_set('log_errors', true);
        ini_set('error_log', ADMINURL.'modules/selfupdate/logs/'.date("YmdHis").'.log');

        try {

          //$inputs = json_decode(file_get_contents('php://input'));
          $inputs = json_decode('{"push": {"changes": [{"forced": false, "old": {"target": {"hash": "6afc6c429a2071687d2f67cf143604b09ef46c4d", "links": {"self": {"href": "https://api.bitbucket.org/2.0/repositories/alihankirimtay/auto_update_local/commit/6afc6c429a2071687d2f67cf143604b09ef46c4d"}, "html": {"href": "https://bitbucket.org/alihankirimtay/auto_update_local/commits/6afc6c429a2071687d2f67cf143604b09ef46c4d"}}, "author": {"raw": "Alihan K\u0131r\u0131mtay <alihan.kirimtay@kodar.com.tr>", "type": "author", "user": {"username": "alihankirimtay", "display_name": "Alihan K\u0131r\u0131mtay", "account_id": "557058:d42f52c3-0a6e-429a-8a0a-5f0836832dae", "links": {"self": {"href": "https://api.bitbucket.org/2.0/users/alihankirimtay"}, "html": {"href": "https://bitbucket.org/alihankirimtay/"}, "avatar": {"href": "https://bitbucket.org/account/alihankirimtay/avatar/"}}, "type": "user", "nickname": "alihankirimtay", "uuid": "{1f4e0061-7496-47f9-abcc-0f0a49f06bdb}"}}, "summary": {"raw": "\u015f\u00f6\u00e7", "markup": "markdown", "html": "<p>\u015f\u00f6\u00e7</p>", "type": "rendered"}, "parents": [{"type": "commit", "hash": "0cfff59dad1020343e1418ed82901cca6b1cc243", "links": {"self": {"href": "https://api.bitbucket.org/2.0/repositories/alihankirimtay/auto_update_local/commit/0cfff59dad1020343e1418ed82901cca6b1cc243"}, "html": {"href": "https://bitbucket.org/alihankirimtay/auto_update_local/commits/0cfff59dad1020343e1418ed82901cca6b1cc243"}}}], "date": "2018-12-13T17:19:56+00:00", "message": "\u015f\u00f6\u00e7", "type": "commit"}, "links": {"commits": {"href": "https://api.bitbucket.org/2.0/repositories/alihankirimtay/auto_update_local/commits/master"}, "self": {"href": "https://api.bitbucket.org/2.0/repositories/alihankirimtay/auto_update_local/refs/branches/master"}, "html": {"href": "https://bitbucket.org/alihankirimtay/auto_update_local/branch/master"}}, "default_merge_strategy": "merge_commit", "merge_strategies": ["merge_commit", "squash", "fast_forward"], "type": "branch", "name": "master"}, "links": {"commits": {"href": "https://api.bitbucket.org/2.0/repositories/alihankirimtay/auto_update_local/commits?include=59a75b0a478e1168363c91f13fd7e9ee42aa8e3e&exclude=6afc6c429a2071687d2f67cf143604b09ef46c4d"}, "html": {"href": "https://bitbucket.org/alihankirimtay/auto_update_local/branches/compare/59a75b0a478e1168363c91f13fd7e9ee42aa8e3e..6afc6c429a2071687d2f67cf143604b09ef46c4d"}, "diff": {"href": "https://api.bitbucket.org/2.0/repositories/alihankirimtay/auto_update_local/diff/59a75b0a478e1168363c91f13fd7e9ee42aa8e3e..6afc6c429a2071687d2f67cf143604b09ef46c4d"}}, "truncated": false, "commits": [{"hash": "59a75b0a478e1168363c91f13fd7e9ee42aa8e3e", "links": {"self": {"href": "https://api.bitbucket.org/2.0/repositories/alihankirimtay/auto_update_local/commit/59a75b0a478e1168363c91f13fd7e9ee42aa8e3e"}, "comments": {"href": "https://api.bitbucket.org/2.0/repositories/alihankirimtay/auto_update_local/commit/59a75b0a478e1168363c91f13fd7e9ee42aa8e3e/comments"}, "patch": {"href": "https://api.bitbucket.org/2.0/repositories/alihankirimtay/auto_update_local/patch/59a75b0a478e1168363c91f13fd7e9ee42aa8e3e"}, "html": {"href": "https://bitbucket.org/alihankirimtay/auto_update_local/commits/59a75b0a478e1168363c91f13fd7e9ee42aa8e3e"}, "diff": {"href": "https://api.bitbucket.org/2.0/repositories/alihankirimtay/auto_update_local/diff/59a75b0a478e1168363c91f13fd7e9ee42aa8e3e"}, "approve": {"href": "https://api.bitbucket.org/2.0/repositories/alihankirimtay/auto_update_local/commit/59a75b0a478e1168363c91f13fd7e9ee42aa8e3e/approve"}, "statuses": {"href": "https://api.bitbucket.org/2.0/repositories/alihankirimtay/auto_update_local/commit/59a75b0a478e1168363c91f13fd7e9ee42aa8e3e/statuses"}}, "author": {"raw": "Alihan K\u0131r\u0131mtay <alihan.kirimtay@kodar.com.tr>", "type": "author", "user": {"username": "alihankirimtay", "display_name": "Alihan K\u0131r\u0131mtay", "account_id": "557058:d42f52c3-0a6e-429a-8a0a-5f0836832dae", "links": {"self": {"href": "https://api.bitbucket.org/2.0/users/alihankirimtay"}, "html": {"href": "https://bitbucket.org/alihankirimtay/"}, "avatar": {"href": "https://bitbucket.org/account/alihankirimtay/avatar/"}}, "type": "user", "nickname": "alihankirimtay", "uuid": "{1f4e0061-7496-47f9-abcc-0f0a49f06bdb}"}}, "summary": {"raw": "\u011f\u011f", "markup": "markdown", "html": "<p>\u011f\u011f</p>", "type": "rendered"}, "parents": [{"type": "commit", "hash": "6afc6c429a2071687d2f67cf143604b09ef46c4d", "links": {"self": {"href": "https://api.bitbucket.org/2.0/repositories/alihankirimtay/auto_update_local/commit/6afc6c429a2071687d2f67cf143604b09ef46c4d"}, "html": {"href": "https://bitbucket.org/alihankirimtay/auto_update_local/commits/6afc6c429a2071687d2f67cf143604b09ef46c4d"}}}], "date": "2018-12-13T17:21:48+00:00", "message": "\u011f\u011f", "type": "commit"}], "created": false, "closed": false, "new": {"target": {"hash": "59a75b0a478e1168363c91f13fd7e9ee42aa8e3e", "links": {"self": {"href": "https://api.bitbucket.org/2.0/repositories/alihankirimtay/auto_update_local/commit/59a75b0a478e1168363c91f13fd7e9ee42aa8e3e"}, "html": {"href": "https://bitbucket.org/alihankirimtay/auto_update_local/commits/59a75b0a478e1168363c91f13fd7e9ee42aa8e3e"}}, "author": {"raw": "Alihan K\u0131r\u0131mtay <alihan.kirimtay@kodar.com.tr>", "type": "author", "user": {"username": "alihankirimtay", "display_name": "Alihan K\u0131r\u0131mtay", "account_id": "557058:d42f52c3-0a6e-429a-8a0a-5f0836832dae", "links": {"self": {"href": "https://api.bitbucket.org/2.0/users/alihankirimtay"}, "html": {"href": "https://bitbucket.org/alihankirimtay/"}, "avatar": {"href": "https://bitbucket.org/account/alihankirimtay/avatar/"}}, "type": "user", "nickname": "alihankirimtay", "uuid": "{1f4e0061-7496-47f9-abcc-0f0a49f06bdb}"}}, "summary": {"raw": "\u011f\u011f", "markup": "markdown", "html": "<p>\u011f\u011f</p>", "type": "rendered"}, "parents": [{"type": "commit", "hash": "6afc6c429a2071687d2f67cf143604b09ef46c4d", "links": {"self": {"href": "https://api.bitbucket.org/2.0/repositories/alihankirimtay/auto_update_local/commit/6afc6c429a2071687d2f67cf143604b09ef46c4d"}, "html": {"href": "https://bitbucket.org/alihankirimtay/auto_update_local/commits/6afc6c429a2071687d2f67cf143604b09ef46c4d"}}}], "date": "2018-12-13T17:21:48+00:00", "message": "\u011f\u011f", "type": "commit"}, "links": {"commits": {"href": "https://api.bitbucket.org/2.0/repositories/alihankirimtay/auto_update_local/commits/master"}, "self": {"href": "https://api.bitbucket.org/2.0/repositories/alihankirimtay/auto_update_local/refs/branches/master"}, "html": {"href": "https://bitbucket.org/alihankirimtay/auto_update_local/branch/master"}}, "default_merge_strategy": "merge_commit", "merge_strategies": ["merge_commit", "squash", "fast_forward"], "type": "branch", "name": "master"}}]}, "repository": {"scm": "git", "website": "", "name": "auto_update_local", "links": {"self": {"href": "https://api.bitbucket.org/2.0/repositories/alihankirimtay/auto_update_local"}, "html": {"href": "https://bitbucket.org/alihankirimtay/auto_update_local"}, "avatar": {"href": "https://bytebucket.org/ravatar/%7Bc42c9d7c-3f2c-4bb2-9f3e-4a510a5ac564%7D?ts=php"}}, "full_name": "alihankirimtay/auto_update_local", "owner": {"username": "alihankirimtay", "display_name": "Alihan K\u0131r\u0131mtay", "account_id": "557058:d42f52c3-0a6e-429a-8a0a-5f0836832dae", "links": {"self": {"href": "https://api.bitbucket.org/2.0/users/alihankirimtay"}, "html": {"href": "https://bitbucket.org/alihankirimtay/"}, "avatar": {"href": "https://bitbucket.org/account/alihankirimtay/avatar/"}}, "type": "user", "nickname": "alihankirimtay", "uuid": "{1f4e0061-7496-47f9-abcc-0f0a49f06bdb}"}, "type": "repository", "is_private": false, "uuid": "{c42c9d7c-3f2c-4bb2-9f3e-4a510a5ac564}"}, "actor": {"username": "alihankirimtay", "display_name": "Alihan K\u0131r\u0131mtay", "account_id": "557058:d42f52c3-0a6e-429a-8a0a-5f0836832dae", "links": {"self": {"href": "https://api.bitbucket.org/2.0/users/alihankirimtay"}, "html": {"href": "https://bitbucket.org/alihankirimtay/"}, "avatar": {"href": "https://bitbucket.org/account/alihankirimtay/avatar/"}}, "type": "user", "nickname": "alihankirimtay", "uuid": "{1f4e0061-7496-47f9-abcc-0f0a49f06bdb}"}}');

          if (!isset($inputs->push->changes[0]->new->name)) {
            throw new Exception('Invalid request: ' . file_get_contents('php://input'));
          }

          $settings = $this->setAuthSettings();

          $branch = $inputs->push->changes[0]->new->name;
          if (!in_array($branch, $settings['branches'])) {
            return;
          }

          $response = $this->apiAuthantication($settings);

          $last_commit = json_decode($response['commit']->all($settings['account'], $settings['repo_slug'], array(
            'branch' => $branch,
            'pagelen' => 1, // this can also be a tag
          ))->getContent());

          if ($last_commit) {
            $last_commit = $last_commit->values[0]->hash;

            $last_update = $this->getLastUpdate($branch);
            if (!$last_update) {
              throw new Exception('Last update commit number is required.');
            }

            $commit_exists = Registry::get("Database")->first('SELECT * FROM '.self::mTable." WHERE commit = '{$last_commit}' and branch_name = '{$branch}'");
            if ($commit_exists) {
              throw new Exception("The '{$last_commit}' already exist!");
            }
            
            if ($last_update !== $last_commit) {

              $diffstats = json_decode($response['commit']->getDiffStat($settings['account'], $settings['repo_slug'], array(
                'user_commit' => $last_update,
                'last_commit' => $last_commit,
              ))->getContent());

              $filename = 'modules/selfupdate/archives/' . $last_commit . '.zip';

              $this->createZip($diffstats, $filename);

              $this->insertArchive($last_commit, $filename, $branch);

              exit('Success.');
            }
          }
        } catch (Exception $e) {
          trigger_error($e->getMessage()); 
        }
      }

      public function insertArchive($commit, $path, $branch_name)
      {
        Registry::get("Database")->insert(self::mTable, [
          'commit' => $commit,
          'path' => $path,
          'branch_name' => $branch_name,
          'created' => date("Y-m-d H:i:s")
        ]);
      }

      public function createZip($diffstats, $filename)
      {
        $zip = new ZipArchive();
        $result = $zip->open(BASEPATH . $filename, ZIPARCHIVE::CREATE);

        if ($result !== true) {
          throw new Exception('Archive couldn\'t created: '.$result);
        }


        $changes = [
          'modified' => [],
          'deleted' =>  [],
        ];

        foreach ($diffstats->values as $key => $diffstat) {

          if ($diffstat->status === 'added' || $diffstat->status === 'modified') {

            $path_parts = pathinfo($diffstat->new->path);
            $filepath = $path_parts['dirname'];
            $fileName = $path_parts['basename'];
            $changes['modified'][] = $diffstat->new->path;

            if ($filepath == '.') {
              $zip->addFromString($fileName, file_get_contents($diffstat->new->links->self->href));
            } else {
              $zip->addEmptyDir($filepath);
              $zip->addFromString($filepath.'/'.$fileName, file_get_contents($diffstat->new->links->self->href));        
            }

          } else if ($diffstat->status === 'removed') {
            $changes['deleted'][] = $diffstat->old->path;
          }
          
          // TO-DO
          // PAGINATION  (Şuan sayfalama yapılmıyor, ön tanımlı olarak 500 satır geliyor.
          // Eğer gerek duyulursa eklenebilir)
        }

        $zip->addFromString('info.json', json_encode($changes));
        $zip->close();

      }

      public function apiAuthantication(array $params = array())
      {

        if ($params['oauth'] == "1") {
          $data['user'] = new Bitbucket\API\User();
          $data['user']->getClient()->addListener(
            new Bitbucket\API\Http\Listener\OAuthListener($params['oauth_params'])
          );

          $data['commit'] = new \Bitbucket\API\Repositories\Commits();
          $data['commit']->getClient()->addListener(
            new Bitbucket\API\Http\Listener\OAuthListener($params['oauth_params'])
          );

          $data['repo'] = new \Bitbucket\API\Repositories\Repository();
          $data['repo']->getClient()->addListener(
            new Bitbucket\API\Http\Listener\OAuthListener($params['oauth_params'])
          );

        }
        else{
          $data['user'] = new Bitbucket\API\User();
          $data['user']->getClient()->addListener(
            new Bitbucket\API\Http\Listener\BasicAuthListener($params['account'],$params['account_pass'])
          );

          $data['commit'] = new \Bitbucket\API\Repositories\Commits();
          $data['commit']->getClient()->addListener(
            new Bitbucket\API\Http\Listener\BasicAuthListener($params['account'],$params['account_pass'])
          );

          $data['repository'] = new \Bitbucket\API\Repositories\Repository();
          $data['repository']->getClient()->addListener(
            new Bitbucket\API\Http\Listener\BasicAuthListener($params['account'],$params['account_pass'])
          );
        } 

        return $data;
      }

      public function triggerAllUsers()
      {

        $users = Registry::get("Database")->fetch_all("
          SELECT mw.folder as destination, usr.database_name as DB_NAME 
          FROM ".self::wTable." mw 
          INNER JOIN ".self::uTable." usr ON(mw.id = usr.mod_website_id) 
          WHERE mw.status = 1 AND usr.active = 'y' AND  mw.deleted is null AND usr.mem_expire > NOW()
        ");
        
        foreach ($users as $user) {
          
          $url = 'http://localhost/posmakscmsgit/'.$user->destination.'/api/app/trigger';
          $curl = curl_init();                
          curl_setopt($curl, CURLOPT_URL, $url);
          curl_setopt($curl, CURLOPT_POST, TRUE);
          curl_setopt($curl, CURLOPT_USERAGENT, 'api');
          curl_setopt($curl, CURLOPT_TIMEOUT, 1); 
          curl_setopt($curl, CURLOPT_HEADER, 0);
          curl_setopt($curl, CURLOPT_RETURNTRANSFER, false);
          curl_setopt($curl, CURLOPT_FORBID_REUSE, true);
          curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 1);
          curl_setopt($curl, CURLOPT_DNS_CACHE_TIMEOUT, 10); 
          curl_setopt($curl, CURLOPT_FRESH_CONNECT, true);
          curl_exec($curl);
          curl_close($curl);
        }
        

      }
  }
<?php
  /**
   * Controller
   *
   * @package VeriAsist
   * @author veriasist.com
   * @copyright 2014
   * @version $Id: controller.php, v4.00 2014-12-24 12:12:12 gewa Exp $
   */
  define("_VALID_PHP", true);

  require_once ("../../init.php");
  if (!$user->is_Admin())
      redirect_to("../../login.php");

  require_once("admin_class.php");
  Registry::set('SelfUpdate', new SelfUpdate());

   
?>

<?php
//Change and add bitbucket
   if (isset($_POST['processSelfUpdate'])):
      Registry::get("SelfUpdate")->processSelfUpdate($_POST);
  endif;

  //delete archives
  if (isset($_POST['delete'])) {
  
    $result = Registry::get("SelfUpdate")->deleteRecord($_POST);
    if ($result == true):
          $json['type'] = 'success';
          $json['title'] = Lang::$word->_SUCCESS;
          $json['message'] = Lang::$word->_DELETED;
          Security::writeLog(Lang::$word->_DELETED, "", "no", "module");
      else:
          $json['type'] = 'warning';
          $json['title'] = Lang::$word->_ALERT;
          $json['message'] = Lang::$word->_SYSTEM_PROCCESS;
      endif;
      print json_encode($json);

  }
  if (isset($_POST['updateAll'])) {
    Registry::get("SelfUpdate")->triggerAllUsers();

  }
  
?>
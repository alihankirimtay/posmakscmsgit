<?php
  /**
   * Digishop Class
   *
   * @package Veri Asist
   * @author veriasist.com
   * @copyright 2014
   * @version $Id: class_admin.php, v4.00 2014-10-01 16:10:25 gewa Exp $
   */
  
  if (!defined("_VALID_PHP"))
      die('Direct access to this location is not allowed.');
  
  class Digishop
  {
	  
	  const mTable = "mod_digishop";
	  const ctTable = "mod_digishop_categories";
	  const trTable = "mod_digishop_transactions";
	  const xTable = "mod_digishop_cart";
	  const salt   = '_Sa1t*My=_Key_';

	  const imagepath = "modules/digishop/dataimages/";
	  
	  private static $db;


      /**
       * Digishop::__construct()
       * 
       * @return
       */
      function __construct($item = false, $cat = false)
      {
		  self::$db = Registry::get("Database");
		  $this->getConfig();
          ($item) ? $this->renderSingleProduct() : null;
          ($cat) ? $this->renderSingleCategory() : null;
      }

	  	  
	  /**
	   * Digishop::getConfig()
	   * 
	   * @return
	   */
	  private function getConfig()
	  {

          $row = INI::read(MODPATH . 'digishop/config.ini');

          $this->filedir = base64_decode($row->ds_config->filedir);
          $this->allow_free = $row->ds_config->allow_free;
          $this->cols = $row->ds_config->cols;
		  $this->ipp = $row->ds_config->ipp;
		  $this->fpp = $row->ds_config->fpp;
		  $this->like = $row->ds_config->like;
		  $this->layout = $row->ds_config->layout;
		  $this->template = base64_decode($row->ds_config->template);

          return ($row) ? $row : 0;
		  

	  }

	  /**
	   * Digishop::processConfig()
	   * 
	   * @return
	   */
	  public function processConfig()
	  {
	
		  Filter::checkPost('filedir', Lang::$word->_MOD_DS_FILEDIR);
	
		  if (empty(Filter::$msgs)) {
			  $data = array('ds_config' => array(
					  'cols' => intval($_POST['cols']),
					  'ipp' => intval($_POST['ipp']),
					  'fpp' => intval($_POST['fpp']),
					  'like' => intval($_POST['like']),
					  'filedir' => '"' . base64_encode($_POST['filedir']) . '"',
					  'allow_free' => intval($_POST['allow_free']),
					  'layout' => intval($_POST['layout']),
					  'template' => '"' . base64_encode($_POST['template']) . '"')
					  );
	
			  if (INI::write(MODPATH . 'digishop/config.ini', $data)) {
				  $json['type'] = 'success';
				  $json['message'] = Filter::msgOk(Lang::$word->_MOD_DS_CGFUPDATED, false);
				  Security::writeLog(Lang::$word->_MOD_DS_CGFUPDATED, "", "no", "module");
			  } else {
				  $json['type'] = 'info';
				  $json['message'] = Filter::msgAlert(Lang::$word->_PROCCESS_C_ERR . '{sistemyonetimi/modules/digishop/config.ini}', false);
			  }
			  print json_encode($json);
	
		  } else {
			  $json['message'] = Filter::msgStatus();
			  print json_encode($json);
		  }
	  }
	  
	  /**
	   * Digishop::getProducts()
	   * 
	   * @return
	   */
	  public function getProducts()
	  {
	
		  if (Filter::$id) {
			  $where = "WHERE p.cid = " . Filter::$id;
			  $counter = countEntries(self::mTable, 'cid', Filter::$id);
		  } else {
			  $where = null;
			  $counter = countEntries(self::mTable);
		  }

		  if (isset($_POST['fromdate_submit']) && $_POST['fromdate_submit'] <> "" || isset($from) && $from != '') {
			  $enddate = date("Y-m-d");
			  $fromdate = (empty($from)) ? $_POST['fromdate_submit'] : $from;
			  if (isset($_POST['enddate_submit']) && $_POST['enddate_submit'] <> "") {
				  $enddate = $_POST['enddate_submit'];
			  }
			  $where = " WHERE p.created BETWEEN '" . trim($fromdate) . "' AND '" . trim($enddate) . " 23:59:59'";
	
			  $q = "SELECT COUNT(*) FROM " . self::mTable . " WHERE created BETWEEN '" . trim($fromdate) . "' AND '" . trim($enddate) . " 23:59:59' LIMIT 1";
			  $record = Registry::get("Database")->query($q);
			  $total = Registry::get("Database")->fetchrow($record);
			  $counter = $total[0];
		  }
		  
		  $pager = Paginator::instance();
		  $pager->items_total = $counter;
		  $pager->default_ipp = Registry::get("Core")->perpage;
		  $pager->paginate();
	
		  $sql = "SELECT p.*, c.id as cid, c.name" . Lang::$lang . " as catname," 
		  . "\n (SELECT COUNT(pid) FROM " . self::trTable . " WHERE pid = p.id) as sales" 
		  . "\n FROM " . self::mTable . " as p" 
		  . "\n LEFT JOIN " . self::ctTable . " as c ON c.id = p.cid" 
		  . "\n $where" 
		  . "\n ORDER BY p.created DESC" . $pager->limit;
		  $row = self::$db->fetch_all($sql);
	
		  return ($row) ? $row : 0;
	  }

	  /**
	   * Digishop::processProduct()
	   * 
	   * @return
	   */
	  public function processProduct()
	  {
	
		  Filter::checkPost('title' . Lang::$lang, Lang::$word->_MOD_DS_NAME);
		  Filter::checkPost('cid', Lang::$word->_MOD_DS_CATEGORY);
		  Filter::checkPost('price', Lang::$word->_MOD_DS_PRODPRICE);

          if (!Filter::$id) {
              if (empty($_FILES['thumb']['name']))
                  Filter::$msgs['thumb'] = Lang::$word->_MOD_DS_PRODIMG;

              if (empty($_FILES['thumbbg']['name']))
                  Filter::$msgs['thumbbg'] = Lang::$word->_MOD_DS_PRODIMG . ' (Arkaplan)';

              if (empty($_FILES['filename']['name']))
                  Filter::$msgs['filename'] = Lang::$word->_MOD_DS_PRODFILE;
          }
		  
		  if (!empty($_FILES['filename']['name'])) {
			  if (!preg_match("/(\.zip|\.rar|\.pdf)$/i", $_FILES['filename']['name']))
				  Filter::$msgs['filename'] = Lang::$word->_MOD_DS_PRODFILE_R;
		  }
	
		  if (!empty($_FILES['thumb']['name'])) {
			  if (!preg_match("/(\.jpg|\.png)$/i", $_FILES['thumb']['name'])) {
				  Filter::$msgs['thumb'] = Lang::$word->_CG_LOGO_R;
			  }
			  $file_info = getimagesize($_FILES['thumb']['tmp_name']);
			  if (empty($file_info))
				  Filter::$msgs['thumb'] = Lang::$word->_CG_LOGO_R;
		  }

		  if (!empty($_FILES['thumbbg']['name'])) {
			  if (!preg_match("/(\.jpg|\.png)$/i", $_FILES['thumbbg']['name'])) {
				  Filter::$msgs['thumbbg'] = Lang::$word->_CG_LOGO_R . ' (Arkaplan)';
			  }
			  $file_info = getimagesize($_FILES['thumbbg']['tmp_name']);
			  if (empty($file_info))
				  Filter::$msgs['thumbbg'] = Lang::$word->_CG_LOGO_R . ' (Arkaplan)';
		  }
	
		  if (empty(Filter::$msgs)) {
			  $data = array(
				  'title' . Lang::$lang => sanitize($_POST['title' . Lang::$lang]),
				  'slug' => (empty($_POST['slug'])) ? doSeo($_POST['title' . Lang::$lang]) : doSeo($_POST['slug']),
				  'cid' => intval($_POST['cid']),
				  'price' => floatval($_POST['price']),
				  'token' => md5(time()),
				  'active' => intval($_POST['active']),
				  'gallery' => intval($_POST['module_data']),
				  'metakey' . Lang::$lang => sanitize($_POST['metakey' . Lang::$lang]),
				  'metadesc' . Lang::$lang => sanitize($_POST['metadesc' . Lang::$lang]),
				  'body' . Lang::$lang => Filter::in_url($_POST['body' . Lang::$lang]),
				  'active' => 1
				  );
	
			  if (empty($_POST['metakey' . Lang::$lang]) or empty($_POST['metadesc' . Lang::$lang])) {
				  include (BASEPATH . 'lib/class_meta.php');
				  parseMeta::instance($_POST['body' . Lang::$lang]);
				  if (empty($_POST['metakey' . Lang::$lang])) {
					  $data['metakey' . Lang::$lang] = parseMeta::get_keywords();
				  }
				  if (empty($_POST['metadesc'])) {
					  $data['metadesc' . Lang::$lang] = parseMeta::metaText($_POST['body' . Lang::$lang]);
				  }
			  }
	
			  if (!Filter::$id) {
				  $data['created'] = "NOW()";
			  }
	
			  if (isset($_POST['membership_id'])) {
				  $data['membership_id'] = Core::_implodeFields($_POST['membership_id']);
			  } else
				  $data['membership_id'] = 0;
	
	
			  // Procces Image
			  if (!empty($_FILES['thumb']['name'])) {
				  $filedir = BASEPATH . self::imagepath;
				  $newName = "IMG_" . randName();
				  $ext = substr($_FILES['thumb']['name'], strrpos($_FILES['thumb']['name'], '.') + 1);
				  $fullname = $filedir . $newName . "." . strtolower($ext);
	
				  if (Filter::$id and $file = getValueById("thumb", self::mTable, Filter::$id)) {
					  @unlink($filedir . $file);
				  }
	
				  if (!move_uploaded_file($_FILES['thumb']['tmp_name'], $fullname)) {
					  die(Filter::msgError(Lang::$word->_FILE_ERR, false));
				  }
				  $data['thumb'] = $newName . "." . strtolower($ext);
			  }

			  if (!empty($_FILES['thumbbg']['name'])) {
				  $filedir = BASEPATH . self::imagepath;
				  $newName = "IMG_" . randName();
				  $ext = substr($_FILES['thumbbg']['name'], strrpos($_FILES['thumbbg']['name'], '.') + 1);
				  $fullname = $filedir . $newName . "." . strtolower($ext);
	
				  if (Filter::$id and $file = getValueById("thumbbg", self::mTable, Filter::$id)) {
					  @unlink($filedir . $file);
				  }
	
				  if (!move_uploaded_file($_FILES['thumbbg']['tmp_name'], $fullname)) {
					  die(Filter::msgError(Lang::$word->_FILE_ERR, false));
				  }
				  $data['thumbbg'] = $newName . "." . strtolower($ext);
			  }
	
			  // Procces File
			  // if (!empty($_FILES['filename']['name'])) {
				 //  $filedir = $this->filedir;
				 //  $newName = "FILE_" . randName();
				 //  $ext = substr($_FILES['filename']['name'], strrpos($_FILES['filename']['name'], '.') + 1);
				 //  $fullname = $filedir . $newName . "." . strtolower($ext);
	
				 //  if (Filter::$id and $file = getValueById("filename", self::mTable, Filter::$id)) {
					//   @unlink($filedir . $file);
				 //  }
	
				 //  if (!move_uploaded_file($_FILES['filename']['tmp_name'], $fullname)) {
					//   die(Filter::msgError(Lang::$word->_FILE_ERR, false));
				 //  }
				 //  $data['filename'] = $newName . "." . strtolower($ext);
				 //  $data['filesize'] = filesize($fullname);
			  // }
	
			  (Filter::$id) ? self::$db->update(self::mTable, $data, "id=" . Filter::$id) : $lastid = self::$db->insert(self::mTable, $data);
			  $message = (Filter::$id) ? Lang::$word->_MOD_DS_PRODUPDATED : Lang::$word->_MOD_DS_PRODADDED;
	
			  if (self::$db->affected()) {
				  Security::writeLog($message, "", "no", "module");
				  $json['type'] = 'success';
				  $json['message'] = Filter::msgOk($message, false);
			  } else {
				  $json['type'] = 'success';
				  $json['message'] = Filter::msgAlert(Lang::$word->_SYSTEM_PROCCESS, false);
			  }
			  print json_encode($json);
	
	
		  } else {
			  $json['message'] = Filter::msgStatus();
			  print json_encode($json);
		  }
	  }

      /**
       * Digishop::renderSingleCategory()
       * 
       * @return
       */
      private function renderSingleCategory()
      {

          $sql = "SELECT * FROM " . self::ctTable . " WHERE slug = '" . Registry::get("Content")->_url[2] . "' AND active = 1";
          $row = self::$db->first($sql);

          return ($row) ? Registry::get("Content")->moduledata->mod = $row : 0;

      }
	  
      /**
       * Digishop::renderSingleProduct()
       * 
       * @return
       */
      public function renderSingleProduct()
      {

          $sql = "SELECT * FROM " . self::mTable . " WHERE slug = '" . Registry::get("Content")->_url[1] . "' AND active = 1";
          $row = self::$db->first($sql);

          return ($row) ? Registry::get("Content")->moduledata->mod = $row : 0;
      }
	  

	  /**
	   * Digishop::renderProducts()
	   * 
	   * @return
	   */
	  public function renderProducts()
	  {
	
		  $pager = Paginator::instance();
		  $pager->items_total = countEntries(self::mTable);
		  $pager->default_ipp = $this->fpp;
		  $pager->path = doUrl(false, false, "digishop", "?");
		  $pager->paginate();
	
		  $sql = "SELECT p.*, c.id as cid, c.name" . Lang::$lang . " as catname, p.title" . Lang::$lang . " as ptitle" 
		  . "\n FROM " . self::mTable . " as p" 
		  . "\n LEFT JOIN " . self::ctTable . " as c ON c.id = p.cid" 
		  . "\n WHERE p.active = 1" 
		  . "\n AND c.active = 1" 
		  . "\n ORDER BY p.created DESC" . $pager->limit;
		  $row = self::$db->fetch_all($sql);
	
		  return ($row) ? $row : 0;
	  }

      /**
       * Digishop::renderProductByCategory()
       * 
       * @param mixed $cid
       * @return
       */
	  public function renderProductByCategory($cid, $slug)
	  {
	
		  $pager = Paginator::instance();
		  $pager->items_total = countEntries(self::mTable, "cid", $cid);
		  $pager->default_ipp = $this->ipp;
		  $pager->path = doUrl(false, $slug, "digishop-cat", "?");
		  $pager->paginate();
	
		  $sql = "SELECT p.*, p.title" . Lang::$lang . " as ptitle" 
		  . "\n FROM " . self::mTable . " as p" 
		  . "\n LEFT JOIN " . self::ctTable . " as c ON c.id = p.cid" 
		  . "\n WHERE c.id = " . (int)$cid 
		  . "\n AND p.active = 1" 
		  . "\n AND c.active = 1" 
		  . "\n ORDER BY p.created DESC" . $pager->limit;
		  $row = self::$db->fetch_all($sql);
	
		  return ($row) ? $row : 0;
	  }

      /**
       * Digishop::getPriceMembershipData()
       * 
       * @param mixed $id
       * @param mixed $token
       * @param mixed $price
       * @param mixed $memid
       * @return
       */
	  public function getPriceMembershipData($id, $token, $price, $memid)
	  {

  		  $m_arr = explode(",", $memid);
		  reset($m_arr);
		  
		  if(Registry::get("Users")->logged_in and Registry::get("Users")->memvalid and in_array(Registry::get("Users")->membership_id, $m_arr)) {
			  print '<a href="' . SITEURL . '/modules/digishop/download.php?member=' . $token . '" 
			  class="veriasist warning labeled icon button" data-content="' . Lang::$word->_MOD_DS_MEMVALID . '"><i class="icon download disk"></i>' . Lang::$word->_MOD_DS_DOWNMEM . '</a>';
		  } else
			  print '<a data-id="' . $id . '" class="veriasist warning labeled icon button" data-content="' . Lang::$word->_MOD_DS_MEMREQ . '"><i class="icon user"></i>' . Lang::$word->_MOD_DS_MEMREQ . '</a>';
	  }


      /**
       * Digishop::getPriceData()
       * 
       * @param mixed $id
       * @param mixed $token
       * @param mixed $price
       * @return
       */
	  public function getPriceData($id, $token, $price)
	  {
		  
		  if($price == 0) {
			  switch ($this->allow_free) {
				  case "1":
				    print '<a href="' . SITEURL . '/modules/digishop/download.php?free=' . $token . '" 
					class="veriasist positive labeled icon button"><i class="icon unlock"></i>' . Lang::$word->_MOD_DS_DOWNFREE . '</a>';
				  break;

				  case "0" and Registry::get("Digishop")->logged_in :
				    print '<a href="' . SITEURL . '/modules/digishop/download.php?free=' . $token . '" 
					class="veriasist positive labeled icon button"><i class="icon unlock"></i>' . Lang::$word->_MOD_DS_DOWNFREE . '</a>';
				  break;
				  
				  default:
				  print Lang::$word->_MOD_DS_LOGIN_TO;
				  break;
			  }
		  } else
			  print '<a data-id="' . $id . '" class="add veriasist labeled icon button"><i class="icon cart"></i>' . Lang::$word->_MOD_DS_ADD_TO . '</a>';
	  }	

	  /**
	   * Digishop::layoutSwitchList()
	   * 
	   * @return
	   */
	  public function layoutSwitchList()
	  {
		  $active = '';
		  if (isset($_COOKIE['DIGIVIEW_VERIASIST']) && $_COOKIE['DIGIVIEW_VERIASIST'] == 'list') {
			  $active = ' active';
		  } elseif($this->layout == 0 && !isset($_COOKIE['DIGIVIEW_VERIASIST'])) {
			  $active = ' active';
		  }
		  
		  return $active;
	  }	

	  /**
	   * Digishop::layoutSwitchGrid()
	   * 
	   * @return
	   */
	  public function layoutSwitchGrid()
	  {
		  $active = '';
		  if (isset($_COOKIE['DIGIVIEW_VERIASIST']) && $_COOKIE['DIGIVIEW_VERIASIST'] == 'grid') {
			  $active = ' active';
		  } elseif($this->layout == 1 && !isset($_COOKIE['DIGIVIEW_VERIASIST'])) {
			  $active = ' active';
		  }
		  
		  return $active;
	  }	
	  	  	  	  	  	  
	  /**
	   * Digishop::getCategories()
	   * 
	   * @return
	   */
	  public function getCategories()
	  {
		  
		  $sql = "SELECT * FROM " . self::ctTable
		  . "\n ORDER BY sorting";
          $row = self::$db->fetch_all($sql);
          
		  return ($row) ? $row : 0;
	  }	

	  /**
	   * Digishop::processCategory()
	   * 
	   * @return
	   */
	  public function processCategory()
	  {
	
		  Filter::checkPost('name' . Lang::$lang, Lang::$word->_MOD_DS_CATNAME);
	
		  if (empty(Filter::$msgs)) {
			  $data = array(
				  'name' . Lang::$lang => sanitize($_POST['name' . Lang::$lang]),
				  'slug' => (empty($_POST['slug'])) ? doSeo($_POST['name' . Lang::$lang]) : doSeo($_POST['slug']),
				  'metakey' . Lang::$lang => sanitize($_POST['metakey' . Lang::$lang]),
				  'metadesc' . Lang::$lang => sanitize($_POST['metadesc' . Lang::$lang]),
				  'active' => 1
				  );
	
			  (Filter::$id) ? self::$db->update(self::ctTable, $data, "id=" . Filter::$id) : self::$db->insert(self::ctTable, $data);
			  $message = (Filter::$id) ? Lang::$word->_MOD_DS_CATUPDATED : Lang::$word->_MOD_DS_CATADDED;
	
			  if (self::$db->affected()) {
				  Security::writeLog($message, "", "no", "module");
				  $json['type'] = 'success';
				  $json['message'] = Filter::msgOk($message, false);
			  } else {
				  $json['type'] = 'success';
				  $json['message'] = Filter::msgAlert(Lang::$word->_SYSTEM_PROCCESS, false);
			  }
			  print json_encode($json);
	
		  } else {
			  $json['message'] = Filter::msgStatus();
			  print json_encode($json);
		  }
	  }
	  
	  /**
	   * Digishop::getCategoryList()
	   * 
	   * @return
	   */
	  public function getCategoryList($active = false)
	  {
	      $where = ($active) ? "WHERE active = 1" : null;
		  $sql = "SELECT *, " 
		  . "\n (SELECT COUNT(" . self::mTable . ".cid) FROM " . self::mTable . " WHERE " . self::mTable . ".cid = " . self::ctTable . ".id) as total"
		  . "\n FROM " . self::ctTable 
		  . "\n $where"
		  . "\n ORDER BY sorting";
		  $row = self::$db->fetch_all($sql);
	
		  return ($row) ? $row : 0;
	  }


      /**
       * Digishop::updateOrder()
       * 
       * @return
       */
      public static function updateOrder()
      {
          foreach ($_POST['node'] as $k => $v) {
              $p = $k + 1;
              $data['sorting'] = intval($p);
              self::$db->update(self::ctTable, $data, "id=" . (int)$v);
          }

      }

      /**
       * Digishop::getPayments()
       * 
       * @param bool $from
       * @return
       */
	  public function getPayments($from = false)
	  {
	
		  if (isset($_POST['fromdate_submit']) && $_POST['fromdate_submit'] <> "" || isset($from) && $from != '') {
			  $enddate = date("Y-m-d");
			  $fromdate = (empty($from)) ? $_POST['fromdate_submit'] : $from;
			  if (isset($_POST['enddate_submit']) && $_POST['enddate_submit'] <> "") {
				  $enddate = $_POST['enddate_submit'];
			  }
			  $q = "SELECT COUNT(*) FROM " . self::trTable . " WHERE created BETWEEN '" . trim($fromdate) . "' AND '" . trim($enddate) . " 23:59:59'";
			  $where = " WHERE t.created BETWEEN '" . trim($fromdate) . "' AND '" . trim($enddate) . " 23:59:59'";
		  } else {
			  $q = "SELECT COUNT(*) FROM " . self::trTable . " LIMIT 1";
			  $where = null;
		  }
	
		  $record = self::$db->query($q);
		  $total = self::$db->fetchrow($record);
		  $counter = $total[0];
	
		  $pager = Paginator::instance();
		  $pager->items_total = $counter;
		  $pager->default_ipp = Registry::get("Core")->perpage;
		  $pager->paginate();
	
		  $sql = "SELECT t.*, t.id as id, u.id as uid, u.username, p.id as did, p.title" . Lang::$lang 
		  . "\n FROM " . self::trTable . " as t" 
		  . "\n LEFT JOIN " . Users::uTable . " as u ON u.id = t.uid" 
		  . "\n LEFT JOIN " . self::mTable . " as p ON p.id = t.pid" 
		  . "\n " . $where 
		  . "\n ORDER BY t.created DESC" . $pager->limit;
	
		  $row = self::$db->fetch_all($sql);
	
		  return ($row) ? $row : 0;
	  }

	  /**
	   * Digishop::activateTransaction()
	   * 
	   * @return
	   */
	  public function activateTransaction()
	  {
	
		  $row = self::$db->first("SELECT t.*, t.id as id, p.title" . Lang::$lang . " as atitle, u.email, CONCAT(u.fname,' ',u.lname) as name" 
		  . "\n FROM " . self::trTable . " as t" 
		  . "\n LEFT JOIN " . Users::uTable . " as u ON u.id = t.uid" 
		  . "\n LEFT JOIN " . self::mTable . " as p ON p.id = t.pid" 
		  . "\n WHERE t.id = " . Filter::$id);
	
		  if ($row) {
			  require_once (BASEPATH . "lib/class_mailer.php");
			  $mailer = Mailer::sendMail();
	
			  $items = '
				<table width="100%" border="0" cellpadding="4" cellspacing="2">
				  <thead>
					<tr>
					  <td width="20"><strong>#</strong></td>
					  <td><strong>' . Lang::$word->_MOD_DS_NAME . '</strong></td>
					  <td><strong>' . Lang::$word->_MOD_DS_PRODPRICE . '</strong></td>
					  <td><strong>' . Lang::$word->_MOD_DS_QTY . '</strong></td>
					  <td><strong>' . Lang::$word->_MOD_DS_TOTPRICE . '</strong></td>
					</tr>
				  </thead>
				  <tbody>
					  <tr>
						<td style="border-bottom-width:1px; border-bottom-color:#bbb; border-bottom-style:dashed">' . $row->id . '.</td>
						<td style="border-bottom-width:1px; border-bottom-color:#bbb; border-bottom-style:dashed">' . sanitize($row->atitle, 30) . '</td>
						<td style="border-bottom-width:1px; border-bottom-color:#bbb; border-bottom-style:dashed">' . Registry::get("Core")->formatMoney($row->price) . '</td>
						<td align="center" style="border-bottom-width:1px; border-bottom-color:#bbb; border-bottom-style:dashed">' . $row->item_qty . '</td>
						<td align="right" style="border-bottom-width:1px; border-bottom-color:#bbb; border-bottom-style:dashed">' . Registry::get("Core")->formatMoney($row->item_qty * $row->price) . '</td>
					  </tr>
				  <tr>
					<td colspan="4" align="right" valign="top"><strong style="color:#F00">' . Lang::$word->_MOD_DS_GTOTAL . ':</strong></td>
					<td align="right" valign="top"><strong style="color:#F00">' . Registry::get("Core")->formatMoney($row->item_qty * $row->price) . '</strong></td>
				  </tr>
					</tbody>
				</table>';
			  $body = str_replace(array(
				  '[NAME]',
				  '[ITEMS]',
				  '[DASH]',
				  '[SITE_NAME]',
				  '[URL]'), array(
				  $row->name,
				  $items,
				  doUrl(false, Registry::get("Core")->account_page, "page"),
				  Registry::get("Core")->site_name,
				  SITEURL), $this->template);
	
			  $message = Swift_Message::newInstance()
						->setSubject(Lang::$word->_MOD_DS_R_SUBJECT . Registry::get("Core")->site_name)
						->setTo(array($row->email => $row->name))
						->setFrom(array(Registry::get("Core")->site_email => Registry::get("Core")->site_name))
						->setBody(cleanOut($body), 'text/html');
	
			  if ($mailer->send($message)):

			  	 $trow = Core::getRow(self::trTable, "id", Filter::$id);
				  $data = array(
					  'status' => 1,
					  'active' => 1,
					  );
				  self::$db->update(self::trTable, $data, "id = " . Filter::$id);

				  if($trow->status === 0)
				  	self::$db->insert('mod_digishop_transaction_statuses', ['transaction_id'=>$trow->transaction_id, 'created'=>'NOW()', 'status'=>1]);

				  $json['type'] = 'success';
				  $json['title'] = Lang::$word->_SUCCESS;
				  $json['message'] = Lang::$word->_MOD_DS_RSENT;
	
			  else:
				  $json['type'] = 'error';
				  $json['title'] = Lang::$word->_ERROR;
				  $json['message'] = Lang::$word->_MOD_DS_RSENT_ERR;
			  endif;
				  print json_encode($json);
	
		   }
	  }

	  public function getUserAddresses($id = false)
	  {
	  	if($id)
	  	{
	  		$sql = 'SELECT * FROM mod_invoices_users WHERE user_id='.Registry::get("Users")->uid.' AND id='.intval($id).' LIMIT 1';
		  	$row = self::$db->first($sql);

		  	$data = array('address', 'city', 'zip_post', 'state_province', 'country');
		  	if($row)
		  	{
		  		$data['name'] 	 = $row->name;
		  		$data['address'] = $row->_address;
		  		$data['country'] = $row->_country;
		  		$data['state_province'] = $row->_state_province;
		  		$data['zip_post']     = $row->_zip_post;
				
				 
				 $cities = $this->getCities($data['country']);
		 		 
				 foreach ($cities as $key => $value) {
					if($value->city_id == $row->_city){
					 $data['city'] = '<option selected="selected" value="'.$value->city_id.'">'.$value->name.'</option>';
					}
				 }
		  	}
			
		  	echo json_encode($data);
	  	}
	  	else
	  	{
	  		$sql = 'SELECT id, name FROM mod_invoices_users WHERE user_id='.Registry::get("Users")->uid;

		  	$row = self::$db->fetch_all($sql);

		  	return ($row) ? $row : 0;
	  	}
	  }

	  public function getContries($id = false)
	  {
	  		$sql = 'SELECT country_id, name FROM _countries WHERE '.($id ? 'country_id='.$id : 1).' ORDER BY name ASC';

		  	$row = self::$db->fetch_all($sql);

		  	return ($row) ? $row : 0;
	  }
	  
	  public function getCities($id = false){
	  		$sql = 'SELECT city_id, name FROM _cities WHERE country_id='.intval($id).' ORDER BY name ASC';

			$row = self::$db->fetch_all($sql);

		  	return ($row) ? $row : 0;
	  }

	  public function paymentAddresses()
	  {
	  	if(isset($_POST['invAdress']) && $_POST['invAdress'] == 0)
	  	{
	  		// 1. Adres Kontrolü
	  		if(!isset($_POST['addressName'], /*$_POST['city'],*/ $_POST['country'], $_POST['state_province'], $_POST['zip_post'], $_POST['address'])) exit('1111_');

	  		if(!isset($_POST['sameaddress']) && $_POST['invAdress1'] == 0)
		  		
				// 2. Adres Kontrolü
		  		if(!isset($_POST['addressName1'], /*$_POST['city1'],*/ $_POST['country1'], $_POST['state_province1'], $_POST['zip_post1'], $_POST['address1'])) return false;
			
			// 1. Adres Bilgileri
	  		$data['name'] 				= sanitize($_POST['addressName']);
	  		$data['_city'] 				= sanitize(@$_POST['city']);
	  		$data['_state_province'] 	= sanitize($_POST['state_province']);
	  		$data['_zip_post'] 			= sanitize($_POST['zip_post']);
	  		$data['_address'] 			= sanitize($_POST['address']);
	  		$data['_country'] 			= intval($_POST['country']);
	  		
	  		if(!isset($_POST['sameaddress']) && $_POST['invAdress1'] == 0):
		  		// 2. Adres Bilgileri
		  		$data1['name']				= sanitize($_POST['addressName1']);
				$data1['_city'] 			= sanitize(@$_POST['city1']);
		  		$data1['_state_province'] 	= sanitize($_POST['state_province1']);
		  		$data1['_zip_post'] 		= sanitize($_POST['zip_post1']);
		  		$data1['_address'] 			= sanitize($_POST['address1']);
		  		$data1['_country'] 			= intval($_POST['country1']);
	  			
	  		endif;

			
	  		if(!isValidString($data['name'])) 				Filter::$msgs['addressName'] = Lang::$word->_MOD_ADR_NAME_ERR;
	  		if(!isValidString($data['_city'])) 				Filter::$msgs['city'] = Lang::$word->_MOD_CTY_ERR;
	  		if(!isValidString($data['_state_province'])) 	Filter::$msgs['state_province'] = Lang::$word->_MOD_STATE_PROV_ERR;
	  		if(!isValidString($data['_address'],1,300)) 	Filter::$msgs['address'] = Lang::$word->_MOD_ADDRESS_ERR;
	  		if($data['_country'] < 1) 						Filter::$msgs['country'] = Lang::$word->_MOD_SEL_CNTRY_ERR;
	  		elseif(!$this->getContries($data['_country']))  Filter::$msgs['country'] = Lang::$word->_MOD_SEL_CNTRY_INV_ERR;

	  		if(!isset($_POST['sameaddress']) && $_POST['invAdress1'] == 0):
	  			if(!isValidString($data1['name'])) 				Filter::$msgs['addressName1'] = Lang::$word->_MOD_CTY_ERR_2_BILLING;
		  		if(!isValidString($data1['_city'])) 			Filter::$msgs['city1'] = Lang::$word->_MOD_CTY_ERR_2_BILLING;
		  		if(!isValidString($data1['_state_province'])) 	Filter::$msgs['state_province1'] = Lang::$word->_MOD_STATE_PROV_ERR_2_BILLING;
		  		if(!isValidString($data1['_address'],1,300)) 	Filter::$msgs['address1'] = Lang::$word->_MOD_ADDRESS_ERR_2_BILLING;
		  		if($data1['_country'] < 1) 						Filter::$msgs['country1'] = Lang::$word->_MOD_SEL_CNTRY_ERR_2_BILLING;
		  		elseif(!$this->getContries($data1['_country'])) Filter::$msgs['country1'] = Lang::$word->_MOD_SEL_CNTRY_INV_ERR_2_BILLING;
			endif;

	  		if(Filter::$msgs) {
	  			$json['type'] = 'error';
	  			$json['title'] = Lang::$word->_ERROR;
	  			$json['message'] = Filter::msgStatus();
				print json_encode($json); exit;
	  		}

	  		$DateName = date('Y-m-d H:i:s');
  			$insertData = $data + array('_active'=>1, 'user_id'=>Registry::get("Users")->uid, '_created'=>'NOW()');
  			$id = self::$db->insert('mod_invoices_users', $insertData);

  			if(!isset($_POST['sameaddress']) && $_POST['invAdress1'] == 0):
	  			$insertData1 = $data1 + array('_active'=>1, 'user_id'=>Registry::get("Users")->uid, '_created'=>'NOW()');
	  			$id1 = self::$db->insert('mod_invoices_users', $insertData1);
	  			$updAddr['adr2'] = $id1;
			endif;

  			$message = 'Kullanıcı '. Registry::get("Users")->username . ' sipariş aşamasında yeni adres oluşturdu.';
  			Security::writeLog($message, "user", "no", "module");

  			$updAddr['adr1'] = $id;
	  		self::$db->update(self::xTable, $updAddr, "user_id='" . Registry::get("Users")->sesid . "'" );

  			$json['type'] = 'success';
			print json_encode($json);
	  	}
	  	elseif(isset($_POST['invAdress']) && $_POST['invAdress'] !== 0)
	  	{
	  		$invAdress = intval($_POST['invAdress']);

	  		if(!$row = self::$db->first('SELECT id FROM mod_invoices_users WHERE id='.$invAdress.' LIMIT 1') )
	  			Filter::$msgs['invAdress'] = Lang::$word->_MOD_ADDRESS_INV_ERR;

	  		if(!isset($_POST['sameaddress']) && $_POST['invAdress1'] == 0)
	  		{
	  		
		  		if(!isset($_POST['addressName1'], $_POST['city1'], $_POST['country1'], $_POST['state_province1'], $_POST['zip_post1'], $_POST['address1'])) return false;

				// 2. Adres Bilgileri
		  		$data1['name']				= sanitize($_POST['addressName1']);
		  		$data1['_city'] 			= sanitize($_POST['city1']);
		  		$data1['_state_province'] 	= sanitize($_POST['state_province1']);
		  		$data1['_zip_post'] 		= sanitize($_POST['zip_post1']);
		  		$data1['_address'] 			= sanitize($_POST['address1']);
		  		$data1['_country'] 			= intval($_POST['country1']);


	  			if(!isValidString($data1['name'])) 				Filter::$msgs['addressName1'] = Lang::$word->_MOD_CTY_ERR_2_BILLING;
		  		if(!isValidString($data1['_city'])) 			Filter::$msgs['city1'] = Lang::$word->_MOD_CTY_ERR_2_BILLING;
		  		if(!isValidString($data1['_state_province'])) 	Filter::$msgs['state_province1'] = Lang::$word->_MOD_STATE_PROV_ERR_2_BILLING;
		  		if(!isValidString($data1['_address'],1,300)) 	Filter::$msgs['address1'] = Lang::$word->_MOD_ADDRESS_ERR_2_BILLING;
		  		if($data1['_country'] < 1) 						Filter::$msgs['country1'] = Lang::$word->_MOD_SEL_CNTRY_ERR_2_BILLING;
		  		elseif(!$this->getContries($data1['_country'])) Filter::$msgs['country1'] = Lang::$word->_MOD_SEL_CNTRY_INV_ERR_2_BILLING;

		  		$insertData1 = $data1 + array('_active'=>1, 'user_id'=>Registry::get("Users")->uid, '_created'=>'NOW()');
		  		$id1 = self::$db->insert('mod_invoices_users', $insertData1);
		  		$updAddr['adr2'] = $id1;
	  		}
	  		elseif(!isset($_POST['sameaddress']) && $_POST['invAdress1'] !== 0)
	  		{
	  			$invAdress1 = intval($_POST['invAdress1']);
	  			if(!$row = self::$db->first('SELECT id FROM mod_invoices_users WHERE id='.$invAdress1.' LIMIT 1') )
	  				Filter::$msgs['invAdress'] = Lang::$word->_MOD_ADDRESS_INV_ERR_2_BILLING;

	  			$updAddr['adr2'] = $invAdress1;
	  		}


	  		if(Filter::$msgs) {
	  			$json['type'] = 'error';
	  			$json['title'] = Lang::$word->_ERROR;
	  			$json['message'] = Filter::msgStatus();
				print json_encode($json); exit;
	  		}


	  		$updAddr['adr1'] = $invAdress;
	  		self::$db->update(self::xTable, $updAddr, "user_id='" . Registry::get("Users")->sesid . "'" );

	  		$json['type'] = 'success';
			print json_encode($json);
	  	}

	  }

	  // public function encodeKey()
	  // {
	  // 	return sha1( self::salt . Registry::get("Users")->uid ) . '-' . sha1( self::salt . Registry::get("Users")->sesid );
	  // }

	  // public function decodeKey($key='')
	  // {
	  // 	if(empty($key)) return false;

	  // 	$key = explode('-', $key);
	  // 	if(!isset($key[0], $key[1])) return false;

	  // 	if(
	  // 		sha1( self::salt . Registry::get("Users")->uid ) == $key[0] &&
	  // 		sha1( self::salt . Registry::get("Users")->sesid ) == $key[1]
	  // 	  )
	  // 		return true;
	  // 	else
	  // 		return false;

	  // }

      /**
       * Digishop::getUserTransactions()
       * 
       * @param bool $sesid
       * @return
       */
	  public function getUserTransactions()
	  {
	
		  $sql = "SELECT t.*, p.id as pid, p.title" . Lang::$lang . " as ptitle, p.price, p.thumb, p.slug"
		  . "\n FROM " . self::trTable . " as t" 
		  . "\n LEFT JOIN " . self::mTable . " as p ON p.id = t.pid" 
		  . "\n WHERE t.uid = " . Registry::get("Users")->uid
		  . "\n AND t.status = 1"
		  . "\n AND t.active = 1"
		  . "\n AND p.active = 1"
		  . "\n GROUP BY t.pid ORDER BY t.created DESC";
		  
		  $row = self::$db->fetch_all($sql);
	
		  return ($row) ? $row : 0;
	  }

	  public function loadcart($param = 'cartsummary')
	  {
	  	$cartdata = Registry::get("Digishop")->getCartContent();
	  	$totalrow = Registry::get("Digishop")->getCartTotal();

	  	$html = '';
	  	if (!$cartdata):
	  		$html = '<tr><td colspan="5" align="center" class="alert alert-danger" >'.Lang::$word->_MOD_DS_NOCART.'</td></tr>';
	  	else:

	  		foreach ($cartdata as $xrow):
	  			$html .= 
	  	'<tr>
	  		<td class="col-sm-8 col-md-6">
	  			<div class="media">
	  				<a class="thumbnail pull-left" href="javascript:;"> <img class="media-object" src="'.SITEURL.'/thumbmaker.php?src='.SITEURL.'/'.Digishop::imagepath.$xrow->thumb.'&amp;h=50&amp;w=50&amp;s=1&amp;a=tl"> </a>
	  				<div class="media-body">
	  					<h4 class="media-heading"><a href="'.doUrl(false, $xrow->slug, "digishop-item").'"> '.$xrow->ptitle.'</a></h4>
	  				</div>
	  			</div>
	  		</td>

	  		<td class="col-sm-1 col-md-1">
	  			<div class="input-group">
	  				<span class="input-group-btn">
	  				<button type="button" class="spinner-control-button btn btn-default" data-target="-1" data-id="'.$xrow->pid.'">
	  						<span class="glyphicon glyphicon-minus"></span>
	  					</button>
	  				</span>

	  				<input type="text" id="cartSpinner'.$xrow->pid.'" class="spinner-control-input form-control input-number" value="'.$xrow->total.'" data-id="'.$xrow->pid.'" data-min="1" data-max="100" style="min-width:35px;padding:0 0 0 0;text-align:center;">

	  				<span class="input-group-btn">
	  				<button type="button" class="spinner-control-button btn btn-default" data-target="1" data-id="'.$xrow->pid.'">
	  						<span class="glyphicon glyphicon-plus"></span>
	  					</button>
	  				</span>
	  			</div>
	  		</td>


	  		<td class="col-sm-1 col-md-1 text-center"><strong>'.Registry::get("Core")->formatMoney($xrow->price).'</strong></td>
	  		<td class="col-sm-1 col-md-1 text-center"><strong>'.Registry::get("Core")->formatMoney($xrow->total * $xrow->price).'</strong></td>
	  		<td class="col-sm-1 col-md-1">
	  			<button type="button" class="btn btn-danger btn-sm removeitem" data-id="'.$xrow->pid.'">
	  				<i class="fa fa-times"></i> '.Lang::$word->_MOD_DS_REMOVE.'
	  			</button>
	  		</td>
	  	</tr>';
	  		endforeach;
	  	$html .=
	  	'<tr>
	  	<td>   </td>
	  	<td>   </td>
	  	<td>   </td>
	  	<td>
	  		<h5>'.Lang::$word->_MOD_DS_TOTPRICE.'</h5>
	  	</td>
	  	<td class="text-right"><h5>
	  		<strong>'.Registry::get("Core")->formatMoney($totalrow->total).'</strong></h5>
	  	</td>
	  </tr>
	  <tr>
	  	<td>   </td>
	  	<td>   </td>
	  	<td>   </td>
	  	<td>
	  		<h5>'.Lang::$word->_MOD_DS_SHPNG.'</h5>
	  	</td>
	  	<td class="text-right">
	  		<h5><strong>$0.00</strong></h5>
	  	</td>
	  </tr>
	  <tr>
	  	<td>   </td>
	  	<td>   </td>
	  	<td>   </td>
	  	<td><h3>'.Lang::$word->_MOD_DS_GTOTAL.'</h3></td>
	  	<td class="text-right">
	  		<h3><strong>'.Registry::get("Core")->formatMoney($totalrow->total).'</strong></h3>
	  	</td>
	  </tr>
	  <tr>
	  	<td>   </td>
	  	<td>   </td>
	  	<td>   </td>
	  	<td>   </td>
	  	<td  class="text-right">
	  		<a href="'.doUrl(false, false, 'digishop-checkout', '?'.$param).'" class="btn btn-success">
	  			<i class="fa fa-check"></i> '.Lang::$word->_MOD_DS_CHECKOUT.'
	  		</a>
	  	</td>
	  </tr>';
	  endif;

	  return $html;
	}



	public function loadcartsummary()
	{
		$cartdata = Registry::get("Digishop")->getCartContent();
	  	$totalrow = Registry::get("Digishop")->getCartTotal();
	  	$html = '';

	  	if($cartdata) {
	  		foreach ($cartdata as $xrow):
	  			$html .= '
	  		<tr>
	  			<td>'.$xrow->ptitle.'</td>
	  			<td>'.$xrow->total.'</td>
	  			<td>'.Registry::get("Core")->formatMoney($xrow->price).'</td>
	  			<td>'.Registry::get("Core")->formatMoney($xrow->total * $xrow->price).'</td>
	  		</tr>
	  		';
	  		endforeach;

	  		$html .= '
			<tr>
				<td></td>
				<td></td>
				<td><strong>'.Lang::$word->_MOD_DS_GTOTAL.'</strong></td>
				<td><strong>'.Registry::get("Core")->formatMoney($totalrow->total).'</strong></td>
			</tr>

	  		';
	  	}
	  		
	  	return $html;
	}
	  
      /**
       * Digishop::getCartContent()
       * 
       * @param bool $sesid
       * @return
       */
	  public function getCartContent($sesid = false)
	  {
	
		  $uid = ($sesid) ? $sesid : Registry::get("Users")->sesid;
	
		  $sql = "SELECT c.*, p.id as pid, p.title" . Lang::$lang . " as ptitle, p.price, p.thumb, p.slug, COUNT(c.pid) as total"
		  . "\n FROM " . self::xTable . " as c" 
		  . "\n LEFT JOIN " . self::mTable . " as p ON p.id = c.pid" 
		  . "\n WHERE c.user_id = '" . self::$db->escape($uid) . "'" 
		  . "\n GROUP BY c.pid ORDER BY c.pid DESC";
		  
		  $row = self::$db->fetch_all($sql);

		  return ($row) ? $row : 0;
	  }

	  public function getLatestTransactionId()
	  {
	  	$sql = 'SELECT MAX(transaction_id) as transaction_id FROM '.self::trTable.' 
	  	GROUP BY transaction_id 
	  	ORDER BY transaction_id DESC 
	  	LIMIT 1';

	  	$row = self::$db->first($sql);
	
		return ($row) ? $row : 1;
 	  }

      /**
       * Digishop::getCartTotal()
       * 
       * @param bool $sesid
       * @return
       */
	  public function getCartTotal($sesid = false)
	  {
	
		  $uid = ($sesid) ? $sesid : Registry::get("Users")->sesid;
	
		  $sql = "SELECT SUM(c.price) as total, COUNT(c.pid) as titems" 
		  . "\n FROM " . self::xTable . " as c" 
		  . "\n WHERE c.user_id = '" . self::$db->escape($uid) . "'" 
		  . "\n GROUP BY c.user_id";
	
		  $row = self::$db->first($sql);
	
		  return ($row) ? $row : 0;
	  }

      /**
       * Digishop::getFreeDownload()
       * 
       * @param mixed $token
       * @return
       */
      public function getFreeDownload($token)
      {
		  
		  $id = sanitize($token);
		  $id = self::$db->escape($token);
		  
          $sql = "SELECT * FROM " . self::mTable
		  . "\n WHERE token = '" .$id . "' AND active = 1 AND price = 0";
          $row = self::$db->first($sql);
          
          return ($row) ? $row : 0;
      }

      /**
       * Digishop::getMembershipDownload()
       * 
       * @param mixed $token
       * @return
       */
      public function getMembershipDownload($token)
      {
		  
		  $id = sanitize($token);
		  $id = self::$db->escape($token);
		  
          $sql = "SELECT * FROM " . self::mTable
		  . "\n WHERE token = '" .$id . "' AND active = 1 AND membership_id <> 0";
          $row = self::$db->first($sql);
          
          return ($row) ? $row : 0;
      }

      /**
       * Digishop::getPaidDownload()
       * 
       * @param mixed $token
       * @return
       */
	  public function getPaidDownload($token)
	  {
	
		  $id = sanitize($token);
		  $id = self::$db->escape($token);
	
		  $row = self::$db->first("SELECT t.*, p.filename" 
		  . " \n FROM " . self::trTable . " as t" 
		  . " \n LEFT JOIN " . self::mTable . " as p ON t.pid = p.id" 
		  . " \n WHERE t.token = '" . self::$db->escape($id) . "'" 
		  . " \n AND t.uid = " . Registry::get("Users")->uid
		  . " \n AND t.status = 1 AND t.active = 1" 
		  . " \n AND p.active = 1");
	
		  return ($row) ? $row : 0;
	  }

	  	  	  
	  	  	  	  	  	  	  	  
      /**
       * Digishop::verifyTxnId()
       * 
       * @param mixed $txn_id
       * @return
       */
      public static function verifyTxnId($txn_id)
      {
          
          $sql = self::$db->query("SELECT id" 
				. "\n FROM ".self::trTable."" 
				. "\n WHERE txn_id = '" . sanitize($txn_id) . "'" 
				. "\n LIMIT 1");
		  		
          if (self::$db->numrows($sql) > 0)
              return false;
          else
              return true;
      }
  
	  /**
	   * Digishop::downloadErrors()
	   * 
	   * @return
	   */
	  public static function downloadErrors()
	  {
		  print '<div id="showerror" style="display:none"> ';
		  switch($_GET['msg']) {
			  case 1 :
				print '<p class="veriasist warning message"><i class="icon attention"></i> ' . Lang::$word->_MOD_DS_MSG1 . '</p>';
			  break;
			  
			  case 2 :
				print '<p class="veriasist warning message"><i class="icon attention"></i> ' . Lang::$word->_MOD_DS_MSG2 . '</p>';
			  break;
			  
			  case 3 :
				print '<p class="veriasist warning message"><i class="icon attention"></i> ' . Lang::$word->_MOD_DS_MSG3 . '</p>';
			  break;
			  
			  case 4 :
				print '<p class="veriasist warning message"><i class="icon attention"></i> ' . Lang::$word->_MOD_DS_MSG4 . '</p>';
			  break;
			  
			  case 5 :
				print '<p class="veriasist error message"><i class="icon attention"></i>' . str_replace("[IP]", $_SERVER['REMOTE_ADDR'], Lang::$word->_MOD_DS_MSG5) . '</p>';
			  break;
		  }
		  print '</div>';
	  }


	  public function firstStep()
	  {
	  	$html = '
	  	<div class="col-md-12">
	  		<ul class="nav nav-tabs nav-justified">
	  			<li class="active"><a href="javascript:;">1. '.Lang::$word->_MOD_CART_SUM.'</a></li>
	  			<li class="disabled"><a href="javascript:;">2. '.Lang::$word->_MOD_CART_SHPP.'</a></li>
	  			<li class="disabled"><a href="javascript:;">3. '.Lang::$word->_MOD_CHCK_PAY.'</a></li>
	  		</ul>
	  	</div>

	  	<div id="cartlist">
	  		<div class="panel-body">
	  			<div class="container">
	  				<div class="row">
	  					<div class="table-responsive" style="    overflow-x: visible;">
	  						<table class="table table-hover table-condensed no-margin">
	  							<thead>
	  								<tr>
	  									<th>'.Lang::$word->_MOD_DS_NAME.'</th>
	  									<th>'.Lang::$word->_MOD_DS_QNTTY.'</th>
	  									<th class="text-center">'.Lang::$word->_MOD_DS_PRICE.'</th>
	  									<th class="text-center">'.Lang::$word->_MOD_DS_TOTPRICE.'</th>
	  									<th> </th>
	  								</tr>
	  							</thead>
	  							<tbody>
	  								'.$this->loadcart('shipping').'
	  							</tbody>
	  						</table>
	  					</div>
	  				</div>
	  			</div>
	  		</div>
	  	</div>';

	  	return $html;
	  }

	  public function secondStep($onlyTabs = false)
	  {
	  	$html = '
	  	<div class="col-md-12">
	  		<ul class="nav nav-tabs nav-justified">
	  			<li class="disabled"><a href="javascript:;">1. '.Lang::$word->_MOD_CART_SUM.'</a></li>
	  			<li class="active"><a href="javascript:;">2. '.Lang::$word->_MOD_CART_SHPP.'</a></li>
	  			<li class="disabled"><a href="javascript:;">3. '.Lang::$word->_MOD_CHCK_PAY.'</a></li>
	  		</ul>
	  	</div>
	  	';
	  	if($onlyTabs) return $html;

	  	$addresses = $this->getUserAddresses();
	  	$countries = $this->getContries();

	  	$html .= '
	  	<div class="container minheight500"> 
	  		<div style="margin-top:50px">
	  			<div class="panel panel-info">
	  				<div class="panel-body" >
	  					<div id="msgholder"></div>
	  					<form id="veriasist_form" name="veriasist_form" method="post" class="form-horizontal" role="form">


	  						<div class="form-group">
	  							<label for="invoiceAddresses" class="col-md-3 control-label">'.Lang::$word->_MOD_SELADDR.'</label>
	  							<div class="col-md-9">
	  								<select name="invAdress" id="invoiceAddresses" class="form-control">
	  									<option value="0">'.Lang::$word->_MOD_ADD_ADRS.'</option>';

	  									if($addresses):
	  										foreach ($addresses as $address):
	  											$html .= ' <option value="'.$address->id.'">'.$address->name.'</option>';
	  										endforeach;
	  									endif;
	  									$html .= '
	  									</select>
	  								</div>
	  							</div>
							
				<div class="address1Container">
	  							<div class="form-group" id="addressNameContiner">
	  							<label for="addressName" class="col-md-3 control-label">'.Lang::$word->_MOD_ADDRESS_NAME.'</label>
	  								<div class="col-md-9">
	  								<input type="text" class="form-control" name="addressName" id="addressName" placeholder="'.Lang::$word->_MOD_ADDRESS_NAME.'">
	  								</div>
	  							</div>


	  							<div class="form-group">
	  								<label for="country" class="col-md-3 control-label">'.Lang::$word->_COUNTRY.'</label>
	  								<div class="col-md-9">
	  									<select name="country" id="country" data-target="city" class="form-control">
	  										<option value="0">'.Lang::$word->_MOD_SELECT.'</option>';
	  										if($countries):
	  											foreach ($countries as $contry):
	  												$html .= '<option value="'.$contry->country_id.'">'.$contry->name.'</option>';
	  											endforeach;
	  										endif;
	  										$html .= '
	  										</select>
	  									</div>
	  								</div>

	  								<div class="form-group">
	  									<label for="city" class="col-md-3 control-label">'.Lang::$word->_CITY.'</label>
	  									<div class="col-md-9">
	  										<select class="form-control" name="city" id="city" disabled="disabled">
	  											<option>Seçiniz</option>
	  										</select>
	  										
	  									</div>
	  								</div>


	  								<div class="form-group">
	  									<label for="state_province" class="col-md-3 control-label">'.Lang::$word->_STATE_PROVINCE.'</label>
	  									<div class="col-md-9">
	  										<input type="text" class="form-control" name="state_province" id="state_province" placeholder="'.Lang::$word->_STATE_PROVINCE.'">
	  									</div>
	  								</div>

	  								<div class="form-group">
	  									<label for="zip_post" class="col-md-3 control-label">'.Lang::$word->_ZIP_POSTAL.'</label>
	  									<div class="col-md-9">
	  										<input type="text" class="form-control" name="zip_post" id="zip_post" placeholder="'.Lang::$word->_ZIP_POSTAL.'">
	  									</div>
	  								</div>

	  								<div class="form-group">
	  									<label for="address" class="col-md-3 control-label">'.Lang::$word->_ADDRESS.'</label>
	  									<div class="col-md-9">
	  										<textarea placeholder="'.Lang::$word->_ADDRESS.'" name="address" class="form-control" id="address"></textarea>
	  									</div>
	  								</div>
	  			</div>

	  								<div class="form-group">
	  									<label for="address" class="col-md-3 control-label">'.Lang::$word->_SAME_BILLING_ADR.'</label>
	  									<div class="col-sm-9">
	  										<div class="checkbox">
	  											<label>
	  												<input name="sameaddress" id="sameaddress" type="checkbox" value="1" checked="checked"> '.Lang::$word->_YES.'
	  											</label>
	  										</div>
	  									</div>
	  								</div>


	  								<!-- Second Address -->
	  								<div id="secondAddress" style="display: none;">

	  									<div class="form-group">
	  										<label for="invoiceAddresses1" class="col-md-3 control-label">'.Lang::$word->_MOD_SELADDR.'</label>
	  										<div class="col-md-9">
	  											<select name="invAdress1" id="invoiceAddresses1" class="form-control">
	  												<option value="0">'.Lang::$word->_MOD_ADD_ADRS.'</option>';
	  												if($addresses):
	  													foreach ($addresses as $address):
	  														$html .= '<option value="'.$address->id.'">'.$address->name.'</option>';
	  													endforeach;
	  												endif;
	  												$html .= '
	  												</select>
	  											</div>
	  										</div>


	  										<div class="form-group">
	  											<label for="country1" class="col-md-3 control-label">'.Lang::$word->_COUNTRY.'</label>
	  											<div class="col-md-9">
	  												<select name="country1" id="country1" data-target="city1" class="form-control">
	  													<option value="0">'.Lang::$word->_MOD_SELECT.'</option>';
	  													if($countries):
	  														foreach ($countries as $contry):
	  															$html .= '<option value="'.$contry->country_id.'">'.$contry->name.'</option>';
	  														endforeach;
	  													endif;
	  													$html .= '
	  													</select>
	  												</div>
	  											</div>

	  											<div class="form-group" id="addressName1Continer">
	  												<label for="addressName1" class="col-md-3 control-label">'.Lang::$word->_MOD_ADDRESS_NAME.'</label>
	  												<div class="col-md-9">
	  													<input type="text" class="form-control" name="addressName1" id="addressName1" placeholder="'.Lang::$word->_MOD_ADDRESS_NAME.'">
	  												</div>
	  											</div>

	  											<div class="form-group">
	  												<label for="city1" class="col-md-3 control-label">'.Lang::$word->_CITY.'</label>
	  												<div class="col-md-9">
	  													<select class="form-control" name="city1" id="city1" disabled="disabled">
				  											<option>Seçiniz</option>
				  										</select>
	  												</div>
	  											</div>


	  											<div class="form-group">
	  												<label for="state_province1" class="col-md-3 control-label">'.Lang::$word->_STATE_PROVINCE.'</label>
	  												<div class="col-md-9">
	  													<input type="text" class="form-control" name="state_province1" id="state_province1" placeholder="'.Lang::$word->_STATE_PROVINCE.'">
	  												</div>
	  											</div>

	  											<div class="form-group">
	  												<label for="zip_post1" class="col-md-3 control-label">'.Lang::$word->_ZIP_POSTAL.'</label>
	  												<div class="col-md-9">
	  													<input type="text" class="form-control" name="zip_post1" id="zip_post1" placeholder="'.Lang::$word->_ZIP_POSTAL.'">
	  												</div>
	  											</div>

	  											<div class="form-group">
	  												<label for="address1" class="col-md-3 control-label">'.Lang::$word->_ADDRESS.'</label>
	  												<div class="col-md-9">
	  													<textarea placeholder="'.Lang::$word->_ADDRESS.'" name="address1" class="form-control" id="address1"></textarea>
	  												</div>
	  											</div>

	  										</div>
	  										<!-- Second Address END -->

	  										<div class="form-group pull-right">                                     
	  											<div class="col-md-9">
	  												<button type="button" id="paymentNext3" class="btn btn-info">'.Lang::$word->_PAG_NEXT.'</button>
	  											</div>
	  										</div>

	  										<input name="paymentNext3" type="hidden" value="1">

	  									</form>
	  								</div>

	  							</div>

	  						</div> 
	  					</div>
	  					';
	  	return $html;
	  }
  }
?>
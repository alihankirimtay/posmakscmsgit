<?php
  /**
   * Controller
   *
   * @package Veri Asist
   * @author veriasist.com
   * @copyright 2014
   * @version $Id: controller.php, v4.00 2014-04-20 10:12:05 gewa Exp $
   */
  define("_VALID_PHP", true);

  require_once ("../../init.php");
  
  if (!$user->is_Admin())
      redirect_to("../../login.php");

  require_once ("admin_class.php");
  Registry::set('Digishop', new Digishop());
?>
<?php
  /* == Update Configuration == */
  if (isset($_POST['processConfig'])):
      Registry::get("Digishop")->processConfig();
  endif;

  /* == Proccess Product == */
  if (isset($_POST['processProduct'])):
      Registry::get("Digishop")->processProduct();
  endif;

  /* == Delete Product == */
  if (isset($_POST['delete']) and $_POST['delete'] == "deleteProduct"):
      $title = sanitize($_POST['title']);
      if ($thumb = getValueById("thumb", Digishop::mTable, Filter::$id)):
          unlink(BASEPATH . Digishop::imagepath . $thumb);
      endif;
      if ($file = getValueById("filename", Digishop::mTable, Filter::$id)):
          unlink(Registry::get("Digishop")->filedir . $file);
      endif;
      $result = $db->delete(Digishop::mTable, "id=" . Filter::$id);

      if ($result):
          $json['type'] = 'success';
          $json['title'] = Lang::$word->_SUCCESS;
          $json['message'] = Lang::$word->_MOD_DS_PRODUCT . ' /' . $title . '/ ' . Lang::$word->_DELETED;
          Security::writeLog(Lang::$word->_MOD_DS_PRODUCT . ' /' . urldecode($title) . '/ ' . Lang::$word->_DELETED, "", "no", "module");
      else:
          $json['type'] = 'warning';
          $json['title'] = Lang::$word->_ALERT;
          $json['message'] = Lang::$word->_SYSTEM_PROCCESS;
      endif;
      print json_encode($json);
  endif;

  /* == Update Category Order == */
  if (isset($_GET['sortcats'])) :
	  Digishop::updateOrder();
 endif;

  /* == Proccess Category == */
  if (isset($_POST['processCategory'])): 
	  Registry::get("Digishop")->processCategory();
  endif;

  /* == Delete Category == */
  if (isset($_POST['delete']) and $_POST['delete'] == "deleteCategory"):
      $title = sanitize($_POST['title']);
      $result = $db->delete(Digishop::ctTable, "id=" . Filter::$id);

      if ($result):
          $json['type'] = 'success';
          $json['title'] = Lang::$word->_SUCCESS;
          $json['message'] = Lang::$word->_MOD_DS_CATEGORY . ' /' . $title . '/ ' . Lang::$word->_DELETED;
          Security::writeLog(Lang::$word->_MOD_DS_CATEGORY . ' /' . urldecode($title) . '/ ' . Lang::$word->_DELETED, "", "no", "module");
      else:
          $json['type'] = 'warning';
          $json['title'] = Lang::$word->_ALERT;
          $json['message'] = Lang::$word->_SYSTEM_PROCCESS;
      endif;
      print json_encode($json);
  endif;

  /* == Delete Transaction == */
  if (isset($_POST['delete']) and $_POST['delete'] == "deleteTransaction"):
      $title = sanitize($_POST['title']);
      $row = Core::getRow(Digishop::trTable, "id", Filter::$id);
      $result = $db->delete(Digishop::trTable, "id=" . Filter::$id);

      if ($result):

          if(!Core::getRow(Digishop::trTable, "transaction_id", $row->transaction_id, false)):
            $db->delete('mod_digishop_transaction_addresses', "transaction_id=" . $row->transaction_id);
            $db->delete('mod_digishop_transaction_statuses', "transaction_id=" . $row->transaction_id);
          endif;

          $json['type'] = 'success';
          $json['title'] = Lang::$word->_SUCCESS;
          $json['message'] = Lang::$word->_MOD_DS_TRANSREC . ' /' . $title . '/ ' . Lang::$word->_DELETED;
          Security::writeLog(Lang::$word->_MOD_DS_TRANSREC . ' /' . urldecode($title) . '/ ' . Lang::$word->_DELETED, "", "no", "module");
      else:
          $json['type'] = 'warning';
          $json['title'] = Lang::$word->_ALERT;
          $json['message'] = Lang::$word->_SYSTEM_PROCCESS;
      endif;
      print json_encode($json);
  endif;
  
  /* == Activate Transaction == */
  if (isset($_POST['activateTransaction'])): 
	  Registry::get("Digishop")->activateTransaction();
  endif;
  
  /* == Export Transactions == */
  if (isset($_GET['exportTransactions'])) :
	  $sql = "SELECT t.*, t.id as id, u.id as uid, u.username, p.id as did, p.title" . Lang::$lang . " as title,"
	  . "\n DATE_FORMAT(p.created, '%d %b %Y') as cdate"
	  . "\n FROM " . Digishop::trTable . " as t" 
	  . "\n LEFT JOIN " . Users::uTable . " as u ON u.id = t.uid" 
	  . "\n LEFT JOIN " . Digishop::mTable . " as p ON p.id = t.pid" 
	  . "\n ORDER BY t.created DESC" . $pager->limit;
      $result = $db->fetch_all($sql);
      
      $type = "vnd.ms-excel";
	  $date = date('m-d-Y H:i');
	  $title = "Exported from the " . $core->site_name . " on $date";

      header("Pragma: public");
      header("Expires: 0");
      header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
      header("Content-Type: application/force-download");
      header("Content-Type: application/octet-stream");
      header("Content-Type: application/download");
	  header("Content-Type: application/$type");
      header("Content-Disposition: attachment;filename=temp_" . time() . ".xls");
      header("Content-Transfer-Encoding: binary ");

	  print '
	  <table width="100%" cellpadding="1" cellspacing="2" border="1">
	  <caption>' . $title . '</caption>
		<tr>
		  <td>TXN_ID</td>
		  <td>' .Lang::$word-> _MOD_DS_NAME . '</td>
		  <td>' . Lang::$word->_USERNAME . '</td>
		  <td>' . Lang::$word->_TR_AMOUNT . '</td>
		  <td>' . Lang::$word->_TR_PAYDATE . '</td>
		  <td>' . Lang::$word->_TR_PROCESSOR . '</td>
		</tr>';
		foreach ($result as $v) {
			print '<tr>
			  <td>'.$v->txn_id.'</td>
			  <td>'.$v->title . '</td>
			  <td>'.$v->username.'</td>
			  <td>'.$v->price.'</td>
			  <td>'.$v->cdate.'</td>
			  <td>'.$v->pp.'</td>
			</tr>';
		}				

	  print '</table>';
	  exit;
  endif;
?>
<?php
  /**
   * Language Data Add
   *
   * @package VeriAsist
   * @author veriasist.com
   * @copyright 2014
   * @version $Id: lang-add.php, v4.00 2014-04-20 10:12:05 gewa Exp $
   */
  if (!defined("_VALID_PHP"))
      die('Direct access to this location is not allowed.');
?>
<?php
  self::$db->query("LOCK TABLES mod_career WRITE");
  self::$db->query("ALTER TABLE mod_career ADD question_$flag_id VARCHAR(150) NOT NULL AFTER question_en");
  self::$db->query("ALTER TABLE mod_career ADD answer_$flag_id TEXT AFTER answer_en");
  self::$db->query("UNLOCK TABLES");

  if ($mod_career = self::$db->fetch_all("SELECT * FROM mod_career")) {
      foreach ($mod_career as $row) {
          $data = array(
              'question_' . $flag_id => $row->question_en,
              'answer_' . $flag_id => $row->answer_en);

          self::$db->update("mod_career", $data, "id = " . $row->id);
      }
      unset($data, $row);
  }
?>
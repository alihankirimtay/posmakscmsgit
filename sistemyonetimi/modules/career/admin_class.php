<?php
  /**
   * CAREER Manager Class
   *
   * @package VeriAsist
   * @author veriasist.com
   * @copyright 2014
   * @version $Id: admin_class.php, v1.00 2014-05-31 20:15:17 gewa Exp $
   */
  
  if (!defined("_VALID_PHP"))
      die('Direct access to this location is not allowed.');
  
  class CareerManager
  {

    const mTable = "mod_career";
	  const imagepath = "modules/career/dataimages/";
	  private static $db;


      /**
       * CareerManager::__construct()
       * 
       * @return
       */
      function __construct()
      {
		  self::$db = Registry::get("Database");
      }

      /**
       * CareerManager::getCareer()
       * 
       * @return
       */
      public function getCareer()
      {
          $sql = "SELECT * FROM " . self::mTable . " ORDER BY position";
          $row = self::$db->fetch_all($sql);

          return ($row) ? $row : 0;
      }

      /**
       * CareerManager::processCareer()
       * 
       * @return
       */
      public function processCareer()
      {
          Filter::checkPost('question' . Lang::$lang, Lang::$word->_MOD_CAREER_TITLE);
		      Filter::checkPost('answer' . Lang::$lang, Lang::$word->_MOD_CAREER_DESC);

          if (!empty($_FILES['thumb']['name'])) {
            if (!preg_match("/(\.jpg|\.png|\.gif)$/i", $_FILES['thumb']['name']))
              Filter::$msgs['thumb'] = Lang::$word->_MOD_AM_IMAGE_R;
          }
			  
          if (empty(Filter::$msgs)) {

          $data = array(
            'question' . Lang::$lang => sanitize($_POST['question' . Lang::$lang]), 
            'answer' . Lang::$lang => Filter::in_url($_POST['answer' . Lang::$lang])
          );

          // Procces Image
          if (!empty($_FILES['thumb']['name'])) {
            $imgdir = BASEPATH . self::imagepath;
            $newName = "IMG_" . randName();
            $ext = substr($_FILES['thumb']['name'], strrpos($_FILES['thumb']['name'], '.') + 1);
            $fullname = $imgdir . $newName . "." . strtolower($ext);
            
            if (Filter::$id and $file = getValueById("thumb", self::mTable, Filter::$id)) {
              @unlink($imgdir . $file);
            }
            
            if (!move_uploaded_file($_FILES['thumb']['tmp_name'], $fullname)) {
              die(Filter::msgError(Lang::$word->_FILE_ERR, false));
            }
            $data['thumb'] = $newName . "." . strtolower($ext);
          }

			  (Filter::$id) ? self::$db->update(self::mTable, $data, "id=" . Filter::$id) : $lastid = self::$db->insert(self::mTable, $data);
			  $message = (Filter::$id) ? Lang::$word->_MOD_CAREER_UPDATED : Lang::$word->_MOD_CAREER_ADDED;


			  if (self::$db->affected()) {
				  Security::writeLog($message, "", "no", "module");
				  $json['type'] = 'success';
				  $json['message'] = Filter::msgOk($message, false);
			  } else {
				  $json['type'] = 'success';
				  $json['message'] = Filter::msgAlert(Lang::$word->_SYSTEM_PROCCESS, false);
			  }
			  print json_encode($json);

		  } else {
			  $json['message'] = Filter::msgStatus();
			  print json_encode($json);
		  }
	  }
	  
      /**
       * CareerManager::updateOrder()
       * 
       * @return
       */
	  public static function updateOrder()
	  {
		  	  
		  foreach ($_POST['node'] as $k => $v) {
			  $p = $k + 1;
			  $data['position'] = intval($p);
			  self::$db->update(self::mTable, $data, "id=" . (int)$v);
		  }
		  
	  }
  }
?>
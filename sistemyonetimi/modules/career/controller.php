<?php
  /**
   * Controller
   *
   * @package VeriAsist
   * @author veriasist.com
   * @copyright 2014
   * @version $Id: controller.php, v4.00 2014-04-20 10:12:05 gewa Exp $
   */
  define("_VALID_PHP", true);
  
  require_once("../../init.php");
  if (!$user->is_Admin())
      redirect_to("../../login.php");
  
  require_once("admin_class.php");
  Registry::set('CareerManager', new CareerManager());
?>
<?php
  /* == Process CAREER == */
  if (isset($_POST['processCareer'])):
      Registry::get("CareerManager")->processCareer();
  endif;

  /* == Update CAREER Order == */
  if (isset($_GET['sortslides'])):
      CareerManager::updateOrder();
  endif;
  
  /* == Delete CAREER == */
  if (isset($_POST['delete']) and $_POST['delete'] == "deleteCareer"):
      $title = sanitize($_POST['title']);
      $result = $db->delete(CareerManager::mTable, "id=" . Filter::$id);

      if ($result):
          $json['type'] = 'success';
          $json['title'] = Lang::$word->_SUCCESS;
          $json['message'] = Lang::$word->_MOD_CAREER_CAREER . ' /' . $title . '/ ' . Lang::$word->_DELETED;
          Security::writeLog(Lang::$word->_MOD_CAREER_CAREER . ' /' . urldecode($title) . '/ ' . Lang::$word->_DELETED, "", "no", "module");
      else:
          $json['type'] = 'warning';
          $json['title'] = Lang::$word->_ALERT;
          $json['message'] = Lang::$word->_SYSTEM_PROCCESS;
      endif;
      print json_encode($json);
  endif;
?>
<?php
  /**
   * CAREER Manager
   *
   * @package VeriAsist
   * @author veriasist.com
   * @copyright 2014
   * @version $Id: admin.php, v0.00 2014-05-31 10:12:05 gewa Exp $
   */
  if (!defined("_VALID_PHP"))
      die('Direct access to this location is not allowed.');

  if(!$user->getAcl("career")): print Filter::msgAlert(Lang::$word->_CG_ONLYADMIN); return; endif;

  Registry::set('CareerManager',new CareerManager());
?>
<?php switch(Filter::$maction): case "edit": ?>
<?php $row = Core::getRowById(CareerManager::mTable, Filter::$id);?>
<div class="veriasist icon heading message blue"> <i class="puzzle piece icon"></i>
  <div class="content">
    <div class="header"> <?php echo Lang::$word->_MOD_CAREER_TITLE3;?> </div>
    <div class="veriasist breadcrumb"><i class="icon home"></i> <a href="index.php" class="section"><?php echo Lang::$word->_N_DASH;?></a>
      <div class="divider"> / </div>
      <a href="index.php?do=modules" class="section"><?php echo Lang::$word->_N_MODS;?></a>
      <div class="divider"> / </div>
      <a href="index.php?do=modules&amp;action=config&amp;modname=career" class="section"><?php echo $content->getModuleName(Filter::$modname);?></a>
      <div class="divider"> / </div>
      <div class="active section"><?php echo Lang::$word->_MOD_CAREER_TITLE1;?></div>
    </div>
  </div>
</div>
<div class="veriasist-large-content">
  <div class="veriasist message"><?php echo Core::langIcon();?><?php echo Lang::$word->_MOD_CAREER_INFO1. Lang::$word->_REQ1 . '<i class="icon asterisk"></i>' . Lang::$word->_REQ2;?></div>
  <div class="veriasist form segment">
    <div class="veriasist header"><?php echo Lang::$word->_MOD_CAREER_SUBTITLE1 . $row->{'question' . Lang::$lang};?></div>
    <div class="veriasist double fitted divider"></div>
    <form id="veriasist_form" name="veriasist_form" method="post">
      <div class="field">
        <label><?php echo Lang::$word->_MOD_CAREER_TITLE;?></label>
        <label class="input"><i class="icon-append icon asterisk"></i>
          <input type="text" value="<?php echo $row->{'question' . Lang::$lang};?>" name="question<?php echo Lang::$lang;?>">
        </label>
      </div>
      <div class="veriasist fitted divider"></div>
      <div class="field">
        <label><?php echo Lang::$word->_MOD_CAREER_DESC;?></label>
        <textarea id="plugpost" class="plugpost" name="answer<?php echo Lang::$lang;?>"><?php echo Filter::out_url($row->{'answer' . Lang::$lang});?></textarea>
      </div>
      <div class="two fields">
        <div class="field">
          <label><?php echo Lang::$word->_MOD_CAREER_PIMAGE;?></label>
          <label class="input">
            <input type="file" name="thumb" class="filefield">
          </label>
        </div>
        <?php if ($row->thumb): ?>
          <div class="field">
          <label><?php echo Lang::$word->_MOD_CAREER_PIMAGEPP;?></label>
            <div class="veriasist small image"> <a class="lightbox" href="<?php echo SITEURL . '/' . CareerManager::imagepath . $row->thumb;?>"><img src="<?php echo SITEURL . '/' . CareerManager::imagepath . $row->thumb;?>" alt="<?php echo $row->thumb;?>"></a> </div>
          </div>
        <?php endif ?>
      </div>

      <div class="veriasist double fitted divider"></div>
      <button type="button" name="dosubmit" class="veriasist positive button"><?php echo Lang::$word->_MOD_CAREER_UPDATE;?></button>
      <a href="index.php?do=modules&amp;action=config&amp;modname=career" class="veriasist basic button"><?php echo Lang::$word->_CANCEL;?></a>
      <input name="id" type="hidden" value="<?php echo Filter::$id;?>">
      <input name="processCareer" type="hidden" value="1">
    </form>
  </div>
  <div id="msgholder"></div>
</div>
<?php break;?>
<?php case"add": ?>
<div class="veriasist icon heading message blue"> <i class="puzzle piece icon"></i>
  <div class="content">
    <div class="header"> <?php echo Lang::$word->_MOD_CAREER_TITLE3;?> </div>
    <div class="veriasist breadcrumb"><i class="icon home"></i> <a href="index.php" class="section"><?php echo Lang::$word->_N_DASH;?></a>
      <div class="divider"> / </div>
      <a href="index.php?do=modules" class="section"><?php echo Lang::$word->_N_MODS;?></a>
      <div class="divider"> / </div>
      <a href="index.php?do=modules&amp;action=config&amp;modname=career" class="section"><?php echo $content->getModuleName(Filter::$modname);?></a>
      <div class="divider"> / </div>
      <div class="active section"><?php echo Lang::$word->_MOD_CAREER_TITLE2;?></div>
    </div>
  </div>
</div>
<div class="veriasist-large-content">
  <div class="veriasist message"><?php echo Core::langIcon();?><?php echo Lang::$word->_MOD_CAREER_INFO. Lang::$word->_REQ1 . '<i class="icon asterisk"></i>' . Lang::$word->_REQ2;?></div>
  <div class="veriasist form segment">
    <div class="veriasist header"><?php echo Lang::$word->_MOD_CAREER_SUBTITLE;?></div>
    <div class="veriasist double fitted divider"></div>
    <form id="veriasist_form" name="veriasist_form" method="post">
      <div class="field">
        <label><?php echo Lang::$word->_MOD_CAREER_TITLE;?></label>
        <label class="input"><i class="icon-append icon asterisk"></i>
          <input type="text" placeholder="<?php echo Lang::$word->_MOD_CAREER_TITLE;?>" name="question<?php echo Lang::$lang;?>">
        </label>
      </div>
      <div class="veriasist fitted divider"></div>
      <div class="field">
        <label><?php echo Lang::$word->_MOD_CAREER_DESC;?></label>
        <textarea id="plugpost" class="plugpost" name="answer<?php echo Lang::$lang;?>"></textarea>
      </div>
      <div class="field">
        <label><?php echo Lang::$word->_MOD_CAREER_PIMAGE;?></label>
        <label class="input">
          <input type="file" name="thumb" class="filefield">
        </label>
      </div>
      <div class="veriasist double fitted divider"></div>
      <button type="button" name="dosubmit" class="veriasist positive button"><?php echo Lang::$word->_MOD_CAREER_ADD;?></button>
      <a href="index.php?do=modules&amp;action=config&amp;modname=career" class="veriasist basic button"><?php echo Lang::$word->_CANCEL;?></a>
      <input name="processCareer" type="hidden" value="1">
    </form>
  </div>
  <div id="msgholder"></div>
</div>
<?php break;?>
<?php default: ?>
<?php $careerrow = Registry::get("CareerManager")->getCareer();?>
<div class="veriasist icon heading message blue"> <i class="puzzle piece icon"></i>
  <div class="content">
    <div class="header"> <?php echo Lang::$word->_MOD_CAREER_TITLE3;?> </div>
    <div class="veriasist breadcrumb"><i class="icon home"></i> <a href="index.php" class="section"><?php echo Lang::$word->_N_DASH;?></a>
      <div class="divider"> / </div>
      <a href="index.php?do=modules" class="section"><?php echo Lang::$word->_N_MODS;?></a>
      <div class="divider"> / </div>
      <div class="active section"><?php echo $content->getModuleName(Filter::$modname);?></div>
    </div>
  </div>
</div>
<div class="veriasist-large-content">
  <div class="veriasist message"><?php echo Core::langIcon();?><?php echo Lang::$word->_MOD_CAREER_INFO3;?></div>
  <div class="veriasist segment"> <a href="<?php echo Core::url("modules", "add");?>" class="veriasist info button push-right"><i class="icon add"></i><?php echo Lang::$word->_MOD_CAREER_ADD;?></a>
    <div class="veriasist header"><?php echo Lang::$word->_MOD_CAREER_SUBTITLE3;?></div>
    <div class="veriasist fitted divider"></div>
    <table class="veriasist sortable table">
      <thead>
        <tr>
          <th class="disabled"></th>
          <th data-sort="string"><?php echo Lang::$word->_MOD_CAREER_TITLE;?></th>
          <th data-sort="int"><?php echo Lang::$word->_MOD_CAREER_POS;?></th>
          <th class="disabled"><?php echo Lang::$word->_ACTIONS;?></th>
        </tr>
      </thead>
      <tbody>
        <?php if(!$careerrow):?>
        <tr>
          <td colspan="4"><?php echo Filter::msgSingleAlert(Lang::$word->_MOD_CAREER_NOCAREERS);?></td>
        </tr>
        <?php else:?>
        <?php foreach ($careerrow as $row):?>
        <tr id="node-<?php echo $row->id;?>">
          <td class="id-handle"><i class="icon reorder"></i></td>
          <td><?php echo $row->{'question' . Lang::$lang};?></td>
          <td><span class="veriasist black label"><?php echo $row->position;?></span></td>
          <td><a href="<?php echo Core::url("modules", "edit", $row->id);?>"><i class="rounded inverted success icon pencil link"></i></a> <a class="delete" data-title="<?php echo Lang::$word->_DELETE.' '.Lang::$word->_MOD_CAREER_CAREER;?>" data-option="deleteCareer" data-id="<?php echo $row->id;?>" data-name="<?php echo $row->{'question' . Lang::$lang};?>"><i class="rounded danger inverted remove icon link"></i></a></td>
        </tr>
        <?php endforeach;?>
        <?php unset($slrow);?>
        <?php endif;?>
      </tbody>
    </table>
  </div>
</div>
<script type="text/javascript"> 
// <![CDATA[
$(document).ready(function () {
    $(".veriasist.table tbody").sortable({
        helper: 'clone',
        handle: '.id-handle',
        placeholder: 'placeholder',
        opacity: .6,
        update: function (event, ui) {
            serialized = $(".veriasist.table tbody").sortable('serialize');
            $.ajax({
                type: "POST",
                url: "modules/career/controller.php?sortslides",
                data: serialized,
                success: function (msg) {}
            });
        }
    });
});
// ]]>
</script>
<?php break;?>
<?php endswitch;?>
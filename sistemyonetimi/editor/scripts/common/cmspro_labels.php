<?php
  define("_VALID_PHP", true);
  require_once("../../../init.php");
  if (!$user->is_Admin())
    exit;
?>
<!DOCTYPE HTML>
<html>
<head>
<meta charset="utf-8">
<title>Insert Labels</title>
<link rel="stylesheet" type="text/css" href="<?php echo SITEURL . '/theme/' . $core->theme;?>/cache/master_main.css">
<script type="text/javascript" src="../../../../assets/jquery.js"></script>
<style type="text/css">
body{
  font-family: "Open Sans", sans-serif;
  margin: 0px;
  padding: 0px;
  color: #999999;
  text-rendering: optimizeLegibility;
  font-weight: 400;
}
a {
  cursor: pointer;
  text-decoration: none;
  -webkit-transition: all .35s ease;
  -moz-transition: all .35s ease;
  -o-transition: all .35s ease;
  transition: all .35s ease;
  outline: none
}
a:hover,
a.hover,
a:focus {
  -webkit-transition: all .55s ease;
  -moz-transition: all .55s ease;
  -o-transition: all .35s ease;
  transition: all .55s ease;
  outline: none;
  text-decoration: none
}
.item {
  padding: 20px 30px 30px;
  border-bottom: #e7e7e7 1px solid;
  -webkit-box-shadow: 0 0 40px rgba(0, 0, 0, 0.1) inset;
  -moz-box-shadow: 0 0 40px rgba(0, 0, 0, 0.1) inset;
  box-shadow: 0 0 40px rgba(0, 0, 0, 0.1) inset;
}
</style>
</head>
<body class="item" style="position:absolute;height:100%;width:100%">
<div class="elements" style="padding-top:15px"> <a class="veriasist label">Default</a> &nbsp; <a class="veriasist positive label">Positive</a> &nbsp; <a class="veriasist label teal">Teal</a> &nbsp; <a class="veriasist label info">Info</a> &nbsp; <a class="veriasist label warning">Warning</a> &nbsp; <a class="veriasist label negative">Danger</a> &nbsp; <a class="veriasist label purple">Purple</a> </div>
<script type="text/javascript">
        $(document).ready(function () {
            $('.elements a').click(function () {
                var classname = $(this).attr('class');
                parent.oUtil.obj.insertHTML('<div class="' + classname + '">' + $(this).html() + '</div>&nbsp;');
            });
        });
    </script>
</body>
</html>
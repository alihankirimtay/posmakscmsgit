<?php
  define("_VALID_PHP", true);
  require_once("../../../init.php");
  if (!$user->is_Admin())
    exit;
?>
<!DOCTYPE HTML>
<html>
<head>
<meta charset="utf-8">
<title>Link Buttons</title>
<link rel="stylesheet" type="text/css" href="<?php echo SITEURL . '/theme/' . $core->theme;?>/cache/master_main.css">
<script type="text/javascript" src="../../../../assets/jquery.js"></script>
<style type="text/css">
body {
  font-family: "Open Sans", sans-serif;
  margin: 0px;
  padding: 1em;
  color: #999999;
  text-rendering: optimizeLegibility;
  font-weight: 400;
}
a {
  cursor: pointer;
  text-decoration: none;
  -webkit-transition: all .35s ease;
  -moz-transition: all .35s ease;
  -o-transition: all .35s ease;
  transition: all .35s ease;
  outline: none
}
a:hover,
a.hover,
a:focus {
  -webkit-transition: all .55s ease;
  -moz-transition: all .55s ease;
  -o-transition: all .35s ease;
  transition: all .55s ease;
  outline: none;
  text-decoration: none
}
</style>
</head>
<body>
<h4>Buttons (Links)</h4>
<p>Select Type:</p>
<div class="small-bottom-space">
  <label for="rdoButtonType1">
    <input id="rdoButtonType1" name="rdoButtonType" type="radio" value="veriasist button" checked="checked" />
    <a class="veriasist small button" href="javascript:$('#rdoButtonType1').click();setPreview()">Primary Button</a></label>
  <label for="rdoButtonType2">
    <input id="rdoButtonType2" name="rdoButtonType" type="radio" value="veriasist basic button" />
    <a class="veriasist small basic button" href="javascript:$('#rdoButtonType2').click();setPreview()">Basic Button</a></label>
  <label for="rdoButtonType3">
    <input id="rdoButtonType3" name="rdoButtonType" type="radio" value="veriasist info button" />
    <a class="veriasist small info button" href="javascript:$('#rdoButtonType3').click();setPreview()">Info Button</a></label>
  <label for="rdoButtonType4">
    <input id="rdoButtonType4" name="rdoButtonType" type="radio" value="veriasist success button" />
    <a class="veriasist small success button" href="javascript:$('#rdoButtonType4').click();setPreview()">Success Button</a></label>
</div>
<div class="small-bottom-space">
  <label for="rdoButtonType5">
    <input id="rdoButtonType5" name="rdoButtonType" type="radio" value="veriasist warning button" />
    <a class="veriasist small warning button" href="javascript:$('#rdoButtonType5').click();setPreview()">Warning Button</a></label>
  <label for="rdoButtonType6">
    <input id="rdoButtonType6" name="rdoButtonType" type="radio" value="veriasist danger button" />
    <a class="veriasist small danger button" href="javascript:$('#rdoButtonType6').click();setPreview()">Danger Button</a></label>
  <label for="rdoButtonType7">
    <input id="rdoButtonType7" name="rdoButtonType" type="radio" value="veriasist black button" />
    <a class="veriasist small black button" href="javascript:$('#rdoButtonType7').click();setPreview()">Black Button</a></label>
  <label for="rdoButtonType8">
    <input id="rdoButtonType8" name="rdoButtonType" type="radio" value="veriasist teal button" />
    <a class="veriasist small teal button" href="javascript:$('#rdoButtonType8').click();setPreview()">Teal Button</a></label>
</div>
<p>Select Size:</p>
<div class="small-bottom-space">
  <label for="rdoButtonSize1">
    <input id="rdoButtonSize1" name="rdoButtonSize" type="radio" value="large" onclick="setPreview()" />
    Large</label>

  <label for="rdoButtonSize2">
    <input id="rdoButtonSize2" name="rdoButtonSize" type="radio" value="" onclick="setPreview()" checked="checked" />
    Default</label>

  <label for="rdoButtonSize3">
    <input id="rdoButtonSize3" name="rdoButtonSize" type="radio" value="small" onclick="setPreview()" />
    Small</label>

  <label for="rdoButtonSize4">
    <input id="rdoButtonSize4" name="rdoButtonSize" type="radio" value="mini" onclick="setPreview()" />
    Mini</label>
</div>
<div class="veriasist-grid">
  <div class="columns gutters">
    <div class="screen-40"> Preview:<br />
      <div id="divSnippet" class="veriasist message" style="text-align:center;"> <a class="veriasist button" href="javascript:$('#rdoButtonType1').click();">Buy Now</a> </div>
    </div>
    <div class="screen-60">
      <div class="veriasist small form">
        <label for="inpUrl">URL: </label>
        <input id="inpUrl" type="text" value="http://" onkeyup="setPreview()" />
        <label for="inpText">Button Text: </label>
        <input id="inpText" type="text" value="Buy Now" onkeyup="setPreview()" />
      </div>
    </div>
  </div>
</div>
<a class="veriasist basic button" href="javascript:insertSnippet('divSnippet')">Insert</a>
</div>
<script type="text/javascript">
	function insertSnippet(id) {
		parent.oUtil.obj.insertHTML(document.getElementById(id).innerHTML);
	}
	function setPreview() {
		var tmp = $('input:radio[name=rdoButtonType]:checked').val();
		var tmp2 = $('input:radio[name=rdoButtonSize]:checked').val();
		$('#divSnippet a').attr('class', tmp + ' ' + tmp2);
		$('#divSnippet a').html($('#inpText').val());
		$('#divSnippet a').attr('href', $('#inpUrl').val());
	}
</script>
</body>
</html>
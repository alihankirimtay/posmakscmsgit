<?php
  define("_VALID_PHP", true);
  require_once("../../../init.php");
  if (!$user->is_Admin())
    exit;
?>
<!DOCTYPE HTML>
<html>
<head>
<meta charset="utf-8">
<title>Icons</title>
<link rel="stylesheet" type="text/css" href="<?php echo SITEURL . '/theme/' . $core->theme;?>/cache/master_main.css">
<script type="text/javascript" src="../../../../assets/jquery.js"></script>
<style type="text/css">
body {
  font-family: "Open Sans", sans-serif;
  margin: 0px;
  padding: 1em;
  color: #999999;
  text-rendering: optimizeLegibility;
  font-weight: 400;
}
a {
  cursor: pointer;
  text-decoration: none;
  -webkit-transition: all .35s ease;
  -moz-transition: all .35s ease;
  -o-transition: all .35s ease;
  transition: all .35s ease;
  outline: none
}
a:hover,
a.hover,
a:focus {
  -webkit-transition: all .55s ease;
  -moz-transition: all .55s ease;
  -o-transition: all .35s ease;
  transition: all .55s ease;
  outline: none;
  text-decoration: none
}
#scroll-icons {
  text-align: center
}
#scroll-icons i.icon {
  color: #787878;
  cursor: pointer;
  box-shadow: 0 0 0 1px rgba(0,0,0,0.2);
  width: 2em;
  height: 2em;
  line-height: 2em;
  -webkit-transition: all .35s ease;
  -moz-transition: all .35s ease;
  -o-transition: all .35s ease;
  transition: all .35s ease;
  margin: .25em
}
#scroll-icons i:hover {
  background-color: #DBA766;
  color:#fff;
  -webkit-transition: all .55s ease;
  -moz-transition: all .55s ease;
  -o-transition: all .35s ease;
  transition: all .55s ease;
  outline: none;
  text-decoration: none
}
</style>
</head>
<body>
<div id="scroll-icons"> <?php print Core::iconList();?> </div>
<script type="text/javascript">
	$(document).ready(function () {
		$('#scroll-icons i').click(function () {
			var classname = $(this).attr('class').replace("big ", "");
			parent.oUtil.obj.insertHTML('<i class="' + classname + '"></i> &nbsp;');
		});
	});
</script>
</body>
</html>
<?php
  define("_VALID_PHP", true);
  require_once("../../../init.php");
  if (!$user->is_Admin())
    exit;
?>
<!DOCTYPE HTML>
<html>
<head>
<meta charset="utf-8">
<title>Images</title>
<link rel="stylesheet" type="text/css" href="<?php echo SITEURL . '/theme/' . $core->theme;?>/cache/master_main.css">
<script type="text/javascript" src="../../../../assets/jquery.js"></script>
<style type="text/css">
body {
  font-family: "Open Sans", sans-serif;
  margin: 0px;
  padding: 0;
  color: #999999;
  text-rendering: optimizeLegibility;
  font-weight: 400;
}
a {
  cursor: pointer;
  text-decoration: none;
  -webkit-transition: all .35s ease;
  -moz-transition: all .35s ease;
  -o-transition: all .35s ease;
  transition: all .35s ease;
  outline: none
}
a:hover,
a.hover,
a:focus {
  -webkit-transition: all .55s ease;
  -moz-transition: all .55s ease;
  -o-transition: all .35s ease;
  transition: all .55s ease;
  outline: none;
  text-decoration: none
}
.item {
  padding: 15px 30px 30px;
  border-bottom: #e7e7e7 1px solid;
  -webkit-box-shadow: 0 0 40px rgba(0, 0, 0, 0.1) inset;
  -moz-box-shadow: 0 0 40px rgba(0, 0, 0, 0.1) inset;
  box-shadow: 0 0 40px rgba(0, 0, 0, 0.1) inset;
}
</style>
</head>
<body>
<div class="veriasist-grid">
  <div class="item">
    <div class="row-fluid">
      <div class="span12">
        <h4>Single Image (Rounded)</h4>
      </div>
    </div>
    <div class="columns">
      <div class="screen-80" id="divImg1"> <img src="http://placehold.it/140x140" class="veriasist rounded image" alt="" />&nbsp; </div>
      <div class="screen-20">
        <a class="veriasist basic button" href="javascript:insertSnippet('divImg1')">Insert</a>
      </div>
    </div>
  </div>
  <div class="item">
    <div class="row-fluid">
      <div class="span12">
        <h4>Single Image (Circle)</h4>
      </div>
    </div>
    <div class="columns">
      <div class="screen-80" id="divImg2"> <img src="http://placehold.it/140x140" class="veriasist circular image" alt="" />&nbsp; </div>
      <div class="screen-20">
         <a class="veriasist basic button" href="javascript:insertSnippet('divImg2')">Insert</a>
      </div>
    </div>
  </div>
  <div class="item">
    <div class="row-fluid">
      <div class="span12">
        <h4>Basic Image</h4>
      </div>
    </div>
    <div class="columns">
      <div class="screen-80" id="divImg3"> <img src="http://placehold.it/140x140" class="veriasist image" alt="" />&nbsp; </div>
     <div class="screen-20">
        <a class="veriasist basic button" href="javascript:insertSnippet('divImg3')">Insert</a>
        
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
	function insertSnippet(id) {
		parent.oUtil.obj.insertHTML(document.getElementById(id).innerHTML);
	}
</script>
</body>
</html>
/**
 * NlsLightBox.js v.1.2
 * Copyright 2011, addobject.com. All Rights Reserved.
 * Author Jack Hermanto, www.addobject.com
 */
function NlsLightBox(id) {
    var me = this,
        ua = window.navigator.userAgent,
        isIE = (ua.indexOf("MSIE") >= 0),
        isIE7 = (ua.indexOf("MSIE 7.0") >= 0),
        isIE8 = (ua.indexOf("MSIE 8.0") >= 0),
        isIE9 = (ua.indexOf("MSIE 9.0") >= 0),
        isIE10 = (ua.indexOf("MSIE 10.0") >= 0),
        isIE6 = (!isIE7 && !isIE8 && !isIE9 && !isIE10 && isIE);
    IEBackCompat = (isIE && document.compatMode == "BackCompat");
    this.id = id;
    this.opts = {
        showOnLoaded: false,
        title: "",
        type: "iframe",
        center: true,
        centerOnResize: true,
        overlay: true,
        floatType: "fixed",
        adjX: 0,
        adjY: 0,
        scrAdjW: 0,
        scrAdjH: 0,
        zoom: false
    };
    this.gropts = [];
    this.rtopts = {};
    this.rt = {
        w: "400px",
        h: "250px",
        x: "10px",
        y: "10px",
        iCnt: null
    };
    this.loadConfig = function (o) {
        this.rtopts = o;
        for (var i in this.opts) {
            if (typeof (this.rtopts[i]) == 'undefined') this.rtopts[i] = this.opts[i]
        }
        this.rt.adjX = pInt(this.rtopts.adjX);
        this.rt.adjY = pInt(this.rtopts.adjY);
        if (document.all) {
            try {
                document.execCommand("BackgroundImageCache", false, true)
            } catch (e) {}
        }
    };
    this.show = function (o) {
        this.loadConfig(o);
        this.paint();
        var op = this.rtopts;
        this.setSize((op.width ? op.width : this.rt.w), (op.height ? op.height : this.rt.h), op.center);
        if (op.center) {
            this.center()
        } else {
            this.setPosition(pInt(op.left ? op.left : this.rt.x) + this.rt.adjX, pInt(op.top ? op.top : this.rt.y) + this.rt.adjY)
        }
    };
    this.paint = function () {
        var db = document.body;
        var de = document.documentElement;
        var v = NlsLightBox.$("box_overlay");
        if (!v) {
            v = NlsLightBox.$crtElement("div", {
                id: "box_overlay",
                "className": "box_overlay"
            }, {
                display: "none",
                position: (isIE6 ? "absolute" : "fixed")
            });
            db.appendChild(v)
        }
        var ep = "fixed";
        if (this.rtopts.floatType == "fixed") {
            if (isIE6) {
                ep = "absolute";
                this.rtopts.floatType = "anim"
            }
        } else {
            ep = "absolute"
        } if (!NlsLightBox.$(me.id)) {
            var ps = isIE6 || IEBackCompat ? "absolute" : "fixed";
            var g = NlsLightBox.$crtElement("div", {
                id: "dd$" + me.id
            }, {
                display: "none",
                position: ps,
                "z-index": 99999,
                "background-color": "#ffffff",
                filter: "alpha(opacity=25)",
                opacity: 0.25,
                "-moz-opacity": 0.25,
                border: "#999999 1px solid"
            });
            var e = NlsLightBox.$crtElement("div", {
                id: me.id,
                "className": "box_container"
            }, {
                display: "none",
                position: ep
            });
            var s = "<div id='@id$content_area' class='content_area' style='position:relative'><div id='@id$box_loading' class='box_loading' style='position:absolute;display:none;width:100%;height:100%'></div><div id='@id$box_content' class='box_content'></div></div><div id='@id$box_close' class='box_close' onclick='NlsLightBox.objs.@id.close()'><i class=\"icon close\"></i></div><div id='@id$box_title' class='box_title' onmousedown='NlsLightBox.$mouseDown(event, \"@id\")'><div id='@id$title_text' class='title_text'></div></div><a href='#' id='@id$box_prev' class='box_prev' onclick='NlsLightBox.objs.@id.groupPrev();return false;'></a><a href='#' id='@id$box_next' class='box_next' onclick='NlsLightBox.objs.@id.groupNext();return false;'></a>";
            e.innerHTML = s.replace(/@id/ig, this.id);
            var prn = this.rtopts.parent;
            if (!prn) prn = document.forms[0];
            if (!prn) prn = document.body;
            prn.appendChild(g);
            prn.appendChild(e);
            this.rt.lb = e;
            var p = NlsLightBox.$crtElement("div", {
                id: me.id + "$box_progress",
                "className": "box_loading"
            }, {
                visibility: "hidden",
                position: "absolute"
            });
            document.body.appendChild(p);
            this.rt.pr = p;
            this.rt.lp = NlsLightBox.$(this.id + "$box_loading");
            this.rt.ca = NlsLightBox.$(this.id + "$content_area");
            this.rt.bc = NlsLightBox.$(this.id + "$box_content");
            this.rt.tl = NlsLightBox.$(this.id + "$box_title");
            this.rt.cl = NlsLightBox.$(this.id + "$box_close");
            this.rt.tc = NlsLightBox.$(this.id + "$title_text");
            this.rt.pv = NlsLightBox.$(this.id + "$box_prev");
            this.rt.nx = NlsLightBox.$(this.id + "$box_next");
            var f = function () {
                if (me.rtopts.centerOnResize) {
                    me.center()
                }
                me.updateOverlay()
            };
            var kp = function (event) {
                if (event.keyCode == 27) {
                    NlsLightBox.closeAll()
                }
            };
            if (window.attachEvent) {
                window.attachEvent("onresize", f);
                window.attachEvent("onkeydown", kp)
            }
            if (window.addEventListener) {
                window.addEventListener("resize", f, true);
                window.addEventListener("keydown", kp, false)
            }
            if (this.rtopts.floatType == "anim") {
                if (NlsAnimation) {
                    var an = new NlsAnimation();
                    var op = {
                        duration: 500,
                        to: function () {
                            var s = NlsLightBox.getScrollXY();
                            return {
                                left: me.rt.cx + s.x + "px",
                                top: me.rt.cy + s.y + "px"
                            }
                        },
                        onComplete: function () {
                            setTimeout(function () {
                                an.move(me.rt.lb, op)
                            }, 10)
                        }
                    };
                    var f = function () {
                        an.move(me.rt.lb, op)
                    };
                    if (window.attachEvent) window.attachEvent("onscroll", f);
                    else if (window.addEventListener) window.addEventListener("scroll", f, true)
                }
            }
            if (isIE6) var dvs = me.rt.lb.getElementsByTagName("div"),
                rx = /\.png/gi,
                ph, rp;
            if (dvs)
                for (var i = 0; i < dvs.length; i++) {
                    ph = dvs[i].currentStyle.backgroundImage;
                    if (ph.match(rx)) {
                        ph = ph.split("\"")[1];
                        rp = (dvs[i].currentStyle.backgroundRepeat == "no-repeat") ? "crop" : "scale";
                        dvs[i].style.filter = "progid:DXImageTransform.Microsoft.AlphaImageLoader(src='" + ph + "',sizingMethod='" + rp + "')";
                        dvs[i].style.backgroundImage = 'none'
                    }
                }
        } else {
            if (this.rtopts.parent) {
                this.rtopts.parent.appendChild(this.rt.lb)
            }
        } if (this.rtopts.titleBar == false) {
            this.rt.tl.style.display = "none"
        }
        if (this.rtopts.closeButton == false) {
            this.rt.cl.style.display = "none"
        }
        if (this.rt.iCnt != null) {
            NlsLightBox.moveChilds(this.rt.bc, this.rt.iCnt);
            this.rt.iCnt = null
        }
        this.rt.bc.innerHTML = "";
        this.rt.bc.style.height = "";
        this.rt.pv.style.display = "none";
        this.rt.nx.style.display = "none";
        this.rt.tc.innerHTML = (this.rtopts.title == "" ? this.rtopts.url : this.rtopts.title)
    };
    this.load = function () {
        switch (this.rtopts.type) {
        case "iframe":
            this.$openIframe(this.rtopts);
            break;
        case "ajax":
            this.$openAJAX(this.rtopts);
            break;
        case "image":
            this.$openImage(this.rtopts);
            break;
        case "inline":
            this.$openInline(this.rtopts);
            break;
        default:
        }
    };
    this.setPosition = function (x, y, abs) {
        var rt = this.rt,
            s = {
                x: 0,
                y: 0
            };
        if (this.rtopts.floatType != "fixed") {
            s = NlsLightBox.getScrollXY()
        }
        rt.cx = x - (abs ? s.x : 0);
        rt.cy = y - (abs ? s.y : 0);
        rt.x = x + (abs ? 0 : s.x);
        rt.y = y + (abs ? 0 : s.y);
        rt.lb.style.left = rt.x + "px";
        rt.lb.style.top = rt.y + "px"
    };
    this.setSize = function (w, h, center) {
        var rt = this.rt;
        rt.w = pInt(w);
        rt.h = pInt(h);
        rt.lb.style.width = rt.w + "px";
        rt.lb.style.height = rt.h + "px";
        if (center == true) this.center()
    };
    this.center = function () {
        var p = NlsLightBox.getCenterXY(this.rt.w, this.rt.h);
        this.setPosition(p.x + this.rt.adjX, p.y + this.rt.adjY)
    };
    this.setTitle = function (t) {
        if (this.rt.tc) this.rt.tc.innerHTML = t;
        this.rtopts.title = t
    };
    this.open = function (o) {
        this.show(o);
        var ro = this.rtopts;
        if (!ro.showOnLoaded) {
            if (!ro.zoom || this.rt.lb.style.display != "none") {
                this.load()
            } else {
                this.$zoom({
                    srcObj: ro.srcObj,
                    to: {
                        left: me.rt.x,
                        top: me.rt.y,
                        width: me.rt.w,
                        height: me.rt.h
                    },
                    onComplete: function () {
                        me.load()
                    }
                })
            }
            this.rt.lb.style.display = "block"
        } else {
            this.load()
        }
        this.showProgress();
        if (ro.overlay && !ro.zoom) this.showOverlay()
    };
    this.close = function () {
        if (this.rt.lb.style.display != "none") {
            if (!this.rtopts.onClose || this.rtopts.onClose(this) != false) {
                if (this.rtopts.zoom) {
                    this.$zoom({
                        srcObj: me.rtopts.srcObj,
                        type: "out",
                        to: NlsLightBox.$PD(me.rtopts.srcObj, (this.rtopts.floatType != "fixed")),
                        onComplete: function () {
                            me.$close()
                        }
                    })
                } else {
                    this.$close()
                }
            }
        }
    };
    this.$close = function () {
        this.rt.lb.style.display = "none";
        if (NlsAnimation) NlsAnimation.setOpacity(me.rt.ca, 100);
        if (this.rt.iCnt != null) {
            NlsLightBox.moveChilds(this.rt.bc, this.rt.iCnt);
            this.rt.iCnt = null
        } else {
            this.rt.bc.innerHTML = ""
        } if (this.rtopts.overlay) this.hideOverlay();
        if (this.rtopts.type == "iframe") {
            try {
                var frmWin = this.rt.bc.childNodes[0].contentWindow;
                if (frmWin && frmWin.bodyOnUnload) frmWin.bodyOnUnload()
            } catch (e) {}
        }
    };
    this.groupOpen = function (g, o, p) {
        for (var i = 0; i < g.length; i++) {
            if (o)
                for (var j in o) {
                    if (typeof (g[i][j]) == 'undefined') g[i][j] = o[j]
                }
            g[i].group = true
        }
        this.rt.pnt = (p ? p : 0);
        this.gropts = g;
        this.open(this.gropts[this.rt.pnt])
    };
    this.groupNext = function () {
        if (this.groupHasNext()) this.rt.pnt++;
        this.open(this.gropts[this.rt.pnt])
    };
    this.groupPrev = function () {
        if (this.groupHasPrev()) this.rt.pnt--;
        this.open(this.gropts[this.rt.pnt])
    };
    this.groupHasPrev = function () {
        return (this.rtopts.group && this.rt.pnt > 0)
    };
    this.groupHasNext = function () {
        return (this.rtopts.group && this.rt.pnt < this.gropts.length - 1)
    };
    this.showProgress = function () {
        if (this.rtopts.showOnLoaded) {
            var p = this.rt.pr;
            var c = NlsLightBox.getCenterXY(p.offsetWidth, p.offsetHeight),
                s = {
                    x: 0,
                    y: 0
                };
            if (isIE6) {
                s = NlsLightBox.getScrollXY()
            } else {
                p.style.position = "fixed"
            }
            p.style.left = c.x + s.x + "px";
            p.style.top = c.y + s.y + "px";
            p.style.visibility = "visible"
        } else this.rt.lp.style.display = "block"
    };
    this.hideProgress = function () {
        this.rt.lp.style.display = "none";
        this.rt.pr.style.visibility = "hidden"
    };
    this.showOverlay = function () {
        this.updateOverlay(true);
        NlsLightBox.$("box_overlay").style.display = "block"
    };
    this.updateOverlay = function (force) {
        var ov = NlsLightBox.$("box_overlay");
        if (ov && (ov.style.display == "block" || force)) {
            var cl = NlsLightBox.getClientSize();
            if (isIE6) {
                var db = document.body,
                    de = document.documentElement,
                    w, h;
                w = Math.min(Math.max(db.scrollWidth, de.scrollWidth), Math.max(db.offsetWidth, de.offsetWidth));
                var mf = ((de.scrollHeight < de.offsetHeight) || (db.scrollHeight < db.offsetHeight)) ? Math.min : Math.max;
                h = mf(Math.max(db.scrollHeight, de.scrollHeight), Math.max(db.offsetHeight, de.offsetHeight));
                cl.w = Math.max(cl.w, w);
                cl.h = Math.max(cl.h, h)
            }
            ov.style.width = cl.w + "px";
            ov.style.height = cl.h + "px"
        }
    };
    this.hideOverlay = function () {
        NlsLightBox.$("box_overlay").style.display = "none"
    };
    this.onload = function (e) {
        return true
    };
    this.register = function (ids, o) {
        var elIds = [],
            el;
        if (ids instanceof Array) {
            elIds = ids
        } else {
            elIds[0] = ids
        }
        for (var i = 0; i < elIds.length; i++) {
            el = NlsLightBox.$(elIds[i]);
            if (el) el.onclick = function () {
                o.url = this.href;
                if (!o.title && this.title) o.title = this.title;
                if (!o.srcObj) o.srcObj = gsrcObj(this);
                me.open(o);
                return false
            }
        }
        return this
    };
    this.registerGroup = function (ids, o) {
        var g = [];
        for (var i = 0; i < ids.length; i++) {
            el = NlsLightBox.$(ids[i]);
            if (el) {
                g[i] = {
                    url: el.href,
                    srcObj: gsrcObj(el)
                };
                if (el.title) g[i].title = el.title;
                el.gpnt = i;
                el.onclick = function () {
                    me.groupOpen(g, o, this.gpnt);
                    return false
                }
            }
        }
        return this
    };
    this.$openIframe = function (o) {
        if (o.url != "") {
            var bc = this.rt.bc;
            bc.style.height = "100%";
            var fr = document.createElement("iframe");
            fr.style.width = "100%";
            fr.style.height = "100%";
            fr.style.display = "block";
            fr.frameBorder = 0;
            bc.appendChild(fr);
            fr.onload = function () {
                me.$onloadCallback()
            };
            setTimeout(function () {
                if (fr.attachEvent) fr.attachEvent('onload', function () {
                    me.$onloadCallback()
                });
                fr.src = o.url
            }, 50)
        }
    };
    this.$openAJAX = function (o) {
        if (o.url != "") {
            var req = NlsLightBox.createRequest();
            req.open("get", o.url, true);
            req.onreadystatechange = function () {
                if (req.readyState == 4) {
                    if (req.status == 200 || req.status == 304) {
                        var bc = me.rt.bc;
                        bc.innerHTML = req.responseText;
                        setTimeout(function () {
                            me.$onloadCallback()
                        }, 50)
                    }
                }
            };
            req.send(null)
        }
    };
    this.$openImage = function (o) {
        if (o.url != "") {
            var img = document.createElement("img");
            this.rt.img = img;
            img.onload = function () {
                me.$onloadCallback()
            };
            setTimeout(function () {
                img.src = o.url
            }, 50)
        }
    };
    this.$openInline = function (o) {
        if (o.url != "") {
            var cntId = o.url.split("#"),
                cnt = NlsLightBox.$(cntId[1]);
            this.rt.iCnt = cnt;
            NlsLightBox.moveChilds(cnt, me.rt.bc);
            setTimeout(function () {
                me.$onloadCallback()
            }, 50)
        }
    };
    this.$onloadCallback = function () {
        this.hideProgress();
        if (this.groupHasPrev()) {
            this.rt.pv.href = this.gropts[this.rt.pnt - 1].url;
            this.rt.pv.style.display = "block"
        }
        if (this.groupHasNext()) {
            this.rt.nx.href = this.gropts[this.rt.pnt + 1].url;
            this.rt.nx.style.display = "block"
        }
        if (!this.rtopts.zoom && this.rtopts.showOnLoaded == true) {
            this.rt.lb.style.display = "block"
        }
        var bc = this.rt.bc;
        switch (this.rtopts.type) {
        case "iframe":
            var ttl = "";
            try {
                ttl = bc.childNodes[0].contentWindow.document.title
            } catch (e) {};
            if (ttl != "") this.setTitle(ttl);
            var f = function (event) {
                if (event.keyCode == 27) {
                    NlsLightBox.closeAll()
                }
            };
            var w = bc.childNodes[0].contentWindow;
            if (w.addEventListener) w.addEventListener("keydown", f, false);
            if (w.attachEvent) w.attachEvent("onkeydown", f);
            break;
        case "ajax":
            break;
        case "inline":
            break;
        case "image":
            NlsLightBox.fitToClient(this.rt.img, {
                avW: this.rtopts.scrAdjW,
                avH: this.rtopts.scrAdjH
            });
            if (this.rt.lb.style.display != "none" && NlsAnimation && (new NlsAnimation()).resize(this.rt.lb, {
                delay: 200,
                duration: 700,
                to: {
                    width: this.rt.img.width,
                    height: this.rt.img.height
                },
                onAnimate: function (p) {
                    me.setSize(p.width, p.height + "px", me.rtopts.center);
                    return false
                },
                onComplete: function () {
                    bc.appendChild(me.rt.img);
                    ie8(me.rt.lb)
                },
                onAbort: function () {
                    bc.appendChild(me.rt.img);
                    ie8(me.rt.lb)
                }
            })) {} else {
                this.setSize(this.rt.img.width, this.rt.img.height, this.rtopts.center);
                bc.appendChild(me.rt.img)
            }
            this.rt.pv.style.height = this.rt.lb.style.height;
            this.rt.nx.style.height = this.rt.lb.style.height;
            break
        }
        if (this.rtopts.zoom && this.rtopts.showOnLoaded == true && this.rt.lb.style.display == "none") {
            this.$zoom({
                srcObj: me.rtopts.srcObj,
                to: {
                    left: this.rt.x,
                    top: this.rt.y,
                    width: this.rt.w,
                    height: this.rt.h
                }
            })
        }
        this.onload()
    };
    this.$zoom = function (o) {
        this.rt.lb.style.overflow = "hidden";
        this.rt.ca.style.overflow = "hidden";
        if (o.type != "out") {
            var ip = NlsLightBox.$PD(o.srcObj);
            this.setSize(ip.width, ip.height, false);
            this.setPosition(ip.left, ip.top)
        }
        this.rt.lb.style.display = "block";
        (new NlsAnimation()).zoom(this.rt.lb, {
            duration: 500,
            to: o.to,
            type: o.type ? o.type : "in",
            onAnimate: function (p) {
                me.setSize(p.width, p.height, false);
                me.setPosition(p.left, p.top, true);
                NlsAnimation.setOpacity(me.rt.ca, p.$opa)
            },
            onComplete: function () {
                me.rt.lb.style.overflow = "";
                me.rt.ca.style.overflow = "auto";
                if (o.onComplete) o.onComplete()
            }
        })
    };
    NlsLightBox.$mouseDown = function (ev, elId) {
        var d = document;
        d.onmousemove = function (e) {
            NlsLightBox.$startDrag(e ? e : ev)
        };
        d.onmouseup = function (e) {
            NlsLightBox.$endDrag(e ? e : ev)
        };
        d.onselectstart = function () {
            return false
        };
        d.onmousedown = function () {
            return false
        };
        d.ondragstart = function () {
            return false
        };
        NlsLightBox.trgElm = d.getElementById(elId);
        NlsLightBox.gstElm = d.getElementById("dd$" + elId);
        NlsLightBox.gstElm.style.top = NlsLightBox.trgElm.style.top;
        NlsLightBox.gstElm.style.left = NlsLightBox.trgElm.style.left;
        NlsLightBox.gstElm.style.width = NlsLightBox.trgElm.style.width;
        NlsLightBox.gstElm.style.height = NlsLightBox.trgElm.style.height;
        NlsLightBox.gstElm.style.display = "block";
        NlsLightBox.gstElm.style.zIndex = 999999;
        NlsLightBox.posDif = {
            x: ev.clientX - parseInt(NlsLightBox.trgElm.style.left, 10),
            y: ev.clientY - parseInt(NlsLightBox.trgElm.style.top, 10)
        }
    };
    NlsLightBox.$startDrag = function (ev) {
        NlsLightBox.gstElm.style.left = (ev.clientX - NlsLightBox.posDif.x) + "px";
        NlsLightBox.gstElm.style.top = (ev.clientY - NlsLightBox.posDif.y) + "px";
        NlsLightBox.trgElm.style.left = (ev.clientX - NlsLightBox.posDif.x) + "px";
        NlsLightBox.trgElm.style.top = (ev.clientY - NlsLightBox.posDif.y) + "px"
    };
    NlsLightBox.$endDrag = function (ev) {
        NlsLightBox.gstElm.style.display = "none";
        document.onmousemove = null;
        document.onmouseup = null;
        document.onmousedown = function () {
            return true
        };
        document.onselectstart = function () {
            return true
        };
        document.onselectstart = function () {
            return true
        }
    };
    NlsLightBox.getCenterXY = function (wd, hg) {
        var c = NlsLightBox.getClientSize();
        var px = pInt(wd),
            py = pInt(hg);
        px = (isNaN(px) ? 0 : (px > c.w ? c.w : px));
        py = (isNaN(py) ? 0 : (py > c.h ? c.h : py));
        return {
            x: (c.w - px) / 2,
            y: (c.h - py) / 2
        }
    };
    NlsLightBox.getClientSize = function () {
        return {
            w: window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth,
            h: window.innerHeight || document.documentElement.clientHeight || document.body.clientHeight
        }
    };
    NlsLightBox.getScrollXY = function () {
        return {
            x: window.scrollX || document.body.scrollLeft || document.documentElement.scrollLeft,
            y: window.scrollY || document.body.scrollTop || document.documentElement.scrollTop
        }
    };
    NlsLightBox.fitToClient = function (img, o) {
        var w = img.width,
            h = img.height;
        var c = NlsLightBox.getClientSize();
        c.h = c.h + o.avH;
        c.w = c.w + o.avW;
        if (w > c.w) {
            img.width = c.w;
            img.height = c.w * h / w
        }
        if (h > c.h) {
            img.height = c.h;
            img.width = c.h * w / h
        }
        return img
    };
    NlsLightBox.createRequest = function () {
        if (typeof XMLHttpRequest != "undefined") {
            return (new XMLHttpRequest())
        } else {
            var arrObj = ["MSXML2.XMLHttp.5.0", "MSXML2.XMLHttp.4.0", "MSXML2.XMLHttp.3.0", "MSXML2.XMLHttp", "Microsoft.XMHttp"];
            var req = null;
            for (var i = 0; i < arrObj.length; i++) {
                try {
                    req = new ActiveXObject(arrObj[i]);
                    return req
                } catch (e) {}
            }
        }
    };
    NlsLightBox.moveChilds = function (f, t) {
        var inps = f.getElementsByTagName("input"),
            map = {};
        for (var i = 0; i < inps.length; i++) {
            if (inps[i].type == "radio" && inps[i].checked) {
                map[inps[i].name] = inps[i].value
            }
        }
        for (var i = 0; i < f.childNodes.length;) {
            t.appendChild(f.childNodes[i])
        }
        inps = t.getElementsByTagName("input");
        for (var i = 0; i < inps.length; i++) {
            if (inps[i].type == "radio") {
                inps[i].checked = (map[inps[i].name] == inps[i].value)
            }
        }
    };
    NlsLightBox.$ = function (id) {
        if (document.getElementById) return document.getElementById(id);
        else if (document.all) return document.all(id)
    };
    NlsLightBox.$PD = function (e, abs) {
        var p = e,
            x = y = w = h = 0,
            s = {
                x: 0,
                y: 0
            };
        while (p) {
            x += p.offsetLeft;
            y += p.offsetTop;
            p = p.offsetParent
        }
        w += e.offsetWidth;
        h += e.offsetHeight;
        if (!abs) {
            s = NlsLightBox.getScrollXY()
        }
        return {
            left: x - s.x,
            top: y - s.y,
            width: w,
            height: h
        }
    };
    NlsLightBox.$crtElement = function (tag, p, s) {
        var e = document.createElement(tag);
        for (var i in p) {
            e[i] = p[i]
        }
        for (var i in s) {
            e.style[i] = s[i]
        }
        return e
    };

    function ie8(lb) {
        if (!isIE8) return;
        var w = pInt(lb.style.width) + 1;
        lb.style.width = w + "px";
        lb.style.width = (w - 1) + "px"
    }

    function pInt(v) {
        return parseInt(v, 10)
    };

    function gsrcObj(e) {
        var c = e.childNodes[0];
        if (c && c.nodeType == 1) {
            if (c.tagName == "IMG") return c
        }
        return e
    };
    NlsLightBox.objs[this.id] = this;
    return this
};
NlsLightBox.closeAll = function () {
    for (var it in NlsLightBox.objs) {
        NlsLightBox.objs[it].close()
    }
};
NlsLightBox.objs = [];
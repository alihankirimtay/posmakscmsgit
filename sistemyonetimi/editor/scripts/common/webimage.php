﻿<?php
  define("_VALID_PHP", true);
  
  require_once("../../../init.php");
  if (!$user->is_Admin())
    exit;
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <link href="../../../assets/cache/master_main.css" rel="stylesheet" type="text/css">
    <script type="text/javascript" src="../../../../assets/jquery.js"></script>
    <script type="text/javascript" src="../../../../assets/jquery-ui.js"></script>
    <script type="text/javascript" src="../../../../assets/global.js"></script>
    <link href="../style/editor.css" rel="stylesheet" type="text/css">
    <script>
        var sLangDir = parent.oUtil.langDir;
        document.write("<scr" + "ipt src='language/" + sLangDir + "/webimage.js'></scr" + "ipt>");
    </script>
    <script>writeTitle()</script>
    <script src="../../../assets/js/jquery.fileupload.js"></script>
    <script src="../../../assets/js/jquery.iframe-transport.js"></script>
    <style type="text/css">
#inpImgURL,
#inpTitle,
#inpURL,
#inpKeywords {

}
.select {
  font-size: 10pt;
  padding: 3px;
  border: 1px solid #adadad;
  background: #ffffff;
}
</style>
    <script src="common.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">

		var UA = navigator.userAgent.toLowerCase();
		var isIE = (UA.indexOf('msie') >= 0) ? true : false;
		//if(!isIE) { //ie11
		//	isIE = (UA.indexOf('trident') >= 0) ? true : false;
		//}

        function getQueryString() {
            var result = {}, queryString = location.search.substring(1), re = /([^&=]+)=([^&]*)/g, m;

            while (m = re.exec(queryString)) {
                result[decodeURIComponent(m[1])] = decodeURIComponent(m[2]);
            }
            return result;
        }

function formatFileSize(bytes) {
	if (typeof bytes !== 'number') {
		return '';
	}

	if (bytes >= 1000000000) {
		return (bytes / 1000000000).toFixed(2) + ' GB';
	}

	if (bytes >= 1000000) {
		return (bytes / 1000000).toFixed(2) + ' MB';
	}

	return (bytes / 1000).toFixed(2) + ' KB';
}

        $(document).ready(function ($) {
            I_RealtimeImage();
            parent.oUtil.onSelectionChanged = new Function("I_RealtimeImage()");

            /*  ASSET MANAGER HERE */
			/*
            if (oUtil.obj.fileBrowser != "") {
                $("#tab1").css("display", "block");
                $("#frameFiles").attr("src", oUtil.obj.fileBrowser + "?img=yes");
            }
            else {
                $("#spanSaveAsNew").css("display", "none");
            }
*/
            if (parent.oUtil.obj.enableLightbox == false) {
                $("#divFlickrLarger").css("visibility", "hidden");
            }

            loadTxt(); /* language */
			/*
            if (parent.oUtil.obj.enableFlickr) {
                search(true);
            } else {
                if (self.oUtil.obj.fileBrowser != "") {
                    tabClick(1);
                    $("#tab0").css("display", "none");
                    $("#tab1").css("margin-left", "22px");
                }
                else {
                    tabClick(2);
                    $("#tab0").css("display", "none");
                    $("#tab2").css("margin-left", "22px");
                }
            }*/

            if(!parent.oUtil.obj.enableImageStyles) {
            	$("#tab2").css("display", "none");
            }
        });


        function setSelectedIndex(s, v) {
            for (var i = 0; i < s.options.length; i++) {
                if (s.options[i].value == v) {
                    s.options[i].selected = true;
                    return;
                }
            }
        }

        function I_RealtimeImage() {
            if (parent.oUtil + '' == 'undefined') return;

            var oEditor = parent.oUtil.oEditor;

            var obj = parent.oUtil.obj;
            //obj.setFocus();

            var src;

            /* Source */
            document.getElementById('inpImgURL').value = "";

            /* Alt/Title */
            document.getElementById('inpTitle').value = "";

            /* Align */
            document.getElementsByName('optAlign')[0].selected = true;


            /* Dimension */
            document.getElementById("inpWidth").value = "";
            document.getElementById("inpHeight").value = "";

            /* OPEN LARGER IMAGE IN A LIGHTBOX */
            document.getElementById('chkOpenLarger').checked = false;

            /* HREF */
            document.getElementById('inpURL').value = "http://";

            /* OPEN IN A NEW WINDOW */
            document.getElementById('chkNewWindow').checked = false;

            /* Button */
            document.getElementById('btnInsert').value = getTxt("insert");

            var oSel;
            var oEl;
            if (isIE) {
                oSel = oEditor.document.selection.createRange();
                if (oSel.parentElement) oEl = GetElement(oSel.parentElement(), "IMG");
                else oEl = GetElement(oSel.item(0), "IMG");
            }
            else {
                if (!oEditor.getSelection()) return;
                oSel = oEditor.getSelection();
                oEl = GetElement(parent.getSelectedElement(oSel), "IMG");
            }

            if (oEl) {
                if (oEl.nodeName == "IMG") {

                    if (isIE) {
                        try {
                            var range = oEditor.document.body.createTextRange();
                            range.moveToElementText(oEl);
                            range.select();
                        } catch (e) { return; }
                    }
                    else {
                        /*var range = oEditor.document.createRange();
                        range.selectNode(oEl);
                        oSel.removeAllRanges();
                        oSel.addRange(range);*/
                        var range = oEditor.document.createRange();
                        range.selectNodeContents(oEl);
                        oSel.addRange(range);
                    }

                    /* Source */
                    src = oEl.getAttribute("SRC");
                    document.getElementById('inpImgURL').value = src;
                    //loadImageIntoCanvas(src);

                    /* Title */
                    if (oEl.getAttribute("ALT") != null) document.getElementById('inpTitle').value = oEl.getAttribute("ALT");

                    /* Align */
                    if (oEl.style.cssFloat == "") document.getElementsByName('optAlign')[0].selected = true;
                    if (oEl.style.cssFloat == "left" || oEl.style.float == "left") document.getElementsByName('optAlign')[1].selected = true;
                    if (oEl.style.cssFloat == "right" || oEl.style.float == "right") document.getElementsByName('optAlign')[2].selected = true;

                    /* Margin */
                    setSelectedIndex(document.getElementById("selMarginTop"), parseInt(oEl.style.marginTop));
                    setSelectedIndex(document.getElementById("selMarginRight"), parseInt(oEl.style.marginRight));
                    setSelectedIndex(document.getElementById("selMarginBottom"), parseInt(oEl.style.marginBottom));
                    setSelectedIndex(document.getElementById("selMarginLeft"), parseInt(oEl.style.marginLeft));

                    if (oEl.parentNode.nodeName == "A" && oEl.parentNode.childNodes.length == 1) {
                        var oLink = oEl.parentNode;

                        /* Align */
                        if (oLink.style.cssFloat == "") document.getElementsByName('optAlign')[0].selected = true;
                        if (oLink.style.cssFloat == "left" || oLink.style.float == "left") document.getElementsByName('optAlign')[1].selected = true;
                        if (oLink.style.cssFloat == "right" || oLink.style.float == "right") document.getElementsByName('optAlign')[2].selected = true;

                        /* OPEN LARGER IMAGE IN A LIGHTBOX */
                        if (oLink.getAttribute("rel") == "lightbox") {
                            document.getElementById('chkOpenLarger').checked = true;
                        }

                        /* HREF */
                        document.getElementById('inpURL').value = oLink.getAttribute("href")

                        /* OPEN IN A NEW WINDOW */
                        if (oLink.getAttribute("target") == "_blank") {
                            document.getElementById('chkNewWindow').checked = true;
                        }

                    }

                    /* Button */
                    document.getElementById('btnInsert').value = getTxt("change");

                }
            }
        }
		
        function changeSize() {
            var sURL = document.getElementById("inpImgURL").value;
            if (sURL == "") return;
            var size;
            var rdoSizes = document.getElementsByName("rdoSize")
            for (i = 0; i < rdoSizes.length; i++) if (rdoSizes[i].checked == true) size = rdoSizes[i].value;

            var ss = document.getElementById("inpImgURL").value.substr(0, document.getElementById("inpImgURL").value.length - 5);
            document.getElementById("inpImgURL").value = ss + size + '.jpg';
        }


        function I_Change(oEl) {
            var obj = parent.oUtil.obj;
            obj.setFocus();

            var oEditor = parent.oUtil.oEditor;

            var oSel;
            if (isIE) {
                oSel = oEditor.document.selection.createRange();
            }
            else {
                oSel = oEditor.getSelection();
            }

            /* Source */
            var src = document.getElementById('inpImgURL').value;
            oEl.setAttribute("SRC", src);

            /* Title */
            oEl.setAttribute("ALT", document.getElementById('inpTitle').value);

            /* Margin */
            var nMarginTop = document.getElementById("selMarginTop").value;
            var nMarginRight = document.getElementById("selMarginRight").value;
            var nMarginBottom = document.getElementById("selMarginBottom").value;
            var nMarginLeft = document.getElementById("selMarginLeft").value;
            if (document.getElementById("selAlign").value == "left")
                oEl.style.cssText = oEl.style.cssText + ";float:left;margin-top:" + nMarginTop + "px;margin-right:" + nMarginRight + "px;margin-bottom:" + nMarginBottom + "px;margin-left:" + nMarginLeft + "px;";
            else if (document.getElementById("selAlign").value == "right")
                oEl.style.cssText = oEl.style.cssText + ";float:right;margin-top:" + nMarginTop + "px;margin-right:" + nMarginRight + "px;margin-bottom:" + nMarginBottom + "px;margin-left:" + nMarginLeft + "px;";
            else {
                oEl.style.cssText = oEl.style.cssText + ";float:none;margin-top:" + nMarginTop + "px;margin-right:" + nMarginRight + "px;margin-bottom:" + nMarginBottom + "px;margin-left:" + nMarginLeft + "px;";
            }

            var bAbs = getQueryString()["abs"];
            if (bAbs == "true")
                if (document.getElementById("selAlign").value == "left") {
                    oEl.align = "left";
                    oEl.hspace = "7";
                    oEl.vspace = "7";
                }
                else if (document.getElementById("selAlign").value == "right") {
                    oEl.align = "right";
                    oEl.hspace = "7";
                    oEl.vspace = "7";
                }
                else {
                    oEl.hspace = "7";
                    oEl.vspace = "7";
                }

            /* Dimension */
            if (document.getElementById("inpWidth").value != "") {
                oEl.style.cssText = oEl.style.cssText + ";width:" + document.getElementById("inpWidth").value + "px;";
            }
            if (document.getElementById("inpHeight").value != "") {
                oEl.style.cssText = oEl.style.cssText + ";height:" + document.getElementById("inpHeight").value + "px;";
            }


            /* Link URL */
            var sLinkURL = "";
            if (document.getElementById("inpImgURL").value.indexOf("flickr.com") != -1 && document.getElementById("chkOpenLarger").checked) {
                var ss = document.getElementById("inpImgURL").value.substr(0, document.getElementById("inpImgURL").value.length - 5);
                sLinkURL = ss + 'z.jpg';
            }
            else {
                sLinkURL = document.getElementById("inpURL").value;
                if (sLinkURL == "http://") sLinkURL = "";
            }

            if (oEl.parentNode.nodeName == "A" && oEl.parentNode.childNodes.length == 1) {

                var oLink = oEl.parentNode;

                if (sLinkURL == "") {
                    oLink.setAttribute("style", "");
                    oEditor.document.execCommand("unlink", false, null);
                    obj.cleanEmptySpan()
                    return;
                }

                /* Title */
                oLink.setAttribute("title", document.getElementById('inpTitle').value);

                /* Align */
                oLink.style.cssFloat = document.getElementById('selAlign').value; /*TODO: Di IE7 jadi cssFloat:left/right, tp tdk problem*/

                /* OPEN LARGER IMAGE IN A LIGHTBOX */
                if (document.getElementById('chkOpenLarger').checked) {
                    oLink.setAttribute("rel", "lightbox");
                    oLink.setAttribute("target", "");
                }

                /* HREF */
                oLink.setAttribute("href", document.getElementById('inpURL').value);

                /* OPEN IN A NEW WINDOW */
                if (document.getElementById('chkNewWindow').checked) {
                    oLink.setAttribute("target", "_blank");
                    oLink.setAttribute("rel", "");
                }
            }
            else {

                if (sLinkURL == "") return;

                /* Link Title */
                var sTitle = document.getElementById("inpTitle").value;

                /* Link Css Style */
                var sCssStyle = "";
                var nMarginTop = document.getElementById("selMarginTop").value;
                var nMarginRight = document.getElementById("selMarginRight").value;
                var nMarginBottom = document.getElementById("selMarginBottom").value;
                var nMarginLeft = document.getElementById("selMarginLeft").value;
                if (document.getElementById("selAlign").value == "left")
                    sCssStyle = "float:left;margin-top:" + nMarginTop + "px;margin-right:" + nMarginRight + "px;margin-bottom:" + nMarginBottom + "px;margin-left:" + nMarginLeft + "px;";
                else if (document.getElementById("selAlign").value == "right")
                    sCssStyle = "float:right;margin-top:" + nMarginTop + "px;margin-right:" + nMarginRight + "px;margin-bottom:" + nMarginBottom + "px;margin-left:" + nMarginLeft + "px;";
                else
                    sCssStyle = "margin-top:" + nMarginTop + "px;margin-right:" + nMarginRight + "px;margin-bottom:" + nMarginBottom + "px;margin-left:" + nMarginLeft + "px;";

                if (navigator.userAgent.indexOf('Safari') != -1) {
                    var range = oSel.getRangeAt(0);
                    var newA = oEditor.document.createElement("A");
                    newA.href = sLinkURL;
                    range.selectNode(oEl);
                    range.surroundContents(newA);

                    range.selectNodeContents(oEl);
                    range.setEndAfter(oEl);

                    oSel.removeAllRanges();
                    oSel.addRange(range);
                }
                else {
                    oEditor.document.execCommand("CreateLink", false, sLinkURL); //tdk jalan di SAFARI
                }

                var oElement;
                if (isIE) {
                    oSel = oEditor.document.selection.createRange();
                    if (oSel.parentElement) oElement = GetElement(oSel.parentElement(), "A");
                    else oElement = GetElement(oSel.item(0), "A");
                }
                else {
                    oSel = oEditor.getSelection();
                    oElement = GetElement(parent.getSelectedElement(oSel), "A");
                }

                if (oElement) {
                    oElement.setAttribute("title", sTitle)
                    oElement.setAttribute("style", sCssStyle)
                    if (document.getElementById("chkNewWindow").checked) {
                        oElement.setAttribute("rel", "");
                        oElement.setAttribute("target", "_blank");
                    }
                    if (document.getElementById("chkOpenLarger").checked) {
                        oElement.setAttribute("rel", "lightbox");
                        oElement.setAttribute("target", "");
                    }
                }
            }
        }

        function I_Insert() {
            if (document.getElementById("inpImgURL").value == "") return false;

            var obj = parent.oUtil.obj;
            obj.setFocus();
            var oEditor = parent.oUtil.oEditor;

            var oSel;
            var oEl;
            if (isIE) {

                var oSel = oEditor.document.selection.createRange();

                if (oSel.parentElement) oEl = GetElement(oSel.parentElement(), "IMG");
                else oEl = GetElement(oSel.item(0), "IMG");

                if (oEl) {
                    I_Change(oEl);
                    return;
                }
            }
            else {
                oSel = oEditor.getSelection();
                oEl = GetElement(parent.getSelectedElement(oSel), "IMG");

                if (oEl) {
                    if (oEl.nodeName == "IMG") {
                        I_Change(oEl);
                        return;
                    }
                }
            }

            /* Link URL */
            var sLinkURL = "";
            if (document.getElementById("chkOpenLarger").checked) {
                var ss = document.getElementById("inpImgURL").value.substr(0, document.getElementById("inpImgURL").value.length - 5);
                sLinkURL = ss + 'z.jpg';
            }
            else {
                sLinkURL = document.getElementById("inpURL").value;
                if (sLinkURL == "http://") sLinkURL = "";
            }

            /* Link Title */
            var sTitle = document.getElementById("inpTitle").value;

            /* Link Target */
            var sTarget = "";
            if (document.getElementById("chkNewWindow").checked) sTarget = "_blank";

            /* Link Css Style */
            var sCssStyle = "";
            var nMarginTop = document.getElementById("selMarginTop").value;
            var nMarginRight = document.getElementById("selMarginRight").value;
            var nMarginBottom = document.getElementById("selMarginBottom").value;
            var nMarginLeft = document.getElementById("selMarginLeft").value;
            if (document.getElementById("selAlign").value == "left")
                sCssStyle = "float:left;margin-top:" + nMarginTop + "px;margin-right:" + nMarginRight + "px;margin-bottom:" + nMarginBottom + "px;margin-left:" + nMarginLeft + "px;";
            else if (document.getElementById("selAlign").value == "right")
                sCssStyle = "float:right;margin-top:" + nMarginTop + "px;margin-right:" + nMarginRight + "px;margin-bottom:" + nMarginBottom + "px;margin-left:" + nMarginLeft + "px;";
            else
                sCssStyle = "margin-top:" + nMarginTop + "px;margin-right:" + nMarginRight + "px;margin-bottom:" + nMarginBottom + "px;margin-left:" + nMarginLeft + "px;";

            /* Dimension */
            if (document.getElementById("inpWidth").value != "") {
                sCssStyle += ";width:" + document.getElementById("inpWidth").value + "px;";
            }
            if (document.getElementById("inpHeight").value != "") {
                sCssStyle += ";height:" + document.getElementById("inpHeight").value + "px;";
            }

            /* Image URL */
            var sImgURL = document.getElementById("inpImgURL").value;

            /* Image Css Class */
            var sImgCssClass = "";

            /* Image Css Style */
            var sImgCssStyle = sCssStyle;
            //if (sLinkURL == "") sImgCssStyle = sCssStyle;

            /* INSERT IMAGE */
            var oImg = I_InsertImage(sImgURL, sTitle, sImgCssClass, sImgCssStyle);

            var bAbs = getQueryString()["abs"];
            if (bAbs == "true")
                if (document.getElementById("selAlign").value == "left") {
                    oImg.align = "left";
                    oImg.hspace = "7";
                    oImg.vspace = "7";
                }
                else if (document.getElementById("selAlign").value == "right") {
                    oImg.align = "right";
                    oImg.hspace = "7";
                    oImg.vspace = "7";
                }
                else {
                    oImg.hspace = "7";
                    oImg.vspace = "7";
                }


            /* INSERT LINK */
            if (sLinkURL != "") {
                var oElement = I_CreateLink(sLinkURL, sTitle, sTarget, "", "");

                /* Append Image to Link, Add Lightbox */
                if (oElement) {
                    if (isIE) {
                        if (document.getElementById("chkOpenLarger").checked) {
                            oElement.rel = "lightbox";
                            oElement.target = "";
                        }
                    }
                    else {
                        oElement.innerHTML = "";
                        oElement.appendChild(oImg);
                        if (document.getElementById("chkOpenLarger").checked) {
                            oElement.setAttribute("rel", "lightbox");
                            oElement.setAttribute("target", "");
                        }
                    }
                }
            }

			
			if(!isIE) {
				var oSel = oEditor.getSelection();
				oSel.removeAllRanges();
				var range = oEditor.document.createRange();
				range.selectNodeContents(oImg);
                oSel.addRange(range);
				if(obj.bookmarkSelection) obj.bookmarkSelection();
			}
			
            return true;
        }

        function previewImage() {
            var imgOriW = parseInt(canvas.width);
            var imgOriH = parseInt(canvas.height);
            var containerW = 378;
            var containerH = 220;
            var oImg = document.getElementById('imgpreview');
            if (imgOriW <= containerW && imgOriH <= containerH) {
                oImg.style.width = imgOriW + "px";
                oImg.style.height = imgOriH + "px";
            }
            else {
                if (imgOriW / containerW <= imgOriH / containerH) {
                    oImg.style.width = ((containerH * imgOriW) / imgOriH) + "px";
                    oImg.style.height = containerH + "px";
                    oImg.style.marginLeft = ((415 - parseInt(oImg.style.width)) / 2) - 5 + "px";
                }
                else {
                    oImg.style.width = containerW + "px";
                    oImg.style.height = ((containerW * imgOriH) / imgOriW) + "px";
                    oImg.style.marginTop = ((312 - parseInt(oImg.style.height)) / 2) - 5 + "px";
                }
            }

            oImg.src = canvas.toDataURL("image/png");
        }

        function tabClick(n) {
            $("#div1").css("display", "none");
            $("#tab1").css("background", "#ccc");
			$("#div2").css("display", "none");
            $("#tab2").css("background", "#ccc");
			

            $("#div" + n).css("display", "block");
            $("#tab" + n).css("background", "#fcfcfc");
        }
    </script>
    </head>
    <body>
<input id="hidPage" type="hidden" value="1" />
<input id="hidEffect" type="hidden" value="" />
<div id="tab1" onclick="tabClick(1)" style="float:left;font-size:11px;text-shadow:1px 1px 1px rgba(255,255,255,0.6);color:#000;letter-spacing:1px;cursor:pointer;width:87px;min-height:15px;text-align:center;margin-bottom:0px;padding:3px;background:#fcfcfc;margin-left:5px;margin-top:7px"> MY FILES </div>
<div id="tab2" onclick="tabClick(2)" style="float:left;font-size:11px;text-shadow:1px 1px 1px rgba(255,255,255,0.6);color:#000;letter-spacing:1px;cursor:pointer;width:87px;min-height:15px;text-align:center;margin-bottom:0px;padding:3px;background:#ccc;margin-left:5px;margin-top:7px"> UPLOAD </div>
<div style="clear:left"></div>
<table cellpadding="0" cellspacing="0">
      <tr>
    <td><div id="div1" style="color:#000;padding:0px;padding-right:0px;width:415px;height:480px;border-top:none;background:#fcfcfc;">
        <section id="cms_files">
          <div style="padding:.75em;background-color:#fff">
            <div data-modal-select="true" class="veriasist fluid selection dropdown">
              <input name="directory" type="hidden" value="">
              <div class="text" id="seldir">Select Directory</div>
              <i class="dropdown icon"></i>
              <div class="menu">
                <div class="item" data-value=""><i class="icon home"></i><span id="homedir">Home Directory</span></div>
              </div>
            </div>
            <div class="veriasist divider"></div>
            <div class="file_wrapper" style="overflow:auto;height:390px;">
              <ul class="filegrid clearfix">
              </ul>
            </div>
          </div>
        </section>
      </div>
          <div id="div2" style="color:#000;padding:0px;padding-right:0px;width:415px;height:480px;overflow:auto;border-top:none;background:#fcfcfc;display:none;">
        <div id="uploader" style="padding:.75em;background-color:#fff">
              <form id="upload" method="post" action="../../../controller.php" enctype="multipart/form-data">
            <div id="drop"><span  id="drophere"> Drop Files Here</span> <a id="upl"> <span id="browse">Browse</span></a>
                  <input type="file" name="mainfile" multiple />
                  <input name="doFiles" type="hidden" value="1">
                  <input name="mfmact" type="hidden" value="doUpload">
                  <input name="fdirectory" type="hidden" value="">
                </div>
            <ul>
                </ul>
          </form>
            </div>
      </div></td>
    <td style="padding-left:20px;height:395px;text-shadow:1px 1px 1px rgba(255,255,255,0.6);color:#000;" valign="top"><div style="margin-bottom:7px;font-size:10px;letter-spacing:1px" id="lblImgSrc">IMAGE SOURCE:</div>
          <input id="inpImgURL" type="text" class="inpTxt"/>
          <br />
          <br />
          <div style="margin-bottom:7px;font-size:10px;letter-spacing:1px" id="lblTitle">TITLE:</div>
          <input id="inpTitle" type="text" class="inpTxt"/>
          <br />
          <br />
          <table cellpadding="0" cellspacing="0">
        <tr>
              <td><div style="margin-bottom:7px;font-size:10px;letter-spacing:1px" id="lblAlign">ALIGN:</div>
            <select id="selAlign" class="select">
                  <option value="" id="optAlign" name="optAlign"></option>
                  <option value="left" id="optAlign" name="optAlign">Left</option>
                  <option value="right" id="optAlign" name="optAlign">Right</option>
                </select></td>
            </tr>
      </table>
          <br />
          <table cellpadding="0" cellspacing="0">
        <tr>
              <td><div style="margin-bottom:7px;font-size:10px;letter-spacing:1px" id="lblMargin">MARGIN: (TOP / RIGHT / BOTTOM / LEFT)</div>
            <select id="selMarginTop" class="select">
                  <option value="0"></option>
                  <option value="1">1px</option>
                  <option value="2">2px</option>
                  <option value="3">3px</option>
                  <option value="4">4px</option>
                  <option value="5">5px</option>
                  <option value="6">6px</option>
                  <option value="7">7px</option>
                  <option value="8">8px</option>
                  <option value="9">9px</option>
                  <option value="10">10px</option>
                  <option value="11">11px</option>
                  <option value="12">12px</option>
                  <option value="13">13px</option>
                  <option value="14">14px</option>
                  <option value="15">15px</option>
                  <option value="16">16px</option>
                  <option value="17">17px</option>
                  <option value="18">18px</option>
                  <option value="19">19px</option>
                  <option value="20">20px</option>
                  <option value="25">25px</option>
                  <option value="30">30px</option>
                  <option value="35">35px</option>
                  <option value="40">40px</option>
                </select>
            <select id="selMarginRight" class="select">
                  <option value="0"></option>
                  <option value="1">1px</option>
                  <option value="2">2px</option>
                  <option value="3">3px</option>
                  <option value="4">4px</option>
                  <option value="5">5px</option>
                  <option value="6">6px</option>
                  <option value="7">7px</option>
                  <option value="8">8px</option>
                  <option value="9">9px</option>
                  <option value="10">10px</option>
                  <option value="11">11px</option>
                  <option value="12">12px</option>
                  <option value="13">13px</option>
                  <option value="14">14px</option>
                  <option value="15">15px</option>
                  <option value="16">16px</option>
                  <option value="17">17px</option>
                  <option value="18">18px</option>
                  <option value="19">19px</option>
                  <option value="20">20px</option>
                  <option value="25">25px</option>
                  <option value="30">30px</option>
                  <option value="35">35px</option>
                  <option value="40">40px</option>
                </select>
            <select id="selMarginBottom" class="select">
                  <option value="0"></option>
                  <option value="1">1px</option>
                  <option value="2">2px</option>
                  <option value="3">3px</option>
                  <option value="4">4px</option>
                  <option value="5">5px</option>
                  <option value="6">6px</option>
                  <option value="7">7px</option>
                  <option value="8">8px</option>
                  <option value="9">9px</option>
                  <option value="10">10px</option>
                  <option value="11">11px</option>
                  <option value="12">12px</option>
                  <option value="13">13px</option>
                  <option value="14">14px</option>
                  <option value="15">15px</option>
                  <option value="16">16px</option>
                  <option value="17">17px</option>
                  <option value="18">18px</option>
                  <option value="19">19px</option>
                  <option value="20">20px</option>
                  <option value="25">25px</option>
                  <option value="30">30px</option>
                  <option value="35">35px</option>
                  <option value="40">40px</option>
                </select>
            <select id="selMarginLeft" class="select">
                  <option value="0"></option>
                  <option value="1">1px</option>
                  <option value="2">2px</option>
                  <option value="3">3px</option>
                  <option value="4">4px</option>
                  <option value="5">5px</option>
                  <option value="6">6px</option>
                  <option value="7">7px</option>
                  <option value="8">8px</option>
                  <option value="9">9px</option>
                  <option value="10">10px</option>
                  <option value="11">11px</option>
                  <option value="12">12px</option>
                  <option value="13">13px</option>
                  <option value="14">14px</option>
                  <option value="15">15px</option>
                  <option value="16">16px</option>
                  <option value="17">17px</option>
                  <option value="18">18px</option>
                  <option value="19">19px</option>
                  <option value="20">20px</option>
                  <option value="25">25px</option>
                  <option value="30">30px</option>
                  <option value="35">35px</option>
                  <option value="40">40px</option>
                </select></td>
            </tr>
      </table>
          <div style="margin-top:12px;margin-bottom:7px;height:50px">
        <div id="divFlickrSize" style="display:none;">
              <input id="rdoSize1" name="rdoSize" type="radio" value="s" group="size" onclick="changeSize()" />
              <label for="rdoSize1" id="lblSize1">SMALL SQUARE</label>
              <input id="rdoSize2" name="rdoSize" type="radio" value="t" group="size" onclick="changeSize()" />
              <label for="rdoSize2" id="lblSize2">THUMBNAIL</label>
              <input id="rdoSize3" name="rdoSize" type="radio" value="m" group="size" onclick="changeSize()" checked="checked" />
              <label for="rdoSize3" id="lblSize3">SMALL</label>
              <br />
              <input id="rdoSize5" name="rdoSize" type="radio" value="z" group="size" onclick="changeSize()" />
              <label for="rdoSize5" id="lblSize5">MEDIUM</label>
              <input id="rdoSize6" name="rdoSize" type="radio" value="b" group="size" onclick="changeSize()" />
              <label for="rdoSize6" id="lblSize6">LARGE</label>
            </div>
        <div id="divImageSize" style="display:none;font-size:10px;letter-spacing:1px">
              <input type="hidden" id="hidWidth" value="" />
              <input type="hidden" id="hidHeight" value="" />
              <label id="lblWidthHeight" for="inpWidth">WIDTH x HEIGHT:</label>
              <input id="inpWidth" type="text" value="" style="width:50px;height:23px;" onblur="dimChange('width', this)" />
              x
              <input id="inpHeight" type="text" value="" style="width:50px;height:23px;" onblur="dimChange('height', this)" />
              px
              <div style="margin-top:5px;">
            <input type="checkbox" id="chkRatio" name="chkRatio" checked="checked"/>
            <label for="chkRatio" id="lblMaintainRatio">MAINTAIN RATIO</label>
            &nbsp;&nbsp;&nbsp; <a href="#" onclick="resetImage();return false;"><span id="resetdimension">RESET DIMENSION</span></a> </div>
            </div>
      </div>
          <div style="border-top:#fff 1px solid;border-bottom:#ccc 1px solid;margin-top:10px;margin-bottom:10px;margin-left:0px;width:265px"></div>
          <div style="height:10px">
        <div id="divFlickrLarger" style="display:none;">
              <div style="margin-bottom:15px;font-size:10px;letter-spacing:1px">
            <input id="chkOpenLarger" type="checkbox" />
            <label for="chkOpenLarger" id="lblOpenLarger">OPEN LARGER IMAGE IN A LIGHTBOX, OR</label>
          </div>
            </div>
      </div>
          <div style="margin-bottom:7px;font-size:10px;letter-spacing:1px" id="lblLinkToUrl">LINK TO URL:</div>
          <input id="inpURL" type="text" value="http://" class="inpTxt"/>
          <div style="margin-top:7px;margin-bottom:20px;font-size:10px;letter-spacing:1px">
        <input id="chkNewWindow" type="checkbox" />
        <label for="chkNewWindow" id="lblNewWindow">OPEN IN A NEW WINDOW.</label>
      </div>
          <input type="button" name="btnCancel" id="btnCancel" value="cancel" onclick="I_Close()" class="inpBtn" style="width:120px;height:33px" onmouseover="this.className='inpBtnOver';" onmouseout="this.className='inpBtnOut'" />
          <input type="button" name="btnInsert" id="btnInsert" value="insert" onclick="I_Insert();I_Close()" class="inpBtn" style="width:120px;height:33px" onmouseover="this.className='inpBtnOver';" onmouseout="this.className='inpBtnOut'" /></td>
  </tr>
    </table>
<script type="text/javascript"> 
// <![CDATA[  
$(document).ready(function () {
	$(".file_wrapper").enscroll({
		showOnHover: true,
		addPaddingToPane: false,
		verticalTrackClass: 'scrolltrack',
		verticalHandleClass: 'scrollhandle'
	});

	$('.filegrid').on('click', 'li img.item', function (e) {
		$items = e.target;
		name = $($items).data("name");
		path = $($items).data("path");
		type = $($items).data("type");
		title = $($items).data("title");

		var URL = '../uploads/' + path + name;
		$("#inpImgURL").val(URL);

		return false;
	});

	getFolders();
	getFiles();

	function getFolders() {
		$.ajax({
			type: "post",
			url: "../../../controller.php",
			dataType: 'json',
			data: {
				doFiles: 1,
				mfmact: "listFolders"
			},
			success: function (json) {
				if (json.type == "success") {
					$("#cms_files .menu").append(json.message);
					$("#cms_files [data-modal-select]").dropdown()
						.dropdown('setting', {
							onChange: function (v) {
								$("input[name='fdirectory']").val(v);
								getFiles();
							}
						});
				}

			}
		});

	}

	function getFiles() {
		$.ajax({
			type: "post",
			url: "../../../controller.php",
			dataType: 'json',
			data: {
				doFiles: 1,
				mfmact: "getFiles",
				directory: $("input[name='directory']").val()
			},
			beforeSend: function () {
				$("#cms_files .veriasist.form").addClass('loading');
			},
			success: function (json) {
				if (json.type == "success") {
					setTimeout(function () {
						$("#cms_files .veriasist.form").removeClass('loading');
					}, 250);
					$("#cms_files ul.filegrid").html(json.message);
				}

				$('#cms_files ul.filegrid').gridalicious({
					width: 140,
					gutter: 0,
					selector: 'li',
					animate: false
				});
			}
		});
	}

	var ul = $('#upload ul');
	$('#drop a').click(function () {
		$(this).parent().find('input').click();
	});

	$('#upload').fileupload({
		dropZone: $('#drop'),
		limitMultiFileUploads: 5,
		sequentialUploads: true,
		add: function (e, data) {
			var tpl = $('<li class="working"><input type="text" value="0" data-width="32" data-height="32" data-fgColor="#3498DB" data-readOnly="1" data-bgColor="#ffffff" /><p><small></small></p><span></span></li>');

			tpl.find('p').text(data.files[0].name)
				.append('<small></small>')
				.append('<i>' + formatFileSize(data.files[0].size) + '</i>');

			data.context = tpl.appendTo(ul);
			tpl.find('input').knob();
			tpl.find('span').click(function () {

				if (tpl.hasClass('working')) {
					jqXHR.abort();
				}

				tpl.fadeOut(function () {
					tpl.remove();
				});

			});
			var jqXHR = data.submit().success(function (result) {
				var json = JSON.parse(result);
				var status = json['status'];

				if (status == 'error') {
					data.context.addClass('error');
					data.context.find('span').addClass('ferror');
					data.context.find('small').append(json['msg']);
				} else {
					getFiles();
				}
				//console.log(json)
			});
			},

            progress: function (e, data) {
                var progress = parseInt(data.loaded / data.total * 100, 10);
                data.context.find('input').val(progress).change();
                if (progress == 100) {
                    data.context.removeClass('working');
                }
            },

            fail: function (e, data) {
                data.context.addClass('error');
            }


	});
});
// ]]>
</script>
</body>
</html>

<?php
  define("_VALID_PHP", true);
  require_once("../../../init.php");
  if (!$user->is_Admin())
    exit;
?>
<!DOCTYPE HTML>
<html>
<head>
<meta charset="utf-8">
<title>Formatted Text</title>
<link rel="stylesheet" type="text/css" href="<?php echo SITEURL . '/theme/' . $core->theme;?>/cache/master_main.css">
<script type="text/javascript" src="../../../../assets/jquery.js"></script>
<style type="text/css">
body {
  font-family: "Open Sans", sans-serif;
  margin: 0px;
  padding: 1em;
  color: #999999;
  text-rendering: optimizeLegibility;
  font-weight: 400;
}
a {
  cursor: pointer;
  text-decoration: none;
  -webkit-transition: all .35s ease;
  -moz-transition: all .35s ease;
  -o-transition: all .35s ease;
  transition: all .35s ease;
  outline: none
}
a:hover,
a.hover,
a:focus {
  -webkit-transition: all .55s ease;
  -moz-transition: all .55s ease;
  -o-transition: all .35s ease;
  transition: all .55s ease;
  outline: none;
  text-decoration: none
}
</style>
</head>
<body>
<div id="icomsg">
  <div class="veriasist icon message"> <i class="inbox icon"></i>
    <div class="content">
      <div class="header"> Have you heard about our mailing list? </div>
      <p>Get all the best inventions in your e-mail every day. Sign up now!</p>
    </div>
  </div>
</div>
<div class="veriasist celled list">
  <div class="item"> <a class="right floated tiny basic veriasist button" href="javascript:insertSnippet('icomsg')">Insert</a>
    <div class="content">
      <div class="header">Icon Message</div>
    </div>
  </div>
</div>
<div id="bmsg">
  <div class="veriasist message">
    <div class="content">
      <div class="header"> Welcome back! </div>
      <p>It's good to see you again. I have had a lot to think about since our last visit. </p>
    </div>
  </div>
</div>
<div class="veriasist celled list">
  <div class="item"> <a class="right floated tiny basic veriasist button" href="javascript:insertSnippet('bmsg')">Insert</a>
    <div class="content">
      <div class="header">Basic Message</div>
    </div>
  </div>
</div>
<div id="dismsg">
  <div class="veriasist message"> <i class="close icon"></i>
    <div class="header"> Congratulations, you are now a member! </div>
    <p>Visit our registration page, then try again</p>
  </div>
</div>
<div class="veriasist celled list">
  <div class="item"> <a class="right floated tiny basic veriasist button" href="javascript:insertSnippet('dismsg')">Insert</a>
    <div class="content">
      <div class="header">Dismissable Message</div>
    </div>
  </div>
</div>
<div id="sucmsg">
  <div class="veriasist positive message">
    <div class="content">
      <p>It's good to see you again. I have had a lot to think about since our last visit. </p>
    </div>
  </div>
</div>
<div class="veriasist celled list">
  <div class="item"> <a class="right floated tiny basic veriasist button" href="javascript:insertSnippet('sucmsg')">Insert</a>
    <div class="content">
      <div class="header">Positive Message</div>
    </div>
  </div>
</div>
<div id="negmsg">
  <div class="veriasist negative message">
    <div class="content">
      <p>It's good to see you again. I have had a lot to think about since our last visit. </p>
    </div>
  </div>
</div>
<div class="veriasist celled list">
  <div class="item"> <a class="right floated tiny basic veriasist button" href="javascript:insertSnippet('negmsg')">Insert</a>
    <div class="content">
      <div class="header">Negative Message</div>
    </div>
  </div>
</div>
<div id="infomsg">
  <div class="veriasist info message">
    <div class="content">
      <p>It's good to see you again. I have had a lot to think about since our last visit. </p>
    </div>
  </div>
</div>
<div class="veriasist celled list">
  <div class="item"> <a class="right floated tiny basic veriasist button" href="javascript:insertSnippet('infomsg')">Insert</a>
    <div class="content">
      <div class="header">Info Message</div>
    </div>
  </div>
</div>
<div id="warmsg">
  <div class="veriasist warning message">
    <div class="content">
      <p>It's good to see you again. I have had a lot to think about since our last visit. </p>
    </div>
  </div>
</div>
<div class="veriasist celled list">
  <div class="item"> <a class="right floated tiny basic veriasist button" href="javascript:insertSnippet('warmsg')">Insert</a>
    <div class="content">
      <div class="header">Warning Message</div>
    </div>
  </div>
</div>
<script type="text/javascript">
        function insertSnippet(id) {
            parent.oUtil.obj.insertHTML(document.getElementById(id).innerHTML);
        }
    </script>
</body>
</html>
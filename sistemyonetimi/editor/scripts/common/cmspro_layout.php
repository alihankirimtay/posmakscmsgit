<?php
  define("_VALID_PHP", true);
  require_once("../../../init.php");
  if (!$user->is_Admin())
    exit;
?>
<!DOCTYPE HTML>
<html>
<head>
<meta charset="utf-8">
<title>Grid Layout</title>
<link rel="stylesheet" type="text/css" href="<?php echo SITEURL . '/theme/' . $core->theme;?>/cache/master_main.css">
<script type="text/javascript" src="../../../../assets/jquery.js"></script>
<style type="text/css">
body {
  font-family: "Open Sans", sans-serif;
  margin: 0px;
  padding: 0;
  color: #999999;
  text-rendering: optimizeLegibility;
  font-weight: 400;
}
a {
  cursor: pointer;
  text-decoration: none;
  -webkit-transition: all .35s ease;
  -moz-transition: all .35s ease;
  -o-transition: all .35s ease;
  transition: all .35s ease;
  outline: none
}
a:hover,
a.hover,
a:focus {
  -webkit-transition: all .55s ease;
  -moz-transition: all .55s ease;
  -o-transition: all .35s ease;
  transition: all .55s ease;
  outline: none;
  text-decoration: none
}
.item {
  padding: 15px 30px 30px;
  border-bottom: #e7e7e7 1px solid;
  -webkit-box-shadow: 0 0 40px rgba(0, 0, 0, 0.1) inset;
  -moz-box-shadow: 0 0 40px rgba(0, 0, 0, 0.1) inset;
  box-shadow: 0 0 40px rgba(0, 0, 0, 0.1) inset;
}
</style>
</head>
<body>
<div class="veriasist-grid">
  <div class="item">
    <h4>2 Columns (Left Sidebar)</h4>
    <div class="columns">
      <div class="screen-80" id="divLayout1">
        <div class="columns small-gutters">
          <div class="screen-40 tabet-50 phone-100" contenteditable="false">
            <div contenteditable="true">Occaecat commodo aliqua delectus. Fap craft beer deserunt skateboard ea. Lomo bicycle rights adipisicing banh mi, velit ea sunt next level locavore single-origin coffee in magna veniam.</div>
          </div>
          <div class="screen-60 tabet-50 phone-100" contenteditable="false">
            <div contenteditable="true">Occaecat commodo aliqua delectus. Fap craft beer deserunt skateboard ea. Lomo bicycle rights adipisicing banh mi, velit ea sunt next level locavore single-origin coffee in magna veniam. High life id vinyl, echo park consequat quis aliquip banh mi pitchfork. Vero VHS est adipisicing. Consectetur nisi DIY minim.</div>
          </div>
        </div>
      </div>
      <div class="screen-20"> <a class="veriasist basic button push-right" href="javascript:insertSnippet('divLayout1')">Insert</a> </div>
    </div>
  </div>
  <div class="item">
    <h4>2 Columns (Right Sidebar)</h4>
    <div class="columns">
      <div class="screen-80" id="divLayout2">
        <div class="columns small-gutters">
          <div class="screen-60 tabet-50 phone-100" contenteditable="false">
            <div contenteditable="true">Occaecat commodo aliqua delectus. Fap craft beer deserunt skateboard ea. Lomo bicycle rights adipisicing banh mi, velit ea sunt next level locavore single-origin coffee in magna veniam.</div>
          </div>
          <div class="screen-40 tabet-50 phone-100" contenteditable="false">
            <div contenteditable="true">Occaecat commodo aliqua delectus. Fap craft beer deserunt skateboard ea. Lomo bicycle rights adipisicing banh mi, velit ea sunt next.</div>
          </div>
        </div>
      </div>
      <div class="screen-20"> <a class="veriasist basic button push-right" href="javascript:insertSnippet('divLayout2')">Insert</a> </div>
    </div>
  </div>
  <div class="item">
    <h4>3 Columns</h4>
    <div class="columns">
      <div class="screen-80" id="divLayout3">
        <div class="columns small-gutters">
          <div class="screen-33 tabet-50 phone-100" contenteditable="false">
            <div contenteditable="true">Occaecat commodo aliqua delectus. Fap craft beer deserunt skateboard ea. Lomo bicycle rights adipisicing banh mi, velit ea sunt next level locavore single-origin coffee in magna veniam.</div>
          </div>
          <div class="screen-33 tabet-50 phone-100" contenteditable="false">
            <div contenteditable="true">Occaecat commodo aliqua delectus. Fap craft beer deserunt skateboard ea. Lomo bicycle rights adipisicing banh mi, velit ea sunt next level locavore single-origin coffee in magna veniam.</div>
          </div>
          <div class="screen-33 tabet-50 phone-100" contenteditable="false">
            <div contenteditable="true">Occaecat commodo aliqua delectus. Fap craft beer deserunt skateboard ea. Lomo bicycle rights adipisicing banh mi, velit ea sunt next level locavore single-origin coffee in magna veniam.</div>
          </div>
        </div>
      </div>
      <div class="screen-20"> <a class="veriasist basic button push-right" href="javascript:insertSnippet('divLayout3')">Insert</a> </div>
    </div>
  </div>
  <div class="item">
    <h4>2 Columns</h4>
    <div class="columns">
      <div class="screen-80" id="divLayout4">
        <div class="columns small-gutters">
          <div class="screen-50 tabet-50 phone-100" contenteditable="false">
            <div contenteditable="true">Occaecat commodo aliqua delectus. Fap craft beer deserunt skateboard ea. Lomo bicycle rights adipisicing banh mi, velit ea sunt next level locavore single-origin coffee in magna veniam.</div>
          </div>
          <div class="screen-50 tabet-50 phone-100" contenteditable="false">
            <div contenteditable="true">Occaecat commodo aliqua delectus. Fap craft beer deserunt skateboard ea. Lomo bicycle rights adipisicing banh mi, velit ea sunt next level locavore single-origin coffee in magna veniam.</div>
          </div>
        </div>
      </div>
      <div class="screen-20"> <a class="veriasist basic button push-right" href="javascript:insertSnippet('divLayout4')">Insert</a> </div>
    </div>
  </div>
  <div class="item">
    <h4>4 Columns</h4>
    <div class="columns">
      <div class="screen-80" id="divLayout5">
        <div class="columns small-gutters">
          <div class="screen-25 tabet-50 phone-100" contenteditable="false">
            <div contenteditable="true">Occaecat commodo aliqua delectus. Fap craft beer deserunt skateboard ea. Lomo bicycle rights adipisicing banh mi. </div>
          </div>
          <div class="screen-25 tabet-50 phone-100" contenteditable="false">
            <div contenteditable="true">Occaecat commodo aliqua delectus. Fap craft beer deserunt skateboard ea. Lomo bicycle rights adipisicing banh m.</div>
          </div>
          <div class="screen-25 tabet-50 phone-100" contenteditable="false">
            <div contenteditable="true">Occaecat commodo aliqua delectus. Fap craft beer deserunt skateboard ea. Lomo bicycle rights adipisicing banh mi .</div>
          </div>
          <div class="screen-25 tabet-50 phone-100" contenteditable="false">
            <div contenteditable="true">Occaecat commodo aliqua delectus. Fap craft beer deserunt skateboard ea. Lomo bicycle rights adipisicing banh mi.</div>
          </div>
        </div>
      </div>
      <div class="screen-20"> <a class="veriasist basic button push-right" href="javascript:insertSnippet('divLayout5')">Insert</a> </div>
    </div>
  </div>
</div>
<script type="text/javascript">
        function insertSnippet(id) {
            parent.oUtil.obj.insertHTML('<div class="veriasist-grid">' + document.getElementById(id).innerHTML + '</div>');
        }  
</script>
</body>
</html>
<?php
  /**
   * Controller
   *
   * @package VeriAsist
   * @author veriasist.com
   * @copyright 2014
   * @version $Id: controller.php, v4.00 2014-04-20 10:12:05 gewa Exp $
   */
  define("_VALID_PHP", true);
  
  require_once("../../init.php");
  if (!$user->is_Admin())
      redirect_to("../../login.php");

  require_once("admin_class.php");
  Registry::set('jTabs', new jTabs());
?>
<?php
  /* == Proccess Tabs == */
  if (isset($_POST['processTabs'])):
      Registry::get("jTabs")->processTabs();
  endif;

  /* == Update Tab Order == */
  if (isset($_GET['sorttabs'])):
      jTabs::updateOrder();
  endif;

  /* == Delete Tab == */
  if (isset($_POST['delete']) and $_POST['delete'] == "deleteTab"):
      $title = sanitize($_POST['title']);
      $result = $db->delete(jTabs::mTable, "id=" . Filter::$id);

      if ($result):
          $json['type'] = 'success';
          $json['title'] = Lang::$word->_SUCCESS;
          $json['message'] = Lang::$word->_PLG_JT_TAB . ' /' . $title . '/ ' . Lang::$word->_DELETED;
          Security::writeLog(Lang::$word->_PLG_JT_TAB . ' /' . urldecode($title) . '/ ' . Lang::$word->_DELETED, "", "no", "plugin");
      else:
          $json['type'] = 'warning';
          $json['title'] = Lang::$word->_ALERT;
          $json['message'] = Lang::$word->_SYSTEM_PROCCESS;
      endif;
      print json_encode($json);
  endif;
?>
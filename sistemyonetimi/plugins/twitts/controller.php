<?php
  /**
   * Controller
   *
   * @package VeriAsist
   * @author veriasist.com
   * @copyright 2014
   * @version $Id: controller.php, v4.00 2014-04-20 10:12:05 gewa Exp $
   */
  define("_VALID_PHP", true);
  
  require_once("../../init.php");
  if (!$user->is_Admin())
      redirect_to("../../login.php");

  require_once("admin_class.php");
  Registry::set('Twitts', new Twitts());
?>
<?php
  /* == Proccess Configuration == */
  if (isset($_POST['processConfig'])):
      Registry::get("Twitts")->processConfig();
  endif;
?>
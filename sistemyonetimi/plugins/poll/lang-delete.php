<?php
  /**
   * Language Data Delete
   *
   * @package VeriAsist
   * @author veriasist.com
   * @copyright 2010
   * @version $Id: lang-delete.php, v2.00 2011-04-20 10:12:05 gewa Exp $
   */
  if (!defined("_VALID_PHP"))
      die('Direct access to this location is not allowed.');
?>
<?php
	self::$db->query("LOCK TABLES plug_poll_options WRITE");
	self::$db->query("ALTER TABLE plug_poll_options DROP COLUMN value_" . $flag_id);
	self::$db->query('UNLOCK TABLES');
  
	self::$db->query("LOCK TABLES plug_poll_questions WRITE");
	self::$db->query("ALTER TABLE plug_poll_questions DROP COLUMN question_" . $flag_id);
	self::$db->query("UNLOCK TABLES");
?>
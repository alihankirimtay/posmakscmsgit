<?php
  /**
   * Language Data Add
   *
   * @package VeriAsist
   * @author veriasist.com
   * @copyright 2014
   * @version $Id: lang-add.php, v4.00 2014-04-20 10:12:05 gewa Exp $
   */
  if (!defined("_VALID_PHP"))
      die('Direct access to this location is not allowed.');
?>
<?php
  self::$db->query("LOCK TABLES plug_videoslider WRITE");
  self::$db->query("ALTER TABLE plug_videoslider ADD title_$flag_id VARCHAR(150) NOT NULL AFTER title_en");
  self::$db->query("UNLOCK TABLES");

  if ($plug_videoslider = self::$db->fetch_all("SELECT * FROM plug_videoslider")) {
      foreach ($plug_videoslider as $row) {
          $data['title_' . $flag_id] = $row->title_en;
          self::$db->update("plug_videoslider", $data, "id = " . $row->id);
      }
      unset($data, $row);
  }
?>
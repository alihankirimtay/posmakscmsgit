<?php
  /**
   * Language Data Delete
   *
   * @package VeriAsist
   * @author veriasist.com
   * @copyright 2010
   * @version $Id: lang-delete.php, v2.00 2011-04-20 10:12:05 gewa Exp $
   */
  if (!defined("_VALID_PHP"))
      die('Direct access to this location is not allowed.');
?>
<?php
	self::$db->query("LOCK TABLES plug_newsslider WRITE");
	self::$db->query("ALTER TABLE plug_newsslider DROP COLUMN title_" . $flag_id);
	self::$db->query("ALTER TABLE plug_newsslider DROP COLUMN body_" . $flag_id);
	self::$db->query("UNLOCK TABLES");
?>
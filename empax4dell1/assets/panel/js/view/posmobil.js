function round(value, decimals) {
    return Number(Math.round(value+'e'+decimals)+'e-'+decimals);
}

function getDateNow() {
  let date = new Date();
  return `${date.getDate()}/${date.getMonth()+1}/${date.getFullYear()} ${date.getHours()}:${date.getMinutes()}`;
}

function priceTax (amount, tax) {
    amount = parseFloat(amount),
    tax = parseFloat(tax);

    return round(amount + amount * tax / 100, 2);
}

function calcTax (amount, tax) {
  amount = parseFloat(amount),
  tax = parseFloat(tax);

  return round(amount * tax / 100, 2);
}

function _loading (action) {
  if(action == "add") $("body").append("<div class='loading-spinner'> <div class='loading-spinner-bussy'></div> </div>");
  else if(action == "delete") $(".loading-spinner").remove();
}

function _calc() {
  var total = 0.00;
  $.each($('#tblProduct > tbody > tr'), function(index, value) {

    is_gift = parseFloat($("input[name*='quantity]']", value).attr('is_gift'));

    if (is_gift == 1) {
      return;
    }
     total += parseFloat($('.total-sum-price', value).html());
  });


  if (posmobil.current_sale.discount_type == "amount") {
    total -= posmobil.current_sale.discount_amount;
  } else if (posmobil.current_sale.discount_type == "percent") {
    total -= total*posmobil.current_sale.discount_amount/100;
  }
  total = round(total,2);

  $('#spanPrice').html(parseFloat(total).toFixed(2)+'₺');
  $('#span-order-price').html(parseFloat(total).toFixed(2)+'₺');
}

function sum_calc(price, tax) {
  tax = price*tax/100;
  tax = round(tax,2);
  return parseFloat(price) + tax;

}

function _price() {
  var price = total = tax = 0.00;
  var quantity = resultPrice = resultTax = is_gift = subtotal  = 0;

  $.each($('#tblProduct > tbody > tr'), function(index, value) {

    is_gift = parseFloat($("input[name*='quantity]']", value).attr('is_gift'));

    if (is_gift == 1) {
      return;
    }

    price = parseFloat($("input[name$='product_id]']", value).attr('price'));
    tax = parseFloat($('input[name^="products"]', value).attr('tax'));
    additionals = $("input[name*='additional_products']", value);
    quantity = parseFloat($('#count', value).text());

    subtax = calcTax(price,tax);
    subtax = subtax*quantity;
    subtotal = price*quantity;

    $.each(additionals, function(index, add) {
      add_price = parseFloat($(this).attr('price'));
      add_tax = parseFloat($(this).attr('tax'));
      subtotal += add_price*quantity;
      subtax += calcTax(add_price*quantity, add_tax); 
    });

    //toplam ara fiyat ve kdv hesaplama

    resultPrice += subtotal;
    resultTax += subtax;
    total = resultPrice+ resultTax;

  });

  //Html'lerin doldurulması

  $('#subtotalCheck').html(resultPrice.toFixed(2)+"₺");
  $('.invoice-subtotal').html(resultPrice.toFixed(2)+"₺");
  $('#kdv').html(resultTax.toFixed(2)+"₺");
  $('.invoice-taxtotal').html(resultTax.toFixed(2)+"₺");


  if (posmobil.current_sale.discount_type == "amount") {
    total -= posmobil.current_sale.discount_amount;
  } else if (posmobil.current_sale.discount_type == "percent") {
    total -= total*posmobil.current_sale.discount_amount/100;
  }
  total = round(total,2);
  $('#sumCheck').html(total.toFixed(2)+"₺");
  $('.invoice-amount').html(total.toFixed(2)+"₺");

}

function _home_reload() {

  if($('#home').parent('li').hasClass('active')){
    reload_tables();
  }

}

function _posRest() {
  var sumCheck = sumAmount = posRest = 0.00; 
  sumCheck = parseFloat($('#sumCheck').html());
  sumAmount = parseFloat($('#sumAmount').html());

  posRest = sumCheck - sumAmount;

  if (posRest <= 0 ) {
    $('#posRest').html("0.00₺");
    $('#posChange').html(posRest.toFixed(2)+"₺");

  } else {

    $('#posRest').html(posRest.toFixed(2)+"₺");
    $('#posChange').html("0.00₺");

  }
}

function _payment(inputName, basketInputName) {
  let 
    input = $('input[name='+inputName+']'),
    basket_input = $('input[name='+basketInputName+']'),
    input_val = basket_input_val = total = posRest = 0;
    basket_input_val = basket_input.val();

    
    input_val = input.val();
    input_val = input_val.replace(/[^0-9\.]/g, '');
    input_val = Number(input_val);
    input.val("");

    posRest = parseFloat($('#posRest').html());

    if (!!!input_val) {
       input_val = posRest;
    }


    total = parseFloat(basket_input_val) + input_val;

    basket_input.val(total);

    sumAmount = $('#sumAmount').html();
    sumAmount = parseFloat(sumAmount);
    sumAmount = round(sumAmount, 2);

    sumAmount += input_val;
    $('#sumAmount').html(sumAmount.toFixed(2)+"₺");
}

function _calculate(basketInputName) {
  let
    basket_input_val = $('input[name='+basketInputName+']').val(),
    sumAmount = $('#sumAmount').html();

    basket_input_val = parseFloat(basket_input_val);
    sumAmount = parseFloat(sumAmount);


    sumAmount -= basket_input_val;
    $('#sumAmount').html(sumAmount.toFixed(2)+"₺");

    $('input[name='+basketInputName+']').val(0.00);

}

function interim_count_calc(products, payment_types) {

  var paid = 0;

  $.each(payment_types, function(index, payment_type) {
    paid += parseInt(products[`paid_${payment_type.alias}`]);
  });

  return paid;
}

function get_products_table(get_data_products) {

  sale_products = get_data_products;

  $.each(sale_products, function(index, val) {

    let total = sumTotal = 0.00;

    total = sum_calc(val.price, val.tax);
    sumTotal = val.quantity* parseFloat(total);

    $('#tblAddition > tbody').append('\
        <tr data-index="'+val.id+'" class="pr-container">\
          <td class="td-title">'+val.title+'</td>\
          <td id="count" is_gift="'+val.is_gift+'">'+val.quantity+'</td>\
          <td class="total-sum-price">'+sumTotal.toFixed(2)+'₺</td>\
        </tr>\
    ');

  });

}

function get_products (get_data_products, interim_payment_amount, payment_types) {

  sale_products = get_data_products;
  let total = subtotal = 0;

  $.each(sale_products, function(index, val) {

    var productItemStr = "",        
    interim_total = addTotal = totalPaid = 0;
    interim_total = parseFloat(interim_payment_amount);

    total = sum_calc(val.price, val.tax);
    sumTotal = val.quantity* parseFloat(total);

    $.each(val.additional_products, function(index, addVal) {
      productItemStr +=  '<input type="hidden" name="products['+val.id+'][additional_products]['+addVal.product_id+']" value="'+addVal.product_id+'" tax="'+addVal.tax+'" price="'+addVal.price+'">';
      addTotal += sum_calc(addVal.price, addVal.tax);
    });

    //Hesap Ekranında Ara Ödeme ve Ödenen Tutarın Doldurulması

    $('#subtotal').text(interim_total.toFixed(2)+"₺");
    $('#sumAmount').text(interim_total.toFixed(2)+"₺");

    if (val.type == "product") {

    $.each(val.stocks,function(key,stockVal) {
      if(stockVal.optional == "1" && stockVal.in_use !== "1" ) {
        productItemStr += `<input type="hidden" name="products[${val.id}][stocks][${stockVal.stock_id}]" value="${stockVal.stock_id}">`;
      }
    });

    }


    sumAddTotal = val.quantity* parseFloat(addTotal);

    checkTotal = parseFloat(sumAddTotal) + parseFloat(sumTotal);

    totalPaid = interim_count_calc(val, payment_types);

    if (val.quantity > 0) {
      $('#tblProduct > tbody').append('\
          <tr data-index="'+val.id+'" class="pr-container">\
            <td class="td-title">'+val.title+'</td>\
            <td id="count">'+val.quantity+'</td>\
            <input type="hidden" name="products['+val.id+'][product_id]" value="'+val.product_id+'" price="'+val.price+'" tax = "'+val.tax+'">\
            <input type="hidden" name="products['+val.id+'][quantity]" value="'+val.quantity+'" is_gift = "'+val.is_gift+'" interim_paid = "'+totalPaid+'" completed="'+val.completed+'">\
            <input type="hidden" name="products['+val.id+'][sale_product_id]" value="'+val.id+'">\
            '+productItemStr+'\
            <td class="total-sum-price">'+checkTotal.toFixed(2)+' ₺</td>\
            <td class="text-right"><i class="iconRemove btn btn-danger glyphicon glyphicon-remove br-30"></i> <i id="iconEdit" class="btn btn-blue glyphicon glyphicon-pencil pull-right br-30" data-toggle="modal" data-target="#productQuantityModal">\
            </i></td>\
          </tr>\
      ');
    }

    posmobil.Helpers.visible("#tblProduct .iconEdit", posmobil.Helpers.hasPermission("pointofsales.productedit"));
    posmobil.Helpers.visible("#tblProduct .iconRemove", posmobil.Helpers.hasPermission("pointofsales.productdelete"));

    row = $('input[name="products['+val.id+'][quantity]"]');

    if (row.attr('is_gift') == 1) {

      table = row.parent('tr');
      $('.td-title', table).css("text-decoration", "line-through");

    }

  });
        
}

function order_hidden() {
  $('.product-footer').addClass('hidden');
  $('#productlist').removeClass('product-list-height');
}


function reload_tables() {

  $.post(base_url+'mobilpos/ajax/getTable', $(posmobil.posForm).serializeArray(),
   function(json) {
    let total_amount = 0.00;
    $.each(json.data,function (key,value) {
      $.each($('#tablelist .table-image'), function(index, now_val) {
        if(value.id == $('input[name=table_id]',now_val).val()) {
          if(!!value.sale_id) {
            $(this).css('background', '#EB212F');
            $(this).css('color', 'white');
          } else {
            $(this).css('background', 'white');
            $(this).css('color', 'black');
          }

          let total_amount = value.amount-value.interim_payment_amount;
          $('.table-price', now_val).text(Number(total_amount).toFixed(2) +"₺");
       }
    });
  });
 
  }, "json");
}

function getMenuView() 
{
  let menuView = getCookie("menu");
  if (menuView === null) {
      setCookie("menu", "tab", 365);
      return "tab";
  }

  return menuView;
  
}

function setCookie(name,value,days) {
    var expires = "";
    if (days) {
        var date = new Date();
        date.setTime(date.getTime() + (days*24*60*60*1000));
        expires = "; expires=" + date.toUTCString();
    }
    document.cookie = name + "=" + (value || "")  + expires + "; path=/";
}

function getCookie(name) {
    var nameEQ = name + "=";
    var ca = document.cookie.split(';');
    for(var i=0;i < ca.length;i++) {
        var c = ca[i];
        while (c.charAt(0)==' ') c = c.substring(1,c.length);
        if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
    }
    return null;
}

function eraseCookie(name) {   
    document.cookie = name+'=; Max-Age=-99999999;';  
}

function calcModalBodyHeight() {
  var modalContent = $('#productQuantityModal .modal-content');

  var 
  modalContentHeight = modalContent.height(),
  header = $('.modal-header', modalContent).height(),
  footer = $('.modal-footer', modalContent).height();

  var modalBody =  modalContentHeight - (header + footer + 60);
  $('#productQuantityModal .product-modal-body').css('max-height', modalBody);

  $('#productQuantityModal').on('shown.bs.modal', function() {
       $("#productQuantityModal .product-modal-body").removeAttr("style");
 });


}

function calcCheckScreenScroll() {
  var windowHeight = $(window).height(),
      header = $('.inputs').offset().top,
      footer = $('.footer-button').height();

  var inputSize = windowHeight - (header + footer + 10);

  $('.inputs').css({
    'max-height': inputSize,
    'overflow': 'auto'
  });
}

function touchSwipeCurrentIndex() {
  
    let zones = $('#zonelist li input');
    
    let current_index;

    $.each(zones, function(index, input) {        

    if ($(this).prop('checked')) {
      current_index = index;
      return ;
    }
  });

    return current_index;
}


var posmobil = {

posForm : "#pos",

payment_types : {},
operator_session: {
  user_id: null,
  user_name: null,
  user_role: null,
  permissions: null,
},
isTablet : false,

cookieValue : getMenuView(),

init: function() {

    //this.get_floors();
    this.get_categoried_products();
    this.on_click_product_add();
    this.onClick_complete();
    this.onClick_interimPaymentSubmit();
    this.interim_payment_unpaid_products();
    this.interim_payment_payable_products();
    this.ProductNotes.init();
    this.OperatorSession.init();
    this.get_tab_cookie();
    this.get_notification();
    this.getFloorsWithZones();
    this.paymentButtonsClick();
    this.lockScreen.init();

    $("body").on("change","#pos input[name=floor_id]", this.get_zones);

    $("body").on("change","#pos input[name=zone_id]", this.get_tables);

    $("body").on("change","#pos input[name=table_id]", this.on_click_table);

    $("body").on("click","#pos #backHome", this.on_click_backHome);

    $("body").on("click","#pos #backHomePage", this.on_click_backHomePage);

    $("body").on("click","#pos #backTable", this.on_click_backTable);

    $("body").on("click","#pos .product", this.on_click_product);

    $("body").on("click","#pos .basket-click", this.on_click_order_check);

    $("body").on("click","#pos #checkBtn", this.on_click_order_complete);

    $("body").on("click","#pos #interimPaymentBtn", this.on_click_interim_payment);

    $("body").on("change","#pos #sel1", this.on_change_products_options);

    $("body").on("click","#pos #iconEdit", this.on_click_iconEdit);

    $("body").on("click","#pos .iconRemove", this.on_click_icon_remove);

    $("body").on("click","#pos #plus", this.on_click_icon_plus);

    $("body").on("click","#pos #minus", this.on_click_icon_minus);

    $("body").on("click","#pos #print-adisyon", this.on_click_print_addition);

    $("body").on("click","#pos #submit-discount-login", this.on_click_print_discount);

    $("body").on("click","#pos #change_pos", this.on_click_change_pos);

    $("body").on("click","#pos #sidebarCollapse", this.on_click_sidebar_button);

    $("body").on("click","#pos #left-sidebar-button", this.on_click_left_sidebar_button);

    $("body").on("click","#pos #backOrderTab", this.on_click_back_order_tab);

    $("body").on("click","#pos #pos-order", this.on_click_pos_order);

    $("body").on("click","#pos #cancel-order", this.on_click_cancel_order);

    $("body").on("click","#pos #tabCategoryList", this.on_click_category_tab);

    $("body").on("click","#pos .home-click", this.on_click_home);

    //$("body").on("click","#pos .basket-click", this.on_click_basket);

    $("body").on("click","#pos #notificationButton", this.on_click_notification);

    $("body").on("click","#pos #product-list .deliver", this.on_click_deliver);

    $("body").on("click","#pos #product-list .account", this.on_click_account);

    $("body").on("click","#pos #callAccount", this.on_click_call_account);

    $("body").on("click","#pos #cancel-sale", this.on_click_cancel_sale);

    $("body").on("click","#pos .locked-click", this.on_click_locked_click);

    $("body").on("click","#pos #updateTable", this.on_click_update_table);

    $("body").on("click","#modal-discount_login .percent-buttons", this.on_click_modal_radio_button);

    $("body").on("click","#pos #add-discount", this.on_click_add_discount);

},

on_click_add_discount: function () {

  $('#modal-discount_login .percent-buttons.first').trigger('click');

},

on_click_modal_radio_button: function () {

  let
  modal = $('#modal-discount_login'), 
  percentValue = $(this).text();

  $('input[name="discount_amount"]', modal).val(percentValue);

},

on_click_update_table: function() {
  let table_id = $('#hiddenInputs input[name="table_id"]').val();

    $(`#input${table_id}`).trigger('change');
    $('.basket-click').click();

},

on_click_locked_click: function() {

$('#tabLocked').click();

},

on_click_cancel_sale: function() {

  $.post(base_url+'mobilpos/ajax/cancelSale',$(posmobil.posForm).serializeArray(), 

  function(json) {

      if (!json.result) {
        Pleasure.handleToastrSettings("Hata!", json.message, "error");
        return;
      }

      Pleasure.handleToastrSettings("Başarılı",json.message,"success");
      setTimeout(function() {
        location.reload();
      }, 1000);
    


  },"json");


},


paymentButtonsClick() {

  $.each(posmobil.payment_types, function(index, val) {
    $(`#add-${val.alias}`).on('click', function(event) {
      _payment(`"add-${val.alias}"`, `"payments[${val.alias}][]"`);
      _posRest();
    });

    $(`#btn-${val.alias}`).on('click', function(event) {

      _calculate(`"payments[${val.alias}][]"`);
      _posRest();

    });
  });

},

floorswithzones:{},

on_click_call_account: function() {

  $.post(base_url+'mobilpos/ajax/createCallAccount',$(posmobil.posForm).serializeArray(), 

  function(json) {

      if (!json.result) {
        Pleasure.handleToastrSettings("Hata!", json.message, "error");
        return;
      }

      Pleasure.handleToastrSettings("Başarılı",json.message,"success");
    


  },"json");

},


getFloorsWithZones: function() {

  $('#tablelist').html("");
  $('#floorid').html("");

  $.post(base_url+'mobilpos/ajax/getFloorsWithZones',$(posmobil.posForm).serializeArray(), function(json) {
  
  if (typeof json.data.floors !== "undefined") {

    posmobil.floorswithzones = json.data.floors;
    $.each(json.data.floors,function (key,value) {
       $('#floorid').append('\
         <li class="list-group-item b-0 br-0">\
             <label>\
                <input type="radio" style="display:none" name="floor_id" value="'+value.id+'">'
                +value.title + 
              '</label>\
          </li>');
   });

  }

  $("#pos input[name=floor_id]:eq(0)")
  .prop("checked", true)
    .trigger('change');

  },"json");

  setInterval(function(){

    _home_reload();
       
  }, 10000);

},

get_zones: function() {

  $("#pos input[name=floor_id]:checked").closest('li').css('background', '#2e8cd3');
  $("#pos input[name=floor_id]:checked").closest('li').css('color', 'white');

  checked_floor_id = $("#pos input[name=floor_id]:checked").val();

  $("#pos input[name=floor_id]:not(:checked)").closest('li').css('background', 'white');
  $("#pos input[name=floor_id]:not(:checked)").closest('li').css('color', 'black');

  $('#zonelist').html("");

  $.each(posmobil.floorswithzones,function (key,value) {
    if (value.id == checked_floor_id) {
      $.each(value.zones, function(index, val) {
        $('#zonelist').append('\
          <li style="margin-right:1em;">\
            <label>\
                <input type="radio" style="display:none" name="zone_id" value="'+val.id+'">'
                 +val.title + 
              '</label>\
          </li>');
      });
    }
  });

  $("#pos input[name=zone_id]:eq(0)")
  .prop("checked", true)
    .trigger('change');
},

on_click_deliver: function() {

  let
  row = $(this).closest("li"),
  sale_id = row.data("sale-id"),
  product_id = row.data("product-id"),
  query_string = `sale_id=${sale_id}&product_id=${product_id}`;

   $.post(base_url+`mobilpos/ajax/productDelivered?${query_string}`,

    function(json) {

      if (!json.result) {
        Pleasure.handleToastrSettings("Hata!", json.message, "error");
        return;
      }

      row.fadeOut(300, function() {
        row.remove();
      });

    },"json");

},

on_click_account: function() {

  let
  row = $(this).closest("li"),
  sale_id = row.data("sale-id");

   $.post(base_url+`mobilpos/ajax/setViewedCallAccount?sale_id=${sale_id}`,

    function(json) {

      if (!json.result) {
        Pleasure.handleToastrSettings("Hata!", json.message, "error");
        return;
      }

      row.fadeOut(300, function() {
        row.remove();
      });

    },"json");

},

get_notification: function() {



  $.post(base_url+'mobilpos/ajax/getCompletedUnDeliveredProducts',

    function(json) {

      setTimeout(posmobil.get_notification, 3000);

      $('#product-list').html("");

      let products = json.data.products,
          call_accounts = json.data.call_accounts,
          additional_products = "";

      if (!json.result) {
        $('.counter').html(0);
        return;
      }

      $('.counter').html(products.length + call_accounts.length);

      $.each(products, function(index, product) {
        additional_products = " ";


        $.each(product.additional_products, function(index, add_product) {
          additional_products += `( ${add_product.title} )`;
        });


        $('#product-list').append('\
        <li class="list-group-item col-lg-12 col-md-12 col-sm-12 col-xs-12" data-sale-id ="'+product.sale_id+'" data-product-id="'+product.id+'">\
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">\
          <h2 class="fs-30">'+product.point_title+' ( '+product.zone_title+' )<h4>'+product.title+''+additional_products+'</h4><label class="label label-danger fs-20">'+product.quantity+' Adet</label></h2>\
        </div>\
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 text-right">\
        <a class="btn btn-success btn-success btn-xxl btn-ripple deliver"><i class="fa fa-thumbs-up" style="font-size: 2em;"></i></a>\
        </div>\
      </li>\
          ');
      });

      $.each(call_accounts, function(index, call_account) {

        $('#product-list').append('\
        <li class="list-group-item col-lg-12 col-md-12 col-sm-12 col-xs-12" data-sale-id ="'+call_account.id+'">\
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">\
          <h2 class="fs-30">'+call_account.point_title+'<h4>'+call_account.amount+' ₺</h4><label class="label label-success notification-label-account">HESAP İSTENİYOR</label></h2>\
        </div>\
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 text-right">\
        <a class="btn btn-primary btn-primary btn-xxl btn-ripple account"><i class="fa fa-check" style="font-size: 2em;"></i></a>\
        </div>\
      </li>\
          ');
      });


    },"json");

},

// SVG Icons Click Event

on_click_home: function() {
  $('#home').click();
},

// on_click_basket: function() {
//   $('#orderCheckTab').click();
// },

on_click_notification: function() {
  $('#tabNotification').click();
},

on_click_category_tab: function() {
  $('#category-box a[href^="#categoriedProductsTab"]').click(function(event) {
    let category_id = $(this).data('category-id');
    let category_name = $(this).data('category-name');

    $('#categorylist a[href="#categoriedProductsTab'+category_id+'"]').click();
    $('#tableName').click();
    $('#categorylist').parent().addClass('hidden');
    $('.category-name').text(category_name);
    $('.category-nav').removeClass('hidden');
  });


  $('.back-category-list').click(function(event) {
    $('#tabCategoryList').click();
  });
},

get_tab_cookie: function() {

  if (posmobil.cookieValue == "list") {

    $('#menu-list').css('background', '#2e8cd3');
    $('#menu-list h4').css('color', 'white');


    let src_tab = base_url+'assets/tab-icon-02.svg'
    let src_list = base_url+'assets/tab-list.svg'

    $('#menu-tab img').attr("src", src_tab);

    $('#menu-list img').attr("src", src_list);

  } else if (posmobil.cookieValue == "tab") {



    $('#menu-tab').css('background', '#2e8cd3');
    $('#menu-tab h4').css('color', 'white');

    let src_tab = base_url+'assets/tab-icon-01.svg'
    $('#menu-tab img').attr("src", src_tab);



  }

  $('#menu-tab').click(function(event) {
    setCookie('menu','tab',365);
    //document.cookie = "menu=tab";
    location.reload();
  });

  $('#menu-list').click(function(event) {
    setCookie('menu','list',365);
    //document.cookie = "menu=list";
    location.reload();

  });

},

on_click_cancel_order: function() {

  if (posmobil.isTablet) {
    $('#tableName').click();
    order_hidden();
    clearTimeout(posmobil.completed_order_timer);
    return;
  }

  reload_tables();
  $('#home').click();
  order_hidden();
  clearTimeout(posmobil.completed_order_timer);

},

on_click_pos_order: function() {

  $('#pos-create-order').click();

},

on_click_back_order_tab: function() {

  $('#orderCheckTab').click();

},

on_click_left_sidebar_button: function() {
  $('.wrapper').toggleClass('active-left');
},

on_click_sidebar_button: function() {
  $('.wrapper').toggleClass('active');
},

on_click_print_discount: function() {

  $.post(base_url+'mobilpos/ajax/loginForDiscountKey', $(posmobil.posForm).serializeArray(), 

    function(json) {

      if (!json.result) {
        Pleasure.handleToastrSettings("Hata!", json.message, "error");
        return;
      }

      $('input[name="discount_key"]').val(json.data.discount_key);
      posmobil.current_sale.discount_amount = json.data.discount_amount;
      posmobil.current_sale.discount_type = json.data.discount_type;
      $('#modal-discount_login').modal("toggle");
      $('#modal-discount_login input').val("");

      _calc()

    },"json");
},

on_click_change_pos: function() {

  $.post(base_url+'mobilpos/ajax/removePosSetup', $(posmobil.posForm).serializeArray(), 

    function(json) {

      if (!json.result) {
        Pleasure.handleToastrSettings("Hata!", json.message, "error");
        return;
      }

      let device = $('#change_pos').data("device");

      if (device === "garson") {
        location.href = base_url + "mobilpos?device=garson";
      } else if (device === "tablet") {
        location.href = base_url + "mobilpos/tablet?device=tablet";
      }

    },"json");
},

sameProductExistsInBasket: function (basket_product_row, selected_stocks, selected_additional_products, callback) {

  let
  hasSameStock = false,
  hasSameAdditionalProducts = false,
  countSameStock = 0,
  countSameAdditionalProducts = 0,
  basket_stocks = $("input[name*='[stocks]']", basket_product_row),
  basket_additional_products = $("input[name*='[additional_products]']", basket_product_row),
  basket_gift = $("input[name*='[quantity]']", basket_product_row).attr("is_gift"),
  basket_interim = $("input[name*='[quantity]']", basket_product_row).attr("interim_paid");


  if (basket_gift == 1) {
    return;
  }

  if (basket_interim == 1) {
    return;
  }

  // if (posmobil.interim_paid_total == 1 ) {

  //   return;
  // }

  if (selected_stocks.length) {
    if (selected_stocks.length === basket_stocks.length ) {
      $.each(selected_stocks, function(index, selected_stock) {
        $.each(basket_stocks, function(index, basket_stock) {
          if (parseInt($(selected_stock).attr('data-stock-id')) === parseInt($(basket_stock).val())) {
            countSameStock++;
            return;
          }
        });
      });
      if (countSameStock === selected_stocks.length) {
        hasSameStock = true;
      }
    }
  } else if (!basket_stocks.length) {
    hasSameStock = true;
  }

  if (!hasSameStock) {
    callback(false);
    return;
  }

  if (selected_additional_products.length) {
    if (selected_additional_products.length === basket_additional_products.length ) {
      $.each(selected_additional_products, function(index, selected_additional_product) {
        $.each(basket_additional_products, function(index, basket_additional_product) {
          if (parseInt($(selected_additional_product).attr('data-additional-id')) === parseInt($(basket_additional_product).val())) {
            countSameAdditionalProducts++;
            return;
          }
        });
      });
      if (countSameAdditionalProducts === selected_additional_products.length) {
        hasSameAdditionalProducts = true;
      }
    }
  } else if (!basket_additional_products.length) {
    hasSameAdditionalProducts = true;
  }

  if (hasSameAdditionalProducts) {
    callback(basket_product_row);
  } else {
    callback(false);
  }
},

on_click_print_addition: function() {

  var 
  sale_id = $('input[name="sale_id"]').val(),
  table_id = $('input[name="table_id"]').val();

  var obj = new Object();
  obj.sale_id = sale_id;
  obj.table_id = table_id;
  var jsonString = JSON.stringify(obj);

  $('#mobile-adisyon-uyarlama').text(jsonString); 


  // if(sale_id) {

  //   $.post(base_url+'mobilpos/ajax/orderCheck', $(posmobil.posForm).serializeArray(), 

  //   function(json) {

  //   _price();

  //   get_products_table(json.data.sale_products);

  //   $('#addition-tab .invoice-id .invoice-no').html(json.data.sale.id);
  //   $("#addition-tab .invoice-table-name").html(json.data.sale.point_title);
  //   $("#addition-tab .invoice-zone-name").html(json.data.sale.zone_title);

  //   let date = getDateNow();

  //   $(' #addition-tab .invoice-datetime').html(date);
  //   $('#adisyonToplam').html(json.data.amount+'₺');
  //   $('#mobile-adisyon-uyarlama').text("");                    

  //   // $('#addition-tab').print();

  //   sale_products = json.data.sale_products;
  //   let total = subtotal = 0;
  //   product = "",
  //   productItemStr = "",
  //   addTotal = totalPaid = checkTotal = 0;       
    
  //   $.each(sale_products, function(index, val) {


  //     total = sum_calc(val.price, val.tax);
  //     sumTotal = val.quantity* parseFloat(total);

  //     let addTotal = 0;

  //     $.each(val.additional_products, function(index, addVal) {
  //       addTotal = sum_calc(addVal.price, addVal.tax);
  //       productItemStr +=  `<CENTER>${addVal.title};;${val.quantity};;${addVal.price};;${addTotal.toFixed(2)}\n`;

  //     });


  //     product +=`<CENTER>${val.title};;${val.quantity};;${val.price};;${sumTotal.toFixed(2)}\n`;

  //     sumAddTotal = val.quantity* parseFloat(addTotal);

  //    // checkTotal += parseFloat(sumAddTotal) + parseFloat(sumTotal);

  //   });

  //   // var discount_amount = 0;

  //   // if (posmobil.current_sale.discount_type == "amount") {
  //   //   discount_amount = `${posmobil.current_sale.discount_type}TL`
  //   // } else if (posmobil.current_sale.discount_type == "percent") {
  //   //   discount_amount = `%${posmobil.current_sale.discount_type}`
  //   // }

  //   checkTotal = json.data.amount - json.data.interim_payment_amount;
  //   checkTotal = checkTotal.toFixed(2)

  //   //Mobil için tasarım uyarlaması

  //   var commandsToPrint = "";

  //   commandsToPrint =
  //                                   `<LEFT>${json.data.sale.point_title}\n` +
  //                                   `<RIGHT>${json.data.sale.id}\n`+
  //                                   `<LEFT>${json.data.sale.zone_title}\n` +
  //                                   `<RIGHT>${date}\n<BR>` +
  //                                   "<CENTER>URUN;;MİKTAR;;FIYAT;;TUTAR\n" +
  //                                   `<CENTER>${product}\n` +  
  //                                   `<CENTER>${productItemStr}\n` +  
  //                                   `<CENTER><BOLD>Toplam : ${json.data.amount} <BR>\n` +
  //                                   `<CENTER><BOLD>Ara Ödeme : ${json.data.interim_payment_amount}<BR>\n` +
  //                                   `<CENTER><BOLD>Genel Toplam : ${checkTotal} <BR>\n` +
  //                                   "<BR>\n" +
  //                                   "<CUT>\n" +
  //                                   "<DRAWER>\n" // OPEN THE CASH DRAWER
  //                           ;

  //     var mapObj = {
  //       ş:"s",
  //       ü:"u",
  //       ö:"o",
  //       ı:"i",
  //       Ş:"S",
  //       Ü:"U",
  //       Ö:"O",
  //       İ:"i",
  //       Ğ:"G",
  //       g:"g"

  //     };

  //     commandsToPrint = commandsToPrint.replace(/ş|ü|ö|ı|Ş|Ü|Ö|İ|Ğ|g/gi, function(matched) {
  //       return mapObj[matched];
  //     });


  //                          


  //   },"json");

  // } else {

  //   Pleasure.handleToastrSettings('Hata!',"Sipariş verilmedi.", 'error');

  // }

},

on_click_icon_remove: function() {

  tr = $(this).parents('tr');

  let interim_paid = $("input[name*='[quantity]']", tr).attr("interim_paid");
  let completed = $("input[name*='[quantity]']", tr).attr("completed");

  if (interim_paid > 0) {
    Pleasure.handleToastrSettings('Hata!', "Ara ödemesi yapılan ürün silinemez", 'error');
  } else if (completed > 0) {
    if (!posmobil.Helpers.hasPermission("pointofsales.forceproductdecrement")) {
      Pleasure.handleToastrSettings('Hata!', "Mutfak tarafından hazırlanan ürün silinemez", 'error');
    }
  } else {
    $(this).closest('tr').remove();
    _calc();

  }

},

get_tables: function() {

  $("#pos input[name=zone_id]:checked").parent().css('border-bottom','10px solid red');
  $("#pos input[name=zone_id]:checked").closest('li').css('line-height','40px');
  $("#pos input[name=zone_id]:not(:checked)").parent().css('border-bottom', '0');
  $("#pos input[name=zone_id]:not(:checked)").closest('li').css('line-height','0');

  // $('#categorylist li:first').addClass('active');
  // $('#categoriedProductsTab1').addClass('active');
    
    $('#tablelist').html("");
    var amount = 0.00;
    var statu = 0;

  $.post(base_url+'mobilpos/ajax/getTable', $(posmobil.posForm).serializeArray(),
   function(json) {
    $.each(json.data,function (key,value) {
      if(value.amount !== null && value.amount >= 0) {
        amount = value.amount - value.interim_payment_amount;
        statu = 1;
      }
      else {
        amount = "0.00";
        statu = 0;
      }

      amount = parseFloat(amount);
      
      $('#tablelist').append('\
        <div class="table-image" data-table-statu="'+statu+'">\
            <label for="input'+value.id+'">\
              <input id="input'+value.id+'" type="radio" class="hidden" name="table_id" value="'+value.id+'">\
              <div>\
                <span class="table-name">'+ value.title + '</span>\
                <span class="table-price" id="tablePrice-'+value.id+'">'+ amount.toFixed(2) +'₺</span>\
              </div>\
            </label>\
        </div>\
        ');
  });

  $('.table-image').each(function() {
    if($(this).attr('data-table-statu') == 0) {

      $(this).css('background', 'white');
      $(this).css('color','black');

    } else {
      $(this).css('background', '#EB212F');
      $(this).css('color', 'white');

    }
  });

  $('.touch-swipe').click('on', function() {
    if ($('.wrapper').hasClass('active-left')) {
      $('.wrapper').removeClass('active-left');
    } else if ($('.wrapper').hasClass('active')) {
      $('.wrapper').removeClass('active');
    }
  });
    
  }, "json");


  $('.touch-swipe').swipe({

    allowPageScroll: "vertical",

    // swipe:function(event, direction, distance, duration, fingerCount, fingerData) {
    //   let zones = $('#zonelist li input');
    //   let zone_length = zones.length;
    //   let current_index;

    //   console.log(direction);


    //   $.each(zones, function(index, input) {        
        
    //     if ($(this).prop('checked')) {
    //       current_index = index;
    //       return;
    //      }
    //   });

    //   if (direction === "left" && current_index < zone_length) {
    //     $('input[name=zone_id]:eq('+(current_index + 1)+')').prop('checked', true).trigger('change');
    //   } else if (direction === "right" && current_index > 0 ) {
    //       $('input[name=zone_id]:eq('+(current_index - 1)+')').prop('checked', true).trigger('change');
    //   }

    //   // if (direction === "down" || direction === "up") {
    //   //   return false;
    //   // }


    // },

    swipeLeft: function() {

     let current_index = touchSwipeCurrentIndex();

    let zones = $('#zonelist li input');
    let zone_length = zones.length;

      if (current_index < zone_length) {

        $('input[name=zone_id]:eq('+(current_index + 1)+')').prop('checked', true).trigger('change');

      }

    
    },

    swipeRight: function() {

      let current_index = touchSwipeCurrentIndex();

      if (current_index > 0) {
        $('input[name=zone_id]:eq('+(current_index - 1)+')').prop('checked', true).trigger('change');
      }

    },


  });

},

current_sale : {},

on_click_table:function(e){
  e.preventDefault();

  if (posmobil.cookieValue == "list") {
    $('#tabCategoryList').click();
  } else {
    $("#tableName").click();
  }

  //Tablonun ve hidden inputların temizlenmesi

  $('#tblProduct tbody').html("");
  $('#hiddenInputs input[name=table_id]').val("");
  $('#hiddenInputs input[name=sale_id]').val("");
  $('#hiddenInputs input[name=discount_key]').val("");

  //Hesap ekranı html temizleme

  $('#subtotalCheck').html("");
  $('#kdv').html("");
  $('#sumCheck').html("");
  $('#subtotal').text("0.00₺");
  $('#sumAmount').text("0.00₺");
  $('#posRest').html("");

  var tableId = $(this).val();
  $('#hiddenInputs input[name=table_id]').val(tableId);


  $("#product-tab ul li a").click(function() {
    $(this).attr("href");
  });

  $("#product-tab ul li:first-child a").click();

  $('#categorylist').animate({
    scrollLeft: 0
  }, 200);

  // Masa adının header kısmına basılması.
  $('.home-table').html($(this).parent().find("span").html());
  $('.order-complete-title').html($(this).parent().find("span").html());


  posmobil.current_sale = {}; 

  $.post(base_url+'mobilpos/ajax/getTableStatus', $(posmobil.posForm).serializeArray(),

  function(json) {
    var sale_id = json.data.sales_id;
    $('#hiddenInputs input[name=sale_id]').val(sale_id);

    if(sale_id) {

    $.post(base_url+'mobilpos/ajax/orderCheck', $(posmobil.posForm).serializeArray(), 

    function(json) {

      $('#tblProduct tbody').html("");

      if (json.result) {
        posmobil.current_sale = json.data.sale;

        get_products(json.data.sale_products, json.data.interim_payment_amount, json.data.payment_types);

        _calc();
      }

    },"json");

  } 

  },"json");

  $(this).prop('checked',false);

  $('#product-tab .product-list').swipe({

     // swipe:function(event, direction, distance, duration, fingerCount, fingerData) {

     //  let categories = $('#categorylist li'),
     //  category_length = categories.length -1,
     //  current_category_index = 0;


     //  $.each(categories, function(index, val) {        
        
     //    if ($(this).hasClass('active')) {
     //      current_category_index = index;
     //      $(this).removeClass('active');
     //      return;
     //     }
     //  });

     //  if (direction === "left" && current_category_index < category_length) {

     //    $('#categorylist li:eq('+(current_category_index + 1)+') a').click();

     //    let title = $('#categorylist li:eq('+(current_category_index + 1)+') p').text();

     //    $('.category-nav .category-name').html(title);

     //    var windowWidth = $(window).width(),
     //    activePosLeft = $('#categorylist > li.active').position().left,
     //    activeWidth = $('#categorylist > li.active').width();

     //    if ( activePosLeft + activeWidth > windowWidth) {
            
     //      var difference = activePosLeft + activeWidth - windowWidth ; 

     //      $('#categorylist').animate({
     //          scrollLeft: "+=" + difference
     //      }, 200);

     //    }

     //  } else if (direction === "right" && current_category_index > 0 ) {
     //      $('#categorylist li:eq('+(current_category_index - 1)+') a').click();

     //      let title = $('#categorylist li:eq('+(current_category_index - 1)+') p').text();

     //      $('.category-nav .category-name').html(title);

     //      var activePosLeft = $('#categorylist > li.active').position().left;

     //      if (activePosLeft < 0) {

     //        $('#categorylist').animate({
     //            scrollLeft: "+=" + activePosLeft
     //        }, 200);

     //      }

     //  } else {
     //     $('#categorylist li:eq('+(current_category_index)+')').addClass('active');
     //  }

      //  if (direction === "down" || direction === "up") {
      //   return false;
      // }

     allowPageScroll: "vertical",
  



    swipeLeft: function() {

      let categories = $('#categorylist li'),
      category_length = categories.length -1,
      current_category_index = 0;


      $.each(categories, function(index, val) {        
        
        if ($(this).hasClass('active')) {
          current_category_index = index;
          $(this).removeClass('active');
          return;
         }
      });

      if (current_category_index < category_length) {

        $('#categorylist li:eq('+(current_category_index + 1)+') a').click();

        let title = $('#categorylist li:eq('+(current_category_index + 1)+') p').text();

        $('.category-nav .category-name').html(title);

        var windowWidth = $(window).width(),
        activePosLeft = $('#categorylist > li.active').position().left,
        activeWidth = $('#categorylist > li.active').width();

        if ( activePosLeft + activeWidth > windowWidth) {
            
          var difference = activePosLeft + activeWidth - windowWidth ; 

          $('#categorylist').animate({
              scrollLeft: "+=" + difference
          }, 200);
        }
      } else {
        $('#categorylist li:eq('+(current_category_index)+')').addClass('active');

      }

    },

    swipeRight: function() {

      let categories = $('#categorylist li'),
      category_length = categories.length -1,
      current_category_index = 0;


      $.each(categories, function(index, val) {        
        
        if ($(this).hasClass('active')) {
          current_category_index = index;
          $(this).removeClass('active');
          return;
         }
      });

      if (current_category_index > 0) {

          $('#categorylist li:eq('+(current_category_index - 1)+') a').click();

          let title = $('#categorylist li:eq('+(current_category_index - 1)+') p').text();

          $('.category-nav .category-name').html(title);

          var activePosLeft = $('#categorylist > li.active').position().left;

          if (activePosLeft < 0) {

            $('#categorylist').animate({
                scrollLeft: "+=" + activePosLeft
            }, 200);

          }
      } else {
        $('#categorylist li:eq('+(current_category_index)+')').addClass('active');

      }

    },

  });
},

on_click_backHome: function(){

  order_hidden()

  $("#home").click();
  $("input[name=table_id]:checked").prop('checked', false);
},

on_click_backTable: function(){

  if (posmobil.cookieValue == "tab") {

    $("#tableName").click();

    // $("#categorylist li a").click(function() {
    //   $(this).attr("href");
    // });

    // $("#categorylist li:first-child a").trigger('click');

    // $('#categoriedProductsTab1').addClass('active');

    $('.category-nav').addClass('hidden');

  } else {
    $('#tabCategoryList').click();
  }

},

on_click_backHomePage: function(){

  $("#home").click();

},

  selectedProductID: null,
  productList:{},

on_click_product: function() {


  posmobil.globalIndex = 0;

  $('#chooseItems').removeClass('disabled-none');
  $('#additionalItems').removeClass('disabled-none');

  $('#sel1').html("");
  $('#product_notes').html("");

  $('#modal-product-add').text("EKLE");


   $.post(base_url+'mobilpos/ajax/getProductsOptions', {product_id : $(this).find("input[name=product_id]").val() },
  
    function(json) {

      posmobil.productList = json.data.products;

      $.each(json.data.products,function (key,value) {
        let price = parseFloat(value.price) + parseFloat(value.price*value.tax/100);
        $('#sel1').append('\
          <option class="portion-select" data-product-id="'+value.id+'">'+value.title +'( '+ price.toFixed(2)+'₺ )</option>\
        ');
      
    });     

      $('#sel1').trigger('change');
     
   },"json");

  setTimeout(calcModalBodyHeight,30);


},

on_change_products_options: function() {


  var 
  id = $('option:selected', this).attr('data-product-id'),
  stock = posmobil.productList[id].stocks,
  additional = posmobil.productList[id].additional_products;
  quick_note = posmobil.productList[id].quick_notes;
  package_products = posmobil.productList[id].products;
  $('#chooseItems').html("");
  $('#additionalItems').html("");
  $('#menuItems').html("");
  $('#product_notes').html("");
  $('#quick_notes').html("");
  $('#productQuantity').val(1);

  $.each(stock,function (key,value) {
    $('#chooseItems').append('\
      <li>\
        <label class="reverse-checbox">\
              <input type="checkbox" data-stock-id="'+value.id+'">\
              <span>'+value.title+'</span>\
        </label>\
      </li>\
    ')    
  });

  $.each(additional,function (key,value) {

    add_price = priceTax(value.price, value.tax);

    $('#additionalItems').append('\
      <li>\
        <label class ="checkboxer checkboxer-deep-orange form-inline">\
              <input type="checkbox" data-additional-id="'+value.id+'" id="inputFor'+value.id+'">\
              <label for="inputFor'+value.id+'">'+value.title+'<small>('+add_price.toFixed(2)+')</small></label>\
        </label>\
      </li>\
    ')   
  });

  $.each(quick_note,function (index, val) {
    $('#quick_notes').append(`
      <button type="button" class="btn btn-primary addtoquickNotes" style="margin-left: 10px;margin-top: 10px;" value="${val.title}">${val.title}</button>\
    `)
  });
  
  if (typeof package_products !== "undefined") {

    package_products = $.map(package_products, function( item, i ) {
      return item;
    });

    let 
    length = package_products.length,
    comma = ",";

    $.each(package_products, function(index, val) {
      if (index === length -1) {
        comma = "";
      }
      $("#menuItems").append(
      `<span> ${val.title} ${comma} </span>`
      );  
    });

  }

  


  var 
  tax = parseFloat(posmobil.productList[id].tax),
  price = parseFloat(posmobil.productList[id].price),
  sumPrice = 0;

  sumPrice = price + (tax*price) / 100;
  sumPrice = parseFloat(sumPrice);

},

globalIndex: 0,
is_completed: 0,
is_interim:0,

on_click_product_add: function() {
  $("body").on("click","#modal-product-add", function() {

  
    let modal = $("#productQuantityModal");
    let selected_stocks = $('#chooseItems input[type=checkbox]:checked');
    let selected_additional_products = $('#additionalItems input[type=checkbox]:checked');
    let sameProductInBasket = false;

    product_id = $('#sel1 option:selected').attr('data-product-id');
    count = $("#productQuantityModal").find("#productQuantity").val(); 
    product_name = $('#sel1 option:selected').html();
    productItemStr = "";

    var product_notes = $('input[name*="[sound_notes]"], input[name*="[text_notes]"]', modal);

    if (posmobil.is_completed > count) {
      if (!posmobil.Helpers.hasPermission("pointofsales.forceproductdecrement")) {
        Pleasure.handleToastrSettings('Hata!', "Mutfak tarafından hazırlanan ürün adeti azaltılamaz", 'error');
        return;
      }
    }

    if (posmobil.is_interim > count) {
      Pleasure.handleToastrSettings('Hata!', "Ara ödemesi yapılan sipariş adeti azaltılamaz", 'error');
      return;
    }
    

    var tax = parseFloat(posmobil.productList[product_id].tax);
    var price = parseFloat(posmobil.productList[product_id].price);
    sumPrice = price + (tax*price) / 100;
    sumPrice = round(sumPrice,2);

    $.each($(`#tblProduct > tbody input[name*="[product_id]"][value="${product_id}"]`), function(index, basket_product) {

      if (sameProductInBasket !== false)
        return;

      posmobil.sameProductExistsInBasket($(basket_product).closest("tr"), selected_stocks, selected_additional_products, function (product_in_basket) {
        sameProductInBasket = product_in_basket;
      });
    });

    function chooseAddItems (index) {

      //Seçilmek istenen stoklar

      chooseItems = $('#chooseItems input[type=checkbox]:checked');

      $.each(chooseItems,function(key,value) {

        stock =  $(this).attr('data-stock-id');
        
        productItemStr += `<input type="hidden" name="products[${index}][stocks][${stock}]" value="${stock}">`;
        
      });
       

      //EK ürünler seçimi 

      chooseAdditionalItems = $('#additionalItems input[type=checkbox]:checked');

      var addPrice = 0;
      var addTax = 0;
      var sumAddPrice = 0;

      $.each(chooseAdditionalItems,function(key,value) {

        additionalItem =  $(this).attr('data-additional-id');
        tempAddItem = posmobil.productList[product_id].additional_products;
        addPrice = parseFloat(tempAddItem[additionalItem].price);
        addTax = parseFloat(tempAddItem[additionalItem].tax);
        sumAddPrice += parseFloat(addPrice) + (parseFloat(addTax)*parseFloat(addPrice))/100;
        sumAddPrice = round(sumAddPrice,2);

        productItemStr +=  '<input type="hidden" name="products['+index+'][additional_products]['+additionalItem+']" value="'+additionalItem+'" price="'+addPrice+'" tax="'+addTax+'">';


      });    

       $.each(product_notes, function(key, note_input) {
          let
          input = $(note_input),
          input_name = input.attr("name"),
          input_value = input.val(),
          sound_url = input.data("url");

          if (input_name.indexOf("[text_notes]") > -1) {
            productItemStr += `<input type="hidden" name="products[${index}]${input_name}" value="${input_value}">`;
          } else if (input_name.indexOf("[sound_notes]") > -1) {
            productItemStr += `<input type="hidden" name="products[${index}]${input_name}" value="${input_value}" data-url="${sound_url}">`;
          }
        });
       
       return sumAddPrice;
    
    }

    
    if(posmobil.globalIndex === 0) {

    index  = (+ new Date());

    if (sameProductInBasket) {
      index = sameProductInBasket.data('index');
    }
    
    tempSumAddPrice = chooseAddItems(index);

    totalPrice = parseFloat(tempSumAddPrice) + parseFloat(sumPrice);
    totalPrice = totalPrice * count;

    if (sameProductInBasket) {

      let
      basket_product_quantity = $("input[name*='[quantity]']", sameProductInBasket).val();
      basket_product_quantity = parseInt(basket_product_quantity);
      oldPrice = $(".total-sum-price", sameProductInBasket).html();
      sumTotalPrice = parseFloat(oldPrice) + parseFloat(totalPrice);
      count = parseInt(count);

      $("input[name*='[additional_products]']", sameProductInBasket).remove();
      $("input[name*='[stocks]']", sameProductInBasket).remove();

      $("#count", sameProductInBasket).html(basket_product_quantity + count);
      $("input[name*='[quantity]']", sameProductInBasket).val(basket_product_quantity + count);
      $(".total-sum-price", sameProductInBasket).html(sumTotalPrice.toFixed(2)+"₺");
      sameProductInBasket.append(productItemStr)
      // console.log(productItemStr);

    } else {

    //Ürünlerin tabloya aktarılması

    $('#tblProduct > tbody').append('\
      <tr data-index="'+index+'" class="pr-container">\
        <td>'+product_name+'</td>\
        <td id="count">'+count+'</td>\
        <input type="hidden" name="products['+index+'][product_id]" value="'+product_id+'" price="'+price+'" tax="'+tax+'">\
        <input type="hidden" name="products['+index+'][quantity]" value="'+count+'">\
        <input type="hidden" name="products['+index+'][sale_product_id]" value="">\
        '+productItemStr+'\
        <td class="total-sum-price">'+totalPrice.toFixed(2)+'₺</td>\
        <td class="text-right"><i class="iconRemove btn btn-danger glyphicon glyphicon-remove br-30"></i> <i id="iconEdit" class="btn btn-blue glyphicon glyphicon-pencil pull-right br-30" data-toggle="modal" data-target="#productQuantityModal"></i></td>\
      </tr>\
      '); 


    posmobil.Helpers.visible("#tblProduct .iconEdit", posmobil.Helpers.hasPermission("pointofsales.productedit"));
    posmobil.Helpers.visible("#tblProduct .iconRemove", posmobil.Helpers.hasPermission("pointofsales.productdelete"));


    }


  } else {


    table = $('#tblProduct tr[data-index='+posmobil.globalIndex+']');

    table.find('#count').text(count);
    $('input[name="products['+posmobil.globalIndex+'][quantity]"]' ,table).val(count);

    //remove additional and stocks row
    $('input[name^="products['+posmobil.globalIndex+'][stocks]"]').remove();
    $('input[name^="products['+posmobil.globalIndex+'][additional_products]"]').remove();
    $('input[name*="[sound_notes]"]').remove();
    $('input[name*="[text_notes]"]').remove();



    editSumAddPrice = chooseAddItems(posmobil.globalIndex);
    quantity = $('#productQuantity').val();
    tablePrice = (parseFloat(sumPrice) + parseFloat(editSumAddPrice))*quantity;

    $('.total-sum-price', table).html(tablePrice.toFixed(2));
   
    $(table).append(''+productItemStr+'');

  }

    $('#chooseItems').html("");
    $('#additionalItems').html("");
    $('#product_notes').html("");
    $('#productQuantity').val(1);
    $('#sel1').html("");

    _calc();

    $('.product-footer').removeClass('hidden');
    $('#productlist').addClass('product-list-height');

   });

  
},

on_click_iconEdit: function() {

  productList = {};
  $('#sel1').html("");
  $('#product_notes').html("");

  var 
  row = $(this).closest("tr"),
  id = row.attr('data-index'),
  productId = $('input[name="products['+id+'][product_id]"]', row).val();
  count = $('input[name="products['+id+'][quantity]"]', row).val();
  posmobil.globalIndex = id;
  posmobil.is_completed = $('input[name="products['+id+'][quantity]"]', row).attr('completed');
  posmobil.is_interim = $('input[name="products['+id+'][quantity]"]', row).attr('interim_paid');
  sale_product_id = $('input[name="products['+id+'][sale_product_id]"]', row).val();
  product_notes = $('input[name*="[sound_notes]"], input[name*="[text_notes]"]', row);
  $('#modal-product-add').html("DÜZENLE");

  //bu çekilen ürünler. bunlar tekrar yüklenecek ve hidden inputtakiler ile karşılaştırılacak. önceden seçili olanlar eklenecek

  $.post(base_url+'mobilpos/ajax/getProductsOptions', {product_id : productId, saleProductId : sale_product_id },
  
    function(json) {
     
      posmobil.productList = json.data.products;

      value = posmobil.productList[productId];

      sale_has_products = json.data.sale_has_products;

      if (typeof sale_has_products !== "undefined") {

        $.each(sale_has_products, function(index, val) {

            interim_paid_total = interim_count_calc(val, json.data.payment_types);
            if (interim_paid_total > 0 && val.completed ) {
              $('#chooseItems').addClass('disabled-none');
              $('#additionalItems').addClass('disabled-none');
            } else if (interim_paid_total != 0 && val.completed != 0) {
              $('#chooseItems').removeClass('disabled-none');
              $('#additionalItems').removeClass('disabled-none');
            } else {
              $('#chooseItems').removeClass('disabled-none');
              $('#additionalItems').removeClass('disabled-none');
            }
        });
      }

      $('#sel1').append('<option class="portion-select" data-product-id="'+value.id+'">'+value.title+'</option>');    

      $('#sel1').trigger('change');

      let stocks =  $('input[name^="products['+id+'][stocks]"]', row);
      $.each(value.stocks, function(index, val) {
        $.each(stocks, function(index, value) {
          stockVal = $(value).val(); 
          if(stockVal == val.id) {
            $('#chooseItems input[data-stock-id='+val.id+']').prop('checked', true);
          }
        });
      });


      let additional_items =  $('input[name^="products['+id+'][additional_products]"]', row);
      $.each(value.additional_products, function(index, val) {
        $.each(additional_items, function(index, value) {
          additonalVal = $(value).val(); 
          if(additonalVal == val.id) {
            $('#additionalItems input[data-additional-id='+val.id+']').prop('checked', true);
          }
        });
      });


      $.each(product_notes, function(index, note_input) {
          let
          input = $(note_input),
          input_name = input.attr("name"),
          input_value = input.val(),
          template = "";

          if (input_name.indexOf("[text_notes]") > -1) {
            template = posmobil.ProductNotes.Templates.newTextNote(input_value);
          } else if (input_name.indexOf("[sound_notes]") > -1) {
            template = posmobil.ProductNotes.Templates.newSoundNote({
              "id": input_value,
              "sound": input.data("url")
            });
          }

          posmobil.ProductNotes.Templates.newNote(template);
        });


      $('#productQuantity').val(count);

     
   },"json");

  setTimeout(calcModalBodyHeight,30);

},

get_categoried_products: function(){

    $.post(base_url+'mobilpos/ajax/getCategoriedProducts', $(posmobil.posForm).serializeArray(),

    function(json) {

    categories = $.map(json.data, (value, key) => { 
        value.sort = Number(value.sort);
        if (value.sort === 0) {
          value.sort = 9999999;
        }
        return [value]; 
      });

    categories.sort((a,b) => {
      if (a.sort === b.sort) {
          if (a.title.toLowerCase() > b.title.toLowerCase()) return 1;
          if (a.title.toLowerCase() < b.title.toLowerCase()) return -1;
      } else {
          if (a.sort > b.sort) return 1;
          if (a.sort < b.sort) return -1;
      }

      return 0;
    });

    $.each(categories,function (key,value) {
      $('#categorylist').append('\
        <li><a href="#categoriedProductsTab'+value.id+'" data-category-id="'+value.id+'" data-toggle="tab" "style="vertical-align: middle;""><p>'+value.title+'</p></a></li>\
      ');
      $('#category-box').append(`
        <a href="#categoriedProductsTab${value.id}" data-category-id="${value.id}" data-category-name = "${value.title}" data-toggle="tab" "style="vertical-align: middle;"">\
          <div class="category-image" style="background-image: url(${base_url}${value.image})">
            <p>${value.title}</p>
          </div>
        </a> 
      `);  
    });

    $.each(json.data,function (key,value) {

      $('#productlist .tab-content ').append('\
        <div class="tab-pane" id="categoriedProductsTab'+value.id+'">\
        </div>\
          ');

    products = $.map(value.products, (value, key) => { 
    value.sort = Number(value.sort);
    if (value.sort === 0) {
      value.sort = 9999999;
    }
      return [value]; 
    });

    products.sort((a,b) => {
    if (a.sort === b.sort) {
        if (a.title.toLowerCase() > b.title.toLowerCase()) return 1;
        if (a.title.toLowerCase() < b.title.toLowerCase()) return -1;
    } else {
        if (a.sort > b.sort) return 1;
        if (a.sort < b.sort) return -1;
    }

      return 0;
    });

    $.each(products,function (key,product) {

      let 
      price = parseFloat(product.price),
      tax = parseFloat(product.tax);

      let total_price = price + price*tax/100;
      total_price = round(total_price,2);

      $("#categoriedProductsTab"+value.id).append('\
      <div class="product table-product" style="background-image : url('+base_url+product.image+');" data-toggle="modal" data-target="#productQuantityModal">\
      <div class="product-content">\
      <span class="product-title">'+product.title+'</span>\
      <span class="product-price" style="position:absolute; top:0; right:0;">'+total_price.toFixed(2)+' ₺</span>\
      <input type="hidden" class="product-selector-id" name="product_id" value="'+product.id+'" disabled>\
      </div>\
      </div>');

      
    });
    });
    
    }, "json");

},

on_click_order_check: function() {

  _calc();
  order_hidden();

  $('#backCategory').trigger('click');
  $("#orderCheckTab").click();
  $('#interimPayment > tbody').html('');


  let winHeight = $( window ).height(),
      footerHeight = $('.order-footer').height();

  let tableHeight = winHeight - (61 + footerHeight);

  $('.tbl-product').css({
    'max-height': tableHeight,
    'overflow-y': 'scroll'
  });

  let sale_id = $('#hiddenInputs input[name=sale_id]').val();

  if (sale_id) {
    $('#cancel-sale').removeClass('disabled'); 
  } else {
    $('#cancel-sale').addClass('disabled');
  }


},

completed_order_timer: null,

onClick_complete: function(){

  $("body").on("click", "#pos-complete, #pos-create-order",function(e) {

    e.preventDefault();

    var is_order = $(this).attr('id') == "pos-create-order" ? 1 : 0;

    $('#pos-order').attr("disabled", true);
    $('#pos-create-order').attr("disabled", true);
    $('#pos-complete').attr("disabled", true);

    $.post(base_url+'mobilpos/ajax/complete?action=' + (is_order ? 'order' : 'checkout'),  $('#pos').serialize(), function(result) {
      _loading("delete");

      $('#pos-create-order').removeAttr('disabled');
      $('#pos-complete').removeAttr('disabled');

      if (!result.result) {
        Pleasure.handleToastrSettings('Hata!', result.message, 'error');
        return;
      }

      $('input[name="discount_key"]').val("");
      $('input[name*="sound_notes"]').remove();
      $('input[name*="text_notes"]').remove();

      if (typeof result.data.new_sale_id !== "undefined") {
        $("input[name=sale_id]", "#pos").val(result.data.new_sale_id);
      }

      $.each(result.data.inserted_product_ids, function(key, val) {
        $(`input[name="products[${val.index}][sale_product_id]"]`, "#tblProduct").val(val.sale_product_id);
      });

      if (is_order == 0 ) {
        Pleasure.handleToastrSettings('Başarılı', result.message, 'success');
        setTimeout(function() {
          location.reload();
          posmobil.is_interim = 0;
        }, 1000);
      } else {

        $('#tabOrderComplete').click();
        posmobil.completed_order_timer = setTimeout(function() {

          posmobil.on_click_cancel_order();
          
        }, 3000);

      }

      $('#pos-order').removeAttr('disabled');
      return;

    }, "json");

    $('#pos-create-order').removeAttr('disabled');

    
  });
},

on_click_order_complete : function() {

  $('#checkScreen').click();

  $('#backOrderCheckTab').click(function() {

  $('#orderCheckTab').click();

  });

  $('#backHomePage1').click(function() {
    $('#home').click();
  });

  //hesap ödeme ekranı temizlenmesi

  $.each(posmobil.payment_types, function(index, val) {
    
    $('input[name="payments['+val.alias+'][]"]').val(0);

  });

  $('#posRest').html("0.00₺");
  $('#discountSpan').html("0");
  $('#sumAmount').html("");

  //hesap ekranı scroll hesaplanması
  setTimeout(calcCheckScreenScroll,0);
  
  if (posmobil.current_sale.discount_type == "amount") {
    $('#discountSpan').html("-"+posmobil.current_sale.discount_amount+"₺");
  } else if (posmobil.current_sale.discount_type == "percent") {
    $('#discountSpan').html("%"+posmobil.current_sale.discount_amount);
  }


  let subtotal = parseFloat($('#subtotal').html());
  $('#sumAmount').html(subtotal.toFixed(2)+"₺");

  _price();

  _posRest();

},

on_click_interim_payment: function() {

  $('#interimPaymentScreen').click();

  $('#backHomePageInterimPayment').click(function(event) {
    $('#home').click();
    _home_reload();
  });

  //ara ödeme tablo boyutlarının hesaplanması

  let winHeight = $( window ).height(),
      offset = $('.interim_payments-info').offset().top,
      footer = $('.button-interim-group').height();

  let tableSize = (winHeight - (offset + footer + 95))/2;

  $('.table-scroll').css('max-height', tableSize);


  //ara ödeme geçmiş tablo boyutunun hesaplanması

  let historyTableSize = winHeight - (offset + 45);

  $('.table-scroll-history').css('max-height', historyTableSize);


  $('#interimPayment > tbody').html("");
  $('#interimPayable > tbody').html("");
  $('#interimPaid > tbody').html("");
  $('#interimPaymentTotalPrice').text("0.00₺");

  $("#interim-payment-tab ul li a").click(function() {
    $(this).attr("href");
  });

  $("#interim-payment-tab ul li:first-child a").click();


  $.post(base_url+'mobilpos/ajax/orderCheck', $(posmobil.posForm).serializeArray(), 

    function(json) {

      sale_products = json.data.sale_products;
      var totalPaid = 0;
      if (typeof sale_products !== "undefined") {
        $.each(sale_products, function(index, val) {

          //totalPaid = parseInt(val.paid_cash) + parseInt(val.paid_credit) + parseInt(val.paid_ticket) + parseInt(val.paid_sodexo) + parseInt(val.paid_voucher);
          totalPaid = interim_count_calc(val, json.data.payment_types);

          result = parseFloat(val.quantity) - parseFloat(totalPaid);

          if (result > 0) {

            var productItemStr ="";
            var addPrice = 0;
            $.each(val.additional_products, function(index, value) {
               addPrice += parseFloat(value.price) + parseFloat(value.price) * value.tax / 100;
               addPrice = round(addPrice,2);
            });


            sumPrice = parseFloat(val.price) + parseFloat(val.price*val.tax/100) + parseFloat(addPrice);
            sumPrice = round(sumPrice,2);


            if (val.is_gift == 0) {
              $('#interimPayment > tbody').append('\
                <tr data-id="'+val.id+'" class="pr-container">\
                  <td class="quantity">'+result+'</td>\
                  <td>'+val.title+'</td>\
                  <input type="hidden" name="interim_payment_products_unuse['+val.id+'][product_id]" value="'+val.product_id+'">\
                  <input type="hidden" name="interim_payment_products_unuse['+val.id+'][quantity]" value="'+result+'">\
                  <input type="hidden" name="interim_payment_products_unuse['+val.id+'][sale_product_id]" value="'+val.id+'">\
                  '+productItemStr+'\
                  <td class="total-sum-price">'+sumPrice.toFixed(2)+'₺</td>\
                </tr>\
              ');
            }
          }

          if (totalPaid > 0) {

            var productItemStr ="";
            var addPrice = 0;
            $.each(val.additional_products, function(index, value) {
               addPrice += parseFloat(value.price) + parseFloat(value.price) * value.tax / 100;
               addPrice = round(addPrice,2);
            });

            sumPrice = parseFloat(val.price) + parseFloat(val.price*val.tax/100) + parseFloat(addPrice);
            sumPrice = round(sumPrice,2);

            if (val.is_gift == 0) {
              $('#interimPaid > tbody').append('\
              <tr data-id="'+val.id+'" class="pr-container">\
                <td class="quantity">'+totalPaid+'</td>\
                <td>'+val.title+'</td>\
                <input type="hidden" name="interim_payment_products_unuse['+val.id+'][product_id]" value="'+val.product_id+'">\
                <input type="hidden" name="interim_payment_products_unuse['+val.id+'][quantity]" value="'+result+'">\
                <input type="hidden" name="interim_payment_products_unuse['+val.id+'][sale_product_id]" value="'+val.id+'">\
                '+productItemStr+'\
                <td class="total-sum-price">'+sumPrice.toFixed(2)+'₺</td>\
              </tr>\
              ');
            }
          }
        });
      }

 },"json");
},

interim_payment_unpaid_products: function() {

  //Ödemesi yapılmayan siparişlerin ödemesi yapılacaklara aktarılması

  $("body").on("click", "#interimPayment tbody tr", function(){

  var payable_basket = $("#interimPayable tbody");
  unpaid_row = $(this);
  payable_clone = unpaid_row.clone();
  data_id = unpaid_row.data("id");
  quantity = unpaid_row.find("input[name*='quantity']").val() | 0;
  if (quantity < 1) {
    return false;
  }

  unpaid_row.find("td.quantity").html(quantity - 1);
  unpaid_row.find("input[name*='quantity']").val(quantity - 1);


  var total = parseFloat(unpaid_row.find('td.total-sum-price').text().slice(0,-1));

  sumTotal = $('#interimPaymentTotalPrice').text().slice(0,-1);

  checkTotal = parseFloat(sumTotal) + parseFloat(total);

  $('#interimPaymentTotalPrice').text(parseFloat(checkTotal).toFixed(2) + '₺');


  var payable_row = $("tr[data-id=" + data_id + "]", payable_basket);
  if (!payable_row.length) {

    payable_clone.find("td.quantity").html(1);
    payable_clone.find("input[name*='quantity']").val(1);
    var inputs = $("input[name^='interim_payment_products_unuse']", payable_clone);

    $.each(inputs, function(index, val) {
      str = val.name;
      new_input_name = str.replace(/interim_payment_products_unuse/,"interim_payment_products");
    $(this).attr("name",new_input_name);
    });

    payable_basket.append(payable_clone);

  } else {

    val = payable_row.find("input[name*='quantity']").val() | 0;
    val = parseInt(val);

    payable_row.find("td.quantity").html(val + 1);
    payable_row.find("input[name*='quantity']").val(val + 1);

  }

  $("tr.interim_payments-info", payable_basket).removeClass("hidden");
  if ($("tr:not(tr.interim_payments-info)", payable_basket).length) {
    $("tr.interim_payments-info", payable_basket).addClass("hidden");
  }

  });

    $('#interimPayment > tbody').html("");
},


interim_payment_payable_products: function() {

  var payable_basket = "#interimPayable tbody";
  
  $("body").on("click", payable_basket + " tr:not(tr.interim_payments-info)", function(){
    var unpaid_basket = $("#interimPayment tbody");
    payable_row = $(this);
    data_id = payable_row.data("id");
    quantity = payable_row.find("input[name*='quantity']").val() | 0;
    quantity = parseInt(quantity);
    unpaid_row = $("tr[data-id=" + data_id + "]", unpaid_basket);


    unpaid_val = unpaid_row.find("input[name*='quantity']").val() | 0;
    unpaid_val = parseInt(unpaid_val);

    var total = parseFloat(unpaid_row.find('td.total-sum-price').text().slice(0,-1));

    sumTotal = $('#interimPaymentTotalPrice').text().slice(0,-1);
    checkTotal = parseFloat(sumTotal) - parseFloat(total);



    $('#interimPaymentTotalPrice').text(parseFloat(checkTotal).toFixed(2) + '₺');
    

    unpaid_row.find("td.quantity").html(unpaid_val + 1);
    unpaid_row.find("input[name*='quantity']").val(unpaid_val + 1);

    if (quantity <= 1) {

      payable_row.remove();

    } else {

      payable_row.find("td.quantity").html(quantity - 1);
      payable_row.find("input[name*='quantity']").val(quantity - 1);
    }

    $(payable_basket + " tr.interim_payments-info").removeClass("hidden");
    if ($(payable_basket + " tr:not(tr.interim_payments-info)").length) {
      $(payable_basket + " tr.interim_payments-info").addClass("hidden");
    }

  });

  $('#interimPayable tbody').html("");
},

onClick_interimPaymentSubmit: function () {
    
    $("body").on("click", ".interim_payments-submit button", function(){


      if (!posmobil.table_exists()) return;
      form_data = $(posmobil.posForm).serializeArray(),
      payment_type = $(this).data("payment");
      form_data.push({"name": "payment_type", "value": payment_type});

      if (payment_type === "gift") {
        $.post(base_url + "mobilpos/ajax/createGift", form_data, function(json) {
          if (!json.result) {
            Pleasure.handleToastrSettings('Hata!', json.message, 'error');
            return;
          }

          Pleasure.handleToastrSettings('Başarılı', json.message, 'success');
          $('.modal-img-close').click();

          $('#tblProduct >tbody').html("");

          $.post(base_url+'mobilpos/ajax/orderCheck', $(posmobil.posForm).serializeArray(), 

          function(json) {

          get_products(json.data.sale_products, json.data.interim_payment_amount, json.data.payment_types);
          
          _calc();

          }, "json");

          $('#backOrderTab').click();

        }, "json");

        return;
      }

      $.post(base_url + 'mobilpos/ajax/completeInterimPayment', form_data ,

       function(json) {
        
        if (!json.result) {
          Pleasure.handleToastrSettings('Hata!', json.message, 'error');
          return;
        }

        Pleasure.handleToastrSettings('Başarılı', json.message, 'success');
          $('.modal-img-close').click();

        $('#tblProduct >tbody').html("");


         // $("#modal-product-payment").modal("toggle");

          $.post(base_url+'mobilpos/ajax/orderCheck', $(posmobil.posForm).serializeArray(), 

          function(json) {

          get_products(json.data.sale_products, json.data.interim_payment_amount, json.data.payment_types);
          
          _calc();

          }, "json");

          $('#backOrderTab').click();

          $('#interimPaymentTotalPrice').text('0.00₺');


      }, "json");
    });

},

table_exists: function() {

  if ($('#pos input[name=table_id]').length !== 0) {
    return true;
  }

  Pleasure.handleToastrSettings('Hata!', 'Masa Seçiniz.', 'error');
},

on_click_icon_plus: function() {
  let 
  quantity = $('#productQuantity').val();
  quantity = parseInt(quantity);

  $('#productQuantity').val(quantity+1);

},

on_click_icon_minus: function() {

  let 
  quantity = $('#productQuantity').val();
  quantity = parseInt(quantity);


  if ( quantity > 1) {
    $('#productQuantity').val(quantity-1);
  }

}

};


posmobil.ProductNotes = {
  modal: "#modal-product_note",
  timer: null,

  init: function () {
    posmobil.SoundRecorder.init();

    posmobil.ProductNotes.Events.init();
  },

  loadModal: function () {
    posmobil.ProductNotes.emptyModal();
    $(posmobil.ProductNotes.modal).modal("toggle");
  },

  emptyModal: function () {

    $(".timer", posmobil.ProductNotes.modal).html("00:00:00");
    $("textarea[name='note']", posmobil.ProductNotes.modal).val("");
  },

  startTimer: function () {
    let second = 1;
    posmobil.ProductNotes.timer = setInterval(function () {
      $(".timer", posmobil.ProductNotes.modal).html(
        posmobil.ProductNotes.Helpers.secondsToHms(second++)
      );
    }, 1000);
  },

  stopTimer: function () {
    clearInterval(posmobil.ProductNotes.timer);
  },

  Events: {
    init: function () {
      // Sound Note
      $("body").on("click", "#create-soundnote", posmobil.ProductNotes.Events.onClickCreateSoundNote);
      $("body").on("click", "#notesound .save", posmobil.ProductNotes.Events.onClickSaveSoundNote);
      $("body").on("click", "#notesound .cancel", posmobil.ProductNotes.Events.onClickCancelSoundNote);

      // Text Note
      $("body").on("click", "#create-textnote", posmobil.ProductNotes.Events.onClickCreateTextNote);
      $("body").on("click", "#notetext .save", posmobil.ProductNotes.Events.onClickSaveTextNote);
      $("body").on("click", "#notetext .cancel", posmobil.ProductNotes.Events.onClickCancelTextNote);
      $("body").on("click", "#quick_notes .addtoquickNotes", posmobil.ProductNotes.Events.onClickAddQuickNotes);
      $("body").on("click", "#productQuantityModal #product_notes .plusQuickNote, #productQuantityModal #product_notes .minusQuickNote", posmobil.ProductNotes.Events.onClickMinusPlusQuickNote);

      $("body").on("click", "#productQuantityModal #product_notes .remove", posmobil.ProductNotes.Events.onClickRemoveNote);
    },

    onClickCreateSoundNote: function (event) {
      event.preventDefault();
      posmobil.ProductNotes.loadModal();
      $("a[href='#notesound']", posmobil.ProductNotes.modal).click();
      posmobil.ProductNotes.startTimer();

      posmobil.SoundRecorder.start();
    },

    onClickCreateTextNote: function (event) {
      event.preventDefault();
      posmobil.ProductNotes.loadModal();
      $("a[href='#notetext']", posmobil.ProductNotes.modal).click();
    },

    onClickRemoveNote: function (event) {
      event.preventDefault();
      let
      button = $(this),
      container = button.closest(".list-group-item");

      container.fadeOut(300, function() {
        container.remove();
      });
    },

    onClickSaveSoundNote: function (event) {
      event.preventDefault();

      // $(posmobil.ProductNotes.modal).modal("toggle");

      // posmobil.SoundRecorder.stop(function (blob) {
      //   posmobil.ProductNotes.Helpers.handleSoundNote(blob, function (note) {
      //     posmobil.ProductNotes.Templates.newNote(note);
      //   });
      // }, "audio/mp3");

      posmobil.ProductNotes.stopTimer();

      posmobil.SoundRecorder.stop(function (blob) {
        posmobil.SoundRecorder.save(blob, base_url+"mobilpos/ajax/saveSoundNote", function (json) {
          
          $(posmobil.ProductNotes.modal).modal("toggle");

          if (!json.result) {
            Pleasure.handleToastrSettings("Hata!", json.message, "error");
            return;
          }

          json.data.sound_note.sound = base_url + json.data.sound_note.sound;

          posmobil.ProductNotes.Templates.newNote(posmobil.ProductNotes.Templates.newSoundNote(json.data.sound_note));

        });

      }, "audio/mp3");
    },

    onClickCancelSoundNote: function (event) {
      event.preventDefault();
      posmobil.ProductNotes.stopTimer();
      $(posmobil.ProductNotes.modal).modal("toggle");

      posmobil.SoundRecorder.stop();
    },

    onClickSaveTextNote: function (event) {
      event.preventDefault();
      $(posmobil.ProductNotes.modal).modal("toggle");

      let
      note = $("textarea[name='note']", posmobil.ProductNotes.modal).val();
      note = $.trim(note);

      if (note.length > 0) {
        note = posmobil.ProductNotes.Templates.newTextNote(note);
        posmobil.ProductNotes.Templates.newNote(note);
      }
    },

    onClickCancelTextNote: function (event) {
      event.preventDefault();
      $(posmobil.ProductNotes.modal).modal("toggle");
    },

    onClickAddQuickNotes: function (event) {
      event.preventDefault();

      let button = $(this);
      let note = button.val();
      note = $.trim(note);

      if (note.length > 0) {
        note = posmobil.ProductNotes.Templates.newDynamicTextNote(note);
        posmobil.ProductNotes.Templates.newNote(note);
      }
    },

    onClickMinusPlusQuickNote: function (event) {
      event.preventDefault();
      
      let button = $(this),
          container = button.closest('.quickNote'),
          quickNoteText = $('.quickNoteText', container),
          quickNoteInput = $("input[name='[text_notes][]']", container),
          quantity = container.data('quantity'),
          note = container.data('note'),
          isPlus = button.hasClass('plusQuickNote');

          if (isPlus) {
            quantity++;
          } else {
            quantity--;
          }

          if (quantity <= 0) {
            return;
          }

          quickNoteText.html(quantity + " " + note);
          quickNoteInput.val(quantity + " " + note);
          container.data('quantity', quantity);
    },
  },

  Helpers: {
    handleSoundNote: function (blob, callback) {
      let 
      url = URL.createObjectURL(blob),
      reader = new window.FileReader();
      reader.readAsDataURL(blob); 

      reader.onloadend = function() {
        let base64data = reader.result;

        callback(posmobil.ProductNotes.Templates.newSoundNote(url, base64data));
      };
    },

    secondsToHms: function (second) {
      second = Number(second);
      let 
      h = Math.floor(second / 3600)
      m = Math.floor(second % 3600 / 60),
      s = Math.floor(second % 3600 % 60),

      hDisplay = (h < 10 ? "0" : "") + h,
      mDisplay = (m < 10 ? "0" : "") + m,
      sDisplay = (s < 10 ? "0" : "") + s;

      return `${hDisplay}:${mDisplay}:${sDisplay}`; 
    }
  },

  Templates: {
    newSoundNote: function (sound) {
      return `
        <audio controls style="width: 100%;">
          <source src="${sound.sound}" type="audio/mp3">
        </audio>
        <input type="hidden" name="[sound_notes][]" value="${sound.id}" data-url="${sound.sound}">
      `;
    },

    newTextNote: function (note) {
      return `
        <i class="small">${note}</i>
        <input type="hidden" name="[text_notes][]" value="${note}">
      `;
    },

    newDynamicTextNote: function (note) {
      return `
        <div class="row quickNote" data-quantity="1" data-note="${note}">
          <div class="col-xs-6" style="overflow-wrap: break-word; overflow-y: auto; max-height: 50px;">
            <i class="small quickNoteText">1 ${note}</i>
            <input type="hidden" name="[text_notes][]" value="1 ${note}">
          </div>
          <div class="col-xs-6">
            <span class="input-group-btn">
                <button type="button" class="btn btn-ripple btn-success col-xs-6 plusQuickNote">+</button>
                <button type="button" class="btn btn-ripple btn-danger col-xs-6 minusQuickNote">-</button>
            </span>
          </div>
        </div>
      `;
    },

    newNote: function (content) {
      $("#productQuantityModal #product_notes").append(`
        <div class="list-group-item row" style="padding-left: 0;">
          <div class="col-xs-10 product-text-note">
            ${content}
          </div>
          <div class="col-xs-2 text-right" style="padding-left: 0;">
            <a href="#" class="btn btn-danger remove">
              <i class="fa fa-trash"></i>
            </a>
          </div>
        </div>
      `);
    },
  }

};

posmobil.SoundRecorder = {
  audio_context: null,
  recorder: null,
  audio_stream: null,

  init: function () {
    let $this = this;
    try {
      // Monkeypatch for AudioContext, getUserMedia and URL
      window.AudioContext = window.AudioContext || window.webkitAudioContext;
      navigator.getUserMedia = navigator.getUserMedia || navigator.webkitGetUserMedia;
      window.URL = window.URL || window.webkitURL;

      // Store the instance of AudioContext globally
      $this.audio_context = new AudioContext;
      console.log('Audio context is ready !');
      console.log('navigator.getUserMedia ' + (navigator.getUserMedia ? 'available.' : 'not present!'));
    } catch (e) {
      Pleasure.handleToastrSettings("Hata!", "Ses kayıt özelliği internet tarayıcınızda desteklenmiyor!", "warning");
    }
  },

  start: function () {
    let $this = this;
    // Access the Microphone using the navigator.getUserMedia method to obtain a stream
    navigator.getUserMedia({ audio: true }, function (stream) {
      // Expose the stream to be accessible globally
      $this.audio_stream = stream;
      // Create the MediaStreamSource for the Recorder library
      var input = $this.audio_context.createMediaStreamSource(stream);
      console.log('Media stream succesfully created');

      // Initialize the Recorder Library
      $this.recorder = new Recorder(input);
      console.log('Recorder initialised');

      // Start recording !
      $this.recorder && $this.recorder.record();
      console.log('Recording...');
    }, function (e) {
      console.error('No live audio input: ' + e);
    });
  },

  stop: function (callback, AudioFormat) {
    let $this = this;
    // Stop the recorder instance
    $this.recorder && $this.recorder.stop();
    console.log('Stopped recording.');

    // Stop the getUserMedia Audio Stream !
    $this.audio_stream.getAudioTracks()[0].stop();

    // Use the Recorder Library to export the recorder Audio as a .wav file
    // The callback providen in the stop recording method receives the blob
    if(typeof(callback) == "function"){

      /**
       * Export the AudioBLOB using the exportWAV method.
       * Note that this method exports too with mp3 if
       * you provide the second argument of the function
       */
      $this.recorder && $this.recorder.exportWAV(function (blob) {
        callback(blob);

        // create WAV download link using audio data blob
        // createDownloadLink();

        // Clear the Recorder to start again !
        $this.recorder.clear();
      }, (AudioFormat || "audio/wav"));
    }
  },

  save: function (blob, url, callback) {
    let
    formData = new FormData();
    formData.append('sound', blob);

    $.ajax({
      url: url,
      type: "POST",
      dataType: "json",
      data: formData,
      contentType: false,
      processData: false,
      success: function(json) {
        callback(json);
      }
    });
  }
};

posmobil.OperatorSession = {

  container: "#tab-operatorsession",

  init: function () {
    setInterval(posmobil.OperatorSession.checkActiveSession, 3000);

    posmobil.OperatorSession.Events.init();

    posmobil.OperatorSession.Helpers.displayOperatorName(posmobil.operator_session.user_name);

    if (posmobil.operator_session.permissions) {
      posmobil.OperatorSession.Helpers.displayButtonsByPermissions();
    }

  },

  loadTab: function () {
    $("a[href='#tab-operatorsession']").trigger("click");

    posmobil.OperatorSession.emptyTab();
  },

  emptyTab: function () {
    $('input[name="password-login"]', posmobil.OperatorSession.container).val("");
  },

  loadTabByStatus: function (json) {
    if (!json.result && !posmobil.Helpers.activeTabIs("tab-operatorsession")) {
      posmobil.OperatorSession.loadTab();
    } else if (json.result && posmobil.Helpers.activeTabIs("tab-operatorsession")) {
      $("a[href='#home-tab']").trigger("click");
    }
  },

  checkActiveSession: function () {
    posmobil.Helpers.postForm(null, "checkOperatorSession", function(json) {
      posmobil.OperatorSession.loadTabByStatus(json);
    }, "json");
  },

  login: function () {
    posmobil.Helpers.postForm(posmobil.posForm, "loginForOperatorSession", function (json) {

      if (!json.result) {
        Pleasure.handleToastrSettings("Hata!", json.message, "error");
        return;
      }

      posmobil.operator_session = {
        user_id: json.data.user.id,
        user_name: json.data.user.username,
        user_role: json.data.user.role_alias,
        permissions: json.data.user.permissions
      };

      posmobil.getFloorsWithZones();

      posmobil.OperatorSession.Helpers.displayOperatorName(json.data.user.username);
      if (posmobil.operator_session.permissions) {
        posmobil.OperatorSession.Helpers.displayButtonsByPermissions();
      }

      $("a[href='#home-tab']").trigger("click");
    });
  },

  Events: {
    init: function () {
      $("body").on("click", `${posmobil.OperatorSession.container} .nums .btn`, posmobil.OperatorSession.Events.onClickNumPadButtons);
      $("body").on("click", "#exit_operator_session", posmobil.OperatorSession.Events.onClickExit);
    },

    onClickNumPadButtons: function (event) {
      event.preventDefault();

      let 
      button = $(this),
      value = button.data("value");

      if ("1234567890".indexOf(value) > -1) {
        posmobil.OperatorSession.Helpers.addNumber(value);
      } else if (value === "delete") {
        posmobil.OperatorSession.Helpers.deleteNumber();
      } else if (value === "submit") {
        posmobil.OperatorSession.login();
      }
    },

    onClickExit: function (event) {
      event.preventDefault();

      posmobil.Helpers.postForm(posmobil.posForm, "logoutForOperatorSession", function (json) {
        posmobil.OperatorSession.loadTab();
      });
    }
  },

  Helpers: {
    addNumber: function (number) {
      let 
      input = $('input[name="password-login"]', posmobil.OperatorSession.container),
      value = input.val();

      input.val(value +""+ number);
    },

    deleteNumber: function () {
      let 
      input = $('input[name="password-login"]', posmobil.OperatorSession.container),
      value = input.val();
      value = value.slice(0, -1);

      input.val(value);
    },

    displayOperatorName: function (username) {
      $("#displayUserName").html(username);
      $(".adisyon-username").html(username);
    },

    displayButtonsByPermissions: function() {
      posmobil.Helpers.visibleDisable("#order-tab #checkBtn", posmobil.Helpers.hasPermission("pointofsales.check"));
      posmobil.Helpers.visibleDisable("#order-tab #interimPaymentBtn", posmobil.Helpers.hasPermission("pointofsales.interimpayment"));
      posmobil.Helpers.visibleDisable("#order-tab #cancel-sale", posmobil.Helpers.hasPermission("pointofsales.tablecancel"));
      posmobil.Helpers.visibleDisable("#left-sidebar #change_pos", posmobil.Helpers.hasPermission("pointofsales.changepos"));
      //posmobil.Helpers.visibleDisable("#order-tab #add-discount", posmobil.Helpers.hasPermission("pointofsales.adddiscount"));
      posmobil.Helpers.visibleInput("#modal-discount_login #username-password-group", posmobil.Helpers.hasPermission("pointofsales.adddiscount"));


    }


  }
};

posmobil.lockScreen = {

  container: "#tab-lock",

  init: function () {

    posmobil.lockScreen.Events.init();
    
    $("body").on("click", `${posmobil.lockScreen.container} .lock-exit`, posmobil.lockScreen.onClickLockExit);


  },

  onClickLockExit: function () {

    if (posmobil.cookieValue == "list") {
      $('#tabCategoryList').click();
    } else {
      $("#tableName").click();
    }

    posmobil.lockScreen.emptyTab();


  },

  emptyTab: function () {

    $('input[name="password-lock-login"]', posmobil.lockScreen.container).val("");

  },

  login: function () {

    posmobil.Helpers.postForm(posmobil.posForm, "loginForMobilSettings", function (json) {

      if (!json.result) {
        Pleasure.handleToastrSettings("Hata!", json.message, "error");
        return;
      }

      posmobil.lockScreen.emptyTab();

      $("a[href='#home-tab']").trigger("click");
    });
  },

  Events: {
    init: function () {
      $("body").on("click", `${posmobil.lockScreen.container} .nums .btn`, posmobil.lockScreen.Events.onClickNumPadButtons);
    },

    onClickNumPadButtons: function (event) {
      event.preventDefault();

      let 
      button = $(this),
      value = button.data("value");

      if ("1234567890".indexOf(value) > -1) {
        posmobil.lockScreen.Helpers.addNumber(value);
      } else if (value === "delete") {
        posmobil.lockScreen.Helpers.deleteNumber();
      } else if (value === "submit") {
        posmobil.lockScreen.login();
      }
    }

  },

  Helpers: {
    addNumber: function (number) {
      let 
      input = $('input[name="password-lock-login"]', posmobil.lockScreen.container),
      value = input.val();

      input.val(value +""+ number);
    },

    deleteNumber: function () {
      let 
      input = $('input[name="password-lock-login"]', posmobil.lockScreen.container),
      value = input.val();
      value = value.slice(0, -1);

      input.val(value);
    },
  }



};

posmobil.Helpers = {
  postForm: function (form, url, callback) {
    $.post(base_url + "mobilpos/ajax/" + url, $(form).serializeArray(), function(json) {
      if (!json.result && json.data !== null && typeof json.data.event !== "undefined") {
        if (json.data.event === "requiredSetup") {
          Pleasure.handleToastrSettings("Hata!", json.message, "error");
          location.reload();
        } else if (json.data.event === "requiredSession") {
          posmobil.OperatorSession.loadTabByStatus(json);
        }
        return;
      }
      callback(json);
    }, "json");
  },

  activeTabIs: function (tab) {
    let active_tab = posmobil.Helpers.getActiveTab() + "";
    tab = tab + ""
    return (active_tab === tab);
  },

  getActiveTab: function (tab) {
    return $("#main-tabs > .tab-pane.active").attr("id");
  },

  round: function (value, decimals) {
    return Number(Math.round(value+'e'+decimals)+'e-'+decimals);
  },

  isEmpty: function (el){
    return !$.trim(el.html())
  },

  secondsToHms: function (second) {
    second = Number(second);
    let 
    h = Math.floor(second / 3600)
    m = Math.floor(second % 3600 / 60),
    s = Math.floor(second % 3600 % 60),

    hDisplay = (h < 10 ? "0" : "") + h,
    mDisplay = (m < 10 ? "0" : "") + m,
    sDisplay = (s < 10 ? "0" : "") + s;

    return `${hDisplay}:${mDisplay}:${sDisplay}`; 
  },

  hasPermission: function (permission) {

    if (posmobil.operator_session.user_role !== 'admin') {
      if ( posmobil.operator_session.permissions.indexOf(permission) === -1 ) {
          return false;
      }
    }

    return true;
  },

  visible: function (selector, visibility ) {
    if (visibility === true) {
      $(selector).removeClass('hidden');
    } else {
      $(selector).addClass('hidden');
    }

  },

  visibleDisable: function (selector, visibility ) {
    if (visibility === true) {
      $(selector).removeAttr("disabled");
    } else {
      $(selector).attr("disabled",true);
    }

  },

  visibleInput: function (selector, visibility ) {
    if (visibility === true) {
      $(selector).addClass('hidden');
    } else {
      $(selector).removeClass('hidden');
    }

  },

};



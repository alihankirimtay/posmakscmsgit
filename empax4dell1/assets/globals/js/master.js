$(function() {
	
	/* == Master Form == */
    $('body').on('click', 'button[name=dosubmit]', function() {
        var form = $(this).closest("form");
        var url = $(this).data('url');
        var returnUrl = $(this).data('return');
        var onSuccess = $(this).data('success'); // on success call function. Example: functionName(1, 2) or Class.functionName(3)

        function showResponse(json) {

            _loading("delete");
            // console.log(json);
            if(json.result === 1){
                if (onSuccess !== undefined) {
                    eval(onSuccess);
                }
                if (json.data) {
                    if (typeof(json.data.onSuccess) !== "undefined") {
                        eval(json.data.onSuccess);
                    }
                }
            	Pleasure.handleToastrSettings('Başarılı', json.message, 'success');
            }
            else
            	Pleasure.handleToastrSettings('Hata!', json.message, 'error');
            if (typeof returnUrl !== 'undefined' && json.result == 1) {
                window.location.href = returnUrl;
            }

        }
        function showLoader() {
            _loading("add");
        }

        var options = {
            beforeSubmit: showLoader,
            success: showResponse,
            type: "post",
            url: url,
            dataType: 'json'
        };

        $(form).ajaxForm(options);

    });

    /* == Master Delete Table Row == */
    $('body').on('click', 'button[name=dodelete]', function() {
        var tr = $(this).closest("tr");
        var table = $(this).closest("table");
        var url = $(this).data('url');

        if(!confirm('Are you sure to delete this?')) return false;

        $.ajax({
            url: url,
            type: 'post',
            beforeSend: function() {
                _loading("add");
            },
            success: function(result) {
                var json = $.parseJSON(result);

                _loading("delete");
                if (json.result == 1) {
                    tr.hide('fast');
                } else {
                    $(table).before(' \
                        <div class="alert alert-danger alert-dismissable"> \
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button> \
                        ' + json.message + ' \
                        </div>');
                }
            }
        });
    });


    function _loading (action) {
        if(action == "add") $("body").append("<div class='loading-spinner'> <div class='loading-spinner-bussy'></div> </div>");
        else if(action == "delete") $(".loading-spinner").remove();
    }
});

//              //
//  UPLOADER    //
//              //
var Eupload = {

    settings: { 
        Match: /^data:(.)+;base64/,
        Icon: "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAEAAAABACAYAAACqaXHeAAAE+0lEQVR4Xu2aT5IMURDGvwkHGDdgVpbYi8AJmAjWOAFzApwAJ8CaCJzAiLDH0gonwAEE8et42V7XVPX7U/mqp6MrIyqme6bqvcwvv8yXmTV72nHZ23H7NQMwM2DHEZhDYMcJMCfBKUPgrKQbkq5JOh+Yx8/v4fPn8Pld9LvmBJ0CgDuS7gbDY4M+RF+udiwFjBeSnrVGoCUANyU9Cd7+LeltuI4l/RowjGe4YMm5wIRHkl62AqIFAFD9oaQHkjD8abiGjB6yDdbw7L4kQDtcA1w1Pt4AYPx7SZckEcsYUWp4bAzrwYD7kgiLe+FntcHdBz0BiI0/Ct7zUhQgnwcwr3uC4AnAp+B5vEQC8xZYRSj8lHR5JLOWunkBQKxCU2/Pd0E0JgAETBgtHgCQsYl7jjU+txYD24VpHgBgPJSEolbUtAaBff56hMJYAMz7j0O2bm24rW+hMHrfsQBQ3FDeHkzofQPBWMDe1TIWAGjIeU/1NrVYLiD8qBGqZAwAGP1mgsw/ZBg5h6N31MmTC4B1cmzKhdDJcY3yQJXb/j9ElYn3q0+fFAAYS13fpbh1cgZCrR3QmDDCizVCPYC4A4DH6eTItgjdGAlvXSdXagA1vgHL2nwvFfShlU45cnDdvgfxOnW3NTR0dd7nu5W15jkM4XNpMosTIV3jBUlfg7VfcsrlLgAoRmEDA1wqrR7oWZvkxbADAxBAppQurfFhDSFqgtEXo+8AC7sGBysxANbNca7WeCOXvhgN0N24RVkYABi5wjPobeH5R9KZsD57EGIwAwaTZ7hvRWIA8DxKtfK8JSuU6CubSagAgNKW3FJAYHxq3hAPVgB/JeEaAFbSkuws8aU2L/27UZ/ydahdxnj+Bhgpw0r2N5aQMFkfJy/EADDvtyxpjX6pqpH7OBoZgXkLxjOkXRZPAADa30KiKIm/EuVKPIu3iFmYeCJmSzbtuZe1CS8S5cLZAIDRnPkMGHJjr0QPNgVgaJdrECE5lCtK9u671xzO+ocAgNEkJRRtITXZHT1IWHjKZfLTMcxC4cAAsAztDQDs4gLg0qSGQzgVbKzuqZsl/SMAIOG0GGdBNQoeklltaMUVY2mVmAJs0Ui1BACFa2v8WHmqPYYuVImeglP2AYCMy1XdUfVoZY2Otc5jFfcCM9Zj0UhZDqBc9ELYsqznnMCGH55rLgGwjsqrCCLpUWz0HXk/Ml6acP7zYrQr1BKM32ra5j4GLvoIGOA92rL3eV36W92eCjVrcLqnBmGA8aWnyVD4sc6xlcJ84ZXTqAlrItAxHANyAOC+2pMjJ98snW4AWF/duhM8LQAsx/kGgNXf1ASwwItmsTdOCwNWOt94HmC0INa8ToTTBkA89FnMJLojMQuFFv+RsWkGYDzvMVaGPn1DUQMBJtA3eyWjTQJAbYLxeJ354LLtHxonx2Mk+2+tsbX4JgDA6wxbMbh30Ltung5qsIGiBqFcJnuSIK18zjly7B7Qvy3pSuKhj5JeVYzI430w1oai/J5mr/dozXmhwEIwgiTZV6GVgMDYOtUfwLR4tF2yfnxv/K95g4OYHAC6CtQOT3juViYDXo9ggLE162VODQC1HtlEDkjqOgOQhMjvhpkB4aVr6sWLvbjwqj/WunDKEEARZg8AwACmT8jcANDq/cSJPacGwC+gnFaaAXACcmuXmRmwta5zUnxmgBOQW7vMzjPgH0OIK30s4UdaAAAAAElFTkSuQmCC",

        LBL_add: "Add",
        LBL_remove: "Remove",

        string: ' \
            <!--  --> \
            <div class="col-md-6 thumbnail eupload-container" data-index="{{INDEX}}"> \
                <div class="col-md-3"> \
                        {{ITEM}} \
                </div> \
                <div class="col-md-9"> \
                    <div class="col-md-5"> \
                        <span class="input-group-btn"> \
                            <button type="button" class="btn btn-default btn-xs eupload-button-add">{{ADD}}</button> \
                            <button type="button" class="btn btn-default btn-xs eupload-button-remove">{{REMOVE}}</button> \
                        </span> \
                    </div> \
                    <div class="col-md-7 eupload-progress hide"> \
                        <div class="progress"> \
                            <div class="progress-bar progress-bar-info progress-bar-striped active" style="width: 0%"> \
                                <span class="glyphicon glyphicon-ban-circle eupload-button-abort" aria-hidden="true"></span> \
                                <span class="progress-bar-text">0%</span> \
                            </div> \
                        </div> \
                    </div> \
                    <div class="col-md-12 eupload-message hide"> \
                        Name.jpg \
                    </div> \
                </div> \
            </div> \
            <!--  --> \
        ',

        form_action: base_url + "panel/Ajax/doUpload",
        form_id: "main-upload-form",
        _dir: "assets/uploads/TMP/",
        name: "FILE-",
    },

    init: function (Settings) {

        var $self = this;

        if(typeof Settings !== 'undefined') {

            $self.settings = $.extend($self.settings, Settings);
        }

        $.each($("body .eupload"), function(index, val) {

            var
            element      = $(this),
            element_val  = element.val(),
            element_type = element.data("type"),
            element_src  = (element_val ? element_val : $self.settings.Icon),
            parent       = element.parent(),
            item;

            // IMAGE
            if(element_type == 'image') {

                item = '<img src="'+element_src+'" width="100px" class="eupload-preview">';

                html = $self.settings.string.replace("{{ITEM}}", item + parent.html());

            //  VIDEO
            } else if (element_type == 'video') {

                item = '<video width="100" height="100" controls class="eupload-preview"> \
                            <source src="'+element_src+'" type="video/mp4"> \
                        </video>';

                html = $self.settings.string.replace("{{ITEM}}", item + parent.html());;
            // AUDIO
            } else if (element_type == 'audio') {


                item = '<audio controls class="eupload-preview"> \
                            <source src="'+element_src+'" type="audio/mpeg"> \
                        </audio>';

                html = $self.settings.string.replace("{{ITEM}}", item + parent.html());
            // FILE
            } else if (element_type == 'file') {


                item = '<a href="'+element_src+'" target="_blank" class="eupload-preview"><span class="glyphicon glyphicon-file" aria-hidden="true"></span> </a>';

                html = $self.settings.string.replace("{{ITEM}}", item + parent.html());
            }

            html = html.replace("{{INDEX}}", "euploadId"+index);
            html = html.replace("{{ADD}}", $self.settings.LBL_add);
            html = html.replace("{{REMOVE}}", $self.settings.LBL_remove);

            parent.html(html);
        });


        $self._form_handler();
        $self._upload_handler();
        $self._click_handler();
        $self._clear_handler();
    },

    _form_handler: function() {

        var $self = this,

        form = $("<form/>", { action: $self.settings.form_action, class:'hidden', id:$self.settings.form_id, "enctype":"multipart/form-data" } );

        form.append( $("<input/>", { type:'file', name:'_file' } ) );
        form.append( $("<input/>", { type:'hidden', name:'_type' } ) );
        form.append( $("<input/>", { type:'hidden', name:'_dir', value:$self.settings._dir } ) );
        form.append( $("<input/>", { type:'hidden', name:'_name', value:$self.settings.name } ) );

        $("body").append(form);
    },

    _click_handler: function() {

        var $self = this;

        $("body").on("click", ".eupload-button-add", function(){

            button  = $(this),
            parent  = button.closest(".eupload-container"),
            element = parent.find(".eupload");


            $("#" + $self.settings.form_id + " > input[name='_type']").val( element.data("type") );

            $("#" + $self.settings.form_id + " > input[name='_file']").data("index", parent.data("index")).trigger("click");
            
        });
    },

    _clear_handler: function() {

        var $self = this;

        $("body").on("click", ".eupload-button-remove", function(){
                
            parent  = $(this).closest(".eupload-container"),
            preview = parent.find(".eupload-preview");
            input   = parent.find(".eupload"),
            type    = input.data("type");

            if(type == 'image') {

                preview.attr('src', $self.settings.Icon);
                input.val("");

            } else if (type == 'video') {

                preview
                .attr("poster", $self.settings.Icon)
                .find("source").attr('src', '');

                input.val("");

            } else if (type == 'audio') {

                preview.find("source").attr('src', '');
                input.val("");
            }

            input.val("");
        });
    },

    _upload_handler: function() {

        var $self = this,
        form      = $("#" + $self.settings.form_id);

        form.fileupload({

            limitMultiFileUploads: 1,
            sequentialUploads: true,

            add: function(e, data) {

                var index = form.find("input[name='_file']").data("index"),
                parent    = $("body").find("[data-index='"+index+"']"),
                button    = parent.find(".eupload-button-add"),
                buttonTxt = button.html();

                data.context = parent;

                if($(parent).find(".eupload-progress").hasClass("hide")) {

                    $(parent).find(".eupload-progress").removeClass("hide");
                    $(parent).find(".eupload-message").removeClass("hide");
                }

                if($(parent).find(".eupload-button-abort").hasClass("hide")) {

                    $(parent).find(".eupload-button-abort").removeClass("hide");
                }

                $(parent).find(".eupload-message").html("<i>"+data.files[0].name+", "+$self.formatFileSize(data.files[0].size)+"</i>");
                $(parent).find(".eupload-button-add").prop('disabled', true);

                $(parent).find(".eupload-button-abort").on("click", function() {

                    jqXHR.abort();
                    button.html( buttonTxt );
                    $(parent).find(".eupload-progress").addClass("hide");
                    $(parent).find(".eupload-message").html("<i>Cancelled</i>");
                });

                var jqXHR = data.submit().success(function (result, textStatus, jqXHR) {
                    
                    var json   = JSON.parse(result);
                    var status = json['status'];

                    if (status == 'error') {

                        data.context.find(".eupload-message").html("<i>"+json['message']+"</i><br>" + data.context.find(".eupload-message").html());

                    } else {
                        
                        data.context.find(".eupload-message").html("<i>"+json['message']+"</i><br>" + data.context.find(".eupload-message").html());

                        if(json.type == "image") {

                            data.context.find(".eupload-preview").attr("src", base_url + json.file_path + json.data.file_name);
                            data.context.find(".eupload").val(json.data.file_name);

                        } else if (json.type == "video") {

                            data.context.find(".eupload-preview")
                            .attr("poster", $self.settings.Icon)
                            .find("source").attr('src', base_url + json.file_path + json.data.file_name);

                        } else if (json.type == "audio") {

                            data.context.find(".eupload-preview")
                            .find("source").attr('src', base_url + json.file_path + json.data.file_name);

                        } else if (json.type == "file") {

                            alert("And what?");
                        }
                    };

                    data.context.find(".eupload-button-add").prop('disabled', false);
                });

            },

            progress: function (e, data) {

                var percentComplete = data.loaded / data.total;

                percentComplete = parseInt(percentComplete * 100);

                data.context.find(".progress-bar-text")
                .html( percentComplete + "%" );

                data.context.find(".progress-bar")
                .css("width", percentComplete + "%");

                if(percentComplete == 100) {

                    data.context.find(".eupload-button-abort").addClass("hide");

                }
            },

            fail: function (e, data) {

                data.context.find(".eupload-message").html("<i>"+e+"</i>");
                data.context.find(".eupload-button-add").prop('disabled', false);
            },
        });
    },

    formatFileSize: function (bytes) {
        if (typeof bytes !== 'number') { return ''; }

        if (bytes >= 1000000000) { return (bytes / 1000000000).toFixed(2) + ' GB'; }

        if (bytes >= 1000000) { return (bytes / 1000000).toFixed(2) + ' MB'; }

        return (bytes / 1000).toFixed(2) + ' KB';
    }
};

/*****
       
 */
/*****
       
 */
/*****
       
 */
/*****
       
 */



var eUploadAttachments = {

    settings:{
        max_file_size : 25000000, // 25Mb

        form_action  : base_url + "Ajax/UploadAttachment",
        form_id  : "attachment-upload-form",

        JQueryFileUpload: base_url + "assets/globals/plugins/jquery-file-upload/js/jquery.fileupload.js",
        ZONE: ".eUploadAttachments",

        HTML: ' <div class="col-xs-12 col-md-12 attachments-container"> \
                    <div class="col-xs-2 col-sm-2 col-md-1 attachments-abort"><i class="fa fa-times-circle"></i></div> \
                    <div class="col-xs-2 col-sm-2 col-md-1 attachments-percent">%0</div> \
                    <div class="col-xs-8 col-sm-8 col-md-10 attachments-url"></div> \
                    <input type="hidden" name="_files[]"> \
                    <input type="hidden" name="_file_name[]"> \
                </div>'
    },

    init: function(Settings) {

        var $self = this;

        if(typeof Settings !== 'undefined') { $self.settings = $.extend($self.settings, Settings); }

        this._form_handler();
        $.getScript( $self.settings.JQueryFileUpload , function() { $self._upload_handler(); });
        this._click_handler();
        this._css();
    },

    _css: function() {
        $(".attachments-dropzone").css({
            border: '1px dashed rgb(0, 0, 0)',
            cursor: 'pointer',
        });;
        $(".attachments-remove > *, .attachments-abort > *").css({
            cursor: 'pointer',
        });;
    },

    _form_handler: function() {

        var $self = this,

        form = $("<form/>", { action: $self.settings.form_action, class:'hidden', id:$self.settings.form_id, "enctype":"multipart/form-data" } );

        form.append( $("<input/>", { type:'file', name:'_file' } ) );

        $("body").append(form);
    },

    _click_handler: function() {

        var $self = this;

        $("body").on("click", $self.settings.ZONE + " > .attachments-dropzone", function() {

            $("#" + $self.settings.form_id + " > input[name='_file']").trigger("click");          
        });
    },

    _upload_handler: function() {

        var $self = this,
        form      = $("#" + $self.settings.form_id);

        form.fileupload({
            dropZone: $($self.settings.ZONE + " > .attachments-dropzone"),
            limitMultiFileUploads: 1,
            sequentialUploads: true,

            add: function(e, data) {

                var ZONE = $($self.settings.ZONE),
                HTML     = $($self.settings.HTML);               

                HTML.find(".attachments-url").html("<i>"+data.files[0].name+", <small>"+formatFileSize(data.files[0].size)+"</small></i>");
                HTML.find("input[name='_file_name[]']").val(data.files[0].name);

                data.context = HTML.appendTo(ZONE);

                HTML.find(".attachments-abort").on("click", function() {
                    jqXHR.abort();
                    $(this).closest(".attachments-container").fadeOut('slow', function() { $(this).remove(); });
                    // console.log('removed');
                });

                var jqXHR = data.submit().success(function (result, textStatus, jqXHR) {

                    var json   = JSON.parse(result);

                    if (! json['result']) {

                        data.context.find(".attachments-url").html( data.context.find(".attachments-url").html() +" <b>"+ json.message +"</b>" );
                        data.context.find("input[name='_files[]']").remove();

                    } else {

                        HTML.find("input[name='_files[]']").val(json.data.file_name);

                        data.context.find(".attachments-abort")
                                    .removeClass("attachments-abort")
                                    .addClass("attachments-remove")
                                    .html('<i class="fa fa-trash"></i>');

                        data.context.find(".attachments-url")
                                    .html("<a href='"+base_url+json.data.upload_path+json.data.file_name+"' target='_blank'>"+json.data.client_name+"</a> <i><small>"+formatFileSize(json.data.file_size)+"</small></i>");
                    }
                });
            },

            progress: function (e, data) {

                var progress = parseInt(data.loaded / data.total * 100, 10);
                data.context.find(".attachments-percent").html("%" + progress).change();
                if (progress == 100) {
                    data.context.find(".attachments-percent").html("");
                }
            },

            fail: function (e, data) {

                data.context.find("input[name='_files[]']").remove();
                data.context.find("input[name='_file_name[]']").remove();
            },
        });
    }
};


function formatFileSize(bytes) {
    if (typeof bytes !== 'number') { return ''; }

    if (bytes >= 1000000000) { return (bytes / 1000000000).toFixed(2) + ' GB'; }

    if (bytes >= 1000000) { return (bytes / 1000000).toFixed(2) + ' MB'; }

    return (bytes / 1000).toFixed(2) + ' KB';
}




// Add Back Button
$(".page-header.full-content h1")
.prepend('<a\
            href="#"\
            onclick="goBack()"\
            class="btn btn-floating btn-primary btn-ripple"\
            style="margin-left: -17px;"\
            >\
            <i class="ion-android-arrow-back"></i>\
            </a>\
        ');
function goBack() {
    window.history.back();
}



function numberFormat1(number, decimals, decPoint, thousandsSep){
    decimals = decimals || 0;
    number = parseFloat(number);

    if(!decPoint || !thousandsSep){
        decPoint = number_format.decimal;
        thousandsSep = number_format.thousands;
    }

    var roundedNumber = Math.round( Math.abs( number ) * ('1e' + decimals) ) + '';
    var numbersString = decimals ? roundedNumber.slice(0, decimals * -1) : roundedNumber;
    var decimalsString = decimals ? roundedNumber.slice(decimals * -1) : '';
    var formattedNumber = "";

    while(numbersString.length > 3){
        formattedNumber += thousandsSep + numbersString.slice(-3)
        numbersString = numbersString.slice(0,-3);
    }

    return (number < 0 ? '-' : '') + numbersString + formattedNumber + (decimalsString ? (decPoint + decimalsString) : '');
}

function numberUnFormat(number) {
    var number = number.toString();

    // 9,999.99 --> 9999.99
    if (number_format.thousands === ",") {
        number = number.replace(/[^0-9\.]+/g, "");
    }

    // 9.999,99 --> 9999.99
    if (number_format.decimal === ',') {
        number = number.replace(/[^0-9\,]+/g, "");
        number = number.replace(/[^0-9\.]+/g, ".");
    }

    return parseFloat(number);
};





function numberFormat (number, decimals, decPoint, thousandsSep) {

  number = (number + '').replace(/[^0-9+\-Ee.]/g, '')
  var n = !isFinite(+number) ? 0 : +number
  var prec = !isFinite(+decimals) ? 0 : Math.abs(decimals)
  var sep = (typeof thousandsSep === 'undefined') ? number_format.thousands : thousandsSep
  var dec = (typeof decPoint === 'undefined') ? number_format.decimal : decPoint
  var s = ''

  var toFixedFix = function (n, prec) {
    var k = Math.pow(10, prec)
    return '' + (Math.round(n * k) / k)
      .toFixed(prec)
  }

  // @todo: for IE parseFloat(0.55).toFixed(0) = 0;
  s = (prec ? toFixedFix(n, prec) : '' + Math.round(n)).split('.')
  if (s[0].length > 3) {
    s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep)
  }
  if ((s[1] || '').length < prec) {
    s[1] = s[1] || ''
    s[1] += new Array(prec - s[1].length + 1).join('0')
  }

  return s.join(dec)
}
<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class MY_Lang extends CI_Lang {

	private $langauge;
	private $languages = ['turkish', 'english'];

    public function __construct()
    {
        parent::__construct();

        $this->detect();
    }

    public function get()
    {
        return $this->langauge;
    }

    public function set($language)
    {
    	global $CFG;

    	if (in_array($language, $this->languages)) {
        	$this->langauge = $language;
        	setcookie('language', $language, time()+60*60*24*365, '/');
        }

        $CFG->set_item('language', $this->langauge);
    }

    public function getList()
    {
    	return $this->languages;
    }

    private function detect()
    {
    	global $CFG;

    	$this->langauge = $CFG->item('language');
    	$cookie_language = (String) (isset($_COOKIE['language']) ? $_COOKIE['language'] : null);

    	if (in_array($cookie_language, $this->languages)) {
        	$this->langauge = $cookie_language;
        }

        $CFG->set_item('language', $this->langauge);
    }
}

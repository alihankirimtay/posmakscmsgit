<?php defined('BASEPATH') OR exit('No direct script access allowed');

class M_Model extends MY_Model
{

    public $timestamps = ['created', 'modified', 'deleted'];
    public $pagination_delimiters = ['<li>', '</li>', '<li class="active">'];
    public $pagination_arrows = ['&laquo;', '&raquo;'];

    public function __construct()
    {
        parent::__construct();
    }

    public function beforeUpdate($methods)
    {
        $this->before_update = $methods;
        return $this;
    }

    public function findOrFail($conditions = [], $error = null)
    {
        if (!is_array($conditions)) {
            $conditions = [
                'id' => (int) $conditions
            ];
        }

        if (!$data = $this->get($conditions)) {
            if ($this->input->is_ajax_request()) {
                messageAJAX('error', $error ? $error : 'İçerik bulunamadı.');
            }

            show_404();
        }

        return $data;
    }


    public function groupWhereIn($array, $field, $array_column = null)
    {
        // Grouping Where In..
        $ids = [];
        $key = 0;
        $this->db->group_start();

        foreach ($array as $id) {

            if (is_array($id)) {
                $id = $id[$array_column];
            } else if (is_object($id)) {
                $id = $id->$array_column;
            }

            $key++;
            $ids[] = $id;
            if ($key % 20 === 0) {
                $this->db->or_where_in($field, $ids);
                $ids = [];
            }
        }

        if ($ids) {
            $this->db->or_where_in($field, $ids);
        }
        $this->db->group_end();
        // END - Grouping Where In..
        
        return $this;
    }
}
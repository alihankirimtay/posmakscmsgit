<?php
/**
 * File for class YemekSepetiStructSetRestaurantForOrderResponse
 * @package YemekSepeti
 * @subpackage Structs
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20140325-01
 * @date 2015-02-01
 */
/**
 * This class stands for YemekSepetiStructSetRestaurantForOrderResponse originally named SetRestaurantForOrderResponse
 * Meta informations extracted from the WSDL
 * - from schema : {@link http://messaging.yemeksepeti.com/MessagingWebService/Integration.asmx?WSDL}
 * @package YemekSepeti
 * @subpackage Structs
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20140325-01
 * @date 2015-02-01
 */
class YemekSepetiStructSetRestaurantForOrderResponse extends YemekSepetiWsdlClass
{
    /**
     * The SetRestaurantForOrderResult
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 1
     * @var boolean
     */
    public $SetRestaurantForOrderResult;
    /**
     * Constructor method for SetRestaurantForOrderResponse
     * @see parent::__construct()
     * @param boolean $_setRestaurantForOrderResult
     * @return YemekSepetiStructSetRestaurantForOrderResponse
     */
    public function __construct($_setRestaurantForOrderResult)
    {
        parent::__construct(array('SetRestaurantForOrderResult'=>$_setRestaurantForOrderResult),false);
    }
    /**
     * Get SetRestaurantForOrderResult value
     * @return boolean
     */
    public function getSetRestaurantForOrderResult()
    {
        return $this->SetRestaurantForOrderResult;
    }
    /**
     * Set SetRestaurantForOrderResult value
     * @param boolean $_setRestaurantForOrderResult the SetRestaurantForOrderResult
     * @return boolean
     */
    public function setSetRestaurantForOrderResult($_setRestaurantForOrderResult)
    {
        return ($this->SetRestaurantForOrderResult = $_setRestaurantForOrderResult);
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see YemekSepetiWsdlClass::__set_state()
     * @uses YemekSepetiWsdlClass::__set_state()
     * @param array $_array the exported values
     * @return YemekSepetiStructSetRestaurantForOrderResponse
     */
    public static function __set_state(array $_array,$_className = __CLASS__)
    {
        return parent::__set_state($_array,$_className);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}

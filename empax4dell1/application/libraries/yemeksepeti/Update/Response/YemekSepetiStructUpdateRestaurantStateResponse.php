<?php
/**
 * File for class YemekSepetiStructUpdateRestaurantStateResponse
 * @package YemekSepeti
 * @subpackage Structs
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20140325-01
 * @date 2015-02-01
 */
/**
 * This class stands for YemekSepetiStructUpdateRestaurantStateResponse originally named UpdateRestaurantStateResponse
 * Meta informations extracted from the WSDL
 * - from schema : {@link http://messaging.yemeksepeti.com/MessagingWebService/Integration.asmx?WSDL}
 * @package YemekSepeti
 * @subpackage Structs
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20140325-01
 * @date 2015-02-01
 */
class YemekSepetiStructUpdateRestaurantStateResponse extends YemekSepetiWsdlClass
{
    /**
     * The UpdateRestaurantStateResult
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 0
     * @var string
     */
    public $UpdateRestaurantStateResult;
    /**
     * Constructor method for UpdateRestaurantStateResponse
     * @see parent::__construct()
     * @param string $_updateRestaurantStateResult
     * @return YemekSepetiStructUpdateRestaurantStateResponse
     */
    public function __construct($_updateRestaurantStateResult = NULL)
    {
        parent::__construct(array('UpdateRestaurantStateResult'=>$_updateRestaurantStateResult),false);
    }
    /**
     * Get UpdateRestaurantStateResult value
     * @return string|null
     */
    public function getUpdateRestaurantStateResult()
    {
        return $this->UpdateRestaurantStateResult;
    }
    /**
     * Set UpdateRestaurantStateResult value
     * @param string $_updateRestaurantStateResult the UpdateRestaurantStateResult
     * @return string
     */
    public function setUpdateRestaurantStateResult($_updateRestaurantStateResult)
    {
        return ($this->UpdateRestaurantStateResult = $_updateRestaurantStateResult);
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see YemekSepetiWsdlClass::__set_state()
     * @uses YemekSepetiWsdlClass::__set_state()
     * @param array $_array the exported values
     * @return YemekSepetiStructUpdateRestaurantStateResponse
     */
    public static function __set_state(array $_array,$_className = __CLASS__)
    {
        return parent::__set_state($_array,$_className);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}

<?php
/**
 * File for class YemekSepetiEnumRestaurantStates
 * @package YemekSepeti
 * @subpackage Enumerations
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20140325-01
 * @date 2015-02-01
 */
/**
 * This class stands for YemekSepetiEnumRestaurantStates originally named RestaurantStates
 * Meta informations extracted from the WSDL
 * - from schema : {@link http://messaging.yemeksepeti.com/MessagingWebService/Integration.asmx?WSDL}
 * @package YemekSepeti
 * @subpackage Enumerations
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20140325-01
 * @date 2015-02-01
 */
class YemekSepetiEnumRestaurantStates extends YemekSepetiWsdlClass
{
    /**
     * Constant for value 'Open'
     * @return string 'Open'
     */
    const VALUE_OPEN = 'Open';
    /**
     * Constant for value 'Closed'
     * @return string 'Closed'
     */
    const VALUE_CLOSED = 'Closed';
    /**
     * Constant for value 'HugeDemand'
     * @return string 'HugeDemand'
     */
    const VALUE_HUGEDEMAND = 'HugeDemand';
    /**
     * Return true if value is allowed
     * @uses YemekSepetiEnumRestaurantStates::VALUE_OPEN
     * @uses YemekSepetiEnumRestaurantStates::VALUE_CLOSED
     * @uses YemekSepetiEnumRestaurantStates::VALUE_HUGEDEMAND
     * @param mixed $_value value
     * @return bool true|false
     */
    public static function valueIsValid($_value)
    {
        return in_array($_value,array(YemekSepetiEnumRestaurantStates::VALUE_OPEN,YemekSepetiEnumRestaurantStates::VALUE_CLOSED,YemekSepetiEnumRestaurantStates::VALUE_HUGEDEMAND));
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}

<?php
/**
 * File for class YemekSepetiStructMessageSuccessful
 * @package YemekSepeti
 * @subpackage Structs
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20140325-01
 * @date 2015-02-01
 */
/**
 * This class stands for YemekSepetiStructMessageSuccessful originally named MessageSuccessful
 * Meta informations extracted from the WSDL
 * - from schema : {@link http://messaging.yemeksepeti.com/MessagingWebService/Integration.asmx?WSDL}
 * @package YemekSepeti
 * @subpackage Structs
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20140325-01
 * @date 2015-02-01
 */
class YemekSepetiStructMessageSuccessful extends YemekSepetiWsdlClass
{
    /**
     * The messageId
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 1
     * @var int
     */
    public $messageId;
    /**
     * Constructor method for MessageSuccessful
     * @see parent::__construct()
     * @param int $_messageId
     * @return YemekSepetiStructMessageSuccessful
     */
    public function __construct($_messageId)
    {
        parent::__construct(array('messageId'=>$_messageId),false);
    }
    /**
     * Get messageId value
     * @return int
     */
    public function getMessageId()
    {
        return $this->messageId;
    }
    /**
     * Set messageId value
     * @param int $_messageId the messageId
     * @return int
     */
    public function setMessageId($_messageId)
    {
        return ($this->messageId = $_messageId);
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see YemekSepetiWsdlClass::__set_state()
     * @uses YemekSepetiWsdlClass::__set_state()
     * @param array $_array the exported values
     * @return YemekSepetiStructMessageSuccessful
     */
    public static function __set_state(array $_array,$_className = __CLASS__)
    {
        return parent::__set_state($_array,$_className);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}

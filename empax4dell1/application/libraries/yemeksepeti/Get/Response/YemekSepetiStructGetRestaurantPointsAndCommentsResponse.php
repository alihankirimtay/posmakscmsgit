<?php
/**
 * File for class YemekSepetiStructGetRestaurantPointsAndCommentsResponse
 * @package YemekSepeti
 * @subpackage Structs
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20140325-01
 * @date 2015-02-01
 */
/**
 * This class stands for YemekSepetiStructGetRestaurantPointsAndCommentsResponse originally named GetRestaurantPointsAndCommentsResponse
 * Meta informations extracted from the WSDL
 * - from schema : {@link http://messaging.yemeksepeti.com/MessagingWebService/Integration.asmx?WSDL}
 * @package YemekSepeti
 * @subpackage Structs
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20140325-01
 * @date 2015-02-01
 */
class YemekSepetiStructGetRestaurantPointsAndCommentsResponse extends YemekSepetiWsdlClass
{
    /**
     * The GetRestaurantPointsAndCommentsResult
     * @var YemekSepetiStructGetRestaurantPointsAndCommentsResult
     */
    public $GetRestaurantPointsAndCommentsResult;
    /**
     * Constructor method for GetRestaurantPointsAndCommentsResponse
     * @see parent::__construct()
     * @param YemekSepetiStructGetRestaurantPointsAndCommentsResult $_getRestaurantPointsAndCommentsResult
     * @return YemekSepetiStructGetRestaurantPointsAndCommentsResponse
     */
    public function __construct($_getRestaurantPointsAndCommentsResult = NULL)
    {
        parent::__construct(array('GetRestaurantPointsAndCommentsResult'=>$_getRestaurantPointsAndCommentsResult),false);
    }
    /**
     * Get GetRestaurantPointsAndCommentsResult value
     * @return YemekSepetiStructGetRestaurantPointsAndCommentsResult|null
     */
    public function getGetRestaurantPointsAndCommentsResult()
    {
        return $this->GetRestaurantPointsAndCommentsResult;
    }
    /**
     * Set GetRestaurantPointsAndCommentsResult value
     * @param YemekSepetiStructGetRestaurantPointsAndCommentsResult $_getRestaurantPointsAndCommentsResult the GetRestaurantPointsAndCommentsResult
     * @return YemekSepetiStructGetRestaurantPointsAndCommentsResult
     */
    public function setGetRestaurantPointsAndCommentsResult($_getRestaurantPointsAndCommentsResult)
    {
        return ($this->GetRestaurantPointsAndCommentsResult = $_getRestaurantPointsAndCommentsResult);
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see YemekSepetiWsdlClass::__set_state()
     * @uses YemekSepetiWsdlClass::__set_state()
     * @param array $_array the exported values
     * @return YemekSepetiStructGetRestaurantPointsAndCommentsResponse
     */
    public static function __set_state(array $_array,$_className = __CLASS__)
    {
        return parent::__set_state($_array,$_className);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}

<?php defined('BASEPATH') OR exit('No direct script access allowed');


class Possale {

    protected $CI;
    private $error_message;
    private $used_stocks = [];
    private $inserted_product_ids = [];
    private $interim_payments = [
        'total' => 0,
        // ... other payment types.
    ];
    private $customer_cari_payments = [];
    private $payment_types;
    private $call_accounts = [];


    private $products = [];
    private $payments = [];
    private $action;
    private $sale_id = 0;
    private $product_id = 0;
    private $location_id = 0;
    private $pos_id = 0;
    private $point_id = 0;
    private $customer_id = 0;
    private $completer_user_id = 0;
    private $user_id = 0;
    private $discount_amount;
    private $discount_type;
    private $discount_remove = false;
    private $force_product_decrement = false;
    private $payment_date;
    private $payment_type;
    private $isPanel = false;
    private $existCariPaymentCustomers = [];

    public function __construct()
    {
        $this->CI =& get_instance();

        $this->CI->load->helper('file');

        $this->CI->load->model('Sales_M');
        $this->CI->load->model('Pos_M');
        $this->CI->load->model('Points_M');
        $this->CI->load->model('Floors_M');
        $this->CI->load->model('Zones_M');
        $this->CI->load->model('Products_M');
        $this->CI->load->model('Locations_M');
        $this->CI->load->model('Saleproducts_M', 'SaleProducts_M');
        $this->CI->load->model('Producttypes_M', 'ProductTypes_M');
        $this->CI->load->model('Paymenttypes_M', 'PaymentTypes_M');
        $this->CI->load->model('Saleproductnotes_M', 'SaleProductNotes_M');
        $this->CI->load->model('Customers_M');

        $this->setPaymentTypes();
    }

    public function errorMessage()
    {
        return $this->error_message;
    }

    public function getPaymentTypes()
    {
        return $this->payment_types;
    }

    public function setProducts($products)
    {
        $this->products = (Array) $products;
        return $this;
    }

    public function setPayments($payments)
    {
        $this->payments = (Array) $payments;
        return $this;
    }

    public function setAction($action)
    {
        $this->action = $action;
        return $this;
    }

    public function setSaleId($sale_id)
    {
        $this->sale_id = (Int) $sale_id;
        return $this;
    }

    public function setProductId($product_id)
    {
        $this->product_id = (Int) $product_id;
        return $this;
    }

    public function setLocationId($location_id)
    {
        $this->location_id = (Int) $location_id;
        return $this;
    }

    public function setPosId($pos_id)
    {
        $this->pos_id = (Int) $pos_id;
        return $this;
    }

    public function setPointId($point_id)
    {
        $this->point_id = (Int) $point_id;
        return $this;
    }

    public function setCustomerId($customer_id)
    {
        $this->customer_id = (Int) $customer_id;
        return $this;
    }

    public function setCompleterUserId($completer_user_id)
    {
        $this->completer_user_id = (Int) $completer_user_id;
        return $this;
    }

    public function setUserId($user_id)
    {
        $this->user_id = (Int) $user_id;
        return $this;
    }

    public function setPaymentDate($payment_date)
    {
        $this->payment_date = (String) $payment_date;
        return $this;
    }

    public function setPaymentType($payment_type)
    {
        $this->payment_type = (String) $payment_type;
        return $this;
    }

    public function setDiscountAmount($discount_amount)
    {
        $this->discount_amount = doubleval($discount_amount);
        return $this;
    }

    public function setDiscountType($discount_type)
    {
        $this->discount_type = $discount_type;
        return $this;
    }

    public function setDiscountRemove($discount_remove)
    {
        $this->discount_remove = $discount_remove;
        return $this;
    }

    public function setForceProductDecrement($force_product_decrement)
    {
        $this->force_product_decrement = $force_product_decrement;
        return $this;
    }

    public function setIsPanel($isPanel)
    {
        $this->isPanel = $isPanel;
        return $this;
    }

    //Edit modunda cari ödemeler de tekrar ödeme sorunu çıkıyor. Bunu engellemek için kullanıldı
    public function setExistCariPaymentCustomers($customers)
    {
        $this->existCariPaymentCustomers = (Array) $customers;
        return $this;
    }

    public function processSale()
    {
        $point = $this->CI->Points_M->get([
            'id' => $this->point_id,
            'location_id' => $this->location_id,
            'active' => 1,
        ]);
        if (!$point) {
            $this->error_message = 'Masa bulunamadı.';
            return false;
        }

        $action = $this->action;
        $sale_products = [];
        $inserted_product_ids = [];
        $discount_total = 0;
        $auto_delivered = false;
        $use_stocks = false;

        $sale_id = $this->sale_id;
        $location_id = $this->location_id;
        $pos_id = $this->pos_id;
        $point_id = $this->point_id;
        $floor_id = $point->floor_id;
        $zone_id = $point->zone_id;
        $user_id = $this->user_id;
        $completer_user_id = null;
        $return_id = null;
        $return = 0;
        $customer_id = $this->customer_id;
        $amount = 0;
        $subtotal = 0;
        $taxtotal = 0;
        $gift_amount = 0;
        $interim_payment_total = 0;
        $discount_amount = $this->discount_amount;
        $discount_type = $this->discount_type;
        $discount_remove = $this->discount_remove;
        $change = 0;
        $payment_type_id = null;
        $payment_date = $this->payment_date;
        $status = null;
        $datetime = date('Y-m-d H:i:s');
        foreach ($this->getPaymentTypes() as $payment_type)
            ${$payment_type->alias} = 0; // creating variables. cash, credit, sodexo.

        if (!$this->customer_id) {
            $customer_id = null;
        }

        if ($point->service_type == 2 && !$customer_id) {
            $this->error_message = 'Müşteri seçmelisiniz.';
            return false;
        }

        if (!in_array($action, ['order', 'checkout'])) {
            $this->error_message = 'Geçersiz işlem.';
            return false;
        }

        if ($action == 'checkout' && !$this->products) {
            $this->error_message = 'Ürün seçiniz.';
            return false;
        }

        if ($sale_id) {
            if (!$this->isPanel)
                $this->CI->db->where_not_in('status', ['canceled','completed']);

            $sale = $this->CI->Sales_M->findSaleWithProducts([
                'id' => $sale_id,
                'location_id' => $location_id,
                'active' => 1,
            ]);

            if (!$sale) {
                $this->error_message = 'Geçersiz sipariş.';
                return false;
            }

            $sale_products = $sale->products;
        }

        $this->filterRequestedProducts();
        if (!$this->handleRequestedProducts($sale_products)) {
            return false;
        }
        $product_calculations = $this->calculateRequestedProducts();
        $payments = $this->calculatePayments();

        if ($discount_amount) {
            $discount = $this->calculateSaleDiscount($discount_amount, $discount_type, $product_calculations->total);
            $discount_total = $discount->discount_total;
            $discount_amount = $discount->discount_amount;
            $discount_type = $discount->discount_type;
        }

        if ($sale_id) {

            $user_id = $sale->user_id;
            $return_id = $sale->return_id;
            $return = $sale->return;

            if (!$discount_amount) {
                $sale_discount_amount = $discount_remove ? 0 : $sale->discount_amount;
                $discount = $this->calculateSaleDiscount($sale_discount_amount, $sale->discount_type, $product_calculations->total);
                $discount_total = $discount->discount_total;
                $discount_amount = $discount->discount_amount;
                $discount_type = $discount->discount_type;
            }
        }

        if ($action == 'order') {

            $message = 'Sipariş alındı.';
            $status = 'pending';
            $completer_user_id = null;
            $amount = $product_calculations->total - $discount_total;
            $subtotal = $product_calculations->subtotal;
            $taxtotal = $product_calculations->taxtotal;
            $gift_amount = $product_calculations->gifttotal;
            $interim_payment_total = $product_calculations->interim_payments->total;

        } elseif ($action == 'checkout') {

            $message = 'Hesap alındı.';
            $status = 'completed';
            $use_stocks = true;
            $completer_user_id = (int) $this->completer_user_id;
            $payment_type_id = $this->getPaymentTypeId($payments, $product_calculations->interim_payments);
            $amount = $product_calculations->total - $discount_total;
            $subtotal = $product_calculations->subtotal;
            $taxtotal = $product_calculations->taxtotal;
            $gift_amount = $product_calculations->gifttotal;
            $interim_payment_total = $product_calculations->interim_payments->total;
            $change = round($payments->total + $product_calculations->interim_payments->total - $amount, 2);
            foreach ($this->getPaymentTypes() as $payment_type)
                ${$payment_type->alias} = $payments->{$payment_type->alias} + $product_calculations->interim_payments->{$payment_type->alias};

            foreach ($this->customer_cari_payments as $customer_id => $cari_payment) {
                $customer = $this->CI->Customers_M->where('id',$customer_id)->get();
                
                if(!$customer) {
                    $this->error_message = 'Kullanıcı bulunamadı.';
                    return false;
                }

            }
            
            if ($change < 0) {
                $this->error_message = 'Yetersiz ödeme.';
                return false;
            }
            if ($change > $cash) {
                $this->error_message = 'Ödenen nakit miktarı para üstü için yetersiz.';
                return false;
            }

        }

        $_POST = array_merge($_POST, [
            'location_id' => $location_id,
            'point_id' => $point_id,
            'pos_id' => $pos_id,
            'floor_id' => $floor_id,
            'zone_id' => $zone_id,
            'user_id' => $user_id,
            'customer_id' => $customer_id,
            'completer_user_id' => $completer_user_id,
            'service_type' => $point->service_type,
            'sale_type_id' => 1,
            'return_id' => $return_id,
            'return' => $return,
            'amount' => $amount,
            'subtotal' => $subtotal,
            'taxtotal' => $taxtotal,
            'gift_amount' => $gift_amount,
            'interim_payment_amount' => $interim_payment_total,
            'discount_amount' => $discount_amount,
            'discount_type' => $discount_type,
            'change' => $change,
            'payment_type_id' => $payment_type_id,
            'payment_date' => $payment_date,
            'status' => $status,
            'active' => 1,
        ]);
        foreach ($this->getPaymentTypes() as $payment_type) {
            if ($payment_type->alias == "cash") {
                $_POST[$payment_type->alias] = ${$payment_type->alias} - $change;
            }
            else {
                $_POST[$payment_type->alias] = ${$payment_type->alias};
            }
        }

        if ($sale_id) {
            if (!$this->CI->Sales_M->rulesPaymetTypes()->from_form(null, null, ['id' => $sale_id])->update()) {
                $this->error_message = validation_errors();
                return false;
            }

        } else {
            if (!$new_sale_id = $this->CI->Sales_M->rulesPaymetTypes()->from_form()->insert()) {
                $this->error_message = validation_errors();
                return false;
            }

            $sale_id = $new_sale_id;

        }

        if ($action == 'checkout' && $point->service_type == 2) {
           $auto_delivered = true;
        }

        $inserted_product_ids = $this->processProducts($sale_id, $datetime, $auto_delivered);

        if ($use_stocks) {
            $this->processProductStocks();
        }

        if ($action == 'order') {

            $this->CI->Points_M->update([
                'sale_id' => $sale_id,
                'status' => 1,
                'opened_at' => $datetime,
            ], ['id' => $point_id]);

        } elseif ($action == 'checkout') {

            $this->CI->Points_M->update([
                'sale_id' => null,
                'status' => 0,
                'opened_at' => null,
            ], ['id' => $point_id]);


            if ($this->customer_cari_payments) {
                foreach ($this->customer_cari_payments as $customer_id => $cari_payment) {

                    if (!in_array($customer_id, $this->existCariPaymentCustomers)) 
                    {

                        $sales_data = [
                        'sale_id' => $sale_id,
                        'customer_id' => $customer_id,
                        'amount' => $cari_payment
                        ];
                        $datetime = date('Y-m-d H:i:s');

                        $this->CI->db->insert('sales_cari', $sales_data);
                        $id = $this->CI->db->insert_id();

                        $this->CI->db->where('id', $customer_id)->set('amount', "amount + {$cari_payment}", false)->update('customers', [
                            'modified' =>$datetime 
                        ]);

                        $customer_amount = $this->CI->db->select('amount')->where('id', $customer_id)->get('customers')->row();
                        $this->CI->db->where('id', $id)->set('customer_amount', $customer_amount->amount)->update('sales_cari');

                    }

                }

                $this->CI->db->where('id', $pos_id)->set('amount', "amount + {$amount}", false)->update('pos', [
                    'modified' => date("Y-m-d H:i:s"),
                ]);
                
                $pos_amount = $this->CI->db->select('amount')->where('id', $pos_id)->get('pos')->row();
                $this->CI->db->where('id', $sale_id)->set('pos_amount', $pos_amount->amount)->update('sales');
                
            }

            

        }

        return (Object) compact('message', 'status', 'new_sale_id', 'sale_id', 'inserted_product_ids', 'product_calculations');
    }

    public function processReturn()
    {
        $sale_id = $this->sale_id;
        $user_id = $this->user_id;
        $location_id = $this->location_id;
        $pos_id = $this->pos_id;
        $payment_type = $this->payment_type;
        $payment_type_id = $this->getPaymentTypeIdByAlias($payment_type);


        $amount = 0;
        $discount_amount = 0;
        $discount_type = 0;
        $datetime = date('Y-m-d H:i:s');
        $discount_total = 0;

        if (!$this->products) {
            $this->error_message = 'Ürün seçiniz.';
            return false;
        }

        if (!$payment_type_id) {
            $this->error_message = 'Ödeme tipini seçiniz.';
            return false;
        }

        $sale = $this->CI->Sales_M
        ->order_by('id', 'desc')
        ->findSaleWithProducts([
            'id' => $sale_id,
            'location_id' => $location_id,
            'status' => 'completed',
            'return_id' => null,
            'active' => 1,
        ]);
        if (!$sale) {
            $this->error_message = 'Geçersiz satış.';
            return false;
        }

        $this->filterRequestedReturnProducts();
        if (!$this->handleRequestedReturnProducts($sale->products)) {
            return false;
        }
        $product_calculations = $this->calculateRequestedProducts();

        $discount = $this->calculateSaleDiscount($sale->discount_amount, $sale->discount_type, $product_calculations->total);
        $discount_total = $discount->discount_total;
        $discount_amount = $discount->discount_amount;
        $discount_type = $discount->discount_type;

        $amount = $product_calculations->total - $discount_total;

        $_POST = array_merge($_POST, [
            'location_id' => $location_id,
            'return_id' => $sale->id,
            'point_id' => $sale->point_id,
            'pos_id' => $pos_id,
            'floor_id' => $sale->floor_id,
            'zone_id' => $sale->zone_id,
            'user_id' => $user_id,
            'customer_id' => $sale->customer_id,
            'completer_user_id' => $user_id,
            'service_type' => $sale->service_type,
            'sale_type_id' => 1,
            'amount' => $amount,
            'subtotal' => $product_calculations->subtotal,
            'taxtotal' => $product_calculations->taxtotal,
            'gift_amount' => 0,
            'interim_payment_amount' => 0,
            'discount_amount' => $discount_amount,
            'discount_type' => $discount_type,
            'change' => 0,
            'payment_type_id' => $payment_type_id,
            'payment_date' => $datetime,
            'status' => 'completed',
            'active' => 1,
        ]);

        foreach ($this->getPaymentTypes() as $_payment_type) {
            $refund = ($_payment_type->alias == $payment_type) ? $amount : 0;
            $_POST[$_payment_type->alias] = $refund;
        }

        if (!$return_sale_id = $this->CI->Sales_M->rulesPaymetTypes()->from_form()->insert()) {
            $this->error_message = validation_errors();
            return false;
        }

        $this->CI->Sales_M->update(['return' => 1], ['id' => $sale->id]);

        $this->CI->db->where('id', $this->pos_id)->set('amount', "amount - {$amount}", false)->update('pos', [
            'modified' => date("Y-m-d H:i:s"),
        ]);

        $pos_amount = $this->CI->db->select('amount')->where('id', $customer_id)->get('pos')->row();
        $this->CI->db->where('id', $return_sale_id)->set('pos_amount', $pos_amount->amount)->update('sales');

        $this->processProducts($return_sale_id, $datetime);
        $this->processReturnedProducts();

        return $return_sale_id;
    }

    public function cancelSale()
    {
        $sale_id = $this->sale_id;
        $location_id = $this->location_id;
        $point_id = $this->point_id;


        $sale = $this->CI->Sales_M->findSaleWithProducts([
            'id' => $sale_id,
            'location_id' => $location_id,
            'status' => 'pending'
        ]);

        if (!$sale) {
            $this->error_message = 'Boş masa iptal edilemez.';
            return false;
        }

         foreach ($sale->products as $product) {
            if ($product->completed > 0) {
                $this->error_message = 'Hazırlanan ürün bulunduğu için masa iptal edilemez.';
                return false;
            }
        }

       $this->CI->Sales_M->update([
            'status' => 'cancelled'
        ], ['id' =>  $sale->id]);

        $this->CI->Points_M->update([
            'sale_id' => null,
            'status' => 0,
            'opened_at' => null,
        ], ['id' => $sale->point_id]);

        return true;
    }

    public function moveAndMergeTables($source_point_id, $target_point_id)
    {
        $location_id = $this->location_id;
        $source_point_id = $source_point_id;
        $target_point_id = $target_point_id;
   
        $target_sale_id = null;
        $datetime = date('Y-m-d H:i:s');

        $source_point = $this->CI->Points_M->where('location_id', $this->CI->user->locations)->get([
            'id' => $source_point_id,
            'location_id' => $location_id,
            'active' => 1,
        ]);
        if (!$source_point) {
            $this->error_message = 'Geçersiz kaynak masa.';
            return false;
        }
        

        $target_point = $this->CI->Points_M->where('location_id', $this->CI->user->locations)->get([
            'id' => $target_point_id,
            'location_id' => $location_id,
            'active' => 1,
        ]);
        if (!$target_point) {
        $this->error_message = 'Geçersiz hedef masa.';
            return false;
        }

        if (!$source_point->sale_id) {
            $this->error_message = 'Kaynak masa boş.';
            return false;
        } elseif ($source_point->id == $target_point->id) {
            $this->error_message = 'Taşınacak masalar farklı olmalı.';
            return false;
        }

        $sale = $this->CI->Sales_M
        ->findSaleWithProducts([
            'id' => $source_point->sale_id,
            'location_id' => $location_id,
            'point_id' => $source_point_id,
            'status' => 'pending',
            'active' => 1,
        ]);
        if (!$sale) {
            $this->error_message = 'Kaynak masada sipariş bulunamadı.';
            return false;
        }

        if ($target_point->sale_id) {
            $target_sale = $this->CI->Sales_M
            ->findSaleWithProducts([
                'id' => $target_point->sale_id,
                'location_id' => $location_id,
                'point_id' => $target_point_id,
                'status' => 'pending',
                'active' => 1,
            ]);

            if ($target_sale) {
                $target_sale_id = $target_sale->id;
            }
        }

        // Cancelling the source sale
        $this->CI->Sales_M->update([
            'status' => 'cancelled'
        ], ['id' =>  $sale->id]);

        $this->CI->Points_M->update([
            'sale_id' => null,
            'status' => 0,
            'opened_at' => null,
        ], ['id' => $source_point->id]);

        $products = $sale->products;
        unset($sale->products);

        if ($target_sale_id) {
            unset($target_sale->products);
            $target_sale->amount += $sale->amount;
            $target_sale->subtotal += $sale->subtotal;
            $target_sale->taxtotal += $sale->taxtotal;
            $target_sale->gift_amount += $sale->gift_amount;
            $target_sale->interim_payment_amount += $sale->interim_payment_amount;
            $this->CI->Sales_M->update($target_sale, ['id' => $target_sale->id]);
        } else {
            $sale->id = null;
            $sale->point_id = $target_point->id;
            $sale->floor_id = $target_point->floor_id;
            $sale->zone_id = $target_point->zone_id;
            $sale->location_id = $target_point->location_id;
            $target_sale_id = $this->CI->Sales_M->insert($sale);

            $this->CI->Points_M->update([
                'sale_id' => $target_sale_id,
                'status' => 1,
                'opened_at' => $datetime,
            ], ['id' => $target_point->id]);
        }

        foreach ($products as $product) {

            $additional_products = $product->additional_products;
            unset($product->additional_products);

            if ($product->type == 'package') {
                $package_products = $product->products;
                unset($product->products);
            } elseif ($product->type == 'product') {
                $stocks = $product->stocks;
                unset($product->stocks);
            }

            $product->id = null;
            $product->sale_id = $target_sale_id;
            $product->created = $datetime;
            $this->CI->db->insert('sale_has_products', $product);
            $new_sale_product_id = $this->CI->db->insert_id();

            if ($product->type == 'package') {
                foreach ($package_products as $package_product) {
                    $package_product_stocks = $package_product->stocks;
                    unset($package_product->stocks, $package_product->additional_products);

                    $package_product->id = null;
                    $package_product->package_id = $new_sale_product_id;
                    $this->CI->db->insert('package_has_products', $package_product);
                    $new_package_product_id = $this->CI->db->insert_id();

                    foreach ($package_product_stocks as $package_product_stock) {
                        unset($package_product_stock->stock_name);
                        $package_product_stock->sale_package_id = $new_package_product_id;
                        $this->CI->db->insert('sale_packages_stocks', $package_product_stock);
                    }
                }
            } elseif ($product->type == 'product') {
                foreach ($stocks as $stock) {
                    unset($stock->stock_name);
                    $stock->sale_product_id = $new_sale_product_id;
                    $this->CI->db->insert('sale_products_stocks', $stock);
                }
            }

            foreach ($additional_products as $additional_product) {

                $additional_product_stocks = $additional_product->stocks;
                unset($additional_product->stocks, $additional_product->additional_products);
                
                $additional_product->id = null;
                $additional_product->additional_sale_product_id = $new_sale_product_id;
                $additional_product->sale_id = $target_sale_id;
                $additional_product->created = $datetime;
                $this->CI->db->insert('sale_has_products', $additional_product);
                $new_sale_additional_product_id = $this->CI->db->insert_id();

                foreach ($additional_product_stocks as $additional_product_stock) {
                    unset($additional_product_stock->stock_name);
                    $additional_product_stock->sale_product_id = $new_sale_additional_product_id;
                    $this->CI->db->insert('sale_products_stocks', $additional_product_stock);
                }
            }
        }

        return $target_sale_id;
    }

    public function makeInterimPayment()
    {
        $location_id = $this->location_id;
        $sale_id = $this->sale_id;
        $point_id = $this->point_id;
        $products = $this->products;
        $payment_type = $this->payment_type;

        $interim_payment_amount = 0;
        $datetime = date('Y-m-d H:i:s');

        $sale_product_updates = [];
        $payment_types = $this->getPaymentTypes();
        $payment_type_aliases = array_map(function ($row) { return $row->alias; }, $payment_types);

        if (!$products) {
            $this->error_message = 'Ürün seçiniz.';
            return false;
        } elseif (!in_array($payment_type, $payment_type_aliases)) {
            $this->error_message = 'Seçilen ödeme geçersiz.';
            return false;
        }

        $sale = $this->CI->Sales_M->where('location_id', $this->CI->user->locations)->findSaleWithProducts([
            'id' => $sale_id,
            'location_id' => $location_id,
            'point_id' => $point_id,
            'status' => 'pending',
            'active' => 1,
        ]);
        if (!$sale) {
            $this->error_message = 'Satış bulunamadı.';
            return false;
        }

        foreach ($products as $sale_product_id => $quantity) {
            
            $paid = 0;
            $unpaid = 0;
            $quantity = abs($quantity);
            $sale_product_id = (int) $sale_product_id;

            if (!$quantity)
                continue;

            $sale_product = null;
            foreach ($sale->products as $product) {
                if ($product->id == $sale_product_id) {
                    $sale_product = $product;

                    $price = $product->price + $product->price * $product->tax / 100;
                    $price = round($price, 2);
                    $interim_payment_amount += $price * $quantity;

                    foreach ($product->additional_products as $additional_product) {
                        $price = $additional_product->price + $additional_product->price * $additional_product->tax / 100;
                        $price = round($price, 2);
                        $interim_payment_amount += $price * $quantity;
                    }
                    break;
                }
            }
            if (!$sale_product) {
                $this->error_message = 'Hatalı ürün.';
                return false;
            }

            foreach ($payment_types as $type) {
                $paid += $sale_product->{"paid_{$type->alias}"};
            }

            $unpaid = $sale_product->quantity - $paid;
            if ($unpaid < $quantity) {
                $this->error_message = "{$sale_product->title} için en fazla {$unpaid} adet ödeme yapabilirsiniz.";
                return false;
            }

            $sale_product_updates[$sale_product->id] = [
                "paid_{$payment_type}" => $sale_product->{"paid_{$payment_type}"} + $quantity,
                'modified' => $datetime,
            ];
        }

        foreach ($sale_product_updates as $id => $update) {
            $this->CI->SaleProducts_M
                ->group_start()
                    ->where('id', $id)
                    ->or_where('additional_sale_product_id', $id)
                ->group_end()
            ->update($update);
        }


        $this->CI->Sales_M->update([
            'interim_payment_amount' => $sale->interim_payment_amount + $interim_payment_amount
        ], [
            'id' => $sale->id
        ]);

        return $interim_payment_amount;
    }

    public function makeProductsAsGift()
    {
        $location_id = $this->location_id;
        $user_id = $this->user_id;
        $sale_id = $this->sale_id;
        $point_id = $this->point_id;
        $products = $this->products;

        $gift_amount =
        $gift_subtotal = 0;
        $datetime = date('Y-m-d H:i:s');

        $sale_product_inserts = [];
        $sale_product_updates = [];
        $new_sale_product_ids = [];
        $payment_types = $this->getPaymentTypes();

        if (!$products) {
            $this->error_message = 'Ürün seçiniz.';
            return false;
        }

        $sale = $this->CI->Sales_M->findSaleWithProducts([
            'id' => $sale_id,
            'location_id' => $location_id,
            'point_id' => $point_id,
            'status' => 'pending',
            'active' => 1,
        ]);

        if (!$sale) {
            $this->error_message = 'Satış bulunamadı.';
            return false;
        }

        foreach ($products as $sale_product_id => $quantity) {
            
            $paid = 0;
            $unpaid = 0;
            $quantity = abs($quantity);
            $sale_product_id = (int) $sale_product_id;

            if (!$quantity)
                continue;

            $sale_product = null;
            foreach ($sale->products as $product) {
                if ($sale_product_id == $product->id) {
                    $sale_product = $product;
                }
            }

            if (!$sale_product) {
                $this->error_message = 'Hatalı ürün.';
                return false;
            }

            foreach ($payment_types as $type) {
                $paid += $sale_product->{"paid_{$type->alias}"};
            }

            $unpaid = $sale_product->quantity - $paid;
            if ($unpaid < $quantity) {
                $this->error_message = "{$sale_product->title} için en fazla {$unpaid} adet ikram yapabilirsiniz.";
                return false;
            }

            $gift_data = $this->calculateGiftProduct($sale_product->quantity, $sale_product->completed, $sale_product->delivered, $quantity);
            
            $sale_product_updates[$sale_product->id] = [
                'quantity' => $gift_data->main_product->quantity,
                'completed' => $gift_data->main_product->completed,
                'delivered' => $gift_data->main_product->delivered,
                'modified' => $datetime,
            ];

            $sale_product->quantity = $gift_data->gift_product->quantity;
            $sale_product->completed = $gift_data->gift_product->completed;
            $sale_product->delivered = $gift_data->gift_product->delivered;

            if ($sale_product->type == "package") {
                
                $sale_package_products_updates[$sale_product->id] = [];
                
                foreach ($sale_product->products as $gift_package_product) {

                    $gift_package_data = $this->calculateGiftProduct($gift_package_product->quantity, $gift_package_product->completed, 0, $quantity);

                    $sale_package_products_updates[$sale_product->id][$gift_package_product->id] = [
                        'quantity' => $gift_package_data->main_product->quantity,
                        'completed' => $gift_package_data->main_product->completed,
                        'modified' => $datetime,
                    ];

                    $gift_package_product->quantity = $gift_package_data->gift_product->quantity;
                    $gift_package_product->completed = $gift_package_data->gift_product->completed;

                }

            }

            foreach ($this->getPaymentTypes() as $payment_type)
                $sale_product->{"paid_{$payment_type->alias}"} = 0;

            $sale_product_inserts[$sale_product->id] = $sale_product;

        }


        // Update main product
        foreach ($sale_product_updates as $id => $update) {
            $this->CI->SaleProducts_M
                ->group_start()
                    ->where('id', $id)
                    ->or_where('additional_sale_product_id', $id)
                ->group_end()
            ->update($update);
        }

        if (isset($sale_package_products_updates[$sale_product->id])) {

            foreach ($sale_package_products_updates[$sale_product->id] as $id => $update) {
                $this->CI->db
                    ->where('id', $id)
                    ->update('package_has_products', $update);
            }
            
        }


        // Insert product as gift
        foreach ($sale_product_inserts as $product) {

            $additional_products = $product->additional_products;
            unset($product->additional_products);

            if ($product->type == 'package') {
                $package_products = $product->products;
                unset($product->products);
            } elseif ($product->type == 'product') {
                $stocks = $product->stocks;
                unset($product->stocks);
            }

            $tax = $product->price * $product->tax / 100;
            $tax = round($tax, 3);
            $tax *= $product->quantity;
            $price = $product->price * $product->quantity;
            $gift_subtotal += $price;
            $gift_amount += $price + $tax;

            $product->id = null;
            $product->user_id = $user_id;
            $product->is_gift = 1;
            $new_sale_product_id = $this->CI->SaleProducts_M->insert($product);
            $new_sale_product_ids[] = $new_sale_product_id;


            if ($product->type == 'package') {
                foreach ($package_products as $package_product) {
                    $package_product_stocks = $package_product->stocks;
                    unset($package_product->stocks, $package_product->additional_products);

                    $package_product->id = null;
                    $package_product->package_id = $new_sale_product_id;
                    $this->CI->db->insert('package_has_products', $package_product);
                    $new_package_product_id = $this->CI->db->insert_id();

                    foreach ($package_product_stocks as $package_product_stock) {
                        unset($package_product_stock->stock_name);
                        $package_product_stock->sale_package_id = $new_package_product_id;
                        $this->CI->db->insert('sale_packages_stocks', $package_product_stock);
                    }
                }
            } elseif ($product->type == 'product') {
                foreach ($stocks as $stock) {
                    unset($stock->stock_name);
                    $stock->sale_product_id = $new_sale_product_id;
                    $this->CI->db->insert('sale_products_stocks', $stock);
                }
            }

            foreach ($additional_products as $additional_product) {

                $additional_product_stocks = $additional_product->stocks;
                unset($additional_product->stocks, $additional_product->additional_products);

                $tax = $additional_product->price * $additional_product->tax / 100;
                $tax = round($tax, 3);
                $tax *= $product->quantity;
                $price = $additional_product->price * $product->quantity;
                $gift_subtotal += $price;
                $gift_amount += $price + $tax;
                
                $additional_product->id = null;
                $additional_product->quantity = $product->quantity;
                $additional_product->is_gift = 1;
                $additional_product->additional_sale_product_id = $new_sale_product_id;
                $additional_product->created = $datetime;
                foreach ($this->getPaymentTypes() as $payment_type)
                    $additional_product->{"paid_{$payment_type->alias}"} = 0;
                $this->CI->db->insert('sale_has_products', $additional_product);
                $new_sale_additional_product_id = $this->CI->db->insert_id();

                foreach ($additional_product_stocks as $additional_product_stock) {
                    unset($additional_product_stock->stock_name);
                    $additional_product_stock->sale_product_id = $new_sale_additional_product_id;
                    $this->CI->db->insert('sale_products_stocks', $additional_product_stock);
                }
            }
        }

        $this->CI->Sales_M->update([
            'amount' => $sale->amount - $gift_amount,
            'subtotal' => $sale->subtotal - $gift_subtotal,
            'gift_amount' => $sale->gift_amount + $gift_amount,
        ], [
            'id' => $sale->id
        ]);


        return $new_sale_product_ids;
    }

    public function getCompletedUnDeliveredProducts()
    {
        $location_id = $this->location_id;
        $user_id = $this->user_id;
        $date_start = date('Y-m-d H:i:s', strtotime('-6 HOURS'));


        $sales = $this->CI->Sales_M
        ->fields(['*', '(select title from points where id = point_id) As point_title', '(select title from zones where id = zone_id) As zone_title'])
        ->findSalesWithProducts([
            'location_id' => $location_id,
            'created >=' => $date_start,
            'status !=' => 'cancelled',
            'active' => 1,
        ], [
            'sale_has_products' => [
                'where' => [
                    'user_id' => $user_id,
                ]
            ]
        ]);

        $products = [];
        foreach ($sales as $sale) {
            if ($sale->call_account && $sale->status == 'pending') {
                $this->call_accounts[] = $sale;
            }
            foreach ($sale->products as $product) {

                $product->quantity = $product->completed - $product->delivered;
                $product->point_title = $sale->point_title;
                $product->zone_title = $sale->zone_title;

                if ($product->quantity > 0) {
                    $products[] = $product;
                }
            }
        }

        return $products;
    }

    public function getActiveCallAccounts()
    {
        return $this->call_accounts;
    }

    public function productDelivered()
    {
        $location_id = $this->location_id;
        $sale_id = $this->sale_id;
        $product_id = $this->product_id;

        $sale = $this->CI->Sales_M
        ->with_products([
            'where' => [
                'id' => $product_id,
            ],
            'limit' => 1
        ])
        ->get([
            'id' => $sale_id,
            'location_id' => $location_id,
            'status !=' => 'cancelled',
            'active' => 1,
        ]);

        if (!$sale) {
            $this->error_message = 'Geçersiz sipariş.';
            return false;
        }
        if (!isset($sale->products[0])) {
            $this->error_message = 'Geçersiz ürün.';
            return false;
        }

        $product = $sale->products[0];

        $this->CI->SaleProducts_M->limit(1)->update([
            'delivered' => $product->completed
        ], [
            'id' => $product->id
        ]);

        return true;
    }

    public function setViewedCallAccount()
    {
        $location_id = $this->location_id;
        $sale_id = $this->sale_id;
        $product_id = $this->product_id;

        $sale = $this->CI->Sales_M
        ->get([
            'id' => $sale_id,
            'location_id' => $location_id,
            'status' => 'pending',
            'active' => 1,
        ]);

        if (!$sale) {
            $this->error_message = 'Geçersiz sipariş.';
            return false;
        }

        $this->CI->Sales_M->where('id', $sale_id)->limit(1)->update(['call_account' => 0]);

        return true;
    }

    private function setPaymentTypes()
    {
        $this->payment_types = $this->CI->db
        ->where(['active' => 1])
        ->get('payment_types')
        ->result();
    }

    private function filterRequestedProducts()
    {
        $products = [];
        
        $i = 0;
        foreach ($this->products as $index => $product) {

            if (!isset($product['product_id'], $product['quantity']))
                continue;

            $product_id = (int) $product['product_id'];
            $quantity = (int) $product['quantity'];

            if ($product_id && $quantity) {

                $sale_product_id = isset($product['sale_product_id']) ? $product['sale_product_id'] : null;

                // Products
                $products[$i] = (Object) [
                    'index' => $index,
                    'product_id' => $product_id,
                    'quantity' => $quantity,
                    'sale_product_id' => $sale_product_id,
                    'stocks' => [],
                    'additional_products' => [],
                    'product' => [], // System product
                    'sale_product' => [], // Product in sale_has_products table
                    'text_notes' => [],
                    'sound_notes' => [],
                ];

                // Stoks
                if (isset($product['stocks'])) {
                    $stocks = (Array) $product['stocks'];

                    foreach ($stocks as $stock_id) {
                        $stock_id = (int) $stock_id;

                        if ($stock_id) {
                            $products[$i]->stocks[$stock_id] = $stock_id;
                        }
                    }
                }

                // Additional Prorducts
                if (isset($product['additional_products'])) {
                    $additional_products = (Array) $product['additional_products'];

                    foreach ($additional_products as $additional_product_id) {
                        $additional_product_id = (int) $additional_product_id;

                        if ($additional_product_id) {
                            $products[$i]->additional_products[$additional_product_id] = $additional_product_id;
                        }
                    }
                }

                // Text Notes
                if (isset($product['text_notes'])) {
                    $text_notes = (Array) $product['text_notes'];

                    foreach ($text_notes as $text_note) {
                        $text_note = trim($text_note);

                        if ($text_note) {
                            $products[$i]->text_notes[] = $text_note;
                        }
                    }
                }

                // Sound Notes
                if (isset($product['sound_notes'])) {
                    $sound_notes = (Array) $product['sound_notes'];

                    foreach ($sound_notes as $sound_note_id) {
                        $sound_note_id = (int) $sound_note_id;

                        if ($sound_note_id) {
                            $products[$i]->sound_notes[] = $sound_note_id;
                        }
                    }
                }

                $i++;
            }
        }

        $this->products = $products;
    }

    private function filterRequestedReturnProducts()
    {
        $products = [];

        foreach ($this->products as $sale_product_id => $quantity) {

            $sale_product_id = (int) $sale_product_id;
            $quantity = (int) $quantity;

            if (!$sale_product_id || !$quantity)
                continue;

            $products[$sale_product_id] = (Object) [
                'index' => $sale_product_id,
                'quantity' => $quantity,
                'sale_product_id' => $sale_product_id,
                'additional_products' => [],
                'product' => [], 
                'text_notes' => [],
                'sound_notes' => [],
            ];
        }

        $this->products = $products;
    }

    private function handleRequestedProducts(Array $sale_products = [])
    {
        $inserts = 
        $updates = 
        $deletes = [];

        $system_products = $this->CI->Products_M->findProductsWithRelatedItems([
            'products' => [
                'location_id' => $this->location_id,
                'active' => 1,
            ],
            'stocks' => [
                'stocks.location_id' => $this->location_id,
                'stocks.active' => 1,
            ],
            'additional_products' => [
                'location_id' => $this->location_id,
                'active !=' => 3,
            ],
        ]);

        foreach ($this->products as $key => $requested_product) {

            if (!isset($system_products[$requested_product->product_id])) {
                $this->error_message = 'Ürün geçersiz.';
                return false;
            }

            $requested_product->product = clone $system_products[$requested_product->product_id];
            
            // Update
            if ($requested_product->sale_product_id) {

                // if the product has at least one interim payment then stock and additional products wont change.
                $interim_payment_counts = 0;

                $requested_product->insert_additional_products = [];
                $requested_product->update_additional_products = [];
                $requested_product->delete_additional_products = [];
                
                $sale_product = null;
                foreach ($sale_products as $_sale_product) {
                    if ($_sale_product->id == $requested_product->sale_product_id) {
                        $sale_product = $_sale_product;
                        break;
                    }
                }

                if (!$sale_product) {
                    $this->error_message = 'Sipariş edilen ürün geçersiz.';
                    return false;
                }

                if ($sale_product->completed > $requested_product->quantity) {
                    if ($this->force_product_decrement == true) {
                        $sale_product->completed = $requested_product->quantity;
                    } else {
                        $this->error_message = "{$sale_product->completed} adet {$sale_product->title} hazır olduğu için bu miktarın altına inemezsiniz.";
                        return false;
                    }
                }

                $interim_payment_counts = $this->countInterimPayments($sale_product);
                if ($interim_payment_counts > $requested_product->quantity) {
                    $this->error_message = 'Sipariş edilen ürün adedi ara ödemesi yapılmış adetten az olamaz.';
                    return false;
                }
                $requested_product->sale_product = $sale_product;

                if ($sale_product->type == 'product') {

                    // Check the stock selections in the product stocks
                    foreach ($requested_product->stocks as $stock_id) {
                        if (!isset($sale_product->stocks[$stock_id])) {
                            $this->error_message = 'Malzeme geçersiz.';
                            return false;
                        }
                        if (!$sale_product->stocks[$stock_id]->optional) {
                            $this->error_message = 'Malzeme seçimi geçersiz.';
                            return false;
                        }

                        $requested_product->sale_product->stocks[$stock_id]->unwanted = true;
                    }
                    if ($interim_payment_counts) {
                        foreach ($sale_product->stocks as $stock) {
                            if ($stock->in_use && isset($requested_product->stocks[$stock->stock_id])) {
                                $this->error_message = 'Ara ödemesi alınmış ürünün malzemesi silinemez.';
                                return false;
                            } else if (!$stock->in_use && !isset($requested_product->stocks[$stock->stock_id])) {
                                $this->error_message = 'Ara ödemesi alınmış ürüne malzeme eklenemez.';
                                return false;
                            }
                        }
                    }

                } else if ($sale_product->type == 'package') {
                    
                    foreach ($sale_product->products as $package_product_key => $package_product) {
                        if ($package_product->completed > $requested_product->quantity) {
                            if ($this->force_product_decrement == true) {
                                $requested_product->sale_product->products[$package_product_key]->completed = $requested_product->quantity;
                                $requested_product->sale_product->completed = $requested_product->quantity;
                            } else {
                                $this->error_message = "{$sale_product->title} menü içeriğinde {$package_product->completed} adet içerik hazır olduğu için bu miktarın altına inemezsiniz.";
                                return false;
                            }
                        }
                    }
                }

                
                // Loop the new selected additional products
                foreach ($requested_product->additional_products as $additional_product_id) {
                    if (isset($sale_product->additional_products[$additional_product_id])) {
                        $requested_product->update_additional_products[$additional_product_id] = $sale_product->additional_products[$additional_product_id];
                    } else {
                        $requested_product->insert_additional_products[] = $system_products[$additional_product_id];
                        if ($interim_payment_counts) {
                            $this->error_message = 'Ara ödemesi alınmış ürüne ilave ürün eklenemez.';
                            return false;
                        }
                    }
                }
                // Loop the old selected additional products
                foreach ($sale_product->additional_products as $sale_product_additional_product) {
                    // if the old additional product doesn't exist in the new additional product selections, then delete it.
                    if (!isset($requested_product->additional_products[$sale_product_additional_product->product_id])) {
                        $requested_product->delete_additional_products[$sale_product_additional_product->id] = $sale_product_additional_product;
                        if ($interim_payment_counts) {
                            $this->error_message = 'Ara ödemesi alınmış ürünün ilave ürünü silinemez.';
                            return false;
                        }
                    }
                }

                // Loop the new sound notes
                foreach ($requested_product->sound_notes as $key_sound_note => $sound_note_id) {
                    $sound_note_exists = $this->CI->SaleProductNotes_M->get([
                        'id' => $sound_note_id,
                        'sale_product_id' => null,
                        'type' => 2,
                    ]);
                    if ($sound_note_exists) {
                        $requested_product->sound_notes[$key_sound_note] = $sound_note_id;
                    } else {
                        $this->error_message = 'Sesli not geçersiz.';
                        return false;
                    }
                }


                $updates[] = $requested_product;
            }

            // Insert
            else {

                if ($requested_product->product->type == 'product') {

                    foreach ($requested_product->product->stocks as $s => $stock) {
                        $requested_product->product->stocks[$s] = clone $stock;
                    }

                    // Check the stock selections in the product stocks
                    foreach ($requested_product->stocks as $stock_id) {
                        if (!isset($requested_product->product->stocks[$stock_id])) {
                            $this->error_message = 'Malzeme geçersiz.';
                            return false;
                        }
                        if (!$requested_product->product->stocks[$stock_id]->product_stock_optional) {
                            $this->error_message = 'Malzeme seçimi geçersiz.';
                            return false;
                        }

                        $requested_product->product->stocks[$stock_id]->unwanted = true;
                    }

                }
                
                // Check the Additional products in product and pacakge
                foreach ($requested_product->additional_products as $additional_product_id) {
                    if (!isset($requested_product->product->additional_products[$additional_product_id])) {
                        $this->error_message = 'İlave Ürün geçersiz.';
                        return false;
                    }
                }

                // Loop the new sound notes
                foreach ($requested_product->sound_notes as $key_sound_note => $sound_note_id) {
                    $sound_note_exists = $this->CI->SaleProductNotes_M->get([
                        'id' => $sound_note_id,
                        'sale_product_id' => null,
                        'type' => 2,
                    ]);
                    if ($sound_note_exists) {
                        $requested_product->sound_notes[$key_sound_note] = $sound_note_id;
                    } else {
                        $this->error_message = 'Sesli not geçersiz.';
                        return false;
                    }
                }

                $inserts[] = $requested_product;
            }
        }

        // Delete
        foreach ($sale_products as $sale_product) {

            // if the product has at least one interim payment then the product wont delete.
            $interim_payment_counts = 0;

            $delete_sale_product = null;
            foreach ($this->products as $requested_product) {
                if ($requested_product->sale_product_id == $sale_product->id) {
                    $delete_sale_product = true;
                    break;
                }
            }

            if (!$delete_sale_product) {
                $quantity = isset($requested_product->quantity) ? $requested_product->quantity : 0;

                if ($sale_product->completed > $quantity && $this->force_product_decrement == false) {
                    $this->error_message = "{$sale_product->completed} adet {$sale_product->title} hazır olduğu için iptal edilemez.";
                    return false;
                }

                $interim_payment_counts = $this->countInterimPayments($sale_product);
                if ($interim_payment_counts) {
                    $this->error_message = 'Ara ödemesi yapılmış ürün iptal edilemez.';
                    return false;
                }

                $deletes[] = $sale_product;
            }
        }

        $results = compact('inserts', 'updates', 'deletes');

        $this->products = $results;
        return true;
    }

    private function handleRequestedReturnProducts(Array $sale_products = [])
    {
        $inserts = 
        $updates = 
        $deletes = [];

        // Liken to sale
        foreach ($this->products as $key => $requested_product) {

            $sale_product = null;
            foreach ($sale_products as $_sale_product) {
                if ($_sale_product->id == $requested_product->sale_product_id) {

                    // Liken to sale
                    $_sale_product->sale_product_id = $_sale_product->id;
                    $_sale_product->id = $_sale_product->product_id;

                    $sale_product = $_sale_product;
                    break;
                }
            }

            if (!$sale_product) {
                $this->error_message = 'Ürün geçersiz.';
                return false;
            }
            if (($sale_product->quantity - $sale_product->return_quantity) < $requested_product->quantity) {
                $this->error_message = 'İade edilen adet satın alınan adetten fazla olamaz.';
                return false;
            }

            if ($sale_product->type == 'product') {
                foreach ($sale_product->stocks as $key_stock => $stock) {
                    // Liken to sale
                    $stock->id = $stock->stock_id;
                    $stock->product_stock_quantity = $stock->quantity;
                    $stock->product_stock_optional = $stock->optional;
                    if (!$stock->in_use) {
                        $stock->unwanted = true;
                    }

                    $sale_product->stocks[$key_stock] = $stock;
                }
            } elseif ($sale_product->type == 'package') {
                
                foreach ($sale_product->products as $key_package_product => $package_product) {
                    // Liken to sale
                    $package_product->sale_product_id = $package_product->id;
                    $package_product->id = $package_product->product_id;

                    foreach ($package_product->stocks as $key_stock => $stock) {
                        // Liken to sale
                        $stock->id = $stock->stock_id;
                        $stock->product_stock_quantity = $stock->quantity;
                        $stock->product_stock_optional = $stock->optional;
                        if (!$stock->in_use) {
                            $stock->unwanted = true;
                        }

                        $package_product->stocks[$key_stock] = $stock;
                    }

                    $sale_product->products[$key_package_product] = $package_product;
                }
            }

            // Liken to sale
            $tmp_additional_products = [];
            foreach ($sale_product->additional_products as $additional_product) {
                // Liken to sale
                $additional_product->sale_product_id = $additional_product->id;
                $additional_product->id = $additional_product->product_id;

                $requested_product->additional_products[] = $additional_product->product_id;

                foreach ($additional_product->stocks as $key_additional_product_stock => $additional_product_stock) {
                    // Liken to sale
                    $additional_product_stock->id = $additional_product_stock->stock_id;
                    $additional_product_stock->product_stock_quantity = $additional_product_stock->quantity;
                    $additional_product_stock->product_stock_optional = $additional_product_stock->optional;

                    $additional_product->stocks[$key_additional_product_stock] = $additional_product_stock;
                }

                $tmp_additional_products[$additional_product->product_id] = $additional_product;
            }
            $sale_product->additional_products = $tmp_additional_products;

            $requested_product->product = $sale_product;
            $inserts[] = $requested_product;
        }

        $results = compact('inserts', 'updates', 'deletes');

        $this->products = $results;
        return true;
    }

    private function calculateRequestedProducts()
    {
        $total =
        $subtotal =
        $taxtotal =
        $gifttotal =
        $giftsubtotal = 0;

        // Prepare interim payment array for $this->calculateInterimPayments
        $this->fillInterimPaymentArray();

        foreach ($this->products['inserts'] as $product) {
            $tax = $product->product->price * $product->product->tax / 100;
            $tax = round($tax, 2);
            $temp_price = round($product->product->price, 2);
            $tax *= $product->quantity;
            $price = $temp_price * $product->quantity;

            $subtotal += $price;
            $taxtotal += $tax;
            $total += $price + $tax;

            foreach ($product->additional_products as $additional_product_id) {
                $additional_product = $product->product->additional_products[$additional_product_id];

                $tax = $additional_product->price * $additional_product->tax / 100;
                $tax = round($tax, 2);
                $temp_add_price = round($additional_product->price,2);
                $tax *= $product->quantity;
                $price = $temp_add_price * $product->quantity;

                $subtotal += $price;
                $taxtotal += $tax;
                $total += $price + $tax;
            }
        }

        foreach ($this->products['updates'] as $product) {
            $tax = $product->sale_product->price * $product->sale_product->tax / 100;
            $tax = round($tax, 2);
            $temp_price = round($product->sale_product->price, 2);
            $tax *= $product->quantity;
            $price = $temp_price * $product->quantity;

            if ($product->sale_product->is_gift) {
                $giftsubtotal += $price;
                $gifttotal += $price + $tax;
            } else {
                $subtotal += $price;
                $taxtotal += $tax;
                $total += $price + $tax;
            }
            

            // Interim Payments
            $this->calculateInterimPayments($product->sale_product);

            foreach ($product->update_additional_products as $additional_product) {
                $tax = $additional_product->price * $additional_product->tax / 100;
                $tax = round($tax, 3);
                $temp_add_price = round($additional_product->price,2);
                $tax *= $product->quantity;
                $price = $temp_add_price * $product->quantity;

                if ($product->sale_product->is_gift) {
                    $giftsubtotal += $price;
                    $gifttotal += $price + $tax;
                } else {
                    $subtotal += $price;
                    $taxtotal += $tax;
                    $total += $price + $tax;
                }

                // Interim Payments
                $this->calculateInterimPayments($additional_product);
            }

            foreach ($product->insert_additional_products as $additional_product) {
                $tax = $additional_product->price * $additional_product->tax / 100;
                $tax = round($tax, 3);
                $temp_add_price = round($additional_product->price,2);
                $tax *= $product->quantity;
                $price = $temp_add_price * $product->quantity;

                $subtotal += $price;
                $taxtotal += $tax;
                $total += $price + $tax;
            }
        }

        $result = (Object) [
            'total' => round($total, 2),
            'subtotal' => round($subtotal, 2),
            'taxtotal' => round($taxtotal, 3),
            'gifttotal' => round($gifttotal, 2),
            'giftsubtotal' => round($giftsubtotal, 2),
            'interim_payments' => (Object) $this->interim_payments
        ];

        return $result;
    }

    private function calculateSaleDiscount($discount_amount, $discount_type, $products_total)
    {
        $calculation = (Object) [
            'discount_type' => $discount_type,
            'discount_amount' => $discount_amount,
            'discount_total' => 0,
        ];

        if ($discount_type == 'amount') {
            $calculation->discount_total = $discount_amount;
        } elseif ($discount_type == 'percent') {
            $calculation->discount_total = round($products_total * $discount_amount / 100, 2);
        }

        return $calculation;
    }

    private function calculatePayments()
    {
        $inputs = [];
        $payments = [
            'total' => 0
            // ... other payment types
        ];

        $this->customer_cari_payments = [];

        foreach ($this->getPaymentTypes() as $payment_type) {

            $inputs[$payment_type->alias] = [];
            $payments[$payment_type->alias] = 0;

            if (isset($this->payments[$payment_type->alias])) {
                $inputs[$payment_type->alias] = $this->payments[$payment_type->alias];
            }
            
            foreach ($inputs[$payment_type->alias] as $key => $value) {
                $clean_value = doubleval(str_replace(',', '', $value));
                $payments[$payment_type->alias] += $clean_value;

                if ($payment_type->alias == "cari") {
                    $customer_id = (int) $key;
                    if ($customer_id) {
                        $this->customer_cari_payments[$customer_id] = $clean_value;
                    }
                }
            }

            $payments['total'] += $payments[$payment_type->alias];
        }

        return (Object) $payments;
    }

    private function processProducts($sale_id, $datetime, $auto_delivered = false)
    {
        $this->_insertProducts($this->products, $sale_id, $datetime, $auto_delivered);
        $this->_updateProducts($this->products, $sale_id, $datetime, $auto_delivered);
        $this->_deleteProducts($this->products, $datetime);

        return $this->inserted_product_ids;
    }

    private function processProductStocks()
    {
        foreach ($this->used_stocks as $stock_id => $value) {
            $this->CI->db->where('id', $stock_id)->set('quantity', "quantity - {$value}", false)->update('stocks');
        }
    }

    private function processReturnedProducts()
    {
        foreach ($this->products['inserts'] as $product) {

            $this->CI->SaleProducts_M->update([
                'return_quantity' => $product->product->return_quantity + $product->quantity
            ], ['id' => $product->product->sale_product_id]);

            foreach ($product->product->additional_products as $additional_product) {

                $this->CI->SaleProducts_M->update([
                    'return_quantity' => $additional_product->return_quantity + $product->quantity
                ], ['id' => $additional_product->sale_product_id]);
            }
        }
    }

    private function _insertProducts($products, $sale_id, $datetime, $auto_delivered)
    {
        // Insert Products
        foreach ($products['inserts'] as $key => $product) {
            $this->CI->db->insert('sale_has_products', [
                'user_id' => $this->user_id,
                'sale_id' => $sale_id,
                'product_id' => $product->product->id,
                'production_id' => $product->product->production_id,
                'title' => $product->product->title,
                'price' => $product->product->price,
                'tax' => $product->product->tax,
                'type' => $product->product->type,
                'quantity' => $product->quantity,
                'bonus' => $product->product->bonus,
                'bonus_type' => $product->product->bonus_type,
                'delivered' => $auto_delivered ? $product->quantity : 0,
               /*'completed' => 0,
                'seen' => 0,
                'paid_cash' => 0,
                'paid_credit' => 0,
                'paid_vaucher' => 0,
                'paid_ticket' => 0,
                'paid_sodexo' => 0,*/
                'created' => $datetime,
                'active' => 1,
            ]);
            $new_sale_product_id = $this->CI->db->insert_id();

            $this->inserted_product_ids[] = [
                'index' => $product->index,
                'sale_product_id' => $new_sale_product_id,
            ];

            if ($product->product->type == 'product') {

                foreach ($product->product->stocks as $stock) {
                    $in_use = isset($stock->unwanted) ? 0 : 1;
                    $this->CI->db->insert('sale_products_stocks', [
                        'sale_product_id' => $new_sale_product_id,
                        'stock_id' => $stock->id,
                        'quantity' => $stock->product_stock_quantity,
                        'optional' => $stock->product_stock_optional,
                        'in_use' => $in_use
                    ]);

                    $this->addUsedStocks((object)[
                        'id' => $stock->id,
                        'quantity' => $stock->product_stock_quantity,
                        'in_use' => $in_use,
                        'product_quantity' => $product->quantity
                    ]);
                }

            } elseif ($product->product->type == 'package') {

                foreach ($product->product->products as $package_product) {
                    
                    $this->CI->db->insert('package_has_products', [
                        'package_id' => $new_sale_product_id,
                        'product_id' => $package_product->id,
                        'production_id' => $package_product->production_id,
                        'title' => $package_product->title,
                        'price' => $package_product->price,
                        'tax' => $package_product->tax,
                        'content' => $package_product->content,
                        'quantity' => $product->quantity,
                        'created' => $datetime,
                        'active' => 1,
                    ]);
                    $new_package_product_id = $this->CI->db->insert_id();

                    foreach ($package_product->stocks as $stock) {
                        $this->CI->db->insert('sale_packages_stocks', [
                            'sale_package_id' => $new_package_product_id,
                            'stock_id' => $stock->id,
                            'quantity' => $stock->product_stock_quantity,
                            'optional' => $stock->product_stock_optional,
                            'in_use' => 1,
                        ]);
                        $this->addUsedStocks((object)[
                            'id' => $stock->id,
                            'quantity' => $stock->product_stock_quantity,
                            'in_use' => 1,
                            'product_quantity' => $product->quantity
                        ]);
                    }
                }
            }

            foreach ($product->additional_products as $additional_product_id) {
                foreach ($product->product->additional_products as $additional_product) {
                    if ($additional_product->id == $additional_product_id) {
                        $this->CI->db->insert('sale_has_products', [
                            'user_id' => $this->user_id,
                            'sale_id' => $sale_id,
                            'product_id' => $additional_product->id,
                            'production_id' => $additional_product->production_id,
                            'additional_sale_product_id' => $new_sale_product_id,
                            'title' => $additional_product->title,
                            'price' => $additional_product->price,
                            'tax' => $additional_product->tax,
                            'type' => $additional_product->type,
                            'quantity' => $product->quantity,
                            'bonus' => $additional_product->bonus,
                            'bonus_type' => $additional_product->bonus_type,
                            'delivered' => $auto_delivered ? $product->quantity : 0,
                           /*'completed' => 0,
                            'seen' => 0,
                            'paid_cash' => 0,
                            'paid_credit' => 0,
                            'paid_vaucher' => 0,
                            'paid_ticket' => 0,
                            'paid_sodexo' => 0,*/
                            'created' => $datetime,
                            'active' => 1,
                        ]);
                        $new_sale_additional_product_id = $this->CI->db->insert_id();

                        foreach ($additional_product->stocks as $additional_product_stock) {
                            $this->CI->db->insert('sale_products_stocks', [
                                'sale_product_id' => $new_sale_additional_product_id,
                                'stock_id' => $additional_product_stock->id,
                                'quantity' => $additional_product_stock->product_stock_quantity,
                                'optional' => $additional_product_stock->product_stock_optional,
                                'in_use' => 1,
                            ]);

                            $this->addUsedStocks((object)[
                                'id' => $additional_product_stock->id,
                                'quantity' => $additional_product_stock->product_stock_quantity,
                                'in_use' => 1,
                                'product_quantity' => $product->quantity
                            ]);
                        }
                    }
                }
            }

            // Text Notes
            foreach ($product->text_notes as $text_note) {
                $this->CI->SaleProductNotes_M->insert([
                    'sale_product_id' => $new_sale_product_id,
                    'note' => $text_note,
                    'viewed' => 0,
                    'type' => 1,
                ]);
            }
            // Sound Notes
            foreach ($product->sound_notes as $sound_note_id) {
                $this->CI->SaleProductNotes_M->update([
                    'sale_product_id' => $new_sale_product_id,
                ], ['id' => $sound_note_id]);
            }
        }
        // END - Insert Products
    }

    private function _updateProducts($products, $sale_id, $datetime, $auto_delivered)
    {
        // Update Products
        foreach ($products['updates'] as $key => $product) {
            $this->CI->db
            ->where('id', $product->sale_product->id)
            ->update('sale_has_products', [
                'quantity' => $product->quantity,
                'delivered' => $auto_delivered ? $product->quantity : $product->sale_product->delivered,
                'completed' => $product->sale_product->completed,
                /*'seen' => 0,
                'paid_cash' => 0,
                'paid_credit' => 0,
                'paid_vaucher' => 0,
                'paid_ticket' => 0,
                'paid_sodexo' => 0,*/
                'modified' => $datetime,
            ]);

            if ($product->sale_product->type == 'product') {

                $this->CI->db->where('sale_product_id', $product->sale_product->id)->delete('sale_products_stocks');

                foreach ($product->sale_product->stocks as $stock) {
                    $in_use = isset($stock->unwanted) ? 0 : 1;
                    $this->CI->db
                    ->insert('sale_products_stocks', [
                        'sale_product_id' => $stock->sale_product_id,
                        'stock_id' => $stock->stock_id,
                        'quantity' => $stock->quantity,
                        'optional' => $stock->optional,
                        'in_use' =>  $in_use
                    ]);

                    $this->addUsedStocks((object)[
                        'id' => $stock->stock_id,
                        'quantity' => $stock->quantity,
                        'in_use' =>  $in_use,
                        'product_quantity' => $product->quantity
                    ]);
                }

            } elseif ($product->sale_product->type == 'package') {
                foreach ($product->sale_product->products as $package_product) {
                    
                    $this->CI->db
                    ->where('id', $package_product->id)
                    ->update('package_has_products', [
                        'quantity' => $product->quantity,
                        'completed' => $package_product->completed,
                        'modified' => $datetime,
                    ]);

                    $this->CI->db->where('sale_package_id', $package_product->id)->delete('sale_packages_stocks');

                    foreach ($package_product->stocks as $stock) {
                        $this->CI->db->insert('sale_packages_stocks', [
                            'sale_package_id' => $package_product->id,
                            'stock_id' => $stock->stock_id,
                            'quantity' => $stock->quantity,
                            'optional' => $stock->optional,
                            'in_use' => 1,
                        ]);
                    }
                }
            }

            // Insert the updated prodcut's additional products and stocks
            foreach ($product->insert_additional_products as $additional_product) {
                $this->CI->db->insert('sale_has_products', [
                    'user_id' => $this->user_id,
                    'sale_id' => $sale_id,
                    'product_id' => $additional_product->id,
                    'production_id' => $additional_product->production_id,
                    'additional_sale_product_id' => $product->sale_product->id,
                    'title' => $additional_product->title,
                    'price' => $additional_product->price,
                    'tax' => $additional_product->tax,
                    'type' => $additional_product->type,
                    'quantity' => $product->quantity,
                    'bonus' => $additional_product->bonus,
                    'bonus_type' => $additional_product->bonus_type,
                    'delivered' => $auto_delivered ? $product->quantity : $product->sale_product->delivered,
                    'completed' => $product->sale_product->completed,
                    /*'seen' => 0,
                    'paid_cash' => 0,
                    'paid_credit' => 0,
                    'paid_vaucher' => 0,
                    'paid_ticket' => 0,
                    'paid_sodexo' => 0,*/
                    'created' => $datetime,
                    'active' => 1,
                ]);
                $new_sale_additional_product_id = $this->CI->db->insert_id();

                foreach ($additional_product->stocks as $additional_product_stock) {
                    $this->CI->db->insert('sale_products_stocks', [
                        'sale_product_id' => $new_sale_additional_product_id,
                        'stock_id' => $additional_product_stock->id,
                        'quantity' => $additional_product_stock->product_stock_quantity,
                        'optional' => $additional_product_stock->product_stock_optional,
                        'in_use' => 1,
                    ]);

                    $this->addUsedStocks((object)[
                        'id' => $additional_product_stock->id,
                        'quantity' => $additional_product_stock->product_stock_quantity,
                        'in_use' => 1,
                        'product_quantity' => $product->quantity
                    ]);
                }
            }
            // Update the updated product's addtional products and stocks
            foreach ($product->update_additional_products as $additional_product) {
                $this->CI->db
                ->where('id', $additional_product->id)
                ->update('sale_has_products', [
                    'quantity' => $product->quantity,
                    'delivered' => $auto_delivered ? $product->quantity : $product->sale_product->delivered,
                    'modified' => $datetime,
                ]);

                $this->CI->db->where('sale_product_id', $additional_product->id)->delete('sale_products_stocks');

                foreach ($additional_product->stocks as $stock) {
                    $this->CI->db
                    ->insert('sale_products_stocks', [
                        'sale_product_id' => $stock->sale_product_id,
                        'stock_id' => $stock->stock_id,
                        'quantity' => $stock->quantity,
                        'optional' => $stock->optional,
                        'in_use' => 1,
                    ]);

                     $this->addUsedStocks((object)[
                        'id' => $stock->stock_id,
                        'quantity' => $stock->quantity,
                        'in_use' =>  1,
                        'product_quantity' => $product->quantity
                    ]);
                }
            }
            // Delete the updated product's addtional products and stocks
            foreach ($product->delete_additional_products as $additional_product) {
                $this->CI->db
                ->where('id', $additional_product->id)
                ->update('sale_has_products', [
                    'active' => 3,
                    'modified' => $datetime,
                ]);

                $this->CI->db->where('sale_product_id', $additional_product->id)->delete('sale_products_stocks');
            }

            // Text Notes
            foreach ($product->text_notes as $text_note) {
                $this->CI->SaleProductNotes_M->insert([
                    'sale_product_id' => $product->sale_product->id,
                    'note' => $text_note,
                    'viewed' => 0,
                    'type' => 1,
                ]);
            }
            // Sound Notes
            foreach ($product->sound_notes as $sound_note_id) {
                $this->CI->SaleProductNotes_M->where(['id' => $sound_note_id])->update([
                    'sale_product_id' => $product->sale_product->id,
                ]);
            }
        }
        // END - Update Products
    }

    private function _deleteProducts($products, $datetime)
    {
        // Delete Products
        foreach ($products['deletes'] as $key => $product) {
            $this->CI->db
            ->where('id', $product->id)
            ->update('sale_has_products', [
                'active' => 3,
                'modified' => $datetime,
            ]);

            if ($product->type == 'product') {

                $this->CI->db->where('sale_product_id', $product->id)->delete('sale_products_stocks');

            } elseif ($product->type == 'package') {
                foreach ($product->products as $package_product) {
                    
                    $this->CI->db
                    ->where('id', $package_product->id)
                    ->update('package_has_products', [
                        'active' => 3,
                        'modified' => $datetime,
                    ]);

                    $this->CI->db->where('sale_package_id', $package_product->id)->delete('sale_packages_stocks');
                }

            }

            // Delete Additional products
            foreach ($product->additional_products as $additional_product) {
                $this->CI->db
                ->where('id', $additional_product->id)
                ->update('sale_has_products', [
                    'active' => 3,
                    'modified' => $datetime,
                ]);

                $this->CI->db->where('sale_product_id', $additional_product->id)->delete('sale_products_stocks');
            }
        }
        // END - Delete Products
    }

    private function calculateInterimPayments($product)
    {
        foreach ($this->getPaymentTypes() as $payment_type) {

            if (empty($this->interim_payments[$payment_type->alias])) {
                $this->interim_payments[$payment_type->alias] = 0;
            }

            $price = $product->price + $product->price * $product->tax / 100;
            $price = round($price, 2);
            $total = $price * $product->{"paid_{$payment_type->alias}"};

            $this->interim_payments[$payment_type->alias] += $total;
            $this->interim_payments['total'] += $total;
        }
    }

    private function fillInterimPaymentArray()
    {
        foreach ($this->getPaymentTypes() as $payment_type) {
            $this->interim_payments[$payment_type->alias] = 0;
        }
    }

    private function countInterimPayments($product)
    {
        $count = 0;
        foreach ($this->getPaymentTypes() as $payment_type) {
            $count += $product->{"paid_{$payment_type->alias}"};
        }
        return $count;
    }

    private function getPaymentTypeId($payments, $interim_payments)
    {
        $payment_type_id = null;

        foreach ($this->getPaymentTypes() as $payment_type) {
            if ($payments->{$payment_type->alias} || $interim_payments->{$payment_type->alias}) {

                // if second payment type exists then this is multiple payment and return null.
                if ($payment_type_id) {
                    return null;
                }

                $payment_type_id = $payment_type->id;
            }
        }

        return $payment_type_id;
    }

    private function getPaymentTypeIdByAlias($payment_type)
    {
        $payment_type_id = null;

        foreach ($this->getPaymentTypes() as $_payment_type) {
            if ($_payment_type->alias == $payment_type) {
                $payment_type_id = $_payment_type->id;
                break;
            }
        }

        return $payment_type_id;
    }

    private function calculateGiftProduct($quantity, $completed, $delivered, $gift) {

        $main_product = (object) [
            'quantity' => 0,
            'completed' => 0,
            'delivered' => 0
        ];

        $gift_product = (object) [
            'quantity' => 0,
            'completed' => 0,
            'delivered' => 0
        ];

        if (($quantity - $gift) > 0) {

            if ($completed > $gift) {

                $main_product->quantity = $quantity - $gift;
                $main_product->completed = $completed - $gift;

                $gift_product->quantity = $gift;
                $gift_product->completed = $gift;
            
            } else if ($completed == $gift) {

                $main_product->quantity = $quantity - $gift;
                $main_product->completed = $completed;

                $gift_product->quantity = $gift;
                $gift_product->completed = 0;

            } else {

                $main_product->quantity = $quantity - $gift;
                $main_product->completed = $completed;

                $gift_product->quantity = $gift;
                $gift_product->completed = 0;

            }

            if ($delivered > $gift) {
                $main_product->delivered = $delivered - $gift;

                $gift_product->delivered = $gift;
            
            } else if ($delivered == $gift) {

                $main_product->delivered = $delivered;

                $gift_product->delivered = 0;

            } else {

                $main_product->delivered = $delivered;

                $gift_product->delivered = 0;

            }



         }  else if (($quantity - $gift) == 0) {

            $main_product->quantity = 0;
            $main_product->completed = 0;
            $main_product->delivered = 0;

            $gift_product->quantity = $quantity;
            $gift_product->completed = $completed;
            $gift_product->delivered = $delivered;

        }

        return  (object) [
            'main_product' => $main_product,
            'gift_product' => $gift_product
        ];

    }

    private function addUsedStocks($stock)
    {
        if (!$stock->in_use) {
            return;
        }

        if (!isset($this->used_stocks[$stock->id])) {
            $this->used_stocks[$stock->id] = 0;
        }

        $this->used_stocks[$stock->id] += $stock->quantity * $stock->product_quantity;

    }

    public function getServicePoint($location_id, $service_type)
    {

        $service_type = $this->CI->Points_M->findServiceTypeByType($service_type);

        $point = $this->CI->Points_M->where('location_id', $this->CI->user->locations)->get([
            'location_id' => $location_id,
            'service_type' => $service_type->id,
            'sale_id' => null,
            'active' => 1,
        ]);

        if (!$point) {
            $point = $this->createServicePoint($service_type, $location_id);
            return $point;
        }

        return $point;

    }

    public function createServicePoint($service_type, $location_id)
    {
        $floor = $this->CI->Floors_M->fields('id')->findOrFail([
            'location_id' => $location_id
        ], 'Kat oluşturun.');

        $zone = $this->CI->Zones_M->fields('id')->findOrFail([
            'location_id' => $location_id
        ], 'Bölge oluşturun.');

        $id = $this->CI->Points_M->insert([
            'title' => $service_type->title,
            'location_id' => $location_id,
            'floor_id' => $floor->id,
            'zone_id' => $zone->id,
            'service_type' => $service_type->id,
            'active' => 1,
        ]);

        return $this->CI->Points_M->get($id);
    }

}
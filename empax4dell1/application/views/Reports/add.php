    
<div class="content">

    <div class="page-header full-content">
        <div class="row">
            <div class="col-sm-6">
                <h1>New Sale <small></small></h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb">
                    <li><a href="<?= base_url(); ?>" ><i class="ion-home"></i></a></li>
                    <li><a href="<?= base_url('salesj'); ?>">Sales</a></li>
                    <li><a href="#" class="active">New Sale</a></li>
                </ol>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="panel">

                <div class="panel-heading">
                    <div class="panel-title">
                        <h4>Sale Properties</h4>
                    </div>
                </div>

                <div class="panel-body">
                    <form action="#" class="form-horizontal">
                        <div class="form-content">

                            <!-- <div class="form-group">
                                <label class="control-label col-md-3">Category</label>
                                <div class="col-md-9">
                                    <select name="type" class="chosen-select">
                                        <option value="">Choose Category</option>
                                        <?php //foreach ($sale_types as $sale_type): ?>
                                            <option value="<?//= $sale_type['id']; ?>"><?//= $sale_type['title'] ;?></option>
                                        <?php //endforeach; ?>
                                    </select>
                                </div>
                            </div> -->

                            <div class="form-group">
                                <label class="control-label col-md-3">Ticket Number</label>
                                <div class="col-md-9">
                                    <div class="inputer">
                                        <div class="input-wrapper">
                                            <input type="text" name="ticket" class="form-control">
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3">Location</label>
                                <div class="col-md-9">
                                    <select name="location" class="chosen-select">
                                        <option value="">Choose Location</option>
                                        <?php foreach ($this->user->locations_array as $location): ?>
                                            <option value="<?= $location['id']; ?>"><?= $location['title'] ;?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3">Product</label>
                                <div class="col-md-9">
                                    <select multiple="multiple" name="product" class="chosen-select product" >
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3"></label>
                                <div class="col-md-9 alert alert-primary-transparent">
                                    <table class="table table-hover">
                                        <thead>
                                            <tr>
                                                <th><sup>Product</sup></th>
                                                <th><sup>Price</sup></th>
                                                <th><sup>Quantity</sup></th>
                                            </tr>
                                        </thead>
                                        <tbody id="product-list">
                                        </tbody>
                                    </table>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3">Amount</label>
                                <div class="col-md-9">
                                    <div class="input-group">
                                        <span class="input-group-addon">$</span>
                                        <div class="inputer">
                                            <div class="input-wrapper">
                                                <input type="text" class="form-control format-money" maxlength="25" name="amount" placeholder="Amount" readonly="readonly">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3">Discount</label>
                                <div class="col-md-9">
                                    <div class="col-md-1">
                                        <select name="discount_type" class="form-control">
                                            <option value="amount">$</option>
                                            <option value="percent">%</option>
                                        </select>
                                    </div>
                                    <div class="col-md-6">
                                        <input type="text" class="form-control format-money" maxlength="25" name="discount_amount" placeholder="Amount">
                                    </div>                                    
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3">Payment Date</label>
                                <div class="col-md-9">
                                    <div class="inputer">
                                        <div class="input-wrapper">
                                            <input type="text" name="payment_date" class="form-control datepicker-basic" value="<?php ECHO date('Y-m-d'); ?>"/>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3">Payment Method</label>
                                <div class="col-md-9">
                                    <select name="payment_method" class="chosen-select">
                                        <?php foreach ($payment_methods as $payment_method): ?>
                                            <option value="<?= $payment_method['id']; ?>"><?= $payment_method['title'] ;?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                            </div>

                            <!-- <div class="form-group">
                                <label class="control-label col-md-3">Name</label>
                                <div class="col-md-9">
                                    <div class="inputer">
                                        <div class="input-wrapper">
                                            <input type="text" name="title" class="form-control">
                                        </div>
                                    </div>
                                </div>
                            </div> -->

                            <div class="form-group">
                                <label class="control-label col-md-3">Description</label>
                                <div class="col-md-9">
                                    <div class="inputer">
                                        <div class="input-wrapper">
                                            <textarea name="desc" class="form-control js-auto-size valid"></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <!-- <div class="form-group">
                                <label class="control-label col-md-3">Attachments</label>
                                <div class="col-md-9 note note-warning note-left-striped eUploadAttachments">
                                    <div class="col-xs-12 col-md-12 well well-sm attachments-dropzone">
                                        <center><small><i>Drag/Drop File or Click Here!</i></small></center>
                                    </div>
                                </div>
                            </div> -->

                            <div class="form-buttons-fixed">
                                <a type="button" href="<?= base_url('salesj'); ?>" class="btn btn-grey">Cancel</a>
                                <div class="btn-group pull-right">
                                    <button type="submit" class="btn btn-success" name="dosubmit" data-url="<?= base_url('salesj/add');?>" >Save</button>
                                    <button type="submit" class="btn btn-blue" name="dosubmit" data-url="<?= base_url('salesj/add');?>" data-return="<?= base_url('salesj'); ?>" >Save & Exit</button>
                                </div>
                            </div>

                        </div>
                    </form>
                </div>

            </div>
        </div>
    </div>

</div>

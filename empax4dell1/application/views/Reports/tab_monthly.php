<?php 
    
    $date_start = dateConvert2($date_start, 'client', 'Y-m-d H:i:s', $date_format);

    $handleDailyUrl = function ($day) use ($location_id) {
        return http_build_query([
            'tab' => 'daily',
            'location_id' => $location_id,
            'date_start' => dateConvert2($day->day_start, 'client', 'Y-m-d H:i:s', dateFormat()),
            'date_end' => dateConvert2($day->day_end, 'client', 'Y-m-d H:i:s', dateFormat()),
        ]);
    };

    $short_days = array(
        'Mon' => 'Pzt',
        'Tue' => 'Sal',
        'Wed' => 'Çar',
        'Thu' => 'Per',
        'Fri' => 'Cum',
        'Sat' => 'Cmt',
        'Sun' => 'Pzr' );

?>
    <div class="col-md-12">

        <div class="dropdown">
            <button class="btn btn-default btn-xl dropdown-toggle" type="button" id="dropdownFilter" data-toggle="dropdown">
                Filter <span class="caret"></span>    
            </button>
            <ul class="dropdown-menu" aria-labelledby="dropdownFilter" style="width: 100%;">

                <form action="" method="get" class="form-horizontal" role="form">
                    <div class="form-content">

                        <div class="col-xs-12 col-sm-12 col-md-6">
                            <div class="form-group">
                                <label class="control-label col-md-3"><?= __('Restoran') ?></label>
                                <div class="col-md-9">
                                    <select name="location_id" class="chosen-select">
                                        <?php foreach ($this->user->locations_array as $location): ?>
                                            <option value="<?= $location['id']; ?>" <?php selected($location_id, $location['id']); ?>>
                                                <?= $location['title'] ;?>
                                            </option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3"><?= __('Dönem') ?></label>
                                <div class="col-md-9">
                                    <div class="inputer">
                                        <div class="input-wrapper">
                                            <input type="text" name="date_start" class="form-control datetimepicker-basic" value="<?= $date_start ?>"/>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <div class="form-group">
                                <div class="col-sm-12 text-right">
                                    <button type="submit" class="btn btn-primary"><?= __('Uygula') ?></button>
                                    <input type="hidden" name="tab" value="monthly">
                                </div>
                            </div>
                        </div>

                    </div>
                </form>

            </ul>
        </div> 


        <div class="panel-body">
            <div class="table-responsive">
                <table class="table table-hover">
                    <thead>
                        <tr>
                            <th><?= __('Tarih') ?></th>
                            <?php foreach ($payment_types as $payment_type): ?>
                                <th><?= $payment_type->title ?></th>
                            <?php endforeach; ?>
                            <th><?= __('Açılış') ?></th>
                            <th><?= __('Toplam') ?></th>
                            <th><?= __('Genel Fark') ?></th>
                            <th><?= __('İşlem') ?></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($days as $day): ?>
                            <?php $D = dateConvert2($day->day_start, 'client', 'Y-m-d H:i:s', 'D');  ?>
                            <tr>
                                <td><?= dateConvert2($day->day_start, 'client', 'Y-m-d H:i:s', 'j ') .$short_days[$D] ?></td>
                                <?php foreach ($payment_types as $payment_type): ?>
                                    <td>
                                        <?= number_format($day->{"{$payment_type->alias}_exists"}, 2) ?>
                                        <p class="small">Pos: <?= number_format($day->{$payment_type->alias}, 2) ?></p>
                                    </td>
                                <?php endforeach; ?>
                                <td><?= number_format($day->cash_opening, 2) ?></td>
                                <td>
                                    <?= number_format($day->total_exists - $day->cash_opening, 2) ?>
                                    <p class="small">Pos: <?= number_format($day->total, 2) ?></p>
                                </td>
                                <td><?= number_format($day->over_short, 2) ?></td>
                                <td>
                                    <a class="btn btn-primary btn-xs show_detail" href='<?= base_url('reports?'.$handleDailyUrl($day)) ?>' target="_blank">
                                        <i class="fa fa-search"></i>
                                    </a>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>

    </div>
<div class="content">

    <div class="page-header full-content">
        <div class="row">
            <div class="col-sm-6">
                <h1><?= __('Stoklar') ?></h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb">
                    <li><a href="<?php ECHO base_url(); ?>" ><i class="ion-home"></i></a></li>
                    <li><a href="#" class="active"><?= __('Stoklar') ?></a></li>
                </ol>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="panel">

                <div class="panel-heading">
                    <div class="panel-title">
                        <?php ECHO dropdownMenu($this->user->locations_array, $current_location, 'stocks/alarm/', 'Restoranlar'); ?>
                    </div>

                    <div class="panel-title">
                        <h4><?= __('Tüm Stoklar') ?></h4>
                    </div>
                </div>

                <div class="panel-body">
                    <div class="overflow-table">
                        <table class="display datatables-basic">
                            <thead>
                                <tr>
                                    <th><?= __('İsim') ?></th>
                                    <th><?= __('Stok Durumu') ?></th>
                                    <th><?= __('Restoran') ?></th>
                                    <th><?= __('Oluşturuldu') ?></th>
                                    <th><?= __('Durum') ?></th>
                                    <th><?= __('İşlem') ?></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($stocks as $stock): ?>
                                    <tr style="color:red;">
                                        <td><?= $stock->title ?></td>
                                        <td><?= $stock->quantity ?> <b><?= @$this->units[$stock->unit] ?></b></td>
                                        <td><?= $stock->location ?> </td>
                                        <td><?= dateConvert($stock->created, 'client', dateFormat())?></td>
                                        <td><?= get_status($stock->active) ?></td>
                                        <td>
                                            <a class="btn btn-info btn-xs" href="<?= base_url('stockcontents/index/'.$stock->id) ?>"> <i class="fa fa-file-text-o"></i> </a>
                                            <a class="btn btn-primary btn-xs" href="<?= base_url('stocks/edit/'.$stock->id) ?>"> <i class="fa fa-pencil"></i> </a>
                                            <button class="btn btn-danger btn-xs" data-url="<?= base_url('stocks/delete/'.$stock->id) ?>" name="dodelete"> <i class="fa fa-trash-o"></i> </button>
                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                </div>

            </div>
        </div>
    </div>

</div>
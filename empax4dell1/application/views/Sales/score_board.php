<div class="content">

    <div class="page-header full-content">
        <div class="row">
            <div class="col-sm-6">
                <h1><?= __('Günlük Performans') ?></h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb">
                    <li><a href="<?php ECHO base_url(); ?>" ><i class="ion-home"></i></a></li>
                    <li><a href="#" class="active"><?= __('Günlük Performans') ?></a></li>
                </ol>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="panel">

                <div class="panel-heading">
                    <div class="panel-title">
                        <?php ECHO dropdownMenu($this->user->locations_array, $location_id, 'sales/scoreboard/', 'Restoranlar'); ?>
                    </div>
                </div>

                <div class="panel-body">
                    <table class="table table-hover" id="score-board" style="font-size: 3rem;">
                        <thead>
                            <tr>
                                <th class="col-md-9"><?= __('İsim') ?></th>
                                <th class="col-md-3 text-right"><?= __('Toplam Satış') ?></th>
                            </tr>
                        </thead>
                        <tbody class="text-white">
                        </tbody>
                    </table>
                </div>

            </div>
        </div>
    </div>

</div>

<div class="content">

    <div class="page-header full-content">
        <div class="row">
            <div class="col-sm-6">
                <h1><?= __('Satış Düzenle') ?> <small></small></h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb">
                    <li><a href="<?= base_url(); ?>" ><i class="ion-home"></i></a></li>
                    <li><a href="#" class="active"><?= __('İşlemler') ?></a></li>
                    <li><a href="<?= base_url('sales'); ?>"><?= __('Satışlar') ?></a></li>
                    <li><a href="#" class="active"><?= __('Satış Düzenle') ?></a></li>
                </ol>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="panel">

                <div class="panel-heading">
                    <div class="panel-title">
                        <h4><?= __('Satış Detayları') ?></h4>
                    </div>
                </div>

                <div class="panel-body">
                    <form action="#" class="form-horizontal" id="sale">
                        <div class="form-content">

                            <div class="form-group">
                                <label class="control-label col-md-3"><?= __('İşlem Tarihi') ?></label>
                                <div class="col-md-9">
                                    <div class="inputer">
                                        <div class="input-wrapper">
                                            <input type="text" name="payment_date" class="form-control datetimepicker-basic" value="<?= dateConvert(date('Y-m-d H:i'), 'client', dateFormat()) ?>">
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3"><?= __('Restoran') ?></label>
                                <div class="col-md-9">
                                    <select name="location_id" class="chosen-select">
                                        <option value=""><?= __('Restoran seç') ?></option>
                                        <?php foreach ($this->user->locations_array as $location): ?>
                                            <option value="<?= $location['id'] ?>" data-currency="<?= $location['currency'] ;?>"><?= $location['title'] ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3"><?= __('Kasa') ?></label>
                                <div class="col-md-9">
                                    <select name="pos_id" class="chosen-select">
                                        <option value=""><?= __('Kasa seç') ?></option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3"><?= __('Hesabı Alan Personel') ?></label>
                                <div class="col-md-9">
                                    <select name="completer_user_id" class="chosen-select">
                                        <option value=""><?= __('Personel Seç') ?></option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3"><?= __('Kat') ?></label>
                                <div class="col-md-9">
                                    <select name="floor_id" class="chosen-select">
                                        <option value=""><?= __('Kat seç') ?></option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3"><?= __('Bölge') ?></label>
                                <div class="col-md-9">
                                    <select name="zone_id" class="chosen-select">
                                        <option value=""><?= __('Bölge seç') ?></option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3"><?= __('Masa') ?></label>
                                <div class="col-md-9">
                                    <select name="point_id" class="chosen-select">
                                        <option value=""><?= __('Masa seç') ?></option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3"><?= __('Ürün Ekle') ?></label>
                                <div class="col-md-9">
                                    <select name="product_id" class="chosen-select">
                                        <option value=""><?= __('Ürün seç') ?></option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group" id="products">

                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3"><?= __('Ödemeler') ?></label>
                                <div class="col-md-9" id="payment_types">
                                </div>

                                <label class="control-label col-md-3"><?= __('Cari') ?></label>
                                <button id="addPaymentCari" type="button" class="btn btn-success"><i class="fa fa-plus"></i></button>
                            </div>

                            <div class="form-group">
                                <div class="col-md-9 col-md-offset-3" id="payment_cari">
                                    
                                </div>
                            </div>

                            <div class="form-group m-b-0">
                                <label class="control-label col-md-3"><?= __('İndirim') ?></label>
                                <div class="col-md-9">
                                    <div class="input-group m-b-0">
                                        <input type="number" name="discount_amount" class="form-control" min="0" step="0.01" placeholder="0.00">
                                        <span class="input-group-btn">
                                            <select name="discount_type" class="form-control-static btn">
                                                <option value="percent">%</option>
                                                <option value="amount">#</option>
                                            </select>
                                        </span>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group m-b-0">
                                <label class="control-label col-md-3"><?= __('Ödenen') ?></label>
                                <div class="col-md-9">
                                    <div class="inputer m-b-0">
                                        <div class="input-wrapper">
                                            <input type="text" value="0.00" class="form-control paid" readonly="readonly">
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group m-b-0">
                                <label class="control-label col-md-3"><?= __('Kalan') ?></label>
                                <div class="col-md-9">
                                    <div class="inputer m-b-0">
                                        <div class="input-wrapper">
                                            <input type="text" value="0.00" class="form-control rest" readonly="readonly">
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group m-b-0">
                                <label class="control-label col-md-3"><?= __('Para Üstü') ?></label>
                                <div class="col-md-9">
                                    <div class="inputer m-b-0">
                                        <div class="input-wrapper">
                                            <input type="text" value="0.00" class="form-control change" readonly="readonly">
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group m-b-0">
                                <label class="control-label col-md-3"><?= __('Ara Toplam') ?></label>
                                <div class="col-md-9">
                                    <div class="inputer m-b-0">
                                        <div class="input-wrapper">
                                            <input type="text" class="form-control subtotal" maxlength="25" placeholder="0.00" readonly="readonly">
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group m-b-0">
                                <label class="control-label col-md-3"><?= __('Toplam Vergi') ?></label>
                                <div class="col-md-9">
                                    <div class="inputer m-b-0">
                                        <div class="input-wrapper">
                                            <input type="text" class="form-control taxtotal" maxlength="25" placeholder="0.00" readonly="readonly">
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3"><b><?= __('TOPLAM') ?></b></label>
                                <div class="col-md-9">
                                    <div class="inputer">
                                        <div class="input-wrapper">
                                            <input type="text" class="form-control title total" maxlength="25" placeholder="0.00" readonly="readonly">
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-buttons-fixed">
                                <a type="button" href="<?= base_url('sales') ?>" class="btn btn-grey"><?= __('İptal') ?></a>
                                <div class="btn-group pull-right">
                                    <button type="submit" class="btn btn-success" name="dosubmit" data-url="<?= base_url('sales/edit/'.$sale->id) ?>"  data-success="location.reload();" ><?= __('Kaydet') ?></button>
                                    <button type="submit" class="btn btn-blue" name="dosubmit" data-url="<?= base_url('sales/edit/'.$sale->id) ?>" data-return="<?= base_url('sales') ?>" ><?= __('Kaydet & Çık') ?></button>
                                </div>
                            </div>

                        </div>
                    </form>
                </div>

            </div>
        </div>
    </div>

</div>

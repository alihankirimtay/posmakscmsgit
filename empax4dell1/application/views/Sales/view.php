<div class="content">
    <div class="page-header full-content">
        <div class="row">
            <div class="col-sm-6">
                <h1><?= __('Fatura') ?> <small></small></h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb">
                    <li><a href="<?= base_url(); ?>" ><i class="ion-home"></i></a></li>
                    <li><a href="<?= base_url('sales'); ?>"><?= __('Satışlar') ?></a></li>
                    <li><a href="#" class="active"><?= __('Fatura') ?></a></li>
                </ol>
            </div>
        </div>
    </div>
    <?php if($usbs): ?>
        <div class="row">

            <div class="col-md-12">
                <div class="panel red ">
                    <div class="panel-heading">
                        <div class="panel-title">
                            <h4>
                                USB PASSWORDS:
                                <?php foreach ($usbs as $usb): ?>
                                    <?= get_status($usb->active, [$usb->key, $usb->key]); ?>
                                <?php endforeach; ?>
                            </h4>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    <?php endif; ?>
    <!-- Invoice -->

    <div style="width: <?= $width ?>; height: 50px; position: relative; right: 0;left: 0; margin: auto;">
        <a class="btn btn-primary pull-right" onclick="Sales.savePDF()">Yazdır</a>
    </div>

    <div id="invoice_page" data-name="<?= $pdfName; ?>" style="width: <?= $width; ?>; height: <?= $height; ?>;background-color: white; position: relative; right: 0; left: 0; margin: auto; ">
        <?php foreach($contents as $c): ?>
            <?php if($c['type'] == 'div'): ?>

                <div class="inv_item" style="position: absolute; width: <?= $c['w']; ?>; height:<?= $c['h']; ?>; top:<?= $c['y']; ?>; left:<?= $c['x']; ?>">
                    <?= $c['content']; ?>
                    
                </div>
            <?php elseif($c['type'] == "table"): ?>

                <table data-default="<?= $c['h']; ?>" style="position: absolute; width: <?= $c['w']; ?>; top:<?= $c['y']; ?>; left:<?= $c['x']; ?>">

                    <?php if ($c['showFieldTitles']): ?>
                        <tr>
                            <?php foreach($c['fields'] as $f): ?>
                                <?php if($f->active): ?>
                                    <th><?= $f->title; ?></th>
                                <?php endif; ?>
                            <?php endforeach; ?>
                        </tr>
                    <?php endif; ?>


                    <?php foreach($c['products'] as $p): ?>

                        <tr>

                            <?php if($c['fields']->id->active == 1): ?>
                                <td><?= $p->id; ?></td>
                            <?php endif; ?>

                            <?php if($c['fields']->title->active): ?>

                                <td><?= $p->title; ?></td>
                            <?php endif; ?>

                            <?php if($c['fields']->quantity->active): ?>

                                <td><?= $p->quantity; ?></td>
                            <?php endif; ?>

                            <?php if($c['fields']->price->active): ?>

                                <td><?= $p->price; ?></td>
                            <?php endif; ?>

                            <?php if($c['fields']->tax->active): ?>

                                <td><?= $p->tax; ?></td>
                            <?php endif; ?>

                            <?php if($c['fields']->total->active): ?>

                                <td><?= number_format($p->price * $p->quantity, 2) ?></td>

                            <?php endif; ?>
                        </tr>

                        <?php foreach($p->additional_products as $a): ?>

                            <tr>
                                <?php if($c['fields']->id->active == 1): ?>
                                    <td><?= $a->id; ?></td>
                                <?php endif; ?>

                                <?php if($c['fields']->title->active): ?>

                                    <td><?= $a->title; ?></td>
                                <?php endif; ?>

                                <?php if($c['fields']->quantity->active): ?>

                                    <td><?= $a->quantity; ?></td>
                                <?php endif; ?>

                                <?php if($c['fields']->price->active): ?>

                                    <td><?= $a->price; ?></td>
                                <?php endif; ?>

                                <?php if($c['fields']->tax->active): ?>

                                    <td><?= $a->tax; ?></td>
                                <?php endif; ?>

                                <?php if($c['fields']->total->active): ?>

                                    <td><?= number_format($a->price * $a->quantity, 2) ?></td>

                                <?php endif; ?>
                            </tr>


                        <?php endforeach; ?>

                    <?php endforeach; ?>
                    
                </table>

            <?php endif; ?>
        <?php endforeach; ?>
    </div>

    <div id="editor"></div>
    
    <!-- Invoice End -->
</div>


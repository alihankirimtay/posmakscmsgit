<div class="content">

    <div class="page-header full-content">
        <div class="row">
            <div class="col-sm-6">
                <h1>Pax & Negs</h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb">
                    <li><a href="<?php ECHO base_url(); ?>" ><i class="ion-home"></i></a></li>
                    <li><a href="#" class="active">Pax & Negs</a></li>
                </ol>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="panel">

                <div class="panel-heading">
                    <div class="panel-title">
                        <?php ECHO dropdownMenu($this->user->locations_array, $current_location, 'paxnegs/index/', 'Locations'); ?>
                    </div>
                </div>

                <div class="panel-heading">
                    <div class="panel-title">
                        <h4>All Pax & Negs</h4>
                        <div class="btn-group pull-right">
                            <a href="<?php ECHO base_url('paxnegs/add/'.$current_location); ?>" class="btn btn-success btn-xs"><i class="fa fa-plus"></i> New Pax & Negs</a>
                        </div>
                    </div>
                </div>

                <div class="panel-body">
                    <div class="overflow-table">
                        <table class="display datatables-basic">
                            <thead>
                                <tr>
                                    <th class="col-md-3">Date</th>
                                    <th class="col-md-2">Pax</th>
                                    <th class="col-md-2">Negs</th>
                                    <th class="col-md-2">Missed</th>
                                    <th class="col-md-2">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($paxnegs as $row): ?>
                                    <tr>
                                        <td><?=dateConvert($row->date, 'client', dateFormat())?></td>
                                        <td><?php ECHO $row->pax; ?></td>
                                        <td><?php ECHO $row->negs; ?></td>
                                        <td><?php ECHO $row->pax - $row->negs; ?></td>
                                        <td>
                                            <a class="btn btn-primary btn-xs" href="<?php ECHO base_url('paxnegs/edit/'.$current_location.'/'.$row->id); ?>"> <i class="fa fa-pencil"></i> </a>
                                            <!-- <button class="btn btn-danger btn-xs" data-url="<?php ECHO base_url('paxnegs/delete/'.$current_location.'/'.$row->id); ?>" name="dodelete"> <i class="fa fa-trash-o"></i> </button> -->
                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                </div>

            </div>
        </div>
    </div>

</div>

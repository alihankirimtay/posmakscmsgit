<div class="content">

    <div class="page-header full-content">
        <div class="row">
            <div class="col-sm-6">
                <h1><?= __("Müşteriler"); ?></h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb">
                    <li><a href="<?php ECHO base_url(); ?>" ><i class="ion-home"></i></a></li>
                    <li><a href="#" class="active"><?= __("Müşteriler"); ?></a></li>
                </ol>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="panel">

                <div class="panel-heading">
                    <div class="panel-title">
                        <h4><?= __("Tüm Müşteriler"); ?></h4>
                        <div class="btn-group pull-right">
                            <a href="<?php ECHO base_url('customers/add/'); ?>" class="btn btn-success btn-xs"><i class="fa fa-plus"></i><?= __("Müşteri Oluştur"); ?></a>
                        </div>
                    </div>
                </div>

                <div class="panel-body">
                    <div class="overflow-table">
                        <table class="display datatables-basic">
                            <thead>
                                <tr>
                                    <th><?= __("Ad Soyad"); ?></th>
                                    <th><?= __("Telefon"); ?></th>
                                    <th><?= __("Oluşturuldu"); ?></th>
                                    <th><?= __("Durum"); ?></th>
                                    <th><?= __("İşlem"); ?></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($customers as $customer): ?>
                                    <tr>
                                        <td><?= $customer->name ?></td>
                                        <td><?= $customer->phone ?></td>
                                        <td><?= dateConvert($customer->created, 'client', dateFormat()) ?></td>
                                        <td><?= get_status($customer->active) ?></td>
                                        <td>
                                            <a class="btn btn-primary btn-xs" href="<?php ECHO base_url('customers/edit/'.$customer->id); ?>"> <i class="fa fa-pencil"></i> </a>
                                            <button class="btn btn-danger btn-xs" data-url="<?php ECHO base_url('customers/delete/'.$customer->id); ?>" name="dodelete"> <i class="fa fa-trash-o"></i> </button>
                                        </td>
                                    </tr>
                                <?php endforeach ?>
                            </tbody>
                        </table>
                    </div>
                </div>

            </div>
        </div>
    </div>

</div>

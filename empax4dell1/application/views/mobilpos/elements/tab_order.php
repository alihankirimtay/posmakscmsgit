<div class="container-fluid tab-body body-bg">

	<div class="row">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 bg-header">

			<div class="col-md-3 col-sm-3 col-xs-3 p-10 pl-0">      
            	<img id="backTable" class="icon-svg-size" src="<?= asset_url('left.svg'); ?>"></img>
			</div>

			<div class="col-md-6 col-sm-6 col-xs-6 nav-order-span p-10">
				<span id="spanTableNameOrder" class="home-table text-center" style="color:white;"></span>    
			</div>

			<div class="col-md-3 col-sm-3 col-xs-3 p-10 text-right">
            	<img id="backHomePage" class="icon-svg-size" src="<?= asset_url('home-01.svg'); ?>"></img>
			</div>

		</div>

		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 tbl-product">
			<table id="tblProduct" class="table table-striped">
				<thead>
					<tr>
						<th class="col-lg-4 col-md-5 col-sm-5 col-xs-5"><?= __("Ürün"); ?></th>
						<th class="col-lg-3 col-md-2 col-sm-2 col-xs-1"><?= __("Adet"); ?></th>
						<th class="col-lg-3 col-md-2 col-sm-2 col-xs-1"><?= __("Toplam"); ?></th>
						<th class="col-lg-2 col-md-3 col-sm-3 col-xs-5 text-center"></th>
					</tr>
				</thead>
				<tbody>
				</tbody>
			</table>
		</div>

		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 order-footer">
			<!-- <a id ="orderBtnDelete" class="btn btn-yellow btn-ripple fa fa-trash iconfa" aria-hidden="true" style = "display: none;"></a> -->
			<a href="#" id="add-discount" class="col-md-6 col-xs-6 col-lg-6 col-sm-6 btn btn-info btn-ripple btn-xxl" data-toggle="modal" data-target="#modal-discount_login"><?= __("İNDİRİM"); ?></a>
			<a id="interimPaymentBtn" class="col-sm-6 col-xs-6 col-lg-6 col-md-6 btn btn btn-orange btn-ripple btn-xxl interim-payment-button"><?= __("ARA ÖDEME"); ?></a>
			<a id="print-adisyon" class="col-sm-6 col-xs-6 col-lg-6 col-md-6 btn btn-purple btn-ripple btn-xxl"><?= __("ADİSYON"); ?></a>
			<a id="cancel-sale" class="col-sm-6 col-xs-6 col-lg-6 col-md-6 btn btn-red btn-ripple btn-xxl"><?= __("MASA İPTAL"); ?></a>
			<a id="checkBtn" class="col-md-12 col-xs-12 col-lg-12 btn btn-success btn-ripple btn-xxl"><?= __("HESAP AL"); ?></a>
			<!-- <span id="spanPrice" class="pull-right pricespan">TOPLAM : </span> -->
			<!-- <a id="pos-create-order" class="btn-order col-md-12 col-xs-12 col-lg-12 btn btn-deep-orange btn-ripple btn-xxl">SİPARİŞ</a> -->
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 bg-nav" style="height: 71px;">
				<div class ="col-md-3 col-sm-3 col-xs-6 col-lg-3" style="line-height: 1.2; padding: 10px">
					<span style="color: white;"><small><?= __("TOPLAM"); ?></small></span>
					<span class="order-price" id="spanPrice"></span>
				</div>

				<div class ="col-md-3 col-sm-3 col-xs-6 col-lg-3 pull-right order-div">
					<a id="pos-create-order" class="col-md-12 col-xs-12 col-lg-12 btn btn-blue btn-ripple btn-xl br-30"><?= __("SİPARİŞ"); ?></a>
				</div>
			</div>
		</div>

	</div> <!-- nav-row close -->


</div>
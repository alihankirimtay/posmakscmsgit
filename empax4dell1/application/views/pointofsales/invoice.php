<div class="content">
    <div class="page-header full-content">
        <div class="row">
            <div class="col-sm-6">
                <h1><?= __("Fatura"); ?> <small></small></h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb">
                    <li><a href="<?= base_url(); ?>" ><i class="ion-home"></i></a></li>
                    <li><a href="<?= base_url('sales'); ?>"><?= __("Satışlar"); ?></a></li>
                    <li><a href="#" class="active"><?= __("Fatura"); ?></a></li>
                </ol>
            </div>
        </div>
    </div>
    <?php if(isset($invoice['key'])): ?>

        <div class="row hidden">

            <div class="col-md-12">
                <div class="panel red ">
                    <div class="panel-heading">
                        <div class="panel-title"><h4>USB PASSWORD : <?= $invoice['key'] ?> <?= get_status($invoice['key_status']); ?></h4></div>
                    </div><!--.panel-heading-->
                </div><!--.panel-->
            </div><!--.col-->

        </div>

    <?php endif; ?>

    <div class="row">
        <div class="col-md-12">
            <div class="panel">
                <div class="panel-heading">
                    <div class="panel-title">
                        <h4><?= __("FATURA"); ?></h4>
                        <div class="panel-buttons">
                            <button type="button" style="padding:6px 44px;font-size:20px;margin-bottom:10px;" class="btn btn-large btn-primary print-trigger btn-ripple"><?= __("Yazdır"); ?></button>
                        </div><!--.panel-buttons-->
                    </div><!--.panel-title-->
                </div><!--.panel-heading-->
                <div class="panel-body">

                    <div class="invoice">
                        <div class="invoice-heading">
                            <div class="row date-row">
                                <div class="col-md-6 col-xs-6">
                                   <img src="<?= $invoice['logo']; ?>">
                                    <h5><?= $invoice['header'] ?></h5>
                                </div><!--.col-md-6-->
                                <div class="col-md-3 col-xs-3 invoice-id pull-right">
                                     <div id="invoice_barcode" style="float: right;padding-bottom: 20px;border-bottom: 1px solid #F1F1F1;margin-bottom: 12px;"></div>
                                     <hr/>
                                    <h4>Bilet: #<?=$invoice['ticket'];?></h4>
                                    <h5><?= $invoice['date']; ?></h5>
                                </div><!--.col-md-6-->
                            </div><!--.row-->
                            <div class="row customer-row">
                                <div class="col-md-6 col-xs-6 company">
                                    
                                    
                                </div><!--.col-md-6-->
                                <div class="col-md-6 col-xs-6 customer">
                                    
                                </div><!--.col-md-6-->
                            </div><!--.row-->
                        </div><!--.invoice-heading-->
                        <div class="invoice-body">
                            <table class="table table-hover">
                                <thead>
                                    <tr>
                                        <th></th>
                                        <th><?= __("Ürün"); ?></th>
                                        <th class="text-right"><?= __("Adet"); ?></th>
                                        <th class="text-right"><?= __("Birim Fiyatı"); ?></th>
                                        <th class="text-right"><?= __("Toplam"); ?></th>
                                    </tr>
                                </thead>
                                <tbody>
                                <?php foreach($invoice['products'] as $key => $product): ?>
                                    <tr>
                                        <td><?= $key+1 ?></td>
                                        <td>
                                            <?= $product['title']; ?>
                                            <small style="width: 50%;"><?= $product['desc']; ?></small>
                                        </td>
                                        <td class="text-right"><?= $product['each']; ?></td>
                                        <td class="text-right"><?=  $invoice['currency'] .' '.$product['price']; ?></td>
                                        <td class="text-right"><?=  $invoice['currency'] .' '.number_format($product['price']*$product['each'] , 2 , '.' , ' '); ?></td>
                                    </tr>
                                <?php endforeach ?>
                                    
                                    <tr>
                                        <td colspan="3"></td>
                                        <td style="width: 200px;" class="text-right"><strong><?= __("Ara Toplam:"); ?></strong></td>
                                        <td class="text-right"><?=  $invoice['currency'] .' '. $invoice['sub_total']; ?></td>
                                    </tr>
                                    <tr>
                                        <td colspan="3"></td>
                                        <td class="text-right"><strong><?= __("İndirim:"); ?></strong></td>
                                        <td class="text-right">
                                            <?= ($invoice['discount_type'] == 'amount' ? $invoice['currency'] : ''); ?>
                                            <?= $invoice['discount_amount']; ?>
                                            <?= ($invoice['discount_type'] == 'percent' ? '%' : ''); ?>
                                        </td>
                                    </tr>
                                    <?php 
                                        if($invoice['discount_type'] == 'amount'):

                                          $total = $invoice['sub_total']-$invoice['discount_amount'];

                                        else:
                                            $total = $invoice['sub_total']-($invoice['sub_total']*($invoice['discount_amount']/100));
                                        endif;
                                    ?>
                                    <tr>
                                        <td colspan="3"></td>
                                        <td class="text-right"><strong><?= __("TOPLAM:"); ?></strong></td>
                                        <td  class="text-right"><?= $invoice['currency'] .' '. number_format((float)$total, 2, '.', '');?></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div><!--.invoice-body-->
                        <div class="invoice-footer">
                            <?= $invoice['footer']; ?>
                        </div><!--.invoice-footer-->
                    </div><!--.invoice-->

                </div><!--.panel-body-->
            </div><!--.panel-->
        </div><!--.col-md-12-->
    </div>
</div>


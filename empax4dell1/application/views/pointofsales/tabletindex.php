<div class="container-fluid">
  <form id="pos">
    <div class="row example-row">
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <ul class="nav nav-tabs hidden" role="tablist">
        <li class="active"><a href="#home-tab" id="home" data-toggle="tab"><?= __("Masa Seç"); ?></a></li>
        <li><a href="#product-tab" id="tableName" data-toggle="tab"><?= __("Masa Adı"); ?></a></li>
        <li><a href="#order-tab" id="orderCheckTab" data-toggle="tab"><?= __("Sipariş Ekranı"); ?></a></li>
        <li><a href="#tab-operatorsession" id="tabOperationSession" data-toggle="tab">Operation Session</a></li>
        <li><a href="#tab-order-complete" id="tabOrderComplete" data-toggle="tab">Order Complete</a></li>
        <li><a href="#tab-category-list" id="tabCategoryList" data-toggle="tab">Category List</a></li>
        <li><a href="#tab-lock" id="tabLocked" data-toggle="tab">Lock Screen</a></li>
        </ul>

        <div class="tab-content home-menu" id="main-tabs">
          <div class="tab-pane active" id="home-tab">
            <?php $this->load->view('tablet/elements/tab_home'); ?>
          </div> <!-- home-tab close -->

          <div class="tab-pane" id="product-tab">
            <?php $this->load->view('tablet/elements/tab_product'); ?>
          </div> <!-- product-tab close -->

          <div class="tab-pane" id="order-tab">
            <?php $this->load->view('tablet/elements/tab_order'); ?>
          </div> <!-- order-tab tab pane close -->

          <div class="tab-pane" id="tab-operatorsession">
            <?php $this->load->view('tablet/elements/tab_operatorsession'); ?>
          </div> 

          <div class="tab-pane" id="tab-order-complete">
            <?php $this->load->view('tablet/elements/tab_ordercomplete'); ?>
          </div> 

          <div class="tab-pane" id="tab-category-list">
            <?php $this->load->view('tablet/elements/tab_categorylist'); ?>
          </div> 

          <div class="tab-pane" id="tab-lock">
            <?php $this->load->view('tablet/elements/tab_lock'); ?>
          </div>  

        </div> <!-- tab-content finish -->
      </div> <!-- genel col-lg-12 close -->
    </div> <!-- row example row close -->

    <div id="hiddenInputs">
      <input type="hidden" name="location_id" value="<?=$pos_settings->location_id;?>">
      <input type="hidden" name="pos_id" value="<?=$pos_settings->pos_id;?>">
      <input type="hidden" name="sale_id" value="">
      <input type="hidden" name="table_id" value="">
      <input type="hidden" name="discount_key" value="">
      <input type="hidden" name="location_currency" value="<?= $location_currency ?>">
    </div>

    <!-- MODALS  -->

    <!-- Product Modal -->

    <div class="modal" id="productQuantityModal" role="dialog">
      <div class="modal-dialog"> 
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title text-center"><?= __("Ürün Seçim Ekranı"); ?></h4>
          </div>

          <div class="modal-body">
            <label id="portionLabel" for="porsiyon"><b><?= __("Porsiyon Seçiniz"); ?></b></label>
            <select class="form-control" id="sel1"></select>

            </br>

            <label class="mw-100" for ="extra-product"><b><?= __("Malzeme Seçimi"); ?></b></label>
            <ul id="chooseItems" class="extra-product">
            </ul>

            </br>

            <label class="mw-100" for ="extra-product"><b><?= __("Menü İçeriği"); ?></b></label>
            <ul id="menuItems" class="menu-products">    
            </ul>

            </br>

            <label class="mw-100" for ="extra-product"><b><?= __("Ek Ürün Seçiniz"); ?></b></label>
            <ul id="additionalItems" class="additional-products">    
            </ul>

            </br>

            <label class="mw-100" for ="extra-product"><b><?= __("Ürün Notu"); ?></b>
              <div class="list-group" id="quick_notes">
              </div>
              <div class="list-group" id="product_notes">
              </div>
            </label>

          </div> <!-- modal body -->

          <div class="modal-footer">
            <label class="text-center"><h4 class="text-center"><?= __("Adet Giriniz"); ?></h4></label>
            <div class="input-group">
              <span class="input-group-btn">
                <button id="minus" type="button" class="btn btn-danger btn-number btn-xxl"">
                  <span class="glyphicon glyphicon-minus"></span>
                </button>
              </span>
              <input type="number" id="productQuantity" name="quant[2]" class="form-control input-number input-xxlarge" value="1" style="text-align: center;">
              <span class="input-group-btn">
                <button id="plus" type="button" class="btn btn-success btn-number btn-xxl">
                  <span class="glyphicon glyphicon-plus"></span>
                </button>
              </span>
            </div> <!-- input group btn -->
            <div class=".btn-group-justified" style="text-align: left;">
              <!-- <button type="button" id="create-soundnote" class="btn btn-gray btn-xxl"><i class="fa fa-microphone"></i></button> -->
              <button type="button" id="create-textnote" class="btn btn-gray btn-xxl"><i class="fa fa-pencil"></i></button>
              <button type="button" id="modal-product-add" class="btn btn-blue btn-xxl col-md-6 col-lg-6 col-sm-6 col-xs-6" style="float:right;" data-dismiss="modal"><?= __("Ekle"); ?></button>
            </div>
          </div> <!-- modal footer -->
        </div>  <!-- modal content -->
      </div> <!-- modal dialog -->
    </div> <!-- modal -->

    <!-- Product Note Modal -->

    <div class="modal" id="modal-product_note" data-keyboard="false" data-backdrop="static">
      <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title"><?= __("Not Ekle"); ?></h4>
            </div>
            <div class="modal-body">

                <div class="hidden">
                    <a href="#notesound" data-toggle="tab"><?= __("Sesli Not"); ?></a>
                    <a href="#notetext" data-toggle="tab"><?= __("Not"); ?></a>
                </div>
            
                <div class="tab-content p-a-0">
                    <div class="tab-pane" id="notesound">
                        <h1 class="text-center note-height">
                            <b class="timer">00:00:00</b><br>
                            <i class="fa fa-microphone fa-flash"></i>
                        </h1>

                        <div class="btn-group btn-group-justified">
                            <a href="#" class="btn btn-danger btn-xxl cancel"><?= __("İptal"); ?></a>
                            <a href="#" class="btn btn-success btn-xxl save"><?= __("Kaydet"); ?></a>
                        </div>
                    </div>

                    <div class="tab-pane" id="notetext">
                        <textarea name="note" class="form-control note-height" rows="4" placeholder="<?= __("Notunuz..."); ?>"></textarea>

                        <div class="btn-group btn-group-justified">
                            <a href="#" class="btn btn-danger btn-xxl cancel"><?= __("İptal"); ?></a>
                            <a href="#" class="btn btn-success btn-xxl save"><?= __("Kaydet"); ?></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
      </div>
    </div>

    <!-- Discount Modal -->
    <div id="modal-discount_login" class="modal fade" role="dialog">
      <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
              <h4 class="modal-title"><?= __("İndirim Oluştur"); ?></h4>
          </div>
          <div class="modal-body">
            <div class="input-group">
                <input type="number" name="discount_amount" class="form-control input-lg" placeholder="<?= __("İndirim miktarı"); ?>" autocomplete="off">
                <span class="input-group-btn">
                    <select name="discount_type" class="form-control-static btn btn-lg">
                        <option value="percent">%</option>
                        <option value="amount"><?= $location_currency ?></option>
                    </select>
                </span>
            </div>
            <div class ="input-group">
              <input type="text" name="username" class="hidden">
              <input type="text" name="username" class="form-control input-lg" placeholder="<?= __("Kullanıcı Adı veya Eposta"); ?>" autocomplete="off">
              <input type="password" name="password" class="form-control input-lg" placeholder="<?= __("Şifre"); ?>" autocomplete="off">
            </div>
          </div>
          <div class="modal-footer">
              <button type="button" class="btn btn-primary btn-xxl btn-block" id="submit-discount-login"><?= __('İndirim Ekle'); ?></button>
          </div>
        </div>

      </div>
    </div>

  </form> <!-- Form Finish -->
</div> <!-- Container Fluid Finish -->


<!-- Interim Payment Type ( sodexo ticket vs. ) -->
<div class="modal" id="modal-product-payment">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-body col-lg-12 col-md-12 col-sm-12 col-xs-12">

        <div class="btn-group interim_payments-submit col-lg-12 col-md-12 col-sm-12 col-xs-12" role="group">
          <div class="btn-group" role="group">
            <button type="button" class="btn btn-red btn-ripple btn-xxl col-lg-12 col-md-12 col-sm-12 col-xs-12" data-payment="ticket"><i class="fa fa-credit-card-alt"></i> <?= __('TICKET') ?></button>
            <button type="button" class="btn btn-blue btn-ripple btn-xxl col-lg-12 col-md-12 col-sm-12 col-xs-12" data-payment="sodexo"><i class="fa fa-credit-card-alt"></i> <?= __('SODEXO') ?></button>
            <button type="button" class="btn btn-green btn-ripple btn-xxl col-lg-12 col-md-12 col-sm-12 col-xs-12" data-payment="multinet"><i class="fa fa-credit-card-alt"></i> <?= __('MULTINET') ?></button>
            <button type="button" class="btn btn-orange btn-ripple btn-xxl col-lg-12 col-md-12 col-sm-12 col-xs-12" data-payment="setcart"><i class="fa fa-credit-card-alt"></i> <?= __('SETCARD') ?></button>
            <button type="button" class="btn btn-purple btn-ripple btn-xxl col-lg-12 col-md-12 col-sm-12 col-xs-12" data-payment="gift"><i class="fa fa-gift"></i> <?= __('İKRAM') ?></button>
          </div>
        </div>

      </div>
      <div class="modal-footer col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center" style="border-top: 0; margin-top: 100px;">
        <img class="modal-img-close" src="<?= asset_url('cancel.svg'); ?>" style="max-height: 50px;" data-dismiss="modal"></img>
      </div>
    </div>
  </div>
</div>
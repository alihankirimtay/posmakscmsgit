<div class="content">

    <div class="page-header full-content">
        <div class="row">
            <div class="col-sm-6">
                <h1>Sales</h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb">
                    <li><a href="<?php ECHO base_url(); ?>" ><i class="ion-home"></i></a></li>
                    <li><a href="#" class="active">Sales</a></li>
                </ol>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="panel">

                <div class="panel-heading">
                    <div class="panel-title">
                        <h4>All Sales</h4>
                        <div class="btn-group pull-right">
                            <a href="<?php ECHO base_url('sales/add/'); ?>" class="btn btn-success btn-xs"><i class="fa fa-plus"></i> New Sale</a>
                        </div>
                    </div>
                </div>

                <div class="panel-body">
                    <div class="overflow-table">
                        <table class="display datatables-basic">
                            <thead>
                                <tr>
                                    <th class="col-md-2">Date</th>
                                    <th class="col-md-1">No</th>
                                    <th class="col-md-2">Ticket Number</th>
                                    <th class="col-md-1">Operator</th>
                                    <th class="col-md-1">Location</th>
                                    <th class="col-md-2">Discount</th>
                                    <th class="col-md-2">Total</th>
                                    <th class="col-md-1">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($sales as $location): ?>
                                    <tr>
                                        <td><?php ECHO $location->title; ?></td>
                                        <td><?php ECHO $location->created; ?></td>
                                        <td><?php ECHO get_status($location->active); ?></td>
                                        <td>
                                            <a class="btn btn-primary btn-xs" href="<?php ECHO base_url('sales/edit/'.$location->id); ?>"> <i class="fa fa-pencil"></i> </a>
                                            <button class="btn btn-danger btn-xs" data-url="<?php ECHO base_url('sales/delete/'.$location->id); ?>" name="dodelete"> <i class="fa fa-trash-o"></i> </button>
                                        </td>
                                    </tr>
                                <?php endforeach ?>
                            </tbody>
                        </table>
                    </div>
                </div>

            </div>
        </div>
    </div>

</div>
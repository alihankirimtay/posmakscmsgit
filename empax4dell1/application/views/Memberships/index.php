<?php
    
    $this->website->date_expire = isValidDate($this->website->date_expire, 'Y-m-d H:i:s') ? $this->website->date_expire : date('Y-m-d H:i:s');

    $dateCurrent = new DateTime();
    $dateExpire = new DateTime($this->website->date_expire);
    $interval = $dateCurrent->diff($dateExpire);

    $date = dateConvert2($this->website->date_expire, 'client', 'Y-m-d H:i:s', 'd/m/Y');

    // Iyzipay den sonuç döndüğünde bildirim göster.
    $has_payment = false;
    if ($this->input->post('payment-message')) {
        $has_payment = true;
        $status = (int) $this->input->post('payment-status');
        $status = $status ? 'success' : 'warning';
        $message = $this->input->post('payment-message');
    }
?>
<div class="content">

    <div class="page-header full-content">
        <div class="row">
            <div class="col-sm-6">
                <h1><?= __("Abonelik Bilgileri"); ?></h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb">
                    <li><a href="<?php ECHO base_url(); ?>" ><i class="ion-home"></i></a></li>
                    <li><a href="#" class="active"><?= __("Abonelik Bilgileri"); ?></a></li>
                </ol>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="panel">

                <div class="panel-heading">
                    <div class="panel-title">
                        <h4><?= __("Abonelik Bilgileri"); ?></h4>
                        <div class="btn-group pull-right">
                            <a href="#modal-membership_info" data-toggle="modal" class="btn btn-brown btn-xs"><i class="fa fa-cog"></i> <?= __("Abonelik Ayarları"); ?></a>
                        </div>
                    </div>
                    <div class="alert alert-danger">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        <strong><?= __('Bir Sonraki Abonelik Yenilemenize {number} Gün Kaldı', ['{number}' => $interval->days]) ?></strong>
                        <p><?= __('Abonelik Yenileme Tarihi: {number} Aboneliğiniz Otomatik Olarak Yenilenecektir.', ['{number}' => $date]); ?></p>
                    </div>

                    <?php if ($has_payment): ?>
                        <div class="alert alert-<?= $status ?>">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <strong><?= __("Ödeme Sonucu:"); ?></strong>
                            <p><?= $message ?></p>
                        </div>
                    <?php endif; ?>
                </div>

                <div class="panel-body">
                    <h3><?= __("POSMAKS Cloud Paketleri"); ?></h3>
                    <table class="table table-hover" id="memberships">
                        <tbody>
                            <tr>
                                <td class="text-center"><i class="fa fa-spinner fa-spin fa-2x"></i></td>
                            </tr>
                        </tbody>
                    </table>
                </div>

                <div class="panel-body">
                    <h3><?= __("İşlem Geçmişiniz"); ?></h3>
                    <table class="table table-hover" id="history">
                        <thead>
                            <tr>
                                <th><?= __("İşlem No"); ?></th>
                                <th><?= __("İşlem Tarihi"); ?></th>
                                <th><?= __("Açıklama"); ?></th>
                                <th><?= __("Ödeme Metodu"); ?></th>
                                <th><?= __("Meblağ"); ?></th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td colspan="5" class="text-center"><i class="fa fa-spinner fa-spin fa-2x"></i></td>
                            </tr>
                        </tbody>
                    </table>
                </div>

            </div>
        </div>
    </div>

</div>




<?php $this->load->view('Memberships/elements/membership_info_modal'); ?>
<?php $this->load->view('Memberships/elements/buy_membership_modal'); ?>
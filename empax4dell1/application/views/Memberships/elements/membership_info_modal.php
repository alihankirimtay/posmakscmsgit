<div class="modal fade" id="modal-membership_info">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title"><?= __("Abonelik Bilgileri"); ?></h4>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-sm-12">
						<b><?= __("Kredi Kartı ve Bilgileriniz"); ?></b>
					</div>
					<div class="col-sm-12 margin-top-20">
						<b><?= __("Kayıtlı Kredi Kartınız:"); ?></b>
						<p id="binNumber">---</p>
					</div>
					<div class="col-sm-12 margin-top-5">
						<b><?= __("Fatura Adresiniz"); ?></b>
						<p id="billingAddress">---</p>
					</div>
					<div class="col-sm-12 margin-top-5">
						<a href="<?= base_url('memberships/settings') ?>" class="btn1 btn-link1"><?= __("Kredi kartı ve adres bilgilerinizi değiştirmek için tıklayın."); ?></a>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
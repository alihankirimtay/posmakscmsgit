<?php 

    $permissionsHasRole = function ($role, $permission) {
        if (isset($permission->roles)) {
            foreach ($permission->roles as $permission_role) {
                if ($permission_role->id == $role->id) {
                    return true;
                }
            }
        }
    };

?>
<div class="content">

    <div class="page-header full-content">
        <div class="row">
            <div class="col-sm-6">
                <h1><?= __('Yetkiler') ?> <small></small></h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb">
                    <li><a href="<?= base_url(); ?>" ><i class="ion-home"></i></a></li>
                    <li><a href="#" class="active"><?= __('Ayarlar') ?></a></li>
                    <li><a href="<?= base_url('Roles'); ?>"><?= __('Roller') ?></a></li>
                    <li><a href="#" class="active"><?= __('Yetkiler') ?></a></li>
                </ol>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="panel m-b-3">

                <div class="panel-heading">
                    <div class="panel-title">
                        <h4><?= __('Yetki Detayları') ?></h4>
                    </div>
                </div>

                <div class="panel-body">
                    <form class="form-horizontal">

                        <div class="form-content">
                            
                            <table class="table table-hover">
                                <thead>
                                    <tr>
                                        <th><?= __('İşlem') ?></th>
                                        <?php foreach ($roles as $role): ?>
                                            <th>
                                                <input type="checkbox" class="check-all" data-role_id="<?= $role->id ?>">
                                                <?= $role->title ?>
                                            </th> 
                                        <?php endforeach; ?>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($permissions as $permission): ?>
                                        <tr>
                                            <td title="<?= $permission->action ?>">
                                                <?= $permission->description ?>
                                            </td>
                                            <?php foreach ($roles as $role): ?>
                                                <td title="<?= $role->title ?>: <?= $permission->description ?>">
                                                    <?php if ($permissionsHasRole($role, $permission)): ?>
                                                        <div class="icheck icheck-flat-green">
                                                            <div class="checkbox">
                                                                <input type="checkbox" value="1" name="roles[<?= $role->id ?>][<?= $permission->id ?>]" checked>
                                                            </div>
                                                        </div>
                                                    <?php else: ?>
                                                        <div class="icheck icheck-flat-blue">
                                                            <div class="checkbox">
                                                                <input type="checkbox" value="1" name="roles[<?= $role->id ?>][<?= $permission->id ?>]">
                                                            </div>
                                                        </div>
                                                    <?php endif; ?>
                                                </td> 
                                            <?php endforeach; ?>
                                        </tr>
                                    <?php endforeach; ?>
                                </tbody>
                            </table>
                            
                            <div class="form-buttons-fixed">
                                <a type="button" href="<?= base_url('roles'); ?>" class="btn btn-grey"><?= __('İptal') ?></a>
                                <div class="btn-group pull-right">
                                    <button type="submit" class="btn btn-success" name="dosubmit" data-url="<?= base_url('roles/permission/');?>" ><?= __('Kaydet') ?></button>
                                    <button id="kaydetVeCik" type="submit" class="btn btn-blue" name="dosubmit" data-url="<?= base_url('roles/permission/');?>" data-return="<?= base_url('roles'); ?>" ><?= __('Kaydet & Çık') ?></button>
                                </div>
                            </div>

                        </div>

                    </form>
                </div>

            </div>
        </div>
    </div>

</div>

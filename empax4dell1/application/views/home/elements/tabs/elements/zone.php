<div class="well p-a-2 col-md-12">
    <div class="hidden">
        <a href="#zoneAdd" data-toggle="tab" >1.</a>
        <a href="#zoneEdit" data-toggle="tab">2.</a>
    </div>

    <h6 style="color: black"><?= __('Lütfen bölge eklemek istediğiniz katı seçiniz.'); ?></h6>

    <div class="floor-tab col-md-12 p-a-0">
        
    </div>
    <div class="tab-content p-a-0 b-0">
        <div class="tab-pane active" id="zoneAdd">
            <div class="col-md-12 p-l-0">
                <form class="form-inline col-md-12 p-l-0" role="form">
                    <div class="form-group col-md-8 p-l-0">
                        <label for="zoneInput" class="control-label d-block" >Bölge Adı</label>
                        <input type="text" id="zoneInput" name="title" class="form-control input-lg bg-special-white bb-0 w-full" />
                        <input type="hidden" name="active" value="1">
                        <input type="hidden" name="floor_id" value="">
                        <input type="hidden" name="location_id" value="">
                    </div>
                    <div class="form-group col-md-2">
                        <label for="tableCountInput" class="control-label">Masa Sayısı</label>                    
                        <input type="number" id="tableCountInput" name="table_count" min="0" class="form-control input-lg bg-special-white w100 bb-0" />
                    </div>
                    <button id="saveZone" type="button" class="btn btn-success col-md-2 input-lg br-0 save-button-fixed">KAYDET</button>
                </form>
            </div>          
        </div>

        <div class="tab-pane b-0 p-a-0" id="zoneEdit">
            <div class="col-md-12 p-l-0">
                <form class="form-inline col-md-12 p-l-0" role="form">
                    <div class="form-group col-md-6 p-l-0">
                        <label for="zoneEditInput" class="control-label d-block">Bölge Adı</label>
                        <input type="text" id="zoneEditInput" name="title" class="form-control input-lg bg-special-white bb-0 w-full" />
                        <input type="hidden" name="active" value="">
                        <input type="hidden" name="zone_id" value="">
                        <input type="hidden" name="location_id" value="">
                        <input type="hidden" name="floor_id" value="">
                    </div>
                    <div class="form-group col-md-2">
                        <label for="tableEditCountInput" class="control-label">Masa Sayısı</label>                                            
                        <input type="number" id="tableEditCountInput" name="table_count" min="0" class="form-control input-lg bg-special-white w100 bb-0" disabled />
                    </div>
                    <button id="btnEditZone" type="button" class="btn btn-success col-md-2 input-lg br-0" style="position:absolute; bottom:0">KAYDET</button>
                    <button id="btnCancelZone" type="button" class="btn btn-danger col-md-2 input-lg br-0 save-button-fixed">İPTAL</button>
                </form>
            </div>   
        </div>
    </div>

</div>

<div class="zone-list">
    
</div>

<button type="button" class="btn btn-success footer-button-fixed br-0 btn-xxl" disabled>İLERİ</button>

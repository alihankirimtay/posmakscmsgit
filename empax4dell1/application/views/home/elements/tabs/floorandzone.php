<div class="panel">
    <div class="panel-heading bb-0">
        <div class="panel-title p-l-0">
            <h4 class="content-header"><?= __('HIZLI KAT, BÖLGE VE MASA EKLEME') ?></h4>
            <h6 style="color: black"><?= __('Bu aşamada, işletmenize ait kat ve bölgelerinizi ekleyebilirsiniz.'); ?></h6>
        </div>
        <ul class="nav nav-tabs pull-right b-0 nav-header-tabs col-sm-12">
            <li class="add-floor col-sm-5"><a class="text-color-white" href="#addFloor" data-toggle="tab"><?= __('KAT EKLE') ?></a></li>
            <li class="add-zone col-sm-5"><a class="text-color-white" href="#addZone" data-toggle="tab"><?= __('BÖLGE EKLE') ?></a></li>
        </ul>
    </div>

    <div class="p-l-0 p-r-0 panel-body" style="overflow-y:auto;height: 300px">
        <div class="tab-content p-a-0 b-0" >
            <div class="tab-pane active" id="addFloor">
                <?php $this->load->view('home/elements/tabs/elements/floor.php'); ?>     
            </div>
            <div class="tab-pane" id="addZone">
                <?php $this->load->view('home/elements/tabs/elements/zone.php'); ?>                     
            </div>
        </div>

    </div>

</div>

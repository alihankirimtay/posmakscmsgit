<div id="addStockModal" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="wizard-header">
                <div class="col-lg-10 header-title">
                    <span><?= __('STOK OLUŞTUR') ?></span>
                </div>
                <div class="col-lg-2 close-button" data-dismiss="modal">
                    <span class="pull-right">X</span>
                </div>
            </div>
            <div class="modal-body">
                <div class="tab-content" style="overflow-y:auto; height: 400px;">
                    <form action="#" class="form-horizontal" id="quickStocksForm">
                        <div class="form-content">

                            <div class="form-group">
                                <label class="control-label col-md-3"><?= __('Stok Adı') ?></label>
                                <div class="col-md-9">
                                    <div class="inputer">
                                        <div class="input-wrapper">
                                            <input type="text" name="title" class="form-control">
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3"><?= __('Ölçü Birimi') ?></label>
                                <div class="col-md-9">
                                    <select name="unit" data-placeholder="Ölçü Birimi bulunamadı" class="chosen-select">
                                        <option value=""><?= __('Ölçü Birimi Seç') ?></option>
                                        <?php foreach ($this->units as $key => $unit): ?>
                                            <option value="<?= $key ?>"><?= $unit ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                            </div>  

                            <div class="form-group">
                                <label class="control-label col-md-3"><?= __('Alt Limit') ?></label>
                                <div class="col-md-9">
                                    <div class="inputer">
                                        <div class="input-wrapper">
                                            <input type="number" name="sub_limit" class="form-control">
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <input type="hidden" name="location_id" value="">
                            <input type="hidden" name="active" value="1">

                            <div class="btn-group col-md-12 p-a-0" style="position: absolute; margin: 0; bottom: 10px; right: 0rem;">
                                <button id="saveQuickStock" type="button" class="br-0 btn btn-ripple btn-success btn-xl col-md-6" ><?= __('Kaydet') ?></button>
                                <button data-dismiss="modal" type="button" class="br-0 btn btn-danger btn-ripple btn-xl col-md-6" ><?= __('İptal') ?></button>
                            </div>
                        </div>
                    </form>  
                </div>
            </div>
        </div>
    
    </div>
</div>

<?php

    $selectedPackageProducts = function ($package_products, $package) {
        foreach ($package_products as $key => $_product) {
            if ($_product->product_id == $package->id) {
                return 'selected="selected"';
            }
        }
    };


    $selectedStocks = function ($stocks, $stock) {
        foreach ($stocks as $_stock) {
            if ($_stock->stock_id == $stock->id) {
                return 'selected="selected"';
            }
        }
    };

    $selectedAdditionalProducts = function ($additional_products, $product) {
        foreach ($additional_products as $_product) {
            if ($_product->additional_product_id == $product->id) {
                return 'selected="selected"';
            }
        }
    };

    $getSelectedStock = function ($stocks, $stock) {
        foreach ($stocks as $_stock) {
            if ($_stock->stock_id == $stock->id) {
                return $_stock;
            }
        }
    };

    $selectedPortionProducts = function ($portion_prodcuts, $product) {
        foreach ($portion_prodcuts as $_product) {
            if ($_product->id == $product->id) {
                return 'selected="selected"';
            }
        }
    };

    $selectedQuickNotesProducts = function ($quickNotes, $product) {
        foreach ($quickNotes as $_product) {
            if ($_product->quick_note_id == $product->id) {
                return 'selected="selected"';
            }
        }
    };

?>
<div class="content">

    <div class="page-header full-content">
        <div class="row">
            <div class="col-sm-6">
                <h1><?= __('Ürün Düzenle') ?> <small></small></h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb">
                    <li><a href="<?= base_url(); ?>" ><i class="ion-home"></i></a></li>
                    <li><a href="<?= base_url('products'); ?>"><?= __('Ürünler') ?></a></li>
                    <li><a href="#" class="active"><?= __('Ürün Düzenle') ?></a></li>
                </ol>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="panel">

                <div class="panel-heading">
                    <div class="panel-title">
                        <h4><?= __('Ürün Detayları') ?></h4>
                    </div>
                </div>

                <div class="panel-body">
                    <form action="#" class="form-horizontal">
                        <div class="form-content">
                            
                            <div class="form-group">
                                <label class="control-label col-md-3"><?= __('Restoran') ?></label>
                                <div class="col-md-9">
                                    <select class="chosen-select" name="location_id">
                                        <option value="" data-tax="0"><?= __('Restoran Seç') ?></option>
                                        <?php foreach ($this->user->locations_array as $location): ?>
                                            <option value="<?= $location['id'] ?>" <?= selected($location['id'], $product->location_id) ?>><?= $location['title'] ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3"><?= __('Kategori') ?></label>
                                <div class="col-md-9">
                                    <select class="chosen-select" name="product_type_id">
                                        <option value=""><?= __('Kategori Seçin') ?></option>
                                        <?php foreach ($product_types as $product_type): ?>
                                            <option value="<?= $product_type->id ?>" <?= selected($product_type->id, $product->product_type_id) ?>><?= $product_type->title ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3"><?= __('Üretim Yeri') ?></label>
                                <div class="col-md-9">
                                    <select class="chosen-select" name="production_id">
                                        <option value=""><?= __('Üretim Yeri Seç') ?></option>
                                        <?php foreach ($productions as $production): ?>
                                            <option value="<?= $production->id ?>" <?= selected($production->id, $product->production_id) ?>><?= $production->title ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3"><?= __('İsim') ?></label>
                                <div class="col-md-9">
                                    <div class="inputer">
                                        <div class="input-wrapper">
                                            <input type="text" name="title" class="form-control" value="<?= $product->title ?>">
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3"><?= __('Açıklama') ?></label>
                                <div class="col-md-9">
                                    <div class="inputer">
                                        <div class="input-wrapper">
                                            <textarea name="desc" class="form-control js-auto-size valid"><?= $product->desc ?></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3"><?= __('Barkod') ?></label>
                                <div class="col-md-9">
                                    <div class="inputer">
                                        <div class="input-wrapper">
                                            <input type="text" name="bar_code" class="form-control" value="<?= $product->bar_code ?>">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="form-group">
                                <label class="control-label col-md-3"><?= __('Vergi Oranı') ?></label>
                                <div class="col-md-9">
                                    <select class="chosen-select" name="tax">
                                        <?php foreach ($this->taxes as $tax_key => $tax): ?>
                                            <option value="<?= $tax_key; ?>" <?= selected($product->tax, $tax_key) ?>><?= $tax ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3"><?= __('Fiyat') ?></label>
                                <div class="col-md-9">
                                    <div class="inputer">
                                        <div class="input-wrapper">
                                            <input type="number" name="price" class="form-control price-without-tax" step="0.001" placeholder="0.00" value="<?= $product->price ?>">
                                        </div>
                                        <p class="help-block"><small class="text-danger"><?= __('Vergi hariç') ?></small></p>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-md-offset-3 col-md-9">
                                    <div class="inputer">
                                        <div class="input-wrapper">
                                            <input type="number" name="price_tax" class="form-control price-with-tax" step="0.01">
                                        </div>
                                        <p class="help-block"><small class="text-danger"><?= __('Vergi dahil') ?></small></p>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3">Ürün Sıralaması</label>
                                <div class="col-md-9">
                                    <div class="inputer">
                                        <div class="input-wrapper">
                                            <input type="text" name="sort" class="form-control" value="<?= $product->sort; ?>">
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="alert alert-primary-transparent">
                                <div class="form-group">
                                    <label class="control-label col-md-3"><?= __('Ürün Tipi') ?></label>
                                    <div class="col-md-9">
                                        <div class="radioer form-inline">
                                            <input type="radio" name="type" id="type0" value="product" <?= checked($product->type, 'product') ?>>
                                            <label for="type0"><i class="fa fa-square"></i> <?= __('Ürün') ?></label>
                                        </div>
                                        <div class="radioer form-inline">
                                            <input type="radio" name="type" id="type1" value="package" <?= checked($product->type, 'package') ?>>
                                            <label for="type1"><i class="fa fa-th-large"></i> <?= __('Menü') ?></label>
                                        </div>
                                    </div>
                                </div>
                                
                                <div id="products-container" class="hidden">
                                    <div class="form-group">
                                        <label class="control-label col-md-3"><?= __('Menü içerisindeki ürünler') ?></label>
                                        <div class="col-md-9">
                                            <select multiple="multiple" name="products[]" class="chosen-select">
                                                <?php foreach ($products as $package_product): ?>
                                                    <?php if ($package_product->type == 'product'): ?>
                                                        <option 
                                                            value="<?= $package_product->id ?>"  
                                                            data-location_id="<?= $package_product->location_id ?>" 
                                                            class="hidden"
                                                            <?= $selectedPackageProducts($product->package_products, $package_product) ?>>
                                                            <?= $package_product->title ?>
                                                        </option>
                                                <?php endif; ?>
                                                <?php endforeach; ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <div id="stocks-container">
                                    <div class="form-group">
                                        <label class="control-label col-md-3"><?= __('Stok İçeriği') ?></label>
                                        <div class="col-md-7">
                                            <select multiple="multiple" class="chosen-select stocks">
                                                <?php foreach ($stocks as $stock): ?>
                                                    <option 
                                                        value="<?= $stock->id ?>" 
                                                        data-location_id="<?= $stock->location_id ?>"
                                                        data-unit="<?= @$this->units[$stock->unit] ?>"
                                                        class="hidden"
                                                        <?= $selectedStocks($product->stocks, $stock) ?>>
                                                        <?= $stock->title ?>
                                                    </option>
                                                <?php endforeach; ?>
                                            </select>
                                        </div>
                                        <div class="col-sm-2">
                                            <a id="addStock" class="btn btn-success btn-xs" style="color: white;"><i class="fa fa-plus-square"></i> <?= __('Stok Oluştur') ?></a>
                                        </div>
                                    </div>
                                    <?php foreach ($stocks as $stock): ?>
                                        <?php if ($selectedStock = $getSelectedStock($product->stocks, $stock)): ?>
                                            <div class="form-group stock-values" id="stockContainer<?= $stock->id ?>">
                                                <label class="control-label col-sm-3"><?= $stock->title ?></label>
                                                <div class="col-sm-3">
                                                    <div class="inputer">
                                                        <div class="input-wrapper">
                                                            <input type="number" name="stocks[<?= $stock->id ?>][quantity]" class="form-control" step="0.01" value="<?= $selectedStock->quantity ?>" placeholder="0.00">
                                                        </div>
                                                    </div>
                                                </div>
                                                <label class="col-sm-1"><b><?= @$this->units[$stock->unit] ?></b></label>
                                                <label class="col-sm-3">
                                                    <div class="checkboxer">
                                                        <input type="checkbox" name="stocks[<?= $stock->id ?>][optional]" value="1" id="stockCheckbox<?= $stock->id ?>" <?= checked($selectedStock->optional, 1) ?>>
                                                        <label for="stockCheckbox<?= $stock->id ?>"><?= __('İsteğe bağlı siparişten çıkarılabilir') ?></label>
                                                    </div> 
                                                </label>
                                            </div>
                                        <?php endif; ?>
                                    <?php endforeach; ?>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3"><?= __('İlave seçilebilir ürünler') ?></label>
                                <div class="col-md-9">
                                    <select name="additional_products[]" class="chosen-select" multiple="multiple">
                                        <?php foreach ($products as $additional_product): ?>
                                            <?php if ($additional_product->type == 'product' && $additional_product->id != $product->id): ?>
                                                <option value="<?= $additional_product->id ?>"
                                                data-location_id="<?= $additional_product->location_id ?>"
                                                class="hidden"
                                                <?= $selectedAdditionalProducts($product->additional_products, $additional_product) ?>>
                                                    <?= $additional_product->title ?>
                                                </option>
                                            <?php endif; ?>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3"><?= __('Ürüne ait diğer porsiyonlar') ?></label>
                                <div class="col-md-9">
                                    <select name="portion_products[]" class="chosen-select" multiple="multiple">
                                        <?php foreach ($products as $portion_product): ?>
                                            <?php if ($portion_product->type == 'product' && $portion_product->id != $product->id ): ?>
                                                <option value="<?= $portion_product->id ?>"
                                                data-location_id="<?= $portion_product->location_id ?>"
                                                class="hidden"
                                                <?= $selectedPortionProducts($product->portion_products, $portion_product) ?>>
                                                    <?= $portion_product->title ?>
                                                </option>
                                            <?php endif; ?>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group" class="list-quickNotes">
                                <label class="control-label col-md-3"><?= __('Hızlı Notlar') ?></label>
                                <div class="col-md-7" class="list-quickNotesGroup">
                                    <select name="quick_notes[]" class="chosen-select" multiple="multiple">
                                        <?php foreach ($quickNotes as $quickNote): ?>
                                            <option value="<?= $quickNote->id ?>" <?= $selectedQuickNotesProducts($product->quick_notes, $quickNote) ?>>
                                                <?= $quickNote->title ?>
                                            </option>
                                        <?php endforeach; ?>
                                    </select>    
                                </div>
                                <div class="col-sm-2">
                                    <a href="#quicknotesmodal" data-toggle="modal" class="btn btn-warning"><i class="fa fa-sticky-note-o"></i><?= __('Hızlı Not Ekle') ?></a>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-sm-3"><?= __('Prim') ?></label>
                                <div class="col-sm-7">
                                    <div class="inputer">
                                        <div class="input-wrapper">
                                            <input type="number" name="bonus" class="form-control" value="<?= $product->bonus ?>">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-2">
                                    <select class="form-control" name="bonus_type">
                                        <option value="value" <?= selected($product->bonus_type, 'value') ?>><?= __('Miktar') ?></option>
                                        <option value="percent" <?= selected($product->bonus_type, 'percent') ?>><?= __('Yüzdelik') ?></option>
                                    </select>
                                </div>
                            </div>

                            <div class="row example-row">
                                <div class="col-md-3 control-label"><?= __('Renk') ?></div>
                                <div class="col-md-9">
                                    <div class="inputer">
                                        <div class="input-wrapper">
                                            <input type="color" name="color" class="form-control" value="<?= $product->color ?>">
                                        </div>
                                    </div>
                                </div>
                            </div>


                            <div class="form-group">
                                <label class="control-label col-md-3"><?= __('Resim') ?></label>
                                <div class="col-md-9">
                                    <div class="fileinput fileinput-new" data-provides="fileinput">
                                        <div class="fileinput-preview thumbnail" data-trigger="fileinput" style="width: 200px; height: 150px;">
                                            <?php if ($product->image && file_exists(FCPATH . $product->image)): ?>
                                                <img src="<?= base_url($product->image) ?>" >
                                            <?php endif; ?>
                                        </div>
                                        <div>
                                            <span class="btn btn-default btn-file">
                                                <span class="fileinput-new"><?= __('Resim Seç') ?></span>
                                                <span class="fileinput-exists"><?= __('Değiştir') ?></span>
                                                <input type="file" name="image">
                                            </span>
                                        </div>
                                        <div class="checkboxer checkboxer-red form-inline">
                                            <input type="checkbox" name="remove_image" id="removeImage" value="1">
                                            <label for="removeImage"><?= __('Sil') ?></label>
                                        
                                    </div>
                                    </div>
                                    
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3"><?= __('Durum') ?></label>
                                <div class="col-md-9">
                                    <div class="radioer form-inline">
                                        <input type="radio" name="active" id="active1" value="1" <?= checked($product->active, 1) ?>>
                                        <label for="active1"><?= __('Aktif') ?></label>
                                    </div>
                                    <div class="radioer form-inline">
                                        <input type="radio" name="active" id="active0" value="0" <?= checked($product->active, 0) ?>>
                                        <label for="active0"><?= __('Pasif') ?></label>
                                    </div>
                                </div>
                            </div>

                            <div class="form-buttons-fixed">
                                <a type="button" href="<?= base_url('products'); ?>" class="btn btn-grey"><?= __('İptal') ?></a>
                                <div class="btn-group pull-right">
                                    <button type="submit" class="btn btn-success" name="dosubmit" data-url="<?= base_url("products/edit/{$product->id}") ?>" ><?= __('Kaydet') ?></button>
                                    <button id="kaydetVeCik" type="submit" class="btn btn-blue" name="dosubmit" data-url="<?= base_url("products/edit/{$product->id}") ?>" data-return="<?= base_url('products'); ?>" ><?= __('Kaydet & Çık') ?></button>
                                </div>
                            </div>

                        </div>
                    </form>
                </div>

            </div>
        </div>
    </div>

</div>

<?php $this->load->view('Products/elements/quicknotesmodal'); ?>
<?php $this->load->view('Products/elements/addStockModal'); ?>

<style type="text/css">
    .fileinput-new {
        position: relative;
    }

    .fileinput-new .checkboxer {
        position: absolute;
        bottom: 0;
        right: 0;
        margin-bottom: 4px;
        margin-right: 0;
    }

    .fileinput-new .checkboxer label {
        margin-bottom: 2px !important;
        margin-right: 0 !important;
    }



</style>

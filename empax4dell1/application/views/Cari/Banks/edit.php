    
<div class="content">

    <div class="page-header full-content">
        <div class="row">
            <div class="col-sm-6">
                <h1> <?= __("Banka Oluştur"); ?><small></small></h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb">
                    <li><a href="<?= base_url(); ?>" ><i class="ion-home"></i></a></li>
                    <li><a href="#" class="active"><?= __("İşlemler"); ?></a></li>
                    <li><a href="<?= base_url('banks'); ?>"><?= __("Banka"); ?></a></li>
                    <li><a href="#" class="active"><?= __("Banka Oluştur"); ?></a></li>
                </ol>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="panel">

                <div class="panel-heading">
                    <div class="panel-title">
                        <h4><?= __("Banka Detayları"); ?></h4>
                    </div>
                </div>

                <div class="panel-body">
                    <form action="#" class="form-horizontal">
                        <div class="form-content">

                            <div class="form-group">
                                <label class="control-label col-md-3"><?= __("Restoran"); ?></label>
                                <div class="col-md-9">
                                    <select name="location_id" data-placeholder="Restoran seç" class="chosen-select">
                                        <option value="0"><?= __("Restoran seç"); ?></option>
                                        <?php foreach ($this->user->locations_array as $location): ?>
                                            <option value="<?php ECHO $location['id']; ?>" <?php ECHO ($location['id']==$bank->location_id)?'selected="selected"':'';?>><?php ECHO $location['title'] ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3"><?= __("İsim"); ?></label>
                                <div class="col-md-9">
                                    <div class="inputer">
                                        <div class="input-wrapper">
                                            <input type="text" name="title" class="form-control" value="<?= $bank->title; ?>">
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3"><?= __("Açılış Bakiyesi"); ?></label>
                                <div class="col-md-9">
                                    <div class="inputer">
                                        <div class="input-wrapper">
                                            <input type="text" class="form-control" name="amount" placeholder="0.00" autocomplete="off" value="<?= $bank->amount; ?>">
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3"><?= __("Açıklama"); ?></label>
                                <div class="col-md-9">
                                    <div class="inputer">
                                        <div class="input-wrapper">
                                            <textarea name="desc" class="form-control js-auto-size valid" <?= $bank->desc; ?>></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3"><?= __("Durum"); ?></label>
                                <div class="col-md-9">
                                    <div class="radioer">
                                        <input type="radio" name="active" id="active1" <?php checked($bank->active, 1); ?> value="1" >
                                        <label for="active1"><?= __("Aktif"); ?></label>
                                    </div>
                                    <div class="radioer">
                                        <input type="radio" name="active" id="active0" <?php checked($bank->active, 0); ?> value="0" >
                                        <label for="active0"><?= __("Pasif"); ?></label>
                                    </div>
                                </div>
                            </div>

                            <div class="form-buttons-fixed">
                                <a type="button" href="<?= base_url('banks'); ?>" class="btn btn-grey"><?= __("İptal"); ?></a>
                                <div class="btn-group pull-right">
                                    <button type="submit" class="btn btn-success" name="dosubmit" data-url="<?= base_url('banks/edit/'.$bank->id);?>" ><?= __("Kaydet"); ?></button>
                                    <button type="submit" class="btn btn-blue" name="dosubmit" data-url="<?= base_url('banks/edit/'.$bank->id);?>" data-return="<?= base_url('banks'); ?>" ><?= __("Kaydet & Çık"); ?></button>
                                </div>
                            </div>

                        </div>
                    </form>
                </div>

            </div>
        </div>
    </div>

</div>

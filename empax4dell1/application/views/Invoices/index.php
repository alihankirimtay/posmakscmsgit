<div class="content">

    <div class="page-header full-content">
        <div class="row">
            <div class="col-sm-6">
                <h1><?= __("Fatura Şablonları"); ?></h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb">
                    <li><a href="<?php ECHO base_url(); ?>" ><i class="ion-home"></i></a></li>
                    <li><a href="#" class="active"><?= __("Fatura Şablonları"); ?></a></li>
                </ol>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="panel">

                <div class="panel-heading">
                    <div class="panel-title">
                        <?= dropdownMenu($this->user->locations_array, $location_id, 'invoices/index/', 'Restoranlar') ?>
                    </div>

                    <div class="panel-title">
                        <h4><?= __("Tüm Fatura Şablonları"); ?></h4>
                        <div class="btn-group pull-right">
                            <a href="<?= base_url('invoices/add') ?>" class="btn btn-success btn-xs"><i class="fa fa-plus"></i> <?= __("Şablon Oluştur"); ?></a>
                        </div>
                    </div>
                </div>

                <div class="panel-body">
                    <div class="overflow-table">
                        <table class="display datatables-basic">
                            <thead>
                                <tr>
                                    <th><?= __("İsim"); ?></th>
                                    <th><?= __("Restoran"); ?></th>
                                    <th><?= __("Oluşturuldu"); ?></th>
                                    <th><?= __("İşlem"); ?></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($invoices as $invoice): ?>
                                    <tr>
                                        <td><?= $invoice->title ?></td>
                                        <td><?= $invoice->location_title ?></td>
                                        <td><?= dateConvert($invoice->created, 'client', dateFormat()) ?></td>
                                        <td>

                                            <?php if($invoice->default == 1): ?>
                                                <a class="btn btn-default btn-xs" href="<?= base_url('invoices/setDefault/'.$invoice->id) ?>" disabled> <i class="fa fa-check"></i> </a>
                                            <?php else: ?>
                                                <a class="btn btn-success btn-xs" href="<?= base_url('invoices/setDefault/'.$invoice->id) ?>"> <i class="fa fa-check"></i> </a>
                                            <?php endif; ?>
                                            
                                            <a class="btn btn-primary btn-xs" href="<?= base_url('invoices/edit/'.$invoice->id) ?>"> <i class="fa fa-pencil"></i> </a>
                                            <button class="btn btn-danger btn-xs" data-url="<?= base_url('invoices/delete/'.$invoice->id) ?>" name="dodelete"> <i class="fa fa-trash-o"></i> </button>
                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                </div>

            </div>
        </div>
    </div>

</div>
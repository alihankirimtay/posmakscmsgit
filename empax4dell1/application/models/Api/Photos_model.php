<?php
	if (!defined('BASEPATH')) exit('No direct script access allowed');
	
	class Photos_Model extends CI_Model {

		const photos = 'photos';

		function __construct()
		{
			parent::__construct();
		}

		function add($values)
		{
			$values['dir']    = FCPATH . 'assets/uploads/photos/' . $values['location'] . '/';
			$values['folder'] = date('Ymd');

			if(!is_dir($values['dir'])) 					mkdir($values['dir']);
			if(!is_dir($values['dir'].$values['folder']))  	mkdir($values['dir'].$values['folder']);
			if(file_exists($values['dir'].$values['folder'].'/'.$values['name'])) {
				
				$values['base64'] = '{REMOVED}';
				api_messageError( logs_(NULL, 'Image already exists.', $values, NULL, $values['location']));
			}

			if (!preg_match("/.(jpg|jpeg|png|gif)$/i", $values['name'])) {
				
				$values['base64'] = '{REMOVED}';
				api_messageError( logs_(NULL, 'Invalid Image extention.', $values, NULL, $values['location']) );
			}

			$base64_decoded   = @base64_decode( $values['base64'] );
			$values['base64'] = '{REMOVED}';
			$info             = @getimagesizefromstring($base64_decoded);
			if(!$info && ($info[0] < 1 && $info[1] < 1 && !$info['mime'])) {
				
				api_messageError( logs_(NULL, 'Invalid Image.', $values, NULL, $values['location']) );
			}

			if( file_put_contents($values['dir'].$values['folder'].'/'.$values['name'], $base64_decoded) ) {

				$data = [
					'location_id' 	=> $values['location'],
					'title' 		=> $values['name'],
					'folder' 		=> $values['folder'],
					'path' 			=> $values['path'],
					'created' 		=> _date(),
				];
				if( $this->db->insert(self::photos, $data) ) {

					$data['id'] = $this->db->insert_id();

					// If possible, create zip-boox album.
					if ($values[ 'location_data' ]->prepaid_limit) {
						$this->createZipBooxAlbum(
							$data + [ 'limit' => $values[ 'location_data' ]->prepaid_limit ]
						);
					}
					
					api_messageOk(logs_(NULL, 'Image has been uploaded.', $data, NULL, $values['location']));
				} else {

					
					api_messageError( logs_(NULL, 'Image Uploaded But Insert Failed.', $data, NULL, $values['location']) );
				}
			} else{

				api_messageError( logs_(NULL, 'Image Upload Failed.', $values, NULL, $values['location']));
			}
		}


		function logs($values)
		{
			$this->db->where('folder', $values['date']);
			$this->db->where('path', $values['path']);
			$photos = $this->db->get(self::photos)->result();
			if(! $photos) {
				
				api_messageError( logs_(NULL, 'No image log found.', $values) );
			}

			$json['count'] = count($photos);

	    	foreach ($photos as $key => $photo) {
	    		$json['items'][] = array(
	    			'id' 	=> $photo->id,
	    			'title' => $photo->path.'\\'.$photo->title,
	    		);
	    	}

	    	api_messageOk($json);
		}

		function ticket($ticket , $location_id){

			return  $this->db->query("
				SELECT 
					location_id , folder , title
				FROM
					photos
				WHERE
					title REGEXP '".'^'.$ticket.'[\.]'."'
				AND
					location_id = $location_id
				ORDER BY
					title ASC
				")->result();
	
		}

		function createZipBooxAlbum($data)
		{
			$ticket = substr($data[ 'title' ], 0, stripos($data[ 'title' ], '_'));
			$folder = 'assets/uploads/photos/';
			$folder_pure   = 'assets/uploads/photos/';
			$images = NULL;

			$photos = $this->db->query("SELECT location_id , folder , title, id
				FROM photos
				WHERE title REGEXP '^{$ticket}[\._]'
				AND location_id = {$data[ 'location_id' ]}
				ORDER BY id DESC LIMIT {$data[ 'limit' ]}")
			->result();

			if (count($photos) == $data[ 'limit' ]) {


				$data[ 'inputs' ] = [
		            'ticket'   => $ticket,
		            'location' => $data[ 'location_id' ],
		            'date'     => $data[ 'created' ]
		        ];

		        // Get Images
		        $data[ 'images' ] = NULL;
		        foreach ($photos as $photo) {
		            
		            $file_folder = $folder . $data[ 'location_id' ] . '/' . $photo->folder . '/';

		            if (!empty($photo->title) && file_exists(FCPATH . $file_folder . $photo->title)) {
		                //$images[ $photo->title ] = $file_folder . $photo->title;//@base64_encode( file_get_contents($file_folder . $photo->title) );
		                $data[ 'images' ][ $photo->id ] = [
		                	'name'  => $photo->title,
		                	'image' => $folder_pure . $data[ 'location_id' ] . '/' . $photo->folder . '/' . $photo->title
		                ];
		            } else {

		            	logs_(NULL, 'Zip-boox: Image not found. File: ' . $file_folder . $photo->title, $data, NULL, $data[ 'location_id' ]);
		            }
		        }

		        $data[ 'inputs' ][ 'images' ] = $data[ 'images' ];


				$ch = curl_init();

				curl_setopt($ch, CURLOPT_URL, zipBooxURL() . 'api/createAccountByStpReport');
				curl_setopt($ch, CURLOPT_POST, 1);
				curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data[ 'inputs' ]));
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
				$server_output = curl_exec ($ch);
				curl_close($ch);

				$zip_boox = json_decode($server_output, TRUE);

				logs_(NULL, 'Zip-boox: ' . $zip_boox[ 'message' ], $zip_boox, NULL, $data[ 'location_id' ]);
			}
		}
	}    
	?>

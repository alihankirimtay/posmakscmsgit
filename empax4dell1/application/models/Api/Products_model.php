<?php
	if (!defined('BASEPATH')) exit('No direct script access allowed');
	
	class Products_Model extends CI_Model {
	
	    const products = 'products';
	    const location_has_products = 'location_has_products';

	    function __construct()
	    {
	    	parent::__construct();
	    }

	    function index($location)
	    {
	    	$this->db->select('P.* , L.price');
	    	$this->db->where('P.active', 1);
	    	$this->db->where('L.location_id', $location);
	    	$this->db->join(self::location_has_products.' AS L', 'L.product_id = P.id');
	    	$products = $this->db->get(self::products.' AS P')->result();

	    	if(!$products) api_messageError('No product found.');

	    	$json['count'] = count($products);

	    	foreach ($products as $key => $product) {
	    		$json['items'][] = array(
	    			'id' 	=> $product->id,
	    			'title' => $product->title,
	    			'desc'  => $product->desc,
					'type'	=> $product->type,
	    			'price' => $product->price,
					'active'=> $product->active,
	    		);
	    	}

	    	api_messageOk($json);
	    }
	}    
?>

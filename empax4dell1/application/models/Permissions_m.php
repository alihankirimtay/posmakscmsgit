<?php
	if (!defined('BASEPATH')) exit('No direct script access allowed');
	
	class Permissions_M extends M_Model {
	
	    public $table = 'permissions';

	    function __construct()
	    {
            $this->before_create = [];
            $this->before_update = [];
            $this->before_get = ['_whereNonDeleted'];

            $this->has_many_pivot['roles'] = [
                'foreign_model'     =>'Roles_M',
                'pivot_table'       =>'role_has_permissions',
                'local_key'         =>'id',
                'pivot_local_key'   =>'permission_id',
                'pivot_foreign_key' =>'role_id',
                'foreign_key'       =>'id',
            ];

            $this->rules = [
                'insert' => [
                    // 'title' => [
                    //     'field' => 'title',
                    //     'label' => 'Başlık',
                    //     'rules' => 'required|trim|xss_clean',
                    // ],
                    // 'alias' => [
                    //     'field' => 'alias',
                    //     'label' => 'Başlık',
                    //     'rules' => 'trim|alpha_dash',
                    // ],
                    // 'editable' => [
                    //     'field' => 'editable',
                    //     'label' => 'editable',
                    //     'rules' => 'required|trim|in_list[1,0]',
                    // ],
                    // 'active' => [
                    //     'field' => 'active',
                    //     'label' => 'Durum',
                    //     'rules' => 'required|trim|in_list[1,0]',
                    // ],
                ],
                'update' => [
                    // 'title' => [
                    //     'field' => 'title',
                    //     'label' => 'Başlık',
                    //     'rules' => 'required|trim|xss_clean',
                    // ],
                    // 'alias' => [
                    //     'field' => 'alias',
                    //     'label' => 'Başlık',
                    //     'rules' => 'trim|alpha_dash',
                    // ],
                    // 'editable' => [
                    //     'field' => 'editable',
                    //     'label' => 'editable',
                    //     'rules' => 'required|trim|in_list[1,0]',
                    // ],
                    // 'active' => [
                    //     'field' => 'active',
                    //     'label' => 'Durum',
                    //     'rules' => 'required|trim|in_list[1,0]',
                    // ],
                ]
            ];

	    	parent::__construct();
	    }

        public function _whereNonDeleted()
        {
            $this->where('active !=', 3);
        }
	}    
?>

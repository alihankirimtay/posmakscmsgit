<?php
	if (!defined('BASEPATH')) exit('No direct script access allowed');
	
	class Expenses_Model extends CI_Model {
	
	    public $table = 'expenses';

	    function __construct()
	    {
            $tax_keys = implode(',', array_keys($this->taxes));

            $this->rules = [
                'insert' => [
                    'location_id' => [
                        'field' => 'location_id',
                        'label' => 'Restoran',
                        'rules' => 'required|trim|search_table[locations.id]',
                    ],
                    'user_id' => [
                        'field' => 'user_id',
                        'label' => 'user_id',
                        'rules' => 'required|trim|search_table[users.id]',
                    ],
                    'expense_type_id' => [
                        'field' => 'expense_type_id',
                        'label' => 'Kategori',
                        'rules' => 'required|trim|search_table[expense_types.id]',
                    ],
                    'customer_id' => [
                        'field' => 'customer_id',
                        'label' => 'Tedarikçi',
                        'rules' => 'required|trim|search_table[customers.id]',
                    ],
                    'transaction_id' => [
                        'field' => 'transaction_id',
                        'label' => 'Transaction',
                        'rules' => 'trim|search_table[transactions.id]',
                    ],
                    'title' => [
                        'field' => 'title',
                        'label' => 'Gider açıklaması',
                        'rules' => 'required|trim|xss_clean',
                    ],
                    'desc' => [
                        'field' => 'desc',
                        'label' => 'Açıklama',
                        'rules' => 'trim|xss_clean',
                    ],
                    'amount' => [
                        'field' => 'amount',
                        'label' => 'Fiyat',
                        'rules' => 'required|trim|is_money',
                    ],
                    'tax' => [
                        'field' => 'tax',
                        'label' => 'Ölçü birimi',
                        'rules' => 'required|trim|in_list['.$tax_keys.']',
                    ],
                    'payment_date' => [
                        'field' => 'payment_date',
                        'label' => 'Ödeme tarihi',
                        'rules' => 'required|trim|is_date['.dateFormat().']',
                    ],
                    'receipt_date' => [
                        'field' => 'receipt_date',
                        'label' => 'Fiş/Fatura tarihi',
                        'rules' => 'required|trim|is_date['.dateFormat().']',
                    ],
                    'has_stock' => [
                        'field' => 'has_stock',
                        'label' => 'has_stock',
                        'rules' => 'required|trim|in_list[1,0]',
                    ],
                    'is_paid' => [
                        'field' => 'is_paid',
                        'label' => 'is_paid',
                        'rules' => 'required|trim|in_list[1,0]',
                    ],
                    'active' => [
                        'field' => 'active',
                        'label' => 'Durum',
                        'rules' => 'required|trim|in_list[1,0]',
                    ],
                ],
                'update' => [
                    'location_id' => [
                        'field' => 'location_id',
                        'label' => 'Restoran',
                        'rules' => 'required|trim|search_table[locations.id]',
                    ],
                    'user_id' => [
                        'field' => 'user_id',
                        'label' => 'user_id',
                        'rules' => 'required|trim|search_table[users.id]',
                    ],
                    'expense_type_id' => [
                        'field' => 'expense_type_id',
                        'label' => 'Kategori',
                        'rules' => 'required|trim|search_table[expense_types.id]',
                    ],
                    'customer_id' => [
                        'field' => 'customer_id',
                        'label' => 'Tedarikçi',
                        'rules' => 'required|trim|search_table[customers.id]',
                    ],
                    'transaction_id' => [
                        'field' => 'transaction_id',
                        'label' => 'Transaction',
                        'rules' => 'trim|search_table[transactions.id]',
                    ],
                    'title' => [
                        'field' => 'title',
                        'label' => 'Gider açıklaması',
                        'rules' => 'required|trim|xss_clean',
                    ],
                    'desc' => [
                        'field' => 'desc',
                        'label' => 'Açıklama',
                        'rules' => 'trim|xss_clean',
                    ],
                    'amount' => [
                        'field' => 'amount',
                        'label' => 'Fiyat',
                        'rules' => 'required|trim|is_money',
                    ],
                    'tax' => [
                        'field' => 'tax',
                        'label' => 'Ölçü birimi',
                        'rules' => 'required|trim|in_list['.$tax_keys.']',
                    ],
                    'payment_date' => [
                        'field' => 'payment_date',
                        'label' => 'Ödeme tarihi',
                        'rules' => 'required|trim|is_date['.dateFormat().']',
                    ],
                    'receipt_date' => [
                        'field' => 'receipt_date',
                        'label' => 'Fiş/Fatura tarihi',
                        'rules' => 'required|trim|is_date['.dateFormat().']',
                    ],
                    'has_stock' => [
                        'field' => 'has_stock',
                        'label' => 'has_stock',
                        'rules' => 'required|trim|in_list[1,0]',
                    ],
                    'is_paid' => [
                        'field' => 'is_paid',
                        'label' => 'is_paid',
                        'rules' => 'required|trim|in_list[1,0]',
                    ],
                    'active' => [
                        'field' => 'active',
                        'label' => 'Durum',
                        'rules' => 'required|trim|in_list[1,0]',
                    ],
                ]
            ];

	    	parent::__construct();
	    }

        public function select($select)
        {
            $this->db->select($select);
            return $this;
        }

        public function getAll($conditions = [])
        {
            return $this->_get($conditions)->result();
        }

        public function get($conditions = [])
        {
            return $this->_get($conditions)->row();
        }

        private function _get($conditions = [])
        {
            if ($conditions) {
                $this->db->where($conditions);
            }
            return $this->db->where('active !=', 3)->get($this->table);
        }

        public function findOrFail($conditions = [])
        {
            if (!is_array($conditions)) {
                $conditions = [
                    'id' => (int) $conditions
                ];
            }

            if (!$data = $this->get($conditions)) {
                if ($this->input->is_ajax_request()) {
                    messageAJAX('error', 'İçerik bulunamadı.');
                }

                show_404();
            }

            return $data;
        }

        public function insert()
        {
            $this->form_validation->set_rules($this->rules['insert']);
            if($this->form_validation->run()) {
                
                $data['created'] = date('Y-m-d H:i:s');
                foreach ($this->rules['insert'] as $input) {
                    $data[$input['field']] = $this->input->post($input['field']);
                }
                $data['amount'] = $this->_setAmount();
                $data['payment_date'] = $this->_setPaymentDate();
                $data['receipt_date'] = $this->_setReceiptDate();

                $this->db->insert($this->table, $data);

                return $this->db->insert_id();
            }
        }

        public function update($conditions = [])
        {
            $this->form_validation->set_rules($this->rules['update']);
            if($this->form_validation->run()) {
                
                $data['modified'] = date('Y-m-d H:i:s');
                foreach ($this->rules['update'] as $input) {
                    $data[$input['field']] = $this->input->post($input['field']);
                }
                $data['amount'] = $this->_setAmount();
                $data['payment_date'] = $this->_setPaymentDate();
                $data['receipt_date'] = $this->_setReceiptDate();


               $this->db->where($conditions);
               $this->db->update($this->table, $data);

                return true;
            }
        
        }    

        public function delete($conditions = [])
        {
            $this->db->where($conditions);
            $this->db->update($this->table,[
                'active' => 3
            ]);
        }    


        private function _setPaymentDate()
        {
            return dateConvert2($this->input->post('payment_date'), 'server', dateFormat());
        }

        private function _setReceiptDate()
        {
            return dateConvert2($this->input->post('receipt_date'), 'server', dateFormat());
        }

        private function _setAmount()
        {
            return doubleval(str_replace(',', '', $this->input->post('amount')));
        }



        public function handleStocks()
        {
            if (!$stocks = (Array) $this->input->post('stocks')) {
                messageAJAX('error', 'En az bir adet Gider Kalemi eklemelisiniz.');
            }

            $unit_keys = implode(',', array_keys($this->units));
            
            $data = [
                'stocks' => [
                    'insert' => [],
                    'update' => [],
                    'delete' => [],
                ],
                'stock_contents' => [
                    'insert' => [],
                    'delete' => [],
                ],
                'expense' => [
                    'amount' => 0
                ]
            ];
            $total_stock = 0;

            foreach ($stocks as $key => $stock) {
               
                // Stock
                $this->form_validation->set_rules("stocks[{$key}][title]", 'Başlık', 'required|trim|xss_clean');
                $this->form_validation->set_rules("stocks[{$key}][unit]", 'Ölçü birim', "required|trim|in_list[{$unit_keys}]");
                // StockContent
                $this->form_validation->set_rules("stocks[{$key}][stock_content][quantity]", 'Miktar', 'required|trim|is_decimal');
                $this->form_validation->set_rules("stocks[{$key}][stock_content][price]", 'Birim Fiyatı', 'required|trim|is_money');
                $this->form_validation->set_rules("stocks[{$key}][stock_content][tax]", 'Vergi Oranı', 'required|trim|is_decimal');
                if (!$this->form_validation->run()) {
                    messageAJAX('error', validation_errors());
                }

                $stock_id = (int) @$stock['id'];
                $stock_content_id = (int) @$stock['stock_content']['id'];

                $title = $_POST['stocks'][$key]['title'];
                $unit = $_POST['stocks'][$key]['unit'];
                $quantity = str_replace(',', '', $_POST['stocks'][$key]['stock_content']['quantity']);
                $price = str_replace(',', '', $_POST['stocks'][$key]['stock_content']['price']);
                $tax = str_replace(',', '', $_POST['stocks'][$key]['stock_content']['tax']);

                // Update OR Delete Stock
                if ($stock_id) {

                    $total_stock++;

                    if (!$stock_data = $this->Stocks->get(['id' => $stock_id])) {
                        messageAJAX('error', 'Stok bulunamadı: ' . $title);
                    }
                    
                    if ($stock_content_id) {
                        $stock_content = $this->StockContents->get([
                            'id' => $stock_content_id,
                            'stock_id' => $stock_id
                        ]);
                    }

                    // Delete Stock
                    if (isset($stock['deleted'])) {
                        if (isset($stock_content)) {     

                            $total_stock--;                
                        
                            $data['stocks']['delete'][$key] = [
                                'id' => $stock_id,
                                'location_id' => $this->input->post('location_id'),
                                'quantity' => $stock_content->quantity,
                            ];

                            $data['stock_contents']['delete'][$key] = [
                                'id' => $stock_content_id,
                                'stock_id' => $stock_id,
                                'expense_id' => null,
                            ];

                            $data['expense']['amount'] -= ($price + $price * $tax / 100) * $quantity;
                        }
                    }

                    // Insert StockContent to an exist Stok.
                    elseif (!$stock_content_id) {

                        $data['stocks']['update'][$key] = [
                            'id' => $stock_id,
                            'location_id' => $this->input->post('location_id'),
                            'quantity' => $quantity,
                        ];

                        $data['stock_contents']['insert'][$key] = [
                            'id' => null,
                            'user_id' => $this->user->id,
                            'stock_id' => $stock_id,
                            'expense_id' => null,
                            'quantity' => $quantity,
                            'price' => $price,
                            'tax' => $tax,
                            'desc' => null,
                            'active' => 1,
                        ];
                    }

                    $data['expense']['amount'] += ($price + $price * $tax / 100) * $quantity;
                }

                // Insert new Stock
                else {

                    $total_stock++;                

                    $data['stocks']['insert'][$key] = [
                        'id' => null,
                        'location_id' => $this->input->post('location_id'),
                        'title' => $title,
                        'desc' => null,
                        'quantity' => $quantity,
                        'sub_limit' => 0,
                        'unit' => $unit,
                        'active' => 1
                    ];

                    $data['stock_contents']['insert'][$key] = [
                        'id' => null,
                        'user_id' => $this->user->id,
                        'stock_id' => null,
                        'expense_id' => null,
                        'quantity' => $quantity,
                        'price' => $price,
                        'tax' => $tax,
                        'desc' => null,
                        'active' => 1,
                    ];

                    $data['expense']['amount'] += ($price + $price * $tax / 100) * $quantity;
                }
            }

            $data['expense']['amount'] = round($data['expense']['amount'], 2);

            if ($total_stock == 0) {
                messageAJAX('error', 'En az bir adet Gider Kalemi eklemelisiniz.');
            }

            return $data;
        }


        public function findStocksWithContents($conditions = [])
        {
            $stocks = [];
            $stock_contents = $this->StockContents->getAll($conditions);

            if ($stock_contents) {

                $this->db->where_in('id', array_map(function ($row) {
                    return $row->stock_id;
                }, $stock_contents));
                $stocks = $this->Stocks->getAll();

                foreach ($stocks as &$stock) {

                    $stock->stock_contents = [];

                    foreach ($stock_contents as $stock_content) {
                        if ($stock_content->stock_id == $stock->id) {
                            $stock->stock_contents[] = $stock_content;
                        }
                    }
                }
            }

            return $stocks;
        }
	}    
?>

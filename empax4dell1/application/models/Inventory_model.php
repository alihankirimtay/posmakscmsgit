<?php
	if (!defined('BASEPATH')) exit('No direct script access allowed');
	
	class Inventory_Model extends CI_Model {
	
	    const inventory = 'inventory';

	    function __construct()
	    {
	    	parent::__construct();
	    }

	    function index($location)
	    {
	    	if($location && !in_array($location, $this->user->locations)) error_('access');

	    	$this->db->select('I.*, (SELECT title FROM locations WHERE id = I.location_id LIMIT 1) AS location');
	    	if($location) {

	    		$this->db->where('I.location_id', $location);
	    	}
    		else {

    			$this->db->where_in('I.location_id', $this->user->locations);
    		}

	    	return $this->db->get(self::inventory.' I')->result();	    	
	    }

	    function add()
	    {
	    	$values['title']    	= $this->input->post('title', TRUE);
            $values['desc']     	= $this->input->post('desc', TRUE);
            $values['barcode']     	= $this->input->post('barcode', TRUE);
            $values['quantity']     = (int) $this->input->post('quantity');
            $values['price']        = doubleval(str_replace(',', '', $this->input->post('price')));
            $values['warranty_exp'] = $this->input->post('warranty_exp') ? dateReFormat($this->input->post('warranty_exp'), dateFormat(), 'Y-m-d H:i:s') : null;
            $values['active']   	= $this->input->post('active');
            $values['location_id']	= (int) $this->input->post('location');
            $values['created']  	= _date();

            if($this->user->locations && !in_array($values['location_id'], $this->user->locations)) {

            	messageAJAX('error', logs_($this->user->id, 'Geçersiz lokasyon. ', $values));
            }

            if( $this->db->insert(self::inventory, $values) ) {

                $values['id'] = $this->db->insert_id();
            	messageAJAX('success', logs_($this->user->id, 'Demirbaş oluşturuld. ', $values));
            }
	    }

	    function edit($id, $data)
	    {
    		$values['title']    	= $this->input->post('title', TRUE);
            $values['desc']     	= $this->input->post('desc', TRUE);
            $values['barcode']     	= $this->input->post('barcode', TRUE);
            $values['quantity']     = (int) $this->input->post('quantity');
            $values['price']        = doubleval(str_replace(',', '', $this->input->post('price')));
            $values['warranty_exp'] = $this->input->post('warranty_exp') ? dateReFormat($this->input->post('warranty_exp'), dateFormat(), 'Y-m-d H:i:s') : null;
            $values['active']   	= $this->input->post('active');
            $values['location_id']	= (int) $this->input->post('location');
            $values['modified'] = _date();

            if($this->user->locations && !in_array($values['location_id'], $this->user->locations)) {

            	messageAJAX('error', logs_($this->user->id, 'Geçersiz lokasyon. ', ['id'=>$id]+$values));
            }

       		$this->db->where('id', $id);
            if( $this->db->update(self::inventory, $values) ) {

            	$data->updated_at = date('Y-m-d H:i:s');
            	$this->db->insert('inventory_histories', ['inventory_id' => $id, 'history' => json_encode($data)]);

            	messageAJAX('success', logs_($this->user->id, 'Demirbaş güncellendi. ', ['id'=>$id]+$values));
            }
	    }

	    function delete($id, $data)
	    {
	    	$this->db->where('id', $id);
            if( $this->db->update(self::inventory, ['active' => 3]) ) {

            	messageAJAX('success', logs_($this->user->id, 'Demirbaş silindi. ', $data));
            }
	    }
	}    
?>

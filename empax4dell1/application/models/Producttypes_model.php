<?php
	if (!defined('BASEPATH')) exit('No direct script access allowed');
	
	class ProductTypes_Model extends CI_Model {
	
        const producttypes = 'product_types';

	    function __construct()
	    {
	    	parent::__construct();
	    }


        function index()
        {
            return $this->db->where('active', 1)->get(self::producttypes)->result();
        }

	    function add()
	    {
            if ($this->input->is_ajax_request()) {
                
                $values['parent_id'] = $this->input->post('parent_id');
                $values['title']    = $this->input->post('title', TRUE);
                $values['desc']     = $this->input->post('desc', TRUE);
                $values['sort']     = $this->input->post('sort', TRUE);
                $values['active']   = $this->input->post('active');
                $values['created']  = _date();

                $values['parent_id'] = $values['parent_id'] ? $values['parent_id'] : null;

                if (isset($_FILES['category_image'])) {
                    $values['image'] = $this->uploadCategoryImage();
                }

                if( $this->db->insert(self::producttypes, $values) ){

                    $id = $this->db->insert_id();

                    messageAJAX( 'success', logs_($this->user->id, 'Ürün türü oluşturuldu.', ['id'=>$id]+$values) );
                }

            }
	    }

        function view($id)
        {
            pr($id,1);
        }

        function edit($id)
        {
            $id = (int) $id;

            if ($this->input->is_ajax_request()) {

                $values['parent_id'] = $this->input->post('parent_id');
                $values['title']    = $this->input->post('title', TRUE);
                $values['desc']     = $this->input->post('desc', TRUE);
                $values['sort']     = $this->input->post('sort', TRUE);
                $values['active']   = $this->input->post('active');
                $values['modified'] = _date();

                $values['parent_id'] = $values['parent_id'] ? $values['parent_id'] : null;

                if (isset($_FILES['category_image'])) {
                    $values['image'] = $this->uploadCategoryImage();
                }

                $this->db->where('id', $id);
                if( $this->db->update(self::producttypes, $values) ){

                    messageAJAX( 'success', logs_($this->user->id, 'Ürün türü güncellendi.', $values) );
                }
                
            } else {
                
                
            }
            
        }

        // function delete($id, $data)
        // {
        //     $id = (int) $id;

        //     $this->db->set('active', 3);
        //     $this->db->where('id', $id);
        //     if( $this->db->update(self::producttypes) ) {

        //         messageAJAX( 'success', logs_($this->user->id, 'Ürün türü silindi. ', $data) );
        //     }
        // }

        public function uploadCategoryImage()
        {
            $image_path  = 'assets/uploads/categories/';

            @mkdir(FCPATH . $image_path);

            $this->load->library('upload', [
                'encrypt_name' => true,
                'upload_path' => FCPATH . $image_path,
                'allowed_types' => 'gif|jpg|jpeg|png',
                'max_size' => 2048, // 2Mb
            ]);

            if (!$this->upload->do_upload('category_image')) {
                messageAJAX('error', $this->upload->display_errors());
            }
            
            return $image_path . $this->upload->data()['file_name'];
        }
	}    
?>

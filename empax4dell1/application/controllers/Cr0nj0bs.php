<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Cr0nj0bs extends MY_Controller {

    public function __construct()
    {
        parent::__construct();
    }

    public function index() { }

    public function backUpDb()
    {
        $this->load->helper('file');
        $this->load->dbutil();

        $date = date('YmdHis');
        $date_days_ago = strtotime('-7 days');
        $filename = url_title($this->website->name) .  '_' . $date;
        $dir = FCPATH . 'assets/backups';
        $zip = $dir . '/' . $date . '.zip';

        
        $backup = $this->dbutil->backup([
            'format'   => 'zip',
            'filename' => $filename . '.sql',
        ]);

        // WRITE FILE
        @mkdir($dir);
        write_file($zip, $backup);

        // EMAIL TO..
        $config['protocol']  = 'smtp';
        $config['smtp_host'] = $this->website->smtp_host;
        $config['smtp_port'] = $this->website->smtp_port;
        $config['smtp_user'] = $this->website->smtp_user;
        $config['smtp_pass'] = $this->website->smtp_pass;
        $config['mailtype']  = 'html';
        $config['charset']   = 'iso-8859-1';
        $this->load->library('email', $config);
        $this->email->set_newline("\r\n");
        $this->email->from('backup@posmaks.com');
        $this->email->to('backup@posmaks.com');
        $this->email->subject($this->website->name . ' - SQL');
        $this->email->message($filename);
        $this->email->attach($zip);
        if (!$this->email->send()) {
            @file_put_contents($dir . '/' . 'BACKUPLOGS', date('Y-m-d H:i:s').' - Failed to send email.' . "\n", FILE_APPEND);
        }


        // Delete older files then seven days.
        if ($files = get_filenames($dir)) {
            foreach ($files as $f) {

                if (!preg_match('/^(\d{4})(\d{2})(\d{2})(\d{2})(\d{2})(\d{2})\.zip$/', $f)) return;

                $file_date = preg_replace('/\\.[^.\\s]{3,4}$/', '', $f);
                $formated_date = date_create_from_format('YmdHis', $file_date);
                $formated_date = date_format($formated_date, 'Y-m-d H:i:s');
                $formated_date = strtotime($formated_date);

                if ((int) $formated_date < (int) $date_days_ago) {
                    
                    @unlink($dir . '/' . $f);
                }
            }
        }
    }

    public function autoAlbums()
    {
        $this->load->library('curl');

        $folder        = FCPATH . 'assets/uploads/photos/';
        $folder_pure   = 'assets/uploads/photos/';

        // Find auto-album function is enabled locations.
        $locations = $this->db
        ->select('id, autoalbum_time, dtz')
        ->where([
            'autoalbum_active' => 1,
            'prepaid_limit'    => 0,
            'active'           => 1
        ])->get('locations')->result_array();
        if (!count($locations)) {
            return false;
        }

        // Find which location has auto-album time is equal at the moment.
        list($server_hour, $server_minute) = array_map('intval', explode(':', date('H:i')));
        foreach ($locations as $key => &$location) {
            
            $time = dateConvert3($location['dtz'], 'H:i:s', $location['autoalbum_time'], 'UTC', 'H:i');
            list($hour, $minute) = array_map('intval', explode(':', $time));

            if ($hour === $server_hour && $minute === $server_minute) {
                $location['start_date'] = dateConvert3($location['dtz'], 'Y-m-d H:i:s', date('Y-m-d 03:00:00'), 'UTC', 'Y-m-d H:i:s');
                $location['end_date']   = dateConvert3($location['dtz'], 'Y-m-d H:i:s', date('Y-m-d H:i:s', strtotime(date('Y-m-d 02:59:59') . ' +24 hours')), 'UTC', 'Y-m-d H:i:s');
            } else {
                unset($locations[$key]);
            }
        }
        if (!count($locations)) {
            return false;
        }

            
        // Find unique tickets in locations's sales
        foreach ($locations as $location) {

            $sales = $this->db
            ->select('id, ticket, payment_date')
            ->where([
                'location_id'     => $location['id'],
                'payment_date >=' => $location['start_date'],
                'payment_date <=' => $location['end_date'],
                'return'          => 0,
                'active'          => 1,
                'ticket !='       => NULL
            ])->group_by('ticket')
            ->get('sales')->result_array();


            if ($sales) {
                
                foreach ($sales as $sale) {
                    
                    $photos = $this->db->query('SELECT * FROM
                                        (
                                            SELECT id, title, location_id, folder
                                            FROM photos
                                            WHERE location_id = ?
                                            AND title REGEXP ?
                                            AND created BETWEEN ? AND ?
                                            ORDER BY id DESC
                                        ) AS tickets
                                        ORDER BY id ASC', [
                                            $location['id'],
                                            '^'.$sale['ticket'].'[\._]',
                                            $location['start_date'],
                                            $location['end_date']
                                        ])->result();

                    if ($photos) {
                       
                        // Preapare Images 
                        $data[ 'inputs' ] = [
                            'ticket'   => $sale['ticket'],
                            'location' => $location['id'],
                            'date'     => $sale['payment_date']
                        ];

                        // Get Images
                        $data[ 'images' ] = NULL;
                        foreach ($photos as $photo) {
                            
                            $file_folder = $folder . $location['id'] . '/' . $photo->folder . '/';

                            if (!empty($photo->title) && file_exists($file_folder . $photo->title)) {
                                $data[ 'images' ][ $photo->id ] = [
                                    'name'  => $photo->title,
                                    'image' => $folder_pure . $location['id'] . '/' . $photo->folder . '/' . $photo->title
                                ];
                            }
                        }


                        // Send Zip-Boox
                        if ($data[ 'images' ]) {

                            $data[ 'inputs' ][ 'images' ] = $data[ 'images' ];
                            
                            $this->curl->simple_post(zipBooxURL() . 'api/createAccountByStpReport', $data[ 'inputs' ]);
                        }
                    }
                }

            }
        }
    }
}
?>

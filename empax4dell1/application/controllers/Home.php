<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Home extends Auth_Controller {

    function __construct()
    {
        parent::__construct();
    }

    function index()
    {
        $count = count($this->user->locations_array);
        $show_modal = 0;
        $units = json_encode($this->units);
        $online_quick_login_url = $this->input->get("online");
        $tutorialize = $this->input->get("tutorialize");

        if($count == 1){
            $id = $this->user->locations[0];
            $title = $this->user->locations_array[$id]['title'];
            
            if($title == "POSMAKS") {
                $show_modal = true;
            }

        }

        
        $this->theme_plugin = [
            'css' => ['panel/css/wizard.css'],
            'js' => array('panel/js/wizard.js','panel/js/view/home.js'),
            'start' => "
                Home.onlineLogin('${online_quick_login_url}');
                Home.popup('${online_quick_login_url}');
            "
        ];

        // tutorialize'den geliniyorsa wizard gösterilmeyecek.
        if (!$tutorialize) {
            $this->theme_plugin['start'] .= "
                Wizard.init({
                    show_modal : {$show_modal},
                    units : {$units},
                });";
        }
        
        $this->load->template('home/index');
        
        
    }
}
?>

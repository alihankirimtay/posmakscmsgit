<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Inventory extends Auth_Controller {

    function __construct()
    {
        parent::__construct();
        $this->check_auth();
        $this->load->model('Inventory_model');
        $this->load->model('Locations_model');
    }

    function index($location = NULL)
    {
        $this->theme_plugin = ['start' => 'TablesDataTables.init();'];

        $data['current_location'] = (int) $location;

        if(!$data['current_location']) {
            $data['current_location'] = $this->user->locations[0];
        }

        $this->db->select('(SELECT id FROM inventory_histories WHERE id = I.id LIMIT 1) as history');
        $this->db->where('active !=', 3);
        $data['items'] = $this->Inventory_model->index($data['current_location']);
        
    	$this->load->template('Inventory/index', $data);
    }

    function add()
    {
        $data['locations'] = $this->Locations_model->locationsByUser()->result_array();

        if ($this->input->is_ajax_request()) {
            
            $this->load->library('form_validation');
            $this->form_validation->set_rules('title', 'İsim', 'trim|required|min_length[3]|max_length[255]');
            $this->form_validation->set_rules('desc', 'Açıklama', 'trim');
            $this->form_validation->set_rules('barcode', 'Seri/Barkod numarası', 'trim');
            $this->form_validation->set_rules('quantity', 'Adet', 'trim|is_natural_no_zero');
            $this->form_validation->set_rules('price', 'Fiyat', 'trim|is_money');
            $this->form_validation->set_rules('warranty_exp', 'Garanti Bitiş', 'trim|is_date['.dateFormat().']');
            $this->form_validation->set_rules('active', 'Durum', 'trim|required|integer|in_list[0,1]');
            $this->form_validation->set_rules('location', 'Restoran', 'trim|integer|required');

            if($this->form_validation->run()) {

                $this->Inventory_model->add($data['locations']);
            }
            
            messageAJAX('error', validation_errors());

        } else {

            $this->theme_plugin['start'] = 'MyFunction.datetimepicker("'.dateFormat(TRUE).'");';

            $this->load->template('Inventory/add', $data);
        }       
    }

    function edit($id = NULL)
    {
        $id = (int) $id;

        if(!$data['item'] = $this->exists('inventory', "id = $id AND active != 3", '*, (SELECT title FROM locations WHERE id = inventory.location_id LIMIT 1) as location')) {

            show_404();
        }

        $data['locations'] = $this->Locations_model->locationsByUser()->result_array();

        if ($this->input->is_ajax_request()) {
            
            $this->load->library('form_validation');
            $this->form_validation->set_rules('title', 'İsim', 'trim|required|min_length[2]|max_length[45]');
            $this->form_validation->set_rules('desc', 'Açıklama', 'trim');
            $this->form_validation->set_rules('active', 'Durum', 'trim|required|integer|in_list[0,1]');
            $this->form_validation->set_rules('location', 'Restoran', 'trim|integer|required');

            if($this->form_validation->run()) {

                $this->Inventory_model->edit($id, $data['item']);
            }
            
            messageAJAX('error', validation_errors());

        } else {

            $this->theme_plugin['start'] = 'MyFunction.datetimepicker("'.dateFormat(TRUE).'");';

            $this->load->template('Inventory/edit', $data);
        }      
    }

    function delete($id = NULL)
    {
        $id = (int) $id;

        if(!$data = $this->exists('inventory', "id = $id AND active != 3", '*')) {

            show_404();
        }

        $this->Inventory_model->delete($id, $data);
    }

    public function history($id = null)
    {
        $this->theme_plugin = ['start' => 'TablesDataTables.init();'];

        if(!$data['item'] = $this->exists('inventory', "id = $id AND active != 3", '*, (SELECT title FROM locations WHERE id = inventory.location_id LIMIT 1) as location')) {
            show_404();
        }

        $data['histories'] = $this->db->order_by('id', 'desc')->where(['inventory_id' => $id])->get('inventory_histories')->result();

        $this->load->template('Inventory/history', $data);
    }
}
?>
<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Login extends MY_Controller {

    function __construct()
    {
        parent::__construct();

        $this->load->model('Login_model');
        $this->load->model('Users_M');
        $this->load->model('Memberships_model', 'Memberships');

    }

    function index()
    {
        $online_quick_logout_url = $this->input->get("online");
        if($online_quick_logout_url)
        {
            //pr($online_quick_logout_url);
            $online_quick_logout_url = "$('body').append(\"<a id='link' target='_blank' href='{$online_quick_logout_url}'>&nbsp;</a>\");
            $('#link')[0].click();";
        }


        if($this->session->userdata('loggedin')) {

            $device = $this->input->get('device');

            if ($device == "garson") {
                redirect('mobilpos?device=garson', 'refresh');
            } else if ($device == "tablet") {
                redirect('mobilpos/tablet?device=tablet', 'refresh');
            } else {
                redirect(base_url());
            }

        }

        $mobile=$this->agent->is_mobile();

        $data = NULL;

        $this->page_title = __('Kullanıcı Girişi');

        if( $this->input->post() ) {

            $this->DDosAttack();

            $this->load->library('form_validation');
            $this->form_validation->set_rules('name', 'Kullanıcı Adı/Eposta', 'trim|required|min_length[4]|max_length[32]');
            $this->form_validation->set_rules('password', 'Şifre', 'trim|required|min_length[6]|max_length[32]');


            
            if($this->form_validation->run()) {

                $data = $this->Login_model->index();

            } else{

                $data['error'] = validation_errors();

            }
        }

        if($mobile){
            $this->load->view('Login/mobil', $data);
        } else {
            $this->theme_plugin['start'] = $online_quick_logout_url;
            $this->load->view('Login/index', $data);
        }
    }

    function logout()
    {
        $url_query = [];


        $device = $this->input->get('device');
        if ($device)
        {
            $url_query['device'] = $device;
        }

        $user_id = $this->session->userdata('loggedin');
        if($user_id)
        {
            $user = $this->db->get_where('users',array('id' => $user_id))->row();
            if($user && $user->role_id == 1)
            {
                $url_query['online'] = base_url('Online/Auth/logout?logoutBySystem=1');
            }
        }
            
        

        // Remove POS Operator Session
        $this->load->library('Possession', null, 'PosSession');
        $this->PosSession->removeSession();

        $this->session->unset_userdata('loggedin');

        redirect(base_url('login?' . http_build_query($url_query)));
    }


    public function activation($code = null)
    {

        $control = $this->Login_model->activationControl($code);

        if($control === FALSE):
            $data['status'] = array(
                'result' => FALSE,
                'message' => __('Hatalı aktivasyon/kullanıcı kodu.')
                );
        else:
            $data['status'] = array(
                'result' => TRUE,
                'message' => __('Şifre aktivasyonu tamamlandı.')
                );
        endif;

        $this->load->view('Login/activation' , $data);

        
    }

    public function forgotPassword()
    {
        $email = trim($this->input->post('email'));

        if (!$email) {
            messageAJAX('eposta', __('Eposta adresinizi yazınız.'));
        }

        $user = $this->Users_M->get([
            'email' => $email,
            'active' => 1
        ]);

        if (!$user) {
            message('error', __('Eposta adresiniz geçersiz veya bulunamadı.'));
        }

        $password_token = 'forgot.' . sha1(uniqid());

        $this->Users_M->beforeUpdate([])->update([
            'password_token' => $password_token
        ], $user->id);

        $template = $this->load->view('Mails/password_query.html', null, true);

        $template = strtr($template, [
            '{name}' => $user->name,
            '{url}' => base_url('login/newpassword/' . $password_token),
            '{shop_name}' => 'Posmaks',
            '{shop_url}' => 'http://posmaks.com/',
        ]);

        if (!$this->website->smtp_host || !$this->website->smtp_user) {


            $info_smtp = $this->Memberships->mailSettings();

            if(!$info_smtp || !isset($info_smtp['smtp_host'], $info_smtp['smtp_user']))
                message('error', $this->Memberships->errorMessage);
                

            $this->website->smtp_host = $info_smtp['smtp_host'];
            $this->website->smtp_port = $info_smtp['smtp_port'];
            $this->website->smtp_user = $info_smtp['smtp_user'];
            $this->website->smtp_pass = $info_smtp['smtp_pass'];
            $this->website->email = $info_smtp['smtp_email'];

        } 


        $this->sendMail($this->website->email, $user->email, __('Şifre Sıfırlama İsteği'), $template);

        messageAJAX('success', __('Eposta adresinize şifrenizi sıfırlamak için bir bağlantı gönderildi.'));
    }

    public function newPassword($token = null)
    {
        if (preg_match('/^forgot\.[a-zA-Z0-9]{40}$/', $token)) {

            $user = $this->Users_M->get([
                'password_token' => $token,
                'active' => 1,
            ]);

            if (!$user) {
                $error = __('Geçersiz istek.');   
            }
            
        } else {
            $error = __('Geçersiz bağlantı.');
        }
        
        $this->load->view('Login/new_password', compact('user', 'error'));
    }

    public function processNewPassword()
    {
        $validation = $this->form_validation
        ->set_rules('token', 'token', 'trim|required|regex_match[/^forgot\.[a-zA-Z0-9]{40}$/]')
        ->set_rules('password', 'Şifre', 'trim|required|min_length[6]')
        ->set_rules('password_confirm', 'Şifre Doğrulama', 'matches[password]');

        $token = $this->input->post('token');
        $password = $this->input->post('password');
        
        if (!$validation->run()) {
            messageAJAX('error', validation_errors());
        }

        $user = $this->Users_M->beforeUpdate([])->get([
            'password_token' => $token,
            'active' => 1,
        ]);

        if (!$user) {
            messageAJAX('error', __('Geçersiz istek.'));
        }

        $this->Users_M->update([
            'password' => $password,
            'password_token' => null
        ], $user->id);

        messageAJAX('success', __('Şifreniz başarıyla güncellendi.'));
    }
}
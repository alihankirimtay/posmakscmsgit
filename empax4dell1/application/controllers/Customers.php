<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Customers extends Auth_Controller {

    function __construct()
    {
        parent::__construct();
        $this->check_auth([
            'allowed' => ['ajax']
        ]);
        $this->load->model('Customers_M');
    }

    function index()
    {
        $customers = $this->Customers_M->where([
            'isSupplier' => 0,
            'active !='  => 3
        ])->get_all();

        $this->theme_plugin = ['start' => 'TablesDataTables.init();'];

    	$this->load->template('Customers/index', compact('customers'));
    }

    public function add()
    {
        if ($this->input->is_ajax_request()) {
            if ($this->Customers_M->from_form()->insert()) {
                messageAJAX('success', __('Müşteri başarıyla oluşturuldu.'));
            } else {
                messageAJAX('error', validation_errors());
            }
        }

        $this->load->template('Customers/add');
    }

    public function edit($id = null)
    {
        $customer = $this->Customers_M->findOrFail($id);

        if ($this->input->is_ajax_request()) {
            if ($this->Customers_M->from_form(null, null, ['id' => $id])->update()) {
                messageAJAX('success', __('Müşteri başarıyla güncellendi.'));
            } else {
                messageAJAX('error', validation_errors());
            }
        }

        $this->load->template('Customers/edit', compact('customer'));
    }

    public function delete($id = null)
    {
        $this->Customers_M->findOrFail($id);

        $this->Customers_M->update([
            'active' => 3
        ], ['id' => $id]);

        messageAJAX('success', __('Müşteri başarıyla silindi.'));
    }

    public function ajax($event = null)
    {
        switch ($event) {
            case 'search':
                $this->search();
                break;

            case 'process':
                $this->process();
                break;
        }
    }


    private function search()
    {
        $q = trim($this->input->post('q', TRUE));


        $this->db
        ->select('id, name, phone, address')
        ->group_start()
            ->or_like('name', $q)
            ->or_like('phone', $q)
            ->or_like('address', $q)
        ->group_end();

        $customers = $this->Customers_M->get_all(['active' => 1]);
        
        api_messageOk([
            'items' => $customers
        ]);
    }



    private function process()
    {
        $id = (int) $this->input->post('id');
        $_POST['active'] = 1;

        // Update customer
        if ($id) {
            
            $this->Customers_M->findOrFail([
                'active' => 1,
                'id' => $id,
            ]);

            if ($this->Customers_M->from_form(null, null, ['id' => $id])->update()) {
                messageAJAX('success', __('Müşteri başarıyla güncellendi.'), [
                    'process' => 'update'
                ]);
            }
            messageAJAX('error', validation_errors());
        }

        // Insert new customer
        else {

            if ($customer_id = $this->Customers_M->from_form()->insert()) {
                messageAJAX('success', __('Müşteri başarıyla oluşturuldu.'), [
                    'id' => $customer_id,
                    'process' => 'insert'
                ]);
            }
            messageAJAX('error', validation_errors());
        }
    }

}
?>
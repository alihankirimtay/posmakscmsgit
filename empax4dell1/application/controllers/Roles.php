<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Roles extends Auth_Controller {

    public function __construct()
    {
        parent::__construct();

        $this->check_auth();

        $this->load->model('Roles_M');
        $this->load->model('Permissions_M');
    }

    public function index()
    {   
        $roles = $this->Roles_M->get_all([
            'active !=' => 3
        ]);

        $this->theme_plugin = [
            'start' => 'TablesDataTables.init();'
        ];

        $this->load->template('Roles/index' , compact('roles'));
    }

    public function add()
    {
        if ($this->input->is_ajax_request()) {
            
            $_POST['editable'] = 1;
            $_POST['alias'] = $this->input->post('title');

            if ($this->Roles_M->from_form()->insert()) {
                messageAJAX('success' , __('Rol oluşturldu.'));
            }

            messageAJAX('error' , validation_errors());
        }

        $this->load->template('Roles/add');
    }

    public function edit($id = null)
    {
        $role = $this->Roles_M->findOrFail([
            'id' => $id,
            'editable' => 1
        ]);

        if ($this->input->is_ajax_request()) {
            
            $_POST['editable'] = 1;
            $_POST['alias'] = $this->input->post('title');

            if ($this->Roles_M->from_form(null, null, ['id' => $id])->update()) {
                messageAJAX('success' , __('Rol güncellendi.'));
            }

            messageAJAX('error' , validation_errors());
        }

        $this->load->template('Roles/edit' , compact('role'));
    }

    public function delete($id = null)
    {
        $this->Roles_M->findOrFail(['id' => $id]);

        $this->Roles_M->where(['id' => $id])->update(['active' => 3]);

        messageAJAX('success' , __('Rol silindi.'));
    }

    public function permission()
    {
        if ($this->input->is_ajax_request()) {
            $this->processPermissions();
        }

        $roles = $this->Roles_M->get_all([
            'alias !=' => 'admin',
            'active !=' => 3
        ]);

        $permissions = $this->Permissions_M->with_roles([
            'alias !=' => 'admin',
            'active !=' => 3
        ])->get_all([
            'active !=' => 3
        ]);

        $this->theme_plugin = [
            'css'   => ['globals/plugins/jquery-icheck/skins/all.css'],
            'js'    => [
                'globals/plugins/jquery-icheck/icheck.min.js',
                'globals/scripts/forms-icheck.js',
                'panel/js/view/roles.js'
            ],
            'start' => '
                FormsIcheck.init();
                Roles.Permissions.init();
            '
        ];

        $this->load->template('Roles/permission' , compact('roles', 'permissions'));
    }

    private function processPermissions()
    {
        $requested_roles = (Array) $this->input->post('roles[][]');

        $roles = $this->Roles_M->get_all([
            'alias !=' => 'admin',
            'active !=' => 3
        ]);

        $permissions = $this->Permissions_M->get_all([
            'active !=' => 3
        ]);

        $role_permissions = [];
        foreach ($roles as $role) {
            
            if (isset($requested_roles[$role->id])) {
                if (is_array($requested_roles[$role->id])) {
                    foreach ($requested_roles[$role->id] as $requested_permissions => $val) {
                        foreach ($permissions as $permission) {
                            if ($requested_permissions == $permission->id) {
                                $role_permissions[] = [
                                    'role_id' => $role->id,
                                    'permission_id' => $permission->id,
                                ];
                            }
                        }
                    }
                }
            }
        }

        $this->db->delete('role_has_permissions', ['role_id !=' => 'delete all']);

        foreach ($role_permissions as $role_permission) {
            $this->db->insert('role_has_permissions', $role_permission);
        }

        messageAJAX('success', __('Yetkiler güncellendi.'));
    }
}
<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Login extends API_Controller {

    function __construct()
    {
        parent::__construct();

        $this->load->model('Api/Login_model');
    }

    function index()
    {
    	$data['name'] 	  = $this->getPOST('name', 'trim|required|min_length[4]|max_length[32]', TRUE);
    	$data['password'] = $this->getPOST('password', 'trim|required|min_length[6]|max_length[32]', TRUE);
    	$data['location'] = (int) $this->getPOST('location', 'trim|integer');

    	if(!$this->exists('locations', "id = ${data['location']} AND active = 1", 'id')) {
			$data['password'] = '{REMOVED}';
			api_messageError( logs(NULL, 'Location does not exist.', $data) );
		}

        $this->Login_model->index($data);
    }
}
?>
<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class App extends API_Controller {

    function __construct()
    {
        parent::__construct();

        //$this->load->model('Api/App_model');
    }

    function index()
    {
        
    }
	
	function version()
    {
		$json['count'] = 1;
		$json['items'][] = array(
						'version' => 1,
						'title'   => base_url('assets/uploads/versions/APP_v_1.exe')
					);

		api_messageOk($json);
    }
}
?>
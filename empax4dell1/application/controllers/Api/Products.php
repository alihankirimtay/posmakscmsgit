<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Products extends API_Controller {

    function __construct()
    {
        parent::__construct();

        $this->load->model('Api/Products_model');
    }

    function index()
    {
    	$location = (int) $this->getPOST('location', 'trim|integer');

    	if(!$this->exists('locations', "id = $location AND active = 1", 'id')) api_messageError( logs(NULL, 'Location does not exist.', $location) );

        $this->Products_model->index($location);
    }
}
?>
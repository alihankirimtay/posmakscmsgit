
<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_add_image_to_producttypes extends CI_Migration
{

   public function up()
   {
       $this->dbforge->add_column('product_types', [
           'image' => [
               'type' => 'varchar',
               'constraint' => '255',
               'null' => true,
           ]
       ]);

   }

   public function down()
   {
       $this->dbforge->drop_column('product_types', 'image');
   }

}
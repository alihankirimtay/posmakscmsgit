<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Info extends My_Controller {


	public function __construct()
	{
		parent::__construct();
		$this->load->model("frontend/address_model");
		$this->load->model("backend/restaurants_model");
		$this->load->model("backend/discounts_model");
		$this->load->model("backend/payment_type_model");
        $this->load->model("backend/min_area_price_model");

		$this->load->helper(array('form', 'url'));

	}


	public function location_info($location_id)
	{
		$location_info = $this->restaurants_model->restaurants_info($location_id);
		$restaurant_info = $this->restaurants_model->restaurants_info($location_id);
		$restaurant_address = $this->restaurants_model->restaurant_address($location_id);
		$discounts = $this->discounts_model->index();
		$categories = $this->Posmaks->getProductsGroup([]);
		$payment_type = $this->payment_type_model->index();
		$payment_type_pos = $this->Posmaks->getPayment_Types([]);
		$min_area_price = $this->min_area_price_model->address($location_id);

		$this->load->view("frontend/info",compact('location_info','restaurant_info','restaurant_address','discounts','categories','payment_type','payment_type_pos','min_area_price'));
	}

	public function contact_info($location_id)
	{
		$location_info = $this->restaurants_model->restaurants_info($location_id);
		$restaurant_info = $this->restaurants_model->restaurants_info($location_id);
		$restaurant_address = $this->restaurants_model->restaurant_address($location_id);
		$this->load->view("frontend/contact",compact('location_info','restaurant_info','restaurant_address'));
	}


}
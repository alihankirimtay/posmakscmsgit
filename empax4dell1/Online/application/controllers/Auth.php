<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends My_Controller {

	public function __construct()
	{
		parent::__construct();

		$this->min_password_length = $this->config->item('min_password_length', 'ion_auth');
		$this->max_password_length = $this->config->item('max_password_length', 'ion_auth');
		$this->load->model("frontend/users_model");
		$this->load->model("backend/admin_model");

	}

	public function quickLoginAdmin($id,$pos_data)
	{
		$admin_info = $this->admin_model->admin_info();
		$pos_admin_info = $this->admin_model->pos_admin_info((int)$id);
		
		$token = sha1(json_encode($pos_admin_info));
		
		if($pos_data == $token)
		{

			$this->ion_auth->quickLogin(1);

			echo '<script type="text/javascript">
				window.close();
			</script>';			
		}
	}

	public function login()
	{
		try {

			if ($this->input->post()) {
				
				$this->form_validation->set_rules('identity', 'Eposta', 'required|trim|valid_email');
				$this->form_validation->set_rules('password', 'Şifre', 'required|trim');

				if (!$this->form_validation->run()) {
					throw new Exception(validation_errors());
				}

				$identity = $this->input->post('identity');
				$password = $this->input->post('password');
				$remember = FALSE;

				

				if (!$this->ion_auth->login($identity, $password, $remember)) {
					throw new Exception($this->ion_auth->errors());
				}
				 echo json_encode([
					'status' => 'success',
					'message' => 'Giriş başarılı',
				]);

			} else {

				if ($this->ion_auth->logged_in()) {
					if ($this->ion_auth->is_admin()) {
						redirect(base_url("panel/home"));
					} else {
						redirect(base_url("home"));
					}
				} else {
					//$this->load->view("backend/auth/admin_login");
					redirect(base_url());

				}

				return;
			}
			
		} catch (Exception $e) {
			echo json_encode([
				'status' => 'error',
				'message' => $e->getMessage(),
			]);
		} 
	}

	public function logout()
	{
        $logoutBySystem = $this->input->get('logoutBySystem');
		$this->ion_auth->logout();

		if($logoutBySystem){
        	echo '<script type="text/javascript">
				window.close();
			</script>';	
        }
        else
        {
			redirect(base_url());
        }
		
	}

	public function signUp()
	{
		
		try
		{
			if($this->input->post())
			{
				$this->form_validation->set_rules('first_name', 'Ad', 'trim|required');
				//$this->form_validation->set_rules('last_name', 'Soyad', 'trim|required');
				$this->form_validation->set_rules('email', 'E Posta', "trim|required|valid_email|is_unique[{$this->database_name}users.email]");
				$this->form_validation->set_rules('phone', 'Telefon', 'trim|required');
				$this->form_validation->set_rules('password', 'Şifre', "required|min_length[{$this->min_password_length}]|max_length[{$this->max_password_length}]|matches[password_confirm]");
				$this->form_validation->set_rules('password_confirm', 'Şifre Tekrar', 'required');

				if(!$this->form_validation->run()) {
					throw new Exception(validation_errors());
				}

			//POS tarafına kullanıcı kaydetme yeri
				$pos_Data = array(
					'first_name' => $this->input->post('first_name'),
					'phone' => $this->input->post('phone')
				);
				$pos_customer_id = $this->Posmaks->create_Customer($pos_Data);
			//
				

				$email 			    = $this->input->post('email');
				$password 		    = $this->input->post('password');
				$password_confirm   = $this->input->post('password_confirm');

				$additional_data = array(
					'first_name' => $this->input->post('first_name'),
					'last_name' => '',
					'phone' => $this->input->post('phone'),
					'pos_customer_id' => $pos_customer_id

				);

				if (!$this->ion_auth->register(null, $password, $email, $additional_data))
				{
					throw new Exception($this->ion_auth->errors());
				}
				 

				 echo json_encode([
					'status' => 'success',
					'message' => 'Kayıt başarılı',
				]);
			}
		}
		catch (Exception $e) {

			echo json_encode([
				'status' => 'error',
				'message' => $e->getMessage(),
			]);
		} 

	}

	public function edit_user()
	{
		$user_id = $this->ion_auth->user()->row()->id;
		
		try
		{
			if($this->input->post())
			{
				$this->form_validation->set_rules('first_name', 'Ad', 'trim|required');
				//$this->form_validation->set_rules('last_name', 'Soyad', 'trim|required');
				$this->form_validation->set_rules('email', 'E Posta', 'trim|required|valid_email');
				$this->form_validation->set_rules('phone', 'Telefon', 'trim|required');
				$this->form_validation->set_rules('password', 'Şifre', "required|min_length[{$this->min_password_length}]|max_length[{$this->max_password_length}]|matches[password_confirm]");
				$this->form_validation->set_rules('password_confirm', 'Şifre Tekrar', 'required');

				if(!$this->form_validation->run()) {
					throw new Exception(validation_errors());
				}

				$additional_data = array(
					'first_name' => $this->input->post('first_name'),
					'last_name' => '',
					'phone' => $this->input->post('phone'),
					'email' => $this->input->post('email'),
					'password' => $this->input->post('password'),
				);

				if (!$this->ion_auth->update($user_id, $additional_data))
				{
					throw new Exception($this->ion_auth->errors());
				}


				//Pos Tarafında Güncelleme
				$customer = $this->users_model->customer_id($user_id);
				foreach ($customer as $cus) {
					$customer_id = $cus;
				}
				$pos_edit_data = array(
					'customer_id' => $customer_id,
					'first_name' => $this->input->post('first_name'),
					'phone' => $this->input->post('phone'),
					'modified' => date('Y-m-d H:i:s')
				);

				$pos_edit = $this->Posmaks->customer_edit($pos_edit_data);
				//


				 echo json_encode([
					'status' => 'success',
					'message' => 'Kayıt başarılı',
				]);
				
			}
		}
		catch (Exception $e) {

			echo json_encode([
				'status' => 'error',
				'message' => $e->getMessage(),
			]);
		}
	}

	public function delete_user()
	{
		$user_id = $this->ion_auth->user()->row()->id;
		$this->ion_auth->logout();
		$this->users_model->delete($user_id);
		redirect(base_url("home"));
	}


	public function forgot_password()
	{
		
		$send_email_config = $this->config->item('ion_auth');
		$send_email_config['site_title'] = $this->website->company;
		$send_email_config['admin_email'] = $this->website->email;
		$send_email_config['email_config'] = array(
    		'mailtype'  => 'html',
			'charset'   => 'UTF-8',
			'protocol'  => 'smtp', 
			'smtp_host' => $this->website->smtp_host, 
			'smtp_user' => $this->website->smtp_user, 
			'smtp_pass' => $this->website->smtp_pass, 
			'smtp_port' => $this->website->smtp_port
    	);

		$this->config->set_item('ion_auth', $send_email_config);
		

		// setting validation rules by checking whether identity is username or email
		if ($this->config->item('identity', 'ion_auth') != 'email')
		{
			$this->form_validation->set_rules('identity', $this->lang->line('forgot_password_identity_label'), 'required');
		}
		else
		{
			$this->form_validation->set_rules('identity', $this->lang->line('forgot_password_validation_email_label'), 'required|valid_email');
		}


		if ($this->form_validation->run() === FALSE)
		{
			$this->data['type'] = $this->config->item('identity', 'ion_auth');
			// setup the input
			$this->data['identity'] = array('name' => 'identity',
				'id' => 'identity',
			);

			if ($this->config->item('identity', 'ion_auth') != 'email')
			{
				$this->data['identity_label'] = $this->lang->line('forgot_password_identity_label');
			}
			else
			{
				$this->data['identity_label'] = $this->lang->line('forgot_password_email_identity_label');
			}

			// set any errors and display the form
			$this->data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');
			$this->_render_page('auth/forgot_password', $this->data);
		}
		else
		{
			$identity_column = $this->config->item('identity', 'ion_auth');
			$identity = $this->ion_auth->where($identity_column, $this->input->post('identity'))->users()->row();

			if (empty($identity))
			{

				if ($this->config->item('identity', 'ion_auth') != 'email')
				{
					$this->ion_auth->set_error('forgot_password_identity_not_found');
				}
				else
				{
					$this->ion_auth->set_error('forgot_password_email_not_found');
				}

				$this->session->set_flashdata('message', $this->ion_auth->errors());
				redirect("auth/forgot_password", 'refresh');
			}

			// run the forgotten password method to email an activation code to the user
			$forgotten = $this->ion_auth->forgotten_password($identity->{$this->config->item('identity', 'ion_auth')});

			if ($forgotten)
			{
				// if there were no errors
				$this->session->set_flashdata('message', $this->ion_auth->messages());
				redirect("auth/login", 'refresh'); //we should display a confirmation page here instead of the login page
			}
			else
			{
				$this->session->set_flashdata('message', $this->ion_auth->errors());
				redirect("auth/forgot_password", 'refresh');
			}
		}
	}





	public function reset_password($code = NULL)
	{
		if (!$code)
		{
			show_404();
		}

		$user = $this->ion_auth->forgotten_password_check($code);

		if ($user)
		{
			// if the code is valid then display the password reset form

			$this->form_validation->set_rules('new', $this->lang->line('reset_password_validation_new_password_label'), 'required|min_length[' . $this->config->item('min_password_length', 'ion_auth') . ']|max_length[' . $this->config->item('max_password_length', 'ion_auth') . ']|matches[new_confirm]');
			$this->form_validation->set_rules('new_confirm', $this->lang->line('reset_password_validation_new_password_confirm_label'), 'required');

			if ($this->form_validation->run() === FALSE)
			{
				// display the form

				// set the flash data error message if there is one
				$this->data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');

				$this->data['min_password_length'] = $this->config->item('min_password_length', 'ion_auth');
				$this->data['new_password'] = array(
					'name' => 'new',
					'id' => 'new',
					'type' => 'password',
					'pattern' => '^.{' . $this->data['min_password_length'] . '}.*$',
				);
				$this->data['new_password_confirm'] = array(
					'name' => 'new_confirm',
					'id' => 'new_confirm',
					'type' => 'password',
					'pattern' => '^.{' . $this->data['min_password_length'] . '}.*$',
				);
				$this->data['user_id'] = array(
					'name' => 'user_id',
					'id' => 'user_id',
					'type' => 'hidden',
					'value' => $user->id,
				);
				$this->data['csrf'] = $this->_get_csrf_nonce();
				$this->data['code'] = $code;

				// render
				$this->_render_page('frontend/auth/elements/reset_password', $this->data);
				
			}
			else
			{
				// do we have a valid request?
				if ($this->_valid_csrf_nonce() === FALSE || $user->id != $this->input->post('user_id'))
				{

					// something fishy might be up
					$this->ion_auth->clear_forgotten_password_code($code);

					show_error($this->lang->line('error_csrf'));

				}
				else
				{
					// finally change the password
					$identity = $user->{$this->config->item('identity', 'ion_auth')};

					$change = $this->ion_auth->reset_password($identity, $this->input->post('new'));

					if ($change)
					{
						// if the password was successfully changed
						$this->session->set_flashdata('message', $this->ion_auth->messages());
						redirect(base_url(), 'refresh');
					}
					else
					{
						$this->session->set_flashdata('message', $this->ion_auth->errors());
						redirect('auth/reset_password/' . $code, 'refresh');
					}
				}
			}
		}
		else
		{
			// if the code is invalid then send them back to the forgot password page
			$this->session->set_flashdata('message', $this->ion_auth->errors());
			redirect("auth/forgot_password", 'refresh');
		}
	}

	public function _get_csrf_nonce()
	{
		$this->load->helper('string');
		$key = random_string('alnum', 8);
		$value = random_string('alnum', 20);
		$this->session->set_flashdata('csrfkey', $key);
		$this->session->set_flashdata('csrfvalue', $value);

		return array($key => $value);
	}

	public function _valid_csrf_nonce()
	{
		$csrfkey = $this->input->post($this->session->flashdata('csrfkey'));
		if ($csrfkey && $csrfkey === $this->session->flashdata('csrfvalue'))
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}

	public function _render_page($view, $data = NULL, $returnhtml = FALSE)//I think this makes more sense
	{

		$this->viewdata = (empty($data)) ? $this->data : $data;

		$view_html = $this->load->view($view, $this->viewdata, $returnhtml);

		// This will return html on 3rd argument being true
		if ($returnhtml)
		{
			return $view_html;
		}
	}

	

}

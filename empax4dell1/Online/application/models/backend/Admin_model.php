<?php

class Admin_model extends CI_Model
{
	public function admin_info()
	{
		$admin_info = $this->db->get_where("{$this->database_name}users",array('id' => 1));
		return $admin_info->row();
    }

    public function pos_admin_info($id)
	{
		$this->db->from("users");
		$this->db->where("id",$id);
		$pos_admin_info=$this->db->get();
		return $pos_admin_info->row();
    }

}
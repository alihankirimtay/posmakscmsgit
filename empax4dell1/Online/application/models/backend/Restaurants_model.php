<?php


class Restaurants_model extends CI_Model
{
	public function restaurants()
	{
		$restaurants = $this->db->get("{$this->database_name}restaurants")->result();
		return $restaurants;

	}

	public function restaurants_info($restaurant_id)
	{
		$info = $this->db->get_where("{$this->database_name}restaurants",array('id' => $restaurant_id));
		return $info->result();
	}

	public function restaurant_address($restaurant_id)
	{
		$restaurant_address = $this->db
		->select('*')
		->select("(select sehir_title from {$this->database_name}sehir where sehir_id = {$this->database_name}restaurants.sehir_id) as sehir_title")
		->select("(select ilce_title from {$this->database_name}ilce where ilce_id = {$this->database_name}restaurants.ilce_id) as ilce_title")
		->select("(select mahalle_title from {$this->database_name}mahalle where mahalle_id = {$this->database_name}restaurants.mahalle_id) as mahalle_title")
		->get_where("{$this->database_name}restaurants",array('id' => $restaurant_id));
		return $restaurant_address->result();	
	}

	public function status($restaurant_id)
	{
		$status = $this->db->get_where("{$this->database_name}restaurants",array('id' => $restaurant_id));
		return $status->result();
	}

	public function statusUpdate($restaurant_id,$status)
	{
		$this->db->set('status', $status); 
		$this->db->where('id', $restaurant_id);   
		$this->db->update("{$this->database_name}restaurants");
		return;
		

	}
	
}


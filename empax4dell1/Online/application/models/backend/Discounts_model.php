<?php


class Discounts_model extends CI_Model
{
	public function index()
	{
		$discounts = $this->db->get("{$this->database_name}discounts");
		return $discounts->result();
	}

	public function delete($id,$deleted_time)
	{
		$this->db->set('deleted_time', $deleted_time);
        $this->db->where('id', $id);
        $this->db->update("{$this->database_name}discounts");
	}
	
    

}
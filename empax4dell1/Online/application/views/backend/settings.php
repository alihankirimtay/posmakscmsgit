<?php $this->load->view('backend/template/header'); ?>

<?php $this->load->view('backend/warning'); ?>


<div class="page_title">
    <div class="col-sm-6">
        <div class="back_icon">
            <a href="<?= base_url('panel/MainPanel'); ?>" class="back_icon_button">
                <div class="back-card-full-icon fa fa-arrow-left"></div>
            </a>
        </div>
        <h4>Ayarlar</h4>
    </div>
    <div class="col-sm-6">
        <a href="#">AYARLAR</a>
        <a href="<?= base_url('panel/MainPanel'); ?>">
            <div class="card-full-icon fa fa-home"></div>
        </a>
    </div>
</div>

<div class="Online_Admin_Panel_Content">

    <div class="main">
        <form action="<?= base_url("panel/Settings/update/"); ?>">
            <?php foreach ($settings as $setting) { ?>
                <input type="hidden" name="settings_id" value="<?= $setting->id; ?>">
                
                <h3>Restaurant Adı(Sitenin Başlığında Gösterilecek İsim)</h3>
                <input type="text" class="form-control" name="location_brand_name" style="width: 100%" value="<?= $setting->restaurant_name; ?>"><br>

                <h3>Google Maps Api Key</h3>
                <input type="text" class="form-control" name="maps_api_key" style="width: 100%" value="<?= $setting->api_key; ?>"><br>
            
            <?php } ?>
            
            <h3>Promosyonlar</h3>
            <div class="promotion" >
                <table class="promotion_table table table-bordered">
                    <thead class="thead-light">
                        <tr>
                            <input type="text" class="form-control" name="" style="width: 100%" aria-describedby="" disabled  value="Her Siparişte İndirim Uygulanmasını İstiyorsanız Sipariş Sayısı Kısmına 0(sıfır) yazınız"><br>
                            <th scope="col">Sipariş Sayısı</th>
                            <th scope="col">İndirim Başlığı</th>
                            <th scope="col">İndirim Oranı(%)</th>
                            <th scope="col">Aktif</th>
                            <th scope="col"></th>
                        
                        </tr>
                    </thead>
                    <tbody class="promotion_body">
                        <?php foreach($discounts as $discount) { ?>
                            
                            <?php if($discount->deleted_time == "") { ?>
                                
                                <tr>
                                    <td><input type="text" class="form-control" name="which_discount[<?= $discount->id; ?>]" style="width: 100%" value="<?= $discount->which_discount; ?>"></td>
                                    <td><input type="text" class="form-control" name="" style="width: 100%" aria-describedby="" disabled  value="<?= $discount->title; ?>"></td>
                                    <td><input type="text" class="form-control" name="discount_rate[<?= $discount->id; ?>]" style="width: 100%" aria-describedby=""  value="<?= $discount->discount_rate; ?>"></td>
                                    <?php if($discount->active == 1) { ?>
                                        <td><input type="checkbox" class="form-control" name="active[<?= $discount->id; ?>]" value="1" checked></td>
                                    <?php }else{ ?>
                                        <td><input type="checkbox" class="form-control" name="active[<?= $discount->id; ?>]" value="1" ></td>
                                    <?php } ?>
                                    <td><button type="button" class="delete_discount btn btn-danger" key="<?= $discount->id; ?>">Sil</button></td>
                                    <input type="hidden" name="discount_id[<?= $discount->id; ?>]" value="<?= $discount->id; ?>">
                                </tr>
                           
                          
                            <?php } ?>
                        <?php } ?>
                        
                    </tbody>
                    
                </table>
                <tr><center><button type="button" id="add_new_discount" class="add_new_discount btn btn-success btn-lg">Yeni İndirim Ekle</button></center></tr>
            </div>

            <br><br>

            <h3>Ödeme Tipleri</h3>
            <div class="payment_type" >
                <table class="table table-bordered">
                    <thead class="thead-light">
                        <tr>
                            <th scope="col">Ödeme Tipi Başlığı</th>
                            <th scope="col">Aktif</th>
                        
                        </tr>
                    </thead>
                    <tbody>
                        <?php if(count($payment_type) >= 1) { ?>
                            <?php foreach($payment_type as $type_) { ?>
                                <?php foreach($payment_type_pos as $type) { ?>
                                    <?php if($type_->id === $type['id']) { ?>
                                        <tr>
                                            <td><input type="text" class="form-control" name="" style="width: 100%" aria-describedby="" disabled  value="<?= $type['title']; ?>"></td>
                                            <?php if($type_->active == 1) { ?>
                                                <td><input type="checkbox" class="form-control" name="active_payment[<?= $type_->id; ?>]" value="1" checked></td>
                                            <?php }else{ ?>
                                                <td><input type="checkbox" class="form-control" name="active_payment[<?= $type_->id; ?>]" value="1" ></td>
                                            <?php } ?>
                                            <input type="hidden" name="payment_id[<?= $type['id']; ?>]" value="<?= $type['id']; ?>">
                                        </tr>
                                    <?php } ?>
                                <?php } ?>
                            <?php } ?>
                        <?php } else{ ?>
                            <?php foreach($payment_type_pos as $type) { ?>
                                <tr>
                                    <td><input type="text" class="form-control" name="" style="width: 100%" aria-describedby="" disabled  value="<?= $type['title']; ?>"></td>
                                    <td><input type="checkbox" class="form-control" name="active_payment[<?= $type['id']; ?>]" value="1" checked></td>
                                    <input type="hidden" name="payment_id[<?= $type['id']; ?>]" value="<?= $type['id']; ?>">
                                </tr>
                            <?php } ?>
                        <?php } ?>

                    </tbody>
                </table>
            </div>

            <br><br>
            <button type="button" name="doSubmit" class="btn btn-success btn-lg btn-block">Kaydet</button>
        </form>
        <br><br>


        <!-- Logo ve Arka Plan Olayları -->
        
        <?php if($this->website->image != null || $this->website->image != "" || $this->website->bodyimage != null || $this->website->bodyimage != "" ): ?>

            <?php if($this->website->image != null || $this->website->image != ""): ?>
                <div class="logo_">
                <center>
                    <table class="table table-bordered" style="width:100%;">
                        <thead class="thead-light">
                            <tr>
                                <th scope="col">Şimdiki Logonuz</th>
                                <th scope="col"></th>
                            </tr>
                        </thead>
                        <tbody>
                                <tr>
                                    <td><center><img src="<?= base_url($this->website->image); ?>" class="settings_logo_image"></center></td>
                                    <td><input type="button"  data-id="<?= $this->website->id ?>" data-src="<?= $this->website->image ?>" value="Sil" class="delete_logo_image btn-danger btn-lg btn-block"/></td>
                                    
                                    <input class="slide_image_src" type="hidden"  name="slide_image_src" value="<?= $this->website->image ?>">        
                                </tr>
                        </tbody>
                    </table>
                </center>              
                </div>
                <br><br>
            <?php endif; ?>
            <?php if($this->website->bodyimage != null || $this->website->bodyimage != ""): ?>
                <div class="logo_">
                <center>
                    <table class="table table-bordered" style="width:100%;">
                        <thead class="thead-light">
                            <tr>
                                <th scope="col">Şimdiki Arka Planınız</th>
                                <th scope="col"></th>
                            </tr>
                        </thead>
                        <tbody>
                                <tr>
                                    <td><center><img src="<?= base_url($this->website->bodyimage); ?>" class="settings_logo_image"></center></td>
                                    <td><input type="button"  data-id="<?= $this->website->id ?>" data-src="<?= $this->website->bodyimage ?>" value="Sil" class="delete_body_image btn-danger btn-lg btn-block"/></td>
                                    
                                    <input class="slide_image_src" type="hidden"  name="slide_image_src" value="<?= $this->website->bodyimage ?>">        
                                </tr>
                        </tbody>
                    </table>
                </center>              
                </div>
                <br><br>
            <?php endif; ?>
        


            <div class="logo_insert">
                <form action="<?= base_url('panel/settings/logoUpdate') ?>">

                    <h3>Logo Seçimi</h3>
                    
                    <div class="slide_image_add" style="width: 100%; margin-top: 10px;">
                        <center>
                            <img src="" class="preview_image" width="600px" height="200px" />
                            <input type="file" class='preview_image_select' name="image">
                        </center>
                        <br><br>
                    </div>
                    <button type="button" name="doSubmit" class="btn btn-success btn-lg btn-block">Kaydet</button>
                    <br><br>
                </form>

                <form action="<?= base_url('panel/settings/BodyImageUpdate') ?>">
                    <h3>Arka Plan Seçimi</h3>
                    
                    <div class="slide_image_add" style="width: 100%; margin-top: 10px;">
                        <center>
                            <img src="" class="preview_image" width="600px" height="200px" />
                            <input type="file" class='preview_image_select' name="bodyimage">
                        </center>
                        <br><br>
                    </div>
                    <button type="button" name="doSubmit" class="btn btn-success btn-lg btn-block">Kaydet</button>
                </form>
            </div>

        <?php else: ?>
        <form action="<?= base_url('panel/settings/ImagesAdd') ?>">
            <div class="logo_insert">
                <h3>Logo Seçimi</h3>
                
                <div class="slide_image_add" style="width: 100%; margin-top: 10px;">
                    <center>
                        <img src="" class="preview_image" width="600px" height="200px" />
                        <input type="file" class='preview_image_select' name="image">
                    </center>
                </div>
            </div><br><br>
            <div class="body_image_insert">
            <h3>Arkaplan Resmi Seçimi</h3>
            
                <div class="slide_image_add" style="width: 100%; margin-top: 10px;">
                    <center>
                        <img src="" class="preview_image" width="600px" height="200px" />
                        <input type="file" class='preview_image_select' name="bodyimage">

                    </center>
                    <br><br>
                </div>
            </div>
        
            <button type="button" name="doSubmit" class="btn btn-success btn-lg btn-block">Kaydet</button>
        </form>
        <?php endif; ?>

    </div>
</div>

<?php $this->load->view('backend/template/footer'); ?>
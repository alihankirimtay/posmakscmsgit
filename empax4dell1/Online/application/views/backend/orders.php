<?php $this->load->view('backend/template/header'); ?>

<div class="page_title">
    <div class="col-sm-6">
        <div class="back_icon">
            <a href="<?= base_url('panel/MainPanel'); ?>" class="back_icon_button">
                <div class="back-card-full-icon fa fa-arrow-left"></div>
            </a>
        </div>
        <h4>Siparişler</h4>
    </div>
    <div class="col-sm-6">
        <a href="#">SİPARİŞLER</a>
        <a href="<?= base_url('panel/MainPanel'); ?>">
            <div class="card-full-icon fa fa-home"></div>
        </a>
    </div>
</div>

<div class="Online_Admin_Panel_Content">

    <div class="main">
        <div class="order_search">
            <h3>Tarihe Göre Arama Yap</h3>
            <div class="row">
                <div class="col-sm-12 col-md-4 col-lg-4">
                    <div class="input-group date" data-provide="datepicker">
                        <input type="text" class="start_date form-control" placeholder="Başlangıç Tarihi">
                        <div class="input-group-addon">
                            <span class="glyphicon glyphicon-th"></span>
                        </div>
                    </div>
                </div>

                <div class="col-sm-12 col-md-4 col-lg-4">
                    <div class="input-group date" data-provide="datepicker">
                        <input type="text" class="finish_date form-control" placeholder="Bitiş Tarihi">
                        <div class="input-group-addon">
                            <span class="glyphicon glyphicon-th"></span>
                        </div>
                    </div>
                </div>

                <div class="col-sm-12 col-md-4 col-lg-4">
                    <button type="button"  class="get_Customer_Order btn btn-success btn-block" >Ara</button>
                </div>
            </div>
        </div>

        <br>
        <br>
            <table class="orders table table-bordered">
                <thead class="thead-light">
                    <tr>
                        <th scope="col">SİPARİŞİN VERİLDİĞİ LOKASYON</th>
                        <th scope="col">KULLANICI BİLGİSİ</th>
                        <th scope="col">ÖDEME TİPİ</th>
                        <th scope="col">ADRES</th>
                        <th scope="col">SİPARİŞ ZAMANI</th>
                        <th scope="col">SİPARİŞ NOTU</th>
                        <th scope="col">SİPARİŞ TUTARI</th>
                        <th scope="col">İNDİRİMLİ SİPARİŞ TUTARI</th>
                        <th scope="col">SİPARİŞ DURUMU</th>
                        <th scope="col"></th>
                    </tr>
                </thead>
                <tbody class="details">

                <?php foreach($orders as $order) { ?>
                    <tr>
                        <?php foreach($restaurantPos as $restaurant) { ?>
                            <?php if($restaurant['id'] == $order->location_id) { ?>
                                <td><?= $restaurant['title']; ?></td>
                            <?php } ?>
                        <?php } ?>

                        <?php foreach($customers as $customer) { ?>
                            <?php if($customer->id == $order->user_id) { ?>
                                <td><?= $customer->first_name; ?> <?= $customer->last_name; ?></td>
                            <?php } ?>
                        <?php } ?>

                        <?php foreach($payment_type as $pay_type) { ?>
                            <?php if($pay_type['id'] == $order->payment_type) { ?>
                                <td><?= $pay_type['title']; ?></td>
                            <?php } ?>
                        <?php } ?>

                        <?php foreach($customer_address as $c_address) { ?>
                            <?php if($c_address->id == $order->address) { ?>
                                <td><?= $c_address->sehir_title ?> / <?= $c_address->ilce_title ?> / <?= $c_address->mahalle_title ?> </td>
                            <?php } ?>
                        <?php } ?>
                        
                        <td><?= $order->order_date; ?></td>
                        <td><?= $order->order_note; ?></td>
                        <td><?= $order->total_price; ?> ₺</td>
                        <td><?= $order->total_discount_price; ?> ₺</td>

                        <?php foreach($pos_orders as $p_order) { ?>
                            <?php if($p_order["id"] == $order->sale_id) { ?>
                                <td> 
                                    <?php if($p_order["status"] == "pending"){ ?>
                                        <div class="order_status p-3 mb-2 bg-warning text-white" style="margin-top: 10%;"><center>Beklemede</center></div>
                                    <?php } ?>
                                    
                                    <?php if($p_order["status"] == "completed"){ ?>
                                        <div class="order_status p-3 mb-2 bg-success text-white" style="margin-top: 10%;"><center>Tamamlandı</center></div>
                                    <?php } ?>
                                    
                                    <?php if($p_order["status"] == "shipped"){ ?>
                                        <div class="order_status p-3 mb-2 bg-info text-white" style="margin-top: 10%;"><center>Yola Çıktı</center></div>
                                    <?php } ?>

                                    <?php if($p_order["status"] == "cancelled"){ ?>
                                        <div class="order_status p-3 mb-2 bg-secondary text-white" style="margin-top: 10%;"><center>İptal Edildi</center></div>
                                    <?php } ?>        
                                </td>
                            <?php } ?>
                        <?php } ?>

                        <td><button type="button" key="<?= $order->sale_id; ?>" class="order_detail btn btn-info" data-toggle="modal" data-placement="top" title="Sipariş İle İlgili Ürünleri Görmek İçin Tıklayınız" data-target="#user_order_detail_Modal" style="margin-top: 13%;">Sipariş Detayları</button></td>

                    </tr>
                <?php } ?>
                </tbody>
            </table>
    </div>


    <!-- Modal -->
    <div class="modal fade"  id="user_order_detail_Modal" tabindex="-1" role="dialog"  aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLongTitle">SİPARİŞ DETAYLARI</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
                <div class="bg-light text-dark">
                    <div class="panel-cart-title">
                        
                    </div>

                    <div class="panel-cart-container">
                        <input type="hidden" class="restaurant_id" name="location_id" value="">
                        <div class="panel-cart-content">
                            <center>
                            <table class="table-cart">

                                <thead>
                                    <th style="color:red;">Ürün Adedi</th>
                                    <th style="color:red;">Ürün Adı</th>
                                    <th style="color:red;">Ürün Birim Fiyatı</th>
                                </thead>
                                
                            </table>
                            </center>
                            <br><br>
                            <div class="summary">

                                <div class="total_price row text-lg" style="font-weight: bold; color: black;">
                                    <div class="col-7 text-right">Toplam Fiyat:</div>
                                </div>

                                <div class="total_discount_price row text-lg" style="font-weight: bold; color: black;">
                                    <div class="col-7 text-right">İndirimli Fiyat:</div>
                                </div>
                                <br><br>

                            </div>
                        </div>
                    </div>
                </div>   
          </div>
          
        </div>
      </div>
    </div>
</div>

<?php $this->load->view('backend/template/footer'); ?>
<?php $this->load->view('backend/template/header'); ?>

<?php $this->load->view('backend/warning'); ?>



<div class="page_title">
    <div class="col-sm-6">
        <div class="back_icon">
            <a href="<?= base_url('panel/MainPanel'); ?>" class="back_icon_button">
                <div class="back-card-full-icon fa fa-arrow-left"></div>
            </a>
        </div>
        <h4>Slayt Yönetimi</h4>
    </div>
    <div class="col-sm-6">
        <a href="#">SLAYT YÖNETİMİ</a>
        <a href="<?= base_url('panel/MainPanel'); ?>">
            <div class="card-full-icon fa fa-home"></div>
        </a>
    </div>
</div>

<div class="Online_Admin_Panel_Content">

    <div class="main">
        <div class="slide_image" style = "width: 100%;">
        <center>
            <form action="<?= base_url('panel/slidemanagement/updateImage') ?>">

                <table class="table table-bordered" style="width:100%;">
                    <thead class="thead-light">
                        <tr>
                            <th scope="col">Resimler</th>
                            <th scope="col"></th>
                            <th scope="col"></th>
                        </tr>
                    </thead>
                    <tbody>

                        <?php foreach($images as $image) { ?>
                            <tr>
                                <td><center><img src="<?= base_url($image->image); ?>" width="800px" height="300px" alt="" style="border-radius: 40px;"></center></td>
                                <td><input type="button"  data-id="<?= $image->id; ?>" data-src="<?= $image->image ?>" value="Sil" class="delete_slide_image btn-danger btn-lg btn-block" style="margin-top: 120px;"/></td>
                                <td>
                                    <?php if($image->active == 1): ?>
                                    <input type="checkbox" class="form-control" name="active[<?= $image->id; ?>]" value="1" checked style="width: 30px; height: 30px; margin-top: 133px;" >
                                    <?php else: ?> 
                                        <input type="checkbox" class="form-control" name="active[<?= $image->id; ?>]" value="1" style="width: 30px; height: 30px; margin-top: 133px;" >
                                    <?php endif; ?> 
                                </td>
                                
                                <input class="slide_image_src" type="hidden"  name="slide_image_src[<?= $image->id; ?>]" value="<?= $image->image ?>">
                                   
                                <input class="slide_image_id" type="hidden"  name="slide_image_id[<?= $image->id; ?>]" value="<?= $image->id ?>">
                                       
                            </tr>
                        <?php } ?>
                            
                    </tbody>
                </table>
            <button type="button" name="doSubmit" class="btn btn-info btn-lg btn-block">Kaydet</button>
            </form>
        </center>
        </div>


    	<div class="slide_image_add" style="width: 100%; margin-top: 50px;">

            <form action="<?= base_url('panel/slidemanagement/process') ?>">
                <center>
                    <h3>Yükleyeceğiniz resimler en fazla 2 mb olmalıdır</h3>
                    <h3>Yükleyeceğiniz resimlerin çözünürlüğü 800x300 px olmalıdır</h3>
                    
                    <center>
                        <img src="" class="preview_image" width="900px" height="300px" /><br>
                        <input type="file" class='preview_image_select' name="image">
                    </center>
                </center>
                <br><br>
                 <button type="button" name="doSubmit" class="btn btn-info btn-lg btn-block">Kaydet</button>
            </form>
        </div>
    </div>
</div>

<?php $this->load->view('backend/template/footer'); ?>


<div class="modal fade" id="editModal" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header modal-header-lg dark bg-dark">
                <div class="bg-image"><img src="<?= base_url(); ?>/assets/img/photos/modal-add.jpg" alt=""></div>
                <h4 class="modal-title">Hesabı Düzenle</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class="ti-close"></i></button>
            </div>
            <div class="modal-product-details">
                <div class="example-box-content">
				
					<ul class="nav nav-tabs" id="myTab" role="tablist">
						<li class="nav-item">
							<a class="nav-link active" id="home-tab" data-toggle="tab" href="#settings" role="tab" aria-controls="home" aria-selected="true">Hesap Ayarları</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" id="profile-tab" data-toggle="tab" href="#address" role="tab" aria-controls="profile" aria-selected="false">Adreslerim</a>
						</li>
					</ul>
					

					<div class="tab-content">
						<div class="tab-pane active" id="settings" role="tabpanel" aria-labelledby="home-tab">
							<br><br>
							
							<center><h3>Kullanıcı Bilgileri</h3></center>
							
							<div class="">		
		
								<form action="<?= base_url("Auth/edit_user/"); ?>" gourl="<?= $_SERVER['REQUEST_URI'] ?>">
									<center>
									
										<div class="form-group">
											<label>Adınız Soyadınız</label>
											<input type="text" id="first_name" class="form-control" name="first_name" style="width: 100%" placeholder="Adınızı ve Soyadınızı Giriniz" value="">
										</div>
									<!--
										<div class="form-group">
											<label>Soyad</label>
											<input type="text" id="last_name" class="form-control" name="last_name" style="width: 100%" placeholder="Soyadınızı Giriniz"  value="">
										</div>
									-->
										<div class="form-group">
											<label>E-Mail</label>
											<input type="text" id="email" class="form-control" name="email" style="width: 100%" aria-describedby="emailHelp" placeholder="E-Mail Giriniz" value="">
										</div>
										<div class="form-group">
											<label>Telefon</label>
											<input type="text" id="phone" class="form-control" name="phone" style="width: 100%" placeholder="Telefon Giriniz" value="">
										</div>
										<div class="form-group">
											<label>Şifre</label>
											<input type="password" class="form-control" name="password" style="width: 100%" placeholder="Şifre Giriniz">
										</div>
										<div class="form-group">
											<label>Şifre Onayı</label>
											<input type="password" class="form-control" name="password_confirm" style="width: 100%" placeholder="Şifreyi Tekrar Giriniz">
										</div>
 										<div class="result"></div>
										<br>
										<button type="button" name="doSubmit" class="btn btn-success" style="width: 100%"><span>Kaydet</span></button>
									
									</center>
								</form>
							</div>	
									
						</div>



						<div class="tab-pane" id="address" role="tabpanel" aria-labelledby="profile-tab">
							<br><br>
							<button type="button" id="newAddress" class="editAddress btn btn-info" style="width: 100%" href="#newAddress_" data-target="#newAddress_" data-toggle="modal"><span>Yeni Adres Ekle</span></button>
							
							<br><br>

							<!-- Kullanıcı adreslerinin listelendiği yer  -->
							<center><h3>Adreslerim</h3></center>
							

						</div>

					</div>
                </div>
            </div>
	    </div>
    </div>
</div>

<div class="newAddress_">
	<div class="modal fade" id="newAddress_" role="dialog">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header modal-header-lg dark bg-dark">
					<div class="bg-image"><img src="<?= base_url(); ?>/assets/img/photos/modal-add.jpg" alt=""></div>
					<h4 class="modal-title">YENİ ADRES EKLEME EKRANI</h4>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class="ti-close"></i></button>
				</div>
				<div class="modal-product-details">
					<div class="example-box-content">

						<form action="<?= base_url("Address/insert/"); ?>" gourl="<?= $_SERVER['REQUEST_URI'] ?>" >
							<center>
								<h2>Yeni Adres</h2>
								<div class="form-group">
									<label>Adres Adı</label>
									<input type="text" class="form-control" name="adres_name" style="width: 100%" placeholder="Adres Başlığını Giriniz" value="">
								</div>
								
								<div class="form-group">
									<label for="sehir">Şehir</label>
									<select class="custom-select mb-2 mr-sm-2 mb-sm-0 select2" style="width: 100%" id="sehir" name="sehir">
										<option value="" key="">Şehir...</option>
									</select>
								</div>
								<div class="form-group">
									<label class="mr-sm-2 " for="inlineFormCustomSelect">İlçe</label>
									<select class="custom-select mb-2 mr-sm-2 mb-sm-0 select2" style="width: 100%" id="ilce" name="ilce">
										<option value=""></option>
									</select>
								</div>
								<div class="form-group">
									<label class="mr-sm-2 " for="inlineFormCustomSelect">Mahalle</label>
									<select class="custom-select mb-2 mr-sm-2 mb-sm-0 select2" style="width: 100%" id="mahalle" name="mahalle">
									</select>
								</div>
								<div class="form-group">
									<label>Açık Adres</label>
									<input type="text" class="form-control" name="acik_adres" style="width: 100%" placeholder="Açık Adresinizi Giriniz" value="">
								</div>

								<div class="result"></div>

								<button type="button" name="doSubmit" class="btn btn-success" style="width: 50%"><span>Kaydet</span> </button>
							</center>
						</form>

					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="editAddress_" role="dialog">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header modal-header-lg dark bg-dark">
				<div class="bg-image"><img src="<?= base_url(); ?>/assets/img/photos/modal-add.jpg" alt=""></div>
				<h4 class="modal-title">ADRES DÜZENLEME EKRANI</h4>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class="ti-close"></i></button>
			</div>
			<div class="modal-product-details">
				<div class="example-box-content">

					<form action="<?= base_url("Address/update/"); ?>" gourl="<?= $_SERVER['REQUEST_URI'] ?>">
						<center>
							<div class="addressName">
								<label>Adres Adı</label>
								<input type="text" class="form-control" id="adres_name" name="adres_name" style="width: 100%" placeholder="Adres Başlığını Giriniz" value="">
							</div>
							<br>
							<div class="form-group">
								<label for="sehir">Şehir</label>
								<select class="custom-select mb-2 mr-sm-2 mb-sm-0 select2" style="width: 100%" id="sehirEdit" name="sehirEdit">
								</select>
							</div>
							<br>
							<div class="form-group">
								<label class="mr-sm-2 " for="inlineFormCustomSelect">İlçe</label>
								<select class="custom-select mb-2 mr-sm-2 mb-sm-0 select2" style="width: 100%" id="ilceEdit" name="ilceEdit">
								</select>
							</div>
							<br>
							<div class="form-group">
								<label class="mr-sm-2 " for="inlineFormCustomSelect">Mahalle</label>
								<select class="custom-select mb-2 mr-sm-2 mb-sm-0 select2" style="width: 100%" id="mahalleEdit" name="mahalleEdit">
								</select>
							</div>
							<br>
							<div class="acikAdres">
								<label>Açık Adres</label>
								<input type="text" class="form-control" id="acik_adres" name="acik_adres" style="width: 100%" placeholder="Açık Adresinizi Giriniz" value="">
							</div>
							<br>
							<button type="button" name="doSubmit" class="btn btn-primary" style="width: 25%"><span>Kaydet</span></button>      
							<input type="hidden" id="address_id" name="address_id" value="" >   
						</center>
					</form>

				</div>
			</div>
		</div>
	</div>
</div>


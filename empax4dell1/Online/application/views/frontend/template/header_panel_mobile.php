<!-- Panel Mobile -->
<nav id="panel-mobile">
<div class="module module-logo bg-light">
<!-- <div class="module module-logo bg-dark dark"> -->
    
        <?php 
            $logo = $this->settings->settings_model->getLogo();
            foreach ($logo as $image) { 
        ?>
        	<a href="<?= base_url(); ?>">
            	<img src="<?= base_url(); ?><?= $image->image; ?>" style="width: 250px; height: 90px;">
        	</a>
        <?php } ?>
    
    <button class="close" data-toggle="panel-mobile"><i class="ti ti-close"></i></button>
</div>
<nav class="module module-navigation"></nav>
	<?php $this->load->view('frontend/template/mobile_slider'); ?>
</nav>

<!-- Body Overlay -->
<div id="body-overlay"></div>

</div>
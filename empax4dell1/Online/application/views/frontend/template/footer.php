    </div>

	
    <script src="<?= base_url('assets/js/jquery.min.js') ?>"></script>
    <script src="<?= base_url('assets/js/bootstrap.min.js') ?>"></script>
    <script src="<?= base_url('assets/js/select2.min.js') ?>"></script>
    <script src="<?= base_url('assets/js/frontendScript.js') ?>"></script>
    <script src="<?= base_url('assets/js/user.js') ?>"></script>
    <script src="<?= base_url('assets/js/master.js') ?>"></script>


	<script src="<?= base_url('assets/plugins/tether/dist/js/tether.min.js') ?>"></script>
    <script src="<?= base_url('assets/plugins/slick-carousel/slick/slick.min.js') ?>"></script>
    <script src="<?= base_url('assets/plugins/jquery.appear/jquery.appear.js') ?>"></script>
    <script src="<?= base_url('assets/plugins/jquery.scrollto/jquery.scrollTo.min.js') ?>"></script>
    <script src="<?= base_url('assets/plugins/jquery.localscroll/jquery.localScroll.min.js') ?>"></script>
    <script src="<?= base_url('assets/plugins/jquery-validation/dist/jquery.validate.min.js') ?>"></script>
    <script src="<?= base_url('assets/plugins/jquery.mb.ytplayer/dist/jquery.mb.YTPlayer.min.js') ?>"></script>
    <script src="<?= base_url('assets/plugins/twitter-fetcher/js/twitterFetcher_min.js') ?>"></script>
    <script src="<?= base_url('assets/plugins/skrollr/dist/skrollr.min.js') ?>"></script>
    <script src="<?= base_url('assets/plugins/animsition/dist/js/animsition.min.js') ?>"></script>
    <script src="<?= base_url('assets/js/core.js') ?>"></script>


  </body>
</html>
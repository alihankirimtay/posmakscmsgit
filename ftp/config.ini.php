<?php 
	/** 
	* Configuration

	* @package VeriAsist
	* @author veriasist.com
	* @copyright 2014
	* @version Id: config.ini.php, v4.00 2015-02-27 16:20:51 gewa Exp $
	*/
 
	 if (!defined("_VALID_PHP")) 
     die('Direct access to this location is not allowed.');
 
	/** 
	* Database Constants - these constants refer to 
	* the database configuration settings. 
	*/
	 define('DB_SERVER', 'localhost'); 
	 define('DB_USER', 'root'); 
	 define('DB_PASS', 'root'); 
	 define('DB_DATABASE', 'Posmaks');
 
	/** 
	* Show MySql Errors. 
	* Not recomended for live site. true/false 
	*/
	$DEBUG = false; 
?>
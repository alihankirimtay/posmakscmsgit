<?php
  /**
   * User
   *
   * @package VeriAsist
   * @author veriasist.com
   * @copyright 2014
   * @version $Id: user.php, v4.00 2014-04-20 10:12:05 gewa Exp $
   */
  define("_VALID_PHP", true);
  require_once("../init.php");
?>
<?php
  /* == Registration == */
  if (isset($_POST['doRegister'])):
      $user->register();
  endif;

  /* == Password Reset == */
  if (isset($_POST['passReset'])):
      $user->passReset();
  endif;

  /* == Account Acctivation == */
  if (isset($_POST['accActivate'])):
      $user->activateUser();
  endif;
?>















<?php

  // Create demo request
  if (isset($_POST['processDemoRequest'])) {
    
    Filter::checkPost('folder', 'Erişim Adresi');
    Filter::checkPost('name', Lang::$word->_MOD_WS_CUST_NAME);
    Filter::checkPost('phone', 'Telefon');
    Filter::checkPost('email', 'E-posta');

    
		

    $data = [
      'folder'   => sanitize(str_replace('.posmaks.com', '', post('folder'))),
      'customer' => [
        'name'    => sanitize(post('name')),
        'company' => sanitize(post('company')),
        'phone'   => sanitize(post('phone')),
        'email'   => sanitize(post('email')),
      ],
      'created' => "NOW()",
      'status'  => '-1'
    ];

    $data['folder'] = strtolower($data['folder']);

    if (!Registry::get('Users')->isValidEmail($data['customer']['email'])) {
      Filter::$msgs['email'] = Lang::$word->_UR_EMAIL_R2;
    } elseif (Registry::get('Users')->emailExists($data['customer']['email'])) {
      Filter::$msgs['email'] = Lang::$word->_UR_EMAIL_R1;
    }

    if (preg_match('/[^a-zA-Z0-9]/', $data['folder'])) {
      Filter::$msgs['folder1'] = 'Erişim adresi sadece harf veya rakam içermelidir.';
    }

    


    if (empty(Filter::$msgs)) {



      // Aynı subdomain/klasör var mı?
      if (is_dir(BASEPATH . $data['folder']) || getValue('folder', 'mod_websites', "deleted IS NULL AND folder = '{$data['folder']}'")) {
        $json['type'] = 'error';
        $json['message'] = '
        <div class="alert alert-danger">
          <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
          Bu Web Site Adı zaten mevcut. Lütfen başka bir Demo Adresi Deneyiniz.
        </div>';
        exit(json_encode($json));
      }

      $response = $_POST["g-recaptcha-response"];
		
      $url = 'https://www.google.com/recaptcha/api/siteverify';
      $data_recaptcha = array(
        'secret' => '6Ldko2AUAAAAABxfwAPi6fiI2m4zFuPa2UsA1YYQ',
        'response' => $_POST["g-recaptcha-response"]
      );
      $options = array(
        'http' => array (
          'header' => "Content-Type: application/x-www-form-urlencoded\r\n",
          'method' => 'POST',
          'content' => http_build_query($data_recaptcha)
        )
      );
      $context  = stream_context_create($options);
      $verify = file_get_contents($url, false, $context);
      $captcha_success=json_decode($verify);

      if($captcha_success->success==false) {
        $json['type'] = 'error';
        $json['message'] = 'Doğrulama boş bırakılamaz';
        exit(json_encode($json));
      }

      $data_insert = $data;
      $data_insert['customer'] = json_encode($data_insert['customer']);
      Registry::get("Database")->insert('mod_websites', $data_insert);

      if (Registry::get("Database")->affected()) {
        $json['type'] = 'success';
        $json['message'] = '
        <div class="alert alert-success">
          POSMAKS demo kullanım hesabınız en kısa sürede aktif edilerek sizlere bildirilecektir. İlginiz için teşekkür ederiz.
        </div>';

        // send mail to admin
        require_once(BASEPATH . "lib/class_mailer.php");
        $mailer = Mailer::sendMail();
        $msg = Swift_Message::newInstance()
          ->setSubject('Posmaks Demo Talebi')
          ->setTo(array($core->site_email => $core->site_name))
          ->setFrom(array($core->site_email => "Posmaks.com"))
          ->setBody(cleanOut("
            <b>{$data['folder']}</b>, için demo talebi mevcut.<br>
            <b>Bilgiler:</b><br>
            {$data['customer']['name']}<br>
            {$data['customer']['company']}<br>
            {$data['customer']['phone']}<br>
            {$data['customer']['email']}<br>
          "), 'text/html');
        $mailer->send($msg);

        $row = Registry::get("Core")->getRowById(Content::eTable, 18);

        $body = str_replace(array('[NAME]', '[SITE_NAME]', '[URL]'), 
        array($data['customer']['name'], $core->site_name, SITEURL), $row->{'body'.Lang::$lang});
        $user_msg = Swift_Message::newInstance()
              ->setSubject("Posmaks'a Hoşgeldiniz")
              ->setTo($data['customer']['email'])
              ->setFrom(array($core->site_email => "Posmaks.com"))
              ->setBody(cleanOut($body), 'text/html');
        

        $mailer->send($user_msg);
          

      } else {
        $json['type'] = 'error';
        $json['message'] = Lang::$word->_SYSTEM_PROCCESS;
      }
      print json_encode($json);

      
    } else {
      $json['message'] = Filter::msgStatus();
      print json_encode($json);
    }
    
  }


?>
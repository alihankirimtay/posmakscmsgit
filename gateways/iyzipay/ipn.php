<?php
  /**
   * Iyzipay IPN
   *
   * @package VeriAsist
   * @author veriasist.com
   * @copyright 2014
   * @version $Id: ipn.php, v4.00 2014-04-12 10:12:05 gewa Exp $
   */
	define("_VALID_PHP", true);
	define("_PIPN", true);


	ini_set('log_errors', true);
	ini_set('error_log', dirname(__file__) . '/ipn_errors.log');

	$status = 0;
	$message = 'Bir hata oluştu!';
	$redirect = isset($_GET['redirect']) ? $_GET['redirect'] : SITEURL;

	if (isset($_POST)) {
    	require_once ('../../init.php');
    	require_once (BASEPATH . 'gateways/iyzipay/config.php');

    	$paymentId = isset($_POST['paymentId']) ? $_POST['paymentId'] : 0;
		$conversationId = isset($_POST['conversationId']) ? $_POST['conversationId'] : 0;
		$conversationData = isset($_POST['conversationData']) ? $_POST['conversationData'] : null;
		$mdStatus = !empty($_POST['mdStatus']) ? $_POST['mdStatus'] : 7;


	    try {

			$gateway = Core::getRow(Membership::gTable, 'name', 'iyzipay');

			$request = new \Iyzipay\Request\CreateThreedsPaymentRequest();
			$request->setLocale(\Iyzipay\Model\Locale::TR);
			$request->setConversationId($conversationId);
			$request->setPaymentId($paymentId);
			$request->setConversationData($conversationData);

			$threedsPayment = \Iyzipay\Model\ThreedsPayment::create($request, Config::options($gateway->extra, $gateway->extra3, $gateway->live));
			$items = $threedsPayment->getPaymentItems();

	    	if ($threedsPayment->getStatus() == 'success') {

				$payment = Core::getRow(Membership::pTable, 'txn_id', $conversationId);

				if (!$payment) {
					throw new Exception('Geçersiz ödeme.');
				} else if ($payment->status == 1) {
					throw new Exception('Ödeme daha önce alındı.');
				}

				$user_id = $payment->user_id;
				$membership_id = $payment->membership_id;
				$payment_id = $payment->id;

				$db->update(Membership::pTable, [			
					'extra' => json_encode([
						'paymentId' => $threedsPayment->getPaymentId(),
						'paymentTransactionId' => $items[0]->getPaymentTransactionId(),
						'paidPrice'	=> $items[0]->getPaidPrice(),
						'paidPriceConverted' => $items[0]->getConvertedPayout()->getPaidPrice()
					])
				],'id='. $payment_id);

				if ($buyer = Core::getRow('users', 'id', $user_id, false)) {

					if ($membership = Core::getRow(Membership::mTable, 'id', $membership_id)) {

						$db->update(Membership::pTable, [			
							'status' => 1,
						],'id='. $payment_id);	

						$status = 1;
						$message = 'Ödeme başarılı.';

						$mem_expire = $user->calculateDays($membership->id);
						$db->update(Users::uTable, [
							'membership_id' => $membership->id,
							'mem_expire' => $mem_expire,
							'trial_used' => ($membership->trial == 1) ? 1 : 0,
							'memused' => 1
						], 'id=' . $buyer->id);

						// Update The Users website expire date
						$DB_NAME = $buyer->database_name;
						$mysqli = @new mysqli(DB_SERVER, DB_USER, DB_PASS, $DB_NAME);
						if ($mysqli->connect_error) {
							error_log($DB_NAME . ' veritabanına bağlantı hatası yaşandı.'.PHP_EOL, 3, 'iyzipay_errorlog.log');
						} else {
							$mysqli->query("UPDATE settings SET date_expire = '{$mem_expire}' WHERE id = 1");
							$mysqli->close();
						}
						
						$mail_template = Core::getRowById(Content::eTable, 5);
						
						// == Notify Administrator ==
						require_once (BASEPATH . "lib/class_mailer.php");
						$username = getValueById('username', Users::uTable, $buyer->id);

						$body = cleanOut(
							str_replace(
								[
									'[USERNAME]',
									'[ITEMNAME]',
									'[PRICE]',
									'[STATUS]',
									'[PP]',
									'[IP]'
								],
								[
									$username,
									$membership->{'title' . Lang::$lang},
									$membership->price .' / '. $paid_price,
									$status ? 'Completed' : 'Failed',
									'Iyzipay',
									$_SERVER['REMOTE_ADDR']
								],
								$mail_template->{'body' . Lang::$lang}
							)
						);

						$mailer = Mailer::sendMail();
						$messageObject = Swift_Message::newInstance()
						->setSubject($mail_template->{'subject' . Lang::$lang})
						->setTo(array($core->site_email => $core->site_name))
						->setFrom(array($core->site_email => $core->site_name))
						->setBody($body, 'text/html');
						$mailer->send($messageObject);

						if ($status)
							Security::writeLog($username . ' ' . Lang::$word->_LG_PAYMENT_OK . ' ' . $membership->{'title' . Lang::$lang}, "", "yes", "payment");
						else
							Security::writeLog(Lang::$word->_LG_PAYMENT_ERR . $username, "", "yes", "payment");

					} else {
						$message = 'Invalid membership.';
					}
				} else {
					$message = 'Invalid user.';
				}

	    	} else {

	    		$message = $threedsPayment->getErrorMessage();
	    		error_log($message.PHP_EOL, 3, 'iyzipay_errorlog.log');
			}

	    } catch (Exception $e) {
	    	$message = $e->getMessage();
	    	error_log($message.json_encode($_POST).PHP_EOL, 3, 'iyzipay_errorlog.log');
	    }
	}
?>

<form id="redirect" action="<?= $redirect ?>" method="post">
	<input type="hidden" name="payment-status" value="<?= $status ?>">
	<input type="hidden" name="payment-message" value="<?= htmlspecialchars($message) ?>">
</form>
<script type="text/javascript">
	document.getElementById('redirect').submit();
</script>
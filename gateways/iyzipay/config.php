<?php

require_once(BASEPATH . 'gateways/iyzipay/IyzipayBootstrap.php');

IyzipayBootstrap::init();

class Config
{
	public static function options($ApiKey = null, $SecretKey = null, $live = true)
    {
    	$BaseUrl = $live ? 'https://api.iyzipay.com' : 'https://sandbox-api.iyzipay.com';

        $options = new \Iyzipay\Options();
        $options->setApiKey($ApiKey);
        $options->setSecretKey($SecretKey);
        $options->setBaseUrl($BaseUrl);
        return $options;
    }
}